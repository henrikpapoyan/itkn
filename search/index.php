<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/intranet/public/search/index.php");
$APPLICATION->SetTitle(GetMessage("SEARCH_TITLE"));
?>

<?$APPLICATION->IncludeComponent(
	"bitrix:search.page", 
	"icons", 
	array(
		"AJAX_MODE" => "N",
		"RESTART" => "N",
		"CHECK_DATES" => "Y",
		"USE_TITLE_RANK" => "Y",
		"arrWHERE" => array(
			0 => "iblock_news",
			1 => "iblock_structure",
			2 => "blog",
			3 => "socialnetwork",
			4 => "intranet",
		),
		"arrFILTER" => array(
			0 => "iblock_news",
			1 => "iblock_events",
			2 => "iblock_photos",
			3 => "intranet",
		),
		"SHOW_WHERE" => "Y",
		"PAGE_RESULT_COUNT" => "50",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"PAGER_TITLE" => GetMessage("SEARCH_RESULT"),
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "",
		"SHOW_RATING" => "",
		"RATING_TYPE" => "",
		"PATH_TO_USER_EDIT" => "/company/personal/user/#user_id#/edit/",
		"PATH_TO_USER_PROFILE" => "/company/personal/user/#user_id#/",
		"AJAX_OPTION_SHADOW" => "Y",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"COMPONENT_TEMPLATE" => "icons",
		"NO_WORD_LOGIC" => "N",
		"DEFAULT_SORT" => "rank",
		"FILTER_NAME" => "",
		"arrFILTER_forum" => array(
			0 => "all",
		),
		"arrFILTER_iblock_news" => array(
			0 => "all",
		),
		"arrFILTER_iblock_structure" => array(
			0 => "5",
		),
		"arrFILTER_iblock_events" => array(
			0 => "all",
		),
		"arrFILTER_iblock_photos" => array(
			0 => "all",
		),
		"arrFILTER_iblock_bitrix_processes" => array(
			0 => "all",
		),
		"arrFILTER_blog" => array(
			0 => "all",
		),
		"arrFILTER_socialnetwork" => array(
			0 => "all",
		),
		"arrFILTER_socialnetwork_user" => "",
		"SHOW_WHEN" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"USE_LANGUAGE_GUESS" => "Y",
		"USE_SUGGEST" => "N",
		"SHOW_ITEM_TAGS" => "Y",
		"TAGS_INHERIT" => "Y",
		"SHOW_ITEM_DATE_CHANGE" => "Y",
		"SHOW_ORDER_BY" => "Y",
		"SHOW_TAGS_CLOUD" => "N",
		"DISPLAY_TOP_PAGER" => "Y",
		"DISPLAY_BOTTOM_PAGER" => "Y"
	),
	false
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>