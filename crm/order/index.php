<?php
/**
 * User: German
 * Date: 31.10.2017
 * Time: 10:08
 */
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/intranet/public/crm/lead/index.php");
?>

<style>
.bx-filter-add-button {
    display:none;
}
</style>




<?$APPLICATION->IncludeComponent(
    "bitrix:crm.lead",
    "",
    array(
        "SEF_MODE" => "Y",
        "PATH_TO_CONTACT_SHOW" => "/crm/contact/show/#contact_id#/",
        "PATH_TO_CONTACT_EDIT" => "/crm/contact/edit/#contact_id#/",
        "PATH_TO_COMPANY_SHOW" => "/crm/company/show/#company_id#/",
        "PATH_TO_COMPANY_EDIT" => "/crm/company/edit/#company_id#/",
        "PATH_TO_DEAL_SHOW" => "/crm/deal/show/#deal_id#/",
        "PATH_TO_DEAL_EDIT" => "/crm/deal/edit/#deal_id#/",
        "PATH_TO_USER_PROFILE" => "/company/personal/user/#user_id#/",
        "PATH_TO_PRODUCT_EDIT" => "/crm/product/edit/#product_id#/",
        "PATH_TO_PRODUCT_SHOW" => "/crm/product/show/#product_id#/",
        "ELEMENT_ID" => $_REQUEST["lead_id"],
        "SEF_FOLDER" => "/crm/",
        "COMPONENT_TEMPLATE" => "htmls.lead.disk",
        "NAME_TEMPLATE" => "",
        "SECTION" => "ORDER",
        "SEF_URL_TEMPLATES" => array(
            "index" => "order/index.php",
            "list" => "lead/list/",
            "edit" => "order/edit/#lead_id#/",
            "show" => "order/show/#lead_id#/",
            "convert" => "order/convert/#lead_id#/",
            "import" => "order/import/",
            "service" => "order/service/",
            "widget" => "order/widget/",
        )
    ),
    false
);
$APPLICATION->SetTitle("Заказы");

?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>