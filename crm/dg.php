#!/usr/bin/php
<?php
/**
 * User: German
 * Date: 02.06.2017
 * Time: 14:56
 */
@set_time_limit(0);

//require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
global $USER;

$bRes = $USER->Authorize(372); // авторизуем

if (!CModule::IncludeModule('highloadblock'))
{
    ShowError(GetMessage("F_NO_MODULE"));
    return 0;
}
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

$dogovor_id = file_get_contents(__FILE__.".id");
file_put_contents(__FILE__.".log", "dogovor_id=[".print_r($dogovor_id, true)."]\n", FILE_APPEND);
$c = 1;
while($dogovor_id > 0 && $c > 0)
{
    $c = AddDogovor1000($dogovor_id);
    $dogovor_id = file_get_contents(__FILE__.".id");
    file_put_contents(__FILE__.".log", "dogovor_id=[".print_r($dogovor_id, true)."]\n", FILE_APPEND);
    file_put_contents(__FILE__.".log", "c=[".print_r($c, true)."]\n", FILE_APPEND);
}

function AddDogovor1000($dogovor_id)
{

    $c = 0;

    $hlblock_id = 2;

    $rsHL = HL\HighloadBlockTable::getById($hlblock_id);
    $hlblock = $rsHL->fetch();
    if (empty($hlblock))
    {
        ShowError(GetMessage('HLBLOCK_LIST_404'));
        return 0;
    }

    $entity = HL\HighloadBlockTable::compileEntity($hlblock);

    // uf info
    $fields = $GLOBALS['USER_FIELD_MANAGER']->GetUserFields('HLBLOCK_'.$hlblock['ID'], 0, LANGUAGE_ID);

    // sort
    $sort_id = 'ID';
    $sort_type = 'ASC';

    // start query
    $mainQuery = new Entity\Query($entity);
    $mainQuery->setSelect(array('*'));
    $mainQuery->setOrder(array($sort_id => $sort_type));
    $mainQuery->setFilter(array('>ID' => $dogovor_id));
    //$mainQuery->setFilter(array('=UF_VLADELETS' => '987a4271-322f-4960-a3f1-57ec694e2667'));
    //$mainQuery->setFilter(array('=UF_VLADELETS' => '18b8e79b-702e-47fb-a4b6-5d80ebd55e9e'));

    //UF_VLADELETS] => 987a4271-322f-4960-a3f1-57ec694e2667
    // execute query
    //	->setGroup($group)
    //	->setOptions($options);
    $result = $mainQuery->exec();
    $result = new CDBResult($result);
    //if
    $base_memory_usage = memory_get_usage();
    file_put_contents(__FILE__.".mem.log", "base =$base_memory_usage\n", FILE_APPEND);
    while($row = $result->fetch())
    {
        $c++;
        if ($row['ID'] < $dogovor_id)
        {

            file_put_contents(__FILE__.".time.log", date("d.m.Y H:i:s")."]001 ".$row['ID']."+\n", FILE_APPEND);
            file_put_contents(__FILE__.".time2.log", date("d.m.Y H:i:s")."]001 ".$row['ID']."+\n", FILE_APPEND);
            file_put_contents(__FILE__.".id", $row['ID']);
            continue;
        }
        $begin_time = microtime(true);
        file_put_contents(__FILE__.".time.log", date("d.m.Y H:i:s")."]001 ".$row['ID']."\n", FILE_APPEND);
        file_put_contents(__FILE__.".time2.log", date("d.m.Y H:i:s")."]001 ".$row['ID']."\n", FILE_APPEND);

        gk_AddDogovor($row);
        file_put_contents(__FILE__.".id", $row['ID']);
        $work_time = microtime(true) - $begin_time;
        file_put_contents(__FILE__.".time.log", date("d.m.Y H:i:s")."]004\n", FILE_APPEND);
        file_put_contents(__FILE__.".time2.log", date("d.m.Y H:i:s")."]004 ".$work_time."\n", FILE_APPEND);
        $mem = memory_get_usage();
        file_put_contents(__FILE__.".mem.log", "base =$base_memory_usage mem=$mem\n", FILE_APPEND);
        if ($c == 1000)
        {
            file_put_contents(__FILE__.".1000.log", date("d.m.Y H:i:s")."]001 ".$row['ID']."\n", FILE_APPEND);
            break;
        }
    }
    unset($result);
    unset($mainQuery);
    return $c;
}

function gk_AddDogovor($row)
{
    $IBLOCK_ID = GetIBlockIDByCode('registry_contracts');

    if ($row['UF_VIDDOGOVORA'] == "СПокупателем")
    {

        $rsCompany = CCrmCompany::GetList(array(),array('UF_LINK' => $row['UF_VLADELETS']));
            file_put_contents(__FILE__.".log", "UF_VLADELETS=[".print_r($row['UF_VLADELETS'], true)."]\n", FILE_APPEND);

        if ($arCompany = $rsCompany->Fetch())
        {
            $company_id = $arCompany['ID'];
            file_put_contents(__FILE__.".log", "company_id=[".print_r($company_id, true)."]\n", FILE_APPEND);
            $arSelect = Array();//"ID", "NAME", "DATE_ACTIVE_FROM");
            $arFilter = Array("IBLOCK_ID" => $IBLOCK_ID, "XML_ID" => $row['UF_XML_ID']);
            $rsDogovor = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
            if($arDogovor = $rsDogovor->Fetch())
            {
                file_put_contents(__FILE__.".log", "arDogovor=[".print_r($arDogovor, true)."]\n", FILE_APPEND);
                $dogovor_id = $arDogovor['ID'];
            }
            else
            {
                $org = "";
                if ($row['UF_ORGANIZATSIYA'] == 'АО "ЭСК"')
                {
                    $org = "ЭСК";
                }

                $tipDogovora = '';
                $tipContrageta = '';
                if ($row['UF_VIDDOGOVORA'] == 'СПокупателем')//Прочее)
                {
                    $tipDogovora = 'Клиент';
                    $tipContrageta = 'Клиент';
                }

                $PROP = array('KOMPANIYA_ZAKAZCHIK' => $org,
                    'KONTRAGENT' => $arCompany['ID'],
                    'TIP_DOGOVORA_NEW' => $tipDogovora,
                    'TIP_KONTRAGENTA' => $tipContrageta,
                    '_DOGOVORA' => $row['UF_NOMER'],
                    'DATA_DOGOVORA' => $row['UF_DATA']->format("d.m.Y H:i:s"),
                    'STATUS_DOGOVORA'=> 872,//''
                );


                $arFields = array("IBLOCK_ID" => $IBLOCK_ID,
                    "XML_ID" => $row['UF_XML_ID'],
                    "NAME" => $row['UF_NAME'],
                    "PROPERTY_VALUES"=> $PROP,
                );

                file_put_contents(__FILE__.".log", "row=[".print_r($row, true)."]\n", FILE_APPEND);
                file_put_contents(__FILE__.".log", "arCompany=[".print_r($arCompany, true)."]\n", FILE_APPEND);

                $CIBlockElement = new CIBlockElement();
                $dogovor_id = $CIBlockElement->Add($arFields);
                file_put_contents(__FILE__.".log", "00104dogovor_id=[".print_r($dogovor_id, true)."]\n", FILE_APPEND);
//                file_put_contents(__FILE__.".log", "LAST_ERROR=[".print_r($CIBlockElement->LAST_ERROR, true)."]\n", FILE_APPEND);
                unset($CIBlockElement);
                unset($arFields);
                unset($PROP);

            }
            unset($arDogovor);
            unset($rsDogovor);
            if ($dogovor_id > 0)
            {

                $rsDeal = CCrmDeal::GetList(array(), array('UF_DOGOVOR' => $dogovor_id));
                if($arDeal = $rsDeal->Fetch())
                {
                    file_put_contents(__FILE__.".log", "arDeal=[".print_r($arDeal, true)."]\n", FILE_APPEND);
                }
                else
                {
                    $CCrmDeal = new CCrmDeal();
                    $arFields = array(
                        'TITLE' => 'Сделка '.$row['UF_NAME'],
                        'COMPANY_ID' => $company_id,
                        'CATEGORY_ID' => 1,
                        'UF_DOGOVOR' => array($dogovor_id),
                        'STAGE_ID' => 'C1:NEGOTIATION',

                    );
                    $deal_id = $CCrmDeal->Add($arFields);
                    file_put_contents(__FILE__.".log", "deal_id=[".print_r($deal_id, true)."]\n", FILE_APPEND);
                    file_put_contents(__FILE__.".log", "LAST_ERROR=[".print_r($CCrmDeal->LAST_ERROR, true)."]\n", FILE_APPEND);
                    unset($CCrmDeal);
                    unset($arFields);
                }
                unset($rsDeal);
                unset($arDeal);
//                break;
            }
//            break;
        }
        unset($arCompany);
        unset($rsCompany);
    }
}

//require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");