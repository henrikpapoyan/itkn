<?php
/**
 * User: German
 * Date: 20.12.2017
 * Time: 14:04
 */
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
if(!CModule::IncludeModule('sale'))
{
    return false;
}
$arFilter = array();
$rsInvoice = CCrmInvoice::GetList(array(), $arFilter);
$arInvoiceAll = array();
$loggerTax = Logger::getLogger("InvoiceTax", "Invoice/ics/icsTax.log");

while($arInvoice = $rsInvoice->Fetch())
{
//    $logger->log("00020 [".print_r($arInvoice,true));

    $number1C = trim($arInvoice['ACCOUNT_NUMBER']);
//    $year = substr($arInvoice['DATE_BILL'], 6, 4);
//    if($year != 2017 )
//    {
//        $number1c = $arInvoice['ACCOUNT_NUMBER']." ($year)";
//    }

    $arInvoiceAll[$number1C]['ACCOUNT_NUMBER'] = $number1C;
    $arInvoiceAll[$number1C]['DATE_BILL'] = $arInvoice['DATE_BILL'];
    $arInvoiceAll[$number1C]['PRICE'] = $arInvoice['PRICE'];
    $arInvoiceAll[$number1C]['ID'] = $arInvoice['ID'];
    $arInvoiceAll[$number1C]['STATUS_ID'] = $arInvoice['STATUS_ID'];
    $arInvoiceAll[$number1C]['PAY_VOUCHER_NUM'] = $arInvoice['PAY_VOUCHER_NUM'];
    $arInvoiceAll[$number1C]['PAY_VOUCHER_DATE'] = $arInvoice['PAY_VOUCHER_DATE'];
    $arInvoiceAll[$number1C]['UF_LINK'] = $arInvoice['UF_LINK'];
    $arInvoiceAll[$number1C]['COMMENTS'] = $arInvoice['COMMENTS'];
//    $loggerTax->log("number1C=[$number1C]");
    $arProductRows = CCrmInvoice::GetProductRows($arInvoiceAll[$number1C]['ID']);
    foreach ($arProductRows as $ipr => $vpr) {
        if($vpr['VAT_INCLUDED'] != 'Y')
        {
            $loggerTax->log("[$number1C] VAT_INCLUDED=[".print_r($vpr['VAT_INCLUDED'], true)."]");
            $loggerTax->log("arInvoiceAll[$number1C]=[".print_r($arInvoiceAll[$number1C], true)."]");
            $loggerTax->log("vpr=[".print_r($vpr, true)."]");
            $arFields = array(
                'VAT_INCLUDED' => 'Y'
            );
            $ret = CSaleBasket::Update($vpr['ID'], $arFields);
            $loggerTax->log("ret=[".print_r($ret, true)."]");
        }
    }

}
$logger = Logger::getLogger("InvoiceICS", "Invoice/ics/ics.log");
$fname = $_SERVER["DOCUMENT_ROOT"]."/log/Invoice/schet.csv";
$arLines = file($fname);
$arAccountNumber = array();
$arAddInvoice = array();
foreach ($arLines as $k => $line) {
    $arLine = explode(";", $line);
    $number1c = trim($arLine[0]);
    $uf_link = $arLine[14];
//    file_put_contents(__FILE__.".log", "arLine=[".print_r($arLine, true)."]\n", FILE_APPEND);
//    if(array_search($number1c, $arAccountNumber) !== false)
//    {
//        file_put_contents(__FILE__.".log", "arLine=[".print_r($arLine, true)."]\n", FILE_APPEND);
        $arAddInvoice[$uf_link]['data'] = $arLine;
//    }
}

//$logger->log("arAddInvoice=[".print_r($arAddInvoice, true));
//return;
$fname = $_SERVER["DOCUMENT_ROOT"]."/log/Invoice/schet2.csv";

if (($handle = fopen($fname, "r")) !== FALSE) {
    $row = 0;
    $CCrmInvoice = new CCrmInvoice();
    while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
        $uf_link = $data[0];
        if(isset($arAddInvoice[$uf_link]))
        {
            $arAddInvoice[$uf_link]['rows'][] = $data;
        }
    }
}
//$logger->log("arInvoiceAll=[".print_r($arInvoiceAll, true));
//$logger->log("arAddInvoice=[".print_r($arAddInvoice, true));
//return;
$loggerNoEqual = Logger::getLogger("InvoiceNoEqual", "Invoice/ics/icsNoEqual.log");
$loggerNoEqualNum = Logger::getLogger("InvoiceNoEqualNum", "Invoice/ics/icsNoEqualNum.log");
$loggerNoEqualNum2 = Logger::getLogger("InvoiceNoEqualNum2", "Invoice/ics/icsNoEqualNum2.log");
$loggerDiff = Logger::getLogger("InvoiceDiff", "Invoice/ics/icsDiff.log");
$loggerKol = Logger::getLogger("InvoiceKol", "Invoice/ics/icsKol.log");
$loggerSum = Logger::getLogger("InvoiceSum", "Invoice/ics/icsSum.log");
$loggerSum2 = Logger::getLogger("InvoiceSum2", "Invoice/ics/icsSum2.log");
$loggerDel = Logger::getLogger("InvoiceDel", "Invoice/ics/icsDel.log");
$loggerDelNum = Logger::getLogger("InvoiceDelNum", "Invoice/ics/icsDelNum.log");
$loggerDelBitrix = Logger::getLogger("InvoiceDelBitrix", "Invoice/ics/icsDelBitrix.log");
$loggerDelJSON = Logger::getLogger("InvoiceDelJSON", "Invoice/ics/icsDelJSON.log");
$loggerAdd = Logger::getLogger("InvoiceAdd", "Invoice/ics/icsAdd.log");
$loggerAddNum = Logger::getLogger("InvoiceAddNum", "Invoice/ics/icsAddNum.log");
$loggerAddBitrix = Logger::getLogger("InvoiceAddBitrix", "Invoice/ics/icsAddBitrix.log");
$loggerDiff2 = Logger::getLogger("InvoiceDiff2", "Invoice/ics/icsDiff2.log");
$c=0;
$cNoEqual = 0;
$cDel = 0;
$cDelAll = 0;
$cAdd = 0;
$cSum = 0;
foreach ($arAddInvoice as $k => &$v) {
    $c++;
    $logger->log(print_r($v, true));
    $number1c = trim($v['data'][0]);
    $year = substr($v['data'][1],6,4);
    $logger->log("year=[".print_r($year, true));
    if( $year != '2017' )
    {
        $number1c = $v['data'][0]." ($year)";
    }
    $logger->log("number1c=[".print_r($number1c, true));
    if (count($v['rows'])>10)
    {
        $new_product_rows = array();
        foreach ($v['rows'] as $key => $val) {
            $xml_id_price = $val[15].$val[5];
            if($new_product_rows[$xml_id_price])
            {
                $new_product_rows[$xml_id_price][4] = $new_product_rows[$xml_id_price][4] + $val[4];
            }
            else
            {
                $new_product_rows[$xml_id_price] = $val;
            }
        }
        $v['rows'] = $new_product_rows;
//                    $logger20->log($DATA['account_number']);
//                    $logger20JSON->log(json_encode($DATA));
//                    return array('ret' => "-4");

    }
    if(isset($arInvoiceAll[$number1c]))
    {
        $summa1c = $v['data'][6];
        $summa1c = str_replace(" ", "", $summa1c);
        $summa1c = str_replace(",", ".", $summa1c);
        $summa = 0;
        foreach ($v['rows'] as $ip => $vp) {
            $logger->log("vp=[".print_r($vp, true)."]");
            $kol = $vp[4];
            if ($kol < 1)
            {
                $kol = 1;
            }
            $kol = str_replace(" ", "", $kol);
            $price = $vp[5];
            $price = str_replace(" ", "", $price);
            $price = str_replace(",", ".", $price);
            $summa_calc = $kol*$price;
            $summa = $summa + $summa_calc;
            $summa_row = $vp[6];
            $summa_row = str_replace(" ", "", $summa_row);
            $summa_row = str_replace(",", ".", $summa_row);

            $logger->log("$kol * $price = $summa_calc == $summa_row");
    //        if()
        }
        $logger->log("summa $summa == ".$arInvoiceAll[$number1c]['PRICE']. " == " .$summa1c);
//        $logger->log("v =[".print_r($v, true));
        $logger->log("arInvoiceAll[$number1c]".print_r($arInvoiceAll[$number1c], true));
//    19526.72 == 19526.72 == 19526.3
//    if($v['data'][0] == '00БП-002368')//'00БП-001545')
//    {
//        var_dump(strval($summa), $v['invoice']['PRICE'], $summa1c);
//        echo "<br>";
//        var_dump((floatval($summa) - round(floatval($v['invoice']['PRICE']),4)),floatval($summa), floatval($v['invoice']['PRICE']), floatval($summa1c));
//        echo "<br>";
//        var_dump((floatval($summa) == floatval($v['invoice']['PRICE'])),(floatval($v['invoice']['PRICE']) != floatval($summa1c)));
//        echo "<br>* ";
//        var_dump(abs((floatval($summa) - floatval($v['invoice']['PRICE']))) < 0.0001,(floatval($v['invoice']['PRICE']) != floatval($summa1c)));
//        echo "<br>";
//        var_dump((floatval($summa) == floatval($v['invoice']['PRICE'])) && (floatval($v['invoice']['PRICE']) != floatval($summa1c)));
//    }
        if((floatval(strval($summa)) != floatval($arInvoiceAll[$number1c]['PRICE']))
//            && (floatval($v['invoice']['PRICE']) != floatval($summa1c))
        )
        {
            $cNoEqual++;
            $loggerNoEqual->log("summa $summa == ".$arInvoiceAll[$number1c]['PRICE']. " == " .$summa1c);
            $loggerNoEqual->log("arInvoiceAll[$number1c]".print_r($arInvoiceAll[$number1c], true));
            $loggerNoEqual->log("v=[".print_r($v,true));
            if($v['data'][7] > 0)
            {
                $loggerNoEqualNum2->log('"'.$number1c.
                    '",'."summa $summa == ".$arInvoiceAll[$number1c]['PRICE']. " == " .$summa1c.
                    " bx=".count($arProductRows)." 1c=".count($v['rows']));
            }
            else
            {
                $loggerNoEqualNum->log('"'.$number1c.
                    '", '."summa $summa == ".$arInvoiceAll[$number1c]['PRICE']. " == " .$summa1c.
                    " bx=".count($arProductRows)." 1c=".count($v['rows']));
            }
        }

//    else {
            $loggerDiff->log("$summa == " . $v['invoice']['PRICE'] . " == " . $summa1c);
    //        $loggerDiff->log($v);
            $arProductRows = CCrmInvoice::GetProductRows($arInvoiceAll[$number1c]['ID']);
//            $arProductCount = array();
//            foreach ($arProductRows as $kproduct => $vproduct) {
//                if (isset($arProductCount[$vproduct['PRODUCT_NAME']]))
//                {
//                    $arProductCount[$vproduct['PRODUCT_NAME']]++;
//
//                }
//                else
//                {
//                    $arProductCount[$vproduct['PRODUCT_NAME']] = 1;
//                }
//            }

            $loggerDiff->log("arProductRows=" . print_r($arProductRows, true));
            $loggerDiff->log("v[rows]=" . print_r($v['rows'], true));
            if (count($arProductRows) == count($v['rows'])) {
                $cSum ++;
                $loggerDiff2->log("arProductRows=" . print_r($arProductRows, true));
                $loggerDiff2->log("v[rows]=" . print_r($v['rows'], true));

                foreach ($arProductRows as $kproduct => $vproduct) {
                    $loggerDiff->log("vproduct=" . print_r($vproduct, true));
                    foreach ($v['rows'] as $kp1c => $vp1c) {
                        $kol1c = str_replace(" ", "", $vp1c[4]);
                        $price1c = str_replace(",", ".", str_replace(" ", "", $vp1c[5]));
                        if ($vproduct['PRODUCT_NAME'] == $vp1c[2]) {
                            if ($vproduct['PRICE'] == $price1c) {
                                if ($vproduct['QUANTITY'] != $kol1c) {
                                    $loggerKol->log("vproduct=" . print_r($vproduct, true));
                                    $loggerKol->log("kol1c=$kol1c price1c = $price1c");
                                    $loggerKol->log("vp1c=" . print_r($vp1c, true));
                                    $arFields = array(
                                        'QUANTITY' => $kol1c
                                    );
    //                                $ret = CSaleBasket::Update($vproduct['ID'], $arFields);
    //                                $loggerKol->log("ret=" . print_r($ret, true));
    //                                die();
                                } else {
    //                                $loggerSum->log("vproduct=".print_r($vproduct, true));
    //                                $loggerSum->log("kol1c=$kol1c price1c = $price1c");
    //                                $loggerSum->log("vp1c=".print_r($vp1c, true));

                                }
                            } else {
    //                            $arProductCount[$vproduct['PRODUCT_NAME']] > 0
                                $loggerSum->log("vproduct=" . print_r($vproduct, true));
                                $loggerSum->log("kol1c=$kol1c price1c = $price1c");
                                $loggerSum->log("vp1c=" . print_r($vp1c, true));
                                $loggerSum->log("arProductCount[".$vproduct['PRODUCT_NAME']."]=" . print_r($arProductCount[$vproduct['PRODUCT_NAME']], true));
                                if(count($arProductCount[$vproduct['PRODUCT_NAME']]) == 1)
                                {
                                    $arFields = array(
                                        'PRICE' => $price1c
                                    );
    //                                $ret = CSaleBasket::Update($vproduct['ID'], $arFields);
    //                                $loggerSum->log("ret=" . print_r($ret, true));

                                }
                            }
                        }
                    }

                }

            } elseif (count($arProductRows) > count($v['rows'])) {
                $cDelAll++;
                if($v[7] > 0)
                {
                    $loggerDelBitrix->log("arInvoiceAll[$number1c]=" . print_r($arInvoiceAll[$number1c], true));
                    $loggerDelBitrix->log("v=" . print_r($v, true));
                    $loggerDelBitrix->log("arProductRows=" . print_r($arProductRows, true));

                }
                else
                {
                    $loggerDelNum->log($number1c." count1c=".count($v['rows'])." countBx=".count($arProductRows));
                    $loggerDel->log("arInvoiceAll[$number1c]=" . print_r($arInvoiceAll[$number1c], true));
                    $loggerDel->log("v=" . print_r($v, true));
                    $loggerDel->log("arProductRows=" . print_r($arProductRows, true));
                }

//                $loggerDel->log("v[rows]=" . print_r($v['rows'], true));
                foreach ($arProductRows as $kproduct => $vproduct) {
                    $bDel = true;
                    $cRows = 0;
                    foreach ($v['rows'] as $kp1c => $vp1c) {
                        if($vproduct['PRODUCT_NAME'] == $vp1c[2])
                        {
                            $bDel = false;
                            $cRows++;
                        }
    //                    $loggerDel->log("vp1c=" . print_r($vp1c, true));
                    }
                    $loggerDel->log("cRows=" . print_r($cRows, true));

                    if($cRows == 1 && $arProductCount[$vproduct['PRODUCT_NAME']] > 0)
                    {
                        $loggerDel->log("arProductCount=" . print_r($arProductCount, true));
                        $loggerDel->log("arProductCount[".$vproduct['PRODUCT_NAME']."]=" . print_r($arProductCount[$vproduct['PRODUCT_NAME']], true));
                        $bDel = true;
                        foreach ($v['rows'] as $kp1c => $vp1c) {
                            $kol1c = str_replace(" ", "", $vp1c[4]);
                            $price1c = str_replace(",", ".", str_replace(" ", "", $vp1c[5]));
                            $loggerDel->log("kol1c=$kol1c price1c = $price1c");

                            $loggerDel->log("vp1c=" . print_r($vp1c, true));
                            if($vproduct['PRODUCT_NAME'] == $vp1c[2])
                            {
                                if($price1c == $vproduct['PRICE'])
                                {
                                    $loggerDel->log("==" . print_r($vproduct['PRICE'], true));
                                    $bDel = false;
                                }
                                else
                                {
                                    $loggerDel->log("<>" . print_r($vproduct['PRICE'], true));
                                }
                            }
                        }
                    }
                    if($bDel)
                    {
                        $loggerDel->log("del vproduct=" . print_r($vproduct, true));
                        $loggerDelJSON->log(json_encode($vproduct));
                        $cDel++;
    //                    $del = CSaleBasket::Delete($vproduct['ID']);
                        $loggerDel->log("del=[" . print_r($del, true)."]");
                    }
                }
            }
            elseif (count($arProductRows) < count($v['rows'])) {
                $cAdd++;
                if($v[7] > 0) {
                    $loggerAddBitrix->log("arProductRows=" . print_r($arProductRows, true));
                    $loggerAddBitrix->log("v[rows]=" . print_r($v['rows'], true));
                }
                else
                {
                    $loggerAddNum->log($number1c." count1c=".count($v['rows'])." countBx=".count($arProductRows));
                    $loggerAdd->log("arProductRows=" . print_r($arProductRows, true));
                    $loggerAdd->log("v[rows]=" . print_r($v['rows'], true));
                }
            }

    //    if($c == 5)
    //    {
    //        break;
    //    }
    //    }
    }
    else
    {

    }
}
$logger->log("c =$c");
$logger->log("cNoEqual =$cNoEqual");
$logger->log("cSum =$cSum");
$logger->log("cDelAll =$cDelAll");
$logger->log("cDel =$cDel");
$logger->log("cAdd =$cAdd");


require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");