<?php
/**
 * User: German
 * Date: 19.10.2017
 * Time: 10:57
 */
@set_time_limit(0);
@ignore_user_abort(true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
if (\Bitrix\Main\Loader::includeModule('sale') === false)
    return;
$logger = Logger::getLogger("InvoiceCompanyUPD", "Invoice/iuc.log");
$loggerJSON = Logger::getLogger("InvoiceCompanyJSON", "Invoice/iucJSON.log");
$loggerNo = Logger::getLogger("InvoiceCompanyNo", "Invoice/iucNo.log");

$rsInvoice = CCrmInvoice::GetList(array('ID' => 'ASC'), array());
$arCompanyDel = array();
$arNewCompany = array();

$requisite = new \Bitrix\Crm\EntityRequisite();

$fieldsInfo = $requisite->getFormFieldsInfo();

$select = array_keys($fieldsInfo);

$CCrmInvoice = new CCrmInvoice();

while  ($arInvoice = $rsInvoice->Fetch()) {
    $company_id = $arInvoice['UF_COMPANY_ID'];
    if ($company_id > 0)
    {
        if (CCrmCompany::GetByID($company_id) === false)
        {
            if (isset($arNewCompany[$company_id]))
            {

            }
            else
            {
                $arProp = gk_GetProp($arInvoice['ID']);
                $arFilter = array(
//            '=ENTITY_TYPE_ID' => 4,//$entityTypeId,
//            '=ENTITY_ID' => 5477,//$entityId
                    '=RQ_INN' => $arProp['INN'],
                    '=RQ_KPP' => $arProp['KPP']
                );
                $logger->log("arFilter=[".print_r($arFilter, true)."]", FILE_APPEND);
                $rsRQ = $requisite->getList(
                    array(
                        'order' => array('SORT' => 'ASC', 'ID' => 'ASC'),
                        'filter' => $arFilter,
                        'select' => $select
                    )
                );

                if ($arRQ = $rsRQ->fetch()) {
                    $logger->log("arRQ=[".print_r($arRQ, true)."]", FILE_APPEND);
                    $arNewCompany[$company_id] = array('PROP' => $arProp,
                        'NEW_COMPANY' => array('ID' => $arRQ['ENTITY_ID'], 'NAME' => $arRQ['RQ_COMPANY_NAME']));
                }
                else
                {
                    $loggerNo->log("NO order_id =". $arInvoice['ID']." ".$arInvoice['ACCOUNT_NUMBER']." company_id=$company_id] ".print_r($arProp, true));
                }
            }
            $NewCompany = $arNewCompany[$company_id];
            $logger->log("order_id =". $arInvoice['ID']." ".$arInvoice['ACCOUNT_NUMBER']." company_id=$company_id]NewCompany=[".print_r($NewCompany, true)."]", FILE_APPEND);
            if($NewCompany['NEW_COMPANY']['ID'] > 0 )
            {
                $loggerJSON->log(json_encode(array('ORDER_ID' => $arInvoice['ID'], 'ACCOUNT_NUMBER' => $arInvoice['ACCOUNT_NUMBER'],
                    'COMPANY_ID' => $company_id, 'DATA' => $NewCompany)));
                $arFields = array('UF_COMPANY_ID' => $NewCompany['NEW_COMPANY']['ID']);
                $ret = $CCrmInvoice->Update($arInvoice['ID'], $arFields);
                $logger->log("ret=[".print_r($ret, true)."]");
//                break;
            }
//            if (isset($arCompanyDel[$company_id]))
//            {
//                $arCompanyDel[$company_id][] = array('ID' => $arInvoice['ID'],
//                    'ACCOUNT_NUMBER' => $arInvoice['ACCOUNT_NUMBER']);
//            }
//            else
//            {
//                $arCompanyDel[$company_id] = array(array('ID' => $arInvoice['ID'],
//                    'ACCOUNT_NUMBER' => $arInvoice['ACCOUNT_NUMBER']));
//            }
        }
    }
}
//$logger->log(print_r($arCompanyDel,true));
function gk_GetProp($order_id)
{
    $valueData = \Bitrix\Sale\Internals\OrderPropsValueTable::getList(
        array(
            "filter" => array("ORDER_ID" => array($order_id),
                'ORDER_PROPS_ID' => array(8, 9, 10))//$idInvoices)
        )
    );
    $arProp = array();
    while ($value = $valueData->fetch())
    {
        $arProp[$value['CODE']] = $value['VALUE'];
    }
    return $arProp;
}
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");