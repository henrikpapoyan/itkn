<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("");
IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/intranet/public/crm/contact/index.php");
$APPLICATION->SetTitle(GetMessage("CRM_TITLE"));
?><style>
.bx-filter-add-button {
    display:none;
}
.bx-filter-item-delete{
	display:none;
}
</style>
<?
echo "<div class='nottoshowelements' style='display:none'> ";
//$logger = Logger::getLogger('infoaboutsession','ofd.bitrix24/infoaboutsession.txt');
session_start();
//print_r($_SESSION["NOTSENDELEMENT"]);
$idelement="";
$uri = $APPLICATION->GetCurUri();
$idUri = explode("/", $uri);
$array2 = array("","0","crm","show", "contact", "edit", "index.php");
$result = array_diff($idUri,$array2);
foreach ($result as $value)
{
    $idelement=$value;
    $logger=$idelement;
}
//echo $idelement."<br>";
foreach ($_SESSION["NOTSENDELEMENT"]["INSERT"] as $key => $value)
{
  /*  if ($key == $idelement) {*/
        $id = "";
        $guidperson = "";
        $inncompany = "";
        $name = "";
        $mobilephone = "";
        $email = "";
        if (isset($value['ID'])) {
            $id = $value['ID'];
        }
        if (isset($value['GUID'])) {
            $guidperson = $value['GUID'];
        }
        if (isset($value['GUIDCOMPANY'])) {
            $guidcompany = $value['GUIDCOMPANY'];
        }
        if (isset($value['NAME'])) {
            $name = $value['NAME'];
        }
        if (isset($value['MOBILEPHONE'])) {
            $mobilephone = $value['MOBILEPHONE'];
        }

        if (isset($value['EMAIl'])) {
            $email = $value['EMAIl'];
        }
        //$logger->log(array("ID=".$id."//INN=".$inncompany."//NAME=".$name."//MOBILEPHONE=".$mobilephone."//EMAIl=".$email));
       // echo ("ID=".$id."//INN=".$inncompany."//NAME=".$name."//MOBILEPHONE=".$mobilephone."//EMAIl=".$email);
        ContactBpmBitrix::contactInsertToBMP($id, $guidperson, $guidcompany, $name, $mobilephone, $email);
    /*}*/
}
//echo "<br>";
foreach ($_SESSION["NOTSENDELEMENT"]["UPDATE"] as $key => $value)
{
   /* if ($key == $idelement) {*/
        $id = "";
        $guidcontact = "";
        $name = "";
        $mobilephone = "";
        $email = "";
        if (isset($value['ID'])) {
            $id = $value['ID'];
        }
        if (isset($value['GUID'])) {
            $guidcontact = $value['GUID'];
        }
        if (isset($value['NAME'])) {
            $name = $value['NAME'];
        }
        if (isset($value['MOBILEPHONE'])) {
            $mobilephone = $value['MOBILEPHONE'];
        }

        if (isset($value['EMAIl'])) {
            $email = $value['EMAIl'];
        }
        //echo ("ID=".$id."//GUID=".$guidcontact."//NAME=".$name."//MOBILEPHONE=".$mobilephone."//EMAIl=".$email);
        ContactBpmBitrix::contactUpdateToBMP($id,$guidcontact,$name,$mobilephone,$email);

   /* }*/
}
echo "</div>";
?>
<?$APPLICATION->IncludeComponent(
	"bitrix:crm.contact",
	"template2",
	Array(
		"ELEMENT_ID" => $_REQUEST["contact_id"],
		"NAME_TEMPLATE" => "",
		"PATH_TO_COMPANY_EDIT" => "/crm/company/edit/#company_id#/",
		"PATH_TO_COMPANY_SHOW" => "/crm/company/show/#company_id#/",
		"PATH_TO_DEAL_EDIT" => "/crm/deal/edit/#deal_id#/",
		"PATH_TO_DEAL_SHOW" => "/crm/deal/show/#deal_id#/",
		"PATH_TO_INVOICE_EDIT" => "/crm/invoice/edit/#invoice_id#/",
		"PATH_TO_INVOICE_SHOW" => "/crm/invoice/show/#invoice_id#/",
		"PATH_TO_LEAD_CONVERT" => "/crm/lead/convert/#lead_id#/",
		"PATH_TO_LEAD_EDIT" => "/crm/lead/edit/#lead_id#/",
		"PATH_TO_LEAD_SHOW" => "/crm/lead/show/#lead_id#/",
		"PATH_TO_USER_PROFILE" => "/company/personal/user/#user_id#/",
		"SEF_FOLDER" => "/crm/contact/",
		"SEF_MODE" => "Y",
		"SEF_URL_TEMPLATES" => Array("dedupe"=>"dedupe/","edit"=>"edit/#contact_id#/","export"=>"export/","import"=>"import/","index"=>"index.php","list"=>"list/","service"=>"service/","show"=>"show/#contact_id#/"),
		"VARIABLE_ALIASES" => array("index"=>"","list"=>"","edit"=>"","show"=>"","service"=>"","export"=>"","import"=>"","dedupe"=>"",)
	)
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>