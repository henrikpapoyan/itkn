<?php
/**
 * User: German
 * Date: 02.08.2017
 * Time: 9:35
 */
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
if (!CModule::IncludeModule('sale'))
{
    ShowError(GetMessage("F_NO_MODULE"));
    return 0;
}

if (!CModule::IncludeModule('crm'))
{
    ShowError(GetMessage("F_NO_MODULE"));
    return 0;
}

$logger = Logger::getLogger("OrderCheckPay", "Order/CheckPay.log");
$loggerInvoice = Logger::getLogger("OrderInvoice", "Order/Invoice.log");
$loggerPay = Logger::getLogger("OrderPay", "Order/Pay.log");
$params = array
(
    'select' => array
        (
            '0' => '*',
            'COMPANY_BY_NAME' => 'COMPANY_BY.NAME',
            'RESPONSIBLE_BY_NAME' => 'RESPONSIBLE_BY.NAME',
            'RESPONSIBLE_BY_LAST_NAME' => 'RESPONSIBLE_BY.LAST_NAME',
            'ORDER_ACCOUNT_NUMBER' => 'ORDER.ACCOUNT_NUMBER',
            'ORDER_USER_LOGIN' => 'ORDER.USER.LOGIN',
            'ORDER_USER_NAME' => 'ORDER.USER.NAME',
            'ORDER_USER_LAST_NAME' => 'ORDER.USER.LAST_NAME',
            'ORDER_USER_ID' => 'ORDER.USER_ID',
            'ORDER_RESPONSIBLE_ID' => 'ORDER.RESPONSIBLE_ID'
        ),

    'filter' => array(),

    'order' => array(
            'ORDER_ID' => 'DESC'
        ),

//    'runtime' => array(),
//
//    'limit' => 20,
//    'offset' => 0
);


$rsPay = \Bitrix\Sale\Internals\PaymentTable::getList($params);

$CCrmInvoice = new CCrmInvoice(false);
$Status = CCrmInvoice::GetStatusList();
//print_r($Status);
while ($arPay = $rsPay->fetch())
{
    $logger->log("arPay=[".print_r($arPay, true)."]");
    if (strlen($arPay['PAY_VOUCHER_NUM']) >0 )
    {
        $arInvoice = CCrmInvoice::GetByID($arPay['ORDER_ID']);
        if($arInvoice['STATUS_ID'] == 'S' )
        {
            $loggerInvoice->log("arInvoice=[".print_r($arInvoice, true)."]");
            $statusParams= array(
                'STATE_SUCCESS' => true,
                'STATE_FAILED' => false,
    //            [PAY_VOUCHER_NUM] => 222
    //            [PAY_VOUCHER_DATE] => 02.08.2017
    //            [DATE_MARKED] => 02.08.2017
    //            [REASON_MARKED] =>
            );
            $ret = $CCrmInvoice->SetStatus($arPay['ORDER_ID'], 'P', $statusParams);
            $loggerInvoice->log("ret=[".print_r($ret, true)."]");
        }
        elseif ($arInvoice['STATUS_ID'] != 'P' )
        {
            $loggerPay->log("ORDER_ID=[".print_r($arPay['ORDER_ID'], true)."]");
        }
    }

}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");