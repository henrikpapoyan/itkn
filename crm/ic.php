<?php
/**
 * User: German
 * Date: 25.08.2017
 * Time: 10:32
 */
@set_time_limit(0);
@ignore_user_abort(true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$fname = $_SERVER["DOCUMENT_ROOT"]."/log/Invoice/schet.csv";

$logger = Logger::getLogger("InvoiceIC", "Invoice/ic/ic.log");
$loggerNo = Logger::getLogger("InvoiceICNo", "Invoice/ic/icNo.log");
$loggerNoJSON = Logger::getLogger("InvoiceICNoJSON", "Invoice/ic/icNoJSON.log");
$loggerNoBitrix = Logger::getLogger("InvoiceICNoBitrix", "Invoice/ic/icNoBitrix.log");
$loggerNoBitrixJSON = Logger::getLogger("InvoiceICNoBitrixJSON", "Invoice/ic/icNoBitrixJSON.log");
$loggerNoBitrix2 = Logger::getLogger("InvoiceICNoBitrix2", "Invoice/ic/icNoBitrix2.log");
$loggerNo1C = Logger::getLogger("InvoiceICNo1C", "Invoice/ic/icNo1C.log");
$loggerSum = Logger::getLogger("InvoiceICSUM", "Invoice/ic/icSum.log");
$loggerSumJSON = Logger::getLogger("InvoiceICSUMJSON", "Invoice/ic/icSumJSON.log");
$loggerSumBitrix = Logger::getLogger("InvoiceICSUMBitrix", "Invoice/ic/icSumBitrix.log");
$loggerStatus = Logger::getLogger("InvoiceICStatus", "Invoice/ic/icStatus.log");
$loggerStatusAll = Logger::getLogger("InvoiceICStatusAll", "Invoice/ic/icStatusAll.log");
$loggerStatusBitrix = Logger::getLogger("InvoiceICStatusBitrix", "Invoice/ic/icStatusBitrix.log");
$loggerStatusBitrixID = Logger::getLogger("InvoiceICStatusBitrixID", "Invoice/ic/icStatusBitrixID.log");
$loggerStatus1C = Logger::getLogger("InvoiceICStatus1C", "Invoice/ic/icStatus1C.log");
$loggerDatePay = Logger::getLogger("InvoiceICDatePay", "Invoice/ic/icDatePay.log");
$loggerDatePay2 = Logger::getLogger("InvoiceICDatePay2", "Invoice/ic/icDatePay2.log");
$arFilter = array();
$rsInvoice = CCrmInvoice::GetList(array(), $arFilter);
$arInvoiceAll = array();
while($arInvoice = $rsInvoice->Fetch())
{
//    $logger->log("00020 [".print_r($arInvoice,true));

    $number1C = trim($arInvoice['ACCOUNT_NUMBER']);
    $year = substr($arInvoice['DATE_BILL'], 6, 4);
    if($year != 2017 )
    {
        $number1c = $arInvoice['ACCOUNT_NUMBER']." ($year)";
    }
    $arInvoiceAll[$number1C]['ACCOUNT_NUMBER'] = $number1C;
    $arInvoiceAll[$number1C]['PRICE'] = $arInvoice['PRICE'];
    $arInvoiceAll[$number1C]['ID'] = $arInvoice['ID'];
    $arInvoiceAll[$number1C]['STATUS_ID'] = $arInvoice['STATUS_ID'];
    $arInvoiceAll[$number1C]['PAY_VOUCHER_NUM'] = $arInvoice['PAY_VOUCHER_NUM'];
    $arInvoiceAll[$number1C]['PAY_VOUCHER_DATE'] = $arInvoice['PAY_VOUCHER_DATE'];
    $arInvoiceAll[$number1C]['UF_LINK'] = $arInvoice['UF_LINK'];

}

$logger->log(print_r($arInvoice,true));
$logger->log(print_r($arInvoiceAll,true));
$cNo = 0;
$cSum = 0;
$cNoBitrix = 0;
$cNoBitrix2 = 0;
$cNo1C = 0;
$cSumBitrix = 0;
$cStatus = 0;
$cStatusBitrix = 0;
$cStatus1C = 0;
$cDatePayAll = 0;
$cDatePay = 0;
$cDatePay2 = 0;
$arStatus = array(
    'A' => 'Не оплачен', //Ждет оплаты
    'P' => 'Оплачен',
    'N' => 'NULL', //Черновик
    'D' => 'Отменен',
    'S' => 'Не оплачен' //Отправлен клиенту
);
$bUpdate = false;
$bUpdate_P = true;
$bUpdate_D = true;
$bUpdate_A = true;
$bUpdate_A1 = true;
$bUpdate_D1 = true;
$bUpdate1C_P = true;
$bUpdate1C_A = true;
$bUpdate1C_D = true;
if (($handle = fopen($fname, "r")) !== FALSE) {
    $logger->log(print_r($data, true));
    $row = 0;
    $CCrmInvoice = new CCrmInvoice();
    while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
        $row++;
        $status = $data[11];
        if ($status == 'NULL')
        {
            $status = 'Не оплачен';
        }
        $id = $data[7];
        if ($row == 1)
        {
            continue;
        }
        $logger->log(print_r($data, true));
//        $arFilter = array('ACCOUNT' => $data);
//        $rsInvoice = CCrmInvoice::GetList(array(), $arFilter);
//        if($arInvoice = $rsInvoice->Fetch())
        $number1C = trim($data[0]);
        $uf_link = $data[14];
        $PPVN = $data[16];
        $PPVD = $data[17];
        $year = substr($data[1], 6, 4);
        $logger->log("year=[".print_r($year, true)."]");
        if($year != 2017)
        {
            $number1C = $data[0]." ($year)";
        }
        if(isset($arInvoiceAll[$number1C]))
        {
            $logger->log("arInvoice=[".print_r($arInvoiceAll[$number1C], true)."]");
            $price = $data[6];
            $price = str_replace(" ", "", $price);
            $price = str_replace(",", ".", $price);
            $logger->log($arInvoiceAll[$number1C]['PRICE']." =[".$price."]");
            if (floatval($arInvoiceAll[$number1C]['PRICE']) != floatval($price))
            {
                $cSum++;
                $loggerSum->log($arInvoiceAll[$number1C]['PRICE']." =[".$price."]");
                $loggerSum->log(print_r($data, true));
                if($data[7] > 0)
                {
                    $cSumBitrix ++;
                    $loggerSumBitrix->log($arInvoiceAll[$number1C]['PRICE']." =[".$price."]");
                    $loggerSumBitrix->log(print_r($data, true));
                }
                $loggerSumJSON->log(json_encode(array('data' => $data, 'invoice' => $arInvoiceAll[$number1C])));
            }
            $loggerStatusAll->log("$number1C $id".$arInvoiceAll[$number1C]['STATUS_ID'] ." = ".$status);
            if($arStatus[$arInvoiceAll[$number1C]['STATUS_ID']] != $status
//            && !($arInvoiceAll[$number1C]['STATUS_ID'] == 'N' && $status == "")
            && !($arInvoiceAll[$number1C]['STATUS_ID'] == 'A' && $status == "")
//            && !($arInvoiceAll[$number1C]['STATUS_ID'] == 'S' && $status == "")
            && !($arInvoiceAll[$number1C]['STATUS_ID'] == 'P' && $status == "Оплачен частично")
            && !($arInvoiceAll[$number1C]['STATUS_ID'] == 'Y' && $status == "Отменен")
            )
            {
                $loggerStatus->log($arInvoiceAll[$number1C]['STATUS_ID'] ." = ".$status);
                $loggerStatus->log(print_r($data, true));
                $cStatus++;
                if ($id > 0)
                {
                    $cStatusBitrix++;
                    $loggerStatusBitrix->log($arInvoiceAll[$number1C]['STATUS_ID'] ." = ".$status);
                    $loggerStatusBitrix->log(print_r($data, true));
                    $loggerStatusBitrix->log(print_r($arInvoiceAll[$number1C], true));
                    if ($arInvoiceAll[$number1C]['ID'] == $id)
                    {

                        if(($status == 'Оплачен' || $status == 'Оплачен частично')
                            && ($arInvoiceAll[$number1C]['STATUS_ID'] == 'A'
                                || $arInvoiceAll[$number1C]['STATUS_ID'] == 'S'
                                || $arInvoiceAll[$number1C]['STATUS_ID'] == 'N'
                            ))
                        {

                            $statusParams= array(
                                'STATE_SUCCESS' => true,
                                'STATE_FAILED' => false,
                                'PAY_VOUCHER_NUM' => $PPVN,
                                'PAY_VOUCHER_DATE' => $PPVD,
                                'DATE_MARKED' => $PPVD,
                                'REASON_MARKED' => ''
                            );
                            if($bUpdate_P)
                            {
                                $ret = $CCrmInvoice->SetStatus($id, 'P', $statusParams);
                                $loggerStatusBitrix->log("ret=[".print_r($ret, true)."]");
                            }
                        }
                        if($status == 'Отменен'
                            && ($arInvoiceAll[$number1C]['STATUS_ID'] == 'A'
                                || $arInvoiceAll[$number1C]['STATUS_ID'] == 'S'
                                || $arInvoiceAll[$number1C]['STATUS_ID'] == 'S'))
                        {

                            $statusParams= array(
                                'STATE_SUCCESS' => false,
                                'STATE_FAILED' => true,
                                //            [PAY_VOUCHER_NUM] => 222
                                //            [PAY_VOUCHER_DATE] => 02.08.2017
                                //            [DATE_MARKED] => 02.08.2017
                                //            [REASON_MARKED] =>
                            );
                            if($bUpdate_D) {
                                $ret = $CCrmInvoice->SetStatus($id, 'D', $statusParams);
                                $loggerStatusBitrix->log("ret3=[" . print_r($ret, true) . "]");
                            }
                        }
                        if(($status == 'Не оплачен' || $status == '')
                            && ($arInvoiceAll[$number1C]['STATUS_ID'] == 'N'
                            || $arInvoiceAll[$number1C]['STATUS_ID'] == 'S'
                                || $arInvoiceAll[$number1C]['STATUS_ID'] == 'P'
                            ))
                        {

                            $statusParams= array(
                                'STATE_SUCCESS' => false,
                                'STATE_FAILED' => false,
                            );
                            if($bUpdate_A) {

                                $ret = $CCrmInvoice->SetStatus($id, 'A', $statusParams);
                                $loggerStatusBitrix->log("ret6=[" . print_r($ret, true) . "]");
                            }
                        }


//                        if($status == 'Не оплачен'
//                            && ($arInvoiceAll[$number1C]['STATUS_ID'] == 'P' ))
//                        {
//
//                            $statusParams= array(
//                                'STATE_SUCCESS' => false,
//                                'STATE_FAILED' => false,
//                                //            [PAY_VOUCHER_NUM] => 222
//                                //            [PAY_VOUCHER_DATE] => 02.08.2017
//                                //            [DATE_MARKED] => 02.08.2017
//                                //            [REASON_MARKED] =>
//                            );
//                            $ret = $CCrmInvoice->SetStatus($id, 'A', $statusParams);
//                            $loggerStatusBitrix->log("ret5=[".print_r($ret, true)."]");
//                        }
                    }
                    else
                    {
                        $loggerStatusBitrix->log("id<>".$arInvoiceAll[$number1C]['ID'] ." = ".$id);
                        $loggerStatusBitrixID->log($arInvoiceAll[$number1C]['STATUS_ID'] ." = ".$status);
                        if(($status == 'Оплачен' || $status == 'Оплачен частично')
                            && ($arInvoiceAll[$number1C]['STATUS_ID'] == 'A'
                                || $arInvoiceAll[$number1C]['STATUS_ID'] == 'S'
                                || $arInvoiceAll[$number1C]['STATUS_ID'] == 'N'
                            ))
                        {

                            $statusParams= array(
                                'STATE_SUCCESS' => true,
                                'STATE_FAILED' => false,
                                'PAY_VOUCHER_NUM' => $PPVN,
                                'PAY_VOUCHER_DATE' => $PPVD,
                                'DATE_MARKED' => $PPVD,
                                'REASON_MARKED' => ''
                            );
                            if($bUpdate) {

                                $ret = $CCrmInvoice->SetStatus($arInvoiceAll[$number1C]['ID'], 'P', $statusParams);
                                $loggerStatusBitrix->log("ret7=[" . print_r($ret, true) . "]");
                            }
                        }

                        if(($status == 'Не оплачен' || $status == '')
                            && ($arInvoiceAll[$number1C]['STATUS_ID'] == 'N'
                                || $arInvoiceAll[$number1C]['STATUS_ID'] == 'S'
                                || $arInvoiceAll[$number1C]['STATUS_ID'] == 'P'
                            ))
                        {

                            $statusParams= array(
                                'STATE_SUCCESS' => false,
                                'STATE_FAILED' => false,
                            );
                            if($bUpdate_A1) {

                                $ret = $CCrmInvoice->SetStatus($arInvoiceAll[$number1C]['ID'], 'A', $statusParams);
                                $loggerStatusBitrixID->log("ret8=[" . print_r($ret, true) . "]");
                            }
                        }
                        if($status == 'Отменен'
                            && ($arInvoiceAll[$number1C]['STATUS_ID'] == 'A'
                                || $arInvoiceAll[$number1C]['STATUS_ID'] == 'S'
                                || $arInvoiceAll[$number1C]['STATUS_ID'] == 'P'))
                        {

                            $statusParams= array(
                                'STATE_SUCCESS' => false,
                                'STATE_FAILED' => true,
                                //            [PAY_VOUCHER_NUM] => 222
                                //            [PAY_VOUCHER_DATE] => 02.08.2017
                                //            [DATE_MARKED] => 02.08.2017
                                //            [REASON_MARKED] =>
                            );
                            if($bUpdate_D1) {
                                $ret = $CCrmInvoice->SetStatus($arInvoiceAll[$number1C]['ID'], 'D', $statusParams);
                                $loggerStatusBitrix->log("ret8=[" . print_r($ret, true) . "]");
                            }
                        }

                    }

                }
                else
                {
                    if($arInvoiceAll[$number1C]['UF_LINK'] == $uf_link)
                    {
                        $cStatus1C++;
                        $loggerStatus1C->log("00001".$arInvoiceAll[$number1C]['STATUS_ID'] ." = [".$status."]");
                        $loggerStatus1C->log("00001"."data=[".print_r($data, true)."]");
                        $loggerStatus1C->log("00001"."arInvoiceAll[$number1C]".print_r($arInvoiceAll[$number1C], true));
                        if(($status == 'Оплачен' || $status == 'Оплачен частично')
                            && ($arInvoiceAll[$number1C]['STATUS_ID'] == 'N'
                            || $arInvoiceAll[$number1C]['STATUS_ID'] == 'A'
                            || $arInvoiceAll[$number1C]['STATUS_ID'] == 'S'
                            || $arInvoiceAll[$number1C]['STATUS_ID'] == 'Y'
                            ))
                        {

                            $statusParams= array(
                                'STATE_SUCCESS' => true,
                                'STATE_FAILED' => false,
                                'PAY_VOUCHER_NUM' => $PPVN,
                                'PAY_VOUCHER_DATE' => $PPVD,
                                'DATE_MARKED' => $PPVD,
                                'REASON_MARKED' => ''
                            );
                            if($bUpdate1C_P)
                            {

                                $ret = $CCrmInvoice->SetStatus($arInvoiceAll[$number1C]['ID'], 'P', $statusParams);
                                $loggerStatus1C->log("ret2=[" . print_r($ret, true) . "]");
                            }
                        }
                        if($status == 'Отменен'
                            && ($arInvoiceAll[$number1C]['STATUS_ID'] == 'N'
                                || $arInvoiceAll[$number1C]['STATUS_ID'] == 'A'
                                || $arInvoiceAll[$number1C]['STATUS_ID'] == 'P'
                                )
                            )
                        {

                            $statusParams= array(
                                'STATE_SUCCESS' => false,
                                'STATE_FAILED' => true,
                                //            [PAY_VOUCHER_NUM] => 222
                                //            [PAY_VOUCHER_DATE] => 02.08.2017
                                //            [DATE_MARKED] => 02.08.2017
                                //            [REASON_MARKED] =>
                            );
                            if($bUpdate1C_D)
                            {

                                $ret = $CCrmInvoice->SetStatus($arInvoiceAll[$number1C]['ID'], 'D', $statusParams);
                                $loggerStatus1C->log("ret4=[" . print_r($ret, true) . "]");
                            }
                        }
                        if(($status == 'Не оплачен' || $status == '')
                            && (
                                $arInvoiceAll[$number1C]['STATUS_ID'] == 'N'
                                || $arInvoiceAll[$number1C]['STATUS_ID'] == 'P'
                                || $arInvoiceAll[$number1C]['STATUS_ID'] == 'D'
                                || $arInvoiceAll[$number1C]['STATUS_ID'] == 'Y'
                                )
                            )
                        {

                            $statusParams= array(
                                'STATE_SUCCESS' => false,
                                'STATE_FAILED' => false,
                                //            [PAY_VOUCHER_NUM] => 222
                                //            [PAY_VOUCHER_DATE] => 02.08.2017
                                //            [DATE_MARKED] => 02.08.2017
                                //            [REASON_MARKED] =>
                            );
                            if($bUpdate1C_A)
                            {

                                $ret = $CCrmInvoice->SetStatus($arInvoiceAll[$number1C]['ID'], 'A', $statusParams);
                                $loggerStatus1C->log("ret5=[" . print_r($ret, true) . "]");
                            }
                        }

                    }
                    else
                    {
                        $loggerStatus1C->log("00002".$arInvoiceAll[$number1C]['STATUS_ID'] ." = ".$status);
                        $loggerStatus1C->log("00002"."data=[".print_r($data, true)."]");
                        $loggerStatus1C->log("00002"."arInvoiceAll[$number1C]".print_r($arInvoiceAll[$number1C], true));
                    }
                }
            }
            else
            {
                if($status == 'Оплачен' || $status == 'Оплачен частично')
                {
                    $ppvNum = $data[16];
                    $ppvDate = $data[17];
                    if($arInvoiceAll[$number1C]['PAY_VOUCHER_DATE'] != $ppvDate) {
                        $cDatePayAll++;
                        if ($arInvoiceAll[$number1C]['PAY_VOUCHER_DATE'] == "" &&  !empty($ppvDate))
                        {
                            $cDatePay++;
                            $loggerDatePay->log("arInvoice=[".print_r($arInvoiceAll[$number1C], true)."]");
                            $loggerDatePay->log("ppvNum = $ppvNum ppvDate= $ppvDate");

                            $statusParams= array(
                                'STATE_SUCCESS' => true,
                                'STATE_FAILED' => false,
                                'PAY_VOUCHER_NUM' => $ppvNum,
                                'PAY_VOUCHER_DATE' => $ppvDate,
                                'DATE_MARKED' => $ppvDate,
                                'REASON_MARKED' => ''
                            );
                            $loggerDatePay->log("statusParams=[".print_r($statusParams, true)."]");
                            if($bUpdate) {

                                $ret = $CCrmInvoice->SetStatus($arInvoiceAll[$number1C]['ID'], 'P', $statusParams);
                                $loggerDatePay->log("ret=[" . print_r($ret, true) . "]");
                            }
                        }
                        else
                        {

                            $cDatePay2++;
                            $loggerDatePay2->log("arInvoice=[".print_r($arInvoiceAll[$number1C], true)."]");
                            $loggerDatePay2->log("ppvNum = $ppvNum ppvDate= $ppvDate");
                        }

                    }
                }
            }
        }
        else
        {
            $cNo++;
            $loggerNo->log(print_r($data, true));
            if($id >0)
            {
                if ($arInvoice = CCrmInvoice::GetByID($id))
                {
                    $loggerNo->log(print_r($arInvoice, true));
                }
                else
                {
                    $loggerNoBitrix->log(print_r($data, true));
                    $loggerNoBitrixJSON->log(json_encode($data));
                    $cNoBitrix ++;
                    if($data[11] != 'Отменен'
                        && $data[11] != 'NULL'
                    )
                    {
                        $loggerNoBitrix2->log(print_r($data, true));
                        $cNoBitrix2 ++;
                    }
                }
//                $loggerNoBitrix->log(print_r($arInvoice, true));
            }
            else
            {
                $loggerNo1C->log(print_r($data, true));
                $cNo1C ++;
                $loggerNoJSON->log(json_encode(array('data' => $data, 'invoice' => $arInvoiceAll[$number1C])));

            }
        }
//        if(!empty($data[0]))
//        {
//            $arINN[$data[0]]['data'] = $data;
//            $arFilter = array(
////            '=ENTITY_TYPE_ID' => 4,//$entityTypeId,
////            '=ENTITY_ID' => 5477,//$entityId
//                '=RQ_INN' => $data[0]
//            );
//            //file_put_contents(__FILE__.".log", "00030arFilter=[".print_r($arFilter, true)."]\n", FILE_APPEND);
//            $rsRQ = $requisite->getList(
//                array(
//                    'order' => array('SORT' => 'ASC', 'ID' => 'ASC'),
//                    'filter' => $arFilter,
//                    'select' => $select
//                )
//            );
//
//            if ($arRQ = $rsRQ->fetch()) {
//                $arINN[$data[0]]['company_id'] = $arRQ['ENTITY_ID'];
//                $arCompany[$arRQ['ENTITY_ID']] = array('company_id' => $arRQ['ENTITY_ID']);
//                $arFIO = explode(" ", $data[3]);
//                $arFilter = array('LAST_NAME' => $arFIO[0]);
//                $rsUser = CUser::GetList(($by="personal_country"), ($order="desc"), $arFilter);
//                if($arUser = $rsUser->Fetch())
//                {
//                    $logger->log($arFIO[0]." arUser=[".print_r($arUser, true)."]");
//                    $arCompany[$arRQ['ENTITY_ID']]['user_id'] = $arUser['ID'];
//                    $arCompany[$arRQ['ENTITY_ID']]['inn'] = $data[0];
//                }
//                else
//                {
//                    $arFilter = array('NAME' => $arFIO[0]);
//                    $rsUser = CUser::GetList(($by="personal_country"), ($order="desc"), $arFilter);
//                    if($arUser = $rsUser->Fetch())
//                    {
//                        $logger->log($arFIO[0]." 2arUser=[".print_r($arUser, true)."]");
//                        $arCompany[$arRQ['ENTITY_ID']]['user_id'] = $arUser['ID'];
//                        $arCompany[$arRQ['ENTITY_ID']]['inn'] = $data[0];
//                    }
//                    else
//                    {
//                        $loggerNoUser->log("NoUser=[".print_r($data, true)."]");
//                    }
//                }
//            }
//            else
//            {
//
//                $loggerNoRQ->log("NoRQ=[".print_r($data, true)."]");
//                $arRQ = array();
//            }
//        }
    }
    fclose($handle);
}
$logger->log("row=[".print_r($row,true)."]");
$logger->log("cNo=[".print_r($cNo,true)."]");
$logger->log("cNoBitrix=[".print_r($cNoBitrix,true)."]");
$logger->log("cNo1C=[".print_r($cNo1C,true)."]");
$logger->log("cNoBitrix2=[".print_r($cNoBitrix2,true)."]");
$logger->log("cSum=[".print_r($cSum,true)."]");
$logger->log("cSumBitrix=[".print_r($cSumBitrix,true)."]");
$logger->log("cStatus=[".print_r($cStatus,true)."]");
$logger->log("cStatusBitrix=[".print_r($cStatusBitrix,true)."]");
$logger->log("cStatus1C=[".print_r($cStatus1C,true)."]");
$logger->log("cDatePayAll=[".print_r($cDatePayAll,true)."]");
$logger->log("cDatePay=[".print_r($cDatePay,true)."]");
$logger->log("cDatePay2=[".print_r($cDatePay2,true)."]");
