<?php
/**
 * User: German
 * Date: 23.08.2017
 * Time: 15:25
 */

@set_time_limit(0);
@ignore_user_abort(true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$logger = Logger::getLogger("ContactDel", "Contact/Del.log");
$loggerTime = Logger::getLogger("ContactDelTime", "Contact/DelTime.log");
$loggerID = Logger::getLogger("ContactDelID", "Contact/DelID.log");

$arFilter = array('NAME' => '-', 'LAST_NAME' => '-');

$rsContact = CCrmContact::GetList(array('ID' => 'ASC'), $arFilter);

$c = 0;
$c1000 = 0;
$loggerTime->log($c);
$CCrmContact = new CCrmContact();
while($arContact = $rsContact->Fetch())
{
    $c++;
    $c1000++;
    $logger->log(json_encode($arContact));
    $ret = $CCrmContact->Delete($arContact[ID]);
    $loggerID->log($arContact['ID']." - ".$ret);
    if($c1000 == 1000)
    {
        $loggerTime->log($c);
        $c1000 = 0;
    }
//    if ($c == 1000)
//    {
//        break;
//    }

}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");