<?php
/**
 * User: German
 * Date: 10.04.2017
 * Time: 12:54
 */
@set_time_limit(3600);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$fname = $_SERVER["DOCUMENT_ROOT"]."/upload/adp.txt";
$handle = @fopen($fname, "r");
$IBLOCK_ID = 40;//43;
echo date("d F Y", mktime(0, 0, 0, 11, 11, 2016))."<br/>";
$sd = date("d F Y");
$t = strtotime($sd);
echo $t."<br/>";

if ($handle) {
    $c = 0;
    while (($buffer = fgets($handle, 4096)) !== false) {
        $c++;
        if ($c == 1 || $c == 2)
        {
            continue;
        }
        if ($c == 1000 )
        {
            break;
        }

//        echo $buffer;
        $arLine = explode("\t", $buffer);
//        file_put_contents(__FILE__.".log", "arLine=[".print_r($arLine, true)."]\n", FILE_APPEND);
//        arLine=[Array
//        (
//    [0] => 1
//    +[1] => 2308116386/141116      № договора
//    +[2] => «14» ноября 2016г.     Дата договора
//    +[3] => Общество с ограниченной ответственностью «Краснодарский комбинат по торговой технике»      Наименование контрагента
//    [4] => Директора      Должность подписанта
//    [5] => Помещика В.В.  Фамилия И.О. подписанта
//    [6] => Устава         Основание
//    [7] => Краснодарский край     Территория
//    +[8] => 350020, г. Краснодар, ул. Бабушкина, д. 248    Юр. Адрес
//    +[9] => 350020, г. Краснодар, ул. Бабушкина, д. 248    Факт. Адрес
//    +[10] => 8(861)215-62-30   Телефон
//    +[11] => 8(861)215-62-31   Факс
//    +[12] => kktt@bk.ru        Электронная почта
//    [13] => 1062308015505     ОГРН
//    +[14] => 2308116386        ИНН
//    [15] => 230801001         КПП
//    +[16] => 40702810230000040652      Р/с
//    +[17] => Краснодарское отделение № 8619 ПАО Сбербанк г.Краснодар   Наименование банка
//    +[18] => 30101810100000000602      К/с
//    +[19] => 040349602                 БИК
//    [20] =>                           Должность подписанта в реквизитах
//    [21] => Помещик В.В.              И.О. Фамилия подписанта
//    [22] =>                           N_СВ_О_РЕГ
//    [23] =>                           ДАТА_СВ_О_РЕГ
//    [24] =>                           N_ДОВ
//    +[25] =>                           Кол-во ККТ
//    +[26] =>                           Ответственный менеджер
//
//)
//]

        $NDogovor = $arLine[1];
        $line = $arLine[2];
        $line = str_replace("«", "", $line);
        $line = str_replace("»", "", $line);
        $line = str_replace("г.", "", $line);

        $line = str_replace("ноября", "November", $line);
        $line = str_replace("декабря", "December", $line);
        $line = str_replace("января", "January", $line);
        $line = str_replace("февраля", "February", $line);
        $line = str_replace("марта", "March", $line);
        $line = str_replace("апреля", "April", $line);


        file_put_contents(__FILE__.".log", "line=[".print_r($line, true)."]\n", FILE_APPEND);
        $t = strtotime($line);
        $DateDogovor = date('d.m.Y', $t);
        file_put_contents(__FILE__.".log", "DateDogovor=[".print_r($DateDogovor, true)."]\n", FILE_APPEND);
        $CompanyName = $arLine[3];

        $rsDogovor = CIBlockElement::GetList(array(),array("IBLOCK_ID" => $IBLOCK_ID, "XML_ID" => $NDogovor));
        $arInn = explode("/", $NDogovor);
        $email = $arLine[12];
        $email = str_replace(',ru"', '.ru' , $email);
        $email = str_replace('"', '' , $email);
        $email = str_replace(';', ',' , $email);
        $arEmail = explode(",", $email);
        $user_id = GetUser($arLine[26]);
        $arFields = array(
            'NN' => $arLine[0],
            'NDOGOVOG' => $NDogovor,
            'DATE' => $DateDogovor,
            'TITLE' => $CompanyName,
            'POSTSIGNER' => $arLine[4],
            'SIGNER' => $arLine[5],
            'BASE' => $arLine[6],
            'REGION' => $arLine[7],
            'ADDR_LEGAL' => $arLine[8],
            'ADDR_FAKT' => $arLine[9],
            'PHONE' => $arLine[10],
            'FAX' => $arLine[11],
            'EMAIL' => $arEmail,
            'OGRN' => $arLine[13], //$arInn[0],
            'INN' => $arLine[14], //$arInn[0],
            'KPP' => $arLine[15],
            'RSCHET' => $arLine[16],
            'BANK' => $arLine[17],
            'CSCHET' => $arLine[18],
            'BIK' => $arLine[19],
            'POSTSIGNER2' => $arLine[20],
            'SIGNER2' => $arLine[21],
            'NREG' => $arLine[22],
            'DATEREG' => $arLine[23],
            'NDOV' => $arLine[24],
            'KKT' => $arLine[25],
            'USER' => $arLine[26],
            'USER_ID' => $user_id,
            'TYPE' => 'AGENT',

        );
        if ($arDogovor = $rsDogovor->Fetch())
        {
            file_put_contents(__FILE__.".log", "YES=[".print_r($NDogovor, true)."]\n", FILE_APPEND);
        }
        else
        {
            file_put_contents(__FILE__.".log", "arLine=[".print_r($arLine, true)."]\n", FILE_APPEND);
            $info = '';
            $companyID = GetCompany($arFields, $info);
            $arFields['COMPANY_ID'] = $companyID;
            $PROP = array('_DOGOVORA' => $NDogovor,
                'DATA_DOGOVORA' => $DateDogovor,
//                'TIP_DOGOVORA' => 'Агентский', //456,
                'TIP_DOGOVORA_NEW' => 'Агентский', //456,
                'KONTRAGENT' => $companyID,
                'OTVETSTVENNYY_MENEDZHER' => $arFields['USER_ID'],
                'TIP_KONTRAGENTA' => 'Агент'
            );


            $CIBlockElement = new CIBlockElement();
            $arFields = array(
                "IBLOCK_ID" => $IBLOCK_ID,
                "XML_ID" => $NDogovor,
                'NAME' => 'Договор с '.$CompanyName,
                'created_date' => $DateDogovor,
                "PROPERTY_VALUES"=> $PROP,
                'DETAIL_TEXT' => json_encode($arFields)
            );
            $ret = $CIBlockElement->Add($arFields);
            file_put_contents(__FILE__.".log", "ret=[".print_r($ret, true)."]\n", FILE_APPEND);
            file_put_contents(__FILE__.".log", "arFields=[".print_r($arFields, true)."]\n", FILE_APPEND);
            file_put_contents(__FILE__.".log", "LAST_ERROR=[".print_r($CIBlockElement->LAST_ERROR, true)."]\n", FILE_APPEND);
            if($ret > 0)
            {
                $arFieldsDeal = array('TITLE' => 'Сделка '.$info['TITLE'],
                    'UF_DOGOVOR' => array($ret),
                    'COMPANY_ID' => $companyID,
                    'UF_KKT' => $arFields['KKT'],
                    'ASSIGNED_BY_ID' => $user_id,
                    'CATEGORY_ID' => 2,
                    'STAGE_ID' => 'C2:NEGOTIATION'
                );
                file_put_contents(__FILE__.".log", "166info=[".print_r($info, true)."]\n", FILE_APPEND);
                file_put_contents(__FILE__.".log", "166arFieldsDeal=[".print_r($arFieldsDeal, true)."]\n", FILE_APPEND);
                AddDeal($arFieldsDeal);
            }

        }
    }
    if (!feof($handle)) {
        echo "Error: unexpected fgets() fail\n";
    }
    fclose($handle);
}
function GetInfo($inn)
{
    $propertyTypeID = 'ITIN';
    $propertyValue = $inn;
    $countryID = 1;
    file_put_contents(__FILE__.".log", "propertyTypeID=[".print_r($propertyTypeID, true)."]\n", FILE_APPEND);
    file_put_contents(__FILE__.".log", "propertyValue=[".print_r($propertyValue, true)."]\n", FILE_APPEND);
    file_put_contents(__FILE__.".log", "countryID=[".print_r($countryID, true)."]\n", FILE_APPEND);

    $result = \Bitrix\Crm\Integration\ClientResolver::resolve(
        $propertyTypeID,
        $propertyValue,
        $countryID
    );
    return $result;
}
function GetCompany($data, &$info)
{
    file_put_contents(__FILE__.".log", "data=[".print_r($data, true)."]\n", FILE_APPEND);
    $requisite = new \Bitrix\Crm\EntityRequisite();

    $fieldsInfo = $requisite->getFormFieldsInfo();

    $select = array_keys($fieldsInfo);
    $arFilter = array(
//            '=ENTITY_TYPE_ID' => 4,//$entityTypeId,
//            '=ENTITY_ID' => 5477,//$entityId
            '=RQ_INN' => $data['INN']
    );
    file_put_contents(__FILE__.".log", "arFilter=[".print_r($arFilter, true)."]\n", FILE_APPEND);
    $res = $requisite->getList(
        array(
            'order' => array('SORT' => 'ASC', 'ID' => 'ASC'),
            'filter' => $arFilter,
            'select' => $select
        )
    );
    while ($row = $res->fetch())
    {
//    print_r($row);
        file_put_contents(__FILE__.".log", "row=[".print_r($row, true)."]\n", FILE_APPEND);
        return $row['ENTITY_ID'];
    }
    $rsCompany = CCrmCompany::GetList(array(), array('UF_CRM_INN' => $data['INN']));
    if($arCompany = $rsCompany->Fetch())
    {
        file_put_contents(__FILE__.".log", "arCompany=[".print_r($arCompany, true)."]\n", FILE_APPEND);
        return $arCompany['ID'];
    }
    $CCrmCompany = new CCrmCompany();

    $arFields = array(
        'TITLE' => $data['TITLE'],
        'UF_KKT' => $data['KKT'],
        'UF_CRM_INN' => $data['INN'],
        'COMPANY_TYPE' => 'PARTNER',
        'ASSIGNED_BY_ID' => $data['USER_ID'],
        'FM' => Array
        (
            'EMAIL' => Array
            (
//                'n1' => Array
//                (
//                    'VALUE' => $data['EMAIL'],
//                    'VALUE_TYPE' => 'WORK'
//                )

            ),

           'PHONE' => Array
            (
                'n1' => Array
                (
                    'VALUE' => $data['PHONE'],
                    'VALUE_TYPE' => 'WORK'
                ),
                'n2' => Array
                (
                    'VALUE' => $data['FAX'],
                    'VALUE_TYPE' => 'FAX'
                )
            )
        )

    );

    $arFields['COMMENTS'] = '';
    foreach ($data['EMAIL'] as $i => $email) {
        if (check_email($email))
        {
            $arFields['FM']['EMAIL']['n'.$i] = Array
            (
                'VALUE' => $email,
                'VALUE_TYPE' => 'WORK'
            );

        }
        else
        {
            file_put_contents(__FILE__.".log.email", "[".print_r($data['TITLE'], true)."]\n", FILE_APPEND);
            file_put_contents(__FILE__.".log.email", "email=[".print_r($email, true)."]\n", FILE_APPEND);
            $arFields['COMMENTS'] .= $email;
        }
    }

//    file_put_contents(__FILE__.".log", "arFields=[".print_r($arFields, true)."]\n", FILE_APPEND);
    $info_ = GetInfo($arFields['UF_CRM_INN']);
    $info = $info_[0];
//    <select class="bx-user-field-enum" name="UF_CRM_1486035185">
//	<option value="" selected="">нет</option>
//    <option value="63">ООО</option>
//    <option value="64">ОАО</option>
//    <option value="65">АО</option>
//    <option value="92">ПАО</option>
//    <option value="66">ЗАО</option>
//    <option value="67">ИП</option>
//    <option value="68">ФГУП</option>
//    <option value="69">Казенное предприятие</option
//    <option value="70">Гос учреждение</option>
//    <option value="97">Частное лицо</option></select>
    $org_forma = '';
    $city = '';
    if (strlen($arFields['UF_CRM_INN']) == 10)
    {
        $info['fields']['NAME'] = 'Организация';
        $info['fields']['PRESET_ID'] = 1;
        $arFields['TITLE'] = $info['fields']['RQ_COMPANY_NAME'];
        $info['TITLE'] = $info['fields']['RQ_COMPANY_NAME'];
        $arFields['UF_FULL_NAME'] = $info['fields']['RQ_COMPANY_FULL_NAME'];
        $city = $info['fields']['RQ_ADDR'][6]['CITY'];
        if (substr($info['fields']['RQ_COMPANY_NAME'], 0, 3) == "ООО")
        {
            $org_forma = 63;
        }
        elseif (substr($info['fields']['RQ_COMPANY_NAME'], 0, 3) == "ЗАО")
        {
            $org_forma = 66;
        }
        else
        {
            file_put_contents(__FILE__.".name", "NAME=[".print_r($info['fields']['RQ_COMPANY_NAME'], true)."]\n", FILE_APPEND);
            file_put_contents(__FILE__.".name", "FN=[".print_r($info['fields']['RQ_COMPANY_FULL_NAME'], true)."]\n", FILE_APPEND);
        }
    }
    elseif (strlen($arFields['UF_CRM_INN']) == 12)
    {
        $info['fields']['NAME'] = 'ИП';
        $info['fields']['PRESET_ID'] = 2;
        $arFields['TITLE'] ='ИП '.$info['fields']['RQ_LAST_NAME'].' '.$info['fields']['RQ_FIRST_NAME'].' '.$info['fields']['RQ_SECOND_NAME'];
        $info['TITLE'] ='ИП '.$info['fields']['RQ_LAST_NAME'].' '.$info['fields']['RQ_FIRST_NAME'].' '.$info['fields']['RQ_SECOND_NAME'];
        $arFields['UF_FULL_NAME'] ='ИП '.$info['fields']['RQ_LAST_NAME'].' '.$info['fields']['RQ_FIRST_NAME'].' '.$info['fields']['RQ_SECOND_NAME'];
        $org_forma = 67;
    }
    $arFields['UF_CRM_1486035185'] = $org_forma;
    $arFields['UF_CRM_1492416196'] = $city;
    $info['fields']['SORT'] = 500;
    file_put_contents(__FILE__.".log", "info=[".print_r($info, true)."]\n", FILE_APPEND);

    $id = $CCrmCompany->Add($arFields);
    file_put_contents(__FILE__.".log", "id=[".print_r($id, true)."]\n", FILE_APPEND);
    file_put_contents(__FILE__.".log", "arFields=[".print_r($arFields, true)."]\n", FILE_APPEND);

    $info['fields']['ENTITY_TYPE_ID'] = 4;
    $info['fields']['ENTITY_ID'] = $id;
    $requisite_id = AddRQ($info);
    $bankInfo = array(
        'ENTITY_TYPE_ID' => 8,
        'ENTITY_ID' => $requisite_id,
        'COUNTRY_ID' => 1,
        'NAME' => 'Банковские реквизиты 1',
        'RQ_BANK_NAME' => $data['BANK'],
        'RQ_BANK_ADDR' => '',
        'RQ_BIK' => $data['BIK'],
        'RQ_ACC_NUM' => $data['RSCHET'],
        'RQ_ACC_CURRENCY' => '',
        'RQ_COR_ACC_NUM' => $data['CSCHET'],
        'RQ_SWIFT' => '',
        'COMMENTS' => ''
    );

    AddBank($bankInfo);
//)
    return $id;
//    [1] => 8602271355/141116
//    [2] => «14» ноября 2016г.
//    [3] => ООО «Алгоритм Север»
//    [4] => 628403, ХМАО-Югра, г. Сургут, ул. 30 лет Победы, д. 19     Юр. Адрес
//    [5] => 628403, ХМАО-Югра, г. Сургут, ул. 30 лет Победы, д. 19     Факт. Адрес
//    [6] => 8 (3462) 20-68-20                                          Телефон
//    [7] => 8 (3462) 20-68-20                                          Факс
//    [8] => surgut@algoritm-t.ru                                       Электронная почта
//    [9] =>                                                            Кол-во ККТ
//    [10] => Смоляков                                                  Ответственный менеджер
//    [11] =>                                                           Система налогообложения
//    [12] =>                                                           ID партнера
//    [13] => 23.12.2016 № 10944306011292                              Отправлено  (дата, номер для отслеживания отправления (при наличии)
//    [14] =>                                                           Получен экземпляр Первого ОФД (дата)
//    [15] =>                                                           Используемая система налогообложения
}
function GetUser($name)
{
    $arName = explode(' ', $name);
    $filter = Array
    (
        "NAME" => $arName[0],
    );
    $rsUsers = CUser::GetList(($by="personal_country"), ($order="desc"), $filter);
    if($arUser = $rsUsers->Fetch())
    {
        file_put_contents(__FILE__.".log", "arUser=[".print_r($arUser['ID'], true)."]\n", FILE_APPEND);
        file_put_contents(__FILE__.".log", "arUser=[".print_r($arUser['LAST_NAME'], true)."]\n", FILE_APPEND);
        return $arUser['ID'];
    }
    return 1;
}
function AddRQ($info)
{
//    info=[Array
//    (
//        [0] => Array
//        (
//            [caption] => ООО "КРАСНОДАРСКИЙ КОМБИНАТ ПО ТОРГОВОЙ ТЕХНИКЕ"
//        [fields] => Array
//        (
//            [RQ_INN] => 2308116386
//            [RQ_KPP] => 230801001
//            [RQ_OGRN] => 1062308015505
//            [RQ_OKVED] => 33.12
//            [RQ_COMPANY_NAME] => ООО "КРАСНОДАРСКИЙ КОМБИНАТ ПО ТОРГОВОЙ ТЕХНИКЕ"
//            [RQ_COMPANY_FULL_NAME] => ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ "КРАСНОДАРСКИЙ КОМБИНАТ ПО ТОРГОВОЙ ТЕХНИКЕ"
//            [RQ_IFNS] => ИНСПЕКЦИЯ ФЕДЕРАЛЬНОЙ НАЛОГОВОЙ СЛУЖБЫ №1 ПО Г. КРАСНОДАРУ
//            [RQ_COMPANY_REG_DATE] => 03.04.2006
//            [RQ_ADDR] => Array
//        (
//            [6] => Array
//            (
//                [ADDRESS_1] => УЛ. ИМ БАБУШКИНА, Д. 248
//                                            [ADDRESS_2] =>
//                                            [CITY] => КРАСНОДАР
//                                            [REGION] =>
//                                            [PROVINCE] => КРАСНОДАРСКИЙ КРАЙ
//                                            [POSTAL_CODE] => 350020
//                                            [COUNTRY] => Россия
//                                        )
//
//                                )
//
//                            [RQ_DIRECTOR] => ГРИБОВСКИЙ ЮРИЙ ИВАНОВИЧ
//                        )
//
//                )
//
//        )

//    requisiteFields=[Array
//    (
//        [NAME] => Организация
//    [PRESET_ID] => 1
//    [SORT] => 500
//    [RQ_ADDR] => Array
//(
//    [6] => Array
//    (
//        [ADDRESS_1] => КОЖЕВНИЧЕСКИЙ 1-Й, ДОМ 6, СТРОЕНИЕ 1
//                    [ADDRESS_2] => ПОМЕЩЕНИЕ 14А
//                    [CITY] => МОСКВА
//                    [REGION] =>
//                    [PROVINCE] =>
//                    [POSTAL_CODE] => 115114
//                    [COUNTRY] => Россия
//                    [COUNTRY_CODE] =>
//                )
//
//        )
//
//    [RQ_COMPANY_NAME] => АО "ЭСК"
//[RQ_COMPANY_FULL_NAME] => АКЦИОНЕРНОЕ ОБЩЕСТВО "ЭНЕРГЕТИЧЕСКИЕ СИСТЕМЫ И КОММУНИКАЦИИ"
//[RQ_COMPANY_REG_DATE] => 23.01.2002
//    [RQ_DIRECTOR] => ЕМЕЛИН АЛЕКСАНДР СЕРГЕЕВИЧ
//    [RQ_ACCOUNTANT] =>
//    [RQ_INN] => 7709364346
//    [RQ_KPP] => 772501001
//    [RQ_OGRN] => 1037739495265
//    [RQ_OKPO] =>
//    [RQ_OKTMO] =>
//    [ENTITY_TYPE_ID] => 4
//    [ENTITY_ID] => 5942
//)
    $requisite = new \Bitrix\Crm\EntityRequisite();
    $requisiteFields = $info['fields'];
    file_put_contents(__FILE__.".log", "requisiteFields=[".print_r($requisiteFields, true)."]\n", FILE_APPEND);
    $result = $requisite->add($requisiteFields);
//    file_put_contents(__FILE__.".log", "result=[".print_r($result, true)."]\n", FILE_APPEND);
    return $result->getId();
}
function AddBank($bankDetailFields)
{
    $bankDetail = new \Bitrix\Crm\EntityBankDetail();
    file_put_contents(__FILE__.".log", "bankDetailFields=[".print_r($bankDetailFields, true)."]\n", FILE_APPEND);


    $bankDetailResult = $bankDetail->add($bankDetailFields);

//    file_put_contents(__FILE__.".log", "bankDetailResult=[".print_r($bankDetailResult, true)."]\n", FILE_APPEND);
}
function AddDeal($arFieldsDeal)
{
    $CCrmDeal = new CCrmDeal();
    $ret = $CCrmDeal->Add($arFieldsDeal);
    file_put_contents(__FILE__.".log", "retDeal=[".print_r($ret, true)."]\n", FILE_APPEND);
    file_put_contents(__FILE__.".log", "arFieldsDeal=[".print_r($arFieldsDeal, true)."]\n", FILE_APPEND);

}
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");





