<?php
/**
 * User: German
 * Date: 27.04.2017
 * Time: 11:05
 */
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$inn ="344210316";
$rq = GetInfo($inn);
file_put_contents(__FILE__.".log", "rq=[".print_r($rq, true)."]\n", FILE_APPEND);



function GetInfo($inn)
{
    $propertyTypeID = 'ITIN';
    $propertyValue = $inn;
    $countryID = 1;

    $result = \Bitrix\Crm\Integration\ClientResolver::resolve(
        $propertyTypeID,
        $propertyValue,
        $countryID
    );
    return $result;
}


require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
