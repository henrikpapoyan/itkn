<?php
/**
 * User: German
 * Date: 23.08.2017
 * Time: 19:12
 */
@set_time_limit(0);
@ignore_user_abort(true);

use Bitrix\Crm\Integrity;
use Bitrix\Crm\Merger;

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$logger = Logger::getLogger("CompanyMergeAll", "Company/MergeAll.log");
$loggerMerge = Logger::getLogger("CompanyMerge", "Company/Merge.log");
$loggerDel = Logger::getLogger("CompanyMergeDel", "Company/MergeDel.log");
$loggerTime = Logger::getLogger("CompanyMergeTime", "Company/MergeTime.log");
$c100 = 0;
$row = 0;
$loggerTime->log($row);
$fname = $_SERVER["DOCUMENT_ROOT"]."/log/Dub/DubNameR2.txt";
if (($handle = fopen($fname, "r")) !== FALSE) {
    $logger->log(print_r($data, true));
    $company_id = 0;
    $name = '';
    $old_data = array();
    while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
        $logger->log(print_r($data, true));
        $num = count($data);
        if ($company_id == 0)
        {
            $company_id = $data[0];
            $name = $data[1];
            $old_data = $data;
        }
        else
        {
            if($name == $data[1] )
            {
                $loggerMerge->log($name."[$company_id] = ".$data[1]."[".$data[0]."]");
                $rsCompany = CCrmCompany::GetList(array(), array('ID' => $company_id));
                if($arCompanyA = $rsCompany->Fetch())
                {
                    $loggerMerge->log("arCompanyA=[".print_r($arCompanyA, true)."]");
                    $rsCompany = CCrmCompany::GetList(array(), array('ID' => $data[0]));
                    if($arCompanyB = $rsCompany->Fetch())
                    {
                        $loggerMerge->log("arCompanyB=[".print_r($arCompanyB, true)."]");
                        $loggerDel->log(json_encode(array('targ'=> $data[0],'seed' => $arCompanyA)));
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        $seedEntityID = $company_id;
                        $targEntityID = $data[0];
                        $entityTypeName = "COMPANY";
                        $entityTypeID = CCrmOwnerType::ResolveID($entityTypeName);
                        $currentUser = CCrmSecurityHelper::GetCurrentUser();
                        $currentUserID = (int)$currentUser->GetID();

                        $index_type_name = 'ORGANIZATION';

                        $typeID = Integrity\DuplicateIndexType::resolveID($index_type_name);
                        //    $matches = isset($_POST['INDEX_MATCHES']) && is_array($_POST['INDEX_MATCHES']) ? $_POST['INDEX_MATCHES'] : array();
                        $matches = array();
                        $criterion = Integrity\DuplicateManager::createCriterion($typeID, $matches);

                        $enablePermissionCheck = !CCrmPerms::IsAdmin($currentUserID);
                        $merger = Merger\EntityMerger::create($entityTypeID, $currentUserID, $enablePermissionCheck);
                        try {

                            $merger->merge($seedEntityID, $targEntityID, $criterion);
                        } catch (Merger\EntityMergerException $e) {
                            $logger->log(__CrmDedupeListErrorText($e));
                        } catch (Exception $e) {
                            $logger->log($e->getMessage());
                        }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    }
                }
            }
            $company_id = 0;
        }
        $row++;
//        if ($row == 2)
//        {
//            break;
//        }
        if ($c100 == 100)
        {
            $loggerTime->log($row);
            $c100 = 0;
        }
    }
    fclose($handle);
}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");