<?php
/**
 * User: German
 * Date: 19.10.2017
 * Time: 10:57
 */
@set_time_limit(0);
@ignore_user_abort(true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$logger = Logger::getLogger("InvoiceCompanyDel", "Invoice/CompanyDel.log");

$rsInvoice = CCrmInvoice::GetList(array('ID' => 'ASC'), array());
$arCompanyDel = array();
while  ($arInvoice = $rsInvoice->Fetch()) {
    $company_id = $arInvoice['UF_COMPANY_ID'];
    if ($company_id > 0)
    {
        if (CCrmCompany::GetByID($company_id) === false)
        {
            if (isset($arCompanyDel[$company_id]))
            {
                $arCompanyDel[$company_id][] = array('ID' => $arInvoice['ID'],
                    'ACCOUNT_NUMBER' => $arInvoice['ACCOUNT_NUMBER']);
            }
            else
            {
                $arCompanyDel[$company_id] = array(array('ID' => $arInvoice['ID'],
                    'ACCOUNT_NUMBER' => $arInvoice['ACCOUNT_NUMBER']));
            }
        }
    }
}
$logger->log(print_r($arCompanyDel,true));
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");