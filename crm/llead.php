<?php
/**
 * User: German
 * Date: 13.11.2017
 * Time: 9:12
 */
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$logger = Logger::getLogger("llead", "Lead/llead.log");
$c = 0;
$arFilter= array();
$arFilter['SOURCE_ID'] = gk_GetOrderFilter();
$rsLead = CCrmLead::GetList(array('ID' => 'DESC'),$arFilter);
while ($arLead = $rsLead->Fetch())
{
    $c++;
    $logger->log("arLead=[".print_r($arLead, true)."]");
    if ($c == 100)
    {
        break;
    }
}
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");