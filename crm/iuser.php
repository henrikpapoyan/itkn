<?php
/**
 * User: German
 * Date: 12.10.2017
 * Time: 14:39
 */
@set_time_limit(0);
@ignore_user_abort(true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$logger = Logger::getLogger("InvoiceUser", "Invoice/InvoiceUser2.log");
$loggerChange = Logger::getLogger("InvoiceUserChange", "Invoice/InvoiceUser2Change.log");
$loggerStat = Logger::getLogger("InvoiceUserStat", "Invoice/InvoiceUser2Stat.log");
$rsInvoice = CCrmInvoice::GetList(array(),array("RESPONSIBLE_ID" => 372));
$CCrmInvoice = new CCrmInvoice();
$all = 0;
$upd = 0;
while($arInvoice = $rsInvoice->Fetch())
{
    $all++;
    $logger->log("arInvoice=[".print_r($arInvoice, true)."]");
    $company_id = $arInvoice['UF_COMPANY_ID'];
    if($company_id > 0)
    {
        $arCompany = CCrmCompany::GetByID($company_id);
        $logger->log("arCompany=[".print_r($arCompany, true)."]");
        if($arCompany['ASSIGNED_BY'] > 0 && $arCompany['ASSIGNED_BY'] != 372)
        {
            $arFields = array("RESPONSIBLE_ID" => $arCompany['ASSIGNED_BY']);
            $loggerChange->log("ID=[".$arInvoice['ID']."]"." COMPANY_ID=[".$arCompany['ID']."]"." USER=[".$arCompany['ASSIGNED_BY']."]");
            $ret = $CCrmInvoice->Update($arInvoice['ID'], $arFields);
            $loggerChange->log("ret=[".print_r($ret, true)."]");
            $upd++;
        }
    }

//    break;
}
$loggerStat->log("all ".$all);
$loggerStat->log("upd ".$upd);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
