<?php
/**
 * User: German
 * Date: 16.11.2017
 * Time: 10:06
 */
@set_time_limit(0);
@ignore_user_abort(true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
if (\Bitrix\Main\Loader::includeModule('sale') === false)
    return;
$logger = Logger::getLogger("InvoiceCompanyUPD", "Invoice/inc.log");
$logger0 = Logger::getLogger("InvoiceCompany0", "Invoice/inc0.log");
$loggerNo = Logger::getLogger("InvoiceCompanyNo", "Invoice/incNo.log");

$rsStatus = CSaleStatus::GetList(array(),array('LID' => 'ru'));
$arStatusName = array();
while($arStatus = $rsStatus->Fetch())
{
//    $logger->log("arStatus=[".print_r($arStatus,true)."]");
    $arStatusName[$arStatus['ID']] = $arStatus['NAME'];
}
$logger->log("arStatusName=[".print_r($arStatusName,true)."]");

$rsInvoice = CCrmInvoice::GetList(array('ID' => 'ASC'), array());
//STATUS_ID
while  ($arInvoice = $rsInvoice->Fetch()) {
    $company_id = $arInvoice['UF_COMPANY_ID'];
    if ($company_id > 0)
    {
        if (CCrmCompany::GetByID($company_id) === false)
        {
            $loggerNo->log(";". $arInvoice['ID'].";".$arInvoice['ACCOUNT_NUMBER'].";".$arInvoice['STATUS_ID'].
                ";".$arStatusName[$arInvoice['STATUS_ID']].";$company_id");
        }

    }
    else
    {
        $logger0->log(";". $arInvoice['ID'].";".$arInvoice['ACCOUNT_NUMBER'].";".$arInvoice['STATUS_ID'].
            ";".$arStatusName[$arInvoice['STATUS_ID']].";$company_id");
    }
}
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");