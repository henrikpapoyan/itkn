<?php
/**
 * User: German
 * Date: 08.08.2017
 * Time: 22:45
 */
@set_time_limit(0);
@ignore_user_abort(true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$loggerID = Logger::getLogger("AgentMessageActivityID", "AgentMessage/activityID.log");
$loggerTime = Logger::getLogger("AgentMessageActivityTime", "AgentMessage/activityTime.log");
$activiti_id = file_get_contents(__DIR__."/activity_id");
if (empty($activiti_id))
{
    $activiti_id = 0;
}
//file_put_contents(__FILE__.".d.log", "activiti_id=[".print_r($activiti_id, true)."]\n", FILE_APPEND);
$arFilter = array(
    'TYPE_ID' => \CCrmActivityType::Email,
    'COMPLETED' => 'N',
    '>ID' => $activiti_id);
$rsActivity = CCrmActivity::GetList(array('ID' => 'ASC'), $arFilter);
$c = 0;
//file_put_contents(__FILE__.".d.log", "__DIR__=[".print_r(__DIR__, true)."]\n", FILE_APPEND);
//file_put_contents(__FILE__.".t.log", date("Y-m-d H:i:s")."\n", FILE_APPEND);
$all = 0;
$loggerTime->log("begin");
while($arActivity = $rsActivity->Fetch() )
{
    $loggerID->log($arActivity['OWNER_TYPE_ID']." ".$arActivity['ID']." ".$arActivity['OWNER_ID']);
//    file_put_contents(__FILE__.".log", "arActivity=[".print_r($arActivity, true)."]\n", FILE_APPEND);
//    file_put_contents(__FILE__.".s.log", "SUBJECT=[".print_r($arActivity['SUBJECT'], true)."]\n", FILE_APPEND);

    file_put_contents(__DIR__."/activity_id", $arActivity['ID']);
    CCrmActivity::Complete($arActivity['ID'], false, array('REGISTER_SONET_EVENT' => true));
    $c++;
    $all++;
    if($c == 100)
    {
        $loggerTime->log($all);
        $c = 0;
//        break;
    }
}
$loggerTime->log("end ".$all);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");