<?php
/**
 * User: German
 * Date: 24.10.2017
 * Time: 15:39
 */

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$IBLOCK_ID = GetIBlockIDByCode('brands');

$logger = Logger::getLogger("CompanyBrend", "Company/Brend.log");

$fname = $_SERVER["DOCUMENT_ROOT"]."/upload/brend.txt";
$handle = @fopen($fname, "r");
if ($handle) {

    $c = 0;

    $requisite = new \Bitrix\Crm\EntityRequisite();

    $fieldsInfo = $requisite->getFormFieldsInfo();

    $select = array_keys($fieldsInfo);

    $CCrmCompany = new CCrmCompany();

    while (($buffer = fgets($handle, 4096)) !== false) {
        $c++;
//        if ($c == 1) {
//            continue;
//        }
        $arLine = explode("\t", $buffer);
        $logger->Log("arLine=[".print_r($arLine, true)."]");
        $arFilter = array(
//            '=ENTITY_TYPE_ID' => 4,//$entityTypeId,
//            '=ENTITY_ID' => $arCompany['ID'],//$entityId
            '=RQ_INN' => $arLine[0]
        );
        $rsRQ = $requisite->getList(
            array(
                'order' => array('SORT' => 'ASC', 'ID' => 'ASC'),
                'filter' => $arFilter,
                'select' => $select
            )
        );
        if ($arRQ = $rsRQ->fetch()) {
            $logger->log("arRQ=[".print_r($arRQ, true)."]");
            $company_id = $arRQ['ENTITY_ID'];
            $rsCompany = CCrmCompany::GetList(array(), array('ID' => $company_id));
            $arCompany = $rsCompany->Fetch();
            $logger->log("arCompany=[".print_r($arCompany, true)."]");
            if ($company_id > 0)
            {
                $arFields = array('UF_BRAND' => $arLine[2]);
                $ret = $CCrmCompany->Update($company_id, $arFields);
                $logger->log("ret=[".print_r($ret, true)."]");
            }
        }
        if ($c == 2) {
//            break;
        }

//        echo $buffer;
    }
}
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");