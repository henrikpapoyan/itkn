<?php
/**
 * User: German
 * Date: 16.10.2017
 * Time: 10:05
 */
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Обращения");
$APPLICATION->IncludeComponent(
	'bitrix:crm.control_panel',
	'',
	array(
		'ID' => 'LEAD_LIST',
		'ACTIVE_ITEM_ID' => 'LEAD',
		'PATH_TO_COMPANY_LIST' => isset($arResult['PATH_TO_COMPANY_LIST']) ? $arResult['PATH_TO_COMPANY_LIST'] : '',
		'PATH_TO_COMPANY_EDIT' => isset($arResult['PATH_TO_COMPANY_EDIT']) ? $arResult['PATH_TO_COMPANY_EDIT'] : '',
		'PATH_TO_CONTACT_LIST' => isset($arResult['PATH_TO_CONTACT_LIST']) ? $arResult['PATH_TO_CONTACT_LIST'] : '',
		'PATH_TO_CONTACT_EDIT' => isset($arResult['PATH_TO_CONTACT_EDIT']) ? $arResult['PATH_TO_CONTACT_EDIT'] : '',
		'PATH_TO_DEAL_LIST' => isset($arResult['PATH_TO_DEAL_LIST']) ? $arResult['PATH_TO_DEAL_LIST'] : '',
		'PATH_TO_DEAL_EDIT' => isset($arResult['PATH_TO_DEAL_EDIT']) ? $arResult['PATH_TO_DEAL_EDIT'] : '',
		'PATH_TO_LEAD_LIST' => isset($arResult['PATH_TO_LEAD_LIST']) ? $arResult['PATH_TO_LEAD_LIST'] : '',
		'PATH_TO_LEAD_EDIT' => isset($arResult['PATH_TO_LEAD_EDIT']) ? $arResult['PATH_TO_LEAD_EDIT'] : '',
		'PATH_TO_QUOTE_LIST' => isset($arResult['PATH_TO_QUOTE_LIST']) ? $arResult['PATH_TO_QUOTE_LIST'] : '',
		'PATH_TO_QUOTE_EDIT' => isset($arResult['PATH_TO_QUOTE_EDIT']) ? $arResult['PATH_TO_QUOTE_EDIT'] : '',
		'PATH_TO_INVOICE_LIST' => isset($arResult['PATH_TO_INVOICE_LIST']) ? $arResult['PATH_TO_INVOICE_LIST'] : '',
		'PATH_TO_INVOICE_EDIT' => isset($arResult['PATH_TO_INVOICE_EDIT']) ? $arResult['PATH_TO_INVOICE_EDIT'] : '',
		'PATH_TO_REPORT_LIST' => isset($arResult['PATH_TO_REPORT_LIST']) ? $arResult['PATH_TO_REPORT_LIST'] : '',
		'PATH_TO_DEAL_FUNNEL' => isset($arResult['PATH_TO_DEAL_FUNNEL']) ? $arResult['PATH_TO_DEAL_FUNNEL'] : '',
		'PATH_TO_EVENT_LIST' => isset($arResult['PATH_TO_EVENT_LIST']) ? $arResult['PATH_TO_EVENT_LIST'] : '',
		'PATH_TO_PRODUCT_LIST' => isset($arResult['PATH_TO_PRODUCT_LIST']) ? $arResult['PATH_TO_PRODUCT_LIST'] : ''
	),
	$component
);

$APPLICATION->IncludeComponent(
    "bitrix:lists",
    ".default",
    array(
        "IBLOCK_TYPE_ID" => "lists_socnet",
        "SEF_MODE" => "Y",
        "SEF_FOLDER" => "/crm/bpm/",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "36000000",
        "COMPONENT_TEMPLATE" => ".default",
        "COMPOSITE_FRAME_MODE" => "A",
        "COMPOSITE_FRAME_TYPE" => "AUTO",
        "SEF_URL_TEMPLATES" => array(
            "lists" => "",
            "list" => "#list_id#/view/#section_id#/",
            "list_sections" => "#list_id#/edit/#section_id#/",
            "list_edit" => "#list_id#/edit/",
            "list_fields" => "#list_id#/fields/",
            "list_field_edit" => "#list_id#/field/#field_id#/",
            "list_element_edit" => "#list_id#/element/#section_id#/#element_id#/",
            "list_file" => "#list_id#/file/#section_id#/#element_id#/#field_id#/#file_id#/",
            "bizproc_log" => "#list_id#/bp_log/#document_state_id#/",
            "bizproc_workflow_start" => "#list_id#/bp_start/#element_id#/",
            "bizproc_task" => "#list_id#/bp_task/#section_id#/#element_id#/#task_id#/",
            "bizproc_workflow_admin" => "#list_id#/bp_list/",
            "bizproc_workflow_edit" => "#list_id#/bp_edit/#ID#/",
            "bizproc_workflow_vars" => "#list_id#/bp_vars/#ID#/",
            "bizproc_workflow_constants" => "#list_id#/bp_constants/#ID#/",
            "list_export_excel" => "#list_id#/excel/",
            "catalog_processes" => "catalog_processes/",
        )
    ),
    false
);
?>












<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>