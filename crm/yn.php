<?php
/**
 * User: German
 * Date: 07.02.2018
 * Time: 16:18
 */

@set_time_limit(0);
@ignore_user_abort(true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
if (\Bitrix\Main\Loader::includeModule('sale') === false)
    return;
$logger = Logger::getLogger("InvoiceYN", "Invoice/yn.log");
$rsInvoice = CCrmInvoice::GetList(array('ID' => 'ASC'), array(
    'STATUS_ID' => 'Y',
    'EXTERNAL_ORDER' => 'N'
    ));
$CCrmInvoice = new CCrmInvoice();
while  ($arInvoice = $rsInvoice->Fetch()) {
    $logger->log($arInvoice['ACCOUNT_NUMBER'].";".$arInvoice['DATE_BILL'].";".$arInvoice['STATUS_ID']
        .";".$arInvoice['EXTERNAL_ORDER'].";".$arInvoice['ID'].";".$arInvoice['UF_LINK']);
}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");