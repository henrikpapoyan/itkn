<?php
/**
 * User: German
 * Date: 25.08.2017
 * Time: 10:32
 */
@set_time_limit(0);
@ignore_user_abort(true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$fname = $_SERVER["DOCUMENT_ROOT"]."/log/Invoice/schet.csv";

$logger = Logger::getLogger("InvoiceIC", "Invoice/Del/icd.log");
$loggerNo = Logger::getLogger("InvoiceICNo", "Invoice/Del/icdNo.log");
$loggerNoJSON = Logger::getLogger("InvoiceICNoJSON", "Invoice/Del/icdNoJSON.log");
$loggerNoBitrix = Logger::getLogger("InvoiceICNoBitrix", "Invoice/Del/icdNoBitrix.log");
$loggerNoBitrixJSON = Logger::getLogger("InvoiceICNoBitrixJSON", "Invoice/Del/icdNoBitrixJSON.log");
$loggerNoBitrix2 = Logger::getLogger("InvoiceICNoBitrix2", "Invoice/Del/icdNoBitrix2.log");
$loggerNo1C = Logger::getLogger("InvoiceICNo1C", "Invoice/Del/icdNo1C.log");
//$loggerSum = Logger::getLogger("InvoiceICSUM", "Invoice/Del/icdSum.log");
//$loggerSumJSON = Logger::getLogger("InvoiceICSUMJSON", "Invoice/Del/icdSumJSON.log");
//$loggerSumBitrix = Logger::getLogger("InvoiceICSUMBitrix", "Invoice/Del/icdSumBitrix.log");
//$loggerStatus = Logger::getLogger("InvoiceICStatus", "Invoice/Del/icdStatus.log");
//$loggerStatusAll = Logger::getLogger("InvoiceICStatusAll", "Invoice/Del/icdStatusAll.log");
//$loggerStatusBitrix = Logger::getLogger("InvoiceICStatusBitrix", "Invoice/Del/icdStatusBitrix.log");
//$loggerStatusBitrixID = Logger::getLogger("InvoiceICStatusBitrixID", "Invoice/Del/icdStatusBitrixID.log");
//$loggerStatus1C = Logger::getLogger("InvoiceICStatus1C", "Invoice/Del/icdStatus1C.log");
//$loggerDatePay = Logger::getLogger("InvoiceICDatePay", "Invoice/Del/icdDatePay.log");
//$loggerDatePay2 = Logger::getLogger("InvoiceICDatePay2", "Invoice/Del/icdDatePay2.log");
$loggerDel1C = Logger::getLogger("InvoiceICDel1C", "Invoice/Del/icdDel1C.log");
$loggerDel1CJSON = Logger::getLogger("InvoiceICDel1CJSON", "Invoice/Del/icdDel1CJSON.log");
$arFilter = array();
$rsInvoice = CCrmInvoice::GetList(array(), $arFilter);
$arInvoiceAll = array();
$arInvoice2 = null;
while($arInvoice = $rsInvoice->Fetch())
{
//    $logger->log("00020 [".print_r($arInvoice,true));

    $number1C = trim($arInvoice['ACCOUNT_NUMBER']);
    $arInvoiceAll[$number1C]['ACCOUNT_NUMBER'] = $number1C;
    $arInvoiceAll[$number1C]['PRICE'] = $arInvoice['PRICE'];
    $arInvoiceAll[$number1C]['ID'] = $arInvoice['ID'];
    $arInvoiceAll[$number1C]['STATUS_ID'] = $arInvoice['STATUS_ID'];
    $arInvoiceAll[$number1C]['PAY_VOUCHER_NUM'] = $arInvoice['PAY_VOUCHER_NUM'];
    $arInvoiceAll[$number1C]['PAY_VOUCHER_DATE'] = $arInvoice['PAY_VOUCHER_DATE'];
    $arInvoiceAll[$number1C]['DATE_INSERT'] = $arInvoice['DATE_INSERT'];
    $arInvoiceAll[$number1C]['DATE_BILL'] = $arInvoice['DATE_BILL'];

    $arInvoice2 = $arInvoice;
}

$logger->log(print_r($arInvoice2,true));
$logger->log("arInvoiceAll=[".print_r($arInvoiceAll,true)."]");
$cNo = 0;
$cSum = 0;
$cNoBitrix = 0;
$cNoBitrix2 = 0;
$cNo1C = 0;
$cSumBitrix = 0;
$cStatus = 0;
$cStatusBitrix = 0;
$cStatus1C = 0;
$cDatePayAll = 0;
$cDatePay = 0;
$cDatePay2 = 0;
$cDel1C = 0;
$arStatus = array(
    'A' => 'Не оплачен', //Ждет оплаты
    'P' => 'Оплачен',
    'N' => 'NULL', //Черновик
    'D' => 'Отменен',
    'S' => 'Не оплачен' //Отправлен клиенту
);
if (($handle = fopen($fname, "r")) !== FALSE) {
    $logger->log(print_r($data, true));
    $row = 0;
    $CCrmInvoice = new CCrmInvoice();
    while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
        $row++;
        $status = $data[11];
        if ($status == 'NULL')
        {
            $status = 'Не оплачен';
        }
        $id = $data[7];
        if ($row == 1)
        {
            continue;
        }
        $logger->log(print_r($data, true));
//        $arFilter = array('ACCOUNT' => $data);
//        $rsInvoice = CCrmInvoice::GetList(array(), $arFilter);
//        if($arInvoice = $rsInvoice->Fetch())
        $number1C = trim($data[0]);
        $year = substr($data[1], 6, 4);
        $logger->log("year=[".print_r($year, true)."]");
        if($year == 2018)
        {
            continue;
        }
        if(isset($arInvoiceAll[$number1C]))
        {
            $arInvoiceAll[$number1C]['1C'] = 'Y';
        }
        else
        {
            $cNo++;
            $loggerNo->log(print_r($data, true));
            if($id >0)
            {
                if ($arInvoice = CCrmInvoice::GetByID($id))
                {
                    $loggerNo->log(print_r($arInvoice, true));
                }
                else
                {
                    $loggerNoBitrix->log(print_r($data, true));
                    $loggerNoBitrixJSON->log(json_encode($data));
                    $cNoBitrix ++;
                    if($data[11] != 'Отменен'
                        && $data[11] != 'NULL'
                    )
                    {
                        $loggerNoBitrix2->log(print_r($data, true));
                        $cNoBitrix2 ++;
                    }
                }
//                $loggerNoBitrix->log(print_r($arInvoice, true));
            }
            else
            {
                $loggerNo1C->log(print_r($data, true));
                $cNo1C ++;
                $loggerNoJSON->log(json_encode(array('data' => $data, 'invoice' => $arInvoiceAll[$number1C])));

            }
        }
    }
    fclose($handle);
}

$arNo1C = array();
foreach ($arInvoiceAll as $iInvoice => $vInvoice) {
    $year = substr($vInvoice['DATE_INSERT'], 6, 4);

    if($vInvoice['1C'] != 'Y' && $year == '2017'
       && (substr($vInvoice['ACCOUNT_NUMBER'], 0, 4) != '0000')
//            || substr($vInvoice['ACCOUNT_NUMBER'], 0, 2) == 'БИ'
//        )
        && $vInvoice['STATUS_ID'] != 'Y'
        && strlen($vInvoice['ACCOUNT_NUMBER']) <= 11
    )
    {
        $arNo1C[$iInvoice] = $vInvoice;
        $cDel1C ++;
    }
}

$logger->log("arNo1C=[".print_r($arNo1C,true)."]");
$CCrmInvoice = new CCrmInvoice();
foreach ($arNo1C as $iInvoice => $vInvoice) {
    $loggerDel1C->log(print_r($vInvoice, true));
    if($vInvoice['ID'] == 187)
    {
        continue;
    }
    if(
        $vInvoice['STATUS_ID'] == 'D'
        || $vInvoice['STATUS_ID'] == 'N'
        || $vInvoice['STATUS_ID'] == 'A'
        || $vInvoice['STATUS_ID'] == 'S'
        || $vInvoice['STATUS_ID'] == 'P'
    )
    {
        $loggerDel1CJSON->log(json_encode($vInvoice));

        $statusParams= array(
            'STATE_SUCCESS' => false,
            'STATE_FAILED' => true,
        );
        try {
            $ret = $CCrmInvoice->SetStatus($vInvoice['ID'], 'Y', $statusParams);
            $loggerDel1C->log("ret=[" . print_r($ret, true) . "]");
        }
        catch (Exception $e) {
            $ErrMSG = $e->getMessage();
            $loggerDel1C->log("ErrMSG=[" . print_r($ErrMSG, true) . "]");
        }
//        break;
    }
}


$logger->log("row=[".print_r($row,true)."]");
$logger->log("cNo=[".print_r($cNo,true)."]");
$logger->log("cNoBitrix=[".print_r($cNoBitrix,true)."]");
$logger->log("cNo1C=[".print_r($cNo1C,true)."]");
$logger->log("cNoBitrix2=[".print_r($cNoBitrix2,true)."]");
//$logger->log("cSum=[".print_r($cSum,true)."]");
//$logger->log("cSumBitrix=[".print_r($cSumBitrix,true)."]");
//$logger->log("cStatus=[".print_r($cStatus,true)."]");
//$logger->log("cStatusBitrix=[".print_r($cStatusBitrix,true)."]");
//$logger->log("cStatus1C=[".print_r($cStatus1C,true)."]");
//$logger->log("cDatePayAll=[".print_r($cDatePayAll,true)."]");
//$logger->log("cDatePay=[".print_r($cDatePay,true)."]");
//$logger->log("cDatePay2=[".print_r($cDatePay2,true)."]");
$logger->log("cDel1C=[".print_r($cDel1C,true)."]");

