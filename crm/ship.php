<?php
/**
 * User: German
 * Date: 15.02.2018
 * Time: 10:28
 */
@set_time_limit(0);
@ignore_user_abort(true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
Bitrix\Main\Loader::includeModule('sale');
$logger = Logger::getLogger("InvoiceShip", "Invoice/ship/ship.log");
$loggerNo = Logger::getLogger("InvoiceShipNo", "Invoice/ship/shipNo.log");
$loggerNum = Logger::getLogger("InvoiceShipNum", "Invoice/ship/shipNum.log");

$arFilter = array(
//    'filter' => array('ID' =>
//    array(5372
////        3189
//        )
//    )
);
$rsOrder = \Bitrix\Sale\Order::getList($arFilter);
$cShip = 0;
$c = 0;
while($arOrder = $rsOrder->fetch())
{
    $c++;
//    $logger->log("arOrder=[".print_r($arOrder, true)."]");
//    if($c == 100)
//    {
//        break;
//    }
    $order = \Bitrix\Sale\Order::load($arOrder['ID']); //5472);//58);//
    $shipmentCollection = $order->getShipmentCollection();
    $logger->log("ID=[".print_r($arOrder['ID'], true)."]");
    $logger->log("shipmentCollection->count()=[".print_r($shipmentCollection->count(), true)."]");
    $cShipItem = 0;
    foreach ($shipmentCollection as $shipment)
    {
//        file_put_contents(__FILE__.".log", "shipment=[".print_r($shipment, true)."]\n", FILE_APPEND);
//        $s = new Bitrix\Sale\Shipment();
        $ShipmentItemCollection = $shipment->getShipmentItemCollection();
        $logger->log("00033 shipment->getId()=[".print_r($shipment->getId(), true)."]");
        $logger->log("00034 shipment->isSystem()=[".print_r($shipment->isSystem(), true)."]");

        $logger->log("ShipmentItemCollection->count()=[".print_r($ShipmentItemCollection->count(), true)."]");
        $cShipItem = $cShipItem + $ShipmentItemCollection->count();
    }
    if($cShipItem ==  0)
    {
        $basket = $order->getBasket();
        if ($basket->count() > 0)
        {
            $loggerNum->log("ID=[".$arOrder['ID']."]".$arOrder['ACCOUNT_NUMBER']." ".
                $arOrder['DATE_BILL']->toString()." ". $arOrder['STATUS_ID']." EX=".$arOrder["EXTERNAL_ORDER"]);
            $loggerNo->log("ID=[".print_r($arOrder['ID'], true)."]");
            $loggerNo->log("basket->count()=[".print_r($basket->count(), true)."]");
            $loggerNo->log("shipmentCollection->count()=[".print_r($shipmentCollection->count(), true)."]");
            foreach ($basket as $itemBasket) {

                $loggerNo->log("itemBasket=[".print_r(
                        $itemBasket->getField('NAME') . ' - ' . $itemBasket->getQuantity()
                        , true)."]");
            }

            foreach ($shipmentCollection as $shipment) {
                $shipment->isSystem();

                $loggerNo->log("00058 shipment->getId()=[".print_r($shipment->getId(), true)."]");
                $loggerNo->log("00058 shipment->isSystem()=[".print_r($shipment->isSystem(), true)."]");
                $loggerNo->log("00059 shipment->getShipmentItemCollection()->count()=[".print_r($shipment->getShipmentItemCollection()->count(), true)."]");
//                if(!$shipment->isSystem())
//                {
//                    $shipmentItemCollection2 = $shipment->getShipmentItemCollection();
//                    file_put_contents(__FILE__.".0.log", "shipmentItemCollection2->count()=[".print_r($shipmentItemCollection2->count(), true)."]\n", FILE_APPEND);
//
//                    foreach ($basket as $basketItem) {
//                        $item = $shipmentItemCollection2->createItem($basketItem);
//                        $item->setQuantity($basketItem->getQuantity());
//                    }
//
//                    $result = $order->save();
//                    file_put_contents(__FILE__.".0.log", "00070 result->isSuccess()=[".print_r($result->isSuccess(), true)."]\n", FILE_APPEND);
//                    if (!$result->isSuccess())
//                    {
//                        file_put_contents(__FILE__.".0.log", "00073s result->getErrors()=[".print_r($result->getErrors(), true)."]\n", FILE_APPEND);
//                    }
//                }
            }
            if (false)//$shipmentCollection->count() == 1)
            {
                $newShipment = $shipmentCollection->createItem(
//                    Bitrix\Sale\Delivery\Services\Manager::getObjectById(1)
                );
                $newShipmentItemCollection = $newShipment->getShipmentItemCollection();
                $newShipment->setField('CURRENCY', $order->getCurrency());
                $newShipment->setField('DELIVERY_ID', 1);
                foreach ($basket as $basketItem) {
                    $item = $newShipmentItemCollection->createItem($basketItem);
                    $item->setQuantity($basketItem->getQuantity());
                    $loggerNo->log("00106 item=[".print_r($item, true)."]");
                }
                $result = $newShipment->save();
                $loggerNo->log("00108 result->isSuccess()=[".print_r($result->isSuccess(), true)."]");
                if (!$result->isSuccess())
                {
                    $loggerNo->log("00111 result->getErrors()=[".print_r($result->getErrors(), true)."]");
                }

                $result = $order->save();
                $loggerNo->log("result->isSuccess()=[".print_r($result->isSuccess(), true)."]");
                if (!$result->isSuccess())
                {
                    $loggerNo->log("result->getErrors()=[".print_r($result->getErrors(), true)."]");
                }
            }

            $cShip++;
        }
    }
//    break;
}
$logger->log("cShip=[".print_r($cShip, true)."]");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");