<?php
/**
 * User: German
 * Date: 04.07.2017
 * Time: 16:10
 */
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$requisite = new \Bitrix\Crm\EntityRequisite();

$fieldsInfo = $requisite->getFormFieldsInfo();

$select = array_keys($fieldsInfo);

$arFilter = array(
//    [ENTITY_TYPE_ID] => 4
            '=ENTITY_TYPE_ID' => 4,//$entityTypeId,
//    '=ENTITY_ID' => 22105,//$arCompany['ID'],//$entityId
//    '=RQ_INN' => $DATA['AGENT']['INN']
);
file_put_contents(__FILE__.".log", "00020arFilter=[".print_r($arFilter, true)."]\n", FILE_APPEND);
$rsRQ = $requisite->getList(
    array(
        'order' => array('SORT' => 'ASC', 'ID' => 'ASC'),
        'filter' => $arFilter,
        'select' => $select
    )
);
$arINN =array();
while ($arRQ = $rsRQ->fetch()) {
    file_put_contents(__FILE__.".log", "00030arRQ=[".print_r($arRQ, true)."]\n", FILE_APPEND);
//    if (isset($arINN[$arRQ['ENTITY_ID']]))
//    {
//
//    }
//    else
//    {
        if ($arRQ['PRESET_ID'] == 1 || $arRQ['PRESET_ID'] ==2)
        {
            $arINN[$arRQ['ENTITY_ID']][] = $arRQ['ID'];
        }
//    }
}
file_put_contents(__FILE__.".inn.log", "arINN=[".print_r($arINN, true)."]\n", FILE_APPEND);
$arDuble = array();
foreach ($arINN as $k => $v) {
    if (count($v) > 1)
    {
        $arDuble[$k] = $v;
    }
}
file_put_contents(__FILE__.".dub.log", "count=[".print_r(count($arDuble), true)."]\n", FILE_APPEND);
file_put_contents(__FILE__.".dub.log", "arDuble=[".print_r($arDuble, true)."]\n", FILE_APPEND);


require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");