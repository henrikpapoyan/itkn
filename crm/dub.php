<?php
/**
 * User: German
 * Date: 22.08.2017
 * Time: 20:08
 */
@set_time_limit(0);
@ignore_user_abort(true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$logger = Logger::getLogger("Dub", "Dub/Dub.log");
$loggerID = Logger::getLogger("DubID", "Dub/DubID.log");
$loggerTime = Logger::getLogger("DubTime", "Dub/DubTime.log");
$company_id = file_get_contents(__DIR__."/company_id");
if (empty($company_id))
{
    $company_id = 0;
}

$rsCompany = CCrmCompany::GetList(array('ID' => 'ASC'),array('>ID' => $company_id),array('ID','TITLE', 'UF_LINK'));

$c = 0;
$c1000 = 0;
$loggerTime->log($c);
$requisite = new \Bitrix\Crm\EntityRequisite();

$fieldsInfo = $requisite->getFormFieldsInfo();

$select = array_keys($fieldsInfo);

while($arCompany = $rsCompany->Fetch())
{
    file_put_contents(__DIR__."/company_id", $arCompany['ID']);
    $c++;
    $c1000++;

    $arFilter = array(
            '=ENTITY_TYPE_ID' => 4,//$entityTypeId,
            '=ENTITY_ID' => $arCompany['ID'],//$entityId
//        '=RQ_INN' => $DATA['AGENT']['INN']
    );
    $rsRQ = $requisite->getList(
        array(
            'order' => array('SORT' => 'ASC', 'ID' => 'ASC'),
            'filter' => $arFilter,
            'select' => $select
        )
    );
    $inn = '';
    $kpp = '';
    if ($arRQ = $rsRQ->fetch()) {
        $inn = $arRQ['RQ_INN'];
        $kpp = $arRQ['RQ_KPP'];

    }



    $logger->log(";".$arCompany['ID'].";".strtoupper($arCompany['TITLE']).";".$arCompany['UF_LINK'].";".$inn.";".$kpp.";");
    if($c1000 == 1000)
    {
        $loggerTime->log($c);
        $c1000 = 0;
    }
//    if ($c == 5000)
//    {
//        break;
//    }
}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");