<?php
/**
 * User: German
 * Date: 04.06.2017
 * Time: 20:53
 */
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$rsCompany = CCrmCompany::GetList();
$fname = $_SERVER["DOCUMENT_ROOT"]."/crm/bcc.csv";
file_put_contents($fname, "");
while ($arCompany = $rsCompany->Fetch())
{
    if(substr($arCompany['UF_LINK'], 8, 1) == "-")
    {
        file_put_contents($fname, $arCompany['UF_LINK'].";".$arCompany['TITLE']."\n", FILE_APPEND);
    }
}
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");