<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/intranet/public/crm/reports/report/index.php");
$APPLICATION->SetTitle(GetMessage("CRM_TITLE"));
?><?$APPLICATION->IncludeComponent("bitrix:crm.report", "template1", Array(
	"SEF_MODE" => "Y",	// Включить поддержку ЧПУ
		"REPORT_ID" => $_REQUEST["report_id"],	// ID отчета
		"SEF_FOLDER" => "/crm/reports/report/",	// Каталог ЧПУ (относительно корня сайта)
		"SEF_URL_TEMPLATES" => array(
			"index" => "index.php",
			"report" => "report/",
			"construct" => "construct/#report_id#/#action#/",
			"show" => "view/#report_id#/",
		),
		"VARIABLE_ALIASES" => array(
			"index" => "",
			"report" => "",
			"construct" => "",
			"show" => "",
		)
	),
	false
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>