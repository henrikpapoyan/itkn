<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("");
IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/intranet/public/crm/reports/report/index.php");
$APPLICATION->SetTitle(GetMessage("CRM_TITLE"));
?><?$APPLICATION->IncludeComponent(
	"bitrix:crm.report",
	"",
	Array(
		"REPORT_ID" => $_REQUEST["report_id"],
		"SEF_FOLDER" => "/crm/reports/report/",
		"SEF_MODE" => "Y",
		"SEF_URL_TEMPLATES" => Array("construct"=>"construct/#report_id#/#action#/","index"=>"index.php","report"=>"report/","show"=>"view/#report_id#/"),
		"VARIABLE_ALIASES" => array("index"=>"","report"=>"","construct"=>"","show"=>"",)
	)
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>