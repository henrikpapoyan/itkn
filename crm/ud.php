<?php
/**
 * User: German
 * Date: 28.04.2017
 * Time: 17:09
 */
@set_time_limit(3600);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$IBLOCK_ID = GetIBlockIDByCode('registry_contracts');

if ($IBLOCK_ID > 0)
{
    $arSelect = Array("ID", "IBLOCK_ID", "NAME", "PROPERTY_TIP_DOGOVORA_NEW", "PROPERTY_TIP_KONTRAGENTA");
    $arFilter = Array("IBLOCK_ID" => $IBLOCK_ID, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
    $rsDogovor = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
    while($arDogovor = $rsDogovor->Fetch()){
        if($arDogovor['PROPERTY_TIP_DOGOVORA_NEW_VALUE'] == 'Агентский')
        {
            file_put_contents(__FILE__.".log", "arDogovor=[".print_r($arDogovor, true)."]\n", FILE_APPEND);
            $ret = CIBlockElement::SetPropertyValueCode($arDogovor['ID'], "KOMPANIYA_ZAKAZCHIK", "ЭСК");
            file_put_contents(__FILE__.".log", "ret=[".print_r($ret, true)."]\n", FILE_APPEND);
        }
        else
        {
            file_put_contents(__FILE__.".log2", "arDogovor=[".print_r($arDogovor, true)."]\n", FILE_APPEND);

        }
    }

}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
