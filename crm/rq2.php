<?php
/**
 * User: German
 * Date: 16.11.2017
 * Time: 11:26
 */
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
global $DB;

$strSql = "
        SELECT
            ENTITY_ID,
            COUNT(*) CNT
        FROM b_crm_requisite
        WHERE
          ENTITY_TYPE_ID = 4
        GROUP BY
            ENTITY_ID
        HAVING COUNT(*) > 1
        ";
$res = $DB->Query($strSql, false, $err_mess.__LINE__);
$logger = Logger::getLogger("CompanyRQ2", "Company/rq2.log");
$loggerCSV = Logger::getLogger("CompanyRQ2CSV", "Company/rq2.csv");
$c = 0;
$arCompanyID = array();
while($arRes = $res->Fetch())
{
    $c++;
//    $logger->log("arRes=[".print_r($arRes, true)."]");
    $arCompanyID[] = $arRes['ENTITY_ID'];
}
$logger->log("c=[".print_r($c, true)."]");
$rsCompany = CCrmCompany::GetList(array(),array('ID' => $arCompanyID));
while($arCompany = $rsCompany->Fetch())
{
    $logger->log("arCompany=[".print_r($arCompany, true)."]");
    $loggerCSV->log(";".$arCompany['ID'].";".$arCompany['UF_CRM_COMPANY_GUID'].";".$arCompany['UF_ID_PROD']);
}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");