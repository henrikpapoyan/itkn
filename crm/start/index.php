<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/intranet/public/crm/start/index.php");
global $APPLICATION;
$APPLICATION->SetTitle(GetMessage("CRM_TITLE"));?>
<style>
.bx-filter-add-button {
    display:none;
}
</style>
<?
$APPLICATION->IncludeComponent(
	"itk:crm.channel_tracker",
	"",
	array()
);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>