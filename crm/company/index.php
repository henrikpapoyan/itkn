<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/intranet/public/crm/company/index.php");
//$APPLICATION->SetTitle(GetMessage("CRM_TITLE"));
$APPLICATION->SetTitle("Список клиентов");
CModule::IncludeModule('crm');

$urls = "https://".$_SERVER["SERVER_NAME"]."/local/php_interface/ofd.bitrix24/ajax/duble-contr.php";
$uri = $APPLICATION->GetCurUri();
$idUri = explode("/", $uri);
$idContragent =trim($idUri[4]);

if(!empty($idContragent))
{
    session_start();
    $_SESSION["AVR"]["COMPANY"] = $idContragent;
    $IBLOCK_ID_UPD = getIblockIDByCode('CLOSING_DOCUMENTS_UPD');
    ?><style>
      #leac-button-add-element-<?=$IBLOCK_ID_UPD?>{display: none;}
    </style> <script>
        $(document).ready(function () {
            function tablecontent(el_id) {
                var newarr = [];
                $("tr[data-id='" + el_id + "'] span.main-grid-cell-content table.grain-tables-table-view  tbody").each(function () {
                    var rowt = $("tr[data-id='" + el_id + "'] span.main-grid-cell-content table.grain-tables-table-view  tbody tr").length;
                    newarr += $(this).html();
                    for(var i = 0; i < rowt.length; i++){
                        newarr[i].innerHTML = newarr[i];
                    }
                });
                return newarr;
            }
            function show() {
                $("tr.main-grid-row").each(function() {
                    var el_id = $(this).attr("data-id");
                    var tu = $(this);
                    $(tu).each(function () {
                        var table_n = $("tr[data-id='" + el_id + "'] table.grain-tables-table-view");
                        var row = tablecontent(el_id);
                        var deleT_table = $("tr[data-id='" + el_id + "'] table.grain-tables-table-view:not(:first)").remove();
                        var table_body_new_tr = $("tr[data-id='" + el_id + "'] span.main-grid-cell-content table.grain-tables-table-view:first tbody tr");
                        var table_body_new = $("tr[data-id='" + el_id + "'] span.main-grid-cell-content table.grain-tables-table-view:first tbody");
                        table_body_new_tr.remove();
                        $(table_body_new).append(row);
                        table_body_new = undefined;
                        table_body_new_tr = undefined;
                        row = undefined;
                        deleT_table = undefined;

                        var i = 0;
                        while (i <= table_n) {
                            $("tr[data-id='" + el_id + "'] span.main-grid-cell-content br").remove();
                            ++i;
                        }
                    });
                    el_id = undefined;
                    tu = undefined;
                });
            }

            $(document).bind('click', function(){
                if($(".grain-tables-table-view")){
                    var timerId = setInterval(function() {
                        show();
                    }, 1000);
                    setTimeout(function() {
                        clearInterval(timerId);
                    }, 3000);
                }
            });

        });
    </script> <?
}



if(!empty($_POST["idContragent"])){
    $idContragentPost = $_POST["idContragent"];
    $name = "BPM_".$_POST["idContragent"];
    $result = readArrayInFile($name);
    $path = \Bitrix\Main\Application::getDocumentRoot() . OFD_BITRIX_TMP;
    $filename = $path.$name.".txt";

    if(strlen($result["inn"]) != 15) {
        $logger = Logger::getLogger('PostIdContragent', 'ofd.bitrix24/contragent/PostIdContragent.log');
        $logger->log("START. Company add BPM ");
        $logger->log($result);
        $log = CrmBitrixBpm::contragenAdd($result);
        if($log["status"] == 200){
            unlink($filename);
        }
        $logger->log("END. Company add BPM ");
        $logger->log(array($result, $log));
    }

    unset($_SESSION["ADD_CONTR_BPM"][$idContragentPost]);
}
?> <style>
.bx-filter-add-button {
    display:none;
}

.bx-filter-item-delete{
	display:none;
}
</style> <span class="error_duble" style="color:red"></span>
<?$APPLICATION->IncludeComponent(
	"bitrix:crm.company",
	"template2",
	Array(
		"ELEMENT_ID" => $_REQUEST["company_id"],
		"NAME_TEMPLATE" => "",
		"PATH_TO_CONTACT_EDIT" => "/crm/contact/edit/#contact_id#/",
		"PATH_TO_CONTACT_SHOW" => "/crm/contact/show/#contact_id#/",
		"PATH_TO_DEAL_EDIT" => "/crm/deal/edit/#deal_id#/",
		"PATH_TO_DEAL_SHOW" => "/crm/deal/show/#deal_id#/",
		"PATH_TO_INVOICE_EDIT" => "/crm/invoice/edit/#invoice_id#/",
		"PATH_TO_INVOICE_SHOW" => "/crm/invoice/show/#invoice_id#/",
		"PATH_TO_LEAD_CONVERT" => "/crm/lead/convert/#lead_id#/",
		"PATH_TO_LEAD_EDIT" => "/crm/lead/edit/#lead_id#/",
		"PATH_TO_LEAD_SHOW" => "/crm/lead/show/#lead_id#/",
		"PATH_TO_USER_PROFILE" => "/company/personal/user/#user_id#/",
		"SEF_FOLDER" => "/crm/company/",
		"SEF_MODE" => "Y",
		"SEF_URL_TEMPLATES" => Array("edit"=>"edit/#company_id#/","import"=>"import/","index"=>"index.php","list"=>"list/","show"=>"show/#company_id#/")
	)
);?>
<?

if (!empty($_SESSION["DELETE_CONTR_LINE"][$idContragent]["guid"])) {
    $data = $_SESSION["DELETE_CONTR_LINE"][$idContragent];
    $logger = Logger::getLogger('deleteContragent', 'ofd.bitrix24/contragent/deleteContragent.log');
    $logger->log($_SESSION["DELETE_CONTR_LINE"][$idContragent]);
    $d[$idContragent] = CrmBitrixBpm::deleteContrSush($data);
    metkaColTime($idContragent, CONTRAGENT_METKA_UPDATE,1);
}
// or !empty($_SESSION["ADD_CONTR_BPM"][$idContragentPost]
if (!empty($_SESSION["ADD_CONTR_BPM"][$idContragent]) and empty($_POST["idContragent"])) {

    $res = RestBpmBitrix::verify(null,$idContragent);
    $res["url"] = $uri;
    if(strlen($res["inn"]) != 15){
        $log = CrmBitrixBpm::contragenAdd($res);
        $logger = Logger::getLogger('AddContragentEven', 'ofd.bitrix24/contragent/AddContragentEven.txt');
        $logger->log(array($log, $res));
    }

    unset($_SESSION["ADD_CONTR_BPM"][$idContragent]);
}
?>
   <script>
        $(document).ready(function () {


//GK            $('tr[data-dragdrop-id="UF_CRM_KPP"]').hide();
            if($("input[name='UF_CRM_INN']").val().length != 10)
            {
                $('tr[data-dragdrop-id="UF_CRM_KPP"]').hide(); //GK
            }
            $("input[name='UF_CRM_INN']").on('input', function() {
                var suminn = Number($( this ).val().length),
                    atribut = $('.crm-offer-requisite-option-text span span'),
                    block = $('.bx-interface-form .bx-crm-edit-form').is(":visible");
                /*if(suminn >= 10){
                    if(block === false){
                        atribut.click();
                    }
                }*/
                if(suminn === 10){
                    $('tr[data-dragdrop-id="UF_CRM_KPP"]').show();
                    $('input[name="UF_CRM_INN"]').css("border","1px solid #d9d9d9");
                    $('.errorinn').empty();
                }else{
                    $('tr[data-dragdrop-id="UF_CRM_KPP"]').hide();
                    $('input[name="UF_CRM_KPP"]').val('');
                }
                //alert($( this ).val());
            });

            if ( $('input[name="UF_CRM_KPP"]').css('display') != 'none' ){

                $("input[name='UF_CRM_KPP']").on('input', function() {
                    var sumkpp = Number($( this ).val().length),
                        sav = $('input[name="saveAndView"]'),
                        savadd = $('input[name="saveAndAdd"]'),
                        inputFocused = true;

                    $(this).on('focus', function() {
                        inputFocused = true;

                    });
                    $(this).on('blur', function() {
                        inputFocused = false;
                        //$('#log').html('Курсор за полем');
                    });
                    if (inputFocused) {
                        if(sumkpp <= 0){
                            $('.errorinn').empty();
                            $('input[name="UF_CRM_KPP"]').css("border","1px solid red");
                            $('input[name="UF_CRM_KPP"]').parent().append("<p style='color:red' class='errorinn'>Поле КПП должно быть заполнено!</p>");
                            sav.prop("disabled", "true");
                            savadd.prop("disabled", "true");
                        }else{
                            $('input[name="UF_CRM_KPP"]').css("border","1px solid #d9d9d9");
                            $('.errorinn').empty();
                            sav.removeAttr('disabled');
                            savadd.removeAttr('disabled');
                        }
                    }
                });
            }
            $(document).on('click', 'input[name="UF_CRM_INN"]', function(e){
                $(this).blur(function() {
                    var inn = $('input[name="UF_CRM_INN"]').val(),
                        sav = $('input[name="saveAndView"]'),
                        savadd = $('input[name="saveAndAdd"]'),
                        kpp = $('input[name="UF_CRM_KPP"]').val(),
                        url = '<?=$urls?>',
                        suminn = Number(inn.length),
                        sumkpp = Number(kpp.length),
                        dan = {'RQ_INN':inn, 'RQ_KPP':kpp},
                        block = $('.bx-interface-form .bx-crm-edit-form').is(":visible");

                    if(suminn === 10 ){
                        $('tr[data-dragdrop-id="UF_CRM_KPP"]').show();
                        $('input[name="UF_CRM_INN"]').css("border","1px solid #d9d9d9");
                        /*if(block === true){
                            $('input[name="REQUISITE[n0][RQ_INN]"]').val(inn);

                        }*/

                    }else if(suminn === 12){
                        $('input[name="UF_CRM_INN"]').css("border","1px solid #d9d9d9");
                        $('.errorinn').empty();
                        $.ajax({
                            type: "POST", //Метод отправки
                            url: url, //путь до php фаила отправителя
                            data: dan,
                            success: function(data){
                                // где показываем результат
                                $('.error_duble').html(data);
                            }
                        });
                        /*if(block === true){
                            $('input[name="REQUISITE[n0][RQ_INN]"]').val(inn);
                        }*/
                    }else if(suminn === 15){
                        $('input[name="UF_CRM_INN"]').css("border","1px solid #d9d9d9");
                        $('.errorinn').empty();
                        $.ajax({
                            type: "POST", //Метод отправки
                            url: url, //путь до php фаила отправителя
                            data: dan,
                            success: function(data){
                                // где показываем результат
                                $('.error_duble').html(data);
                            }
                        });
                        /*if(block === true){
                            $('input[name="REQUISITE[n0][RQ_INN]"]').val(inn);
                        }*/
                    }else if(suminn == 0){
                        $('.errorinn').empty();
                        $('input[name="UF_CRM_INN"]').css("border","1px solid red");
                        $('input[name="UF_CRM_INN"]').parent().append("<p style='color:red' class='errorinn'>Поле ИНН должно быть заполнено!</p>");
                        sav.prop("disabled", "true");
                        savadd.prop("disabled", "true");
                    }else{
                        //alert( suminn + 'ИНН' );
                        $('.errorinn').empty();
                        $('input[name="UF_CRM_INN"]').css("border","1px solid red");
                        $('input[name="UF_CRM_INN"]').parent().append("<p style='color:red' class='errorinn'>Не верный ИНН</p>");
                        sav.prop("disabled", "true");
                        savadd.prop("disabled", "true");
                    }
                });
            });
            $(document).on('click', 'input[name="UF_CRM_KPP"]', function(e){
                $(this).blur(function() {
                    var inn = $('input[name="UF_CRM_INN"]').val(),
                        sav = $('input[name="saveAndView"]'),
                        savadd = $('input[name="saveAndAdd"]'),
                        kpp = $('input[name="UF_CRM_KPP"]').val(),
                        url = '<?=$urls?>',
                        suminn = Number(inn.length),
                        sumkpp = Number(kpp.length),

                        dan = {'RQ_INN':inn, 'RQ_KPP':kpp};
                    $('#RQ_INN').click();
                    if (sumkpp === 9) {
                        //$('input[name="REQUISITE[n0][RQ_KPP]"]').val(kpp);
                        $.ajax({
                            type: "POST", //Метод отправки
                            url: url, //путь до php фаила отправителя
                            data: dan,
                            success: function (data) {
                                // где показываем результат
                                $('.error_duble').html(data);
                            }
                        });
                        $('.popup-search-result-item span').appendTo();
                        sav.removeAttr('disabled');
                        savadd.removeAttr('disabled');

                    }else if (sumkpp != 9) {
                        $('.errorinn').empty();
                        $('input[name="UF_CRM_KPP"]').css("border", "1px solid red");
                        $('input[name="UF_CRM_KPP"]').parent().append("<p style='color:red' class='errorinn'>Поле КПП должно содержать 9 цифр!</p>");
                        sav.prop("disabled", "true");
                        savadd.prop("disabled", "true");
                    }

                });
            });
        });
    </script><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>