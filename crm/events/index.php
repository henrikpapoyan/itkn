<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/intranet/public/crm/events/index.php");
$APPLICATION->SetTitle(GetMessage("CRM_TITLE"));
?><?$APPLICATION->IncludeComponent("bitrix:crm.event.view", "template1", Array(
	"ENTITY_ID" => "",	// ID элемента сущности
		"EVENT_COUNT" => "20",	// Количество событий на странице
		"EVENT_ENTITY_LINK" => "Y",	// Выводить заголовок сущности
		"PATH_TO_DEAL_SHOW" => "/crm/deal/show/#deal_id#/",
		"PATH_TO_QUOTE_SHOW" => "/crm/quote/show/#quote_id#/",
		"PATH_TO_CONTACT_SHOW" => "/crm/contact/show/#contact_id#/",
		"PATH_TO_COMPANY_SHOW" => "/crm/company/show/#company_id#/",
		"PATH_TO_LEAD_SHOW" => "/crm/lead/show/#lead_id#/",
		"PATH_TO_USER_PROFILE" => "/company/personal/user/#user_id#/"
	),
	false
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>