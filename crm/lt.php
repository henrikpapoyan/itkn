<?php
/**
 * User: German
 * Date: 21.11.2017
 * Time: 14:56
 */

@set_time_limit(0);
@ignore_user_abort(true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
CModule::IncludeModule("crm");
$logger = Logger::getLogger("lt", "Lead/lt.log");

$countLeads = 0;
$leadStateId = getIdElementListState("XML_STATE_LEAD_ACTIVITY");
$arFilterLead = array();//"UF_STATE_LEAD" => $leadStateId);
//if($type_id > 0) //GK
//{
//    $arFilterLead['UF_TYPE'] = $type_id;
//}
$lead_id = file_get_contents('lt.id');
$arFilterLead['>ID'] = $lead_id;
$arFilterLead['!SOURCE_ID'] = gk_GetOrderFilter();
$arSelectLead = array("ID","UF_CHANNEL", 'UF_TYPE');
$logger->log("arFilterLead=[".print_r($arFilterLead,true)."]");
$dbLeads = CCrmLead::GetList(array(), $arFilterLead, $arSelectLead);
$no_type = 0;
$CCrmLead = new CCrmLead();
while($arLead = $dbLeads->Fetch())
{
    $logger->log($arLead['ID'].';'.$arLead['UF_CHANNEL'].";".$arLead['UF_TYPE']);
    if (empty($arLead['UF_TYPE']))
    {
        $no_type++;
        $newFields = array('UF_TYPE' => 3418);
        $ret = $CCrmLead->Update($arLead['ID'], $newFields);
        $logger->log("ret=[".print_r($ret, true)."]");
    }
    file_put_contents('lt.id', $arLead['ID']);
}
$logger->log("no_type =$no_type");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");