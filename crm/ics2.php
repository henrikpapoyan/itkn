<?php
/**
 * User: German
 * Date: 20.12.2017
 * Time: 14:04
 */
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
if(!CModule::IncludeModule('sale'))
{
    return false;
}
$arFilter = array();
$rsInvoice = CCrmInvoice::GetList(array(), $arFilter);
$arInvoiceAll = array();
$loggerTax = Logger::getLogger("InvoiceTax", "Invoice/ics/icsTax.log");

while($arInvoice = $rsInvoice->Fetch())
{
//    $logger->log("00020 [".print_r($arInvoice,true));

    $number1C = trim($arInvoice['ACCOUNT_NUMBER']);
//    $year = substr($arInvoice['DATE_BILL'], 6, 4);
//    if($year != 2017 )
//    {
//        $number1c = $arInvoice['ACCOUNT_NUMBER']." ($year)";
//    }

    $arInvoiceAll[$number1C]['ACCOUNT_NUMBER'] = $number1C;
    $arInvoiceAll[$number1C]['DATE_BILL'] = $arInvoice['DATE_BILL'];
    $arInvoiceAll[$number1C]['PRICE'] = $arInvoice['PRICE'];
    $arInvoiceAll[$number1C]['ID'] = $arInvoice['ID'];
    $arInvoiceAll[$number1C]['STATUS_ID'] = $arInvoice['STATUS_ID'];
    $arInvoiceAll[$number1C]['PAY_VOUCHER_NUM'] = $arInvoice['PAY_VOUCHER_NUM'];
    $arInvoiceAll[$number1C]['PAY_VOUCHER_DATE'] = $arInvoice['PAY_VOUCHER_DATE'];
    $arInvoiceAll[$number1C]['UF_LINK'] = $arInvoice['UF_LINK'];
    $arInvoiceAll[$number1C]['COMMENTS'] = $arInvoice['COMMENTS'];
//    $loggerTax->log("number1C=[$number1C]");
    $arProductRows = CCrmInvoice::GetProductRows($arInvoiceAll[$number1C]['ID']);
    foreach ($arProductRows as $ipr => $vpr) {
        if($vpr['VAT_INCLUDED'] != 'Y')
        {
            $loggerTax->log("[$number1C] VAT_INCLUDED=[".print_r($vpr['VAT_INCLUDED'], true)."]");
            $loggerTax->log("arInvoiceAll[$number1C]=[".print_r($arInvoiceAll[$number1C], true)."]");
            $loggerTax->log("vpr=[".print_r($vpr, true)."]");
            $arFields = array(
                'VAT_INCLUDED' => 'Y'
            );
            $ret = CSaleBasket::Update($vpr['ID'], $arFields);
            $loggerTax->log("ret=[".print_r($ret, true)."]");
        }
    }

}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");