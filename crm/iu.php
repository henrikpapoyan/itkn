<?php
/**
 * User: German
 * Date: 25.08.2017
 * Time: 10:32
 */
@set_time_limit(0);
@ignore_user_abort(true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$fname = $_SERVER["DOCUMENT_ROOT"]."/log/Invoice/invoice_user.csv";

$logger = Logger::getLogger("InvoiceBitrix", "Invoice/Bitrix.log");
$loggerUser = Logger::getLogger("InvoiceBitrix2User", "Invoice/Bitrix2User.log");
$loggerTime = Logger::getLogger("InvoiceBitrixTime", "Invoice/BitrixTime.log");
$loggerNoRQ = Logger::getLogger("InvoiceBitrixNoRQ", "Invoice/BitrixNoRQ.log");
$loggerNoUser = Logger::getLogger("InvoiceBitrixNoUser", "Invoice/BitrixNoUser.log");
$loggerNoCompany = Logger::getLogger("InvoiceBitrixNoCompany", "Invoice/BitrixNoCompany.log");

if (($handle = fopen($fname, "r")) !== FALSE) {
    $logger->log(print_r($data, true));
    $company_id = 0;
    $name = '';
    $old_data = array();
    $arINN = array();
    $arCompany = array();
    $arUsers = array();
    $requisite = new \Bitrix\Crm\EntityRequisite();

    $fieldsInfo = $requisite->getFormFieldsInfo();

    $select = array_keys($fieldsInfo);

    while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
        $row++;
        if ($row == 1)
        {
            continue;
        }
        $logger->log(print_r($data, true));
        if(!empty($data[0]))
        {
            $arINN[$data[0]]['data'] = $data;
            $arFilter = array(
//            '=ENTITY_TYPE_ID' => 4,//$entityTypeId,
//            '=ENTITY_ID' => 5477,//$entityId
                '=RQ_INN' => $data[0]
            );
            //file_put_contents(__FILE__.".log", "00030arFilter=[".print_r($arFilter, true)."]\n", FILE_APPEND);
            $rsRQ = $requisite->getList(
                array(
                    'order' => array('SORT' => 'ASC', 'ID' => 'ASC'),
                    'filter' => $arFilter,
                    'select' => $select
                )
            );

            if ($arRQ = $rsRQ->fetch()) {
                $arINN[$data[0]]['company_id'] = $arRQ['ENTITY_ID'];
                $arCompany[$arRQ['ENTITY_ID']] = array('company_id' => $arRQ['ENTITY_ID']);
                $arFIO = explode(" ", $data[3]);
                $arFilter = array('LAST_NAME' => $arFIO[0]);
                $rsUser = CUser::GetList(($by="personal_country"), ($order="desc"), $arFilter);
                if($arUser = $rsUser->Fetch())
                {
                    $logger->log($arFIO[0]." arUser=[".print_r($arUser, true)."]");
                    $arCompany[$arRQ['ENTITY_ID']]['user_id'] = $arUser['ID'];
                    $arCompany[$arRQ['ENTITY_ID']]['inn'] = $data[0];
                }
                else
                {
                    $arFilter = array('NAME' => $arFIO[0]);
                    $rsUser = CUser::GetList(($by="personal_country"), ($order="desc"), $arFilter);
                    if($arUser = $rsUser->Fetch())
                    {
                        $logger->log($arFIO[0]." 2arUser=[".print_r($arUser, true)."]");
                        $arCompany[$arRQ['ENTITY_ID']]['user_id'] = $arUser['ID'];
                        $arCompany[$arRQ['ENTITY_ID']]['inn'] = $data[0];
                    }
                    else
                    {
                        $loggerNoUser->log("NoUser=[".print_r($data, true)."]");
                    }
                }
            }
            else
            {

                $loggerNoRQ->log("NoRQ=[".print_r($data, true)."]");
                $arRQ = array();
            }
        }
    }
    fclose($handle);
}
$logger->log("arINN=[".print_r($arINN, true)."]");
$logger->log("arCompany=[".print_r($arCompany, true)."]");

$rsInvoice = CCrmInvoice::GetList(array('ID' => 'ASC'), array('RESPONSIBLE_ID' => 372));
$CCrmInvoice = new CCrmInvoice();
while  ($arInvoice = $rsInvoice->Fetch())
{
    $logger->log("arInvoice=[".print_r($arInvoice, true)."]");
    $company_id = $arInvoice['UF_COMPANY_ID'];
    if(isset($arCompany[$company_id]))
    {
        $user_id = $arCompany[$company_id]['user_id'];
        if ($user_id > 0)
        {
            $loggerUser->log("id=".$arInvoice['ID']." company_id=".$company_id." user_id=[".$user_id);
            $arFields = array('RESPONSIBLE_ID' => $user_id);
            $ret = $CCrmInvoice->Update($arInvoice['ID'], $arFields);
            $loggerUser->log("ret=".$ret);
//            break;
        }
    }
    else
    {
        $logger->log("NoCompany =".$company_id." Invoice=".$arInvoice['ID']);
        $arCmp = CCrmCompany::GetByID($company_id);

        $arFilter = array(
            '=ENTITY_TYPE_ID' => 4,//$entityTypeId,
            '=ENTITY_ID' => $company_id,//$entityId
//            '=RQ_INN' => $data[0]
        );
        //file_put_contents(__FILE__.".log", "00030arFilter=[".print_r($arFilter, true)."]\n", FILE_APPEND);
        $rsRQ = $requisite->getList(
            array(
                'order' => array('SORT' => 'ASC', 'ID' => 'ASC'),
                'filter' => $arFilter,
                'select' => $select
            )
        );
        $inn = '';
        if ($arRQ = $rsRQ->fetch())
        {
            $inn = $arRQ['RQ_INN'];
        }
        $loggerNoCompany->log(";".$company_id.";".$inn.";".$arCmp['TITLE'].";".$arInvoice['ID']);
    }
}
