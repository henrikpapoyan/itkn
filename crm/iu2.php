<?php
/**
 * User: German
 * Date: 25.08.2017
 * Time: 10:32
 */
@set_time_limit(0);
@ignore_user_abort(true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$fname = $_SERVER["DOCUMENT_ROOT"]."/log/Invoice/invoice_user.csv";

$logger = Logger::getLogger("InvoiceBitrix", "Invoice/iu2.log");
$loggerUser = Logger::getLogger("InvoiceBitrix2User", "Invoice/iu2User.log");
$loggerTime = Logger::getLogger("InvoiceBitrixTime", "Invoice/iu2Time.log");
//$loggerNoRQ = Logger::getLogger("InvoiceBitrixNoRQ", "Invoice/iuNoRQ.log");
//$loggerNoUser = Logger::getLogger("InvoiceBitrixNoUser", "Invoice/iuNoUser.log");
//$loggerNoCompany = Logger::getLogger("InvoiceBitrixNoCompany", "Invoice/iuNoCompany.log");


$rsInvoice = CCrmInvoice::GetList(array('ID' => 'ASC'), array('RESPONSIBLE_ID' => 372));
$CCrmInvoice = new CCrmInvoice();
$cBitrix = 0;
$cNoCompany = 0;
$cBitrixAll = 0;
$CCrmInvoice = new CCrmInvoice();
while  ($arInvoice = $rsInvoice->Fetch())
{
    $cBitrixAll++;
    $logger->log("arInvoice=[".print_r($arInvoice, true)."]");
    $company_id = $arInvoice['UF_COMPANY_ID'];
    if($arCompany = CCrmCompany::GetByID($company_id))
    {
        $logger->log("arCompany=[".print_r($arCompany, true)."]");
        if($arInvoice['RESPONSIBLE_ID'] != $arCompany['ASSIGNED_BY'])
        {
            $loggerUser->log($arInvoice['ACCOUNT_NUMBER']." ".$arInvoice['RESPONSIBLE_ID']." != ". $arCompany['ASSIGNED_BY']);
            $arFields = array('RESPONSIBLE_ID' => $arCompany['ASSIGNED_BY']);
            $ret = $CCrmInvoice->Update($arInvoice['ID'], $arFields);
            $loggerUser->log("ret=[".print_r($ret, true)."]");
            $cBitrix++;
        }
//        break;
    }
    else
    {
        $cNoCompany++;
    }
//    $company_id = $arInvoice['UF_COMPANY_ID'];
//    if(isset($arCompany[$company_id]))
//    {
//        $user_id = $arCompany[$company_id]['user_id'];
//        if ($user_id > 0)
//        {
//            $loggerUser->log("id=".$arInvoice['ID']." company_id=".$company_id." user_id=[".$user_id);
//            $arFields = array('RESPONSIBLE_ID' => $user_id);
////            $ret = $CCrmInvoice->Update($arInvoice['ID'], $arFields);
//            $loggerUser->log("ret=".$ret);
////            break;
//        }
//    }
//    else
//    {
//        $logger->log("NoCompany =".$company_id." Invoice=".$arInvoice['ID']);
//        $arCmp = CCrmCompany::GetByID($company_id);
//
//        $arFilter = array(
//            '=ENTITY_TYPE_ID' => 4,//$entityTypeId,
//            '=ENTITY_ID' => $company_id,//$entityId
////            '=RQ_INN' => $data[0]
//        );
//        //file_put_contents(__FILE__.".log", "00030arFilter=[".print_r($arFilter, true)."]\n", FILE_APPEND);
//        $rsRQ = $requisite->getList(
//            array(
//                'order' => array('SORT' => 'ASC', 'ID' => 'ASC'),
//                'filter' => $arFilter,
//                'select' => $select
//            )
//        );
//        $inn = '';
//        if ($arRQ = $rsRQ->fetch())
//        {
//            $inn = $arRQ['RQ_INN'];
//        }
//        $loggerNoCompany->log(";".$company_id.";".$inn.";".$arCmp['TITLE'].";".$arInvoice['ID']);
//    }
}
$logger->log("cBitrixAll $cBitrixAll");
$logger->log("cBitrix $cBitrix");
$logger->log("cNoCompany $cNoCompany");

