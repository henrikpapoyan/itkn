<?php
/**
 * User: German
 * Date: 25.08.2017
 * Time: 10:32
 */
@set_time_limit(0);
@ignore_user_abort(true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$logger = Logger::getLogger("InvoiceIC", "Invoice/Sum/icd.log");
$loggerNo = Logger::getLogger("InvoiceICNo", "Invoice/Sum/icdNo.log");
$loggerNoJSON = Logger::getLogger("InvoiceICNoJSON", "Invoice/Sum/icdNoJSON.log");
$loggerNoBitrix = Logger::getLogger("InvoiceICNoBitrix", "Invoice/Sum/icdNoBitrix.log");
$loggerNoBitrixNum = Logger::getLogger("InvoiceICNoBitrixNum", "Invoice/Sum/icdNoBitrixNum.log");
$loggerNoBitrixJSON = Logger::getLogger("InvoiceICNoBitrixJSON", "Invoice/Sum/icdNoBitrixJSON.log");
$loggerNoBitrix2 = Logger::getLogger("InvoiceICNoBitrix2", "Invoice/Sum/icdNoBitrix2.log");
$loggerNo1C = Logger::getLogger("InvoiceICNo1C", "Invoice/Sum/icdNo1C.log");
$loggerSum = Logger::getLogger("InvoiceICSUM", "Invoice/Sum/icdSum.log");
//$loggerSumJSON = Logger::getLogger("InvoiceICSUMJSON", "Invoice/Del/icdSumJSON.log");
$loggerSumBitrix = Logger::getLogger("InvoiceICSUMBitrix", "Invoice/Sum/icdSumBitrix.log");
$loggerSum2 = Logger::getLogger("InvoiceICSUM2", "Invoice/Sum/icdSum2.log");
$loggerUser = Logger::getLogger("InvoiceICUser", "Invoice/Sum/icdUser.log");
$loggerUser4 = Logger::getLogger("InvoiceICUser4", "Invoice/Sum/icdUser4.log");
$loggerUser6 = Logger::getLogger("InvoiceICUser6", "Invoice/Sum/icdUser6.log");
$loggerUser7 = Logger::getLogger("InvoiceICUser7", "Invoice/Sum/icdUser7.log");
$loggerUserBitrix = Logger::getLogger("InvoiceICUserBitrix", "Invoice/Sum/icdUserBitrix.log");
$loggerUserBitrix1 = Logger::getLogger("InvoiceICUserBitrix1", "Invoice/Sum/icdUserBitrix1.log");
$loggerUserBitrix2 = Logger::getLogger("InvoiceICUserBitrix2", "Invoice/Sum/icdUserBitrix2.log");
$loggerUserBitrix3 = Logger::getLogger("InvoiceICUserBitrix3", "Invoice/Sum/icdUserBitrix3.log");
$loggerSuff = Logger::getLogger("InvoiceICSuff", "Invoice/Sum/icdSuff.log");
$loggerSuff2 = Logger::getLogger("InvoiceICSuff2", "Invoice/Sum/icdSuff2.log");
//$loggerStatusBitrix = Logger::getLogger("InvoiceICStatusBitrix", "Invoice/Del/icdStatusBitrix.log");
//$loggerStatusBitrixID = Logger::getLogger("InvoiceICStatusBitrixID", "Invoice/Del/icdStatusBitrixID.log");
//$loggerStatus1C = Logger::getLogger("InvoiceICStatus1C", "Invoice/Del/icdStatus1C.log");
//$loggerDatePay = Logger::getLogger("InvoiceICDatePay", "Invoice/Del/icdDatePay.log");
//$loggerDatePay2 = Logger::getLogger("InvoiceICDatePay2", "Invoice/Del/icdDatePay2.log");
$loggerDel1C = Logger::getLogger("InvoiceICDel1C", "Invoice/Sum/icdDel1C.log");
$loggerDel1CNum = Logger::getLogger("InvoiceICDel1CNum", "Invoice/Sum/icdDel1CNum.log");
$loggerDel1CJSON = Logger::getLogger("InvoiceICDel1CJSON", "Invoice/Sum/icdDel1CJSON.log");
$loggerBillDate = Logger::getLogger("InvoiceICBillDate", "Invoice/Sum/icdBillDate.log");
$loggerBillDate2 = Logger::getLogger("InvoiceICBillDate2", "Invoice/Sum/icdBillDate2.log");
$loggerPayDate = Logger::getLogger("InvoiceICPayDate", "Invoice/Sum/icdPayDate.log");
$loggerPayDate2 = Logger::getLogger("InvoiceICPayDate2", "Invoice/Sum/icdPayDate2.log");
$loggerPayBefore = Logger::getLogger("InvoiceICPayBefore", "Invoice/Sum/icdPayBefore.log");
$loggerPayed = Logger::getLogger("InvoiceICPayed", "Invoice/Sum/icdPayed.log");
$loggerPayed2 = Logger::getLogger("InvoiceICPayed2", "Invoice/Sum/icdPayed2.log");
$loggerCanceled = Logger::getLogger("InvoiceICCanceled", "Invoice/Sum/icdCanceled.log");
$loggerCanceled2 = Logger::getLogger("InvoiceICCanceled2", "Invoice/Sum/icdCanceled2.log");
$loggerCanceledNum = Logger::getLogger("InvoiceICCanceledNum", "Invoice/Sum/icdCanceledNum.log");

$arFilter = array();
$rsInvoice = CCrmInvoice::GetList(array(), $arFilter);
$arInvoiceAll = array();
$arInvoice2 = null;
$cSchet = 0;
$CCrmInvoice = new CCrmInvoice();
while($arInvoice = $rsInvoice->Fetch())
{
//    $logger->log("00020 [".print_r($arInvoice,true));

    $number1C = trim($arInvoice['ACCOUNT_NUMBER']);
    $year = substr($arInvoice['DATE_BILL'], 6, 4);
    if($year != 2017 )
    {
        $number1c = $arInvoice['ACCOUNT_NUMBER']." ($year)";
    }
    $arInvoiceAll[$number1C]['ACCOUNT_NUMBER'] = $number1C;
    $arInvoiceAll[$number1C]['PRICE'] = $arInvoice['PRICE'];
    $arInvoiceAll[$number1C]['ID'] = $arInvoice['ID'];
    $arInvoiceAll[$number1C]['STATUS_ID'] = $arInvoice['STATUS_ID'];
    $arInvoiceAll[$number1C]['PAY_VOUCHER_NUM'] = $arInvoice['PAY_VOUCHER_NUM'];
    $arInvoiceAll[$number1C]['PAY_VOUCHER_DATE'] = $arInvoice['PAY_VOUCHER_DATE'];
    $arInvoiceAll[$number1C]['DATE_INSERT'] = $arInvoice['DATE_INSERT'];
    $arInvoiceAll[$number1C]['DATE_BILL'] = $arInvoice['DATE_BILL'];
    $arInvoiceAll[$number1C]['DATE_PAY_BEFORE'] = $arInvoice['DATE_PAY_BEFORE'];
    $arInvoiceAll[$number1C]['UF_CRM_1514204770'] = $arInvoice['UF_CRM_1514204770'];
    $arInvoiceAll[$number1C]['UF_LINK'] = $arInvoice['UF_LINK'];
    $arInvoiceAll[$number1C]['RESPONSIBLE_ID'] = $arInvoice['RESPONSIBLE_ID'];
    $arInvoiceAll[$number1C]['FIO'] = GetUserName($arInvoice['RESPONSIBLE_ID']);
    $arInvoiceAll[$number1C]['COMMENTS'] = $arInvoice['COMMENTS'];
    $arInvoiceAll[$number1C]['EXTERNAL_ORDER'] = $arInvoice['EXTERNAL_ORDER'];
    $arInvoiceAll[$number1C]['PAYED'] = $arInvoice['PAYED'];
    $arInvoiceAll[$number1C]['CANCELED'] = $arInvoice['CANCELED'];

    $arInvoice2 = $arInvoice;
    $cSchet++;
    $suff1 = substr($arInvoice['DATE_BILL'], 6, 4);
    $suff2 = substr($arInvoice['ACCOUNT_NUMBER'], 11);
    if(($suff1 == '2017' && $suff2 == "") ||
        ($suff1 == '2018' && $suff2 ==' (2018)') ||
        ($suff1 == '2016' && $suff2 ==' (2016)'))
    {

    }
    else
    {
        $loggerSuff->log($arInvoice['DATE_BILL']." ".$arInvoice['ACCOUNT_NUMBER']." suff1=|$suff1| suff2=|$suff2|");
        if(($suff1 == '2017' && $suff2 == ' (2017)')
        || ($suff1 =='2017' && $suff2 ==' (2018)')
        )
        {
            $loggerSuff2->log($arInvoice['DATE_BILL']." ".$arInvoice['ACCOUNT_NUMBER']." suff1=|$suff1| suff2=|$suff2|");
            $ACCOUNT_NUMBER = substr($arInvoice['ACCOUNT_NUMBER'], 0, 11);
            $loggerSuff2->log("ACCOUNT_NUMBER=[".$ACCOUNT_NUMBER."] ID=[".$arInvoice['ID']."]");
            $newFields = array(
                'ACCOUNT_NUMBER' => $ACCOUNT_NUMBER
            );
            $ret = $CCrmInvoice->Update($arInvoice['ID'], $newFields);
            $loggerSuff2->log("ret=[".print_r($ret, true)."]");
        }elseif($suff1 == '2018' && $suff2 == '')
        {
            $loggerSuff2->log("00002".$arInvoice['DATE_BILL']." ".$arInvoice['ACCOUNT_NUMBER']." suff1=|$suff1| suff2=|$suff2|");
            $ACCOUNT_NUMBER = $arInvoice['ACCOUNT_NUMBER'].' (2018)';
            $loggerSuff2->log("00002ACCOUNT_NUMBER=[".$ACCOUNT_NUMBER."] ID=[".$arInvoice['ID']."]");
            $newFields = array(
                'ACCOUNT_NUMBER' => $ACCOUNT_NUMBER
            );
            $ret = $CCrmInvoice->Update($arInvoice['ID'], $newFields);
            $loggerSuff2->log("ret2=[".print_r($ret, true)."]");

        }elseif($suff1 == '2016' && $suff2 == '')
        {
            $loggerSuff2->log("00002".$arInvoice['DATE_BILL']." ".$arInvoice['ACCOUNT_NUMBER']." suff1=|$suff1| suff2=|$suff2|");
            $ACCOUNT_NUMBER = $arInvoice['ACCOUNT_NUMBER'].' (2016)';
            $loggerSuff2->log("00002ACCOUNT_NUMBER=[".$ACCOUNT_NUMBER."] ID=[".$arInvoice['ID']."]");
            $newFields = array(
                'ACCOUNT_NUMBER' => $ACCOUNT_NUMBER
            );
            $ret = $CCrmInvoice->Update($arInvoice['ID'], $newFields);
            $loggerSuff2->log("ret3=[".print_r($ret, true)."]");
        }

    }
//    PAYED
    $loggerPayed->log("$number1C PAYED=[".$arInvoiceAll[$number1C]['PAYED']."] [".$arInvoiceAll[$number1C]['STATUS_ID']);
    if($arInvoiceAll[$number1C]['PAYED'] == 'Y' && $arInvoiceAll[$number1C]['STATUS_ID'] != 'P')
    {
        $loggerPayed2->log("$number1C PAYED=[".$arInvoiceAll[$number1C]['PAYED']."] [".$arInvoiceAll[$number1C]['STATUS_ID']);
        $loggerPayed2->log("arInvoiceAll[$number1C]=[".print_r($arInvoiceAll[$number1C], true));
        $arFields = array(
            'PAYED' => 'N',
        );
        $ret = $CCrmInvoice->Update($arInvoiceAll[$number1C]['ID'], $arFields);
        $loggerPayed2->log("$ret [".print_r($ret, true)."]");
    }
//CANCELED
    $loggerCanceled->log("$number1C CANCELED=[".$arInvoiceAll[$number1C]['CANCELED']."] [".$arInvoiceAll[$number1C]['STATUS_ID']);
    if($arInvoiceAll[$number1C]['CANCELED'] == 'Y'
        && $arInvoiceAll[$number1C]['STATUS_ID'] != 'Y'
        && $arInvoiceAll[$number1C]['STATUS_ID'] != 'D'

    )
    {
        $loggerCanceled2->log("$number1C CANCELED=[".$arInvoiceAll[$number1C]['CANCELED']."] [".$arInvoiceAll[$number1C]['STATUS_ID']);
        $loggerCanceled2->log("arInvoiceAll[$number1C]=[".print_r($arInvoiceAll[$number1C], true));
        $loggerCanceledNum->log("$number1C ".$arInvoiceAll[$number1C]['DATE_BILL']." ".
            $arInvoiceAll[$number1C]['CANCELED']." EX=".$arInvoiceAll[$number1C]['EXTERNAL_ORDER']." ".
            $arInvoiceAll[$number1C]['STATUS_ID']." ".$arInvoiceAll[$number1C]['ID']);
        $arFields = array(
            'CANCELED' => 'N',
        );
        $ret = $CCrmInvoice->Update($arInvoiceAll[$number1C]['ID'], $arFields);
        $loggerCanceled2->log("$ret [".print_r($ret, true)."]");
    }
}

$logger->log("cSchet=".print_r($cSchet,true));
$logger->log("count=".print_r(count($arInvoiceAll),true));
$logger->log(print_r($arInvoice2,true));
$logger->log("arInvoiceAll=[".print_r($arInvoiceAll,true)."]");
$cNo = 0;
$cSum = 0;
$cNoBitrix = 0;
$cNoBitrix2 = 0;
$cNo1C = 0;
$cSumBitrix = 0;
$cStatus = 0;
$cStatusBitrix = 0;
$cStatus1C = 0;
$cDatePayAll = 0;
$cDatePay = 0;
$cDatePay2 = 0;
$cDel1C = 0;
$cBitrixYes = 0;
$cBitrixNo = 0;
$cUserBitrix = 0;
$cUserBitrix3 = 0;
$cUserBitrix4 = 0;
$cUser4 = 0;
$cUser6 = 0;
$cUser7 = 0;
$arStatus = array(
    'A' => 'Не оплачен', //Ждет оплаты
    'P' => 'Оплачен',
    'N' => 'NULL', //Черновик
    'D' => 'Отменен',
    'S' => 'Не оплачен' //Отправлен клиенту
);
$arUserBitrix = array();
$bUpdateSum = true;//false;
//$fname = $_SERVER["DOCUMENT_ROOT"]."/log/Invoice/schet.csv";
$arFileName = array(
    $_SERVER["DOCUMENT_ROOT"]."/log/Invoice/schet.csv",
//    $_SERVER["DOCUMENT_ROOT"]."/log/Invoice/schet_is.csv",
//    $_SERVER["DOCUMENT_ROOT"]."/log/Invoice/schet_re.csv",
);
foreach ($arFileName as $ifname => $fname) {

    if (($handle = fopen($fname, "r")) !== FALSE) {
    $logger->log(print_r($data, true));
    $row = 0;
    while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
        $row++;
        $status = $data[11];
        if ($status == 'NULL')
        {
            $status = 'Не оплачен';
        }
        $id = $data[7];
        $price = $data[6];
        $price =str_replace(",",".", $price);
        $price = floatval($price);
        $uf_link = $data[14];
        $manager = GetUser($data[10]);
        if ($row == 1)
        {
            continue;
        }
        $logger->log(print_r($data, true));
//        $arFilter = array('ACCOUNT' => $data);
//        $rsInvoice = CCrmInvoice::GetList(array(), $arFilter);
//        if($arInvoice = $rsInvoice->Fetch())
        $number1C = trim($data[0]);
        $year = substr($data[1], 6, 4);
        $logger->log("year=[".print_r($year, true)."]");
        if($year != 2017)
        {
//            continue;
            $number1C = $data[0]." ($year)";
        }
        $date_bill = substr($data[1], 0, 10);
        $pay_date = substr($data[17], 0, 10);
        $pay_before = substr($data[15], 0, 10);
        if(isset($arInvoiceAll[$number1C]))
        {
            $cBitrixYes ++;
            if($arInvoiceAll[$number1C]['DATE_PAY_BEFORE'] != $pay_before)
            {
                $loggerPayBefore->log("00001 bx=[".$arInvoiceAll[$number1C]['DATE_PAY_BEFORE']." 1c=[".$pay_before. "][".$data[16]."] ".$data[11]);
                $loggerPayBefore->log("00001".print_r($arInvoiceAll[$number1C], true));
                $loggerPayBefore->log("00001".print_r($data, true));
                $newFields = array(
                    'DATE_PAY_BEFORE' => $pay_before
                );
                $ret = $CCrmInvoice->Update($arInvoiceAll[$number1C]['ID'], $newFields);
                $loggerPayBefore->log("00001 ret=[".print_r($ret, true)."]");
            }
            if($arInvoiceAll[$number1C]['PAY_VOUCHER_DATE'] != $pay_date)
            {

                $loggerPayDate->log("00001  [$number1C] EX=".$arInvoiceAll[$number1C]['EXTERNAL_ORDER'].
                    "ID=".$arInvoiceAll[$number1C]['ID'].
                    " bx=[".$arInvoiceAll[$number1C]['PAY_VOUCHER_DATE']." 1c=[".$pay_date. "][".$data[16]."] ".$data[11]);
                if($data[11] == "Оплачен" && $pay_date != '')
                {
                    $loggerPayDate2->log("00001 [$number1C] bx=[".$arInvoiceAll[$number1C]['PAY_VOUCHER_DATE']." 1c=[".$pay_date. "][".$data[16]."] ".$data[11]);
                    $statusParams= array(
                        'STATE_SUCCESS' => true,
                        'STATE_FAILED' => false,
                        'PAY_VOUCHER_NUM' => $data[16],
                        'PAY_VOUCHER_DATE' => $pay_date,
                        'DATE_MARKED' => $pay_date,
                        'REASON_MARKED' => ''
                    );
                    $ret = $CCrmInvoice->SetStatus($arInvoiceAll[$number1C]['ID'], 'P', $statusParams);
                    $loggerPayDate2->log("ret=[" . print_r($ret, true) . "]");

                }
                if(($data[11] == 'Не оплачен' || $data[11] == '') && $pay_date == '')
                {
                    $loggerPayDate2->log("00002 [$number1C] EX=".$arInvoiceAll[$number1C]['EXTERNAL_ORDER']."[".$arInvoiceAll[$number1C]['ID']
                        ."] bx=[".$arInvoiceAll[$number1C]['PAY_VOUCHER_DATE']." 1c=[".$pay_date. "][".$data[16]."] ".$data[11]);
//                    $newFields = array(
//                        '
//' => ''
//                    );
//                    $ret = $CCrmInvoice->Update($arInvoiceAll[$number1C]['ID'], $newFields);
//                    $loggerPayDate2->log("00002 ret=[" . print_r($ret, true) . "]");
                }
            }


            if($arInvoiceAll[$number1C]['DATE_BILL'] != $date_bill)
            {
                $loggerBillDate->log("00001 bx=[".$arInvoiceAll[$number1C]['DATE_BILL']." 1c=[".$date_bill);
                $loggerBillDate->log("00001".print_r($arInvoiceAll[$number1C], true));
                $loggerBillDate->log("00001".print_r($data, true));
            }
            $price1C = floatval($arInvoiceAll[$number1C]['UF_CRM_1514204770']);
            $arInvoiceAll[$number1C]['1C'] = 'Y';

            $loggerUser->log("00001".print_r($arInvoiceAll[$number1C], true));
            $loggerUser->log("00001".print_r($data, true));
            $loggerSum->log("00001 [$number1C] price1C=$price1C price=$price UF_CRM_1514204770=[".print_r($arInvoiceAll[$number1C]['UF_CRM_1514204770'], true));

            if($arInvoiceAll[$number1C]['ID'] == $id)
            {
                if($arInvoiceAll[$number1C]['DATE_BILL'] != $date_bill)
                {
                    $loggerBillDate->log("00002 bx=[".$arInvoiceAll[$number1C]['DATE_BILL']." 1c=[".$date_bill);
                    $loggerBillDate->log("00002".print_r($arInvoiceAll[$number1C], true));
                    $loggerBillDate->log("00002".print_r($data, true));
                    $newFields = array(
                        'DATE_BILL' => $date_bill
                    );
                    $ret = $CCrmInvoice->Update($arInvoiceAll[$number1C]['ID'], $newFields);
                    $loggerBillDate->log("00002 ret=[".print_r($ret, true)."]");
                }
                if($price1C != $price)
                {
                    $loggerSumBitrix->log("00001UF_CRM_1514204770=[".print_r($arInvoiceAll[$number1C]['UF_CRM_1514204770'], true));
                    $loggerSumBitrix->log("00001price=[".print_r($price, true));
                    $loggerSumBitrix->log("00001".print_r($arInvoiceAll[$number1C], true));
                    $loggerSumBitrix->log("00001".print_r($data, true));
                    $newFields= array(
                        'UF_CRM_1514204770' => $price
                    );
                    if($bUpdateSum)
                    {
                        $ret = $CCrmInvoice->Update($arInvoiceAll[$number1C]['ID'], $newFields);
                        $loggerSumBitrix->log("00001ret=[".print_r($ret, true)."]");
                    }

                }
                if($arInvoiceAll[$number1C]['RESPONSIBLE_ID'] == 372)
                {
                    $loggerUserBitrix1->log("00001manager=".print_r($manager, true));
                    $loggerUserBitrix1->log("00001".print_r($arInvoiceAll[$number1C], true));
                    $loggerUserBitrix1->log("00001".print_r($data, true));
                    $cUserBitrix++;
                }
                elseif($arInvoiceAll[$number1C]['RESPONSIBLE_ID'] != $manager && $manager == 0 )
                {
                    $loggerUserBitrix2->log("00002manager=".print_r($manager, true));
                    $loggerUserBitrix2->log("00002".print_r($arInvoiceAll[$number1C], true));
                    $loggerUserBitrix2->log("00002".print_r($data, true));
                    $cUserBitrix++;
                    if (isset($arUserBitrix[$arInvoiceAll[$number1C]['FIO']]))
                    {
                        $arUserBitrix[$arInvoiceAll[$number1C]['FIO']]++;
                    }
                    else
                    {
                        $arUserBitrix[$arInvoiceAll[$number1C]['FIO']] = 1;
                    }
                    $newFields = array(
                        'COMMENTS' => $arInvoiceAll[$number1C]['COMMENTS']." Ответственный"
                    );
//                    $ret = $CCrmInvoice->Update($arInvoiceAll[$number1C]['ID'], $newFields);
//                    $loggerUserBitrix2->log("00002 ret=[".print_r($ret, true)."]");

                }
                elseif($arInvoiceAll[$number1C]['RESPONSIBLE_ID'] != $manager )
                {
                    $loggerUserBitrix3->log("00003manager=".print_r($manager, true));
                    $loggerUserBitrix3->log("00003".print_r($arInvoiceAll[$number1C], true));
                    $loggerUserBitrix3->log("00003".print_r($data, true));
                    $cUserBitrix3++;
                    $newFields = array(
                        'COMMENTS' => $arInvoiceAll[$number1C]['COMMENTS']." Ответственный3"
                    );
//                    $ret = $CCrmInvoice->Update($arInvoiceAll[$number1C]['ID'], $newFields);
//                    $loggerUserBitrix3->log("00002 ret=[".print_r($ret, true)."]");
                }
                else
                {
                    $loggerUser->log("00002manager=".print_r($manager, true));
                    $loggerUser->log("00002".print_r($arInvoiceAll[$number1C], true));
                    $loggerUser->log("00002".print_r($data, true));
                    $cUserBitrix4++;
                }
            }elseif($arInvoiceAll[$number1C]['UF_LINK'] == $uf_link)
            {
                if($arInvoiceAll[$number1C]['DATE_BILL'] != $date_bill)
                {
                    $loggerBillDate2->log("00001 bx=[".$arInvoiceAll[$number1C]['DATE_BILL']." 1c=[".$date_bill);
                    $loggerBillDate2->log("00001".print_r($arInvoiceAll[$number1C], true));
                    $loggerBillDate2->log("00001".print_r($data, true));
                    $newFields = array(
                        'DATE_BILL' => $date_bill
                    );
                    $ret = $CCrmInvoice->Update($arInvoiceAll[$number1C]['ID'], $newFields);
                    $loggerBillDate2->log("00001 ret=[".print_r($ret, true)."]");
                }

                if($arInvoiceAll[$number1C]['RESPONSIBLE_ID'] == 372
                && $manager != 372 && $manager != 0 && $arInvoiceAll[$number1C]['EXTERNAL_ORDER'] == 'Y'
                )
                {
                    $loggerUser4->log("00004manager=".print_r($manager, true));
                    $loggerUser4->log("00004".print_r($arInvoiceAll[$number1C], true));
                    $loggerUser4->log("00004".print_r($data, true));
                    $cUser4++;
                    $newFields = array(
                        'RESPONSIBLE_ID' => $manager
                    );
                    $ret = $CCrmInvoice->Update($arInvoiceAll[$number1C]['ID'], $newFields);
                    $loggerUser4->log("00004 ret=[".print_r($ret, true)."]");
                }
                elseif($arInvoiceAll[$number1C]['RESPONSIBLE_ID'] == 372 && $manager == 0
                    && $arInvoiceAll[$number1C]['EXTERNAL_ORDER'] == 'Y' )
                {
                    $loggerUser6->log("00006manager=".print_r($manager, true));
                    $loggerUser6->log("00006".print_r($arInvoiceAll[$number1C], true));
                    $loggerUser6>log("00006".print_r($data, true));
                    $cUser6++;
                    $newFields = array(
                        'RESPONSIBLE_ID' => $manager
                    );
//                    $ret = $CCrmInvoice->Update($arInvoiceAll[$number1C]['ID'], $newFields);
//                    $loggerUser6->log("00006 ret=[".print_r($ret, true)."]");
                }
                elseif($arInvoiceAll[$number1C]['RESPONSIBLE_ID'] != $manager )
                {
                    $loggerUser7->log("00005manager=".print_r($manager, true));
                    $loggerUser7->log("00005".print_r($arInvoiceAll[$number1C], true));
                    $loggerUser7->log("00005".print_r($data, true));
                    $cUser7++;
                }
                else
                {
                    $loggerUser->log("00003manager=".print_r($manager, true));
                    $loggerUser->log("00003".print_r($arInvoiceAll[$number1C], true));
                    $loggerUser->log("00003".print_r($data, true));
                }
                if($price1C != $price)
                {
                    $loggerSum2->log("00002UF_CRM_1514204770".print_r($arInvoiceAll[$number1C]['UF_CRM_1514204770'], true));
                    $loggerSum2->log("00002price".print_r($price, true));
                    $loggerSum2->log("00002".print_r($arInvoiceAll[$number1C], true));
                    $loggerSum2->log("00002".print_r($data, true));
                    $newFields= array(
                        'UF_CRM_1514204770' => $price
                    );
                    if($bUpdateSum)
                    {
                        $ret = $CCrmInvoice->Update($arInvoiceAll[$number1C]['ID'], $newFields);
                        $loggerSum2->log("00002ret=[".print_r($ret, true)."]");
                    }
                }
            }else
            {
                $loggerSumBitrix->log("00003".print_r($arInvoiceAll[$number1C], true));
                $loggerSumBitrix->log("00003".print_r($data, true));
            }
        }
        else
        {
            $cBitrixNo ++;
            $loggerNoBitrixNum->log("[".$data[1]."] $year [".$number1C."] ".$data[4]." ".$data[5]." [".$data[7]."] ".$data[3]);
            $loggerNoBitrix->log("00004".print_r($data, true));
            $cNo++;
            $loggerNo->log(print_r($data, true));
            if($id >0)
            {
                if ($arInvoice = CCrmInvoice::GetByID($id))
                {
                    $loggerNo->log(print_r($arInvoice, true));
                }
                else
                {
                    $loggerNoBitrix->log(print_r($data, true));
                    $loggerNoBitrixJSON->log(json_encode($data));
                    $cNoBitrix ++;
                    if($data[11] != 'Отменен'
                        && $data[11] != 'NULL'
                    )
                    {
                        $loggerNoBitrix2->log(print_r($data, true));
                        $cNoBitrix2 ++;
                    }
                }
//                $loggerNoBitrix->log(print_r($arInvoice, true));
                $loggerNo1C->log("00001".print_r($data, true));
                $cNo1C ++;
            }
            else
            {
                $loggerNo1C->log("00002".print_r($data, true));
                $cNo1C ++;
                $loggerNoJSON->log(json_encode(array('data' => $data, 'invoice' => $arInvoiceAll[$number1C])));

            }
        }
    }
    fclose($handle);
}
}

$arNo1C = array();
$cDelAll = 0;
$cYes;
foreach ($arInvoiceAll as $iInvoice => $vInvoice) {
//    $year = substr($vInvoice['DATE_INSERT'], 6, 4);
    $year = substr($vInvoice['DATE_BILL'], 6, 4);

    if($vInvoice['1C'] != 'Y'
    //&& $year == '2017'
        && $year == '2018'
       && (substr($vInvoice['ACCOUNT_NUMBER'], 0, 4) != '0000')
//            || substr($vInvoice['ACCOUNT_NUMBER'], 0, 2) == 'БИ'
//        )
        && $vInvoice['STATUS_ID'] != 'Y'
//        && strlen($vInvoice['ACCOUNT_NUMBER']) <= 11
    && $vInvoice['DATE_BILL'] != '08.02.2018'
    )
    {
        $arNo1C[$iInvoice] = $vInvoice;
        $cDel1C ++;
    }
    if($vInvoice['1C'] != 'Y')
    {
        $cDelAll++;
    }
    else
    {
        $cYes++;
    }
}

$logger->log("arNo1C=[".print_r($arNo1C,true)."]");
$CCrmInvoice = new CCrmInvoice();
foreach ($arNo1C as $iInvoice => $vInvoice) {
    $loggerDel1C->log(print_r($vInvoice, true));
    if($vInvoice['ID'] == 187)
    {
        continue;
    }
    if(
        $vInvoice['STATUS_ID'] == 'D'
//        || $vInvoice['STATUS_ID'] == 'N'
        || $vInvoice['STATUS_ID'] == 'A'
        || $vInvoice['STATUS_ID'] == 'S'
        || $vInvoice['STATUS_ID'] == 'P'
    )
    {
        $loggerDel1CJSON->log(json_encode($vInvoice));
        $loggerDel1CNum->log($vInvoice['ACCOUNT_NUMBER']." ".$vInvoice['DATE_BILL']
            ." [".$vInvoice['STATUS_ID']."][".$vInvoice['EXTERNAL_ORDER']);

        $statusParams= array(
            'STATE_SUCCESS' => false,
            'STATE_FAILED' => true,
        );
        try {
            $ret = $CCrmInvoice->SetStatus($vInvoice['ID'], 'Y', $statusParams);
//            $loggerDel1C->log("ret=[" . print_r($ret, true) . "]");
        }
        catch (Exception $e) {
            $ErrMSG = $e->getMessage();
            $loggerDel1C->log("ErrMSG=[" . print_r($ErrMSG, true) . "]");
        }
//        break;
    }
}


$logger->log("row=[".print_r($row,true)."]");
$logger->log("cNo=[".print_r($cNo,true)."]");
$logger->log("cNoBitrix=[".print_r($cNoBitrix,true)."]");
$logger->log("cNo1C=[".print_r($cNo1C,true)."]");
$logger->log("cNoBitrix2=[".print_r($cNoBitrix2,true)."]");
//$logger->log("cSum=[".print_r($cSum,true)."]");
//$logger->log("cSumBitrix=[".print_r($cSumBitrix,true)."]");
//$logger->log("cStatus=[".print_r($cStatus,true)."]");
//$logger->log("cStatusBitrix=[".print_r($cStatusBitrix,true)."]");
//$logger->log("cStatus1C=[".print_r($cStatus1C,true)."]");
//$logger->log("cDatePayAll=[".print_r($cDatePayAll,true)."]");
//$logger->log("cDatePay=[".print_r($cDatePay,true)."]");
//$logger->log("cDatePay2=[".print_r($cDatePay2,true)."]");
$logger->log("cDel1C=[".print_r($cDel1C,true)."]");
$logger->log("cDelAll=[".print_r($cDelAll,true)."]");
$logger->log("cYes=[".print_r($cYes,true)."]");
$logger->log("cBitrixYes=[".print_r($cBitrixYes,true)."]");
$logger->log("cBitrixNo=[".print_r($cBitrixNo,true)."]");
$logger->log("cUserBitrix=[".print_r($cUserBitrix,true)."]");
$logger->log("cUserBitrix3=[".print_r($cUserBitrix3,true)."]");
$logger->log("cUserBitrix4=[".print_r($cUserBitrix4,true)."]");
$logger->log("cUser4=[".print_r($cUser4,true)."]");
$logger->log("cUser6=[".print_r($cUser6,true)."]");
$logger->log("cUser7=[".print_r($cUser7,true)."]");
$loggerUserBitrix->log("arUserBitrix=[".print_r($arUserBitrix,true)."]");
function GetUserName($user_id)
{
    $rsUser = CUser::GetByID($user_id);
    $FIO = "";
    if($arUser = $rsUser->Fetch())
    {
        $FIO = $arUser['LAST_NAME']." ".$arUser['NAME']." ".$arUser['SECOND_NAME'];
    }
    return $FIO;
}

function GetUser($fio)
{
    $manager = 0;
    $arFIO = explode(" ", $fio);
    if(count($arFIO) >= 2 )
    {
        $arFilter = array(
            'LAST_NAME' => $arFIO[0],
            'NAME' => $arFIO[1],
        );
        $rsUser = CUser::GetList(($by="personal_country"), ($order="desc"), $arFilter);
        if($arUser = $rsUser->Fetch()) {
            $manager = $arUser['ID'];
        }
    }
    return $manager;
}

















