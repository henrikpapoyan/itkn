<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Отчеты по сущностям Bitrix, BPM, 1с и Прод");
$APPLICATION->SetAdditionalCSS("/reports/assets/css/style.css", true);

$APPLICATION->IncludeComponent("bitrix:breadcrumb", "", array(
    "START_FROM" => 1,
    "SITE_ID" => SITE_ID
));
?>

<table cellpadding="0" cellspacing="0" border="0" class="field-types-table">
    <tbody>
        <tr>
            <td>
                <div class="field-types-name"><a href="company">Компания</a></div>
                <div class="field-types-desc">Список функциональных возможностей для компании</div>
                <div class="field-types-list"><a href="company/get-by-id" class="disabled">Поиск компании по ID</a></div>
                <div class="field-types-list"><a href="company/get-by-inn" class="disabled">Поиск компании по ИНН/КПП</a></div>
                <div class="field-types-list"><a href="company/get-by-guid" class="disabled">Поиск компании по GUID Bpm</a></div>
                <div class="field-types-list"><a href="company/disbalans">Выполнить проверку дисбаланса БД между Bitrix - Bpm</a></div>
                <div class="field-types-list"><a href="company/disbalans-prod">Выполнить проверку дисбаланса БД между Прод и Bitrix</a></div>
				<div class="field-types-list"><a href="company/correctminuskktcount">Исправление отрицального количества ККТ у контрагента</a></div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="field-types-name"><a href="invoice">Счета</a></div>
                <div class="field-types-desc">Список функциональных возможностей для счетов</div>
                <div class="field-types-list"><a href="invoice/check-invoice-balance">Проверка баланса счетов</a></div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="field-types-name"><a href="contact">Контакт</a></div>
                <div class="field-types-desc">Список функциональных возможностей для контактов</div>
                <div class="field-types-list"><a href="contact/get-by-id" class="disabled">Найти контакт по ID</a></div>
                <div class="field-types-list"><a href="contact/get-by-personal-info" class="disabled">Найти контакт по ФИО/Email/Телефон</a></div>
                <div class="field-types-list"><a href="contact/get-by-guid" class="disabled">Найти контакт по GUID Bpm</a></div>
                <div class="field-types-list"><a href="contact/disbalans">Выполнить проверку дисбаланса БД между Bitrix - Bpm</a></div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="field-types-name"><a href="kkt" class="disabled">ККТ</a></div>
                <div class="field-types-desc">Список функциональных возможностей для ККТ</div>
                <div class="field-types-list"><a href="kkt/get-by-id" class="disabled">Найти ККТ по ID</a></div>
                <div class="field-types-list"><a href="kkt/get-by-rnm" class="disabled">Найти ККТ по РНМ</a></div>
                <div class="field-types-list"><a href="kkt/get-by-inn" class="disabled">Найти ККТ по ИНН/КПП</a></div>
                <div class="field-types-list"><a href="kkt/get-by-company" class="disabled">Найти ККТ по ID компании</a></div>
                <div class="field-types-list"><a href="kkt/get-by-deal" class="disabled">Найти ККТ по ID сделки</a></div>
                <div class="field-types-list"><a href="kkt/get-by-guid" class="disabled">Найти ККТ по GUID Bpm</a></div>
                <div class="field-types-list"><a href="kkt/disbalans" class="disabled">Выполнить проверку дисбаланса БД между Bitrix - Bpm</a></div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="field-types-name"><a href="tiket" class="disabled">Обращения</a></div>
                <div class="field-types-desc">Список функциональных возможностей для обращений</div>
                <div class="field-types-list"><a href="tiket/get-by-id" class="disabled">Найти обращение по ID</a></div>
                <div class="field-types-list"><a href="tiket/get-by-number" class="disabled">Найти обращение по № обращения в Servicedesk</a></div>
                <div class="field-types-list"><a href="tiket/get-by-guid" class="disabled">Найти обращение по GUID Bpm</a></div>
                <div class="field-types-list"><a href="tiket/disbalans" class="disabled">Выполнить проверку дисбаланса БД между Bitrix - Bpm</a></div>
            </td>
        </tr>
		<tr>
            <td>
                <div class="field-types-name"><a href="tiket">Postfix</a></div>
                <div class="field-types-desc">Список функциональных возможностей для обращений</div>
                <div class="field-types-list"><a href="postfix/notprocess">Не обработанные события</a></div>
                <div class="field-types-list"><a href="postfix/lastday">Информация о Postfix сообщениях за последние 4 часа</a></div>
            </td>
        </tr>
    </tbody>
</table>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>