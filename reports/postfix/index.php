<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Отчеты по Компаниям Bitrix и BPM");
$APPLICATION->SetAdditionalCSS("/reports/assets/css/style.css", true);

$APPLICATION->IncludeComponent("bitrix:breadcrumb", "", array(
    "START_FROM" => 1,
    "SITE_ID" => SITE_ID
));
?>
    <table cellpadding="0" cellspacing="0" border="0" class="field-types-table">
        <tbody>
		<tr>
            <td>
                <div class="field-types-name"><a href="tiket">Postfix</a></div>
                <div class="field-types-desc">Список функциональных возможностей для обращений</div>
                <div class="field-types-list"><a href="notprocess">Не обработанные события</a></div>
                <div class="field-types-list"><a href="lastday">Информация о Postfix сообщениях за последние 4 часа</a></div>
            </td>
        </tr>
        </tbody>
    </table>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>