<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Информация о Postfix сообщениях за последние 4 часа");
$APPLICATION->SetAdditionalCSS("/reports/assets/css/style.css", true);
$APPLICATION->IncludeComponent("bitrix:breadcrumb", "", array(
    "START_FROM" => 1,
    "SITE_ID" => SITE_ID
));
?>

<?

/**
 * Подключение конфиг файлов и модулей
 * postfixtypes.php - конфиг файл для инициации реализованных postfix типов
 * highloadblock - модуль для работы с highloadblock блоком, в котором записаны Postfix сообщения.
 * */

include_once(__DIR__ . "/../../../local/php_interface/include/postfixtypes.php");
\Bitrix\Main\Loader::includeModule('highloadblock');
use Bitrix\Highloadblock as HL;

/**
 * Инициализируем переменные для работы скрипта.
 * $fromTime - минус 4 часа от текущего времени
 * $toTime - текушее время
 * $HL_Infoblock_ID - ID highloadblock блока, в котором запписаны Postfix сообщения
 * $arFilter - фильтр для уточнения выборки Postfix сообщений использует:
 * 1) фильтр по дате получения (значение до текущего времени) - UF_DATE
 * 2) фильтр по дате получения (значение после -4 часов) - UF_DATE
 * */

$HL_Infoblock_ID = 3; //ID предварительно созданного HL ИБ
$arFilter = array();
$fromTime = date('d.m.Y H:i:s', time() - 4 * 60 * 60); 
$toTime = date("d.m.Y H:i:s", time()); 
$arFilter['>=UF_DATE'] = $fromTime;
$arFilter['<=UF_DATE'] = $toTime;
$arFilter['!UF_EVENT_TYPE'] = "";


/**
 * Выбираем данные по событиям и подсчитываем общее количество сообщений для каждого события
 * с сохренением их в массив $arResultCountMessagePerDay
 * */

$arResultCountMessagePerDay = array();
foreach ($arMessageType as $typeMessage) {
    if (!isset($arResultCountMessagePerDay[$typeMessage])) {
        $arResultCountMessagePerDay[$typeMessage] = 0;
    }
}

$hlblock = HL\HighloadBlockTable::getById($HL_Infoblock_ID)->fetch();
$entity = HL\HighloadBlockTable::compileEntity($hlblock);
$entity_data_class = $entity->getDataClass();
$rsData = $entity_data_class::getList(array(
    'select' => array('ID', 'UF_EVENT_TYPE', 'UF_DATE'),
    'filter' => $arFilter
)); ?>


    <div id="dates" style="position: absolute; right: 80px">
        <table class="customers">
            <tbody>
            <tr>
                <th>Проверка производилась:</th>
                <th><?= $fromTime; ?> (МСК.)</th>
            </tr>
            </tbody>
        </table>
    </div>
    <br><br><br><br><br><br>

<?
while ($el = $rsData->fetch()) {
    if (!isset($arResultCountMessagePerDay[$el['UF_EVENT_TYPE']])) {
        $arResultCountMessagePerDay[$el['UF_EVENT_TYPE']] = 1;
    } else {
        $arResultCountMessagePerDay[$el['UF_EVENT_TYPE']]++;
    }
}
?>


<?
/**
 * Генерация html таблицы для вывода информации на экран
 * */
?>
    <style>
        table.customers {
            width: 550px;
        }
    </style>

    <table class="customers">
        <tbody>
        <tr>
            <th>Наименование события</th>
            <th>количество</th>
        </tr>
        <?
        foreach ($arResultCountMessagePerDay as $index => $elementResultCountMessagePerDay) {
            echo "<tr>";
            if ($elementResultCountMessagePerDay == 0) {
                echo "<td style='color:red'>" . $index . "</td>";
                echo "<td>" . $elementResultCountMessagePerDay . "</td>";
            } else {
                echo "<td>" . $index . "</td>";
                echo "<td>" . $elementResultCountMessagePerDay . "</td>";
            }
            echo "</tr>";
        }
        ?>
        </tbody>
    </table>

    <div>
        <h3>Особенности работы с данным функционалом:</h3>
        <ul>
            <li>Период выборки отчета 4 часа.</li>
            <li><span style='color:red'>Красным цветом</span> помечаются события, количество которых за данный период
                равно 0
            </li>
            <li>При наличии событий за данный период - события выводятся черным цветом</li>
            <li>Данный отчет не учитывается количество обработанных событий Postfix за данный период.</li>
        </ul>
    </div>
    <a href="/reports/postfix/lastday/" class="login-btn">Обновить страницу</a>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>