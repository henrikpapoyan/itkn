<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("События не обработанные в Postfix");
$APPLICATION->SetAdditionalCSS("/reports/assets/css/style.css", true);
$APPLICATION->IncludeComponent("bitrix:breadcrumb", "", array(
    "START_FROM" => 1,
    "SITE_ID" => SITE_ID
));
?>

<?

/**
 * Подключение конфиг файлов и модулей
 * postfixtypes.php - конфиг файл для инициации реализованных postfix типов
 * highloadblock - модуль для работы с highloadblock блоком, в котором записаны Postfix сообщения.
 * */
include_once(__DIR__ . "/../../../local/php_interface/include/postfixtypes.php");
\Bitrix\Main\Loader::includeModule('highloadblock');
use Bitrix\Highloadblock as HL;


/**
 * Инициализируем переменные для работы скрипта.
 * $toTime - хранит дату предыдущего дня
 * $HL_Infoblock_ID - ID highloadblock блока, в котором запписаны Postfix сообщения
 * $arFilter - фильтр для уточнения выборки Postfix сообщений использует:
 * 1) Фильтр по типам событий (значение - реализованные события) - UF_EVENT_TYPE
 * 2) Фильтр по состоянию Обработано(значение - Нет) - UF_PROCESSED
 * 3) фильтр по дате получения (значение до вчерашней даты) - UF_DATE
 * 4) фильтр по состоянию в работе (значение да) - UF_IN_WORK
 * $allcount - переменная для хранения общего количества сообщений необработанных
 * */
$arResultCountMessagePerDay = array();
$HL_Infoblock_ID = 3; //ID предварительно созданного HL ИБ
$arFilter = array();
$toTime = date("d.m.Y", time() - 86400 * 1); //по текущую дату
$arFilter['<=UF_DATE'] = $toTime;
$arFilter['UF_PROCESSED'] = false; //Берем неотработанные письма
$arFilter['!UF_EVENT_TYPE'] = "";
$allcount = 0;


/**
 * Выбираем данные по событиям и подсчитываем общее количество сообщений для каждого события
 * с сохренением их в массив $arResultCountMessagePerDay
 * Для событий, которые есть в массиве $arMessageType проставляем статус Есть реализация.
 * Для событий, которые есть в Postfix, но нет в $arMessageType проставляем статус Нет реализации.
 * */

foreach ($arMessageType as $typeMessage) {
    if (!isset($arResultCountMessagePerDay[$typeMessage])) {
        $arResultCountMessagePerDay[$typeMessage]["VALUE"] = 0;
        $arResultCountMessagePerDay[$typeMessage]["HASREALISATION"] = "Y";
    }
}

$hlblock = HL\HighloadBlockTable::getById($HL_Infoblock_ID)->fetch();
$entity = HL\HighloadBlockTable::compileEntity($hlblock);
$entity_data_class = $entity->getDataClass();
$rsData = $entity_data_class::getList(array(
    'select' => array('UF_EVENT_TYPE'),
    'filter' => $arFilter
));

while ($el = $rsData->fetch()) {
    if (!isset($arResultCountMessagePerDay[$el['UF_EVENT_TYPE']])) {
        $arResultCountMessagePerDay[$el['UF_EVENT_TYPE']]["VALUE"] = 1;
        $arResultCountMessagePerDay[$el['UF_EVENT_TYPE']]["HASREALISATION"] = "N";
        $allcount++;
    } else {
        $arResultCountMessagePerDay[$el['UF_EVENT_TYPE']]["VALUE"]++;
        $allcount++;
    }
}
?>


<?
/**
 * Генерация html таблицы для вывода информации на экран
 * */
?>
    <div id="dates" style="position: absolute; right: 80px">
        <table class="customers">
            <tbody>
            <tr>
                <th>Проверка производилась:</th>
                <th><?= $toTime; ?> (МСК.)</th>
            </tr>
            </tbody>
        </table>
    </div>
    <br><br>
    <style>
        table.customers {
            width: 550px;
        }
    </style>
    <table class="customers">
        <tbody>
        <tr>
            <th>Наименование события</th>
            <th>Количество не обработанных</th>
        </tr>
        <?
        echo "<h2 style='color:black'>Всего не обработанных " . $allcount . "</h2>";
        foreach ($arResultCountMessagePerDay as $index => $elementResultCountMessagePerDay) {
            if ($elementResultCountMessagePerDay["VALUE"] > 0) {
                echo "<tr>";
                if ($elementResultCountMessagePerDay["HASREALISATION"] == "N") {
                    echo "<td style='color:red'>" . $index . "</td>";
                    echo "<td>" . $elementResultCountMessagePerDay["VALUE"] . "</td>";
                } else {
                    echo "<td style='color:green'>" . $index . "</td>";
                    echo "<td>" . $elementResultCountMessagePerDay["VALUE"] . "</td>";
                }
                echo "</tr>";
            }
        }
        ?>
        </tbody>
    </table>
    <div>
        <h3>Особенности работы с данным функционалом:</h3>
        <ul>
            <li>Отчет включает все события за прошлый день, которые не обработаны.</li>
            <li><span style='color:red'>Красный цветом выводятся события, у которых нет внутреннего обработчика.</span>
            </li>
            <li><span style='color:green'>Зеленым  цветом выводятся события, у которых есть обработчик в Bitrix.</span>
            </li>
        </ul>
    </div>
    <a href="/reports/postfix/notprocess/" class="login-btn">Обновить страницу</a>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>