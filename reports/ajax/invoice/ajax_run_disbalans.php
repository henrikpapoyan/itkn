<?php

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
define('CHK_EVENT', true);
ini_set('memory_limit', '-1');

if(intval(date("i")) < 5 || intval(date("i")) > 55){
    $res = ["status" => false, "error" => "Запуск обработчика недоступен за 5 минут до окончания часа и 5 минут в начале часа (например: с 16:55 до 17:05)"];
    echo prepareJsonData($res);
    exit;
}

//Проверка на AJAX запрос
if( !checkAjaxRequest() ) {
    $res = ["status" => false, "error" => "Not ajax request"];
    echo prepareJsonData($res);
    exit;
}

//Проверка на наличие данных
if(empty($_POST)){
    $res = ["status" => false, "error" => "Нет данных для обработки."];
    echo prepareJsonData($res);
    exit;
}

$arInvoices = json_decode($_POST['data'], true);
$categoryInvoice = $_POST['idElement'];

//pre(array('$categoryInvoice' => $categoryInvoice, '$arInvoices' => $arInvoices));

if(empty($categoryInvoice) || empty($categoryInvoice)){
    $res = ["status" => false, "error" => "Нет данных для обработки."];
    echo prepareJsonData($res);
    exit;
}

switch ($categoryInvoice) {
    case "COUNT_INVOICE_NOT_MATCH_MAIN_PARAM":
        $result = Reports::updateInvoicesBitrix($arInvoices, 'updateMainParam');
        break;
    case "COUNT_INVOICE_NOT_MATCH_PRODUCTS_PARAM":
        $result = Reports::updateInvoicesBitrix($arInvoices, 'updateProds');
        break;
    case "COUTN_BITRIX_INVOICE_NO_MATCH_1C":
        $result = Reports::updateInvoicesBitrix($arInvoices, 'updateDateModify');
        break;
    default:
        $res = ["status" => false, "error" => "Для данного параметра [$categoryInvoice] обработка не реализована."];
        echo prepareJsonData($res);
        exit;
        break;
}


if(!empty($result) && $result['status'] == 200) {
    echo prepareJsonData($result);
} else {
    $strError = $result['error'];
    $res = ["status" => false, "error" => "Ошибка актуализации информации для категории [$categoryInvoice]. $strError"];
    echo prepareJsonData($res);
}

function prepareJsonData($data) {
    if(!array_key_exists('status', $data)){
        $data['status'] = true;
    }
    if( $data['status'] == false && !array_key_exists('error', $data) ){
        $data['error'] = "not description error";
    }

    return json_encode($data);
}