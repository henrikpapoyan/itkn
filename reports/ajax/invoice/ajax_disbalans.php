<?php

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
define('CHK_EVENT', true);
ini_set('memory_limit', '-1');

//Проверка на AJAX запрос
if( !checkAjaxRequest() ) {
    $res = ["status" => false, "error" => "Not ajax request"];
    echo prepareJsonData($res);
    exit;
}

if(intval(date("i")) < 5 || intval(date("i")) > 55){
    $res = ["status" => false, "error" => "Запуск обработчика недоступен за 5 минут до окончания часа и 5 минут в начале часа (например: с 16:55 до 17:05)"];
    echo prepareJsonData($res);
    exit;
}

$arResult = [];

//Get all company Bitrix and Bpm
$resBitrixInvoice = Reports::getAllInvoiceBitrix();
$res1cInvoice = Reports::getAllInvoice1с();

//Check data
if( count($resBitrixInvoice) <= 0 ){
    $res = ["status" => false, "error" => "Ошибка запроса получения счетов Битрикс24. Повторите запрос позже."];
    echo prepareJsonData($res);
    exit;
}
if( count($res1cInvoice) <= 0 ){
    $res = ["status" => false, "error" => "Ошибка запроса получения счетов 1С. Повторите запрос позже."];
    echo prepareJsonData($res);
    exit;
}
if($res1cInvoice["status"] === false){
    echo prepareJsonData($res1cInvoice);
    exit;
}

//Compare company Bitrix and Bpm
$arResult = Reports::compareInvoiceFrom1cAndBitrix($resBitrixInvoice, $res1cInvoice);

//Save result to CSV
foreach($arResult as $key => $value){
    if(is_array($value) && sizeOf($value) > 1){
        $res = Reports::prepareDataToSaveCsv($value, true, "/log/reports/invoice/$key.csv");
    }
}

echo prepareJsonData($arResult);

function prepareJsonData($data) {
    if(!array_key_exists('status', $data)){
        $data['status'] = true;
    }
    if( $data['status'] == false && !array_key_exists('error', $data) ){
        $data['error'] = "not description error";
    }

    return json_encode($data);
}