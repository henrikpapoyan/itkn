<?php

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
define('CHK_EVENT', true);
ini_set('memory_limit', '-1');

//Проверка на AJAX запрос
if( !checkAjaxRequest() ) {
    $res = ["status" => false, "error" => "Not ajax request"];
    echo prepareJsonData($res);
    exit;
}

//Проверка на наличие данных
if(empty($_POST)){
    $res = ["status" => false, "error" => "Нет данных для обработки."];
    echo prepareJsonData($res);
    exit;
}

$arCompany = json_decode($_POST['data'], true);
$categoryCompany = $_POST['idElement'];

if(empty($arCompany) || empty($categoryCompany)){
    $res = ["status" => false, "error" => "Нет данных для обработки."];
    echo prepareJsonData($res);
    exit;
}

switch ($categoryCompany) {
      case "BITRIX_NOT_IN_BPM":
          $result = Reports::addContacttoBpm($arCompany);
        break;
    case "BPM_NOT_IN_BITRIX":
        $result = Reports::addContacttoBitrix($arCompany);
        break;
    case "BITRIX_DUBLE_GUID":
        $result = Reports::MergeContactBitrix($arCompany);
        break;
    case "BITRIX_AND_BPM_NOT_SAME_ACCOUNT":
       // $result = Reports::MergeContactBitrix($arCompany);
        break;

    default:
        $res = ["status" => false, "error" => "Для данного параметра [$categoryCompany] обработка не реализована."];
        echo prepareJsonData($res);
        exit;
        break;
}

if(!empty($result) && $result['status'] == 200) {
    echo prepareJsonData($result);
} else {
    $statusError    = "";
    $strError       = "";

    if(!empty($result['status'])){
        $statusError .= "\n\nОшибка от BPM, ErrorStatus: " . $result['status'];
    }
    if(!empty($result['success']['ResponseStatus']['ErrorCode'])){
        $strError .= "\nErrorCode: " . $result['success']['ResponseStatus']['ErrorCode'];
    }
    if(!empty($result['success']['ResponseStatus']['Message'])){
        $strError .= "\nMessage: " . $result['success']['ResponseStatus']['Message'];
    }

    $res = ["status" => false, "error" => "Ошибка актуализации информации для категории [$categoryCompany]. $statusError $strError"];
    echo prepareJsonData($res);
}

function prepareJsonData($data) {
    if(!array_key_exists('status', $data)){
        $data['status'] = true;
    }
    if( $data['status'] == false && !array_key_exists('error', $data) ){
        $data['error'] = "not description error";
    }

    return json_encode($data);
}