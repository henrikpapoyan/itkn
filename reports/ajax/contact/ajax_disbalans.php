<?php

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
define('CHK_EVENT', true);
ini_set('memory_limit', '-1');

//Проверка на AJAX запрос
if( !checkAjaxRequest() ) {
    $res = ["status" => false, "error" => "Not ajax request"];
    echo prepareJsonData($res);
    exit;
}

$arResult = [];

$resBitrix = Reports::getAllContactBitrix();
$resBpm = Reports::getAllContactBpm();

if( count($resBitrix) <= 0 || count($resBpm) <= 0 ){
    $res = ["status" => false, "error" => "Ошибка запроса получения данных из BPM-системы. Повторите запрос позже."];
    echo prepareJsonData($res);
    exit;
}

$arResult = Reports::compareArrayContactBpmAndBitrix($resBitrix, $resBpm);


echo prepareJsonData($arResult);

function prepareJsonData($data) {
    if(!array_key_exists('status', $data)){
        $data['status'] = true;
    }
    if( $data['status'] == false && !array_key_exists('error', $data) ){
        $data['error'] = "not description error";
    }

    return json_encode($data);
}