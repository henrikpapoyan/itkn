<?php

    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
    require_once($_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/Classes/common/CComplianceCompany.php");

    define("NO_KEEP_STATISTIC", true);
    define("NOT_CHECK_PERMISSIONS", true);
    define('CHK_EVENT', true);
    ini_set('memory_limit', '-1');

    $CComplianceCompany = new CComplianceCompany;

//Проверка на AJAX запрос
    if ( ! checkAjaxRequest()) {
        $res = ["status" => false, "error" => "Not ajax request"];
        echo prepareJsonData($res);
        exit;
    }

    if ($_REQUEST["updateAllElements"] === "y") {
        $CComplianceCompany->setStatus($CComplianceCompany::STATUS_UPDATE_ALL_ELEMENTS_ID);
        $res = ["status" => true, "message" => "Обработка запущена"];
        echo prepareJsonData($res);
        exit;
    }

    //Быстрая обработка синхронизации.
    //Предварительно сравниваются GUID, ИНН и КПП, и обрабатываются только те записи,
    //по которым не нашлось соответствия в Б24
    if ($_REQUEST["updateFastElements"] === "y") {
        if($CComplianceCompany->getUploadType() != $CComplianceCompany::ID_FAST_UPLOAD) {
            $rsElements = CIBlockElement::GetList(
                array("SORT" => "ASC"),
                array("IBLOCK_CODE" => "import_company"),
                false,
                false,
                array(
                    "ID",
                    "ACTIVE",
                    "NAME",
                    "PROPERTY_INN",
                    "PROPERTY_KPP",
                    "PROPERTY_ID_PROD",
                )
            );

            $arImportElem = [];
            while ($arElement = $rsElements->GetNext()) {
                $arImportElem[] = $arElement;
            }

            $arrayBitrix = Reports::getAllCompanyBitrixWithTenant();

            //Ищем элементы, которые будут обработаны при запуске быстрой обработки
            foreach ($arImportElem as $arVal) {
                $arCompareProd[$arVal["ID"]] = $arVal["PROPERTY_ID_PROD_VALUE"] . '_' . $arVal["PROPERTY_INN_VALUE"] . '_' . $arVal["PROPERTY_KPP_VALUE"];
            }

            foreach ($arrayBitrix as $arVal) {
                $arCompareBitrix[$arVal["ID"]] = $arVal["ID_PROD"] . '_' . $arVal["INN"] . '_' . $arVal["KPP"];
            }

            $arDifferentCompare = array_diff($arCompareProd, $arCompareBitrix);
            foreach ($arDifferentCompare as $elemProdID => $val) {
                $el->SetPropertyValuesEx(
                    $elemProdID,
                    false,
                    array(
                        "FAST_UPDATE" => getPropertyEnumIdByXmlId(GetIBlockIDByCode('import_company'), 'FAST_UPDATE_LIST_ELEM'),
                    )
                );
            };
        }

        $CComplianceCompany->setStatus($CComplianceCompany::STATUS_FAST_UPDATE_ELEMENTS_ID);
        $res = ["status" => true, "message" => "Быстрая обработка запущена"];
        echo prepareJsonData($res);
        exit;
    }

    if ($_REQUEST["new_upload"] === "y") {
        $CComplianceCompany->setStatus($CComplianceCompany::STATUS_IMPORT_CSV_ID);
        $res = ["status" => true, "message" => "Установлен статус загрузки файла"];
        echo prepareJsonData($res);
        exit;
    }

    if ($_REQUEST["uploadCSV"] === "y") {
        $result = $CComplianceCompany->importCSVFile();

        $res = ["status" => true, "message" => "Загрузка запущена", "result" => $result];
        echo prepareJsonData($res);
        exit;
    }

    if ($_REQUEST["getProgressStatus"] === "y") {
        $progress = $CComplianceCompany->getProgressStatus();

        $res = ["status" => true, "result" => $progress];
        echo prepareJsonData($res);
        exit;
    }

    $arResult = [];

//Get all company Bitrix and Prod
    $resBitrix = Reports::getAllCompanyBitrixWithTenant();
    $resProd   = Reports::getAllCompaniesFromProd();

//Check data
    if (empty($resBitrix) || empty($resProd)) {
        $res = [
            "status" => false,
            "error"  => "Ошибка получения списка компаний.",
        ];
        echo prepareJsonData($res);
        exit;
    }
	

//Compare company Bitrix and Bpm

    $arResult = Reports::compareArrayCompanyProdAndBitrix($resBitrix, $resProd);

//Save result to CSV
    foreach ($arResult as $key => $value) {
        $res = Reports::prepareDataToSaveCsv($value, false, "/log/reports/company/$key.csv");
    }

    echo prepareJsonData($arResult);

    function prepareJsonData($data)
    {
        if ( ! array_key_exists('status', $data)) {
            $data['status'] = true;
        }
        if ($data['status'] == false && ! array_key_exists('error', $data)) {
            $data['error'] = "not description error";
        }

        return json_encode($data);
    }