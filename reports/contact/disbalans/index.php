<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Проверка дисбаланса БД между Bitrix - Bpm.Контакты");
$APPLICATION->SetAdditionalCSS("/reports/assets/css/style.css", true);

$APPLICATION->IncludeComponent("bitrix:breadcrumb", "", array(
    "START_FROM" => 1,
    "SITE_ID" => SITE_ID
));

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
define('CHK_EVENT', true);
ini_set("memory_limit", "64M");
?>

    <div>
        <input type="submit" id="runCheck" value="Запустить проверку" class="login-btn">
        <div id="timeLimit" class="visible-off">
            <span style="float: right;color: red;font-weight: bold;">Время жизни данных, сек.: <span id="timeValue"></span></span>
        </div>
        </span></span>
    </div>

    <br>
    <span id="notification" class="visible-off">
Запрос на проверку отправлен. Пожалуйста ожидайте .... <br>
Время обработки занимает до 5-ти минут.
</span>

    <span id="error" class="error visible-off"></span>

    <!-- HTML block with result -->
    <div id="result" class="visible-off">
        <h2>Результат обработки:</h2>

        <table class="customers">
            <tbody>
            <tr>
                <th>Параметр</th>
                <th>Система</th>
                <th>Значение</th>
                <th>Действие</th>
                <th>Описание действия</th>
                <th>CSV</th>
            </tr>

            <tr id="COUNT_CONTACT_BITRIX">
                <td>Общее кол-во контактов в Bitrix</td>
                <td></td>
                <td></td>
                <td></td>
                <td> - </td>
                <td style="text-align:center;">-</td>
            </tr>

            <tr id="COUNT_CONTACT_BPM">
                <td>Общее кол-во контактов в BPM</td>
                <td></td>
                <td></td>
                <td></td>
                <td> - </td>
                <td style="text-align:center;">-</td>
            </tr>

            <tr id="COUNT_FULL_COPY_BITRIX_BPM">
                <td>Одинаковых контактов в BPM и Bitrix</td>
                <td></td>
                <td></td>
                <td></td>
                <td> - </td>
                <td style="text-align:center;">-</td>
            </tr>




            <tr id="BITRIX_NOT_IN_BPM">
                <td>
                    Количество контактов, которые есть в Bitrix, но нету в BPM<br>
                </td>
                <td></td>
                <td></td>
                <td>
                    <a href="#runCheck" data-name="actual" class="visible-off">Актуализировать</a>
                </td>
                <td>
                </td>
                <td style="text-align:center;">
                </td>
            </tr>


            <tr id="BPM_NOT_IN_BITRIX">
                <td>Количество контактов, которые есть в BPM, но нету в Bitrix</td>
                <td></td>
                <td></td>
                <td>
                    <a href="#runCheck" data-name="actual" class="visible-off">Актуализировать</a>
                </td>
                <td>

                </td>
                <td style="text-align:center;">
                </td>
            </tr>


            <tr id="BITRIX_DUBLE_GUID">
                <td>Количество GUID, по которым есть дубликаты контактов в Битриксе</td>
                <td></td>
                <td></td>
                <td></td>
                <td> - </td>
                <td style="text-align:center;">
                </td>
            </tr>

            <tr id="BITRIX_AND_BPM_NOT_SAME_ACCOUNT">
                <td>Количество контактов c разными контагентами </td>
                <td></td>
                <td></td>
                <td></td>
                <td> - </td>
                <td style="text-align:center;">
                  </td>
            </tr>


            </tbody></table>
    </div>

    <div>
        <h3>Особенности работы с данным функционалом:</h3>
        <ul>
            <li>Рекомендуется перед балансировкой выполнить бэкап БД Битрикс и BPM.</li>
            <li>Не рекомендуется запускать балансировку баз в рабочее время.</li>
            <li>Идеальное время для запуска балансировки БД: с 18:00 до 08:00</li>
            <li>Идеальное время запуска в рамках часа: с 0 до 30 минуты каждого часа</li>
            <li>
                При объемном дисбалансе БД выполнение актуализации может занимать до 10-ти минут.<br>
                Не стоит закрывать страницу раньше времени, дождитесь окончания процесса.</li>
            <li>
                Во время актуализации БД портал будет не доступен для текущей сессии(браузера).<br>
                Для доступа достаточно открыть портал в другом браузере.
            </li>
        </ul>
    </div>

    <script>
        $( document ).ready(function() {

            //Запуск проверки по дисбалансу БД
            $("#runCheck").click(function () {
                if($("#runCheck").hasClass("wait") === true){
                    return;
                }

                disableForm();

                $.ajax({
                    type: "GET", //Метод отправки
                    url: "/reports/ajax/contact/ajax_disbalans.php",
                    success: function(data){
                        $('#notification').addClass('visible-off');
                        if(data == ""){
                            $('#error').removeClass('visible-off');
                            $("#runCheck").removeClass('wait');
                            $("#runCheck").val('Запустить повторно');
                            $('#error').text('Ошибка запроса получения данных от сервера. Повторите запрос позже.');
                            return;
                        }

                        var result = JSON.parse(data);
                        console.log(result);

                        enableForm();

                        //Обработка ошибок
                        if(!result.status) {
                            $('#error').removeClass('visible-off');
                            $('#error').text('Произошла ошибка: ' + result.error);
                            return;
                        } else {
                            $("#result").removeClass('visible-off');
                        }

                        showResult('COUNT_CONTACT_BITRIX', "Bitrix", result.COUNT_CONTACT_BITRIX);
                        showResult('COUNT_CONTACT_BPM', "Bpm", result.COUNT_CONTACT_BPM);
                        showResult('COUNT_FULL_COPY_BITRIX_BPM', "Bitrix/Bpm", result.COUNT_FULL_COPY_BITRIX_BPM);
                        showResult('BITRIX_NOT_IN_BPM', "Bitrix/Bpm", Object.keys(result.BITRIX_NOT_IN_BPM).length, true, result.BITRIX_NOT_IN_BPM);
                        showResult('BPM_NOT_IN_BITRIX', "Bitrix/Bpm", Object.keys(result.BPM_NOT_IN_BITRIX).length, true, result.BPM_NOT_IN_BITRIX);
                        showResult('BITRIX_DUBLE_GUID', "Bitrix", Object.keys(result.BITRIX_DUBLE_GUID).length);
                        showResult('BITRIX_AND_BPM_NOT_SAME_ACCOUNT', "Bitrix/Bpm",result.BITRIX_AND_BPM_NOT_SAME_ACCOUNT);
                       }
                });
            });


            function showResult(idElement, system, count, action = ' - ', arData = "") {
                $('#' + idElement + ' > td:eq(1)').text(system);
                $('#' + idElement + ' > td:eq(2)').text(getNumberBeautifulFormat(count));

                if(action === true && count > 0){
                    $('#' + idElement + ' > td:eq(3) a').removeClass('visible-off');
                    $('#' + idElement + ' > td:eq(3) a').on( "click", function() {
                        var isRun = confirm("Вы уверены что хотите это сделать?\n\nПосле запуска страница будет заблокирована, ожидайте пожалуйста выполнения операции...\n\nПримечание: после запуска данные не обратимы!");
                        if(isRun === true){
                            actualData(idElement, arData);
                            $(this).text("Готово");
                            $(this).addClass("disable-link");
                        }
                    });
                } else {
                    if(action === true){
                        $('#' + idElement + ' > td:eq(3)').text(" - ");
                    } else{
                        $('#' + idElement + ' > td:eq(3)').text(action);
                    }
                }
            }

            function actualData(idElement, arData){
                //Отправка синхронного запроса
                $.ajax({
                    async: false,
                    type: "POST", //Метод отправки
                    url: "/reports/ajax/contact/ajax_run_disbalans.php",
                    dataType: "json",
                    data: {'idElement':idElement,'data':JSON.stringify(arData)},

                    success: function(data){
                        console.log(data);
                        if(data.status === true || data.status == 200){
                            var strRes = "Актуализация завершена успешно!\n\nОтвет от сервера: \n";
                            for (var k in data){
                                if (data.hasOwnProperty(k)) {
                                    strRes += k + ": " + data[k] + "\n";
                                }
                            }
                            alert(strRes);
                        } else {
                            alert("Ошибка актуализации: " + data.error);
                        }
                    }
                });
            }

            function getNumberBeautifulFormat(data) {
                var price       = Number.prototype.toFixed.call(parseFloat(data) || 0, 0),
                    price_sep   = price.replace(/(\D)/g, ","),
                    price_sep   = price_sep.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1 ");

                return price_sep;
            };

            function enableForm() {
                $("#runCheck").removeClass('wait');
                $("#runCheck").val('Запустить повторно');
            }

            function disableForm() {
                $('#runCheck').addClass('wait');
                $('#notification').removeClass('visible-off');
                $("#result").addClass('visible-off');
                $('#error').addClass('visible-off');
                $("a[data-name=actual]").each(function(){
                    $(this).off('click');
                    $(this).text("Актуализировать");
                    $(this).removeClass('disable-link');
                });
            }

        });
    </script>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>