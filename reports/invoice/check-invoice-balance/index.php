<?
    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
    $APPLICATION->SetTitle("Проверка баланса счетов между 1c - Bitrix");
    $APPLICATION->SetAdditionalCSS("/reports/assets/css/style.css", true);

    $APPLICATION->IncludeComponent("bitrix:breadcrumb", "", array(
        "START_FROM" => 1,
        "SITE_ID"    => SITE_ID
    ));
?>
<?
/*$resBitrixInvoice = Reports::getAllInvoiceBitrix();
foreach($resBitrixInvoice as $arInvoice){
    pre(array('$arInvoice' => $arInvoice));
}*/
?>

<div>
    <?if(intval(date("i")) > 5 && intval(date("i"))<55):?>
        <input type="submit" id="runCheck" value="Запустить проверку" class="login-btn">
    <?else:?>
        <p>Запуск обработчика недоступен за 5 минут до окончания часа и 5 минут в начале часа (например: с 16:55 до 17:05)</p>
    <?endif;?>
    <div id="timeLimit" class="visible-off">
        <span style="float: right;color: red;font-weight: bold;">Время жизни данных, сек.: <span id="timeValue"></span></span>
    </div>
    </span></span>
</div>

<br>
<span id="notification" class="visible-off">
Запрос на проверку отправлен. Пожалуйста ожидайте .... <br>
Время обработки занимает до 5-ти минут.
</span>

<span id="error" class="error visible-off"></span>

<div id="result_block">

</div>

    <!-- HTML block with result -->
    <div id="result" class="visible-off">
        <h2>Результат обработки:</h2>

        <table class="customers">
            <tbody>
            <tr>
                <th>#</th>
                <th>Параметр</th>
                <th>Система</th>
                <th>Значение</th>
                <th>Действие</th>
                <th>Описание действия</th>
                <th>CSV</th>
            </tr>

            <tr id="COUNT_INVOICE_BITRIX">
                <td>1.</td>
                <td>Общее количество счетов в Bitrix</td>
                <td>Bitrix</td>
                <td></td>
                <td> - </td>
                <td>Справочная информация</td>
                <td style="text-align:center;">-</td>
            </tr>

            <tr id="COUTN_BITRIX_INVOICE_NO_MATCH_1C">
                <td>2.</td>
                <td>Кол-во счетов, которые есть в Б24, но нет в 1С</td>
                <td>Б24</td>
                <td></td>
                <td>
                    <a href="#runCheck" data-name="actual" class="visible-off">Актуализировать</a>
                </td>
                <td>
                    Пересохранение счета на стороне Битрикса, после которого данный счет попадет в выгрузку 1С.<br /><br />
                    Под данную статистику не попадают счета Б24 в статусе "Черновик", “Удален” и "Отменен"<br /><br />
                    За одну "Актуализацию" происходит обновление данных по 50 счетам.
                </td>
                <td style="text-align:center;">
                    <a href="/log/reports/invoice/COUTN_BITRIX_INVOICE_NO_MATCH_1C.csv"><img src="/reports/assets/images/csv-icon.png" width="32" height="32"></a>
                </td>
            </tr>

            <tr id="COUTN_BITRIX_INVOICE_NO_MATCH_1C_IN_STATUS_DRAFT">
                <td>3.</td>
                <td>Кол-во счетов в статусе "Черновик", которые есть в Б24, но нет в 1С</td>
                <td>Б24</td>
                <td></td>
                <td> - </td>
                <td>
                    Действие не предусмотрено, т.к. данные счета находятся в статусе "Черновик"
                </td>
                <td style="text-align:center;">
                    -
                </td>
            </tr>

            <tr id="COUTN_BITRIX_INVOICE_NO_MATCH_1C_IN_STATUS_DEL">
                <td>4.</td>
                <td>Кол-во счетов в статусе "Удален", которые есть в Б24, но нет в 1С</td>
                <td>Б24</td>
                <td></td>
                <td> - </td>
                <td>
                    Действие не предусмотрено, т.к. данные счета находятся в статусе "Удален"
                </td>
                <td style="text-align:center;">
                    -
                </td>
            </tr>

            <tr id="COUTN_BITRIX_INVOICE_NO_MATCH_1C_IN_STATUS_CANCELED">
                <td>5.</td>
                <td>Кол-во счетов в статусе "Отменен", которые есть в Б24, но нет в 1С</td>
                <td>Б24</td>
                <td></td>
                <td> - </td>
                <td>
                    Действие не предусмотрено, т.к. данные счета находятся в статусе "Отменен"
                </td>
                <td style="text-align:center;">
                    -
                </td>
            </tr>

            <tr id="COUNT_INVOICE_1C">
                <td>6.</td>
                <td>Общее количество счетов в 1C</td>
                <td>1C</td>
                <td></td>
                <td> - </td>
                <td>Справочная информация</td>
                <td style="text-align:center;">-</td>
            </tr>

            <tr id="COUTN_1C_INVOICE_NO_MATCH_BITRIX">
                <td>7.</td>
                <td>
                    Кол-во счетов, которые есть в 1С, но нет в Б24
                </td>
                <td>1С</td>
                <td></td>
                <td> - </td>
                <td>Действие не предусмотрено, т.к. для передачи данного счета в Битрикс – необходимо сохранить его в 1С. Возможности это сделать через сайт нет.</td>
                <td style="text-align:center;">
                    <a href="/log/reports/invoice/COUTN_1C_INVOICE_NO_MATCH_BITRIX.csv"><img src="/reports/assets/images/csv-icon.png" width="32" height="32"></a>
                </td>
            </tr>

            <tr id="COUNT_FULL_COPY">
                <td>8.</td>
                <td>Одинаковых счетов в Битриксе и 1С</td>
                <td>Б24/1C</td>
                <td></td>
                <td> - </td>
                <td>Справочная информация</td>
                <td style="text-align:center;">-</td>
            </tr>

            <tr id="COUNT_INVOICE_NOT_MATCH_MAIN_PARAM">
                <td>9.</td>
                <td>Кол-во счетов, которые различаются по значениям полей. (номер счета совпадает)</td>
                <td>Б24/1C</td>
                <td></td>
                <td>
                    <a href="#runCheck" data-name="actual" class="visible-off">Актуализировать</a>
                </td>
                <td>
                    Поля счета для сравнения:
                    <ul>
                        <li>Статус счета</li>
                        <li>Сумма счета (производим сравнение с полем «Сумма счета из 1С»)</li>
                        <li>Дата выставления счета</li>
                        <li>Срок оплаты</li>
                        <li>Дата оплаты</li>
                    </ul>
                    За одну "Актуализацию" происходит обновление данных по 50 счетам.<br /><br />
                    Среднее время актуализации 3 минуты.
                </td>
                <td style="text-align:center;">
                    <a href="/log/reports/invoice/COUNT_INVOICE_NOT_MATCH_MAIN_PARAM.csv"><img src="/reports/assets/images/csv-icon.png" width="32" height="32"></a>
                </td>
            </tr>

            <tr id="COUNT_INVOICE_NOT_MATCH_PRODUCTS_PARAM">
                <td>10.</td>
                <td>Кол-во счетов, которые совпадают по номеру счета и основным параметрам, но различаются по значениям товарных позиций</td>
                <td>Б24/1C</td>
                <td></td>
                <td>
                    <a href="#runCheck" data-name="actual" class="visible-off">Актуализировать</a>
                </td>
                <td>
                    Поля товарных позиций для сравнения:
                    <ul>
                        <li>Наименование товара</li>
                        <li>Стоимость</li>
                        <li>Кол-во</li>
                        <li>Ставка НДС</li>
                        <li>Сумма НДС</li>
                    </ul>
                    За одну "Актуализацию" происходит обновление данных по 50 счетам.<br /><br />
                    Среднее время актуализации 3 минуты.
                </td>
                <td style="text-align:center;">
                    <a href="/log/reports/invoice/COUNT_INVOICE_NOT_MATCH_PRODUCTS_PARAM.csv"><img src="/reports/assets/images/csv-icon.png" width="32" height="32"></a>
                </td>
            </tr>
            </tbody>
        </table>
    </div>

<div>
    <h3>Особенности работы с данным функционалом:</h3>
    <ul>
        <li>1. При актуализации счетов в Битрикс24 обновляются только те счета, дата изменения которых менее текущего часа. Т.к. данные по счетам из 1с - менее текущего часа.</li>
        <li>2. Запуск обработчика недоступен за 5 минут до окончания часа и 5 минут в начале часа (например: с 16:55 до 17:05)</li>
        <li>3. За одну "Актуализацию" происходит обновление данных по 50 счетам.</li>
        <li>4. В таблице отображаются счета, которые привязаны к компании ЭСК. (в поле "Реквизиты вашей компании" счета выбрана компания "АО "ЭСК")</li>
    </ul>
</div>

<script>
    $( document ).ready(function() {
        //Запуск проверки по дисбалансу БД
        $("#runCheck").click(function () {
            if($("#runCheck").hasClass("wait") === true){
                return;
            }
            $('#result_block').html('');

            disableForm();

            $.ajax({
                type: "GET", //Метод отправки
                url: "/reports/ajax/invoice/ajax_disbalans.php",
                error: function(){
                    alert('Ошибка при получение данных по счетам');
                    enableForm();
                },
                success: function(data){
                    enableForm();

                    $('#notification').addClass('visible-off');
                    if(data == ""){
                        $('#error').removeClass('visible-off');
                        $("#runCheck").removeClass('wait');
                        $("#runCheck").val('Запустить повторно');
                        $('#error').text('Ошибка запроса получения данных от сервера. Повторите запрос позже.');
                        return;
                    }

                    var result = JSON.parse(data);

                    enableForm();

                    //Обработка ошибок
                    if(!result.status) {
                        $('#error').removeClass('visible-off');
                        if(Array.isArray(result.error)){
                            arError = result.error;
                            errorText = '';
                            for (var k in arError){
                                if (arError.hasOwnProperty(k)) {
                                    errorText += arError[k] + "<br />";
                                }
                            }
                            $('#error').html(errorText);
                        } else {
                            $('#error').html("Произошла ошибка: <br />" + result.error);
                        }
                        return;
                    } else {
                        $("#result").removeClass('visible-off');
                    }

                    showResult('COUNT_INVOICE_BITRIX', "Б24", result.COUNT_INVOICE_BITRIX);
                    showResult('COUTN_BITRIX_INVOICE_NO_MATCH_1C', "Б24", Object.keys(result.COUTN_BITRIX_INVOICE_NO_MATCH_1C).length, true, result.COUTN_BITRIX_INVOICE_NO_MATCH_1C);
                    showResult('COUTN_BITRIX_INVOICE_NO_MATCH_1C_IN_STATUS_DEL', "Б24", Object.keys(result.COUTN_BITRIX_INVOICE_NO_MATCH_1C_IN_STATUS_DEL).length, true, result.COUTN_BITRIX_INVOICE_NO_MATCH_1C_IN_STATUS_DEL);
                    showResult('COUTN_BITRIX_INVOICE_NO_MATCH_1C_IN_STATUS_CANCELED', "Б24", Object.keys(result.COUTN_BITRIX_INVOICE_NO_MATCH_1C_IN_STATUS_CANCELED).length, true, result.COUTN_BITRIX_INVOICE_NO_MATCH_1C_IN_STATUS_CANCELED);
                    showResult('COUTN_BITRIX_INVOICE_NO_MATCH_1C_IN_STATUS_DRAFT', "Б24", Object.keys(result.COUTN_BITRIX_INVOICE_NO_MATCH_1C_IN_STATUS_DRAFT).length, true, result.COUTN_BITRIX_INVOICE_NO_MATCH_1C_IN_STATUS_DRAFT);
                    showResult('COUNT_INVOICE_1C', "1C", result.COUNT_INVOICE_1C);
                    showResult('COUTN_1C_INVOICE_NO_MATCH_BITRIX', "1C", Object.keys(result.COUTN_1C_INVOICE_NO_MATCH_BITRIX).length, true, result.COUTN_1C_INVOICE_NO_MATCH_BITRIX);
                    showResult('COUNT_FULL_COPY', "Б24/1C", result.COUNT_FULL_COPY);
                    showResult('COUNT_INVOICE_NOT_MATCH_MAIN_PARAM', "Б24/1C", Object.keys(result.COUNT_INVOICE_NOT_MATCH_MAIN_PARAM).length/2, true, result.COUNT_INVOICE_NOT_MATCH_MAIN_PARAM);
                    showResult('COUNT_INVOICE_NOT_MATCH_PRODUCTS_PARAM', "Б24/1C", Object.keys(result.INVOICE_ID_NOT_MATCH_PRODUCTS_PARAM).length, true, result.COUNT_INVOICE_NOT_MATCH_PRODUCTS_PARAM);
                }
            });
        });


        function showResult(idElement, system, count, action = ' - ', arData = "") {
            $('#' + idElement + ' > td:eq(2)').text(system);
            $('#' + idElement + ' > td:eq(3)').text(getNumberBeautifulFormat(count));

            if(action === true && count > 0){
                $('#' + idElement + ' > td:eq(4) a').removeClass('visible-off');
                $('#' + idElement + ' > td:eq(4) a').on( "click", function() {
                    var isRun = confirm("Вы уверены что хотите это сделать?\n\nПосле запуска страница будет заблокирована, ожидайте пожалуйста выполнения операции...\n\nПримечание: после запуска данные не обратимы!");
                    if(isRun === true){
                        actualData(idElement, arData);
                        $(this).text("Готово");
                        $(this).addClass("disable-link");
                    }
                });
            } else {
                if(action === true){
                    $('#' + idElement + ' > td:eq(4)').text(" - ");
                } else{
                    $('#' + idElement + ' > td:eq(4)').text(action);
                }
            }
        }

        /*function actualData(idElement, arData){
            $('#updateInvoice').modal('show');

            $.each(arData, function(index, arInvoice){
                console.log('arInvoice = ', arInvoice);
            });
        }*/

        function actualData(idElement, arData){
            console.log('idElement = ', idElement);
            console.log('arData = ', arData);
            //Отправка синхронного запроса
            $.ajax({
                async: false,
                type: "POST", //Метод отправки
                url: "/reports/ajax/invoice/ajax_run_disbalans.php",
                dataType: "json",
                data: {'idElement':idElement,'data':JSON.stringify(arData)},
                error: function(){
                    alert('Ошибка при актуализации данных!');
                },
                success: function(data){
                    //console.log(data);
                    if(data.status === true || data.status == 200){
                        var strRes = "Актуализация завершена успешно! \n\n";
                        strRes += "Успешно обновлено счетов: " + data.updateElemCount + "\n";
                        strRes += "Кол-во счетов, которые небыли обновлены из-за 'изменения' в текущем часу: " + data.countBorder;
                        alert(strRes);
                    } else {
                        alert("Ошибка актуализации: " + data.error);
                    }
                }
            });
        }

        function getNumberBeautifulFormat(data) {
            var price       = Number.prototype.toFixed.call(parseFloat(data) || 0, 0),
                price_sep   = price.replace(/(\D)/g, ","),
                price_sep   = price_sep.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1 ");

            return price_sep;
        };

        function enableForm() {
            $("#runCheck").removeClass('wait');
            $("#runCheck").val('Запустить повторно');
        }

        function disableForm() {
            $('#runCheck').addClass('wait');
            $('#notification').removeClass('visible-off');
            $("#result").addClass('visible-off');
            $('#error').addClass('visible-off');
            $("a[data-name=actual]").each(function(){
                $(this).off('click');
                $(this).text("Актуализировать");
                $(this).removeClass('disable-link');
            });
        }

    });
</script>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>