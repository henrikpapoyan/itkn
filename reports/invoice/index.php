<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Список функциональных возможностей для счетов");
$APPLICATION->SetAdditionalCSS("/reports/assets/css/style.css", true);

$APPLICATION->IncludeComponent("bitrix:breadcrumb", "", array(
    "START_FROM" => 1,
    "SITE_ID" => SITE_ID
));
?>
    <table cellpadding="0" cellspacing="0" border="0" class="field-types-table">
        <tbody>
        <tr>
            <td>
                <div class="field-types-name"><a href="#">Счета</a></div>
                <div class="field-types-desc">Список функциональных возможностей для счетов</div>
                <div class="field-types-list"><a href="check-invoice-balance">Проверка баланса счетов</a></div>
            </td>
        </tr>
        </tbody>
    </table>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>