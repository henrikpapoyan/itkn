<?
    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
    require_once($_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/Classes/common/CComplianceCompany.php");
    global $APPLICATION;
    $APPLICATION->SetTitle("Проверка дисбаланса между Прод - Bitrix");
    $APPLICATION->SetAdditionalCSS("/reports/assets/css/style.css", true);

    $APPLICATION->IncludeComponent("bitrix:breadcrumb", "", array(
        "START_FROM" => 1,
        "SITE_ID"    => SITE_ID
    ));
    $CComplianceCompany     = new CComplianceCompany;
    $currentOperationStatus = $CComplianceCompany->getCurrentStatus();

    define("IMPORT_DELAY_MESSAGE", "Выполняется импорт файла, пожалуйста подождите.");
    define("DELETING_DELAY_MESSAGE", "Выполняется удаление старых элементов, пожалуйста подождите.");
    define("UPLOADING_TO_IBLOCK_DELAY_MESSAGE", "Выполняется загрузка данных из CSV-файла в БД, пожалуйста подождите.<br />Обработано: #READY_PERCENT#");
    define("UPDATING_DELAY_MESSAGE", "Выполняется обработка, пожалуйста подождите.");
    define("ERROR_NO_FILE_FOR_UPLOAD", "Выберите файл для загрузки.");
    define("ERROR_EMPTY_DATE_UPLOADED_FILE", "Заполните дату формирования файла.");
    define("AJAX_SUBMIT_URL", "/reports/ajax/company/ajax_disbalans_prod.php");

    $progressStatusString = "<br><br><div id='progress-notification' style='color: green'>Выполняется запрос статуса обработки...</div><script> var runProgressStatus = true;</script>";

    switch ($currentOperationStatus) {
        case $CComplianceCompany::STATUS_IMPORT_CSV_IN_PROGRESS_VALUE:
            $notification = IMPORT_DELAY_MESSAGE;
            break;
        case $CComplianceCompany::STATUS_DELETE_OLD_ELEMENTS_VALUE:
        case $CComplianceCompany::STATUS_DELETE_OLD_ELEMENTS_IN_PROGRESS_VALUE:
            $notification = DELETING_DELAY_MESSAGE;
            break;
        case $CComplianceCompany::STATUS_PARSE_CSV_VALUE:
        case $CComplianceCompany::STATUS_PARSE_CSV_IN_PROGRESS_VALUE:
            $MESS["UPLOADING_TO_IBLOCK_DELAY_MESSAGE"] = UPLOADING_TO_IBLOCK_DELAY_MESSAGE;
            if($CComplianceCompany->getUploadType() == $CComplianceCompany::ID_FAST_UPLOAD){
                //echo "Выбрана быстрая загрузка<br/ >";
                $START_TIME = mktime();
                $arrayBitrix = Reports::getAllCompanyBitrixWithTenant();
                foreach($arrayBitrix as $arVal) {
                    $arCompareBitrix[$arVal["ID_PROD"].'_'.$arVal["INN"].'_'.$arVal["KPP"]] = $arVal["ID"];
                }
                $TIME = (mktime() - $START_TIME);
                //echo "\n\nВремя  сбора данных о компаниях из Битрикса , сек: $TIME <br />";
                if (($handle = fopen($_SERVER["DOCUMENT_ROOT"] . $CComplianceCompany::PATH_TO_UPLOAD_FILE, "r")) !== false) {
                    $START_TIME = mktime();
                    while (($data = fgetcsv($handle, 0, "\t")) !== false) {
                        $searchCombination = $data[0] . '_' . $data[1] . '_' . $data[2];
                        if (!isset($arCompareBitrix[$searchCombination])) {
                            $fastUpdateAdd++;
                        }
                    }
                    $TIME = (mktime() - $START_TIME);
                    //echo "\n\nВремя обработки сбора данных с файла , сек: $TIME <br />";
                    $rsData = CIBlockElement::GetList(
                        array("ID" => "DESC"),
                        array("IBLOCK_ID" => getIblockIDByCode($CComplianceCompany::COMPLIANCE_IBLOCK_CODE)),
                        false,
                        false,
                        array()
                    );
                    $elementsCount = $rsData->SelectedRowsCount();
                    $readyPercent = round($elementsCount/($fastUpdateAdd/100), 2);
                }
            } else {
                $readyPercent = $CComplianceCompany->getImportReadyPercent();
            }
            $notification = GetMessage("UPLOADING_TO_IBLOCK_DELAY_MESSAGE", array("#READY_PERCENT#" => $readyPercent.'%'));
            break;
        case $CComplianceCompany::STATUS_UPDATE_ALL_ELEMENTS_VALUE:
        case $CComplianceCompany::STATUS_UPDATE_ALL_ELEMENTS_IN_PROGRESS_VALUE:
        case $CComplianceCompany::STATUS_FAST_UPDATE_ELEMENTS_VALUE:
        case $CComplianceCompany::STATUS_FAST_UPDATE_ELEMENTS_IN_PROGRESS_VALUE:
            $notification = UPDATING_DELAY_MESSAGE . $progressStatusString;
            break;
        default:
            $notification = "";
            break;
    }

    $obEnum = new \CUserFieldEnum;
    $arDownloadType = [];
    $rsEnum = $obEnum->GetList(array(), array("USER_FIELD_NAME" => "UF_UPLOAD_TYPE"));
    while($arEnum = $rsEnum->Fetch()){
        $arDownloadType[$arEnum["ID"]] = $arEnum["VALUE"];
    }

?>
    <div id="dates" style="position: absolute; right: 80px">
        <table class="customers">
            <tbody>
            <tr>
                <th>Дата полной обработки всех элементов:</th>
                <th><?=$CComplianceCompany->getLastUpdateDate()?></th>
            </tr>
            <tr>
                <th>Дата формирования файла:</th>
                <th><?=$CComplianceCompany->getLastFileDate()?></th>
            </tr>
            <tr>
                <th>"Тип загрузки":</th>
                <th><?=$arDownloadType[$CComplianceCompany->getUploadType()]?></th>
            </tr>
            </tbody>
        </table>
    </div>


    <span id="notification" style="color: red"><?=$notification?></span>

    <div id="upload-file-form" class="visible-<?=$currentOperationStatus ===
                                                 $CComplianceCompany::STATUS_IMPORT_CSV_VALUE ? "on" : "off"?>">
        <form action="" method="post" enctype=multipart/form-data>
            <label>
                <input type="file" name="<?=$CComplianceCompany::UPLOAD_INPUT_NAME?>" accept=".csv"
                       onchange="getFileInputInfo();" id="uploaded-file" required hidden>
                <span class="login-btn">Открыть файл</span>
            </label>
            <br>
            <br>
            <small>Максимальный размер файла 500Мб.</small>
            <br>
            <br>
            <span id="fileName"></span>
            <br>
            <br>
            <label for="<?=$CComplianceCompany::UF_PROD_FILE_DATE?>">Дата формирования файла:</label>
            <input type="date" name="<?=$CComplianceCompany::UF_PROD_FILE_DATE?>" class="hasDatepicker"
                   id="date-uploaded-file" required>
            <br>
            <br>
            <label for="<?=$CComplianceCompany::UF_UPLOAD_TYPE?>">Тип загрузки:</label>
            <select name="<?=$CComplianceCompany::UF_UPLOAD_TYPE?>" id="<?=$CComplianceCompany::UF_UPLOAD_TYPE?>">
                <?foreach($arDownloadType as $key => $value):?>
                    <option value="<?=$key?>"><?=$value?></option>
                <?endforeach;?>
            </select>
            <br>
            <br>
            <input type="submit" id="ajax-input" value="Загрузить" class="login-btn">
        </form>
    </div>

    <br>

    <div id="actions" class="visible-<?=$currentOperationStatus ===
                                        $CComplianceCompany::STATUS_IMPORT_CSV_COMPLETE_VALUE ? "on" : "off"?>">
        <input type="button" id="re-upload-csv" value="Повторно загрузить файл" class="login-btn">
        <br>
        <br>
        <input type="button" id="disbalans-prod" value="Запустить проверку баланса" class="login-btn">
        <br>
        <br>
        <input type="button" id="disbalans-update-begin" value="Выполнить обработку" class="login-btn">
        <br>
        <br>
        <input type="button" id="disbalans-fast-update-begin" value="Выполнить быструю обработку" class="login-btn">
        <br>
        <br>
    </div>

    <!-- HTML block with result -->
    <div id="result-table" class="visible-off">
        <h2>Результат обработки:</h2>

        <table class="customers">
            <tbody>
            <tr>
                <th>Параметр</th>
                <th>Система</th>
                <th>Значение</th>
                <th>Действие</th>
                <th>Описание действия</th>
                <th>CSV</th>
            </tr>

            <tr id="COUNT_COMPANY_BITRIX">
                <td>Общее кол-во компаний в Bitrix</td>
                <td>Bitrix</td>
                <td></td>
                <td></td>
                <td> -</td>
                <td style="text-align:center;">-</td>
            </tr>

            <tr id="COUNT_COMPANY_PROD">
                <td>Общее кол-во компаний в ПРОДе</td>
                <td>Прод</td>
                <td></td>
                <td></td>
                <td> -</td>
                <td style="text-align:center;">-</td>
            </tr>

            <tr id="COUNT_FULL_COPY_BITRIX_PROD">
                <td>Одинаковых компаний в Bitrix и ПРОД</td>
                <td>Bitrix/Прод</td>
                <td></td>
                <td></td>
                <td>Компания считается одинаковой, если совпадает ИНН/КПП, и тенант.
                </td>
                <td style="text-align:center;">-</td>
            </tr>

            <tr id="PROD_BITRIX_DIFF_INN_OR_KPP">
                <td>Кол-во компаний, которые отличаются по ИНН/КПП.</td>
                <td>Bitrix/Прод</td>
                <td></td>
                <td>
                    <!--                    <a href="#runCheck" data-name="actual" class="visible-off">Актуализировать</a>-->
                </td>
                <td>
                    -
                </td>
                <td style="text-align:center;">
                    <a href="/log/reports/company/PROD_BITRIX_DIFF_INN_OR_KPP.csv"><img
                                src="/reports/assets/images/csv-icon.png" width="32" height="32"></a>
                </td>
            </tr>

            <tr id="PROD_BITRIX_DIFF_TENANT_OR_ASSIGNED">
                <td>
                    Кол-во компаний, которые отличаются по полю Тенант.
                </td>
                <td>Bitrix/Прод</td>
                <td></td>
                <td>
                    <!--                    <a href="#runCheck" data-name="actual" class="visible-off">Актуализировать</a>-->
                </td>
                <td>
                    Cравнение по тенанту из прода 9(временный пользователь) - НЕ ПРОИЗВОДИТСЯ.
                </td>
                <td style="text-align:center;">
                    <a href="/log/reports/company/PROD_BITRIX_DIFF_TENANT_OR_ASSIGNED.csv"><img
                                src="/reports/assets/images/csv-icon.png" width="32" height="32"></a>
                </td>
            </tr>

            <tr id="FAST_UPDATE_ELEMENTS">
                <td>
                    Кол-во компаний из прод, которые будут обработаны при запуске быстрой обработки
                </td>
                <td>Прод</td>
                <td></td>
                <td>

                </td>
                <td>

                </td>
                <td style="text-align:center;">
                    <a href="/log/reports/company/FAST_UPDATE_ELEMENTS.csv"><img
                                src="/reports/assets/images/csv-icon.png" width="32" height="32"></a>
                </td>
            </tr>
            </tbody>
        </table>
    </div>

    <div>
        <h3>Особенности работы с данным функционалом:</h3>
        <ul>
            <li style="color: red">Внимание! Загружаемый CSV файл должен иметь кодировку UTF-8 и разделитель символ
                табуляции.
            </li>
            <li>Рекомендуется перед полной балансировкой выполнить бэкап БД Битрикс.</li>
            <li>
                Не рекомендуется запускать балансировку баз в рабочее время. <br>
                Обработка выполняется в 10-ть потоков, запуск в рабочее время скажется на скорости работы портала.
            </li>
            <li>Идеальное время для запуска балансировки БД: в пятницу в 18:00</li>
            <li>Удаление старых элементов занимает около 2 часов.</li>
            <li>Загрузка данных из CSV-файл в БД занимает около 2 часов.</li>
            <li>Полная обработка всех элементов занимает от 10 часов.</li>

            <li><span style="color: red">NEW!!</span></li>
            <li>Если при загрузке файла выбрать тип загрузки "Быстрая обработка", то импортируются только те позиции из файла, по которым не нашлось совпадений в Б24 по трем параметрам ID ПРОДа, ИНН и КПП. <b>После такой загрузки кнопки "Выполнить обработку" и "Выполнить быструю обработку" выполняют одинаковый фукнционал</b></li>
            <li>При нажатии на кнопку "Выполнить быструю обработку", обрабатываются только те элементы из ПРОДа, по которым не нашлось совпадений в Б24 по трем параметрам ID ПРОДа, ИНН и КПП</li>
        </ul>

        <h3>Инструкция импорта CSV файла</h3>
        <ol>
            <li>Открыть CSV файл с помощью программы Notepad++</li>
            <li>Отрыть меню "Кодировки"</li>
            <li>Выбрать пункт "Преобразовать в UTF-8 без BOM. (см. скриншот)</li>
            <li>Сохранить изменённый файл</li>
        </ol>
        <img src="/images/FaqConvert.jpg">
    </div>

    <script>
        var files;
        var fileDate;
        $(document).ready(function () {
            getFileInputInfo();
            if (typeof runProgressStatus !== "undefined") {
                if (runProgressStatus) {
                    getProgressStatus();
                }
            }

            $('input[type=file]').on('change', function () {
                files = this.files;
            });

            $('input[name="<?=$CComplianceCompany::UF_PROD_FILE_DATE?>"]').on('change', function () {
                fileDate = $(this).val();
            });

            $('#ajax-input').on('click', function (event) {

                event.stopPropagation();
                event.preventDefault();

                if (document.getElementById('uploaded-file').value === "") {
                    $("#notification").html("<?=ERROR_NO_FILE_FOR_UPLOAD?>");
                    return;
                }
                if (document.getElementById('date-uploaded-file').value === "") {
                    $("#notification").html("<?=ERROR_EMPTY_DATE_UPLOADED_FILE?>");
                    return;
                }

                $('#ajax-input').addClass("wait");

                if (typeof files == 'undefined') return;

                var data = new FormData();

                data.append("<?=$CComplianceCompany::UF_UPLOAD_TYPE?>", $("select#<?=$CComplianceCompany::UF_UPLOAD_TYPE?>").val());

                $.each(files, function (key, value) {
                    data.append("<?=$CComplianceCompany::UPLOAD_INPUT_NAME?>", value);
                });

                data.append("<?=$CComplianceCompany::UF_PROD_FILE_DATE?>", fileDate);

                $.ajax({
                    url: '<?=AJAX_SUBMIT_URL?>?uploadCSV=y',
                    type: 'POST',
                    data: data,
                    cache: false,
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        $('#ajax-input').removeClass("wait");
                        $("#upload-file-form").removeClass("visible-on").addClass("visible-off");
                        $("#notification").html('<?=IMPORT_DELAY_MESSAGE?>');
                    }
                });
            });


            $('#re-upload-csv').on('click', function (event) {
                $.ajax({
                    type: 'GET',
                    url: '<?=AJAX_SUBMIT_URL?>?new_upload=y',
                    success: function (data) {
                        var result = JSON.parse(data);
                        if (result.status) {
                            $("#upload-file-form").removeClass("visible-off").addClass("visible-on");
                            $("#actions").removeClass("visible-on").addClass("visible-off");
                            $("#result-table").removeClass("visible-on").addClass("visible-off");
                        }
                    }
                });
            });

            $("#disbalans-prod").click(function (event) {
                $("#disbalans-prod").addClass("wait");
                $("#re-upload-csv").prop('disabled', true).addClass("login-btn-deactivate");
                $("#disbalans-update-begin").prop('disabled', true).addClass("login-btn-deactivate");
                $("#disbalans-fast-update-begin").prop('disabled', true).addClass("login-btn-deactivate");
                $.ajax({
                    type: 'GET',
                    url: "<?=AJAX_SUBMIT_URL?>",
                    success: function (data) {
                        $("#disbalans-prod").removeClass("wait");
                        var result = JSON.parse(data);
                        fillResultTable(result);
                        $("#result-table").removeClass("visible-off").addClass("visible-on");
                        $("#re-upload-csv").prop('disabled', false).removeClass("login-btn-deactivate");
                        $("#disbalans-update-begin").prop('disabled', false).removeClass("login-btn-deactivate");
                        $("#disbalans-fast-update-begin").prop('disabled', false).removeClass("login-btn-deactivate");
                    }
                });
            });

            $("#disbalans-update-begin").click(function (event) {
                $.ajax({
                    type: 'GET',
                    url: '<?=AJAX_SUBMIT_URL?>?updateAllElements=y',
                    success: function (data) {
                        var result = JSON.parse(data);
                        if (result.status) {
                            $("#actions").removeClass("visible-on").addClass("visible-off");
                            $("#result-table").removeClass("visible-on").addClass("visible-off");
                            $("#notification").html('<?=UPDATING_DELAY_MESSAGE?>');
                        }
                    }
                });
            });

            $("#disbalans-fast-update-begin").click(function (event) {
                $.ajax({
                    type: 'GET',
                    url: '<?=AJAX_SUBMIT_URL?>?updateFastElements=y',
                    success: function (data) {
                        var result = JSON.parse(data);
                        if (result.status) {
                            $("#actions").removeClass("visible-on").addClass("visible-off");
                            $("#result-table").removeClass("visible-on").addClass("visible-off");
                            $("#notification").html('<?=UPDATING_DELAY_MESSAGE?>');
                        }
                    }
                });
            });
        });

        function fillResultTable(result) {
            $("#COUNT_COMPANY_BITRIX td:eq(2)").html(result.COUNT_COMPANY_BITRIX);
            $("#COUNT_COMPANY_PROD td:eq(2)").html(result.COUNT_COMPANY_PROD);
            $("#COUNT_FULL_COPY_BITRIX_PROD td:eq(2)").html(result.COUNT_FULL_COPY_BITRIX_PROD);
            $("#PROD_BITRIX_DIFF_INN_OR_KPP td:eq(2)").html(Object.keys(result.PROD_BITRIX_DIFF_INN_OR_KPP).length - 1);
            $("#PROD_BITRIX_DIFF_TENANT_OR_ASSIGNED td:eq(2)").html(Object.keys(result.PROD_BITRIX_DIFF_TENANT_OR_ASSIGNED).length - 1);
            $("#FAST_UPDATE_ELEMENTS td:eq(2)").html(Object.keys(result.FAST_UPDATE_ELEMENTS).length - 1);
        }

        function getFileInputInfo() {
            var file = document.getElementById('uploaded-file').value;
            if (file.length > 0) {
                var file_name = file.split('\\').pop();
                $("#fileName").html("Файл: " + file_name);
            }
        }

        function getProgressStatus() {
            $.ajax({
                url: '<?=AJAX_SUBMIT_URL?>?getProgressStatus=y',
                type: 'GET',
                success: function (data) {
                    var result = JSON.parse(data);
                    var progressMessage = "Обработано: " + result.result.ready + " из " + (result.result.ready + result.result.new) + " элементов."
                    $("#progress-notification").html(progressMessage);
                }
            });
        }
    </script>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>