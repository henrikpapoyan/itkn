<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Контрагенты с некорректным количеством ККТ(отрицательным)");
$APPLICATION->SetAdditionalCSS("/reports/assets/css/style.css", true);

$APPLICATION->IncludeComponent("bitrix:breadcrumb", "", array(
    "START_FROM" => 1,
    "SITE_ID" => SITE_ID
));
?>

<?
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('CHK_EVENT', true);
@set_time_limit(0);
@ignore_user_abort(true);
$res = CCrmCompany::GetList(array(), array("UF_KKT"=>"-%","CHECK_PERMISSIONS" => "N"));
while ($arRes = $res->Fetch())
{
    $arSelectKKT = Array("*");
    $arFilterKKT = Array("IBLOCK_ID"=>"76","PROPERTY_COMPANY"=>$arRes['ID']);
    $resKKT = CIBlockElement::GetList(array(), $arFilterKKT, false, false, $arSelectKKT);
	$resultcountKKT=$resKKT->SelectedRowsCount();
    echo "Контрагент с некорректным количеством ККТ(отрицательным):".$arRes['ID'].".Исправлено его количество на ".$resultcountKKT;echo "<br>";
    SetUserField("CRM_COMPANY", $arRes['ID'], "UF_KKT",$resultcountKKT );
}


?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>