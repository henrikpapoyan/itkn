<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Отчеты по Компаниям Bitrix и BPM");
$APPLICATION->SetAdditionalCSS("/reports/assets/css/style.css", true);

$APPLICATION->IncludeComponent("bitrix:breadcrumb", "", array(
    "START_FROM" => 1,
    "SITE_ID" => SITE_ID
));
?>
    <table cellpadding="0" cellspacing="0" border="0" class="field-types-table">
        <tbody>
        <tr>
            <td>
                <div class="field-types-name"><a href="#">Компания</a></div>
                <div class="field-types-desc">Список функциональных возможностей для компании</div>
                <div class="field-types-list"><a href="get-by-id">Поиск компании по ID</a></div>
                <div class="field-types-list"><a href="get-by-inn">Поиск компании по ИНН/КПП</a></div>
                <div class="field-types-list"><a href="get-by-guid">Поиск компании по GUID Bpm</a></div>
                <div class="field-types-list"><a href="disbalans">Выполнить проверку дисбаланса БД между Bitrix - Bpm</a></div>
                <div class="field-types-list"><a href="disbalans-prod">Выполнить проверку дисбаланса БД между Прод и Bitrix</a></div>
            </td>
        </tr>
        </tbody>
    </table>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>