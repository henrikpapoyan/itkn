<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Проверка дисбаланса БД между Bitrix - Bpm");
$APPLICATION->SetAdditionalCSS("/reports/assets/css/style.css", true);

$APPLICATION->IncludeComponent("bitrix:breadcrumb", "", array(
    "START_FROM" => 1,
    "SITE_ID" => SITE_ID
));
?>

<div>
    <input type="submit" id="runCheck" value="Запустить проверку" class="login-btn">
    <div id="timeLimit" class="visible-off">
        <span style="float: right;color: red;font-weight: bold;">Время жизни данных, сек.: <span id="timeValue"></span></span>
    </div>
</span></span>
</div>

<br>
<span id="notification" class="visible-off">
Запрос на проверку отправлен. Пожалуйста ожидайте .... <br>
Время обработки занимает до 5-ти минут.
</span>

<span id="error" class="error visible-off"></span>

<!-- HTML block with result -->
<div id="result" class="visible-off">
    <h2>Результат обработки:</h2>

    <table class="customers">
        <tbody>
            <tr>
                <th>Параметр</th>
                <th>Система</th>
                <th>Значение</th>
                <th>Действие</th>
                <th>Описание действия</th>
                <th>CSV</th>
            </tr>

            <tr id="COUNT_COMPANY_BITRIX">
                <td>Общее кол-во компаний в Bitrix</td>
                <td></td>
                <td></td>
                <td></td>
                <td> - </td>
                <td style="text-align:center;">-</td>
            </tr>

            <tr id="COUNT_COMPANY_BPM">
                <td>Общее кол-во компаний в BPM</td>
                <td></td>
                <td></td>
                <td></td>
                <td> - </td>
                <td style="text-align:center;">-</td>
            </tr>

            <tr id="COUNT_FULL_COPY_BITRIX_BPM">
                <td>Одинаковых компаний в BPM и Bitrix</td>
                <td></td>
                <td></td>
                <td></td>
                <td>Компании считаются одинаковые, если совпадают: GUID, INN, KPP</td>
                <td style="text-align:center;">-</td>
            </tr>

            <tr id="BPM_BITRIX_NOT_INN_KPP_BPM">
                <td>Количество компаний, которые есть в BPM и Bitrix.<br>У компаний в BPM отсутствует ИНН/КПП</td>
                <td></td>
                <td></td>
                <td>
                    <a href="#runCheck" data-name="actual" class="visible-off">Актуализировать</a>
                </td>
                <td>
                    Обновление компаний на стороне BPM.<br>
                    Обновляются реквизиты ИНН/КПП. Соответствие осуществляется по GUID.<br>
                    <em>
                        Примечание: Если после актуализации остались элементы, то обновление компании на стороне BPM произошло с ошибкой.<br>
                        Пожалуйста, скачайте CSV файл и проведите анализ компании в Битриксе на валидность ИНН/КПП
                    </em>
                </td>
                <td style="text-align:center;">
                    <a href="/log/reports/company/BPM_BITRIX_NOT_INN_KPP_BPM.csv"><img src="/reports/assets/images/csv-icon.png" width="32" height="32"></a>
                </td>
            </tr>

            <tr id="BPM_BITRIX_NOT_INN_KPP_BITRIX">
                <td>Количество компаний, которые есть в BPM и Bitrix.<br>У компаний в Bitrix отсутствует ИНН/КПП</td>
                <td></td>
                <td></td>
                <td>
                    <a href="#runCheck" data-name="actual" class="visible-off">Актуализировать</a>
                </td>
                <td>
                    Обновление компаний на стороне Bitrix.<br>
                    Сценарий работы:<br>
                    <ul>
                        <li>Поиск существующих реквизитов(компании) по BPM-реквизитами ИНН/КПП.</li>
                        <li>Реквизиты(компания) найдены. Завершаем работу.</li>
                        <li>Реквизиты НЕ найдены. Добавление/обновление реквизитов у компании в Bitrix.</li>
                    </ul>
                    Обновляются реквизиты ИНН/КПП. Соответствие осуществляется по GUID.
                </td>
                <td style="text-align:center;">
                    <a href="/log/reports/company/BPM_BITRIX_NOT_INN_KPP_BITRIX.csv"><img src="/reports/assets/images/csv-icon.png" width="32" height="32"></a>
                </td>
            </tr>

            <tr id="BITRIX_NOT_IN_BPM">
                <td>
                    Количество компаний, которые есть в Bitrix, но нету в BPM<br>
                    У компании есть ИНН/КПП.
                </td>
                <td></td>
                <td></td>
                <td>
                    <a href="#runCheck" data-name="actual" class="visible-off">Актуализировать</a>
                </td>
                <td>
                    Добавление компаний в BPM.
                    <br>Генерируется GUID, отправляется запрос на добавление компании в BPM.<br>
					<em>Ограничения: за один запрос происходит обработка 100 элементов, время обработки до 10 минут. Для полной обработки всех элементов требуется повторно запускать актуализацию.</em>
                </td>
                <td style="text-align:center;">
                    <a href="/log/reports/company/BITRIX_NOT_IN_BPM.csv"><img src="/reports/assets/images/csv-icon.png" width="32" height="32"></a>
                </td>
            </tr>

            <tr id="BITRIX_NOT_IN_BPM_NOT_INN">
                <td>
                    Количество компаний, которые есть в Bitrix, но нету в BPM<br>
                    У компании нету ИНН/КПП.
                </td>
                <td></td>
                <td></td>
                <td></td>
                <td>
                    Компанию без ИНН/КПП в BPM не добавляем.
                </td>
                <td style="text-align:center;">
                    <a href="/log/reports/company/BITRIX_NOT_IN_BPM_NOT_INN.csv"><img src="/reports/assets/images/csv-icon.png" width="32" height="32"></a>
                </td>
            </tr>

            <tr id="BPM_NOT_IN_BITRIX">
                <td>Количество компаний, которые есть в BPM, но нету в Bitrix</td>
                <td></td>
                <td></td>
                <td>
                    <a href="#runCheck" data-name="actual" class="visible-off">Актуализировать</a>
                </td>
                <td>
                    <em>ПОДУМАТЬ! Bitrix - мастер система. Возможно не стоит делать обноление BPM -> Bitrix.</em>
                    <br><br>Добавление компаний в Bitrix.
                    <br>Если есть ИНН/КПП,то добавляем компанию в Битриксе. Получение доп. информации из налоговой. Создание реквизитов.
                    <br>Если реквизиты отсутствует, то добавление в Битрикс не происходит.
                </td>
                <td style="text-align:center;">
                    <a href="/log/reports/company/BPM_NOT_IN_BITRIX.csv"><img src="/reports/assets/images/csv-icon.png" width="32" height="32"></a>
                </td>
            </tr>

            <tr id="BITRIX_NOT_GUID_ID">
                <td>Количество компаний в Bitrix без GUID</td>
                <td></td>
                <td></td>
                <td>
                    <a href="#runCheck" data-name="actual" class="visible-off">Актуализировать</a>
                </td>
                <td>
                    Обновление компании в Bitrix и BPM.
                    Сценарий работы:<br>
                    <ul>
                        <li>Если нету ИНН/КПП. Обновляем компанию в Bitrix, проставляем GUID. Создаем компанию в BPM без реквизитов.</li>
                        <li>Есть ИНН/КПП. Выполнить поиск компании по ИНН/КПП в BPM.</li>
                        <li>Если компания найдена. Обновляем компанию в Bitrix, проставляем GUID</li>
                        <li>Если компания НЕ найдена. Обновляем компанию в Bitrix, проставляем GUID. Создаем компанию в BPM</li>
                    </ul>
                </td>
                <td style="text-align:center;">
                    <a href="/log/reports/company/BITRIX_NOT_GUID_ID.csv"><img src="/reports/assets/images/csv-icon.png" width="32" height="32"></a>
                </td>
            </tr>

            <tr id="BITRIX_DUBLE_GUID">
                <td>Количество GUID, по которым есть дубликаты компаний в Битриксе</td>
                <td></td>
                <td></td>
                <td></td>
                <td> - </td>
                <td style="text-align:center;">
                    <a href="/log/reports/company/BITRIX_DUBLE_GUID.csv"><img src="/reports/assets/images/csv-icon.png" width="32" height="32"></a>
                </td>
            </tr>

            <tr id="BITRIX_INN_10_NOT_KPP">
                <td>Количество компаний «Организация(инн=10)» без КПП </td>
                <td></td>
                <td></td>
                <td></td>
                <td> - </td>
                <td style="text-align:center;">
                    <a href="/log/reports/company/BITRIX_INN_10_NOT_KPP.csv"><img src="/reports/assets/images/csv-icon.png" width="32" height="32"></a>
                </td>
            </tr>

            <tr id="BITRIX_INN_NOT_VALID">
                <td>Количество компаний с неккоректной длиной ИНН (не равно 10,12)</td>
                <td></td>
                <td></td>
                <td></td>
                <td> - </td>
                <td style="text-align:center;">
                    <a href="/log/reports/company/BITRIX_INN_NOT_VALID.csv"><img src="/reports/assets/images/csv-icon.png" width="32" height="32"></a>
                </td>
            </tr>

            <tr id="BITRIX_REQUISITE_MORE_ONE">
                <td>Количество компаний, которые имеют более 1-го реквизита</td>
                <td></td>
                <td></td>
                <td>
                    <a href="#runCheck" data-name="actual" class="visible-off">Актуализировать</a>
                </td>
                <td>
                    Чистка неактуальных реквизитов у компаний в Bitrix.<br>
                    Сценарий очистки компаний имеющих 2 и более реквизита:
                    <ul>
                        <li>
                            Все реквизиты имеют одинаковый ИНН/КПП. Merge всех реквизитов в один. Главный - первый в списке (самый старый).<br>
                            Проверяем наличие счетов у неактуальных реквизитов.<br>
                            - Счет есть. Обновляем счет и привязываем к счету главный реквизит. Удаляем неактуальный реквизит.<br>
                            - Счета нету. Удаляем неактуальный реквизит.
                        </li>
                        <li>
                            Реквизиты имеют разный ИНН/КПП. У данной компании оставляем актуальный реквизит, который первый в списке (самый старый).<br>
                            Сценарий обработки неактуальных реквизитов:<br>
                            - По ИНН/КПП неактуального реквизита выполняем поиск компании.<br>
                            - - Компания найдена.<br>
                            - - - Проверяем наличие счета по данному реквизиту.<br>
                            - - - - Счет есть. Обновляем счет и привязываем к нему найденную компанию/реквизит. Удаляем неактуальный реквизит.<br>
                            - - - - Счета нету. Удаляем неактуальный реквизит.<br>
                            - - Компания не найдена. Создаем компанию по существующим ИНН/КПП. Информацию о компании/реквизите получаем из не актуального реквизита.<br>
                            - - - Проверяем наличие счета по данному реквизиту.<br>
                            - - - - Счет есть. Обновляем счет и привязываем к нему созданную компанию/реквизит. Удаляем неактуальный реквизит.<br>
                            - - - - Счета нету. Удаляем неактуальный реквизит.
                        </li>
                    </ul>
                    <em>Ограничения: за один запрос происходит обработка 200 элементов, время обработки 5-10 минут. Для полной обработки всех элементов требуется повторно запускать актуализацию.</em>
                    <em>Примечачние: после каждой обработки необходимо в ручную пересохранить счета, указанные в CSV файле.</em>
                </td>
                <td style="text-align:center;">
                    <a href="/log/reports/company/BITRIX_REQUISITE_MORE_ONE.csv"><img src="/reports/assets/images/csv-icon.png" width="32" height="32"></a>
					<a href="/log/reports/company/InvoiceManualProcessing.csv"><img src="/reports/assets/images/csv-icon.png" width="32" height="32"></a>
                </td>
            </tr>
        </tbody></table>
</div>

<div>
    <h3>Особенности работы с данным функционалом:</h3>
    <ul>
        <li>Рекомендуется перед балансировкой выполнить бэкап БД Битрикс и BPM.</li>
        <li>Не рекомендуется запускать балансировку баз в рабочее время.</li>
        <li>Идеальное время для запуска балансировки БД: с 18:00 до 08:00</li>
        <li>Идеальное время запуска в рамках часа: с 0 до 30 минуты каждого часа</li>
        <li>
            При объемном дисбалансе БД выполнение актуализации может занимать до 10-ти минут.<br>
            Не стоит закрывать страницу раньше времени, дождитесь окончания процесса.</li>
        <li>
            Во время актуализации БД портал будет не доступен для текущей сессии(браузера).<br>
            Для доступа достаточно открыть портал в другом браузере.
        </li>
    </ul>
</div>

    <script>
        $( document ).ready(function() {

            //Запуск проверки по дисбалансу БД
            $("#runCheck").click(function () {
                if($("#runCheck").hasClass("wait") === true){
                    return;
                }

                disableForm();

                $.ajax({
                    type: "GET", //Метод отправки
                    url: "/reports/ajax/company/ajax_disbalans.php",
                    success: function(data){
                        $('#notification').addClass('visible-off');
                        if(data == ""){
                            $('#error').removeClass('visible-off');
                            $("#runCheck").removeClass('wait');
                            $("#runCheck").val('Запустить повторно');
                            $('#error').text('Ошибка запроса получения данных от сервера. Повторите запрос позже.');
                            return;
                        }

                        var result = JSON.parse(data);
                        console.log(result);

                        enableForm();

                        //Обработка ошибок
                        if(!result.status) {
                            $('#error').removeClass('visible-off');
                            $('#error').text('Произошла ошибка: ' + result.error);
                            return;
                        } else {
                            $("#result").removeClass('visible-off');
                        }

                        showResult('COUNT_COMPANY_BITRIX', "Bitrix", result.COUNT_COMPANY_BITRIX);
                        showResult('COUNT_COMPANY_BPM', "Bpm", result.COUNT_COMPANY_BPM);
                        showResult('COUNT_FULL_COPY_BITRIX_BPM', "Bitrix/Bpm", result.COUNT_FULL_COPY_BITRIX_BPM);

                        showResult('BPM_BITRIX_NOT_INN_KPP_BPM', "Bitrix/Bpm", Object.keys(result.BPM_BITRIX_NOT_INN_KPP_BPM).length, true, result.BPM_BITRIX_NOT_INN_KPP_BPM);
                        showResult('BPM_BITRIX_NOT_INN_KPP_BITRIX', "Bitrix/Bpm", Object.keys(result.BPM_BITRIX_NOT_INN_KPP_BITRIX).length, true, result.BPM_BITRIX_NOT_INN_KPP_BITRIX);

                        showResult('BITRIX_NOT_IN_BPM', "Bitrix/Bpm", Object.keys(result.BITRIX_NOT_IN_BPM).length, true, result.BITRIX_NOT_IN_BPM);
                        showResult('BITRIX_NOT_IN_BPM_NOT_INN', "Bitrix", Object.keys(result.BITRIX_NOT_IN_BPM_NOT_INN).length);

                        showResult('BPM_NOT_IN_BITRIX', "Bitrix/Bpm", Object.keys(result.BPM_NOT_IN_BITRIX).length, "На паузе", result.BPM_NOT_IN_BITRIX);
                        showResult('BITRIX_NOT_GUID_ID', "Bitrix", Object.keys(result.BITRIX_NOT_GUID_ID).length, "Ручная обработка", result.BITRIX_NOT_GUID_ID);

                        showResult('BITRIX_DUBLE_GUID', "Bitrix", Object.keys(result.BITRIX_DUBLE_GUID).length);
                        showResult('BITRIX_INN_10_NOT_KPP', "Bitrix", Object.keys(result.BITRIX_INN_10_NOT_KPP).length);
                        showResult('BITRIX_INN_NOT_VALID', "Bitrix", Object.keys(result.BITRIX_INN_NOT_VALID).length);

                        showResult('BITRIX_REQUISITE_MORE_ONE', "Bitrix", Object.keys(result.BITRIX_REQUISITE_MORE_ONE).length, true, result.BITRIX_REQUISITE_MORE_ONE);
                    }
                });
            });


            function showResult(idElement, system, count, action = ' - ', arData = "") {
                $('#' + idElement + ' > td:eq(1)').text(system);
                $('#' + idElement + ' > td:eq(2)').text(getNumberBeautifulFormat(count));

                if(action === true && count > 0){
                    $('#' + idElement + ' > td:eq(3) a').removeClass('visible-off');
                    $('#' + idElement + ' > td:eq(3) a').on( "click", function() {
                        var isRun = confirm("Вы уверены что хотите это сделать?\n\nПосле запуска страница будет заблокирована, ожидайте пожалуйста выполнения операции...\n\nПримечание: после запуска данные не обратимы!");
                        if(isRun === true){
                            actualData(idElement, arData);
                            $(this).text("Готово");
                            $(this).addClass("disable-link");
                        }
                    });
                } else {
                    if(action === true){
                        $('#' + idElement + ' > td:eq(3)').text(" - ");
                    } else{
                        $('#' + idElement + ' > td:eq(3)').text(action);
                    }
                }
            }

            function actualData(idElement, arData){
                //Отправка синхронного запроса
                $.ajax({
                    async: false,
                    type: "POST", //Метод отправки
                    url: "/reports/ajax/company/ajax_run_disbalans.php",
                    dataType: "json",
                    data: {'idElement':idElement,'data':JSON.stringify(arData)},

                    success: function(data){
                        console.log(data);
                        if(data.status === true || data.status == 200){
                            var strRes = "Актуализация завершена успешно!\n\nОтвет от сервера: \n";
                            for (var k in data){
                                if (data.hasOwnProperty(k)) {
                                    strRes += k + ": " + data[k] + "\n";
                                }
                            }
                            alert(strRes);
                        } else {
                            alert("Ошибка актуализации: " + data.error);
                        }
                    }
                });
            }

            function getNumberBeautifulFormat(data) {
                var price       = Number.prototype.toFixed.call(parseFloat(data) || 0, 0),
                    price_sep   = price.replace(/(\D)/g, ","),
                    price_sep   = price_sep.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1 ");

                return price_sep;
            };

            function enableForm() {
                $("#runCheck").removeClass('wait');
                $("#runCheck").val('Запустить повторно');
            }

            function disableForm() {
                $('#runCheck').addClass('wait');
                $('#notification').removeClass('visible-off');
                $("#result").addClass('visible-off');
                $('#error').addClass('visible-off');
                $("a[data-name=actual]").each(function(){
                    $(this).off('click');
                    $(this).text("Актуализировать");
                    $(this).removeClass('disable-link');
                });
            }

        });
    </script>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>