<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/intranet/public/timeman/index.php");
$APPLICATION->SetTitle(GetMessage("COMPANY_TITLE"));
?>
<?$APPLICATION->IncludeComponent(
    "itk:intranet.absence.calendar",
    "vacation",
    array(
        "FILTER_NAME" => "absence",
        "FILTER_SECTION_CURONLY" => "N",
        "TYPES" => "VACATION",
        "SHORT_EVENTS" => "N",
        "USERS_ALL" => "N",
        "VIEW_START" => "year",
        "COMPONENT_TEMPLATE" => "vacation",
        "NAME_TEMPLATE" => "#NOBR##LAST_NAME# #NAME##/NOBR#",
        "FILTER_CONTROLS" => array(
            0 => "DATEPICKER",
            1 => "DEPARTMENT",
        ),
        "FIRST_DAY" => "1",
        "DAY_START" => "9",
        "DAY_FINISH" => "18",
        "DAY_SHOW_NONWORK" => "N",
        "DATE_FORMAT" => "d.m.Y",
        "DATETIME_FORMAT" => "d.m.Y H:i:s"
    ),
    false
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>