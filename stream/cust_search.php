<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/stream/function.php");
$APPLICATION->SetTitle("Поиск контрагентов");
?>
<style>
	div.nav_block ul li {
		display: inline-block;
	}
</style>
<script src="table2csv.js"></script>
<?php

if (isset($_GET['inn'], $_GET['name'] )) {
    $inn = ($_GET['inn']);
    $name = ($_GET['name']);
 	$startPage = 0;
	
	if(isset($_GET['startPage']))
		$startPage = intval($_GET['startPage']);
	
	if(isset($_GET['go_to'])) {
		$go_to = intval($_GET['go_to'])-1;
		if($go_to < 0)
			$go_to = 0;
		$startPage = $go_to;
	}
	
	$limit = 100;
    if ($inn != "" and $name != "") {
        $check = "<div style ='font: 15px  Arial,tahoma,sans-serif;color:red'>Выберите что-то одно!</div>";
    } else {
		
        if ($inn != "") {
			$queryText = "SELECT * FROM First WHERE `110`='$inn'";
        }
        if ($name != "") {
            $queryText = "SELECT * FROM First WHERE `137` LIKE '%'  '$name' '%'";
        }
        if ($ofd != "") {
            $queryText = "SELECT * FROM First WHERE `137` LIKE '%'  '$ofd' '%'";
        }


		$result = queryMysql($queryText);
		$totalCount = mysqli_num_rows($result);
		$queryTextLimit = $queryText." LIMIT $startPage, $limit";
		$result = queryMysql($queryTextLimit);
		$num_rows = mysqli_num_rows($result);
    }
}
?>
<div class="main">
	<h2>Заполните данные контрагента</h2>
	<?="<label><span class='error'>$error<br><br></span></label>";?>
    <form action='cust_search.php' method='GET'>
        <table>
            <tr>
                <td><?=$check;?></td>
                <td></td>
                <td></td>
            </tr>
			<tr>
                <td>
                    <label>Здесь нужно ввести ИНН.</label>
                </td>
				<td>
                <input type='text' size='40' maxlength='16' name='inn' value='<?=$inn?>'>
            </td>
            </tr>

            <td>

            </td>
            <tr>
                <td>
                    <label>Здесь нужно ввести Название.</label>
                </td>
                <td>
                    <input type='text' size='40' name='name' value='<?=$name?>'>
                </td>
                <td></td>
            </tr>



            <tr>
                <td></td>
                <td align="center">
                    <input type="submit" value="Искать">
                </td>
                <td>
                </td>
            </tr>
        </table>
	</form>
	<table>
		<tr>
			<td width="400"></td>
			<td width="300" align="right">
				<?="<div style ='font: 20px  Arial,tahoma,sans-serif;color:green'>______ $totalCount ______</div>";?>
			</td>
			<td>
				<?php

				if ($totalCount > 0) {
					echo '<button onclick="Export()">Выгрузить в CSV Файл</button>';

					if ($inn != "") {
                        session_start();
                        $_SESSION['inn'] = $inn;
                        $_SESSION['name'] = "";


                    }
                    if ($name != "") {
                        session_start();
                        $_SESSION['inn'] = "";
                        $_SESSION['name'] = $name;

                    }



				}
				?>
			</td>
		</tr>
		<tr>
			<td width="400"></td>
			<td><br><br></td>
		</tr>
	</table>
	<?if ($num_rows > 0):?>
		<div class="nav_block">
			<?$pageCount = ceil($totalCount/$limit);?>
			Всего страниц: <?=$pageCount?><br />
			<form action='cust_search.php' method='GET'>
                <input type='hidden' size='40' maxlength='16' name='inn' value='<?=$inn?>'>
                <input type='hidden' size='40' name='name' value='<?=$name?>'>
                <input type='hidden' size='40' name='name' value='<?=$ofd?>'>
				<p>Перейти к странице: <input type="text" name="go_to" value="" /> <input type="submit" value="перейти" /></p>
			</form>
			<ul>
				<?if($startPage != 0):?>
				<li>
					<a href="<?=$APPLICATION->GetCurPageParam("startPage=".($startPage-1), array("startPage", "go_to"));?>">Пред. страница</a>
				</li>
				<?endif;?>
				<li>
					<?=$startPage+1?>
				</li>
				<?if($startPage != ($pageCount-1)):?>
				<li>
					<a href="<?=$APPLICATION->GetCurPageParam("startPage=".($startPage+1), array("startPage", "go_to"));?>">След. страница</a>
				</li>
				<?endif;?>
				</li>
			</ul>
		</div>
		<table id='table-res' border='1' cellspacing='0' cellpadding='5'>
			<tr>
				<th>110</th>
				<th>137</th>
				<th>138</th>
				<th>139</th>
				<th>140</th>
				<th>141</th>
				<th>142</th>
				<th>143</th>
				<th>144</th>
				<th>145</th>
				<th>146</th>
				<th>147</th>
			</tr>

		<?while ($row = mysqli_fetch_array($result)) {
			echo "<tr>";
			echo "<td>" . $row['110'] . "</td>";
			echo "<td>" . $row['137'] . "</td>";
			echo "<td>" . $row['138'] . "</td>";
			echo "<td>" . $row['139'] . "</td>";
			echo "<td>" . $row['140'] . "</td>";
			echo "<td>" . $row['141'] . "</td>";
			echo "<td>" . $row['142'] . "</td>";
			echo "<td>" . $row['143'] . "</td>";
			echo "<td>" . $row['144'] . "</td>";
			echo "<td>" . $row['145'] . "</td>";
			echo "<td>" . $row['146'] . "</td>";
			echo "<td>" . $row['147'] . "</td>";
			echo "</tr>";
		}?>
		</table>
	<?endif?>
	<script>
		function Export()
		{
			var conf = confirm("Export users to CSV?");
			if(conf == true)
			{
				window.open("export.php", '_blank');
			}
		}
	</script>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>