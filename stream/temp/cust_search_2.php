<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/stream/function.php");
$APPLICATION->SetTitle("Поиск контрагентов");
?>
<link rel="stylesheet" href="https://tableexport.v3.travismclarke.com/assets/css/font-awesome.min.css">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">

<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://tableexport.v3.travismclarke.com/bower_components/jquery/dist/jquery.min.js"></script>
<script src="https://tableexport.v3.travismclarke.com/bower_components/js-xlsx/dist/xlsx.core.min.js"></script>
<script src="https://tableexport.v3.travismclarke.com/bower_components/blobjs/Blob.min.js"></script>
<script src="https://tableexport.v3.travismclarke.com/bower_components/file-saverjs/FileSaver.min.js"></script>
<script src="https://tableexport.v3.travismclarke.com/dist/js/tableexport.min.js"></script>

<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

<?php

if (isset($_GET['inn'], $_GET['name'] )) {
    $inn = ($_GET['inn']);
    $name = ($_GET['name']);
	$startPage = 0;
	if(isset($_GET['startPage']))
		$startPage = intval($_GET['startPage']);
	$limit = 100;
    if ($inn != "" and $name != "") {
        $check = "<div style ='font: 15px  Arial,tahoma,sans-serif;color:red'>Выберите что-то одно!</div>";
    } else {
		
        if ($inn != "") {
			$queryText = "SELECT * FROM First WHERE `110`='$inn'";
        }
        if ($name != "") {
			$queryText = "SELECT * FROM First WHERE `137` LIKE '%'  '$name' '%'";
        }
		$result = queryMysql($queryText);		
		//$queryTextLimit = $queryText." LIMIT $startPage, $limit";
		//$result = queryMysql($queryTextLimit);
		$num_rows = mysqli_num_rows($result);
    }
}
?>
<div class="main">
	<h2>Заполните данные контрагента</h2>
	<?="<label><span class='error'>$error<br><br></span></label>";?>
    <form action='cust_search_2.php' method='GET'>
		<div class="form-group">
			<label for="exampleInputEmail1">Введите ИНН:</label>
			<input type="number" class="form-control" id="exampleInputEmail1" placeholder="ИНН" minlength="10" maxlength="12">			
		</div>
		
		<div class="form-group">
			<label for="exampleInputEmail1">Введите название компании:</label>
			<input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Название компании">			
		</div>
	
        <table>
            <tr>
                <td><?=$check;?></td>
                <td></td>
                <td></td>
            </tr>
			<tr>
                <td>
                    <label>Здесь нужно ввести ИНН.</label>
                </td>
				<td>
                <?="<input type='text' size='40' maxlength='16' name='inn' value='$inn'>";?>
            </td>
            </tr>

            <td>

            </td>
            <tr>
                <td>
                    <label>Здесь нужно ввести Название.</label>
                </td>
                <td>
                    <?="<input type='text' size='40' name='name' value='$name'>";?>
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td align="center">
                    <input type="submit" value="Искать">
                </td>
                <td>
                </td>
            </tr>
        </table>
	</form>
	<table>
		<tr>
			<td width="400"></td>
			<td width="300" align="right">
				<?="<div style ='font: 20px  Arial,tahoma,sans-serif;color:green'>______ $num_rows ______</div>";?>
			</td>
		</tr>
		<tr>
			<td width="400"></td>
			<td><br><br></td>
		</tr>
	</table>
	<?if ($num_rows > 0):?>		
		<table id='table-res' class="table table-striped table-bordered" style="width:100%">
			<thead>
				<tr>
					<th>110</th>
					<th>137</th>
					<th>138</th>
					<th>139</th>
					<th>140</th>
					<th>141</th>
					<th>142</th>
					<th>143</th>
					<th>144</th>
					<th>145</th>
					<th>146</th>
					<th>147</th>
				</tr>
			</thead>
			<tbody>
				<?while ($row = mysqli_fetch_array($result)) {
					echo "<tr>";
					echo "<td>" . $row['110'] . "</td>";
					echo "<td>" . $row['137'] . "</td>";
					echo "<td>" . $row['138'] . "</td>";
					echo "<td>" . $row['139'] . "</td>";
					echo "<td>" . $row['140'] . "</td>";
					echo "<td>" . $row['141'] . "</td>";
					echo "<td>" . $row['142'] . "</td>";
					echo "<td>" . $row['143'] . "</td>";
					echo "<td>" . $row['144'] . "</td>";
					echo "<td>" . $row['145'] . "</td>";
					echo "<td>" . $row['146'] . "</td>";
					echo "<td>" . $row['147'] . "</td>";
					echo "</tr>";
				}?>
			</tbody>
		</table>
	<?endif?>
	<script>
		$(document).ready(function() {
			$('#table-res').DataTable();
		} );
		
		/*$("#table-res").tableExport({
			bootstrap: true,
			position: "top"
		});*/
	
		function Export()
		{
			var conf = confirm("Export users to CSV?");
			if(conf == true)
			{
				window.open("export.php", '_blank');
			}
		}
	</script>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>