<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/stream/function.php");
$APPLICATION->SetTitle("Поиск контрагентов по дате");
?>

<?php

if (isset($_POST['startDate'], $_POST['endDate'], $_POST['idReg'], $_POST['nameReg'])) {
    $startDate = ($_POST['startDate']);
    $endDate = ($_POST['endDate']);
    $idReg = ($_POST['idReg']);
    $nameReg = ($_POST['nameReg']);
    $select = '';

    if (!empty($startDate) && !empty($endDate)) {
        $check = 1;
        $select = "( STR_TO_DATE(`123`, '%d/%m/%Y') BETWEEN STR_TO_DATE('$startDate', '%d/%m/%Y') AND STR_TO_DATE('$endDate', '%d/%m/%Y') )";
    }

    if ( !empty($idReg) ) {
        $check = 3;
        if(!empty($select)){
            $select .= " AND `143` LIKE '%'  '$idReg' '%'";
        } else {
            $select .= "`143` LIKE '%'  '$idReg' '%'";
        }
    }
    if ($nameReg != "") {
        $check = 4;
        if(!empty($select)){
            $select .= " AND `146` LIKE '%'  '$nameReg' '%'";
        } else {
            $select .= "`146` LIKE '%'  '$nameReg' '%'";
        }
    }
    if(!empty($select)) {
        $queryText = "SELECT * FROM First WHERE ($select)";
        echo "Выполненный запрос: $queryText <br />";
        $result = queryMysql($queryText);
        $totalCount = mysqli_num_rows($result);
        $num_rows = mysqli_num_rows($result);
    }


}



?>
<div class="main">
	<h2>Заполните данные контрагента</h2>
	<label>Дату вводить точно! как в примере.</label>
	<?="<label><span class='error'>$error<br><br></span></label>";?>
    <form action='cust_search_date.php' method='POST'>
        <table>
            <tr>
                <td><?=$check_lable;?></td>
                <td></td>
                <td></td>
            </tr>
			<tr>
                <td>
					<label>Дата начала (30/12/1015).</label>
                </td>
				<td>
                <input type='text' size='40' maxlength='16' name='startDate' value='<?=$startDate?>'>
            </td>
            </tr>

            <td>

            </td>
            <tr>
                <td>
                    <label>Дата окончания (30/12/1015)</label>
                </td>
                <td>
                    <input type='text' size='40' name='endDate' value='<?=$endDate?>'>
                </td>
                <td></td>
            </tr>

            <tr>
                <td>
                    <label>ID Региона (77)</label>
                </td>
                <td>
                    <input type='text' size='40' name='idReg' value='<?=$idReg?>'>
                </td>
                <td></td>
            </tr>

            <tr>
                <td>
                    <label>Название Региона (Москва)</label>
                </td>
                <td>
                    <input type='text' size='40' name='nameReg' value='<?=$nameReg?>'>
                </td>
                <td></td>
            </tr>

            <tr>
                <td></td>
                <td align="center">
                    <input type="submit" value="Искать">
                </td>
                <td>
                </td>
            </tr>
        </table>
	</form>
	<table>
		<tr>
			<td width="400"></td>
			<td width="300" align="right">
				<?="<div style ='font: 20px  Arial,tahoma,sans-serif;color:green'>______ $totalCount ______</div>";?>
			</td>
			<td>
				<?php

				if ($totalCount > 0) {
					echo '<button onclick="Export()">Выгрузить в CSV Файл</button>';

					    session_start();
                        $_SESSION['queryText'] = $queryText;





				}
				?>
			</td>
		</tr>
		<tr>
			<td width="400"></td>
			<td><br><br></td>
		</tr>
	</table>
	
	<script>
		function Export()
		{
			var conf = confirm("Export users to CSV?");
			if(conf == true)
			{
				window.open("export_date.php", '_blank');
			}
		}
	</script>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>

