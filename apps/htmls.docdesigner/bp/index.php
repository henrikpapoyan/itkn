<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
require_once($_SERVER["DOCUMENT_ROOT"].'/bitrix/modules/htmls.docdesigner/lang/ru/apps/htmls.docdesigner.php');
$APPLICATION->SetTitle(GetMessage('menuBizProc'));
IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/bitrix24/public/crm/configs/bp/index.php");
CModule::IncludeModule('bizproc');


//$APPLICATION->SetTitle(GetMessage("TITLE"));
?><?$APPLICATION->IncludeComponent("bitrix:menu", "htmls.docdesigner", array(
	"ROOT_MENU_TYPE" => "left",
	"MENU_CACHE_TYPE" => "N",
	"MENU_CACHE_TIME" => "3600",
	"MENU_CACHE_USE_GROUPS" => "Y",
	"MENU_CACHE_GET_VARS" => array(
	),
	"MAX_LEVEL" => "1",
	"CHILD_MENU_TYPE" => "left",
	"USE_EXT" => "N",
	"DELAY" => "N",
	"ALLOW_MULTI_SELECT" => "N"
	),
	false,
	array(
	"ACTIVE_COMPONENT" => "N"
	)
);?>
<br />
<?$APPLICATION->IncludeComponent("bitrix:crm.config.bp", "htmls.crm.config.bp", array(
	"SEF_MODE" => "Y",
	"SEF_FOLDER" => "/apps/htmls.docdesigner/bp/",
	"SEF_URL_TEMPLATES" => array(
		"ENTITY_LIST_URL" => "",
		"BP_LIST_URL" => "#entity_id#/",
		"BP_EDIT_URL" => "#entity_id#/edit/#bp_id#/",
	)
	),
	false
);?>
<br />
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>