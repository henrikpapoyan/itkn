<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
require_once($_SERVER["DOCUMENT_ROOT"].'/bitrix/modules/htmls.docdesigner/lang/ru/apps/htmls.docdesigner.php');
//echo '<pre>'; print_r($MESS); echo '</pre>';
$APPLICATION->SetTitle(GetMessage('indexTitle'));
?>
<script type="text/javascript">
function ForAllClick(el){
	if(el.checked){
		if(confirm('<?=GetMessage('apply4all');?>')){
			//go through rows
			var ids = el.form['ID[]'];
			if(ids){
				if(!ids.length)
					ids = new Array(ids);

				for(var i=0; i<ids.length; i++){
					ids[i].disabled = el.checked;
					ids[i].checked = el.checked;
				}
			}
			if(el.checked)
				document.getElementById('submit').disabled = false;
			else
				document.getElementById('submit').disabled = true;
		}
		else
			el.checked = false;
	}
}

function ForClick(el){
	if(el.checked)
		document.getElementById('submit').disabled = false;
	else
		ForAllCheck(el);
}

function ForAllCheck(el){
	//go through rows
	var ids = el.form['ID[]'];

	var disabled = true;

	if(ids)
	{
		if(!ids.length)
			ids = new Array(ids);

		for(var i=0; i<ids.length; i++){
			if(ids[i].checked)
				disabled = false;
		}
	}
	document.getElementById('submit').disabled = disabled;
}
</script>
 <?$APPLICATION->IncludeComponent(
	"bitrix:menu",
	"htmls.docdesigner",
	Array(
		"ROOT_MENU_TYPE" => "left",
		"MAX_LEVEL" => "1",
		"CHILD_MENU_TYPE" => "left",
		"USE_EXT" => "N",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "N",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => ""
	),
	false,
	array(
	"ACTIVE_COMPONENT" => "N"
	)
);?>
<br />
 <? $IBLOCK_ID = COption::GetOptionInt("htmls.docdesigner", 'DOCDESIGNER_DOCS_REGISTER_ID');

 $APPLICATION->IncludeComponent(
	"htmls:docdesigner.journal",
	".default",
	Array(
		"IBLOCK_TYPE" => "lists",
		"IBLOCK_ID" => $IBLOCK_ID,
		"NEWS_COUNT" => "20",
		"SORT_BY1" => "ID",
		"SORT_ORDER1" => "ASC",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"FILTER_NAME" => "",
		"FIELD_CODE" => array(0=>"NAME",1=>"DATE_CREATE",2=>"",),
		"PROPERTY_CODE" => array(0=>"DOC",1=>"",2=>"SUMMA",3=>"COMPANY_ID",4=>"DEAL_ID",5=>"LINK2FILE",6=>"",),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "N",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"SET_TITLE" => "Y",
		"SET_STATUS_404" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"DISPLAY_TOP_PAGER" => "Y",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "�������",
		"PAGER_SHOW_ALWAYS" => "Y",
		"PAGER_TEMPLATE" => "",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"AJAX_OPTION_ADDITIONAL" => "",
	)
);?>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>