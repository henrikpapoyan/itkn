<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
require_once($_SERVER["DOCUMENT_ROOT"].'/bitrix/modules/htmls.docdesigner/lang/ru/apps/htmls.docdesigner.php');
$APPLICATION->SetTitle(GetMessage('menuEx'));
?> <?$APPLICATION->IncludeComponent(
	"bitrix:menu",
	"htmls.docdesigner",
	Array(
		"ROOT_MENU_TYPE" => "left",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array(),
		"MAX_LEVEL" => "1",
		"CHILD_MENU_TYPE" => "left",
		"USE_EXT" => "N",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "N"
	),
	false,
	array(
	"ACTIVE_COMPONENT" => "N"
	)
);?>

<br />

 <?$APPLICATION->IncludeComponent("bitrix:photogallery", "docdesigner", array(
	"USE_LIGHT_VIEW" => "Y",
	"IBLOCK_TYPE" => "services",
	"IBLOCK_ID" => "208",
	"PATH_TO_USER" => "",
	"DRAG_SORT" => "Y",
	"USE_COMMENTS" => "N",
	"SEF_MODE" => "N",
	"SEF_FOLDER" => "/apps/htmls.docdesigner/help/",
	"CACHE_TYPE" => "A",
	"CACHE_TIME" => "3600",
	"SET_TITLE" => "N",
	"ALBUM_PHOTO_SIZE" => "120",
	"THUMBNAIL_SIZE" => "100",
	"ORIGINAL_SIZE" => "1280",
	"PHOTO_LIST_MODE" => "Y",
	"SHOWN_ITEMS_COUNT" => "6",
	"USE_RATING" => "N",
	"SHOW_TAGS" => "N",
	"UPLOADER_TYPE" => "applet",
	"APPLET_LAYOUT" => "extended",
	"UPLOAD_MAX_FILE_SIZE" => "1000",
	"USE_WATERMARK" => "Y",
	"WATERMARK_RULES" => "USER",
	"WATERMARK_MIN_PICTURE_SIZE" => "800",
	"SHOW_LINK_ON_MAIN_PAGE" => array(
		0 => "comments",
	),
	"SECTION_SORT_BY" => "SORT",
	"SECTION_SORT_ORD" => "ASC",
	"ELEMENT_SORT_FIELD" => "sort",
	"ELEMENT_SORT_ORDER" => "asc",
	"DATE_TIME_FORMAT_DETAIL" => "d.m.Y",
	"DATE_TIME_FORMAT_SECTION" => "d.m.Y",
	"SECTION_PAGE_ELEMENTS" => "15",
	"ELEMENTS_PAGE_ELEMENTS" => "50",
	"PAGE_NAVIGATION_TEMPLATE" => "",
	"JPEG_QUALITY1" => "100",
	"JPEG_QUALITY" => "100",
	"ADDITIONAL_SIGHTS" => array(
	),
	"SHOW_NAVIGATION" => "N",
	"VARIABLE_ALIASES" => array(
		"SECTION_ID" => "SECTION_ID",
		"ELEMENT_ID" => "ELEMENT_ID",
		"PAGE_NAME" => "PAGE_NAME",
		"ACTION" => "ACTION",
	)
	),
	false
);?> <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>