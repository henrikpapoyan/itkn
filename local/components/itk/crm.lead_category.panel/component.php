<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponent $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */

if (!CModule::IncludeModule('crm'))
{
	ShowError(GetMessage('CRM_MODULE_NOT_INSTALLED'));
	return;
}

$userID =  CCrmPerms::GetCurrentUserID();
$userPermissions = CCrmPerms::GetCurrentUserPermissions();
if (!CCrmLead::CheckReadPermission(0, $userPermissions))
{
	ShowError(GetMessage('CRM_PERMISSION_DENIED'));
	return;
}

use Bitrix\Crm\Counter\EntityCounterFactory;
use Bitrix\Crm\Counter\EntityCounterType;

$arResult = array();
$arResult['ITEMS'] = array();
$arResult["LEAD_ALL_COUNT"] = 0;

if(!empty($arParams["selFastFilter"]))
	$arResult["selFastFilter"] = $arParams["selFastFilter"];

if(!empty($arParams["arLeadChannels"]))
{
	// выбираем ID состояния лида Активный
	$leadStateId = 0;
	$leadStateId = getIdElementListState("XML_STATE_LEAD_ACTIVITY");
    $TYPE_ID = $this->getParent()->arResult['TYPE_ID']; //GK
	foreach($arParams["arLeadChannels"] as $chennalID => $chennalValue)
	{
		$countLeads = 0;

		$arOrderLead = array();
		$arFilterLead = array("UF_CHANNEL" => $chennalID,
            "STATUS_ID" => '4'
//            "UF_STATE_LEAD" => $leadStateId
        );
        if($TYPE_ID > 0) //GK
        {
            $arFilterLead['UF_TYPE'] = $TYPE_ID;
        }
        $arFilterLead['!SOURCE_ID'] = gk_GetOrderFilter();
		$arSelectLead = array("ID");
		$dbLeads = CCrmLead::GetList($arOrderLead, $arFilterLead, $arSelectLead);
		$countLeads = $dbLeads->SelectedRowsCount();

		$arResult["LEAD_ALL_COUNT"] += $countLeads;

		$arResult['ITEMS'][] = array(
			'ID' => $chennalID,
			'NAME' => $chennalValue,
			'COUNTER' => $countLeads
		);
	}
}

$this->IncludeComponentTemplate();

