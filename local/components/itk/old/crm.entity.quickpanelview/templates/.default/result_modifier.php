<?php
/**
 * User: German
 * Date: 17.05.2017
 * Time: 15:53
 */
if (array_search($arResult['ENTITY_TYPE_NAME'], array('LEAD', 'CONTACT', 'COMPANY')) !== false)
{
    global $USER_FIELD_MANAGER;

    if($arResult['ENTITY_TYPE_NAME'] == 'LEAD')
    {
        if($arParams['SECTION'] == 'ORDER') //GK
        {
            $arUserProp = getUserPropArray("XML_ID", "LEAD_SERT_COUNT");
            $arResult['CONFIG']['center'] = 'UF_DEP,ASSIGNED_BY_ID,UF_PARTNER,UF_CLIENT';
            $arResult['CONFIG']['left'] = 'UF_TYPE_ORDER,SOURCE_ID,'.$arUserProp["FIELD_NAME"];
            $arResult['CONFIG']['bottom'] = 'COMMENTS, UF_RESULT';
        }
        else
        {
            $arResult['CONFIG']['center'] = 'UF_DEP,ASSIGNED_BY_ID,UF_PARTNER,UF_CLIENT';
            $arResult['CONFIG']['left'] = 'UF_NEED,UF_TYPE,SOURCE_ID,SOURCE_DESCRIPTION,UF_IN_GROUP,UF_KKT';
            $arResult['CONFIG']['bottom'] = 'COMMENTS, UF_RESULT';
        }
        global $USER_FIELD_MANAGER;
        $ufEntityID = 'CRM_LEAD';
        $entityID=$arResult['ENTITY_ID'];
        $arUserFields = $USER_FIELD_MANAGER->GetUserFields($ufEntityID, $entityID, LANGUAGE_ID);
        $arFields = array('UF_NEED', 'UF_TYPE', 'UF_IN_GROUP', 'UF_DEP');
        $arSectionFields = array('UF_DEP');
    }
    elseif($arResult['ENTITY_TYPE_NAME'] == 'CONTACT')
    {
        $arResult['CONFIG']['center'] = 'UF_CRM_592D7A849D74F,ASSIGNED_BY_ID,UF_ACTIVE,UF_ARCHIV';

        $ufEntityID = 'CRM_CONTACT';
        $entityID=$arResult['ENTITY_ID'];
        $arUserFields = $USER_FIELD_MANAGER->GetUserFields($ufEntityID, $entityID, LANGUAGE_ID);
        $arFields = array('UF_CRM_592D7A849D74F');
        $arSectionFields = array('UF_CRM_592D7A849D74F');

    }elseif($arResult['ENTITY_TYPE_NAME'] == 'COMPANY') {
        $arResult['CONFIG']['left'] = 'UF_STATUS,UF_CRM_INN,UF_FULL_NAME,COMPANY_TYPE,UF_SEGMENT,UF_OKVED,UF_REGION,UF_CRM_1492416196,UF_LOYALTY';
        $arResult['CONFIG']['center'] = 'UF_NDA,UF_KKT2,UF_CRM_1486035185,REVENUE,UF_PAYMENT,UF_COST,UF_RATE,UF_BRAND';
        $arResult['CONFIG']['right'] = 'UF_DEP,ASSIGNED_BY_ID,CONTACT_ID,POST,UF_SEX,HONORIFIC,PHONE,EMAIL';
        $arContact_ID = \Bitrix\Crm\Binding\ContactCompanyTable::getCompanyContactIDs($arResult['ENTITY_ID']);
        $arContact = array();
        $UF_SEX = '';
        global $USER_FIELD_MANAGER;
        if(count($arContact_ID) > 0)
        {
            $rsContact = CCrmContact::GetListEx(array(),array('ID' => $arContact_ID[0]));
            $arContact = $rsContact->Fetch();
            $ufEntityID = 'CRM_CONTACT';
            $entityID = $arContact_ID[0];
            $arUserFields = $USER_FIELD_MANAGER->GetUserFields($ufEntityID, $entityID, LANGUAGE_ID);
            $arHONORIFIC_LIST = CCrmStatus::GetStatusList('HONORIFIC');
            $HONORIFIC = $arHONORIFIC_LIST[$arContact['HONORIFIC']];
            if ($arUserFields['UF_SEX']['VALUE'] > 0)
            {
                $rsSex = CUserFieldEnum::GetList(array(), array('ID' =>$arUserFields['UF_SEX']['VALUE']));
                $arSex = $rsSex->Fetch();
                $UF_SEX = $arSex['VALUE'];
            }
        }
        $v = Array(
            'type' => 'text',
            'editable' => 1,
            'caption' => "Контакт",
            'data' => Array(
                'text' => $arContact['FULL_NAME'],//"FieldVal",
                'multiline' => 1,

            )
        );
        $arResult['ENTITY_DATA']['CONTACT_ID'] = $v;
        $v = Array(
            'type' => 'text',
            'editable' => 1,
            'caption' => "Роль (должность)",
            'data' => Array(
                'text' => $arContact['POST'],
                'multiline' => 1,

            )
        );
        $arResult['ENTITY_DATA']['POST'] = $v;
        $v = Array(
            'type' => 'text',
            'editable' => 1,
            'caption' => "Пол",
            'data' => Array(
                'text' => $UF_SEX,
                'multiline' => 1,

            )
        );
        $arResult['ENTITY_DATA']['UF_SEX'] = $v;

        $v = Array(
            'type' => 'text',
            'editable' => 1,
            'caption' => "Обращение",
            'data' => Array(
                'text' => $HONORIFIC,
                'multiline' => 1,

            )
        );
        $arResult['ENTITY_DATA']['HONORIFIC'] = $v;

        $ufEntityID = 'CRM_COMPANY';
        $entityID = $arResult['ENTITY_ID'];
        $arUserFields = $USER_FIELD_MANAGER->GetUserFields($ufEntityID, $entityID, LANGUAGE_ID);
        $arFields = array('UF_SEGMENT', 'UF_REGION', 'UF_BRAND', 'UF_DEP');
        $arSectionFields = array("UF_DEP");
    }
    foreach ($arFields as $FieldName) {
        $FieldData = $arUserFields[$FieldName];
        if(array_search($FieldName, $arSectionFields) !== false)
        {
            $res = CIBlockSection::GetByID($FieldData["VALUE"]);
        }
        else
        {
            $res = CIBlockElement::GetByID($FieldData["VALUE"]);
        }
        $FieldVal = "";
        if($ar_res = $res->Fetch())
        {
            $FieldVal = $ar_res['NAME'];
        }

        $v = Array(
            'type' => 'text',
            'editable' => 1,
            'caption' => $FieldData['EDIT_FORM_LABEL'],
            'data' => Array(
                'text' => $FieldVal,
                'multiline' => 1
            )
        );
        $arResult['ENTITY_DATA'][$FieldName] = $v;

    }

}
