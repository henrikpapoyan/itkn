<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 */

$showCount = 2;
$selFastFilter = $arResult["selFastFilter"];
?>
<div class="crm-deal-panel-array-have" id="deal_category_panel_container">
	<div class="crm-deal-panel-tab">
		<div class="crm-deal-panel-tab-item crm-deal-panel-tab-item-icon-1 <?=$selFastFilter=='type_ALL'?'crm-deal-panel-tab-item-active':''?>">
			<?if($arResult["LEAD_ALL_COUNT"] > 0):?>
				<span class="crm-deal-panel-tab-item-message"><?=$arResult["LEAD_ALL_COUNT"];?></span>
			<?endif;?>
            <a href="javascript:void(0)" id="type_ALL" class="crm-deal-panel-tab-item-text">Все</a>
        </div>
<?
foreach($arResult['ITEMS'] as $arChennal):
	$chennalID =    $arChennal["ID"];
	$chennalValue = $arChennal["NAME"];
	$chennalCount = $arChennal["COUNTER"];
?>
	<div class="crm-deal-panel-tab-item crm-deal-panel-tab-item-icon-<?=$showCount?> <?=$selFastFilter=='type_'.$chennalID?'crm-deal-panel-tab-item-active':''?>">
		<?if($chennalCount > 0):?>
			<span class="crm-deal-panel-tab-item-message"><?=$chennalCount;?></span>
		<?endif;?>
		<a href="javascript:void(0)" id="type_<?=$chennalID?>" class="crm-deal-panel-tab-item-text"><?=$chennalValue?></a>
	</div>
<?
$showCount++;
endforeach;
?>
	</div>
</div>