<?
global $PRODUCT_ID;
global $SEGMENT;


//pre($_REQUEST);

$flagProductSetUp = $flagSegmentSetUp = $flagTimeSetUp = false;
if (isset($_REQUEST["products-group"]) && !empty($_REQUEST["products-group"])) {
    $productsGroup = $_REQUEST["products-group"];
    $PRODUCT_ID = $productsGroup;
    $flagProductSetUp = true;
}

if (isset($_REQUEST["segment-group"]) && !empty($_REQUEST["segment-group"])) {
    $segmentsGroup = $_REQUEST["segment-group"];
    $SEGMENT = $segmentsGroup;
    $flagSegmentSetUp = true;
}

$arSelectRespUser = array();
if (isset($_REQUEST["select_responsible_user"]) && !empty($_REQUEST["select_responsible_user"])) {
    $ASSIGNED = explode(',', $_REQUEST["select_responsible_user"]);
}

$periodTypes = array(
    'month',
    'month_ago',
    'week',
    'week_ago',
    'days',
    'after',
    'before',
    'interval',
    'all'
);


$date_from = $date_to = null;
$form_date = array('from' => null, 'to' => null, 'days' => null);
if (!empty($_GET['F_DATE_TYPE']) && in_array($_GET['F_DATE_TYPE'], $periodTypes, true)) {

    $period = array('type' => $_GET['F_DATE_TYPE']);
    switch ($_GET['F_DATE_TYPE']) {
        case 'days':
            $days = !empty($_GET['F_DATE_DAYS']) ? (int)$_GET['F_DATE_DAYS'] : 1;
            $period['value'] = $days ? $days : 1;
            break;

        case 'after':
            $date = !empty($_GET['F_DATE_TO']) ? (string)$_GET['F_DATE_TO'] : ConvertTimeStamp(false, 'SHORT');
            $date = MakeTimeStamp($date);
            $period['value'] = $date ? $date : time();
            break;

        case 'before':
            $date = !empty($_GET['F_DATE_FROM']) ? (string)$_GET['F_DATE_FROM'] : ConvertTimeStamp(false, 'SHORT');
            $date = MakeTimeStamp($date);
            $period['value'] = $date ? $date + (3600 * 24 - 1) : time() + (3600 * 24 - 1);
            break;

        case 'interval':
            $date_f = !empty($_GET['F_DATE_FROM']) ? (string)$_GET['F_DATE_FROM'] : ConvertTimeStamp(false, 'SHORT');
            $date_f = MakeTimeStamp($date_f);
            $date_t = !empty($_GET['F_DATE_TO']) ? (string)$_GET['F_DATE_TO'] : ConvertTimeStamp(false, 'SHORT');
            $date_t = MakeTimeStamp($date_t);
            if ($date_f || $date_t) {
                $period['value'][0] = $date_f ? $date_f : time();
                $period['value'][1] = $date_t ? $date_t + (3600 * 24 - 1) : time() + (3600 * 24 - 1);
            }
            break;

        default:
            $period['value'] = null;
    }
} else {
    $period = $settings['period'];
}

//pre(array("period" => $period));

switch ($period['type']) {
    case 'month':
        $date_from = strtotime(date("Y-m-01"));
        $flagTimeSetUp = true;
        break;

    case 'month_ago':
        $date_from = strtotime(date("Y-m-01", strtotime("-1 month")));
        $date_to = strtotime(date("Y-m-t", strtotime("-1 month"))) + (3600 * 24 - 1);
        $flagTimeSetUp = true;
        break;

    case 'week':
        $date_from = strtotime("-" . ((date("w") == 0 ? 7 : date("w")) - 1) . " day 00:00");
        $flagTimeSetUp = true;
        break;

    case 'week_ago':
        $date_from = strtotime("-" . ((date("w") == 0 ? 7 : date("w")) + 6) . " day 00:00");
        $date_to = strtotime("-" . (date("w") == 0 ? 7 : date("w")) . " day 23:59:59");
        $flagTimeSetUp = true;
        break;

    case 'days':
        $date_from = strtotime(date("Y-m-d") . " -" . intval($period['value']) . " day");
        $form_date['days'] = intval($period['value']);
        $flagTimeSetUp = true;
        break;

    case 'after':
        $date_from = $period['value'];
        $form_date['to'] = ConvertTimeStamp($period['value'], 'SHORT');
        $flagTimeSetUp = true;
        break;

    case 'before':
        $date_to = $period['value'];
        $form_date['from'] = ConvertTimeStamp($period['value'], 'SHORT');
        $flagTimeSetUp = true;
        break;

    case 'interval':
        list($date_from, $date_to) = $period['value'];
        $form_date['from'] = ConvertTimeStamp($period['value'][0], 'SHORT');
        $form_date['to'] = ConvertTimeStamp($period['value'][1], 'SHORT');
        $flagTimeSetUp = true;
        break;
    case 'all':
        $date_from = "915138000"; //01.01.1999
        $flagTimeSetUp = true;
        break;


}


//pre(array($ASSIGNED,$SEGMENT,$PRODUCT_ID, $date_from, $date_to));
if ($flagSegmentSetUp && $flagProductSetUp && $flagTimeSetUp) {
    showTopPartner($ASSIGNED, $SEGMENT, $PRODUCT_ID, $date_from, $date_to);
}
$arResult['form_date'] = $form_date;


function showTopPartner($ASSIGNED = null, $SEGMENT, $PRODUCT_ID, $date_from = null, $date_to = null)
{

    global $DB;

    $err_mess = '';
    /*Получили всех контров со статусом *Партнер* */
    //  $strSql = "SELECT b_crm_company.ID,b_crm_company.TITLE,b_crm_company.ASSIGNED_BY_ID,b_uts_crm_company.UF_KKT,b_uts_crm_company.UF_CRM_INN,b_uts_crm_company.UF_KKT2,b_uts_crm_company.UF_SEGMENT FROM b_crm_company left JOIN b_uts_crm_company ON b_crm_company.ID = b_uts_crm_company.VALUE_ID where (b_crm_company.COMPANY_TYPE='PARTNER' or b_crm_company.COMPANY_TYPE='AGENTPARTNER' or b_crm_company.COMPANY_TYPE='2' or b_crm_company.COMPANY_TYPE='CLIENTAGENTPARTNER' or b_crm_company.COMPANY_TYPE='18')";

    $strSql = "SELECT b_crm_company.ID,b_crm_company.TITLE,b_crm_company.ASSIGNED_BY_ID,b_uts_crm_company.UF_KKT,b_uts_crm_company.UF_CRM_INN,b_uts_crm_company.UF_KKT2,b_uts_crm_company.UF_SEGMENT FROM b_crm_company left JOIN b_uts_crm_company ON b_crm_company.ID = b_uts_crm_company.VALUE_ID";
    $rsCompany = $DB->Query($strSql, false, $err_mess . __LINE__);
    global $allCompany;
    $allCompany = array();
    $allcount = 0;
    while ($arCompany = $rsCompany->Fetch()) {
        $allCompany[$arCompany["ID"]]['KKT'] = 0;
        $allCompany[$arCompany["ID"]]['KKTPLAN'] = $arCompany["UF_KKT2"];
        $allCompany[$arCompany["ID"]]['INN'] = $arCompany["UF_CRM_INN"];
        $allCompany[$arCompany["ID"]]['TITLE'] = $arCompany["TITLE"];
        $allCompany[$arCompany["ID"]]['ASSIGNED_BY_ID'] = $arCompany["ASSIGNED_BY_ID"];
        $allCompany[$arCompany["ID"]]['UF_SEGMENT'] = $arCompany["UF_SEGMENT"];
        $allCompany[$arCompany["ID"]]['SHOWELEMENT'] = "N";
        $allCompany[$arCompany["ID"]]['SHOWELEMENTWITHSCHET'] = "N";
        $allcount++;
        //pre($allCompany);break;

    }
    //pre(count($allCompany));

    $site_date_from = !is_null($date_from) ? ConvertTimeStamp($date_from, 'FULL') : null;
    $site_date_to = !is_null($date_to) ? ConvertTimeStamp($date_to, 'FULL') : null;


    /* Показать контров в зависимости от выбранного типа и ответсвенных*/
    $countshow = 0;
    $showCompanyId = array();
    $showCompanyINN = array();


    foreach ($allCompany as $index => $companyelement) {
        if (!empty($ASSIGNED) && !empty($SEGMENT)) {
            if ((in_array($companyelement['ASSIGNED_BY_ID'], $ASSIGNED)) && (in_array($companyelement['UF_SEGMENT'], $SEGMENT))) {
                //echo $index."____1_____".$companyelement['UF_SEGMENT']."<br>";
                $allCompany[$index]['SHOWELEMENT'] = "Y";
                $countshow++;
                $showCompanyId[$countshow] = $index;
                $showCompanyINN[$countshow] = $companyelement['INN'];
            }
        } else if (!empty($SEGMENT)) {
            if ((in_array($companyelement['UF_SEGMENT'], $SEGMENT))) {
                //echo $index."____2_____".$companyelement['UF_SEGMENT']."<br>";
                $allCompany[$index]['SHOWELEMENT'] = "Y";
                $countshow++;
                $showCompanyId[$countshow] = $index;
                $showCompanyINN[$countshow] = $companyelement['INN'];
            }
        }
    }

    //echo "Количество на показ: " . $countshow . "<br>";
    //pre($showCompanyINN);

    //$showCompanyINN=array("5752001578","7743173126");

    $site_date_from_KKT = !is_null($date_from) ? ConvertTimeStamp($date_from, 'SHORT') : null;
    $site_date_to_KKT = !is_null($date_to) ? ConvertTimeStamp($date_to, 'SHORT') : null;


    //pre($site_date_from_KKT);
    //pre($site_date_to_KKT);


    if ((!empty($site_date_from_KKT)) && (!empty($site_date_to_KKT))) {
        $site_date_from_KKT_array = $pieces = explode(".", $site_date_from_KKT);
        $site_date_to_KKT_array = $pieces = explode(".", $site_date_to_KKT);

        $site_date_from_KKT_array = array_reverse($site_date_from_KKT_array);
        $site_date_to_KKT_array = array_reverse($site_date_to_KKT_array);

        $site_date_from_KKT = implode("-", $site_date_from_KKT_array);
        $site_date_to_KKT = implode("-", $site_date_to_KKT_array);

        $strSql = "SELECT IBLOCK_ELEMENT_ID,PROPERTY_480,PROPERTY_481,PROPERTY_487 FROM b_iblock_element_prop_s76 where ( PROPERTY_480>= '" . $site_date_from_KKT . "' and PROPERTY_480<= '" . $site_date_to_KKT . "')";
    } else if (!empty($site_date_from_KKT)) {
        $site_date_from_KKT_array = $pieces = explode(".", $site_date_from_KKT);

        $site_date_from_KKT_array = array_reverse($site_date_from_KKT_array);

        $site_date_from_KKT = implode("-", $site_date_from_KKT_array);

        $strSql = "SELECT IBLOCK_ELEMENT_ID,PROPERTY_480,PROPERTY_481,PROPERTY_487 FROM b_iblock_element_prop_s76 where ( PROPERTY_480>= '" . $site_date_from_KKT . "')";

    } else if (!empty($site_date_to_KKT)) {

        $site_date_to_KKT_array = $pieces = explode(".", $site_date_to_KKT);

        $site_date_to_KKT_array = array_reverse($site_date_to_KKT_array);

        $site_date_to_KKT = implode("-", $site_date_to_KKT_array);

        $strSql = "SELECT IBLOCK_ELEMENT_ID,PROPERTY_480,PROPERTY_481,PROPERTY_487 FROM b_iblock_element_prop_s76 where ( PROPERTY_480<= '" . $site_date_to_KKT . "')";

    } else {
        $strSql = "SELECT IBLOCK_ELEMENT_ID,PROPERTY_480,PROPERTY_481,PROPERTY_487 FROM b_iblock_element_prop_s76";
    }


    $rsKKT = $DB->Query($strSql, false, $err_mess . __LINE__);
    $allKKT = array();
    $allcount = 0;
    while ($arKKT = $rsKKT->Fetch()) {
        if (!empty($arKKT["PROPERTY_481"])) {
            if ((in_array($arKKT["PROPERTY_481"], $showCompanyINN))) {
                //pre($arKKT["IBLOCK_ELEMENT_ID"]."      ".$arKKT["PROPERTY_487"]);
                /*$allCompany[$arKKT["PROPERTY_487"]]['KKT']++;
                pre($arKKT["IBLOCK_ELEMENT_ID"]);
                pre($arKKT["PROPERTY_481"]);
                pre($arKKT["PROPERTY_487"]);
                $allcount++;break;*/
                if (empty($allKKT[$arKKT["PROPERTY_481"]]['KKT'])) {
                    $allKKT[$arKKT["PROPERTY_481"]]['KKT'] = 1;
                } else {
                    $allKKT[$arKKT["PROPERTY_481"]]['KKT']++;
                }
            }
        }
    }

    //pre($allKKT);


    foreach ($showCompanyId as $indexelement => $element) {

        if ($allKKT[$showCompanyINN[$indexelement]]['KKT'] > 0) {
            //pre($indexelement);
            //pre($showCompanyINN[$indexelement]);
            //pre($element);

            $allCompany[$element]['KKT'] = $allKKT[$showCompanyINN[$indexelement]]['KKT'];
            $allCompany[$element]['SHOWELEMENTWITHSCHET'] = "Y";
        }
    }


    if (!empty($showCompanyId)) {
        CModule::IncludeModule('sale');
        $arFilterOrder = array("STATUS_ID" => "P");
        $arFilterOrder['=UF_COMPANY_ID'] = $showCompanyId;
        if (!empty($ASSIGNED)) {
            $arFilterOrder['=RESPONSIBLE_ID'] = $ASSIGNED;
        }

        //- дата оплаты, попадает в выбранный отчетный период.
        if (!empty($site_date_from)) {
            $arFilterOrder[">=PAY_VOUCHER_DATE"] = $site_date_from;
        }

        if (!empty($site_date_to)) {
            $arFilterOrder["<=PAY_VOUCHER_DATE"] = $site_date_to;

        }


        //pre($arFilterOrder);
        $CSaleBasket = new CSaleBasket();
        $allInvoice = array();
        $rsOrders = CCrmInvoice::GetList(array('PAY_VOUCHER_DATE' => 'DESC'), $arFilterOrder, false, false, array("*"));
        while ($arOrder = $rsOrders->GetNext()) {
            //pre($arOrder['ID']);
            $rsBasket = $CSaleBasket->GetList(
                array("ID" => "DESC"),
                array("ORDER_ID" => $arOrder['ID'], "=PRODUCT_ID" => $PRODUCT_ID),
                false,
                false,
                array("*")
            );
            while ($arBasket = $rsBasket->GetNext()) {
                //echo "Корзина результат:";

                if (empty($allInvoice[$arOrder['UF_COMPANY_ID']]['DATE_PAYED'])) {
                    $allInvoice[$arOrder['UF_COMPANY_ID']]['DATE_PAYED'] = $arOrder["PAY_VOUCHER_DATE"];
                    $allCompany[$arOrder['UF_COMPANY_ID']]['DATE_PAYED'] = $arOrder["PAY_VOUCHER_DATE"];
                    $allCompany[$arOrder['UF_COMPANY_ID']]['SHOWELEMENTWITHSCHET'] = "Y";
                } else {
                    if (strtotime($allInvoice[$arOrder['UF_COMPANY_ID']]['DATE_PAYED']) < strtotime($arOrder["PAY_VOUCHER_DATE"])) {
                        $allInvoice[$arOrder['UF_COMPANY_ID']]['DATE_PAYED'] = $arOrder["PAY_VOUCHER_DATE"];
                        $allCompany[$arOrder['UF_COMPANY_ID']]['DATE_PAYED'] = $arOrder["PAY_VOUCHER_DATE"];
                        $allCompany[$arOrder['UF_COMPANY_ID']]['SHOWELEMENTWITHSCHET'] = "Y";
                    }
                }
                if (empty($allInvoice[$arOrder['UF_COMPANY_ID']][$arBasket['PRODUCT_ID']]['COUNTPAY'])) {
                    $arBasket["QUANTITY"] = str_replace(".0000", "", $arBasket["QUANTITY"]);
                    $allInvoice[$arOrder['UF_COMPANY_ID']][$arBasket['PRODUCT_ID']]['COUNTPAY'] = $arBasket["QUANTITY"];
                    $allCompany[$arOrder['UF_COMPANY_ID']][$arBasket['PRODUCT_ID']]['COUNTPAY'] = $arBasket["QUANTITY"];

                    //pre("1) ".$arOrder["ID"]."   ".$arOrder['UF_COMPANY_ID']."   ".$arBasket["QUANTITY"]);
                    $allCompany[$arOrder['UF_COMPANY_ID']]['SHOWELEMENTWITHSCHET'] = "Y";
                } else {
                    $arBasket["QUANTITY"] = str_replace(".0000", "", $arBasket["QUANTITY"]);
                    $allInvoice[$arOrder['UF_COMPANY_ID']][$arBasket['PRODUCT_ID']]['COUNTPAY'] += $arBasket["QUANTITY"];
                    $allCompany[$arOrder['UF_COMPANY_ID']][$arBasket['PRODUCT_ID']]['COUNTPAY'] += $arBasket["QUANTITY"];
                    //pre("2) ".$arOrder["ID"]."   ".$arOrder['UF_COMPANY_ID']."   ".$arBasket["QUANTITY"]);
                }

                if (empty($allInvoice[$arOrder['UF_COMPANY_ID']][$arBasket['PRODUCT_ID']]['PRICE'])) {
                    $arBasket["PRICE"] = str_replace(".0000", "", $arBasket["PRICE"]);
                    $allInvoice[$arOrder['UF_COMPANY_ID']][$arBasket['PRODUCT_ID']]['PRICE'] = $arBasket["PRICE"] * $arBasket["QUANTITY"];
                    $allCompany[$arOrder['UF_COMPANY_ID']][$arBasket['PRODUCT_ID']]['PRICE'] = $arBasket["PRICE"] * $arBasket["QUANTITY"];
                    $allCompany[$arOrder['UF_COMPANY_ID']][$arBasket['PRODUCT_ID']]['COUNTPRICE'] = $arBasket["QUANTITY"];
                    $allCompany[$arOrder['UF_COMPANY_ID']][$arBasket['PRODUCT_ID']]['SHOWELEMENTWITHSCHET'] = "Y";
                    /*if($arOrder['UF_COMPANY_ID']=="6300"){
                    pre($arBasket);
    }*/
                } else {
                    $arBasket["PRICE"] = str_replace(".0000", "", $arBasket["PRICE"]);
                    $allInvoice[$arOrder['UF_COMPANY_ID']][$arBasket['PRODUCT_ID']]['PRICE'] += $arBasket["PRICE"] * $arBasket["QUANTITY"];
                    $allCompany[$arOrder['UF_COMPANY_ID']][$arBasket['PRODUCT_ID']]['PRICE'] += $arBasket["PRICE"] * $arBasket["QUANTITY"];
                    $allCompany[$arOrder['UF_COMPANY_ID']][$arBasket['PRODUCT_ID']]['COUNTPRICE'] += $arBasket["QUANTITY"];
                    /*if($arOrder['UF_COMPANY_ID']=="6300"){
                    pre("2_PRICE) ".$arOrder["ID"]."   ".$arOrder['UF_COMPANY_ID']."   ".$arBasket["PRICE"]);
                pre("2_COUNTPRICE) ".$arOrder["ID"]."   ".$arOrder['UF_COMPANY_ID']."   ".$arBasket["COUNTPRICE"]);
    }*/
                }

                if (empty($allInvoice[$arOrder['UF_COMPANY_ID']][$arBasket['PRODUCT_ID']]['LASTPRICE'])) {
                    $allInvoice[$arOrder['UF_COMPANY_ID']][$arBasket['PRODUCT_ID']]['LASTPRICE'] = $arBasket["PRICE"];
                    $allCompany[$arOrder['UF_COMPANY_ID']][$arBasket['PRODUCT_ID']]['LASTPRICE'] = $arBasket["PRICE"];
                    $allCompany[$arOrder['UF_COMPANY_ID']][$arBasket['PRODUCT_ID']]['SHOWELEMENTWITHSCHET'] = "Y";
                }
            }
        }
    }

}


