<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();


global $PRODUCT_ID;
global $SEGMENT;
global $allCompany;
$el = new CIBlockElement();
foreach ($allCompany as $index => $elementCompany) {
    if ($elementCompany['SHOWELEMENTWITHSCHET'] == "Y") {
        $total_KKT += $elementCompany['KKT'];

        foreach ($elementCompany as $index_price => $elementCompanyprice) {
            $total_COUNTPAY[$index_price] += $elementCompany[$index_price]['COUNTPAY'];
            $total_PRICE[$index_price] += $elementCompany[$index_price]['PRICE'] / $elementCompany[$index_price]['COUNTPRICE'];
            $total_LASTPRICE[$index_price] += $elementCompany[$index_price]['LASTPRICE'];
        }
    }
}

$sec = new CIBlockSection();

$arNeededSections = array();
$rsSec = $sec->GetList(
    array("ID" => "ASC"),
    array("IBLOCK_ID" => GetIBlockIDByCode("products_catalog"), "ACTIVE" => "Y"),
    false,
    array("ID", "NAME")
);
while ($arSec = $rsSec->GetNext()) {
    if ($arSec["NAME"] == 'Услуги') {
        $arNeededSections[] = $arSec["ID"];
    }
}

$arProds = array();
$rsGroup = $el->GetList(
    array("NAME" => "ASC"),
    array("IBLOCK_ID" => GetIBlockIDByCode("products_catalog"), "NAME" => "Код%", "ACTIVE" => "Y", "SECTION_ID" => $arNeededSections),
    false,
    false,
    array("ID", "NAME")
);
while ($arProd = $rsGroup->GetNext()) {
    $arProds[$arProd["ID"]] = $arProd["NAME"];
}


/** @var CBitrixComponentTemplate $this */
$this->IncludeLangFile('template.php');
?>
<meta http-equiv="Content-type" content="text/html;charset=<?echo LANG_CHARSET?>" />
<style type="text/css">
    .report-red-neg-val { color: red; }
</style>



<table border="1">
    <thead>
    <tr>
        <th>
            <div class="reports-head-cell">
                <span class="reports-table-arrow"></span>
                <span class="reports-head-cell-title">№ п/п</span>
            </div>
        </th>
        <th>
            <div class="reports-head-cell">
                <span class="reports-table-arrow"></span>
                <span class="reports-head-cell-title">Ответственный</span>
            </div>
        </th>
        <th>
            <div class="reports-head-cell">
                <span class="reports-table-arrow"></span>
                <span class="reports-head-cell-title">Наименование компании партнера</span>
            </div>
        </th>
        <th>
            <div class="reports-head-cell">
                <span class="reports-table-arrow"></span>
                <span class="reports-head-cell-title">Сегмент компании партнера</span>
            </div>
        </th>


        <?

        if (!empty($PRODUCT_ID)) {
            foreach ($PRODUCT_ID as $prodID):?>
                <th>
                    <div class="reports-head-cell">
                        <span class="reports-table-arrow"></span>
						<span class="reports-head-cell-title">Количество оплаченных КА, шт.: <?= $arProds[$prodID] ?></span>
                    </div>
                </th>
                <th>
                    <div class="reports-head-cell">
                        <span class="reports-table-arrow"></span>
						<span class="reports-head-cell-title">Средняя стоимость 1 КА, руб.: <?= $arProds[$prodID] ?></span>
                    </div>
                </th>
                <th>
                    <div class="reports-head-cell">
                        <span class="reports-table-arrow"></span>
						<span class="reports-head-cell-title">Стоимость 1 КА по последнему счету, руб.: <?= $arProds[$prodID] ?></span>
                    </div>
                </th>
            <? endforeach;
        } ?>


        <th>
            <div class="reports-head-cell">
                <span class="reports-table-arrow"></span>
                <span class="reports-head-cell-title">Количество ККТ, активированных с ID агента, шт.</span>
            </div>
        </th>
        <th>
            <div class="reports-head-cell">
                <span class="reports-table-arrow"></span>
                <span class="reports-head-cell-title">Потенциал ККТ, шт.</span>
            </div>
        </th>
        <th>
            <div class="reports-head-cell">
                <span class="reports-table-arrow"></span>
                <span class="reports-head-cell-title">Дата последней оплаты счета</span>
            </div>
        </th>
    </tr>
    <!-- head -->





    </thead>
    <tbody>
    <?
    $arTotalCount = array();
    $arTotalSumm = array();
    $totalCountKKTAgent = 0;
    $totalMoneyRS = 0;
    $i = 1;
    foreach ($allCompany as $index => $elementCompany) {
        if ($elementCompany['SHOWELEMENTWITHSCHET'] == "Y") {
            ?>
            <tr class="reports-list-item">
                <td class="reports-first-column"><?= $i++; ?></td>
                <td class="reports-last-column">
                    <?
                    //echo $elementCompany['ASSIGNED_BY_ID']."<br>";
                    $rsUser = $USER->GetByID($elementCompany['ASSIGNED_BY_ID']);
                    $arUser = $rsUser->Fetch();
                    echo $arUser["LAST_NAME"] . ' ' . $arUser["NAME"];
                    ?>
                </td>
                <td class="reports-last-column"><? /*echo $index."<br>"; */
                    echo $elementCompany['TITLE']; ?></td>
                <td class="reports-last-column">
                    <?

                    //echo $elementCompany['UF_SEGMENT']."<br>";

                    $rsGroup = $el->GetList(
                        array("NAME" => "ASC"),
                        array("IBLOCK_ID" => GetIBlockIDByCode("segments"), "ACTIVE" => "Y", "ID" => $elementCompany['UF_SEGMENT']),
                        false,
                        false,
                        array("ID", "NAME")
                    );
                    if ($arProd = $rsGroup->GetNext()) {
                        echo $arProd["NAME"];
                    }

                    ?>
                </td>

                <?
                if (!empty($PRODUCT_ID)) {
                    foreach ($PRODUCT_ID as $prodID):?>
                        <td class="reports-last-column reports-numeric-column"><?
                            if (empty($elementCompany[$prodID]['COUNTPAY'])) {
                                echo "0";
                            } else {
                                echo $elementCompany[$prodID]['COUNTPAY'];
                            } ?>
                        </td>
                        <td class="reports-last-column reports-numeric-column">
                            <? echo number_format($elementCompany[$prodID]['PRICE'] / $elementCompany[$prodID]['COUNTPRICE'], 2, ',', ''); ?>
                        </td>
                        <td class="reports-last-column reports-numeric-column">
                            <? echo number_format($elementCompany[$prodID]['LASTPRICE'], 2, ',', ''); ?>
                        </td>
                    <? endforeach;
                } ?>

                <td class="reports-last-column reports-numeric-column"><?= $elementCompany['KKT']; ?></td>
                <td class="reports-last-column reports-numeric-column">
                    <?

                    if (!empty($elementCompany['KKTPLAN'])) {
                        //echo $elementCompany['KKTPLAN']."<br>";
                        $rsGender = CUserFieldEnum::GetList(array(), array("ID" => $elementCompany['KKTPLAN']));
                        if ($arCat = $rsGender->GetNext())
                            echo $arCat["VALUE"];
                    }
                    ?>
                </td>
                <td class="reports-last-column reports-numeric-column"><?= $elementCompany['DATE_PAYED']; ?></td>

            </tr>
            <?
        }
    } ?>
    <tr>
        <td class="reports-pretotal-column">

            <span style="font-size: 14px;">Всего:</span>
        </td>
    </tr>
    <tr>
        <td class="reports-first-column reports-total-column" style="background-color: #F0F0F0;">№ п/п</td>
        <td class="reports-first-column reports-total-column" style="background-color: #F0F0F0;">
            Ответственный
        </td>
        <td class="reports-first-column reports-total-column" style="background-color: #F0F0F0;">
            Наименование компании партнера
        </td>
        <td class="reports-first-column reports-total-column" style="background-color: #F0F0F0;">Сегмент
            компании партнера
        </td>


        <!--<td class="reports-first-column reports-total-column" style="background-color: #F0F0F0;">Количество оплаченных КА, шт.:</td>
        <td class="reports-first-column reports-total-column" style="background-color: #F0F0F0;">Средняя стоимость 1 КА, руб.:</td>
        <td class="reports-first-column reports-total-column" style="background-color: #F0F0F0;">Стоимость 1 КА по последнему счету, руб.:</td>-->

        <? if (!empty($PRODUCT_ID)) {
            foreach ($PRODUCT_ID as $prodID):?>
                <td class="reports-first-column reports-total-column" style="background-color: #F0F0F0;">
                    Количество оплаченных КА, шт.: <?= $arProds[$prodID] ?>
                </td>

                <td class="reports-first-column reports-total-column" style="background-color: #F0F0F0;">
                    Средняя стоимость 1 КА, руб.: <?= $arProds[$prodID] ?>
                </td>

                <td class="reports-first-column reports-total-column" style="background-color: #F0F0F0;">
                    Стоимость 1 КА по последнему счету, руб.: <?= $arProds[$prodID] ?>
                </td>
            <? endforeach;
        } ?>


        <td class="reports-first-column reports-total-column" style="background-color: #F0F0F0;">Количество
            ККТ, активированных с ID агента, шт.
        </td>
        <td class="reports-first-column reports-total-column" style="background-color: #F0F0F0;">Потенциал
            ККТ, шт.
        </td>
        <td class="reports-total-column" style="background-color: #F0F0F0;">Дата последней оплаты счета</td>
    </tr>
    <tr>
        <td class="reports-first-column">Все</td>
        <td class="reports-numeric-column">-</td>
        <td class="reports-numeric-column">-</td>
        <td class="reports-numeric-column">-</td>


        <?
        if (!empty($PRODUCT_ID)) {
            foreach ($PRODUCT_ID as $prodID):?>
                <td class="reports-numeric-column"><?
                    if (empty($total_COUNTPAY[$prodID])) {
                        echo "0";
                    } else {
                        echo $total_COUNTPAY[$prodID];
                    } ?>
                </td>
                <td class="reports-numeric-column">
                    <? echo number_format($total_PRICE[$prodID], 2, ',', ''); ?>
                </td>
                <td class="reports-numeric-column">
                    <? echo number_format($total_LASTPRICE[$prodID], 2, ',', ''); ?>
                </td>
            <? endforeach;
        } ?>


        <!--	<td class="reports-numeric-column"><?= $total_COUNTPAY; ?></td>
					<td class="reports-numeric-column"><?= number_format($total_PRICE, 2, ',', ''); ?></td>
					<td class="reports-numeric-column"><?= number_format($total_LASTPRICE, 2, ',', ''); ?></td>-->


        <td class="reports-numeric-column"><?= $total_KKT; ?></td>
        <td class="reports-numeric-column">-</td>
        <td class="reports-numeric-column">-</td>
    </tr>
    </tbody>
</table>