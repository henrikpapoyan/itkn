<?
/** @global CUser $USER */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$requiredModules = array('report');
$arResult = array();
//pre(array('$_REQUEST' => $_REQUEST));

foreach ($requiredModules as $requiredModule)
{
	if (!CModule::IncludeModule($requiredModule))
	{
		ShowError(GetMessage("F_NO_MODULE"));
		return 0;
	}
}

if (!isset($arParams['REPORT_HELPER_CLASS']) || strlen($arParams['REPORT_HELPER_CLASS']) < 1)
{
	ShowError(GetMessage("REPORT_HELPER_NOT_DEFINED"));
	return 0;
}

// Suppress the timezone, while report works in server time
CTimeZone::Disable();

use Bitrix\Main\Entity;

// <editor-fold defaultstate="collapsed" desc="period types">
$periodTypes =
$arResult['periodTypes'] = array(
	'month',
	'month_ago',
	'week',
	'week_ago',
	'days',
	'after',
	'before',
	'interval',
	'all'
);
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="chart types">
if ($arParams['USE_CHART'])
{
	$arResult['chartTypes'] = array(
		array('id' => 'line', 'name' => GetMessage('REPORT_CHART_TYPE_LINE'), 'value_types' => array(
			/*'boolean', 'date', 'datetime', */
			'float', 'integer'/*, 'string', 'text', 'enum', 'file', 'disk_file', 'employee', 'crm', 'crm_status',
			'iblock_element', 'iblock_section'*/
		)),
		array('id' => 'bar', 'name' => GetMessage('REPORT_CHART_TYPE_BAR'), 'value_types' => array(
			/*'boolean', 'date', 'datetime', */
			'float', 'integer'/*, 'string', 'text', 'enum', 'file', 'disk_file', 'employee', 'crm', 'crm_status',
			'iblock_element', 'iblock_section'*/
		)),
		array('id' => 'pie', 'name' => GetMessage('REPORT_CHART_TYPE_PIE'), 'value_types' => array(
			/*'boolean', 'date', 'datetime', */
			'float', 'integer'/*, 'string', 'text', 'enum', 'file', 'disk_file', 'employee', 'crm', 'crm_status',
			'iblock_element', 'iblock_section'*/
		))
	);
}
// </editor-fold>

// get view params
$strReportViewParams = CReport::getViewParams($arParams['REPORT_ID'], $this->GetTemplateName());
if (isset($_GET['set_filter']))
{
    if (substr($_SERVER['QUERY_STRING'], 0, 6) !== 'EXCEL=')
    {
        if ($_SERVER['QUERY_STRING'] !== $strReportViewParams)
        {
            CReport::setViewParams($arParams['REPORT_ID'], $this->GetTemplateName(), $_SERVER['QUERY_STRING']);
        }
    }
}
else
{
    if (!empty($strReportViewParams))
    {
        if (!is_set($_GET['sort_id']))
        {
            $len = strpos($arParams['PATH_TO_REPORT_VIEW'], '?');

            if ($len === false) $redirectUrl = $arParams['PATH_TO_REPORT_VIEW'];
            else $redirectUrl = substr($arParams['PATH_TO_REPORT_VIEW'], 0, $len);
            $redirectUrl = CComponentEngine::makePathFromTemplate($redirectUrl, array('report_id' => $arParams['REPORT_ID']));
            $redirectUrl .= '?'.$strReportViewParams;
            LocalRedirect($redirectUrl);
        }
        else
        {
            CReport::clearViewParams($arParams['REPORT_ID']);
        }
    }
}
// select report info/settings
$report = array();
$result = false;
if (intval($arParams['REPORT_ID']) > 0)
{
    $result = Bitrix\Report\ReportTable::getById($arParams['REPORT_ID']);
}
if (is_object($result))
{
    $report = $result->fetch();
}

if (empty($report))
{
    throw new BXUserException(sprintf(GetMessage('REPORT_NOT_FOUND'), $arParams['REPORT_ID']));
}

$userId = $USER->GetID();

$rightsManager = new Bitrix\Report\RightsManager($userId);
if(!$rightsManager->canRead($report['ID']))
    throw new BXUserException(GetMessage('REPORT_VIEW_PERMISSION_DENIED'));

$arResult['AUTHOR'] = true;
if($userId != $report['CREATED_BY'])
    $arResult['AUTHOR'] = false;

$arResult['MARK_DEFAULT'] = 0;
if (isset($report['MARK_DEFAULT']))
{
    $arResult['MARK_DEFAULT'] = intval($report['MARK_DEFAULT']);
}

// action
$settings = unserialize($report['SETTINGS']);

$date_from = $date_to = null;
$form_date = array('from' => null, 'to' => null, 'days' => null);
$startSearch = false;

//pre($_REQUEST);
// <editor-fold defaultstate="collapsed" desc="get value from POST or DB">
if (!empty($_GET['F_DATE_TYPE']) && in_array($_GET['F_DATE_TYPE'], $periodTypes, true))
{
    $startSearch = true;
    $period = array('type' => $_GET['F_DATE_TYPE']);

    switch ($_GET['F_DATE_TYPE'])
    {
        case 'days':
            $days = !empty($_GET['F_DATE_DAYS']) ? (int) $_GET['F_DATE_DAYS'] : 1;
            $period['value'] = $days ? $days : 1;
            break;

        case 'after':
            $date = !empty($_GET['F_DATE_TO']) ? (string) $_GET['F_DATE_TO'] : ConvertTimeStamp(false, 'SHORT');
            $date = MakeTimeStamp($date);
            $period['value'] = $date ? $date : time();
            break;

        case 'before':
            $date = !empty($_GET['F_DATE_FROM']) ? (string) $_GET['F_DATE_FROM'] : ConvertTimeStamp(false, 'SHORT');
            $date = MakeTimeStamp($date);
            $period['value'] = $date ? $date + (3600*24-1) : time() + (3600*24-1);
            break;

        case 'interval':
            //Костыль для нескольких периодов для отчета "Отчет по суммам оплаченных и не оплаченных счетов"
            if(!empty($_GET['F_DATE_FROM']) && is_array($_GET['F_DATE_FROM'])){
                foreach($_GET['F_DATE_FROM'] as $key => $dateStartFilter){
                    $dateEndFilter = $_GET['F_DATE_TO'][$key];
                    if($dateStartFilter != '' || $dateEndFilter != ''){
                        $date_f = !empty($dateStartFilter) ? (string) $dateStartFilter : '';
                        $date_f = MakeTimeStamp($date_f);
                        $date_t = !empty($_GET['F_DATE_TO'][$key]) ? (string) $_GET['F_DATE_TO'][$key] : ConvertTimeStamp(false, 'SHORT');
                        $date_t = MakeTimeStamp($date_t);
                        if ($date_f || $date_t)
                        {
                            $period['value'][] = array(
                                "0" => $date_f ? $date_f : '',
                                "1" => $date_t ? $date_t + (3600*24-1) : time() + (3600*24-1)
                            );
                        }
                    }
                }
            } else {
                $date_f = !empty($_GET['F_DATE_FROM']) ? (string) $_GET['F_DATE_FROM'] : ConvertTimeStamp(false, 'SHORT');
                $date_f = MakeTimeStamp($date_f);
                $date_t = !empty($_GET['F_DATE_TO']) ? (string) $_GET['F_DATE_TO'] : ConvertTimeStamp(false, 'SHORT');
                $date_t = MakeTimeStamp($date_t);
                if ($date_f || $date_t)
                {
                    $period['value'][0] = $date_f ? $date_f : time();
                    $period['value'][1] = $date_t ? $date_t + (3600*24-1) : time() + (3600*24-1);
                }
            }
            break;

        default:
            $period['value'] = null;
    }
}
else
{
    $period = $settings['period'];
}

//pre(array("period" => $period));

// <editor-fold defaultstate="collapsed" desc="parse period">
switch ($period['type'])
{
    case 'month':
        $date_from = strtotime(date("Y-m-01"));
        if($report["TITLE"] == "Отчет по суммам оплаченных и не оплаченных счетов") {
            $form_date['from'][] = ConvertTimeStamp($date_from, 'SHORT');
            $form_date['to'][] = '';
        }
        break;

    case 'month_ago':
        $date_from = strtotime(date("Y-m-01", strtotime("-1 month")));
        $date_to = strtotime(date("Y-m-t", strtotime("-1 month"))) + (3600*24-1);
        break;

    case 'week':
        $date_from = strtotime("-".((date("w") == 0 ? 7 : date("w")) - 1)." day 00:00");
        break;

    case 'week_ago':
        $date_from = strtotime("-".((date("w") == 0 ? 7 : date("w")) + 6)." day 00:00");
        $date_to = strtotime("-".(date("w") == 0 ? 7 : date("w"))." day 23:59:59");
        break;

    case 'days':
        $date_from = strtotime(date("Y-m-d")." -".intval($period['value'])." day");
        $form_date['days'] = intval($period['value']);
        break;

    case 'after':
        $date_from = $period['value'];
        $form_date['to'] = ConvertTimeStamp($period['value'], 'SHORT');
        break;

    case 'before':
        $date_to = $period['value'];
        $form_date['from'] = ConvertTimeStamp($period['value'], 'SHORT');
        break;

    case 'interval':
        if(!is_array($period['value'][0])){
            list($date_from, $date_to) = $period['value'];
            $form_date['from'] = ConvertTimeStamp($date_from, 'SHORT');
            $form_date['to'] = ConvertTimeStamp($date_to, 'SHORT');
        } else {
            foreach($period['value'] as $periodVal){
                list($date_from, $date_to) = $periodVal;
                $form_date['from'][] = !empty($date_from) ? ConvertTimeStamp($date_from, 'SHORT') : '';
                $form_date['to'][] = ConvertTimeStamp($date_to, 'SHORT');
            }
        }
        break;
}

//pre(array("form_date" => $form_date));

//pre(array('$report' => $report));

$site_date_from = !is_null($date_from) ? ConvertTimeStamp($date_from, 'FULL') : null;
$site_date_to = !is_null($date_to) ? ConvertTimeStamp($date_to, 'FULL') : null;

//echo '$site_date_from = '.$site_date_from.' | $site_date_to = '.$site_date_to.'<br />';

$CSaleOrder = new CSaleOrder();
$CSaleBasket = new CSaleBasket();
$CCrmCompany = new CCrmCompany();
global $DB;
$el = new CIBlockElement();

$arFilterIB = array(
    "IBLOCK_ID" => GetIBlockIDByCode("registry_kkt"),
    "PROPERTY_ACTIVE_VALUE" => "Да",
    "!PROPERTY_CODE_AGENT" => false,
    "!PROPERTY_COMPANY" => false,
);
$arFilterOrder = array(
    /*'PAYED' => 'Y',*/
    "STATUS_ID"=>"P"
);

$arFilterReestrPay = array(
    "IBLOCK_ID" => GetIBlockIDByCode("reestr_pay"),
    "PROPERTY_TYPE_VALUE" => "Пополнение",
    "!PROPERTY_COMPANY" => false,
);

if(!empty($site_date_from)){
    $arFilterOrder[">=PAY_VOUCHER_DATE"] = $site_date_from;
    $dateFrom = strtotime($site_date_from);
    $arFilterIB[">=PROPERTY_DATE_ACTIVE"] = date('Y-m-d', $dateFrom);
    $arFilterReestrPay[">=PROPERTY_ACTIVATION_DATE"] = date('Y-m-d', $dateFrom);
}
if(!empty($site_date_to)){
    $arFilterOrder["<=PAY_VOUCHER_DATE"] = $site_date_to;
    $dateTo = strtotime($site_date_to);
    $arFilterIB["<=PROPERTY_DATE_ACTIVE"] = date('Y-m-d', $dateTo);
    $arFilterReestrPay["<=PROPERTY_ACTIVATION_DATE"] = date('Y-m-d', $dateTo);
}
$arAllKKTbyResponsible = array();
//pre(array('$arFilterIB'=>$arFilterIB));

$arFilterNew = array();
$productsGroup = array();
//Проверяем, выбраны ли товары в списке
if(isset($_REQUEST["products-group"]) && !empty($_REQUEST["products-group"])) {
    $productsGroup = $_REQUEST["products-group"];
    $arFilterNew["PRODUCTS"] = $productsGroup;
}

//Проверяем, выбраны ли ответственные
$arSelectRespUser = array();
if(isset($_REQUEST["select_responsible_user"]) && !empty($_REQUEST["select_responsible_user"])) {
    $arSelectRespUser = explode(',', $_REQUEST["select_responsible_user"]);
    $arFilterOrder["RESPONSIBLE_ID"] = $arSelectRespUser;
    //pre(array('$arSelectRespUser' => $arSelectRespUser));
    foreach($arSelectRespUser as $userID){
        $arResult["DATA_ROW"][$userID] = array(
            "PRODUCTS" => array(
                "0" => array(
                    "TITLE" => "Все",
                    "COUNT" => 0,
                    "SUMM" => 0,
                ),
            ),
            "COUNT_KKT_AGENT" => 0,
            "MONEY_RS" => 0,
            "MONEY_LS" => 0,
            "MONEY_RS_BY_ORDER" => 0,
        );
    }
}

if($report["TITLE"] == "Отчет по количеству продаж и получению средств"){
    //Получаем все компании
    $strSql = "SELECT ID, ASSIGNED_BY_ID FROM b_crm_company";
    $rsCompany = $DB->Query($strSql, false, $err_mess.__LINE__);
    $allCompany = array();
    while($arCompany = $rsCompany->Fetch()){
        $allCompany[$arCompany["ID"]] = $arCompany["ASSIGNED_BY_ID"];
    }

    //Получаем данные ККТ
    $rsKKT = $el->GetList(
        array("ID"=>"ASC"),
        $arFilterIB,
        false,
        false,
        array("ID", "NAME", "PROPERTY_COMPANY", "PROPERTY_ACTIVE", "PROPERTY_CODE_AGENT", "PROPERTY_DATE_CONNECT")
    );
    //echo 'Найдено ККТ :'.$rsKKT->SelectedRowsCount().'<br />';
    $showCount = 0;
    $arAgentKKT = array();
    while($arKKt = $rsKKT->GetNext()){
        $arReq = getCompanyIDbyINN($arKKt["PROPERTY_CODE_AGENT_VALUE"]);
        $respIDTemp = $allCompany[$arReq["ENTITY_ID"]];
        //echo 'По ИНН = '.$arKKt["PROPERTY_CODE_AGENT_VALUE"].' найден контрагент с ID '.$arReq["ENTITY_ID"].' по которому ответственный пользователь с ID = '.$allCompany[$arReq["ENTITY_ID"]].'<br />';
        if($arReq){
            if(empty($arSelectRespUser)) {
                if (!isset($arResult["DATA_ROW"][$respIDTemp])) {
                    $arResult["DATA_ROW"][$respIDTemp] = array(
                        "PRODUCTS" => array(
                            "0" => array(
                                "TITLE" => "Все",
                                "COUNT" => 0,
                                "SUMM" => 0,
                            ),
                        ),
                        "COUNT_KKT_AGENT" => 0,
                        "MONEY_RS" => 0,
                        "MONEY_LS" => 0,
                        "MONEY_RS_BY_ORDER" => 0,
                    );
                }
                $arResult["DATA_ROW"][$respIDTemp]["COUNT_KKT_AGENT"]++;
            } else {
                if(in_array($respIDTemp, $arSelectRespUser)){
                    if (!isset($arResult["DATA_ROW"][$respIDTemp])) {
                        $arResult["DATA_ROW"][$respIDTemp] = array(
                            "PRODUCTS" => array(
                                "0" => array(
                                    "TITLE" => "Все",
                                    "COUNT" => 0,
                                    "SUMM" => 0,
                                ),
                            ),
                            "COUNT_KKT_AGENT" => 0,
                            "MONEY_RS" => 0,
                            "MONEY_LS" => 0,
                            "MONEY_RS_BY_ORDER" => 0,
                        );
                    }
                    $arResult["DATA_ROW"][$respIDTemp]["COUNT_KKT_AGENT"]++;
                }
            }
        }
    }

    //Получаем данные по поступлению на л/с (ИБ "Реестр платежей")
    $rsLS = $el->GetList(
        array("ID"=>"ASC"),
        $arFilterReestrPay,
        false,
        false,
        array("ID", "NAME", "PROPERTY_SUMM", "PROPERTY_TYPE", "PROPERTY_COMPANY")
    );
    //echo 'Найдено записей в "Реестр платежей" :'.$rsLS->SelectedRowsCount().'<br />';
    $reestrPayByCompany = array();
    while($arLS = $rsLS->GetNext()){
        $respIDTemp = $allCompany[$arLS["PROPERTY_COMPANY_VALUE"]];
        if(empty($arSelectRespUser)) {
            if (!isset($arResult["DATA_ROW"][$respIDTemp])) {
                $arResult["DATA_ROW"][$respIDTemp] = array(
                    "PRODUCTS" => array(
                        "0" => array(
                            "TITLE" => "Все",
                            "COUNT" => 0,
                            "SUMM" => 0,
                        ),
                    ),
                    "COUNT_KKT_AGENT" => 0,
                    "MONEY_RS" => 0,
                    "MONEY_LS" => 0,
                    "MONEY_RS_BY_ORDER" => 0,
                );
            }
            $arResult["DATA_ROW"][$respIDTemp]["MONEY_LS"] += $arLS["PROPERTY_SUMM_VALUE"];
        } else {
            if(in_array($respIDTemp, $arSelectRespUser)) {
                if (!isset($arResult["DATA_ROW"][$respIDTemp])) {
                    $arResult["DATA_ROW"][$respIDTemp] = array(
                        "PRODUCTS" => array(
                            "0" => array(
                                "TITLE" => "Все",
                                "COUNT" => 0,
                                "SUMM" => 0,
                            ),
                        ),
                        "COUNT_KKT_AGENT" => 0,
                        "MONEY_RS" => 0,
                        "MONEY_LS" => 0,
                        "MONEY_RS_BY_ORDER" => 0,
                    );
                }
                $arResult["DATA_ROW"][$respIDTemp]["MONEY_LS"] += $arLS["PROPERTY_SUMM_VALUE"];
            }
        }
    }
}

//pre($reestrPayByCompany);



//Получаем данные по Счетам
$arOrders = array();
$rsOrders = CSaleOrder::GetList(
    array('ID' => 'DESC'),
    $arFilterOrder,
    false,
    false,
    array("ID", "PAY_VOUCHER_DATE", "STATUS_ID", "PRICE", "SUM_PAID", "RESPONSIBLE_ID")
);
//echo 'Найдено счетов:'.$rsOrders->SelectedRowsCount().'<br />';

//DATE_BILL - Дата выставления
if($startSearch) {
    if ($report["TITLE"] == "Отчет по суммам оплаченных и не оплаченных счетов") {
        //Получаем все выставленный в этот период счета
        $arResult["DATA_ROW"] = array();
        foreach ($form_date["from"] as $key => $filterPeriodStart) {
            $strFilter = '';
            $filterPeriodStart = strtotime($filterPeriodStart);
            $filterPeriodEnd = strtotime($form_date["to"][$key]);
            //echo '$filterPeriodStart = '.$filterPeriodStart.' | $filterPeriodEnd = '.$filterPeriodEnd.'<br />';
            $periodText = !empty($filterPeriodStart) ? ConvertTimeStamp($filterPeriodStart, 'SHORT') : '...';
            $periodText .= ' - ';
            $periodText .= !empty($filterPeriodEnd) ? ConvertTimeStamp($filterPeriodEnd, 'SHORT') : '...';
            //echo '$periodText = '.$periodText.'<br />';
            if (!empty($filterPeriodStart)) {
                $site_date_from = !is_null($filterPeriodStart) ? ConvertTimeStamp($filterPeriodStart, 'FULL') : null;
                $dateFrom = strtotime($site_date_from);
                $strFilter = " AND `DATE_BILL` >= '" . date('Y-m-d', $dateFrom) . "'";
            }
            if (!empty($filterPeriodEnd)) {
                $site_date_to = !is_null($filterPeriodEnd) ? ConvertTimeStamp($filterPeriodEnd, 'FULL') : null;
                $dateTo = strtotime($site_date_to);
                $strFilter .= " AND `DATE_BILL` <= '" . date('Y-m-d', $dateTo) . "'";
            }
            //echo '$dateFrom = '.$dateFrom.' | $dateTo = '.$dateTo.'<br />';
            //echo '$strFilter = '.$strFilter.'<br />';
            $arResult["DATA_ROW"][$periodText] = array();
            if (!empty($arSelectRespUser)) {
                $strFilter .= ' AND `RESPONSIBLE_ID` IN (' . implode(',', $arSelectRespUser) . ')';
                foreach ($arSelectRespUser as $userID) {
                    $arResult["DATA_ROW"][$periodText][$userID] = array(
                        "PRODUCTS" => array(
                            "0" => array(
                                "TITLE" => "Все",
                                "COUNT" => 0,
                                "SUMM" => 0,
                            ),
                        ),
                        "COUNT_KKT_AGENT" => 0,
                        "MONEY_RS" => 0,
                        "MONEY_LS" => 0,
                        "MONEY_RS_BY_ORDER" => 0,
                        "AMOUNT_INVOICES" => array(
                            "ISSUED" => 0,
                            "ISSUED_PAYED" => 0,
                            "ISSUED_NOT_PAYED_NOT_OVER" => 0,
                            "ISSUED_NOT_PAYED_OVER" => 0,
                            "ISSUED_NOT_PAYED" => 0,
                            "REJECTED" => 0,
                            "DELETED" => 0,
                        ),
                    );
                }
            }
            $strSql = "SELECT `ID`, `PAYED`, `DATE_PAYED`, `CANCELED`, `DATE_CANCELED`, `STATUS_ID`, `PRICE`, `SUM_PAID`, `RESPONSIBLE_ID`, `DATE_PAY_BEFORE`, `DATE_BILL` from `b_sale_order` WHERE (`STATUS_ID` != 'N' $strFilter) ORDER BY `DATE_BILL` ASC";
            //echo '$strSql ='.$strSql.'<br />';
            $rsInvoices = $DB->Query($strSql, false, $err_mess . __LINE__);
            echo '$rsInvoices->SelectedRowsCount() = '.$rsInvoices->SelectedRowsCount().'<br />';
            //$nullPayDateBefore = 0;
            while ($arInvoice = $rsInvoices->Fetch()) {
                $emplID = $arInvoice["RESPONSIBLE_ID"];
                if (!isset($arResult["DATA_ROW"][$periodText][$emplID])) {
                    $arResult["DATA_ROW"][$periodText][$emplID] = array(
                        "PRODUCTS" => array(
                            "0" => array(
                                "TITLE" => "Все",
                                "COUNT" => 0,
                                "SUMM" => 0,
                            ),
                        ),
                        "COUNT_KKT_AGENT" => 0,
                        "MONEY_RS" => 0,
                        "MONEY_LS" => 0,
                        "MONEY_RS_BY_ORDER" => $arInvoice["PRICE"],
                        "AMOUNT_INVOICES" => array(
                            "ISSUED" => 0,
                            "ISSUED_PAYED" => 0,
                            "ISSUED_NOT_PAYED_NOT_OVER" => 0,
                            "ISSUED_NOT_PAYED_OVER" => 0,
                            "ISSUED_NOT_PAYED" => 0,
                            "REJECTED" => 0,
                            "DELETED" => 0,
                        ),
                    );
                }
                /*if($arInvoice["DATE_PAY_BEFORE"] == '0000-00-00 00:00:00') {
                    $nullPayDateBefore++;
                }*/
                $arResult["DATA_ROW"][$periodText][$emplID]["AMOUNT_INVOICES"]["ISSUED"] += $arInvoice["PRICE"];
                if ($arInvoice["STATUS_ID"] == "Y") { // Удаленные счета
                    $arResult["DATA_ROW"][$periodText][$emplID]["AMOUNT_INVOICES"]["DELETED"] += $arInvoice["PRICE"];
                } elseif ($arInvoice["STATUS_ID"] == "D") { //Отмененные счета
                    $arResult["DATA_ROW"][$periodText][$emplID]["AMOUNT_INVOICES"]["REJECTED"] += $arInvoice["PRICE"];
                } else {
                    if ($arInvoice["STATUS_ID"] == "P") { //Оплаченные счета
                        $arResult["DATA_ROW"][$periodText][$emplID]["AMOUNT_INVOICES"]["ISSUED_PAYED"] += $arInvoice["PRICE"];
                    } else { //Не оплаченные счета
                        $datePayBefore = new DateTime($arInvoice["DATE_PAY_BEFORE"]);
                        if (!empty($site_date_to)) {
                            $datePeriodEnd = new DateTime($site_date_to);
                        } else {
                            $datePeriodEnd = new DateTime();
                        }
                        $arResult["DATA_ROW"][$periodText][$emplID]["AMOUNT_INVOICES"]["ISSUED_NOT_PAYED"] += $arInvoice["PRICE"];
                        if ($datePayBefore > $datePeriodEnd || $arInvoice["DATE_PAY_BEFORE"] == '0000-00-00 00:00:00') { // с НЕистекшим сроком оплаты
                            $arResult["DATA_ROW"][$periodText][$emplID]["AMOUNT_INVOICES"]["ISSUED_NOT_PAYED_NOT_OVER"] += $arInvoice["PRICE"];
                        } else { // с ИСТЕКШИМ сроком оплаты
                            $arResult["DATA_ROW"][$periodText][$emplID]["AMOUNT_INVOICES"]["ISSUED_NOT_PAYED_OVER"] += $arInvoice["PRICE"];
                        }
                    }
                }
            }
            //echo $periodText.' $nullPayDateBefore = '.$nullPayDateBefore.'<br />';
        }
    } else {
        $ordersID = array();
        while ($arOrder = $rsOrders->GetNext()) {
            //Формируем пустые данные
            $emplID = $arOrder["RESPONSIBLE_ID"];
            if (!isset($arResult["DATA_ROW"][$emplID])) {
                $arResult["DATA_ROW"][$emplID] = array(
                    "PRODUCTS" => array(
                        "0" => array(
                            "TITLE" => "Все",
                            "COUNT" => 0,
                            "SUMM" => 0,
                        ),
                    ),
                    "COUNT_KKT_AGENT" => 0,
                    "MONEY_RS" => 0,
                    "MONEY_LS" => 0,
                    "MONEY_RS_BY_ORDER" => $arOrder["PRICE"],
                    "AMOUNT_INVOICES" => array(
                        "ISSUED" => 0,
                        "ISSUED_PAYED" => 0,
                        "ISSUED_NOT_PAYED_NOT_OVER" => 0,
                        "ISSUED_NOT_PAYED_OVER" => 0,
                        "ISSUED_NOT_PAYED" => 0,
                        "REJECTED" => 0,
                        "DELETED" => 0,
                    ),
                );
            } else {
                $arResult["DATA_ROW"][$emplID]["MONEY_RS_BY_ORDER"] += $arOrder["PRICE"];
            }

            $rsBasket = $CSaleBasket->GetList(
                array("ID" => "ASC"),
                array("ORDER_ID" => $arOrder["ID"]),
                false,
                false,
                array("ID", "PRODUCT_ID", "NAME", "PRICE", "QUANTITY")
            );
            while ($arBasket = $rsBasket->GetNext()) {
                $arOrder["BASKET"][] = $arBasket;
                //Если не выбрано ни один товар.
                $prodID = $arBasket["PRODUCT_ID"];
                $prodQuan = $arBasket["QUANTITY"];
                $prodPrice = $arBasket["PRICE"];
                $prodName = $arBasket["NAME"];
                if (empty($arFilterNew["PRODUCTS"])) {
                    $ordersID[$arOrder["ID"]][] = $arBasket;
                    $arResult["DATA_ROW"][$emplID]["PRODUCTS"][0]["COUNT"] += $prodQuan;
                    $arResult["DATA_ROW"][$emplID]["PRODUCTS"][0]["SUMM"] += $prodPrice * $prodQuan;
                    $arResult["DATA_ROW"][$emplID]["MONEY_RS"] += $prodPrice * $prodQuan;
                } else {
                    if (in_array($prodID, $arFilterNew["PRODUCTS"])) {
                        $ordersID[$arOrder["ID"]][] = $arBasket;
                        if (!isset($arResult["DATA_ROW"][$emplID]["PRODUCTS"][$prodID])) {
                            $arResult["DATA_ROW"][$emplID]["PRODUCTS"][$prodID] = array(
                                "TITLE" => $prodName,
                                "COUNT" => 0,
                                "SUMM" => 0,
                            );
                        }
                        $arResult["DATA_ROW"][$emplID]["PRODUCTS"][$prodID]["COUNT"] += $prodQuan;
                        $arResult["DATA_ROW"][$emplID]["PRODUCTS"][$prodID]["SUMM"] += $prodPrice * $prodQuan;
                        $arResult["DATA_ROW"][$emplID]["MONEY_RS"] += $prodPrice * $prodQuan;
                    }
                }
            }
            $arOrders[$emplID][] = $arOrder;
        }
    }


    // <editor-fold defaultstate="collapsed" desc="collect changeables">
    $changeableFilters = array();
    $changeableFiltersEntities = array();
    //pre($_REQUEST);
    //pre(array("DATA_ROW ДО" => $arResult["DATA_ROW"]));
    $sortID = $_GET["sort_id"];
    $sortType = $_GET["sort_type"];
    $prodID = 0;

    if ($report["TITLE"] == "Отчет по суммам оплаченных и не оплаченных счетов") {
        $arDataRowNew = array();
        $arSortInvoice = array(
            'ISSUED',
            'ISSUED_PAYED',
            'ISSUED_NOT_PAYED_NOT_OVER',
            'ISSUED_NOT_PAYED_OVER',
            'ISSUED_NOT_PAYED',
            'REJECTED',
            'DELETED',
        );
        if ($sortID == "RESP_ID" || $sortID == '') {
            foreach ($arResult["DATA_ROW"] as $periodKey => $arPeriod) {
                $arSort = array();
                if (!empty($arPeriod)) {
                    foreach ($arPeriod as $userID => $data) {
                        if (!empty($userID)) {
                            $rsUser = $USER->GetByID($userID);
                            $arUser = $rsUser->Fetch();
                            $userName = $arUser["LAST_NAME"] . ' ' . $arUser["NAME"];
                            $arSort[$arUser["ID"]] = $userName;
                        }
                    }
                    if ($sortType == "ASC" || $sortType == "") {
                        asort($arSort);
                    }
                    if ($sortType == "DESC") {
                        arsort($arSort);
                    }
                    foreach ($arSort as $userID => $userName) {
                        $arDataRowNew[$periodKey][$userID] = $arResult["DATA_ROW"][$periodKey][$userID];
                    }
                } else {
                    $arDataRowNew[$periodKey] = array();
                }
            }
        }
        if (in_array($sortID, $arSortInvoice)) {
            foreach ($arResult["DATA_ROW"] as $periodKey => $arPeriod) {
                $arSort = array();
                if (!empty($arPeriod)) {
                    foreach ($arPeriod as $userID => $data) {
                        $arSort[$userID] = $data["AMOUNT_INVOICES"][$sortID];
                    }
                    if ($sortType == "ASC") {
                        asort($arSort);
                    }
                    if ($sortType == "DESC") {
                        arsort($arSort);
                    }
                    foreach ($arSort as $userID => $userName) {
                        $arDataRowNew[$periodKey][$userID] = $arResult["DATA_ROW"][$periodKey][$userID];
                    }
                } else {
                    $arDataRowNew[$periodKey] = array();
                }
            }
        }
        $arResult["DATA_ROW"] = $arDataRowNew;
    } else {
        //Сортировка по Ответственному
        if ($sortID == "RESP_ID" || $sortID == '') {
            foreach ($arResult["DATA_ROW"] as $userID => $data) {
                if (!empty($userID)) {
                    $rsUser = $USER->GetByID($userID);
                    $arUser = $rsUser->Fetch();
                    $userName = $arUser["LAST_NAME"] . ' ' . $arUser["NAME"];
                    $arSort[$arUser["ID"]] = $userName;
                }
            }
            if ($sortType == "ASC" || $sortType == "") {
                asort($arSort);
            }
            if ($sortType == "DESC") {
                arsort($arSort);
            }
        }
        //Сортировка по товарам
        if ($sortID == "PROD_COUNT") {
            $prodID = $_GET["prod_id"];
            foreach ($arResult["DATA_ROW"] as $userID => $data) {
                if (isset($data["PRODUCTS"][$prodID])) {
                    $arSort[$userID] = $data["PRODUCTS"][$prodID]["COUNT"];
                } else {
                    $arSort[$userID] = 0;
                }
            }
            if ($sortType == "ASC") {
                asort($arSort);
            }
            if ($sortType == "DESC") {
                arsort($arSort);
            }
        }
        //Сортировка по "Количество ККТ, активированных с ID агента"
        if ($sortID == "KKT_COUNT") {
            foreach ($arResult["DATA_ROW"] as $userID => $data) {
                $arSort[$userID] = $data["COUNT_KKT_AGENT"];
            }
            if ($sortType == "ASC") {
                asort($arSort);
            }
            if ($sortType == "DESC") {
                arsort($arSort);
            }
        }
        //Сортировка по "Поступление денежных средств на р/с, руб."
        if ($sortID == "MONEY_RS_BY_ORDER") {
            foreach ($arResult["DATA_ROW"] as $userID => $data) {
                $arSort[$userID] = $data["MONEY_RS_BY_ORDER"];
            }
            if ($sortType == "ASC") {
                asort($arSort);
            }
            if ($sortType == "DESC") {
                arsort($arSort);
            }
        }
        //Сортировка по "Поступление денежных средств на л/с, руб."
        if ($sortID == "MONEY_LS") {
            foreach ($arResult["DATA_ROW"] as $userID => $data) {
                $arSort[$userID] = $data["MONEY_LS"];
            }
            if ($sortType == "ASC") {
                asort($arSort);
            }
            if ($sortType == "DESC") {
                arsort($arSort);
            }
        }
        //Сортировка по "Поступление денежных средств на р/с и л/с, руб."
        if ($sortID == "TOTAL_MONEY") {
            foreach ($arResult["DATA_ROW"] as $userID => $data) {
                $arSort[$userID] = $data["MONEY_LS"] + $data["MONEY_RS_BY_ORDER"];
            }
            if ($sortType == "ASC") {
                asort($arSort);
            }
            if ($sortType == "DESC") {
                arsort($arSort);
            }
        }

        //Сортировка по "Поступление денежных средств на р/с за товары"
        if ($sortID == "PROD_PRICE") {
            $prodID = $_GET["prod_id"];
            foreach ($arResult["DATA_ROW"] as $userID => $data) {
                if (isset($data["PRODUCTS"][$prodID])) {
                    $arSort[$userID] = $data["PRODUCTS"][$prodID]["SUMM"];
                } else {
                    $arSort[$userID] = 0;
                }
            }
            if ($sortType == "ASC") {
                asort($arSort);
            }
            if ($sortType == "DESC") {
                arsort($arSort);
            }
        }
        //Сортировка по "Поступление денежных средств на р/с всего, руб."
        if ($sortID == "MONEY_RS") {
            foreach ($arResult["DATA_ROW"] as $userID => $data) {
                $arSort[$userID] = $data["MONEY_RS"];
            }
            if ($sortType == "ASC") {
                asort($arSort);
            }
            if ($sortType == "DESC") {
                arsort($arSort);
            }
        }
        $arDataRowNew = array();
        foreach ($arSort as $userID => $userName) {
            $arDataRowNew[$userID] = $arResult["DATA_ROW"][$userID];
        }
        $arResult["DATA_ROW"] = $arDataRowNew;
    }
}
//pre(array('$ordersID' => $ordersID));
//pre(array("DATA_ROW После" => $arResult["DATA_ROW"]));


$arResult['period'] = $period;
//pre(array('$form_date' => $form_date));
$arResult['report'] = $report;
$arResult["filter"] = $arFilterNew;
$excelView = isset($_GET["EXCEL"]) && $_GET["EXCEL"] == "Y";
$arResult['form_date'] = $form_date;
$arResult["sort_id"] = $sortID;
$arResult["sort_type"] = $sortType;
$arResult["prod_id"] = $prodID;
if ($excelView)
{
	$APPLICATION->RestartBuffer();

	Header("Content-Type: application/force-download");
	Header("Content-Type: application/octet-stream");
	Header("Content-Type: application/download");
	Header("Content-Disposition: attachment;filename=report.xls");
	Header("Content-Transfer-Encoding: binary");

	$this->IncludeComponentTemplate('excel');

	exit;
}
else
{
	$this->IncludeComponentTemplate();
}

