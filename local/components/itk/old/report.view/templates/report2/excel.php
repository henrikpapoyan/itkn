<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponentTemplate $this */
$el = new CIBlockElement();
$arProds = array();
$rsGroup = $el->GetList(
    array("ID"=>"ASC"),
    array("IBLOCK_ID" => GetIBlockIDByCode("products_catalog"), "ACTIVE" => "Y"),
    false,
    false,
    array("ID", "NAME")
);
while($arProd = $rsGroup->GetNext()) {
    $arProds[$arProd["ID"]] = $arProd["NAME"];
}
$arSelProds = array();
if(isset($arResult["filter"]["PRODUCTS"]) && !empty($arResult["filter"]["PRODUCTS"])) {
    $arSelProds = $arResult["filter"]["PRODUCTS"];
}

//Формируем данные для сотрировки по сотрудника
$arUserName = array();
foreach($arResult["DATA_ROW"] as $userID => $data){
    if(!empty($userID)){
        $rsUser = $USER->GetByID($userID);
        $arUser = $rsUser->Fetch();
        $userName = $arUser["LAST_NAME"].' '.$arUser["NAME"];
        $arUserName[$arUser["ID"]] = $userName;
    }
};

$this->IncludeLangFile('template.php');
?>
<meta http-equiv="Content-type" content="text/html;charset=<?echo LANG_CHARSET?>" />
<style type="text/css">
	.report-red-neg-val { color: red; }
</style>
<table border="1" cellspacing="0" cellpadding="5">
	<thead>
		<tr>
            <th>Ответственный</th>
            <?if( !empty($arSelProds) ):
                foreach($arSelProds as $prodID):?>
                    <th>Количество проданных <?=$arProds[$prodID]?>, шт.</th>
                    <th>Поступление денежных средств на р/с за <?=$arProds[$prodID]?>, руб</th>
                <?  endforeach;
            else: ?>
                <th>Количество всех проданных товаров, шт.</th>
            <?endif;?>
            <th>Поступление денежных средств на р/с всего, руб.</th>
		</tr>
	</thead>
	<tbody>
    <?$arTotalCount= array();
    $arTotalSumm = array();
    $totalCountKKTAgent = 0;
    $totalMoneyRS = 0;

    foreach($arResult["DATA_ROW"] as $responsID => $data):
        if(!empty($arResult["filter"]["PRODUCTS"])){
            $arFilterProd = $arResult["filter"]["PRODUCTS"];
            $arDataProds = array_keys($data["PRODUCTS"]);
            $arNeededProd = array_intersect($arDataProds, $arFilterProd);
            if(empty($arNeededProd)){
                continue;
            }
        }
        $totalMoneyRS += $data["MONEY_RS"];
        $userName = "Ответственный не указан";
        if(!empty($arUserName[$responsID])) {
            $userName = $arUserName[$responsID];
        }?>
        <tr>
            <td><?=$userName?></td>
            <?if( !empty($arSelProds) ):
                foreach($arSelProds as $prodID):
                    $arTotalCount[$prodID] += $data["PRODUCTS"][$prodID]["COUNT"];
                    $arTotalSumm[$prodID] += $data["PRODUCTS"][$prodID]["SUMM"];?>
                    <td><?=!empty($data["PRODUCTS"][$prodID]["COUNT"]) ? $data["PRODUCTS"][$prodID]["COUNT"] : '0';?></td>
                    <td><?=!empty($data["PRODUCTS"][$prodID]["SUMM"]) ? number_format($data["PRODUCTS"][$prodID]["SUMM"], 2, ",", "") : '0';?></td>
                <?  endforeach;
            else:
                $arTotalCount[0] += $data["PRODUCTS"][0]["COUNT"]?>
                <td><?=!empty($data["PRODUCTS"][0]["COUNT"]) ? $data["PRODUCTS"][0]["COUNT"] : '0';?></td>
            <?endif;?>
            <td><?=number_format($data["MONEY_RS"], 2, ",", "")?></td>
        </tr>
    <?endforeach;?>
        <tr>
            <td>Всего</td>
            <?if( !empty($arSelProds) ):
                foreach($arSelProds as $prodID):?>
                    <td><?=$arTotalCount[$prodID];?></td>
                    <td><?=number_format($arTotalSumm[$prodID], 2, ",", "");?></td>
                <?  endforeach;
            else:?>
                <td><?=$arTotalCount[0]?></td>
            <?endif;?>
            <td><?=number_format($totalMoneyRS, 2, ",", "")?></td>
        </tr>
	</tbody>
</table>