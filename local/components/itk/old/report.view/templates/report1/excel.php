<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

/** @var CBitrixComponentTemplate $this */
$this->IncludeLangFile('template.php');
$el = new CIBlockElement();
$arProds = array();
$rsGroup = $el->GetList(
    array("ID"=>"ASC"),
    array("IBLOCK_ID" => GetIBlockIDByCode("products_catalog"), "ACTIVE" => "Y"),
    false,
    false,
    array("ID", "NAME")
);
while($arProd = $rsGroup->GetNext()) {
    $arProds[$arProd["ID"]] = $arProd["NAME"];
}
$arSelProds = array();
if(isset($arResult["filter"]["PRODUCTS"]) && !empty($arResult["filter"]["PRODUCTS"])) {
    $arSelProds = $arResult["filter"]["PRODUCTS"];
}

//Формируем данные для сотрировки по сотрудника
$arUserName = array();
foreach($arResult["DATA_ROW"] as $userID => $data){
    if(!empty($userID)){
        $rsUser = $USER->GetByID($userID);
        $arUser = $rsUser->Fetch();
        $userName = $arUser["LAST_NAME"].' '.$arUser["NAME"];
        $arUserName[$arUser["ID"]] = $userName;
    }
};
?>
<meta http-equiv="Content-type" content="text/html;charset=<?=LANG_CHARSET?>" />
<style type="text/css">
	.report-red-neg-val { color: red; }
</style>
<table border="1" cellspacing="0" cellpadding="5">
	<thead>
        <tr>
            <th rowspan="2">Ответственный</th>
            <th colspan="<?=empty($arSelProds) ? '2' : (sizeOf($arSelProds)+1)?>" style="text-align: center;">Количество продаж</th>
            <th colspan="3" style="text-align: center;">Получение денежных средств</th>
        </tr>
        <tr>
            <?if( !empty($arSelProds) ):
                foreach($arSelProds as $prodID):?>
                    <th><?=$arProds[$prodID]?>, шт.</th>
                <?  endforeach;
            else: ?>
                <th>Все товары, шт.</th>
            <?endif;?>
            <th>Количество ККТ, активированных с ID агента, шт.</th>
            <th>Поступление денежных средств на р/с, руб.</th>
            <th>Поступление денежных средств на л/с, руб.</th>
            <th>Поступление денежных средств на р/с и л/с, руб.</th>
        </tr>
	</thead>
	<tbody>
		<?$arTotalCount = array();
        $totalCountKKTAgent = 0;
        $totalMoneyRS = 0;
        $totalMoneyLS = 0;
        $totalMoney = 0;
        foreach($arResult["DATA_ROW"] as $responsID => $data):
            if(!empty($arResult["filter"]["PRODUCTS"])){
                $arFilterProd = $arResult["filter"]["PRODUCTS"];
                $arDataProds = array_keys($data["PRODUCTS"]);
                $arNeededProd = array_intersect($arDataProds, $arFilterProd);
                if(empty($arNeededProd)){
                    continue;
                }
            }
            $totalCountKKTAgent += $data["COUNT_KKT_AGENT"];
            $totalMoneyRS += $data["MONEY_RS_BY_ORDER"];
            $totalMoneyLS += $data["MONEY_LS"];
            $totalMoney += ($data["MONEY_RS_BY_ORDER"]+$data["MONEY_LS"]);
            $userName = "Ответственный не указан";
            if(!empty($arUserName[$responsID])) {
                $userName = $arUserName[$responsID];
            }?>
            <tr>
                <td><?=$userName?></td>
                <?if( !empty($arSelProds) ):
                    foreach($arSelProds as $prodID):
                        $arTotalCount[$prodID] += $data["PRODUCTS"][$prodID]["COUNT"];?>
                        <td><?=!empty($data["PRODUCTS"][$prodID]["COUNT"]) ? $data["PRODUCTS"][$prodID]["COUNT"] : '0';?></td>
                    <?  endforeach;
                else:
                    $arTotalCount[0] += $data["PRODUCTS"][0]["COUNT"]?>
                    <td><?=$data["PRODUCTS"][0]["COUNT"]?></td>
                <?endif;?>
                <td><?=$data["COUNT_KKT_AGENT"]?></td>
                <td><?=number_format($data["MONEY_RS_BY_ORDER"], 2, ",", "")?></td>
                <td><?=number_format($data["MONEY_LS"], 2, ",", "")?></td>
                <td><?=number_format($data["MONEY_RS_BY_ORDER"]+$data["MONEY_LS"], 2, ",", "")?></td>
            </tr>
        <?endforeach;?>
            <tr>
                <td>Всего</td>
                <?if( !empty($arSelProds) ):
                    foreach($arSelProds as $prodID):?>
                        <td><?=$arTotalCount[$prodID]?></td>
                    <?  endforeach;
                else:?>
                    <td><?=$arTotalCount[0]?></td>
                <?endif;?>
                <td><?=$totalCountKKTAgent?></td>
                <td><?=number_format($totalMoneyRS, 2, ",", "")?></td>
                <td><?=number_format($totalMoneyLS, 2, ",", "")?></td>
                <td><?=number_format($totalMoney, 2, ",", "")?></td>
            </tr>
	</tbody>
</table>