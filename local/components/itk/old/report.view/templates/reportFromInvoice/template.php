<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
//echo 'itk:report.view<br />';
/** @global CMain $APPLICATION */
global $APPLICATION;
//var_dump($arResult);
/**
 * @param CBitrixComponentTemplate &$component
 * @param mixed &$arParams[]
 * @param mixed &$arResult[]
 */

//Формируем данные для сотрировки по сотрудника
$arUserName = array();
foreach($arResult["DATA_ROW"] as $periodKey => $arPeriod){
    foreach($arPeriod as $userID => $data){
        if(!empty($userID)){
            $rsUser = $USER->GetByID($userID);
            $arUser = $rsUser->Fetch();
            $userName = $arUser["LAST_NAME"].' '.$arUser["NAME"];
            $arUserName[$arUser["ID"]] = $userName;
        }
    };
}
//pre(array('$arUserName' => $arUserName));

function reportViewShowTopButtons(&$component, &$arParams, &$arResult)
{
	/** @global CMain $APPLICATION */
	global $APPLICATION;

	$component->SetViewTarget("pagetitle", 100);?>
    <div class="reports-title-buttons">
        <a class="reports-title-button" href="<?php echo $APPLICATION->GetCurPageParam("EXCEL=Y&ncc=1")?>"> <?//ncc=1 is for preventing composite work on this hit?>
            <i class="reports-title-button-excel-icon"></i><span class="reports-link"><?=GetMessage('REPORT_EXCEL_EXPORT')?></span>
        </a>
        &nbsp;
        <!--<a class="reports-title-button" href="<?=CComponentEngine::MakePathFromTemplate($arParams["PATH_TO_REPORT_CONSTRUCT"], array("report_id" => $arParams['REPORT_ID'], 'action' => 'copy'));?>">
            <i class="reports-title-button-edit-icon"></i><span class="reports-link"><?=GetMessage('REPORT_COPY')?></span>
        </a>!-->
    <? if ($arResult['MARK_DEFAULT'] <= 0 && $arResult['AUTHOR']) : ?>
        <a class="reports-title-button" href="<?=CComponentEngine::MakePathFromTemplate($arParams["PATH_TO_REPORT_CONSTRUCT"], array("report_id" => $arParams['REPORT_ID'], 'action' => 'edit'));?>">
            <i class="reports-title-button-edit-icon"></i><span class="reports-link"><?=GetMessage('REPORT_EDIT')?></span>
        </a>
    <? endif; ?>
        &nbsp;
        <a class="reports-title-button" href="<?=CComponentEngine::MakePathFromTemplate($arParams["PATH_TO_REPORT_LIST"], array());?>">
            <i class="reports-title-button-back-icon"></i><span class="reports-link"><?=GetMessage('REPORT_RETURN_TO_LIST')?></span>
        </a>
    </div>
    <?php
	$component->EndViewTarget();
}
if (!empty($arResult['ERROR']))
{
	$GLOBALS['APPLICATION']->SetAdditionalCSS('/bitrix/js/report/css/report.css');
	$APPLICATION->SetTitle($arResult['report']['TITLE']);
	echo $arResult['ERROR'];
	reportViewShowTopButtons($this, $arParams, $arResult);
	return false;
}

// calendar
CJSCore::Init(array('date'));

$arPeriodTypes = array(
	"interval" => GetMessage("TASKS_DATE_INTERVAL"),
);

$GLOBALS['APPLICATION']->SetAdditionalCSS('/bitrix/js/report/css/report.css');

$APPLICATION->SetTitle($arResult['report']['TITLE']);
?>
<div class="reports-result-list-wrap">
	<div class="report-table-wrap">
		<div class="reports-list-left-corner"></div>
		<div class="reports-list-right-corner"></div>
		<table cellspacing="0" class="reports-list-table" id="report-result-table">
            <tr>
                <th>
                    <div class="reports-head-cell">
                        <span class="reports-table-arrow"></span>
                        <span class="reports-head-cell-title">Период</span>
                    </div>
                </th>
                <th class="from_sort <?=($arResult["sort_id"] == "RESP_ID" || $arResult["sort_id"] == "") ? ' reports-selected-column' : ''?><?=( ($arResult["sort_id"] == "RESP_ID" || $arResult["sort_id"] == "") && ($arResult["sort_type"] == "DESC")) ? ' ' : ' reports-head-cell-top'?>" colid="RESP_ID" defaultsort="ASC">
                    <div class="reports-head-cell">
                        <span class="reports-table-arrow"></span>
                        <span class="reports-head-cell-title">Ответственный</span>
                    </div>
                </th>
                <th class="from_sort <?=($arResult["sort_id"] == "ISSUED") ? ' reports-selected-column' : ''?><?=($arResult["sort_id"] == "ISSUED" && $arResult["sort_type"] == "ASC") ? ' reports-head-cell-top' : ''?>" colid="ISSUED" defaultsort="DESC">
                    <div class="reports-head-cell">
                        <span class="reports-table-arrow"></span>
                        <span class="reports-head-cell-title">Сумма выставленных, счетов, руб.</span>
                    </div>
                </th>
                <th class="from_sort <?=($arResult["sort_id"] == "ISSUED_PAYED") ? ' reports-selected-column' : ''?><?=($arResult["sort_id"] == "ISSUED_PAYED" && $arResult["sort_type"] == "ASC") ? ' reports-head-cell-top' : ''?>" colid="ISSUED_PAYED" defaultsort="DESC">
                    <div class="reports-head-cell">
                        <span class="reports-table-arrow"></span>
                        <span class="reports-head-cell-title">Сумма выставленных и оплаченных счетов, руб.</span>
                    </div>
                </th>
                <th class="from_sort <?=($arResult["sort_id"] == "ISSUED_NOT_PAYED_NOT_OVER") ? ' reports-selected-column' : ''?><?=($arResult["sort_id"] == "ISSUED_NOT_PAYED_NOT_OVER" && $arResult["sort_type"] == "ASC") ? ' reports-head-cell-top' : ''?>" colid="ISSUED_NOT_PAYED_NOT_OVER" defaultsort="DESC">
                    <div class="reports-head-cell">
                        <span class="reports-table-arrow"></span>
                        <span class="reports-head-cell-title">Сумма выставленных, но не оплаченных счетов, руб. (срок оплаты не прошел)</span>
                    </div>
                </th>
                <th class="from_sort <?=($arResult["sort_id"] == "ISSUED_NOT_PAYED_OVER") ? ' reports-selected-column' : ''?><?=($arResult["sort_id"] == "ISSUED_NOT_PAYED_OVER" && $arResult["sort_type"] == "ASC") ? ' reports-head-cell-top' : ''?>" colid="ISSUED_NOT_PAYED_OVER" defaultsort="DESC">
                    <div class="reports-head-cell">
                        <span class="reports-table-arrow"></span>
                        <span class="reports-head-cell-title">Сумма выставленных, но не оплаченных счетов, руб. (срок оплаты прошел)</span>
                    </div>
                </th>
                <th class="from_sort <?=($arResult["sort_id"] == "ISSUED_NOT_PAYED") ? ' reports-selected-column' : ''?><?=($arResult["sort_id"] == "ISSUED_NOT_PAYED" && $arResult["sort_type"] == "ASC") ? ' reports-head-cell-top' : ''?>" colid="ISSUED_NOT_PAYED" defaultsort="DESC">
                    <div class="reports-head-cell">
                        <span class="reports-table-arrow"></span>
                        <span class="reports-head-cell-title">Сумма выставленных, но не оплаченных счетов, руб. (все)</span>
                    </div>
                </th>
                <th class="from_sort <?=($arResult["sort_id"] == "REJECTED") ? ' reports-selected-column' : ''?><?=($arResult["sort_id"] == "REJECTED" && $arResult["sort_type"] == "ASC") ? ' reports-head-cell-top' : ''?>" colid="REJECTED" defaultsort="DESC">
                    <div class="reports-head-cell">
                        <span class="reports-table-arrow"></span>
                        <span class="reports-head-cell-title">Сумма отклоненных счетов, руб.</span>
                    </div>
                </th>
                <th class="from_sort <?=($arResult["sort_id"] == "DELETED") ? ' reports-selected-column' : ''?><?=($arResult["sort_id"] == "DELETED" && $arResult["sort_type"] == "ASC") ? ' reports-head-cell-top' : ''?>" colid="DELETED" defaultsort="DESC">
                    <div class="reports-head-cell">
                        <span class="reports-table-arrow"></span>
                        <span class="reports-head-cell-title">Сумма удаленных счетов, руб.</span>
                    </div>
                </th>
            </tr>
			<!-- head -->
            <?
            $arTotalAmountInvoice["AMOUNT_INVOICES"] = array(
                'ISSUED' => 0,
                'ISSUED_PAYED' => 0,
                'ISSUED_NOT_PAYED_NOT_OVER' => 0,
                'ISSUED_NOT_PAYED_OVER' => 0,
                'ISSUED_NOT_PAYED' => 0,
                'REJECTED' => 0,
                'DELETED' => 0,
            );
            foreach($arResult["DATA_ROW"] as $periodKey => $arPeriod):
                if(empty($arPeriod)){?>
                    <tr class="reports-list-item">
                        <td><?=$periodKey?></td>
                        <td>н/д</td>
                        <td class="price_column">н/д</td>
                        <td class="price_column">н/д</td>
                        <td class="price_column">н/д</td>
                        <td class="price_column">н/д</td>
                        <td class="price_column">н/д</td>
                        <td class="price_column">н/д</td>
                        <td class="price_column">н/д</td>
                    </tr>
               <? } else {
                    $firstResp = true;
                    foreach($arPeriod as $responsID => $data):
                        $userName = "Ответственный не указан $responsID";
                        if(!empty($arUserName[$responsID])) {
                            $userName = $arUserName[$responsID];
                        }?>
                        <tr class="reports-list-item">
                            <?if($firstResp):
                                $firstResp = false;?>
                                <td rowspan="<?=sizeOf($arPeriod)?>"><?=$periodKey?></td>
                            <?endif;?>
                            <td><?=$userName?></td>
                            <?foreach($data["AMOUNT_INVOICES"] as $invoiceKey => $invoiceVal):
                                $arTotalAmountInvoice["AMOUNT_INVOICES"][$invoiceKey] += $invoiceVal;?>
                                <td class="price_column"><?=number_format($invoiceVal, 2, ".", " ")?></td>
                            <?endforeach;?>
                        </tr>
                    <?endforeach;
                }?>
            <?endforeach;?>
            <tr>
                <td colspan="9" class="reports-pretotal-column">
                    <br><br>
                    <span style="font-size: 14px;">Всего:</span>
                </td>
            </tr>
            <tr>
                <td class="reports-first-column reports-total-column" style="background-color: #F0F0F0;">Период</td>
                <td class="reports-total-column" style="background-color: #F0F0F0;">Ответственный</td>
                <td class="reports-total-column" style="background-color: #F0F0F0;">Сумма выставленных, счетов, руб.</td>
                <td class="reports-total-column" style="background-color: #F0F0F0;">Сумма выставленных и оплаченных счетов, руб.</td>
                <td class="reports-total-column" style="background-color: #F0F0F0;">Сумма выставленных, но не оплаченных счетов, руб.
                    (срок оплаты не прошел)</td>
                <td class="reports-total-column" style="background-color: #F0F0F0;">Сумма выставленных, но не оплаченных счетов, руб.
                    (срок оплаты прошел)</td>
                <td class="reports-total-column" style="background-color: #F0F0F0;">Сумма выставленных, но не оплаченных счетов, руб.
                    (все)</td>
                <td class="reports-total-column" style="background-color: #F0F0F0;">Сумма отклоненных счетов, руб.</td>
                <td class="reports-last-column reports-total-column" style="text-align: center; background-color: #F0F0F0;">Сумма удаленных счетов, руб.</td>
            </tr>
            <tr>
                <td class="reports-first-column reports-numeric-column">Все</td>
                <td class="price_column reports-numeric-column">Все</td>
                <td class="price_column reports-numeric-column"><?=number_format($arTotalAmountInvoice["AMOUNT_INVOICES"]["ISSUED"], 2, ".", " ")?></td>
                <td class="price_column reports-numeric-column"><?=number_format($arTotalAmountInvoice["AMOUNT_INVOICES"]["ISSUED_PAYED"], 2, ".", " ")?></td>
                <td class="price_column reports-numeric-column"><?=number_format($arTotalAmountInvoice["AMOUNT_INVOICES"]["ISSUED_NOT_PAYED_NOT_OVER"], 2, ".", " ")?></td>
                <td class="price_column reports-numeric-column"><?=number_format($arTotalAmountInvoice["AMOUNT_INVOICES"]["ISSUED_NOT_PAYED_OVER"], 2, ".", " ")?></td>
                <td class="price_column reports-numeric-column"><?=number_format($arTotalAmountInvoice["AMOUNT_INVOICES"]["ISSUED_NOT_PAYED"], 2, ".", " ")?></td>
                <td class="price_column reports-numeric-column"><?=number_format($arTotalAmountInvoice["AMOUNT_INVOICES"]["REJECTED"], 2, ".", " ")?></td>
                <td class="price_column reports-last-column reports-numeric-column"><?=number_format($arTotalAmountInvoice["AMOUNT_INVOICES"]["DELETED"], 2, ".", " ")?></td>
            </tr>
		</table>
		<script type="text/javascript">
		BX.ready(function(){
			var rows = $("table#report-result-table th.from_sort");
			for (i = 0 ; i < rows.length ; i++)
			{
				var ds = rows[i].getAttribute('defaultSort');
				if (ds == '')
				{
					BX.addClass(rows[i], 'report-column-disabled-sort');
					continue;
				}

				BX.bind(rows[i], 'click', function(){
					var colId = this.getAttribute('colId');
                    var prodID = '';
					if(colId == 'PROD_COUNT')
                        var prodID = this.getAttribute('prodID');
					var sortType = '';

					var isCurrent = BX.hasClass(this, 'reports-selected-column');

					if (isCurrent)
					{
						var currentSortType = BX.hasClass(this, 'reports-head-cell-top') ? 'ASC' : 'DESC';
						sortType = currentSortType == 'ASC' ? 'DESC' : 'ASC';
					}
					else
					{
						sortType = this.getAttribute('defaultSort');
					}

					var idInp = BX.findChild(BX('report-rewrite-filter'), {attr:{name:'sort_id'}});
					var typeInp = BX.findChild(BX('report-rewrite-filter'), {attr:{name:'sort_type'}});
					var prodIdInp = BX.findChild(BX('report-rewrite-filter'), {attr:{name:'prod_id'}});

					idInp.value = colId;
					typeInp.value = sortType;
                    prodIdInp.value = prodID;

					BX.submit(BX('report-rewrite-filter'));
				});
			}
		});
		</script>
	</div>
</div>

<?php $this->SetViewTarget("sidebar_tools_1", 100);?>

<!-- control examples -->
<div id="report-chfilter-examples" style="display: none;">

	<div class="filter-field filter-field-user chfilter-field-\Bitrix\Main\User" callback="RTFilter_chooseUser">
		<label for="user-email" class="filter-field-title">%TITLE% "%COMPARE%"</label>
		<span class="webform-field-textbox-inner">
			<input id="%ID%" type="text" class="webform-field-textbox" caller="true" autocomplete="off"/>
			<input type="hidden" name="%NAME%" value=""/>
			<a href="" class="webform-field-textbox-clear"></a>
		</span>
	</div>

	<div class="filter-field filter-field-crm chfilter-field-crm">
		<label class="filter-field-title">%TITLE% "%COMPARE%"</label>
	</div>

	<div class="filter-field filter-field-crm_status chfilter-field-crm_status">
		<label class="filter-field-title">%TITLE% "%COMPARE%"</label>
	</div>

	<div class="filter-field filter-field-iblock_element chfilter-field-iblock_element">
		<label class="filter-field-title">%TITLE% "%COMPARE%"</label>
	</div>

	<div class="filter-field filter-field-iblock_section chfilter-field-iblock_section">
		<label class="filter-field-title">%TITLE% "%COMPARE%"</label>
	</div>

	<div class="filter-field filter-field-employee chfilter-field-employee" callback="RTFilter_chooseUser">
		<label for="user-email" class="filter-field-title">%TITLE% "%COMPARE%"</label>
		<span class="webform-field-textbox-inner">
			<input id="%ID%" type="text" class="webform-field-textbox" caller="true" />
			<input type="hidden" name="%NAME%" value=""/>
			<a href="" class="webform-field-textbox-clear"></a>
		</span>
	</div>

	<div class="filter-field filter-field-user chfilter-field-\Bitrix\Socialnetwork\Workgroup" callback="RTFilter_chooseGroup">
		<label for="user-email" class="filter-field-title">%TITLE% "%COMPARE%"</label>
		<span class="webform-field-textbox-inner">
			<input id="%ID%" type="text" class="webform-field-textbox" caller="true" />
			<input type="hidden" name="%NAME%" value=""/>
			<a href="" class="webform-field-textbox-clear"></a>
		</span>
	</div>

	<div class="filter-field chfilter-field-datetime">
		<label for="" class="filter-field-title">%TITLE% "%COMPARE%"</label>
		<input type="text" value="%VALUE%" name="%NAME%" value="" class="filter-field-calendar" id="" /><a class="filter-date-interval-calendar" href="" title="<?=GetMessage('TASKS_PICK_DATE')?>"><img border="0" src="/bitrix/js/main/core/images/calendar-icon.gif" alt="<?=GetMessage('TASKS_PICK_DATE')?>"></a>
	</div>

	<div class="filter-field chfilter-field-string">
		<label for="" class="filter-field-title">%TITLE% "%COMPARE%"</label>
		<input type="text" value="%VALUE%" name="%NAME%" value="" class="filter-textbox" id="" />
	</div>

	<div class="filter-field chfilter-field-text">
		<label for="" class="filter-field-title">%TITLE% "%COMPARE%"</label>
		<input type="text" value="%VALUE%" name="%NAME%" value="" class="filter-textbox" id="" />
	</div>

	<div class="filter-field chfilter-field-integer">
		<label for="" class="filter-field-title">%TITLE% "%COMPARE%"</label>
		<input type="text" value="%VALUE%" name="%NAME%" value="" class="filter-textbox" id="" />
	</div>

	<div class="filter-field chfilter-field-float">
		<label for="" class="filter-field-title">%TITLE% "%COMPARE%"</label>
		<input type="text" value="%VALUE%" name="%NAME%" value="" class="filter-textbox" id="" />
	</div>

	<div class="filter-field chfilter-field-boolean" callback="RTFilter_chooseBoolean">
		<label for="" class="filter-field-title">%TITLE% "%COMPARE%"</label>
		<select name="%NAME%" class="filter-dropdown" id="%ID%" caller="true">
			<option value=""><?=GetMessage('REPORT_IGNORE_FILTER_VALUE')?></option>
			<option value="true"><?=GetMessage('REPORT_BOOLEAN_VALUE_TRUE')?></option>
			<option value="false"><?=GetMessage('REPORT_BOOLEAN_VALUE_FALSE')?></option>
		</select>
		<script type="text/javascript">
			function RTFilter_chooseBooleanCatch(value)
			{
				setSelectValue(RTFilter_chooseBoolean_LAST_CALLER, value);
			}
		</script>
	</div>

</div>

<!-- UF enumerations control examples -->
<div id="report-chfilter-examples-ufenums" style="display: none;">
	<?
	if (is_array($arResult['ufEnumerations'])):
		foreach ($arResult['ufEnumerations'] as $ufId => $enums):
			foreach ($enums as $fieldKey => $enum):
	?>
	<div class="filter-field chfilter-field-<?=($ufId.'_'.$fieldKey)?>" callback="RTFilter_chooseBoolean">
		<label for="" class="filter-field-title">%TITLE% "%COMPARE%"</label>
		<select name="%NAME%" class="filter-dropdown" id="%ID%" caller="true">
			<option value=""><?=GetMessage('REPORT_IGNORE_FILTER_VALUE')?></option>
			<?
			foreach ($enum as $itemId => $itemInfo):
			?>
			<option value="<?=$itemId?>"><?=$itemInfo['VALUE']?></option>
			<?
			endforeach;
			?>
		</select>
	</div>
	<?
			endforeach;
		endforeach;
	endif;
	?>
</div>

<div class="sidebar-block">
	<b class="r2"></b><b class="r1"></b><b class="r0"></b>
	<div class="sidebar-block-inner">
		<div class="filter-block-title report-filter-block-title"><?=GetMessage('REPORT_FILTER')?><!--<a class="filter-settings" href=""></a>--></div>
		<div class="filter-block filter-field-date-combobox filter-field-date-combobox-interval">

			<form id="report-rewrite-filter" action="<?=CComponentEngine::MakePathFromTemplate(
				$arParams["PATH_TO_REPORT_VIEW"],
				array('report_id' => $arParams['REPORT_ID'])
			);?>" method="GET">

			<input type="hidden" name="set_filter" value="Y" />
			<input type="hidden" name="sort_id" value="<?=htmlspecialcharsbx($arResult['sort_id'])?>" />
			<input type="hidden" name="sort_type" value="<?=htmlspecialcharsbx($arResult['sort_type'])?>" />
			<input type="hidden" name="prod_id" value="<?=htmlspecialcharsbx($arResult['prod_id'])?>" />

			<?=$APPLICATION->GetViewContent("report_view_prefilter")?>

			<!-- period -->
			<div class="filter-field">
				<label for="task-interval-filter" class="filter-field-title"><?=GetMessage('REPORT_PERIOD')?></label>
				<input type="hidden" class="filter-dropdown" style="margin-bottom: 0;" id="task-interval-filter" name="F_DATE_TYPE" value="interval" />
                <?foreach($arResult['form_date']["from"] as $key => $dateVal):?>
                    <span class="filter-date-interval filter-date-interval-after filter-date-interval-before">
                        <span class="filter-date-interval-from">
                            <input type="text" class="filter-date-interval-from" name="F_DATE_FROM[]" id="REPORT_INTERVAL_F_DATE_FROM_<?=$key?>" value="<?=$dateVal?>"/>
                            <a class="filter-date-interval-calendar link-sel-date-interval-from" href="" title="<?php echo GetMessage("TASKS_PICK_DATE")?>" id="filter-date-interval-calendar-from" attrInputNum="<?=$key?>">
                                <img border="0" src="/bitrix/js/main/core/images/calendar-icon.gif" alt="<?php echo GetMessage("TASKS_PICK_DATE")?>">
                            </a>
                        </span>
                        <span class="filter-date-interval-hellip">&hellip;</span>
                        <span class="filter-date-interval-to">
                            <input type="text" class="filter-date-interval-to" name="F_DATE_TO[]" id ="REPORT_INTERVAL_F_DATE_TO_<?=$key?>" value="<?=$arResult['form_date']['to'][$key]?>"/>
                            <a href="" class="filter-date-interval-calendar link-sel-date-interval-to" title="<?php echo GetMessage("TASKS_PICK_DATE")?>" id="filter-date-interval-calendar-to" attrInputNum="<?=$key?>">
                                <img border="0" src="/bitrix/js/main/core/images/calendar-icon.gif" alt="<?php echo GetMessage("TASKS_PICK_DATE")?>">
                            </a>
                        </span>
                        <?if($key != 0):?><span class="remove_period" onclick="removePeriod(this)"> - </span><?endif;?>
                    </span>
                <?endforeach;?>
                <span id="add_new_date_period" onClick="addNewPeriodFilter()">+</span>

				<script type="text/javascript">
                    function addNewPeriodFilter() {
                        var currPeriodCunt = $("div.filter-field span.filter-date-interval").length;
                        console.log('currPeriodCunt = ', currPeriodCunt);
                        var newPeriodText = '<span class="filter-date-interval filter-date-interval-after filter-date-interval-before">\n' +
                            '<span class="filter-date-interval-from">\n' +
                            '<input type="text" class="filter-date-interval-from" name="F_DATE_FROM[]" id="REPORT_INTERVAL_F_DATE_FROM_'+currPeriodCunt+'" value=""/>\n'+
                            '<a class="filter-date-interval-calendar link-sel-date-interval-from" href="" title="<?php echo GetMessage("TASKS_PICK_DATE")?>" id="filter-date-interval-calendar-from" attrInputNum="'+currPeriodCunt+'">\n' +
                            '<img border="0" src="/bitrix/js/main/core/images/calendar-icon.gif" alt="<?php echo GetMessage("TASKS_PICK_DATE")?>">\n' +
                            '</a>\n' +
                            '</span>\n' +
                            '<span class="filter-date-interval-hellip">&hellip;</span>\n' +
                            '<span class="filter-date-interval-to">\n' +
                            ' <input type="text" class="filter-date-interval-to" name="F_DATE_TO[]" id ="REPORT_INTERVAL_F_DATE_TO_'+currPeriodCunt+'" value=""/>\n' +
                            '<a href="" class="filter-date-interval-calendar link-sel-date-interval-to" title="<?php echo GetMessage("TASKS_PICK_DATE")?>" id="filter-date-interval-calendar-to" attrInputNum="'+currPeriodCunt+'">\n' +
                            '<img border="0" src="/bitrix/js/main/core/images/calendar-icon.gif" alt="<?php echo GetMessage("TASKS_PICK_DATE")?>">\n' +
                            '</a>\n' +
                            '</span>\n' +
                            '<span class="remove_period" onclick="removePeriod(this)"> - </span>' +
                            '</span>';
                        $("span#add_new_date_period").before(newPeriodText);
                    }

                    function removePeriod(currElem) {
                        $(currElem).parent().remove();
                    }


					BX.ready(function() {
					    $("div.filter-field").on("click", "a.link-sel-date-interval-from", function(event){
                            event.preventDefault ? event.preventDefault() : (event.returnValue=false);
                            console.log('$(this).attr(attrInputNum) = ', $(this).attr('attrInputNum'));

                            var curDate = new Date();
                            var curTimestamp = Math.round(curDate / 1000) - curDate.getTimezoneOffset()*60;

                            BX.calendar({
                                node: this,
                                field: BX('REPORT_INTERVAL_F_DATE_FROM_'+$(this).attr('attrInputNum')),
                                bTime: false
                            });

                            event.preventDefault();
                        });

                        $("div.filter-field").on("click", "a.link-sel-date-interval-to", function(event){
                            event.preventDefault ? event.preventDefault() : (event.returnValue=false);
                            console.log('$(this).attr(attrInputNum) = ', $(this).attr('attrInputNum'));

							var curDate = new Date();
							var curTimestamp = Math.round(curDate / 1000) - curDate.getTimezoneOffset()*60;

							BX.calendar({
								node: this,
								field: BX('REPORT_INTERVAL_F_DATE_TO_'+$(this).attr('attrInputNum')),
								bTime: false
							});

                            event.preventDefault();
						});

						jsCalendar.InsertDate = function(value) {
							BX.removeClass(this.field.parentNode.parentNode, "webform-field-textbox-empty");
							var value = this.ValueToString(value);
							this.field.value = value.substr(11, 8) == "00:00:00" ? value.substr(0, 10) : value.substr(0, 16);
							this.Close();
						}
					});

				</script>
			</div>
			<div id="report-filter-chfilter">
                <?$info[] = array(
                    'TITLE' => "Ответственный",
                    'COMPARE' => "равно",
                    'NAME' =>"select_responsible_user",
                    'ID' => "select_responsible_user",
                    'VALUE' => array("id"=>""),
                    'FIELD_NAME' => "ASSIGNED_BY",
                    'FIELD_TYPE' => "\\Bitrix\\Main\\User",
                    'IS_UF' => false,
                    'UF_ID' => "",
                    'UF_NAME' => "",
                );?>
                <script type="text/javascript">

                    BX.ready(function() {
                        var info = <?=CUtil::PhpToJSObject($info)?>;
                        var cpControl, fieldType, tipicalControl, isUF, ufId, ufName, cpSelector, selectorIndex;

                        for (var i in info)
                        {
                            if (!info.hasOwnProperty(i))
                                continue;

                            cpControl = null;
                            fieldType = info[i].FIELD_TYPE;
                            if (info[i]['IS_UF'] && fieldType === 'enum')
                            {
                                cpControl = BX.clone(
                                    BX.findChild(
                                        BX('report-chfilter-examples-ufenums'),
                                        {className:'chfilter-field-'+info[i]['UF_ID'] + "_" + info[i]['UF_NAME']}
                                    ),
                                    true
                                );
                            }
                            else
                            {
                                // insert value control
                                // search in `examples-custom` by name or type
                                // then search in `examples` by type
                                cpControl = BX.clone(
                                    BX.findChild(
                                        BX('report-chfilter-examples-custom'),
                                        {className:'chfilter-field-'+info[i].FIELD_NAME}
                                    )
                                    ||
                                    BX.findChild(
                                        BX('report-chfilter-examples-custom'),
                                        {className:'chfilter-field-'+fieldType}
                                    )
                                    ||
                                    BX.findChild(
                                        BX('report-chfilter-examples'),
                                        {className:'chfilter-field-'+fieldType}
                                    ),
                                    true
                                );
                            }

                            //global replace %ID%, %NAME%, %TITLE% and etc.
                            cpControl.innerHTML = cpControl.innerHTML.replace(/%((?!VALUE)[A-Z]+)%/gi,
                                function(str, p1, offset, s)
                                {
                                    var n = p1.toUpperCase();
                                    return typeof(info[i][n]) != 'undefined' ? BX.util.htmlspecialchars(info[i][n]) : str;
                                });
                            tipicalControl = true;
                            isUF = !!info[i]["IS_UF"];
                            if (isUF)
                            {
                                ufId = info[i]["UF_ID"];
                                ufName = info[i]["UF_NAME"];
                                if (fieldType === 'crm' || fieldType === 'crm_status'
                                    || fieldType === 'iblock_element' || fieldType === 'iblock_section')
                                {
                                    tipicalControl = false;
                                }
                            }
                            if (tipicalControl)
                            {
                                if (cpControl.getAttribute('callback') != null)
                                {
                                    // set last caller
                                    var callerName = cpControl.getAttribute('callback') + '_LAST_CALLER';
                                    var callerObj = BX.findChild(cpControl, {attr:'caller'}, true);
                                    window[callerName] = callerObj;

                                    // set value
                                    var cbFuncName = cpControl.getAttribute('callback') + 'Catch';
                                    window[cbFuncName](info[i].VALUE);
                                }
                                else
                                {
                                    cpControl.innerHTML = cpControl.innerHTML.replace('%VALUE%', BX.util.htmlspecialchars(info[i].VALUE));
                                }
                                BX('report-filter-chfilter').appendChild(cpControl);
                            }
                            else
                            {
                                BX('report-filter-chfilter').appendChild(cpControl);
                                var filterFieldSelector = BX.Report.FilterFieldSelectorManager.getSelector(ufId, ufName);
                                if (filterFieldSelector)
                                {
                                    cpSelector = filterFieldSelector.makeFilterField(cpControl, null, info[i]["NAME"]);
                                    if (cpSelector)
                                    {
                                        selectorIndex = cpSelector.getAttribute("ufSelectorIndex");
                                        filterFieldSelector.setFilterValue(selectorIndex, info[i]["VALUE"]);
                                    }
                                }
                            }
                        }
                    });

                </script>

			</div>

			</form>

			<form id="report-reset-filter" action="<?=CComponentEngine::MakePathFromTemplate(
				$arParams["PATH_TO_REPORT_VIEW"],
				array('report_id' => $arParams['REPORT_ID'])
			);?>" method="GET">
				<input type="hidden" name="sort_id" value="<?=htmlspecialcharsbx($arResult['sort_id'])?>" />
				<input type="hidden" name="sort_type" value="<?=htmlspecialcharsbx($arResult['sort_type'])?>" />
			</form>


			<div class="filter-field-buttons">
				<input id="report-rewrite-filter-button" type="submit" value="<?=GetMessage('REPORT_FILTER_APPLY')?>" class="filter-submit">&nbsp;&nbsp;<input id="report-reset-filter-button" type="submit" name="del_filter_company_search" value="<?=GetMessage('REPORT_FILTER_CANCEL')?>" class="filter-submit">
			</div>

			<script type="text/javascript">

			BX.ready(function(){
				BX.bind(BX('report-reset-filter-button'), 'click', function(){
					BX.submit(BX('report-reset-filter'));
				});
				BX.bind(BX('report-rewrite-filter-button'), 'click', function(){
					BX.submit(BX('report-rewrite-filter'));
				});

				// User controls
				var controls = BX.findChildren(BX('report-rewrite-filter'), {className: /chfilter-field-(\\Bitrix\\Main\\User|employee)/}, true);
				if (controls != null)
				{
				    console.log('User controls');
					for (i in controls)
					{
						var inp = BX.findChild(controls[i], {tag:'input', attr:{type:'text'}}, true);
						var x = BX.findNextSibling(inp, {tag:'a'});
						BX.bind(inp, 'click', RTFilter_chooseUser);
						BX.bind(inp, 'blur', RTFilter_chooseUserCatchFix);
						BX.bind(x, 'click', RTFilter_chooseUserClear);
					}
				}

				// Group controls
				var controls = BX.findChildren(BX('report-rewrite-filter'), {className:'chfilter-field-\\Bitrix\\Socialnetwork\\Workgroup'}, true);
				if (controls != null)
				{
                    console.log('Group controls');
					for (i in controls)
					{
						var inp = BX.findChild(controls[i], {tag:'input', attr:{type:'text'}}, true);
						var x = BX.findNextSibling(inp, {tag:'a'});
						BX.bind(inp, 'click', RTFilter_chooseGroup);
						BX.bind(inp, 'blur', RTFilter_chooseGroupCatchFix);
						BX.bind(x, 'click', RTFilter_chooseGroupClear);
					}
				}

				// Date controls
				var controls = BX.findChildren(BX('report-rewrite-filter'), {className:'chfilter-field-datetime'}, true);
				if (controls != null)
				{
					for (i in controls)
					{
						var butt = BX.findChild(controls[i], {tag:'img'}, true);

						BX.bind(butt, "click", function(e) {
							BX.PreventDefault(e);

							var valueInput = BX.findChild(this.parentNode.parentNode, {tag:'input'});

							var curDate = new Date();
							var curTimestamp = Math.round(curDate / 1000) - curDate.getTimezoneOffset()*60;

							BX.calendar({
								node: this,
								field: valueInput,
								bTime: false
							});
						});
					}
				}
			});

			function setSelectValue(select, value)
			{
				var i, j;
				var bFirstSelected = false;
				var bMultiple = !!(select.getAttribute('multiple'));
				if (!(value instanceof Array)) value = new Array(value);
				for (i=0; i<select.options.length; i++)
				{
					for (j in value)
					{
						if (select.options[i].value == value[j])
						{
							if (!bFirstSelected) {bFirstSelected = true; select.selectedIndex = i;}
							select.options[i].selected = true;
							break;
						}
					}
					if (!bMultiple && bFirstSelected) break;
				}
			}

			function RTFilter_chooseUser(control)
			{
				if (this.parentNode)
				{
					var elem = this;
				}
				else
				{
					var elem = BX.findChild(control, {tag:'input', attr: {type:'text'}}, true);
				}

				singlePopup = BX.PopupWindowManager.create("single-employee-popup-"+Math.random(), elem, {
					offsetTop : 1,
					autoHide : true,
					content : BX("Single_"+elem.id+"_selector_content"),
                    closeIcon: true,
                });

				if (singlePopup.popupContainer.style.display != "block")
				{
					singlePopup.show();
				}

				RTFilter_chooseUser_LAST_CALLER = elem;
			}

			function RTFilter_chooseUserCatch(user)
			{
                console.log('RTFilter_chooseUserCatch = ', user);
				var inp = RTFilter_chooseUser_LAST_CALLER;
				var hid = BX.findNextSibling(inp, {tag:'input',attr:{type:'hidden'}});
				var x = BX.findNextSibling(inp, {tag:'a'});
                console.log('RTFilter_chooseUserCatch  hid.value= ' + hid.value);
                if(hid.value == '') {
				    hid.value = user.id;
                } else {
                    hid.value = hid.value + ',' +user.id;
                }

				if (hid.value == '')
				{
				    inp.value = "Выбрано: все";
                    x.style.display = 'none';
				}
				else
				{
                    hid.split(",");
                    console.log("hid = ", hid);
					inp.value = 'Выбрано: '+hid.length+' отв';
                    x.style.display = 'inline';
				}

				try
				{
					// singlePopup.close();
				}
				catch (e) {}
			}

			function RTFilter_chooseUserCatchChange(arSelected)
			{
                var inp = RTFilter_chooseUser_LAST_CALLER;
                var hid = BX.findNextSibling(inp, {tag:'input',attr:{type:'hidden'}});
                var x = BX.findNextSibling(inp, {tag:'a'});
                var selectUsers = '';
                var selectUserscount = 0;
                console.log('RTFilter_chooseUserCatchChange arSelected = ', arSelected);
                $(arSelected).each(function(index, elem){
                    if(typeof(elem) != 'undefined' && elem != null) {
                        //RTFilter_chooseUserCatch(elem);
                        if(selectUsers == ''){
                            selectUsers = elem.id;
                        } else {
                            selectUsers = selectUsers+','+elem.id;
                        }
                        selectUserscount++;
                    }
                })
                hid.value = selectUsers;
                if(selectUserscount == 0) {
                    inp.value = 'Выбрано: все';
                    x.style.display = 'none';
                } else {
                    inp.value = 'Выбрано: '+selectUserscount+' отв.';
                    x.style.display = 'inline';
                }
			}

			function RTFilter_chooseUserCatchFix()
			{
			    console.log('RTFilter_chooseUserCatchFix');
				var inp = RTFilter_chooseUser_LAST_CALLER;
				var hid = BX.findNextSibling(inp, {tag:'input',attr:{type:'hidden'}});

                if(hid.value != ''){
                    var arEmployers = hid.value.split(',');
                    inp.value = 'Выбрано: '+arEmployers.length+' отв.';
                } else {
                    inp.value = 'Выбрано: все';
                }
			}

			function RTFilter_chooseUserClear(e)
			{
                console.log('RTFilter_chooseUserClear');
                var inp = RTFilter_chooseUser_LAST_CALLER;
                var hid = BX.findNextSibling(inp, {tag:'input',attr:{type:'hidden'}});
				RTFilter_chooseUser_LAST_CALLER = BX.findChild(this.parentNode, {tag:'input',attr:{type:'text'}});
                if(hid.value != ''){
                    var arEmployers = hid.value.split(',');
                    arEmployers.forEach(function(item, i, arEmployers) {
                        O_Single_select_responsible_user.unselect(item);
                    });
                }
                BX.PreventDefault(e);
				//RTFilter_chooseUserCatch({id:''});
			}

			function RTFilter_chooseGroup(control)
			{
				if (this.parentNode)
				{
					var elem = this;
				}
				else
				{
					var elem = BX.findChild(control, {tag:'input', attr: {type:'text'}}, true);
				}

				var popup = window['filterGroupsPopup_'+elem.id];
				popup.searchInput = elem;
				popup.popupWindow.setBindElement(elem);
				popup.show();

				RTFilter_chooseGroup_LAST_CALLER = elem;
			}

			function RTFilter_chooseGroupCatch(group)
			{
				if (group.length < 1) return;

				group = group[0];

				var inp = RTFilter_chooseGroup_LAST_CALLER;
				var hid = BX.findNextSibling(inp, {tag:'input',attr:{type:'hidden'}});
				var x = BX.findNextSibling(inp, {tag:'a'});

				hid.value = group.id;

				if (parseInt(group.id) > 0)
				{
					inp.value = group.title;
					x.style.display = 'inline';
				}
				else
				{
					inp.value = '';
					x.style.display = 'none';
				}

				try
				{
					var popup = window['filterGroupsPopup_'+inp.id];
					popup.popupWindow.close();
				}
				catch (e) {}
			}

			function RTFilter_chooseGroupClear(e)
			{
				RTFilter_chooseGroup_LAST_CALLER = BX.findChild(this.parentNode, {tag:'input',attr:{type:'text'}});

				BX.PreventDefault(e);
				RTFilter_chooseGroupCatch([{id:0}]);
			}

			</script>
			<?$name = $APPLICATION->IncludeComponent(
                "bitrix:intranet.user.selector.new",
                ".default",
                array(
                    "MULTIPLE" => "Y",
                    "NAME" => "Single_select_responsible_user",
                    "INPUT_NAME" => "select_responsible_user",
                    "VALUE" => isset($_REQUEST["select_responsible_user"]) ? $_REQUEST["select_responsible_user"] : ' ',
                    "POPUP" => "Y",
                    //"ON_SELECT" => "RTFilter_chooseUserCatch",
                    "ON_CHANGE" => "RTFilter_chooseUserCatchChange",
                    "NAME_TEMPLATE" => $arParams["USER_NAME_FORMAT"]
                ),
                null,
                array("HIDE_ICONS" => "Y")
            );?>
		</div>
	</div>
	<i class="r0"></i><i class="r1"></i><i class="r2"></i>
</div>

<?php
$this->EndViewTarget();

reportViewShowTopButtons($this, $arParams, $arResult);
?>