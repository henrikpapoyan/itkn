<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

/** @var CBitrixComponentTemplate $this */
$this->IncludeLangFile('template.php');
//Формируем данные для сотрировки по сотрудника
$arUserName = array();
foreach($arResult["DATA_ROW"] as $periodKey => $arPeriod){
    foreach($arPeriod as $userID => $data){
        if(!empty($userID)){
            $rsUser = $USER->GetByID($userID);
            $arUser = $rsUser->Fetch();
            $userName = $arUser["LAST_NAME"].' '.$arUser["NAME"];
            $arUserName[$arUser["ID"]] = $userName;
        }
    };
}
?>
<meta http-equiv="Content-type" content="text/html;charset=<?=LANG_CHARSET?>" />
<style type="text/css">
	.report-red-neg-val { color: red; }
</style>
<table border="1" cellspacing="0" cellpadding="5">
	<thead>
        <tr>
            <th>Период</th>
            <th>Ответственный</th>
            <th>Сумма выставленных, счетов, руб.</th>
            <th>Сумма выставленных и оплаченных счетов, руб.</th>
            <th>Сумма выставленных, но не оплаченных счетов, руб. (срок оплаты не прошел)</th>
            <th>Сумма выставленных, но не оплаченных счетов, руб. (срок оплаты прошел)</th>
            <th>Сумма выставленных, но не оплаченных счетов, руб. (все)</th>
            <th>Сумма отклоненных счетов, руб.</th>
            <th>Сумма удаленных счетов, руб.</th>
        </tr>
	</thead>
	<tbody>
		<?$arTotalAmountInvoice["AMOUNT_INVOICES"] = array(
            'ISSUED' => 0,
            'ISSUED_PAYED' => 0,
            'ISSUED_NOT_PAYED_NOT_OVER' => 0,
            'ISSUED_NOT_PAYED_OVER' => 0,
            'ISSUED_NOT_PAYED' => 0,
            'REJECTED' => 0,
            'DELETED' => 0,
        );
        foreach($arResult["DATA_ROW"] as $periodKey => $arPeriod):
            if(empty($arPeriod)){?>
                <tr>
                    <td><?=$periodKey?></td>
                    <td>н/д</td>
                    <td>н/д</td>
                    <td>н/д</td>
                    <td>н/д</td>
                    <td>н/д</td>
                    <td>н/д</td>
                    <td>н/д</td>
                    <td>н/д</td>
                </tr>
            <?} else {
                $firstResp = true;
                foreach($arPeriod as $responsID => $data):
                    $userName = "Ответственный не указан $responsID";
                    if(!empty($arUserName[$responsID])) {
                        $userName = $arUserName[$responsID];
                    }?>
                    <tr>
                        <?if($firstResp):
                            $firstResp = false;?>
                            <td rowspan="<?=sizeOf($arPeriod)?>"><?=$periodKey?></td>
                        <?endif;?>
                        <td><?=$userName?></td>
                        <?foreach($data["AMOUNT_INVOICES"] as $invoiceKey => $invoiceVal):
                            $arTotalAmountInvoice["AMOUNT_INVOICES"][$invoiceKey] += $invoiceVal;?>
                            <td><?=number_format($invoiceVal, 2, ",", "")?></td>
                        <?endforeach;?>
                    </tr>
                <?endforeach;
            }
        endforeach;?>
            <tr>
                <td>Все</td>
                <td>Все</td>
                <?foreach($arTotalAmountInvoice["AMOUNT_INVOICES"] as $invoiceKey => $invoiceVal):?>
                    <td><?=number_format($invoiceVal, 2, ",", "")?></td>
                <?endforeach;?>
            </tr>
	</tbody>
</table>