<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 */

$showCount = 2;
$selFastFilter = $arResult["selFastFilter"];
?>
<div class="crm-deal-panel-array-have" id="lead_type_panel_container">
	<div class="crm-deal-panel-tab">
		<div class="crm-deal-panel-tab-item crm-deal-panel-tab-item-icon-1 <?=$selFastFilter=='type_ALL'?'crm-deal-panel-tab-item-active':''?>">
            <a href="javascript:void(0)" id="leadtype_ALL" class="crm-deal-panel-tab-item-text">Все</a>
        </div>
<?
foreach($arResult['ITEMS'] as $arChennal):
	$chennalID =    $arChennal["ID"];
	$chennalValue = $arChennal["NAME"];
	$chennalCount = $arChennal["COUNTER"];
?>
	<div class="crm-deal-panel-tab-item crm-deal-panel-tab-item-icon-<?=$showCount+5?> <?=$selFastFilter=='type_'.$chennalID?'crm-deal-panel-tab-item-active':''?>">
		<a href="javascript:void(0)" id="leadtype_<?=$chennalID?>" class="crm-deal-panel-tab-item-text"><?=$chennalValue?></a>
	</div>
<?
$showCount++;
endforeach;
?>
	</div>
</div>