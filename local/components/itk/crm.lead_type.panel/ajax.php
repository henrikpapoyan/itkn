<?php
/**
 * User: German
 * Date: 16.11.2017
 * Time: 15:17
 */
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$type_id = substr($_REQUEST['type'],9);
CUserOptions::SetOption('LeadFilter', 'type_id', $type_id);
CModule::IncludeModule("crm");
$arLeadChannels = array(
    5276 => '1-ОФД',
    5275 => 'Контакт-центр',
    3336 => 'Маркетинг',
    5274 => 'Сайт'
);
	// выбираем ID состояния лида Активный
$leadStateId = 0;
$leadStateId = getIdElementListState("XML_STATE_LEAD_ACTIVITY");
$arResult = array();
$arResult["type_ALL"] = 0;
foreach($arLeadChannels as $chennalID => $chennalValue)
{
    $countLeads = 0;
    $arFilterLead = array("UF_CHANNEL" => $chennalID,
        'STATUS_ID' => 4,
//        "UF_STATE_LEAD" => $leadStateId
    );
    if($type_id > 0) //GK
    {
        $arFilterLead['UF_TYPE'] = $type_id;
    }
    $arFilterLead['!SOURCE_ID'] = gk_GetOrderFilter();
    $arSelectLead = array("ID");
    $dbLeads = CCrmLead::GetList(array(), $arFilterLead, $arSelectLead);
    $countLeads = $dbLeads->SelectedRowsCount();

    $arResult["type_ALL"] += $countLeads;

    $arResult['type_'.$chennalID] = $countLeads;
}

echo json_encode($arResult);

