<?php
/**
 * User: German
 * Date: 01.08.2017
 * Time: 15:30
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Crm\Integrity;
use Bitrix\Crm\Merger;

if(!CModule::IncludeModule("webservice") || !CModule::IncludeModule("iblock"))
    return;

// наш новый класс наследуется от базового IWebService
class CToolsWS extends IWebService
{
    // метод GetWebServiceDesc возвращает описание сервиса и его методов
    function GetWebServiceDesc()
    {
        $wsdesc = new CWebServiceDesc();
        $wsdesc->wsname = "itk.webservice.tools"; // название сервиса
        $wsdesc->wsclassname = "CToolsWS"; // название класса
        $wsdesc->wsdlauto = true;
        $wsdesc->wsendpoint = CWebService::GetDefaultEndpoint();
        $wsdesc->wstargetns = CWebService::GetDefaultTargetNS();

        $wsdesc->classTypes = array();

        $wsdesc->structTypes = Array("TCompany" => array(
            "address" => array("varType" => "string", "strict" => "no"),
            "address_legal" => array("varType" => "string", "strict" => "no"),
            "address_post" => array("varType" => "string", "strict" => "no"),
            "banking_details" => array("varType" => "string", "strict" => "no"),
            "title" => array("varType" => "string", "strict" => "no"),
            "full_name" => array("varType" => "string", "strict" => "no"),
            "comments" => array("varType" => "string", "strict" => "no"),
            "inn" => array("varType" => "string", "strict" => "no"),
            "kpp" => array("varType" => "string", "strict" => "no"),
//            "post" => array("varType" => "string", "strict" => "no"),
//            "ownership" => array("varType" => "string", "strict" => "no"),
            "okpo" => array("varType" => "string", "strict" => "no"),
            "ogrn" => array("varType" => "string", "strict" => "no"),
//            "buying_curren" => array("varType" => "string", "strict" => "no"),
//            "user_id" => array("varType" => "string", "strict" => "no"),
            "id" => array("varType" => "string", "strict" => "no"),
            "link" => array("varType" => "string", "strict" => "no"),
//            "partner" => array("varType" => "string", "strict" => "no"),
            "contact_info" => array("varType" => "TContactInfo", "strict" => "no"),
//            "relation" => array("varType" => "relation", "arrType" => "string", "strict" => "no"),
//            "abc" => array("varType" => "string", "strict" => "no"),
//            "xyz" => array("varType" => "string", "strict" => "no"),
//            "abc_count" => array("varType" => "string", "strict" => "no"),
//            "xyz_count" => array("varType" => "string", "strict" => "no"),
//            "district" => array("varType" => "string", "strict" => "no"),
//            "agreement_avail" => array("varType" => "agreement_avail", "arrType" => "string", "strict" => "no"),
//            "addr_legal" => array("varType" => "TAddress", "strict" => "no"),
//            "addr_fact" => array("varType" => "TAddress", "strict" => "no"),
//            "addr_post" => array("varType" => "TAddress", "strict" => "no"),
            "bank_details" => array("varType" => "TBank", "strict" => "no"),
//            "affiliates" => array("varType" => "affiliate", "arrType" => "string", "strict" => "no"),
//            "manager" => array("varType" => "string", "strict" => "no"),
//            "country" => array("varType" => "string", "strict" => "no"),
//            "industry" => array("varType" => "industry", "arrType" => "string", "strict" => "no"),
//            "industry_othe" => array("varType" => "string", "strict" => "no"),
//            'employees' => array("varType" => "string", "strict" => "no"),
//            'branch_exist' => array("varType" => "boolean", "strict" => "no"),
//            'branch_count' => array("varType" => "integer", "strict" => "no"),
//            'sector' => array("varType" => "sector", "arrType" => "string", "strict" => "no"),
//            "know" => array("varType" => "string", "strict" => "no"),
//            "cargo_address" => array("varType" => "string", "strict" => "no"),
//            "cargo_name" => array("varType" => "string", "strict" => "no"),
            "manager" => array("varType" => "string", "strict" => "no"),
            "cname" => array("varType" => "string", "strict" => "no"),
            ),
            "TContact" => array(
                "id" => array("varType" => "string", "strict" => "no"),
                "partner" => array("varType" => "string", "strict" => "no"),
                "link" => array("varType" => "string", "strict" => "no"),
                "name" => array("varType" => "string", "strict" => "no"),
                "phone" => array("varType" => "string", "strict" => "no"),
                "email" => array("varType" => "string", "strict" => "no"),
//                "internal" => array("varType" => "string", "strict" => "no"),
                "mobile" => array("varType" => "string", "strict" => "no"),
//                "fax" => array("varType" => "string", "strict" => "no"),
//                "skype" => array("varType" => "string", "strict" => "no"),
//                "icq" => array("varType" => "string", "strict" => "no"),
//                "sip" => array("varType" => "string", "strict" => "no"),
//                "other" => array("varType" => "string", "strict" => "no"),
                "post" => array("varType" => "string", "strict" => "no"),
                "manager" => array("varType" => "string", "strict" => "no"),
            ),

            "TContactInfo" => array(
//                "web" => array("varType" => "string", "strict" => "no"),
                "phone" => array("varType" => "string", "strict" => "no"),
                "email" => array("varType" => "string", "strict" => "no"),
//                "internal" => array("varType" => "string", "strict" => "no"),
//                "mobile" => array("varType" => "string", "strict" => "no"),
//                "fax" => array("varType" => "string", "strict" => "no"),
//                "skype" => array("varType" => "string", "strict" => "no"),
//                "icq" => array("varType" => "string", "strict" => "no"),
//                "sip" => array("varType" => "string", "strict" => "no"),
//                "other" => array("varType" => "string", "strict" => "no"),
            ),
            "TAddress" => array(
                "address" => array("varType" => "string", "strict" => "no"),
                "postcode" => array("varType" => "string", "strict" => "no"),
                "city" => array("varType" => "string", "strict" => "no"),
            ),
            "TBank" => array(
                "xml_id" => array("varType" => "string", "strict" => "no"),
                "name" => array("varType" => "string", "strict" => "no"),
                "bank" => array("varType" => "string", "strict" => "no"),
                "city" => array("varType" => "string", "strict" => "no"),
                "bik" => array("varType" => "string", "strict" => "no"),
                "rschet" => array("varType" => "string", "strict" => "no"),
                "cschet" => array("varType" => "string", "strict" => "no"),
            ),
            "TTest" => array(
                "yes" => array("varType" => "ArrayOfCompany",
                            "arrType" => "TCompany", "strict" => "no"),
                "no" => array("varType" => "ArrayOfCompany",
                    "arrType" => "TCompany", "strict" => "no"),
            ),
            "TInvoice" => array(
                "id" => array("varType" => "string", "strict" => "no"),
                "company" => array("varType" => "TCompany", "strict" => "no"),
                "xml_id" => array("varType" => "string", "strict" => "no"),
                "account_number" => array("varType" => "string", "strict" => "no"),
                "date_bill" => array("varType" => "string", "strict" => "no"),
                "pay_voucher_date" => array("varType" => "string", "strict" => "no"),
                "date_pay_before" => array("varType" => "string", "strict" => "no"),
                "comments" => array("varType" => "string", "strict" => "no"),
                "user_description" => array("varType" => "string", "strict" => "no"),
                "payed" => array("varType" => "string", "strict" => "no"),
                "pay_voucher_num" => array("varType" => "string", "strict" => "no"),
                "pay_voucher_date" => array("varType" => "string", "strict" => "no"),
                "order_topic" => array("varType" => "string", "strict" => "no"),
                "manager" => array("varType" => "string", "strict" => "no"),
                "pay_voucher_date" => array("varType" => "string", "strict" => "no"),
                "status" => array("varType" => "string", "strict" => "no"),
                "summa" => array("varType" => "string", "strict" => "no"),
                "product_rows" => array("varType" => "ArrayOfProduct",
                    "arrType" => "TProduct", "strict" => "no"),
            ),
            "TProduct" => array(
                "xml_id" => array("varType" => "string", "strict" => "no"),
                "product_name" => array("varType" => "string", "strict" => "no"),
                "full_name" => array("varType" => "string", "strict" => "no"),
                "quantity" => array("varType" => "string", "strict" => "no"),
                "price" => array("varType" => "string", "strict" => "no"),
                "sort" => array("varType" => "string", "strict" => "no"),
//GK_28.12.2018
                "vat_rate" => array("varType" => "string", "strict" => "no"),
                "vat_amount" => array("varType" => "string", "strict" => "no"),
            ),
//GK_04.04.2018
            "TFile" => array(
                "name" => array("varType" => "string", "strict" => "no"),
                "data" => array("varType" => "string", "strict" => "no"),
            ),

        );
        $wsdesc->classes = array(
            "CToolsWS"=> array(
                "TestCompany" => array(
                    "type"      => "public",
                    "input"      => array(
                        "DATA" => array("varType" => "ArrayOfCompany",
                            "arrType" => "TCompany", "strict" => "no"),
                    ),
                    "output"   => array(
                        "ret" => array("varType" => "TTest", "strict" => "no"),
                    ),
                    "httpauth" => "Y"
                ),
                "AddInvoice" => array(
                    "type"      => "public",
                    "input"      => array(
                        "DATA" => array("varType" => "TInvoice", "strict" => "no"),
                    ),
                    "output"   => array(
                        "ret" => array("varType" => "string", "strict" => "no"),
                    ),
                    "httpauth" => "Y"
                ),
                "DelInvoice" => array(
                    "type"      => "public",
                    "input"      => array(
                        "DATA" => array("varType" => "TInvoice", "strict" => "no"),
                    ),
                    "output"   => array(
                        "ret" => array("varType" => "string", "strict" => "no"),
                    ),
                    "httpauth" => "Y"
                ),
//GK_04.04.2018
                "PutFile" => array(
                    "type"      => "public",
                    "input"      => array(
                        "DATA" => array("varType" => "TFile", "strict" => "no"),
                    ),
                    "output"   => array(
                        "ret" => array("varType" => "string", "strict" => "no"),
                    ),
                    "httpauth" => "Y"
                ),
            )
        );

        return $wsdesc;
    }
    public function TestCompany($DATA)
    {
        $Result['ret']['yes'] = array();
        $Result['ret']['no'] = array();
        foreach ($DATA as $k => $v) {
            $arFilter = array('CHECK_PERMISSIONS' => 'N');
            $arFilter['UF_LINK'] = $v['link'];

            $rsCompany = CCrmCompany::GetList(array(),
                $arFilter,
                array('ID',
                    'TITLE',
                    'UF_CRM_INN',
                    'UF_LINK',
                )//'ASSIGNED_BY_ID')
            );
            if ($arCompany = $rsCompany->Fetch())
            {

                $Result['ret']['yes'][] = array(
                    'title' => $arCompany['TITLE'],
                    'inn' => $arCompany['UF_CRM_INN'],
                    'link' => $arCompany['UF_LINK']
                );
            }
            else
            {
                $Result['ret']['no'][] = array(
                    'title' => $v['title'],
                    'inn' => $v['inn'],
                    'link' => $v['link']
                );
            }
        }
        return $Result;
    }

    function AddInvoice($DATA)
    {
        define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/log.txt");
        AddMessage2Log('AddInvoice $DATA[] = '.print_r($DATA, true));
//        file_put_contents(__FILE__.".log", "DATA=[".print_r($DATA, true)."]\n", FILE_APPEND);

        $loggerNum = Logger::getLogger("InvoiceNum", "Invoice/InvoiceNum.log");
        $loggerNumNew = Logger::getLogger("InvoiceNumNew", "Invoice/InvoiceNumNew.log");
        $loggerJSON = Logger::getLogger("AddInvoiceAllJSON", "Invoice/AddAllJSON.log");
        $logger20 = Logger::getLogger("AddInvoice20", "Invoice/Invoice20.log");
        $logger20JSON = Logger::getLogger("AddInvoice20JSON", "Invoice/Invoice20JSON.log");
        $loggerErr = Logger::getLogger("AddInvoiceErr", "Invoice/Err.log");
        $logger = Logger::getLogger("AddInvoiceAll", "Invoice/AddAll.log");
        $loggerYesData = Logger::getLogger("AddInvoiceYesData", "Invoice/AddYesData.log");
        $loggerYes = Logger::getLogger("AddInvoiceYes", "Invoice/AddYes.log");
        $loggerNo = Logger::getLogger("AddInvoiceNo", "Invoice/AddNo.log");
        $loggerNoCompany = Logger::getLogger("AddInvoiceNoCompany", "Invoice/AddNoCompany.log");
        $loggerUser = Logger::getLogger("AddInvoiceUser", "Invoice/InvoiceUser.log");
        $loggerProduct = Logger::getLogger("AddInvoiceProduct", "Invoice/InvoiceProduct.log");
        $logger->log("DATA=[".print_r($DATA, true)."]");

        $loggerJSON->log(json_encode($DATA));
        $arFilter = array('UF_LINK' => $DATA['xml_id']);
        $rsInvoice = CCrmInvoice::GetList(array(), $arFilter);

        $IBLOCK_ID = 30;

        $loggerNum->log($DATA['date_bill']. ' ' . $DATA['account_number']);
        if($arInvoice = $rsInvoice->Fetch())
        {
            $logger->log("00262");
            $loggerYes->log($DATA['date_bill']. ' ' . $DATA['account_number']);
            $loggerYesData->log("DATA=[".print_r($DATA, true)."]");
            $loggerYesData->log("arInvoice=[".print_r($arInvoice, true)."]");

            $CCrmInvoice = new CCrmInvoice();
            if($arInvoice['DATE_BILL'] != $DATA['date_bill'])
            {
                $newFields = array(
                    'DATE_BILL' => $DATA['date_bill']
                );
                $loggerNumNew->log("DATA=[".print_r($DATA, true)."]");
                $loggerNumNew->log("arInvoice=[".print_r($arInvoice, true)."]");
                $ret = $CCrmInvoice->Update($arInvoice['ID'], $newFields);
                $loggerNumNew->log("ret2=[".print_r($ret, true)."]");
                $loggerNumNew->log("newFields=[".print_r($newFields, true)."]");
            }
            $year = substr($DATA['date_bill'], 6, 4);
            $number1c = $DATA['account_number'];
            if($year != 2017 )
            {
                $number1c = $DATA['account_number']." ($year)";
            }
            if($arInvoice['ACCOUNT_NUMBER'] != $number1c)
            {
                $newFields = array(
                    'ACCOUNT_NUMBER' => $number1c
                );
                $loggerNumNew->log("DATA=[".print_r($DATA, true)."]");
                $loggerNumNew->log("arInvoice=[".print_r($arInvoice, true)."]");
                $ret = $CCrmInvoice->Update($arInvoice['ID'], $newFields);
                $loggerNumNew->log("ret=[".print_r($ret, true)."]");
                $loggerNumNew->log("newFields=[".print_r($newFields, true)."]");
            }
            $manager = $this->FindUser($DATA);
            if($manager != $arInvoice['RESPONSIBLE_ID'] && $manager != 372 && $manager > 0)
            {
                $newFields = array(
                    'RESPONSIBLE_ID' => $manager
                );
                $ret5 = $CCrmInvoice->Update($arInvoice['ID'], $newFields);
            }
            $arProductRows = CCrmInvoice::GetProductRows($arInvoice['ID']);
            $loggerYesData->log("arProductRows=[".print_r($arProductRows, true)."]");
//            file_put_contents(__FILE__.".log", "arProductRows=[".print_r($arProductRows, true)."]\n", FILE_APPEND);
//            file_put_contents(__FILE__.".log", "product_rows=[".print_r($DATA['product_rows'], true)."]\n", FILE_APPEND);
            if (count($DATA['product_rows'])>10)
            {
                $new_product_rows = array();
                foreach ($DATA['product_rows'] as $key => $val) {
                    $xml_id_price = $val['xml_id'].$val['price'];
                    if($new_product_rows[$xml_id_price])
                    {
                        $new_product_rows[$xml_id_price]['quantity'] = $new_product_rows[$xml_id_price]['quantity'] + $val['quantity'];
                    }
                    else
                    {
                        $new_product_rows[$xml_id_price] = $val;
                    }
                }
                $DATA['product_rows'] = array();//$new_product_rows;
                foreach ($new_product_rows as $iprod => $vprod) {
                    $DATA['product_rows'][] = $vprod;
                }

//                    $logger20->log($DATA['account_number']);
//                    $logger20JSON->log(json_encode($DATA));
//                    return array('ret' => "-4");

            }

            $ProductRowsCount = count($arProductRows);
            $product_rows_count = count($DATA['product_rows']);
            file_put_contents(__FILE__.".log", "00324DATA=".print_r($DATA, true)."\n", FILE_APPEND);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $summa1c = $DATA['summa'];
            $summa1c = str_replace(" ", "", $summa1c);
            $summa1c = str_replace(",", ".", $summa1c);
            $summa = 0;
            foreach ($DATA['product_rows'] as $ip => $vp) {
                $logger->log("vp=[".print_r($vp, true)."]");
                $kol = $vp['quantity'];
                if ($kol < 1)
                {
                    $kol = 1;
                }
                $kol = str_replace(" ", "", $kol);
                $price = $vp['price'];
                $price = str_replace(" ", "", $price);
                $price = str_replace(",", ".", $price);
                $summa_calc = $kol*$price;
                $summa = $summa + $summa_calc;
////                $summa_row = $vp[6];
////                $summa_row = str_replace(" ", "", $summa_row);
////                $summa_row = str_replace(",", ".", $summa_row);
//
//                $logger->log("$kol * $price = $summa_calc == $summa_row");
            }
            $logger->log("summa $summa == ".$arInvoice['PRICE']. " == " .$summa1c);
            $bSumma = false;
            if((floatval(strval($summa)) != floatval($arInvoice['PRICE']))
//            && (floatval($v['invoice']['PRICE']) != floatval($summa1c))
            )
            {
                $bSumma = true;
            }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $msg="";
            if ($ProductRowsCount != $product_rows_count || $bSumma)
            {
                $i=0;
                if($product_rows_count < $ProductRowsCount)
                {
                    file_put_contents(__FILE__.".log",
                        "00311 i=$i ProductRowsCount = $ProductRowsCount product_rows_count=$product_rows_count]\n", FILE_APPEND);
                    foreach ($arProductRows as $ip => $vp) {
                        if($ip >= $product_rows_count)
                        {
                            unset($arProductRows[$ip]);
                            file_put_contents(__FILE__.".log",
                                "00317 unset ip=$ip \n", FILE_APPEND);
                        }
                    }

                    $ProductUpdate = true;
                }
                foreach ($DATA['product_rows'] as $iproduct => $vproduct) {
                    if ($i < $ProductRowsCount && $i < $product_rows_count)
                    {
                        if($arProductRows[$i]['PRODUCT_XML_ID'] != $DATA['product_rows'][$i]['xml_id'])
                        {
                            $arProduct = $this->FindProduct($vproduct);
                            $arProductRows[$i]['PRODUCT_ID'] = $arProduct['ID'];
                            $arProductRows[$i]['PRODUCT_NAME'] = $arProduct['NAME'];
                            $ProductUpdate = true;
                        }
                        if($arProductRows[$i]['PRICE'] != $DATA['product_rows'][$i]['price'])
                        {
                            $arProductRows[$i]['PRICE'] = $DATA['product_rows'][$i]['price'];
                            $ProductUpdate = true;
                        }
                        if($arProductRows[$i]['QUANTITY'] != $DATA['product_rows'][$i]['quantity'])
                        {
                            $quantity =  $DATA['product_rows'][$i]['quantity'];
                            if ($quantity == 0)
                            {
                                $quantity = 1;
                            }
                            $loggerErr->log("00330quantity =[".print_r($quantity, true));
                            $arProductRows[$i]['QUANTITY'] = $quantity;
                            $ProductUpdate = true;
                        }
//                        if (isset($arProductRows[$i]['MODULE']) && empty($arProductRows[$i]['MODULE']))
//                        {
//                            $arProductRows[$i]['MODULE'] = "sale";
//                        }
                    }
                    elseif($ProductRowsCount <= $i)
                    {
                        $arProduct = $this->FindProduct($vproduct);
    //                    file_put_contents(__FILE__.".log", "arProduct=[".print_r($arProduct,true)."]\n", FILE_APPEND);
                        $quantity =  $vproduct['quantity'];
                        if ($quantity == 0)
                        {
                            $quantity = 1;
                        }
                        $loggerErr->log("00343quantity =[".print_r($quantity, true));

                        $arProductRows[] = array(
                            'ID' => 0,
                            'ORDER_ID' => $arInvoice['ID'],
                            'PRODUCT_ID' => $arProduct['ID'],
                            'QUANTITY' => $quantity, //$vproduct['quantity'],//88.0000,
                            'PRICE' => $vproduct['price'],//250.0000,
                            'CUSTOM_PRICE' => 'Y',
                            'DISCOUNT_PRICE' => 0.0000,
                            'VAT_RATE' => $vproduct['vat_rate']/100,
                            'VAT_INCLUDED' => 'Y',
                            'MEASURE_CODE' => 796,
                            'MEASURE_NAME' => 'шт',
    //                        [MODULE] =>.
                            'CATALOG_XML_ID' => '3d9921f1-6994-4270-ac8a-7e7f3e3e6cb5',
                            'PRODUCT_XML_ID' => $vproduct['xml_id'],//'3d1151dd-40af-11e7-80be-00155d305d10',
                            'PRODUCT_NAME' => $vproduct['product_name']//'Услуга ОФД'

                        );
                        $ProductUpdate = true;
                    }
                    else
                    {
                        file_put_contents(__FILE__.".log",
                            "i=$i ProductRowsCount = $ProductRowsCount product_rows_count=$product_rows_count]\n", FILE_APPEND);
                    }
                    $i++;
                }
                $logger->log('$ProductUpdate'.var_dump($ProductUpdate));
                if($ProductUpdate)
                {
                    $arFields = $arInvoice;
                    // Get invoice properties
                    $arInvoiceProperties = array();
                    $personTypeId = 1;
                    $tmpArProps = $CCrmInvoice->GetProperties($arInvoice['ID'], $personTypeId);
                    if ($tmpArProps !== false)
                    {
                        $arInvoiceProperties = $tmpArProps;
                        if (!isset($arFields['PR_LOCATION']) && isset($arInvoiceProperties['PR_LOCATION']))
                            $arFields['PR_LOCATION'] = $arInvoiceProperties['PR_LOCATION']['VALUE'];
                    }
                    unset($tmpArProps);
                    $formProps = array();
                    $tmpArInvoicePropertiesValues = $CCrmInvoice->ParsePropertiesValuesFromPost($personTypeId, $formProps, $arInvoiceProperties);
                    if (isset($tmpArInvoicePropertiesValues['PROPS_VALUES']) && isset($tmpArInvoicePropertiesValues['PROPS_INDEXES']))
                    {
                        $arFields['INVOICE_PROPERTIES'] = $tmpArInvoicePropertiesValues['PROPS_VALUES'];
                        foreach ($tmpArInvoicePropertiesValues['PROPS_INDEXES'] as $propertyName => $propertyIndex)
                            if (!isset($arFields[$propertyName]))
                                $arFields[$propertyName] = $tmpArInvoicePropertiesValues['PROPS_VALUES'][$propertyIndex];
                    }
                    unset($tmpArInvoicePropertiesValues);

                    $arFields['PRODUCT_ROWS'] = $arProductRows;
                    file_put_contents(__FILE__.".log", "00439arFields=[".print_r($arFields,true)."]\n", FILE_APPEND);
                    try
                    {
                        $arUserProp = getUserPropArray('XML_ID', 'UF_INVOICE_SUMM_FROM_1C');
                        if(!empty($arUserProp)) {
                            $fieldName = $arUserProp["FIELD_NAME"];
                            $arFields[$fieldName] = $summa;
                        }
                        $logger->log('$arFields'.print_r($arFields, true));
                        if($ret3 = $CCrmInvoice->Update($arInvoice['ID'], $arFields)) {
                            $logger->log('Invoice '.$arInvoice['ID'].' updated');
                        } else {
                            $logger->log('ERROR updated Invoice '.$arInvoice['ID'].'. '.$CCrmInvoice->LAST_ERROR);
                        }
                    }
                    catch (Exception $e) {
                        $loggerErr->log($e->getMessage());
                        $loggerErr->log("Code=[".print_r($e->getCode(), true));
                        $loggerErr->log("File=[".print_r($e->getFile(), true));
                        $loggerErr->log("Line=[".print_r($e->getLine(), true));
                        $loggerErr->log("DATA =[".print_r($DATA, true));
                        $loggerErr->log("arInvoice[ID]".print_r($arInvoice['ID'], true));
                        $loggerErr->log("arFields".print_r($arFields, true));
                        $ErrMSG = $e->getMessage();

                        if (strpos($ErrMSG, "Mysql query error: (1062) Duplicate entry") !== false)
                        {
                            return array('ret' => "-461");
                        }
                        elseif(strpos($ErrMSG, "Empty module name") !== false)
                        {
                            $ret = $CCrmInvoice->Delete($arInvoice['ID']);
                            $loggerErr->log("Delete[".print_r($ret, true)."]");
                            return array('ret' => "-440");
                        }
                        else
                        {
                            return array('ret' => "-444");
                        }
                    }
    //                file_put_contents(__FILE__.".log", "ret3=[".print_r($ret3,true)."]\n", FILE_APPEND);
                }
            }

            if($DATA['payed'] == 'Y' and $arInvoice["PAYED"] != "Y")
            {

                $statusParams= array(
                    'STATE_SUCCESS' => true,
                    'STATE_FAILED' => false,
                    'PAY_VOUCHER_NUM' => $DATA['pay_voucher_num'],
                    'PAY_VOUCHER_DATE' => $DATA['pay_voucher_date'],//] => 02.08.2017
                    'DATE_MARKED' => $DATA['pay_voucher_date'],//02.08.2017
                    'REASON_MARKED' => ''
                );
                $ret = $CCrmInvoice->SetStatus($arInvoice["ID"], 'P', $statusParams);

                $logger->log("ret=[".print_r($ret, true)."]");

                AddMessage2Log($ret,"arInvoice2");
            }
            else
            {
                if($DATA['status'] == 'Отменен')
                {
                    $statusParams= array(
                        'STATE_SUCCESS' => false,
                        'STATE_FAILED' => true,
                    );
                    $ret = $CCrmInvoice->SetStatus($arInvoice["ID"], 'D', $statusParams);
                }
                elseif($DATA['status'] == 'Оплачен' || $DATA['status'] == 'ОплаченЧастично')
                {
                    $statusParams= array(
                        'STATE_SUCCESS' => true,
                        'STATE_FAILED' => false,
                        //            [PAY_VOUCHER_NUM] => 222
                        //            [PAY_VOUCHER_DATE] => 02.08.2017
                        //            [DATE_MARKED] => 02.08.2017
                        //            [REASON_MARKED] =>
                    );
                    $ret = $CCrmInvoice->SetStatus($arInvoice["ID"], 'P', $statusParams);
                }
                elseif($DATA['status'] == 'НеОплачен')
                {
                    $statusParams= array(
                        'STATE_SUCCESS' => false,
                        'STATE_FAILED' => false,
                    );
                    $ret = $CCrmInvoice->SetStatus($arInvoice["ID"], 'A', $statusParams);
                }
            }

            return array('ret' => "22");
        }
        else
        {
            $loggerNo->log($DATA['date_bill']. ' ' . $DATA['account_number']);
            $arCompany = false;

            $requisite = new \Bitrix\Crm\EntityRequisite();
            $fieldsInfo = $requisite->getFormFieldsInfo();
            $select = array_keys($fieldsInfo);
            $arRQ = false;

            if(!empty($DATA['company']['link']))
            {
                $arFilter = array('UF_LINK' => $DATA['company']['link']);
                $rsCompany = CCrmCompany::GetList(array(),  $arFilter);
                $arCompany = $rsCompany->Fetch();

                $arFilter = array(
                    '=ENTITY_TYPE_ID' => 4,
                    '=ENTITY_ID' => $arCompany["ID"]
                );

                $rsRQ = $requisite->getList(
                    array(
                        'order' => array('SORT' => 'ASC', 'ID' => 'ASC'),
                        'filter' => $arFilter,
                        'select' => $select
                    )
                );
                $arRQ = $rsRQ->fetch();
            }
            if(!$arCompany)
            {
                $arFilter = array(
                    '=RQ_INN' => $DATA['company']['inn']
                );
                $rsRQ = $requisite->getList(
                    array(
                        'order' => array('SORT' => 'ASC', 'ID' => 'ASC'),
                        'filter' => $arFilter,
                        'select' => $select
                    )
                );

                if ($arRQ = $rsRQ->fetch()) {
                    $logger->log("arRQ=[".print_r($arRQ, true)."]");
                    $rsCompany = CCrmCompany::GetList(array(), array('ID' => $arRQ['ENTITY_ID']));
                    $arCompany = $rsCompany->Fetch();
                    $logger->log("INN");
                }

            }
            if ($arCompany)
            {
                $logger->log("arRQ=[".print_r($arRQ, true)."]");
                $logger->log("arCompany=[".print_r($arCompany, true)."]");
                //Сверяем ИНН и КПП из 1С и Б24
                if( !empty($DATA['company']['kpp']) && !empty($arRQ) ){
                    $kppFrom1C = $DATA['company']['kpp'];
                    if( $arRQ["RQ_KPP"] != $kppFrom1C ) {
                        $requisite->Update($arRQ["ID"], array("RQ_KPP" => $kppFrom1C));
                        SetUserField("CRM_COMPANY", $arCompany["ID"], "UF_CRM_KPP", $kppFrom1C);
                    }
                };

                $product_rows = array();
                $arFilter = Array('IBLOCK_ID'=>$IBLOCK_ID, 'XML_ID'=>'fe864148-2dd7-456a-b9c9-37c6dae7be1d');
                $rsProductSection = CIBlockSection::GetList(array(), $arFilter);
                $section_id = 0;
                if ($arProductSection = $rsProductSection->Fetch())
                {
                    $section_id = $arProductSection['ID'];
                    $loggerProduct->log("section_id=[".print_r($section_id, true)."]");
                }
                foreach ($DATA['product_rows'] as $key => $val) {
                    $DATA['product_rows'][$key]['price'] = str_replace(",", ".", $val['price']);
                }
                $DATA['summa'] = str_replace(",", ".", $DATA['summa']);
                if (count($DATA['product_rows'])>10)
                {
                    $new_product_rows = array();
                    foreach ($DATA['product_rows'] as $key => $val) {
                        $xml_id_price = $val['xml_id'].$val['price'];
                        if($new_product_rows[$xml_id_price])
                        {
                            $new_product_rows[$xml_id_price]['quantity'] = $new_product_rows[$xml_id_price]['quantity'] + $val['quantity'];
                        }
                        else
                        {
                            $new_product_rows[$xml_id_price] = $val;
                        }
                    }
                    $DATA['product_rows'] = $new_product_rows;
//                    $logger20->log($DATA['account_number']);
//                    $logger20JSON->log(json_encode($DATA));
//                    return array('ret' => "-4");

                }
                foreach ($DATA['product_rows'] as $k => $v) {
                    $logger->log("v=[".print_r($v, true)."]");
                    $rsProduct = CIBlockElement::GetList(array(), array('XML_ID' => $v['xml_id'], 'IBLOCK_ID' => $IBLOCK_ID));
                    if ($arProduct = $rsProduct->Fetch())
                    {
                    }
                    else
                    {
                        $v['section_id'] = $section_id;
                        $loggerProduct->log("account_number=[".print_r($DATA['account_number'], true)."]");
                        $loggerProduct->log("date_bill=[".print_r($DATA['date_bill'], true)."]");
                        $loggerProduct->log("v=[".print_r($v, true)."]");
                        $product_id = $this->AddProduct($v);
                        $loggerProduct->log("product_id=[".print_r($product_id, true)."]");
                        $arProduct = array(
                            'ID' => $product_id,
                            'NAME' => $v['product_name'],
                        );
                    }
                    if (count($arProduct) > 0)
                    {
                        $logger->log("arProduct=[".print_r($arProduct, true)."]");
                        $quantity = $v['quantity'];
                        if ($quantity == 0)
                        {
                            $quantity = 1;
                        }
                        $product_row = array(
                            'ID' => 0,
                            'PRODUCT_ID' => $arProduct['ID'],
                            'PRODUCT_NAME' => $arProduct['NAME'],
                            'QUANTITY' => $quantity,//GK_15.01.2018 $v['quantity'],
                            'PRICE' => $v['price'],
                            'VAT_INCLUDED' => 'Y',
                            'VAT_RATE' => $v['vat_rate']/100,
                            'DISCOUNT_PRICE' => 0,
                            'MEASURE_CODE' => 796,
                            'MEASURE_NAME' => 'шт',
                            'CUSTOMIZED' => 'Y',
                            'SORT' => $v['sort'],
                        );
                        $product_rows[] = $product_row;
                    }
                }
                if (empty($DATA['order_topic']))
                {
                    $order_topic = 'Из 1С';
                }
                else
                {
                    $order_topic = $DATA['order_topic'];
                }
                $loggerUser->log("manager=[".print_r($DATA['manager'], true)."]");
                if(empty($DATA['manager']))
                {
                    $manager = 372;
                }
                else
                {
                    $arFIO = explode(" ",$DATA['manager']);
                    $arFilter = array(
                        'LAST_NAME' => $arFIO[0],
                        'NAME' => $arFIO[1],
                    );
                    $loggerUser->log("manager=[".print_r($DATA['manager'], true)."]");
                    $rsUser = CUser::GetList(($by="personal_country"), ($order="desc"), $arFilter);
                    if($arUser = $rsUser->Fetch())
                    {
                        $loggerUser->log("arUser=[".print_r($arUser, true)."]");
                        $manager = $arUser['ID'];
                    }
                    else
                    {
                        $loggerUser->log("NO");
                        $manager = 372;
                    }
                }
                if($manager == 372)
                {
                    $arCompany2 = CCrmCompany::GetByID($arCompany['ID']);
                    if ($arCompany2['ASSIGNED_BY'] > 0 && $arCompany2['ASSIGNED_BY'] != $manager)
                    {
                        $manager = $arCompany2['ASSIGNED_BY'];
                    }
                }
                $year = date("Y", strtotime($DATA['date_bill']));

                if ($year == 2017)
                {
                    $ACCOUNT_NUMBER = $DATA['account_number'];
                }
                else

                {
                    $ACCOUNT_NUMBER = $DATA['account_number']." ($year)";
                }
                $arFields = array
                (
                    'ORDER_TOPIC' => $order_topic,
                    'STATUS_ID' => 'N',
                    'DATE_BILL' => $DATA['date_bill'],//'02.08.2017',
                    'PAY_VOUCHER_DATE' => '',
                    'DATE_PAY_BEFORE' => $DATA['date_pay_before'],//'04.08.2017',
                    'RESPONSIBLE_ID' => $manager,
                    'COMMENTS' => '',
                    'USER_DESCRIPTION' => '',
                    'UF_QUOTE_ID' => 0,
                    'UF_DEAL_ID' => 0,
                    'UF_COMPANY_ID' => $arCompany['ID'],
                    'UF_CONTACT_ID' => 0,
                    'UF_MYCOMPANY_ID' => 55060,
                    'UF_LINK' => $DATA['xml_id'],
                    'UF_CRM_1514204770' => $DATA['summa'],
                    'EXTERNAL_ORDER' => 'Y',
                    'PRODUCT_ROWS' => $product_rows,

                    'PERSON_TYPE_ID' => 1,
                    'PAY_SYSTEM_ID' => 1,
                    'ACCOUNT_NUMBER' => $ACCOUNT_NUMBER,//$DATA['account_number'],

                    'INVOICE_PROPERTIES' => array
                    (
                        '10' => $DATA['company']['title'],//'ИП Иванова Анна Вадимовна', //Имя
                        '11' => $DATA['company']['address_legal'],//'ПСКОВСКАЯ, ПСКОВ,  ДОМ 21, КВАРТИРА 30', //Адрес
                        '8' => $DATA['company']['inn'],//'143527334122', //ИНН
                        '9' => $DATA['company']['kpp'], //КПП
                        '12' => '',
                        '13' => $DATA['company']['contact_info']['email'],//'zahareckaya@list.ru', //email
                        '14' => $DATA['company']['contact_info']['phone'],//'9517558844', //телефон
                        '15' => '',
                        '16' => '',
                        '17' => '',
                        '18' => '',
                        '19' => '',
                    ),

                    'PR_INVOICE_10' => $DATA['company']['title'],//'ИП Иванова Анна Вадимовна',
                    'PR_INVOICE_11' => $DATA['company']['address_legal'],//'ПСКОВСКАЯ, ПСКОВ,  ДОМ 21, КВАРТИРА 30',
                    'PR_INVOICE_8' => $DATA['company']['inn'],//'143527334122',
                    'PR_INVOICE_9' => $DATA['company']['kpp'],//'',
                    'PR_INVOICE_12' => '',
                    'PR_INVOICE_13' => $DATA['company']['contact_info']['email'],//'zahareckaya@list.ru',
                    'PR_INVOICE_14' => $DATA['company']['contact_info']['phone'],//'9517558844',
                    'PR_INVOICE_15' => '',
                    'PR_INVOICE_16' => '',
                    'PR_INVOICE_17' => '',
                    'PR_INVOICE_18' => '',
                    'PR_INVOICE_19' => '',
                );

                $CCrmInvoice = new CCrmInvoice();
                $logger->log("arFields=[".print_r($arFields, true)."]");
                try
                {
                    $invoice_id = $CCrmInvoice->Add($arFields);
                }
                catch (Exception $e) {
                    $loggerErr->log(json_encode($DATA));
                    $loggerErr->log($e->getMessage());
                    $ErrMSG = $e->getMessage();
                    if (strpos($ErrMSG, "Mysql query error: (1062) Duplicate entry") !== false)
                    {
                        return array('ret' => "-751");
                    }
                    else
                    {
                        return array('ret' => "-686");
                    }
                }
                $logger->log("invoice_id=[".print_r($invoice_id, true)."]");
                $year = date("Y", strtotime($DATA['date_bill']));
                if ($year == 2017)
                {
                    $arFields = array(
                        'ACCOUNT_NUMBER' => $DATA['account_number'],
                    );
                }
                else

                {
                    $arFields = array(
                        'ACCOUNT_NUMBER' => $DATA['account_number']." ($year)", //GK_11.01.2018
                    );
                }
                $ret = $CCrmInvoice->Update($invoice_id, $arFields);
                $logger->log("ret=[".print_r($ret, true)."]");
                if($DATA['payed'] == 'Y')
                {
                    $statusParams= array(
                        'STATE_SUCCESS' => true,
                        'STATE_FAILED' => false,
                        'PAY_VOUCHER_NUM' => $DATA['pay_voucher_num'],
                        'PAY_VOUCHER_DATE' => $DATA['pay_voucher_date'],//] => 02.08.2017
                        'DATE_MARKED' => $DATA['pay_voucher_date'],//02.08.2017
                        'REASON_MARKED' => ''
                    );
                    $ret = $CCrmInvoice->SetStatus($invoice_id, 'P', $statusParams);
                    $logger->log("ret=[".print_r($ret, true)."]");
                }
                else
                {
                    if($DATA['status'] == 'Отменен')
                    {
                        $statusParams= array(
                            'STATE_SUCCESS' => false,
                            'STATE_FAILED' => true,
                        );
                        $ret = $CCrmInvoice->SetStatus($invoice_id, 'D', $statusParams);
                    }
                    elseif($DATA['status'] == 'Оплачен' || $DATA['status'] == 'ОплаченЧастично')
                    {
                        $statusParams= array(
                            'STATE_SUCCESS' => true,
                            'STATE_FAILED' => false,
                            //            [PAY_VOUCHER_NUM] => 222
                            //            [PAY_VOUCHER_DATE] => 02.08.2017
                            //            [DATE_MARKED] => 02.08.2017
                            //            [REASON_MARKED] =>
                        );
                        $ret = $CCrmInvoice->SetStatus($invoice_id, 'P', $statusParams);
                    }
                    else
                    {
                        $statusParams= array(
                            'STATE_SUCCESS' => false,
                            'STATE_FAILED' => false,
                        );
                        $ret = $CCrmInvoice->SetStatus($invoice_id, 'A', $statusParams);
                    }
                }

            }
            else
            {
                $loggerNo->log("company=[".print_r($DATA['company'], true)."]");
                $loggerNoCompany->log($DATA['account_number']);
                return array('ret' => "-825");
            }

        }
        return array('ret' => "21");
        return;
    }
    function AddProduct($data)
    {
        AddMessage2Log($data,"AddProduct");
        $arFields = array(
            'NAME' => $data['product_name'],
//    [DESCRIPTION] =>
            'ACTIVE' => 'Y',
            'CURRENCY_ID' => 'RUB',
            'PRICE' => 0,
            'VAT_ID' => 2,
            'VAT_INCLUDED' => 'Y',
            'MEASURE' => 5,
            'SECTION_ID' => $data['section_id'],
            'SORT' => 500,
            'PREVIEW_TEXT' => $data['full_name'],
            'PREVIEW_TEXT_TYPE' => 'text',
            'XML_ID' => $data['xml_id'],
            'CATALOG_ID' => 30,
        );
        $product_id = CCrmProduct::Add($arFields);

        return $product_id;
    }
    function FindProduct($v)
    {
        $IBLOCK_ID = 30;
        $arFilter = Array('IBLOCK_ID'=>$IBLOCK_ID, 'XML_ID'=>'fe864148-2dd7-456a-b9c9-37c6dae7be1d');
        $rsProductSection = CIBlockSection::GetList(array(), $arFilter);
        $section_id = 0;
        if ($arProductSection = $rsProductSection->Fetch())
        {
            $section_id = $arProductSection['ID'];
        }

        $rsProduct = CIBlockElement::GetList(array(), array('XML_ID' => $v['xml_id'], 'IBLOCK_ID' => $IBLOCK_ID));
        if ($arProduct = $rsProduct->Fetch())
        {
        }
        else
        {
            $v['section_id'] = $section_id;
            $product_id = $this->AddProduct($v);
            $arProduct = array(
                'ID' => $product_id,
                'NAME' => $v['product_name'],
            );
        }
        return $arProduct;
    }

    function FindUser($DATA, $company_id = 0)
    {
        $manager = 372;
        if(empty($DATA['manager']))
        {
            $manager = 372;
        }
        else
        {
            $arFIO = explode(" ",$DATA['manager']);
            $arFilter = array(
                'LAST_NAME' => $arFIO[0],
                'NAME' => $arFIO[1],
            );
            $rsUser = CUser::GetList(($by="personal_country"), ($order="desc"), $arFilter);
            if($arUser = $rsUser->Fetch())
            {
                $manager = $arUser['ID'];
            }
            else
            {
                $manager = 372;
            }
        }
        if($manager == 372 && $company_id > 0)
        {
            $arCompany2 = CCrmCompany::GetByID($company_id);
            if ($arCompany2['ASSIGNED_BY'] > 0 && $arCompany2['ASSIGNED_BY'] != $manager)
            {
                $manager = $arCompany2['ASSIGNED_BY'];
            }
        }
        return $manager;
    }

    function DelInvoice($DATA)
    {
        $logger = Logger::getLogger("InvoiceDel", "Invoice/InvoiceDel.log");
        $loggerJSON = Logger::getLogger("InvoiceDelJSON", "Invoice/InvoiceDelJSON.log");
        $logger->log("DATA=[".print_r($DATA, true)."]");

        $CCrmInvoice = new CCrmInvoice();
        $loggerJSON->log(json_encode($DATA));
        if($DATA['id'] > 0)
        {
            $statusParams= array(
                'STATE_SUCCESS' => false,
                'STATE_FAILED' => true,
            );
            $ret = $CCrmInvoice->SetStatus($DATA['id'], 'Y', $statusParams);
            $logger->log("ret=[".print_r($ret, true)."]");
            $logger->log("CCrmInvoice=[".print_r($CCrmInvoice, true)."]");
            $newInvoice = CCrmInvoice::GetByID($DATA['id']);
            $logger->log("newInvoice=[".print_r($newInvoice, true)."]");
        }
        elseif(!empty($DATA['xml_id']))
        {
            $rsInvoice = CCrmInvoice::GetList(array(), array('UF_LINK' => $DATA['xml_id']));
            if($arInvoice = $rsInvoice->Fetch())
            {
                $logger->log("arInvoice=[".print_r($arInvoice, true)."]");
                $statusParams= array(
                    'STATE_SUCCESS' => false,
                    'STATE_FAILED' => true,
                );
                $ret = $CCrmInvoice->SetStatus($arInvoice['ID'], 'Y', $statusParams);
                $logger->log("ret2=[".print_r($ret, true)."]");
            }
        }
        return array('ret' => "0");
    }
//GK_04.04.2018
    function PutFile($DATA)
    {
        $dir = $_SERVER["DOCUMENT_ROOT"]."/upload/1c_invoice";
        if (!file_exists($dir))
        {
            mkdir($dir, 0777, true);
        }
        $pathZip = $dir."/".$DATA['name'];
        file_put_contents($pathZip, base64_decode($DATA['data']));
        $hZip = zip_open($pathZip);
        if(!$hZip)
        {
            return array('ret' => "-1");
        }
        while($entry = zip_read($hZip))
        {
            $entry_name = zip_entry_name($entry);
            //Check for directory
            zip_entry_open($hZip, $entry);
            if(zip_entry_filesize($entry))
            {
                $file_name = $dir."/".$entry_name;

                $fout = fopen($file_name, "wb");
                if(!$fout)
                    return false;
                while($data = zip_entry_read($entry, 102400))
                {
                    $data_len = function_exists('mb_strlen') ? mb_strlen($data, 'latin1') : strlen($data);
                    $result = fwrite($fout, $data);
                    if($result !== $data_len)
                        return false;
                }

            }
            zip_entry_close($entry);

        }
        zip_close($hZip);

        return array('ret' => "0");
    }
}

$arParams["WEBSERVICE_NAME"] = "itk.webservice.tools";
$arParams["WEBSERVICE_CLASS"] = "CToolsWS";
$arParams["WEBSERVICE_MODULE"] = "";

// передаем в компонент описание веб-сервиса
$APPLICATION->IncludeComponent(
    "bitrix:webservice.server",
    "",
    $arParams
);

die();
?>
