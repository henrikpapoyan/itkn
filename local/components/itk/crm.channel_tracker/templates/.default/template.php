<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

/**
 * Bitrix vars
 * @global CUser $USER
 * @global CMain $APPLICATION
 * @global CDatabase $DB
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponent $component
 */

$APPLICATION->IncludeComponent(
	'bitrix:crm.control_panel',
	'',
	array(
		'ID' => 'START',
		'ACTIVE_ITEM_ID' => 'START',
		'PATH_TO_COMPANY_LIST' => isset($arParams['PATH_TO_COMPANY_LIST']) ? $arParams['PATH_TO_COMPANY_LIST'] : '',
		'PATH_TO_COMPANY_EDIT' => isset($arParams['PATH_TO_COMPANY_EDIT']) ? $arParams['PATH_TO_COMPANY_EDIT'] : '',
		'PATH_TO_CONTACT_LIST' => isset($arParams['PATH_TO_CONTACT_LIST']) ? $arParams['PATH_TO_CONTACT_LIST'] : '',
		'PATH_TO_CONTACT_EDIT' => isset($arParams['PATH_TO_CONTACT_EDIT']) ? $arParams['PATH_TO_CONTACT_EDIT'] : '',
		'PATH_TO_DEAL_LIST' => isset($arParams['PATH_TO_DEAL_LIST']) ? $arParams['PATH_TO_DEAL_LIST'] : '',
		'PATH_TO_DEAL_EDIT' => isset($arParams['PATH_TO_DEAL_EDIT']) ? $arParams['PATH_TO_DEAL_EDIT'] : '',
		'PATH_TO_LEAD_LIST' => isset($arParams['PATH_TO_LEAD_LIST']) ? $arParams['PATH_TO_LEAD_LIST'] : '',
		'PATH_TO_LEAD_EDIT' => isset($arParams['PATH_TO_LEAD_EDIT']) ? $arParams['PATH_TO_LEAD_EDIT'] : '',
		'PATH_TO_QUOTE_LIST' => isset($arResult['PATH_TO_QUOTE_LIST']) ? $arResult['PATH_TO_QUOTE_LIST'] : '',
		'PATH_TO_QUOTE_EDIT' => isset($arResult['PATH_TO_QUOTE_EDIT']) ? $arResult['PATH_TO_QUOTE_EDIT'] : '',
		'PATH_TO_INVOICE_LIST' => isset($arResult['PATH_TO_INVOICE_LIST']) ? $arResult['PATH_TO_INVOICE_LIST'] : '',
		'PATH_TO_INVOICE_EDIT' => isset($arResult['PATH_TO_INVOICE_EDIT']) ? $arResult['PATH_TO_INVOICE_EDIT'] : '',
		'PATH_TO_REPORT_LIST' => isset($arParams['PATH_TO_REPORT_LIST']) ? $arParams['PATH_TO_REPORT_LIST'] : '',
		'PATH_TO_DEAL_FUNNEL' => isset($arParams['PATH_TO_DEAL_FUNNEL']) ? $arParams['PATH_TO_DEAL_FUNNEL'] : '',
		'PATH_TO_EVENT_LIST' => isset($arParams['PATH_TO_EVENT_LIST']) ? $arParams['PATH_TO_EVENT_LIST'] : '',
		'PATH_TO_PRODUCT_LIST' => isset($arParams['PATH_TO_PRODUCT_LIST']) ? $arParams['PATH_TO_PRODUCT_LIST'] : ''
	),
	$component
);

$guid = $arResult['GUID'];
$config = $arResult['CONFIG'];
$items = $arResult['ITEMS'];
$groupItems = $arResult['GROUP_ITEMS'];
$totals = $arResult['TOTALS'];
$groupTotals = $arResult['GROUP_TOTALS'];
$messages =  $arResult['MESSAGES'];
$containerID = "{$guid}_container";
$toggleButtonID = "{$guid}_toggle_btn";
$helpButtonID = "{$guid}_help_btn";
$isExpanded = $config['expanded'] === 'Y';
$APPLICATION->ShowViewContent('widget_panel_header');
$arLeadsConvert = $arResult["arLeadsConvert"];
?>
<div class="startpage-table-wrap">
	<div class="startpage-table-wrap-head">
		<span id="<?=htmlspecialcharsbx($helpButtonID)?>" class="startpage-table-wrap-help"></span>
		<span class="startpage-table-wrap-title-container">
			<span class="startpage-table-wrap-title-inner">
				<span class="startpage-table-wrap-title"><?=htmlspecialcharsbx($arResult['TITLE'])?></span>
			</span>
		</span>
	</div>
</div>
<?$tableHead = array($arLeadsConvert[0],$arLeadsConvert[1]);?>
<div style="overflow: auto">
    <table border="0" cellpadding="5" cellspacing="0" id="leads_convert_table">
        <thead>
        <?foreach($tableHead as $key=>$lineText):
            $arTD = explode('|', $lineText);
            $trClass = array_pop($arTD);?>
            <tr>
            <?
            foreach($arTD as $tdKey=>$tdVal):?>
                <th class="col_<?=$tdKey?>">
                    <span class="<?=$key==0?'source-name':'source-val'?>"><?=$tdVal?></span>
                </th>
            <?endforeach;?>
            </tr>
        <?endforeach;?>
        </thead>
        <tbody>
        <?$hiddenLine = false;
        foreach($arLeadsConvert as $key=>$lineText):
            if($key<2)
                continue;
            $arTD = explode('|', $lineText);
            $trClass = array_pop($arTD);
            if($trClass=='hidden')
                $hiddenLine = true;?>
            <tr class="<?=$trClass;?>">
                <?
                foreach($arTD as $tdKey=>$tdVal):?>
                    <td class="col_<?=$tdKey?>">
                        <?=$tdVal?>
                    </td>
                <?endforeach;?>
            </tr>
        <?endforeach;?>
        </tbody>
    </table>
    <?if($hiddenLine):?>
        <span id="show_all_source" class="show">Показать источники c количеством лидов = 0 ▼</span>
    <?endif;?>
</div>
<script type="text/javascript">
    $(function(){
       $('span#show_all_source').click(function(){
           if($(this).hasClass('show')){
               $("table#leads_convert_table tr.hidden").slideDown();
               $(this).removeClass().addClass('hidden');
               $(this).text('Скрыть источники c количеством лидов = 0 ▲');
           } else {
               $("table#leads_convert_table tr.hidden").slideUp();
               $(this).removeClass().addClass('show');
               $(this).text('Показать источники c количеством лидов = 0 ▼');
           }
       });
    });
</script>
<script type="text/javascript">
	BX.CrmChannelTracker.messages =
	{
		minimize: "<?=GetMessageJS('CRM_CH_TRACKER_MINIMIZE')?>",
		maximize: "<?=GetMessageJS('CRM_CH_TRACKER_MAXIMIZE')?>",
		helpTitle: "<?=GetMessageJS('CRM_CH_TRACKER_HELP_POPUP_TITLE')?>",
		helpContent: "<?=GetMessageJS('CRM_CH_TRACKER_HELP_POPUP_CONTENT')?>"
	};

	BX.CrmChannelTracker.create(
		"<?=CUtil::JSEscape($guid)?>",
		{
			config: <?=CUtil::PhpToJSObject($config)?>,
			containerId: "<?=CUtil::JSEscape($containerID)?>",
			toggleButtonId: "<?=CUtil::JSEscape($toggleButtonID)?>",
			helpButtonId: "<?=CUtil::JSEscape($helpButtonID)?>",
			serviceUrl: "<?='/bitrix/components/bitrix/crm.channel_tracker/settings.php?'.bitrix_sessid_get()?>",
		}
	);

</script><?
$currentUserID = CCrmSecurityHelper::GetCurrentUserID();
$isSupervisor = CCrmPerms::IsAdmin($currentUserID)
	|| Bitrix\Crm\Integration\IntranetManager::isSupervisor($currentUserID);

$rowData = array(
	array(
		'height' => 380,
		'cells' => array(
			array(
				'controls' => array(
					array(
						'entityTypeName' => CCrmOwnerType::ActivityName,
						'typeName' => 'pie',
						'title' => GetMessage('CRM_CH_TRACKER_WGT_ACTIVITY_QUANTITY'),
						'group' => 'CHANNEL',
						'configs' => array(
							array(
								'name' => 'activity_qty',
								'dataPreset' => 'ACTIVITY_CHANNEL_STATS::OVERALL_COUNT',
								'dataSource' => 'ACTIVITY_CHANNEL_STATS',
								'select' => array('name' => 'COUNT', 'aggregate' => 'COUNT')
							)
						)
					)
				)
			),
			array(
				'controls' => array(
					array(
						'entityTypeName' => CCrmOwnerType::DealName,
						'typeName' => 'number',
						'configs' => array(
							array(
								'name' => 'deal_success',
								'title' => GetMessage('CRM_CH_TRACKER_WGT_DEAL_SUCCESS_SUM'),
								'dataPreset' => 'DEAL_SUM_STATS::OVERALL_SUM',
								'dataSource' => 'DEAL_SUM_STATS',
								'select' => array('name' => 'SUM_TOTAL', 'aggregate' => 'SUM'),
								'filter' => array('semanticID' => 'S'),
								'display' => array('colorScheme' => 'green'),
								'format' => array('isCurrency' => 'Y', 'enableDecimals' => 'N')
							)
						)
					),
					array(
						'entityTypeName' => CCrmOwnerType::DealName,
						'typeName' => 'number',
						'format' => array('isCurrency' => 'Y'),
						'configs' => array(
							array(
								'name' => 'deal_process',
								'title' => GetMessage('CRM_CH_TRACKER_WGT_DEAL_PROCESS_SUM'),
								'dataPreset' => 'DEAL_SUM_STATS::OVERALL_SUM',
								'dataSource' => 'DEAL_SUM_STATS',
								'select' => array('name' => 'SUM_TOTAL', 'aggregate' => 'SUM'),
								'filter' => array('semanticID' => 'P'),
								'display' => array('colorScheme' => 'blue'),
								'format' => array('isCurrency' => 'Y', 'enableDecimals' => 'N')
							)
						)
					)
				)
			)
		)
	),
	array(
		'height' => 380,
		'cells' => array(
			array(
				'controls' => array(
					array(
						'entityTypeName' => CCrmOwnerType::DealName,
						'typeName' => 'graph',
						'title' => GetMessage('CRM_CH_TRACKER_WGT_AMOUNT_OF_SALE'),
						'group' => 'DATE',
						'context' => 'F',
						'combineData' => 'Y',
						'format' => array('isCurrency' => 'Y'),
						'configs' => array(
							array(
								'name' => 'deal_success',
								'title' => GetMessage('CRM_CH_TRACKER_WGT_DEAL_SUCCESS_SUM'),
								'dataPreset' => 'DEAL_SUM_STATS::OVERALL_SUM',
								'dataSource' => 'DEAL_SUM_STATS',
								'select' => array('name' => 'SUM_TOTAL', 'aggregate' => 'SUM'),
								'filter' => array('semanticID' => 'S')
							)
						)
					)
				)
			)
		)
	)
);

if($isSupervisor)
{
	$rowData[] = array(
		'height' => 380,
		'cells' => array(
			array(
				'controls' => array(
					array(
						'entityTypeName' => CCrmOwnerType::DealName,
						'typeName' => 'bar',
						'title' => GetMessage('CRM_CH_TRACKER_WGT_DEAL_IN_WORK_BY_EMPLOYEE'),
						'group' => 'USER',
						'context' => 'F',
						'combineData' => 'Y',
						'enableStack' => 'N',
						'format' => array('isCurrency' => 'Y'),
						'configs' => array(
							array(
								'name' => 'deal_process',
								'title' => GetMessage('CRM_CH_TRACKER_WGT_DEAL_PROCESS_SUM'),
								'dataPreset' => 'DEAL_SUM_STATS::OVERALL_SUM',
								'dataSource' => 'DEAL_SUM_STATS',
								'select' => array('name' => 'SUM_TOTAL', 'aggregate' => 'SUM'),
								'filter' =>  array('semanticID' => 'P'),
								'display' => array('colorScheme' => 'blue')
							),
							array(
								'name' => 'deal_success',
								'title' => GetMessage('CRM_CH_TRACKER_WGT_DEAL_SUCCESS_SUM'),
								'dataPreset' => 'DEAL_SUM_STATS::OVERALL_SUM',
								'dataSource' => 'DEAL_SUM_STATS',
								'select' => array('name' => 'SUM_TOTAL', 'aggregate' => 'SUM'),
								'filter' => array('semanticID' => 'S'),
								'display' => array('colorScheme' => 'green')
							)
						)
					)
				)
			)
		)
	);
}
else
{
	$rowData[] = array(
		'height' => 380,
		'cells' => array(
			array(
				'controls' => array(
					array(
						'entityTypeName' => CCrmOwnerType::DealName,
						'typeName' => 'rating',
						'title' => GetMessage('CRM_CH_TRACKER_WGT_RATING_BY_SUCCESSFUL_DEALS'),
						'group' => 'USER',
						'nominee' => $currentUserID,
						'configs' => array(
							array(
								'name' => 'deal_success',
								'dataPreset' => 'DEAL_SUM_STATS::OVERALL_SUM',
								'dataSource' => 'DEAL_SUM_STATS',
								'select' => array('name' => 'SUM_TOTAL', 'aggregate' => 'SUM'),
								'filter' => array('semanticID' => 'S'),
								'format' => array('isCurrency' => 'Y', 'enableDecimals' => 'N')
							),
						)
					)
				)
			)
		)
	);
}

?><div class="bx-crm-view"><?
	$APPLICATION->IncludeComponent(
		'bitrix:crm.widget_panel',
		'',
		array(
			'GUID' => $arResult['WIDGET_GUID'],
			'LAYOUT' => 'L50R50',
			'ENABLE_NAVIGATION' => true,
			'ENTITY_TYPES' => array(
				CCrmOwnerType::ActivityName,
				CCrmOwnerType::LeadName,
				CCrmOwnerType::DealName,
				CCrmOwnerType::ContactName,
				CCrmOwnerType::CompanyName,
				CCrmOwnerType::InvoiceName
			),
			'DEFAULT_ENTITY_TYPE' => CCrmOwnerType::ActivityName,
			'PATH_TO_WIDGET' => isset($arResult['PATH_TO_LEAD_WIDGET']) ? $arResult['PATH_TO_LEAD_WIDGET'] : '',
			'PATH_TO_LIST' => isset($arResult['PATH_TO_LEAD_LIST']) ? $arResult['PATH_TO_LEAD_LIST'] : '',
			'PATH_TO_DEMO_DATA' => $_SERVER['DOCUMENT_ROOT'].'/bitrix/components/bitrix/crm.channel_tracker/templates/.default/widget',
			'IS_SUPERVISOR' => $isSupervisor,
			'ROWS' => $rowData,
			'DEMO_TITLE' => GetMessage('CRM_CH_TRACKER_WGT_DEMO_TITLE'),
			'DEMO_CONTENT' => '',
			'RENDER_HEAD_INTO_VIEW' => 'widget_panel_header',
		)
	);
?></div>
