<?
$periodType = 'D30';
if(isset($_REQUEST["PERIOD"])&&$_REQUEST["PERIOD"]!=''){
    $periodType = $_REQUEST["PERIOD"];

    if ("LN" == $_REQUEST["PERIOD"])
    {
        $_REQUEST["BEGINDATE_days"] = (int)$_REQUEST["BEGINDATE_days"];
        if ($_REQUEST["BEGINDATE_days"])
            $periodType = $_REQUEST["PERIOD"] . "-" . $_REQUEST["BEGINDATE_days"];
    }
    elseif ("SD" == $_REQUEST["PERIOD"])
    {
        $_REQUEST["BEGINDATE_since"] = filter_var($_REQUEST["BEGINDATE_since"], FILTER_SANITIZE_NUMBER_INT);
        if ($_REQUEST["BEGINDATE_since"])
            $periodType = $_REQUEST["PERIOD"] . "-" . $_REQUEST["BEGINDATE_since"];
    }
    elseif ("PD" == $_REQUEST["PERIOD"])
    {
        $_REQUEST["BEGINDATE_from"] = filter_var($_REQUEST["BEGINDATE_from"], FILTER_SANITIZE_NUMBER_INT);
        $_REQUEST["BEGINDATE_to"] = filter_var($_REQUEST["BEGINDATE_to"], FILTER_SANITIZE_NUMBER_INT);

        if(!$_REQUEST["BEGINDATE_from"])
            $_REQUEST["BEGINDATE_from"] = 0;
        if(!$_REQUEST["BEGINDATE_to"])
            $_REQUEST["BEGINDATE_to"] = 0;

        $periodType = $_REQUEST["PERIOD"] . "-" . $_REQUEST["BEGINDATE_from"] . "-" . $_REQUEST["BEGINDATE_to"];
    }
} else {
    if(isset($_SESSION["main.interface.grid"]["start_widget"]["filter"]["PERIOD"])&&$_SESSION["main.interface.grid"]["start_widget"]["filter"]["PERIOD"]!=''){
        if($_REQUEST["clear_filter"]!='Y'){
            $periodType = $_SESSION["main.interface.grid"]["start_widget"]["filter"]["PERIOD"];
        }
    }
}

if(!empty($periodType)){
    $chanelInfo = new controlSalesCompanyByLead($periodType);
    $arResult["arLeadsConvert"] = $chanelInfo->getElemData();
}

?>