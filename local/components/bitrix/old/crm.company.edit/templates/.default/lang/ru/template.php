<?
$MESS["CRM_TAB_1"] = "Контрагент";
$MESS["CRM_TAB_1_TITLE"] = "Свойства контрагента";
$MESS["CRM_TAB_2"] = "События";
$MESS["CRM_TAB_2_TITLE"] = "События контрагента";
$MESS["CRM_TAB_3"] = "Бизнес-процессы";
$MESS["CRM_TAB_3_TITLE"] = "Бизнес-процессы контрагента";
$MESS["CRM_COMPANY_EDIT_DUP_CTRL_SUMMARY_TITLE"] = "Найденные совпадения";
$MESS["CRM_COMPANY_EDIT_DUP_CTRL_SHORT_SUMMARY_TITLE"] = "найдено";
$MESS["CRM_COMPANY_EDIT_DUP_CTRL_WARNING_DLG_TITLE"] = "Подозрение на дубликаты";
$MESS["CRM_COMPANY_EDIT_DUP_CTRL_WARNING_ACCEPT_BTN_TITLE"] = "Игнорировать и сохранить";
$MESS["CRM_COMPANY_EDIT_DUP_CTRL_WARNING_CANCEL_BTN_TITLE"] = "Отменить";
$MESS["CRM_COMPANY_EDIT_DUP_CTRL_TTL_SUMMARY_TITLE"] = "по названию";
$MESS["CRM_COMPANY_EDIT_DUP_CTRL_PHONE_SUMMARY_TITLE"] = "по телефону";
$MESS["CRM_COMPANY_EDIT_DUP_CTRL_EMAIL_SUMMARY_TITLE"] = "по e-mail";
$MESS["CRM_COMPANY_EDIT_TITLE"] = "Контрагент №#ID# &mdash; #TITLE#";
$MESS["CRM_COMPANY_CREATE_TITLE"] = "Новый контрагент";
?>