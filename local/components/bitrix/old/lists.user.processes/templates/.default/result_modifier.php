<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

function getCompanyInn($companyID)
{
    $req = new \Bitrix\Crm\EntityRequisite();
    $rs = $req->getList([
        "filter" => [
            "ENTITY_ID" => $companyID,
            "ENTITY_TYPE_ID" => CCrmOwnerType::Company
        ]
    ]);
    if($row = $rs->fetch())
    {
        return $row["RQ_INN"];
    }
    else
    {
        return '';
    }

    //echo '<pre>'.print_r($row, true).'</pre>';

}

if(!empty($arResult["HEADERS"]))
{
    array_push($arResult["HEADERS"],
        [
            "id" => "TYPE_PARTICIPATION",
            "name" => Loc::getMessage("FIELD_TYPE_PARTICIPATION"),
            "default" => true,
            "sort" => "",
        ],
        [
            "id" => "TIME_WASTED",
            "name" => Loc::getMessage("TIME_WASTED"),
            "default" => true,
            "sort" => "",
        ],
        [
            "id" => "CONTRAGENT",
            "name" => Loc::getMessage("FIELD_LEAD"),
            "default" => true,
            "sort" => "",
        ],
        [
            "id" => "INN",
            "name" => Loc::getMessage("FIELD_INN"),
            "default" => true,
            "sort" => "",
        ],
        [
            "id" => "INVOICE_SUMM",
            "name" => Loc::getMessage("FIELD_INVOICE_SUMM"),
            "default" => true,
            "sort" => "",
        ]
    );

    $iblockTypeId = COption::GetOptionString("lists", "livefeed_iblock_type_id");

    // далее будет подготовка фильтра
//    $elementObject = CIBlockElement::getList(
//        $gridSort['sort'],
//        $filter,
//        false,
//        $gridOptions->getNavParams(),
//        $selectFields
//    );
}

if(!empty($arResult["RECORDS"]))
{
    $el = new CIBlockElement();
    $CCrmCompany = new CCrmCompany();
    foreach($arResult["RECORDS"] as $key => $arRecord){
        $arData = $arRecord["data"];
        if($arData["MODULE_ID"]=="lists" && intval($arData["DOCUMENT_ID"])>0){
            $iblockID = $el->GetIBlockByID($arData["DOCUMENT_ID"]);
            $rsElem = $el->GetByID($arData["DOCUMENT_ID"]);
            $arElem = $rsElem->Fetch();
            $startTime = strtotime($arElem["DATE_CREATE"]);			
            if($arData["WORKFLOW_STATE_CODE"] == "Completed" || $arData["WORKFLOW_STATE_CODE"] == "Terminated"){
                $crntTime = strtotime(date("d.m.Y H:i:s", strtotime($arData["WORKFLOW_STATE_MODIFIED"])+CTimeZone::GetOffset()));
            } else {
                $crntTime = strtotime(date("d.m.Y H:i:s", time()+CTimeZone::GetOffset()));
            }
            $diff = $crntTime - $startTime;
            $days = (int)floor($diff/86400);
            if($days > 0){
                $diff = $diff - ($days*86400);
            }
            $hour = (int)floor($diff/3600);
            $min = floor(($diff%3600)/60);
            $sec = $diff%60;
            $zatrachenoText = "$days дн. $hour ч. $min мин. $sec сек.";
            $arData["DATE_CREATE"] = $arElem["DATE_CREATE"];
            $arData["TIME_WASTED"] = $zatrachenoText;
            //Если БП "Согласование"
            if($iblockID == GetIBlockIDByCode('bp_soglasovaniye'))
            {
                //получаем Контрагента
                $db_props = $el->GetProperty($iblockID, $arData["ID"], array("sort" => "asc"), Array("CODE"=>"NAIMENOVANIE_KLIENTA"));
                if($ar_props = $db_props->Fetch())
                {
                    $companyID = $ar_props["VALUE"];
                    $arCompany = $CCrmCompany->GetByID($companyID);
                    $arData["CONTRAGENT"] = '<a href="/crm/company/show/'.$companyID.'/">'.$arCompany["TITLE"].'</a>';
                    $arData["INN"] = getCompanyInn($companyID);
                }
            }

            //Если БП "Согласование 3.0"
            if($iblockID == GetIBlockIDByCode('soglasovaniye_3'))
            {
                //получаем Контрагента
                $db_props = $el->GetProperty($iblockID, $arData["ID"], array("sort" => "asc"), Array("CODE"=>"KONTRAGENT"));
                if($ar_props = $db_props->Fetch())
                {
                    $companyID = $ar_props["VALUE"];
                    $arCompany = $CCrmCompany->GetByID($companyID);
                    $arData["CONTRAGENT"] = '<a href="/crm/company/show/'.$companyID.'/">'.$arCompany["TITLE"].'</a>';
                    $arData["INN"] = getCompanyInn($companyID);
                }
                //Получаем Стоимость договора
                $db_props = $el->GetProperty($iblockID, $arData["ID"], array("sort" => "asc"), Array("CODE"=>"STOIMOST_DOGOVORA"));
                if($ar_props = $db_props->Fetch())
                {
                    $arData["INVOICE_SUMM"] = CurrencyFormat($ar_props["VALUE"], "RUB");
                }
            }

            //Если БП "Заявка на оплату счетов"
            if($iblockID == GetIBlockIDByCode('application_payment_invoices'))
            {
                //получаем Контрагента
                $db_props = $el->GetProperty($iblockID, $arData["ID"], array("sort" => "asc"), Array("CODE"=>"KONTRAGENT"));
                if($ar_props = $db_props->Fetch()){
                    $companyID = $ar_props["VALUE"];
                    $arCompany = $CCrmCompany->GetByID($companyID);
                    $arData["CONTRAGENT"] = '<a href="/crm/company/show/'.$companyID.'/">'.$arCompany["TITLE"].'</a>';
                    $arData["INN"] = getCompanyInn($ar_props["VALUE"]);
                }

                //получаем Сумму счета
                $db_props = $el->GetProperty($iblockID, $arData["ID"], array("sort" => "asc"), Array("CODE"=>"SUMMA_SCHETA"));
                if($ar_props = $db_props->Fetch())
                    $arData["INVOICE_SUMM"] = CurrencyFormat($ar_props["VALUE"], "RUB");

            }
            $arData["IBLOCK_ID"] = $iblockID;
        }
        $arRecord["data"] = $arData;
        $arResult["RECORDS"][$key] = $arRecord;
    }
}
//echo '<pre>$arResult["RECORDS"] = '.print_r($arResult["RECORDS"], true).'</pre>';
?>