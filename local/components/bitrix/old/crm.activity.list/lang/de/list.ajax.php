<?
$MESS["CRM_ACTIVITY_LIST_ROW_COUNT"] = "Gesamt: #ROW_COUNT#";
$MESS["CRM_ACTIVITY_LIST_REBUILD_ACT_STATISTICS_NOT_REQUIRED_SUMMARY"] = "Aktivitätsstatistik muss nicht aktualisiert werden.";
$MESS["CRM_ACTIVITY_LIST_REBUILD_ACT_STATISTICS_PROGRESS_SUMMARY"] = "Aktivitäten verarbeitet: #PROCESSED_ITEMS# von #TOTAL_ITEMS#.";
$MESS["CRM_ACTIVITY_LIST_REBUILD_ACT_STATISTICS_COMPLETED_SUMMARY"] = "Verarbeitung von statistischen Daten für Aktivitäten wurde abgeschlossen. Aktivitäten verarbeitet: #PROCESSED_ITEMS#.";
?>