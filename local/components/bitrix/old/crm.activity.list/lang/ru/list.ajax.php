<?
$MESS["CRM_ACTIVITY_LIST_ROW_COUNT"] = "Всего: #ROW_COUNT#";
$MESS["CRM_ACTIVITY_LIST_REBUILD_ACT_STATISTICS_NOT_REQUIRED_SUMMARY"] = "Обновление статистических данных для дел не требуется.";
$MESS["CRM_ACTIVITY_LIST_REBUILD_ACT_STATISTICS_PROGRESS_SUMMARY"] = "Обработано дел: #PROCESSED_ITEMS# из #TOTAL_ITEMS#.";
$MESS["CRM_ACTIVITY_LIST_REBUILD_ACT_STATISTICS_COMPLETED_SUMMARY"] = "Обновление статистических данных для дел завершено. Обработано дел: #PROCESSED_ITEMS#.";
?>