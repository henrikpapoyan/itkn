<?php
/**
 * User: German
 * Date: 04.08.2017
 * Time: 9:31
 */

if (!CModule::IncludeModule('sale'))
    return;

class CCrmInvoiceITK extends CCrmInvoice
{

    public function CheckFields(&$arFields, $ID = false, $bStatusSuccess = true, $bStatusFailed = true)
    {
        /** @global CUserTypeManager $USER_FIELD_MANAGER */
        /** @global CMain $APPLICATION */
        global $APPLICATION, $USER_FIELD_MANAGER;

        $this->LAST_ERROR = '';

        $bTaxMode = CCrmTax::isTaxMode();

        if (!isset($arFields['PRODUCT_ROWS']) || !is_array($arFields['PRODUCT_ROWS']) || count($arFields['PRODUCT_ROWS']) === 0)
        {
            $this->LAST_ERROR .= GetMessage('CRM_ERROR_EMPTY_INVOICE_SPEC')."<br />\n";
        }
        else
        {
            $invalidQuantityExists = false;
            foreach ($arFields['PRODUCT_ROWS'] as $productRow)
            {
                if (!isset($productRow['QUANTITY']) || round(doubleval($productRow['QUANTITY']), 4) <= 0.0)
                {
                    $invalidQuantityExists = true;
                    break;
                }
            }
            unset($productRow);

            if ($invalidQuantityExists)
                $this->LAST_ERROR .= GetMessage('CRM_ERROR_INVOICE_SPEC_INVALID_QUANTITY')."<br />\n";

            unset($invalidQuantityExists);
        }

        if ($ID !== false && isset($arFields['ACCOUNT_NUMBER']))
        {
            if (strlen($arFields['ACCOUNT_NUMBER']) <= 0)
                $this->LAST_ERROR .= GetMessage('CRM_ERROR_FIELD_IS_MISSING', array('%FIELD_NAME%' => GetMessage('CRM_FIELD_ACCOUNT_NUMBER')))."<br />\n";
        }

        if (($ID == false || isset($arFields['ORDER_TOPIC'])) && strlen($arFields['ORDER_TOPIC']) <= 0)
            $this->LAST_ERROR .= GetMessage('CRM_ERROR_FIELD_IS_MISSING', array('%FIELD_NAME%' => GetMessage('CRM_FIELD_ORDER_TOPIC')))."<br />\n";

        if (($ID == false || isset($arFields['DATE_PAY_BEFORE'])) && strlen($arFields['DATE_PAY_BEFORE']) <= 0) //GK
        {
            $this->LAST_ERROR .= GetMessage('CRM_ERROR_FIELD_IS_MISSING', array('%FIELD_NAME%' => 'Срок оплаты'))."<br />\n";
        }

        if (!empty($arFields['ORDER_TOPIC']) && strlen($arFields['ORDER_TOPIC']) > 255)
            $this->LAST_ERROR .= GetMessage('CRM_ERROR_FIELD_INCORRECT', array('%FIELD_NAME%' => GetMessage('CRM_FIELD_ORDER_TOPIC')))."<br />\n";

        if (!empty($arFields['COMMENTS']) && strlen($arFields['COMMENTS']) > 2000)
            $this->LAST_ERROR .= GetMessage('CRM_ERROR_FIELD_INCORRECT', array('%FIELD_NAME%' => GetMessage('CRM_FIELD_COMMENTS')))
                .' ('.GetMessage('CRM_FIELD_COMMENTS_INCORRECT_INFO').").<br />\n";

        if (!empty($arFields['USER_DESCRIPTION']) && strlen($arFields['USER_DESCRIPTION']) > 2000)
            $this->LAST_ERROR .= GetMessage('CRM_ERROR_FIELD_INCORRECT', array('%FIELD_NAME%' => GetMessage('CRM_FIELD_USER_DESCRIPTION')))
                .' ('.GetMessage('CRM_FIELD_USER_DESCRIPTION_INCORRECT_INFO').").<br />\n";

        if (empty($arFields['STATUS_ID']) || strlen($arFields['STATUS_ID']) !== 1)
            $this->LAST_ERROR .= GetMessage('CRM_ERROR_FIELD_INCORRECT', array('%FIELD_NAME%' => GetMessage('CRM_FIELD_STATUS_ID')))."<br />\n";

        if ($bStatusSuccess)
        {
            if (!empty($arFields['PAY_VOUCHER_NUM']) && strlen($arFields['PAY_VOUCHER_NUM']) > 20)
                $this->LAST_ERROR .= GetMessage('CRM_ERROR_FIELD_INCORRECT', array('%FIELD_NAME%' => GetMessage('CRM_FIELD_PAY_VOUCHER_NUM')))."<br />\n";
            if (!empty($arFields['PAY_VOUCHER_DATE']) && !CheckDateTime($arFields['PAY_VOUCHER_DATE']))
                $this->LAST_ERROR .= GetMessage('CRM_ERROR_FIELD_INCORRECT', array('%FIELD_NAME%' => GetMessage('CRM_FIELD_PAY_VOUCHER_DATE')))."<br />\n";
            if (!empty($arFields['REASON_MARKED']) && strlen($arFields['REASON_MARKED']) > 255)
                $this->LAST_ERROR .= GetMessage('CRM_ERROR_FIELD_INCORRECT', array('%FIELD_NAME%' => GetMessage('CRM_FIELD_REASON_MARKED_SUCCESS')))."<br />\n";
        }
        elseif ($bStatusFailed)
        {
            if (!empty($arFields['DATE_MARKED']) && !CheckDateTime($arFields['DATE_MARKED']))
                $this->LAST_ERROR .= GetMessage('CRM_ERROR_FIELD_INCORRECT', array('%FIELD_NAME%' => GetMessage('CRM_FIELD_DATE_MARKED')))."<br />\n";
            if (!empty($arFields['REASON_MARKED']) && strlen($arFields['REASON_MARKED']) > 255)
                $this->LAST_ERROR .= GetMessage('CRM_ERROR_FIELD_INCORRECT', array('%FIELD_NAME%' => GetMessage('CRM_FIELD_REASON_MARKED')))."<br />\n";
        }

        if (!isset($arFields['PERSON_TYPE_ID']) || intval($arFields['PERSON_TYPE_ID']) <= 0
            || (intval($arFields['UF_COMPANY_ID']) <= 0 && intval($arFields['UF_CONTACT_ID']) <= 0))
            $this->LAST_ERROR .= GetMessage('CRM_ERROR_PAYER_IS_MISSING')."<br />\n";

        if ($bTaxMode)
        {
            if (!isset($arFields['PR_LOCATION']) || intval($arFields['PR_LOCATION']) <= 0)
                $this->LAST_ERROR .= GetMessage('CRM_ERROR_FIELD_IS_MISSING', array('%FIELD_NAME%' => GetMessage('CRM_FIELD_PR_LOCATION')))."<br />\n";
        }

        if (!isset($arFields['PAY_SYSTEM_ID']) || intval($arFields['PAY_SYSTEM_ID']) <= 0)
            $this->LAST_ERROR .= GetMessage('CRM_ERROR_FIELD_IS_MISSING', array('%FIELD_NAME%' => GetMessage('CRM_FIELD_PAY_SYSTEM_ID')))."<br />\n";

        if (!empty($arFields['DATE_INSERT']) && !CheckDateTime($arFields['DATE_INSERT']))
            $this->LAST_ERROR .= GetMessage('CRM_ERROR_FIELD_INCORRECT', array('%FIELD_NAME%' => GetMessage('CRM_FIELD_DATE_INSERT')))."<br />\n";

        if (!empty($arFields['DATE_BILL']) && !CheckDateTime($arFields['DATE_BILL']))
            $this->LAST_ERROR .= GetMessage('CRM_ERROR_FIELD_INCORRECT', array('%FIELD_NAME%' => GetMessage('CRM_FIELD_DATE_BILL')))."<br />\n";

        if (!empty($arFields['DATE_PAY_BEFORE']) && !CheckDateTime($arFields['DATE_PAY_BEFORE']))
            $this->LAST_ERROR .= GetMessage('CRM_ERROR_FIELD_INCORRECT', array('%FIELD_NAME%' => GetMessage('CRM_FIELD_DATE_PAY_BEFORE')))."<br />\n";

        CCrmEntityHelper::NormalizeUserFields($arFields, self::$sUFEntityID, $USER_FIELD_MANAGER, array('IS_NEW' => ($ID == false)));
        if(!$USER_FIELD_MANAGER->CheckFields(self::$sUFEntityID, $ID, $arFields))
        {
            $e = $APPLICATION->GetException();
            $this->LAST_ERROR .= $e->GetString();
        }

        if (strlen($this->LAST_ERROR) > 0)
            return false;

        return true;
    }

}