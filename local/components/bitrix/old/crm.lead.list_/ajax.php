<?php
/**
 * User: German
 * Date: 23.11.2017
 * Time: 16:59
 */
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("crm");
if($_REQUEST['c'] == 'get')
{
    $rsLead = CCrmLead::GetList(array(),array('ID' => $_REQUEST['entityId']),array('UF_AUTO', 'UF_KPP', 'UF_CRM_1499414186'));
    $arResult = array();
    if ($arLead = $rsLead->Fetch())
    {
        $arResult = array('ufAuto' => $arLead['UF_AUTO'],
            'inn'=> $arLead['UF_CRM_1499414186'],
            'kpp'=> $arLead['UF_KPP'],
            'id' => $arLead['ID']);
    }
    echo json_encode($arResult);
    return;
}elseif($_REQUEST['c'] == 'set')
{
    $lead_id = $_REQUEST['lead_id'];
    $CCrmLead = new CCrmLead();
    $arFields = array(
        'UF_CRM_1499414186'=> $_REQUEST['inn'],
        'UF_KPP'=> $_REQUEST['kpp'],
    );
    $ret = $CCrmLead->Update($lead_id, $arFields);
    $arResult=array('ret'=> $ret);
    return json_encode($arResult);
}
else
{
    echo json_encode(array());
}
