<?php
/**
 * User: German
 * Date: 27.01.2017
 * Time: 16:53
 */

foreach ($arResult['FIELDS']['tab_1'] as $key => $val) {
	if($val['id'] == 'UF_EVENT')
	{
		$arFilter = array('ID' => $arResult['ELEMENT']['ID']);
		$rsDeal = CCrmDeal::GetList(array(), $arFilter, array("UF_EVENT"));
		$arDeal = $rsDeal->Fetch();
		$IBLOCK_ID = 31;
		$stage_id = $arResult['ELEMENT']['STAGE_ID'];
		$UF_EVENT = $arDeal['UF_EVENT'];
		if (empty($stage_id))
		{
			$stage_id = 'NEW';
			$UF_EVENT = 151;
		}
		$arFilter = array(
			"IBLOCK_ID" => $IBLOCK_ID,
			"SECTION_CODE" => "SECTION_".$stage_id,
		);
		$rsEvent = CIBlockElement::GetList(array("SORT" =>"ASC") , $arFilter);
		$event = '<select name="UF_EVENT" size="5"  >
					<option value=""  title="нет">нет</option>';
		while ($arEvent = $rsEvent->Fetch())
		{
			$selected = "";
			if($arEvent['ID'] == $UF_EVENT)
			{
				$selected = 'selected ';
			}
			$event .='<option '.$selected.'value="' . $arEvent['ID'] . '"  title="'.
				$arEvent['NAME'] . '">' .
				$arEvent['NAME'] . "</option>\n";

		}
		$event .='</select>';
		$arResult['FIELDS']['tab_1'][$key]['value'] = $event;
	}
	if ( $val['id'] == "section_product_rows")
	{
		$section_product_rows = $key;
	}
	if ( $val['id'] == "PRODUCT_ROWS")
	{
		$PRODUCT_ROWS = $key;
	}
}
unset($arResult['FIELDS']['tab_1'][$PRODUCT_ROWS]);
unset($arResult['FIELDS']['tab_1'][$section_product_rows]);

