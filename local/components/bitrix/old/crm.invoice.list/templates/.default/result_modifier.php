<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

// удаляем поле тема
foreach ($arResult['FILTER'] as $keyFilter => $valueFilter)
{
    if($valueFilter["id"] == "ORDER_TOPIC")
        unset($arResult['FILTER'][$keyFilter]);
}

// добавляем поля в фильтр
$arResult['FILTER'][] = ['id' => 'DATE_BILL', 'name' => GetMessage('FILTER_FIELD_DATE_BILL'), 'type' => 'date'];
$arResult['FILTER'][] = ['id' => 'COMMENTS', 'name' => GetMessage('FILTER_FIELD_COMMENTS')];
$arResult['FILTER'][] = ['id' => 'TAX_VALUE', 'name' => GetMessage('FILTER_FIELD_TAX')];