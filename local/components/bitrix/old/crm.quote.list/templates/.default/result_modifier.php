<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

// добавляем данные по полю Ответственный
$arQuoteSelect = ['ASSIGNED_BY','ASSIGNED_BY_NAME','ASSIGNED_BY_LAST_NAME'];
$arQuoteFilter = [];
$quoteDb = CCrmQuote::GetList(
    ['DATE_CREATE' => 'DESC'],
    $arQuoteFilter,
    $arQuoteSelect
);

$arAssignedUsers = [];
while($ar = $quoteDb->Fetch()){
    if (!array_key_exists($ar['ASSIGNED_BY'], $arAssignedUsers)){
        $arAssignedUsers[$ar['ASSIGNED_BY']] = $ar['ASSIGNED_BY_LAST_NAME'].' '.$ar['ASSIGNED_BY_NAME'];
    }
}
asort($arAssignedUsers);

// добавляем поля в фильтр
$arResult['FILTER'][] = ['id' => 'STATUS_ID', 'name' => GetMessage('FILTER_FIELD_STATUS_ID'), 'params' => array('multiple' => 'Y'), 'default' => true, 'type' => 'list', 'items' => CCrmStatus::GetStatusList('QUOTE_STATUS')];
$arResult['FILTER'][] = ['id' => 'SUM', 'name' => GetMessage('FILTER_FIELD_SUM')];
$arResult['FILTER'][] = ['id' => 'ENTITIES_LINKS', 'name' => GetMessage('FILTER_FIELD_ENTITIES_LINKS')];
$arResult['FILTER'][] = ['id' => 'CLOSEDATE', 'name' => GetMessage('FILTER_FIELD_CLOSEDATE'), 'type' => 'date'];
$arResult['FILTER'][] = ['id' => 'ASSIGNED_BY_ID', 'name' => GetMessage('FILTER_FIELD_ASSIGNED_BY'), 'default' => true, 'type' => 'list', 'params' =>  array('multiple' => 'Y'), 'items' => $arAssignedUsers,
    "selector" => ['TYPE' => 'user', 'DATA' => ['ID' => 'assigned_by', 'FIELD_ID' => 'ASSIGNED_BY']]];
$arResult['FILTER'][] = ['id' => 'DATE_CREATE', 'name' => GetMessage('FILTER_FIELD_DATE_CREATE'), 'type' => 'date'];