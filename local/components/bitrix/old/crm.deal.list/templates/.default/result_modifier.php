<?
/**
 * User: German
 * Date: 08.02.2017
 * Time: 15:53
 */
use Bitrix\Main\Loader;
Loader::includeModule("tasks");
foreach ($arResult['DEAL_UF'] as $deal_id => $ufdata) {
    $UF_GROUP = $arResult['DEAL'][$deal_id]['UF_GROUP'];
    $UF_DOCUMENTS = "";
    if ($UF_GROUP > 0)
    {
        $rsTask = CTasks::GetList(array("ID" => "ASC"), array('GROUP_ID' => $UF_GROUP));


        while ($taskData = $rsTask->Fetch())
        {
            $taskData["FILES_IN_COMMENTS"] = 0;
            if ($taskData["FORUM_ID"] > 0 && $taskData["FORUM_TOPIC_ID"] > 0)
            {
                $taskData["FILES_IN_COMMENTS"] = \Bitrix\Tasks\Integration\Forum\Task\Topic::getFileCount($taskData["FORUM_TOPIC_ID"], $taskData["FORUM_ID"]);
            }
            if ($taskData["FILES_IN_COMMENTS"] >0)
            {
                ob_start();

                $APPLICATION->IncludeComponent(
                    "bitrix:disk.uf.comments.attached.objects",
                    "itkn",
                    array(
                        "MAIN_ENTITY" => array(
                            "ID" => $taskData["ID"]
                        ),
                        "COMMENTS_MODE" => "forum",
                        "ENABLE_AUTO_BINDING_VIEWER" => false, // Viewer cannot work in the iframe (see logic.js)
                        "DISABLE_LOCAL_EDIT" => false,//$arParams["PUBLIC_MODE"],
                        "COMMENTS_DATA" => array(
                            "TOPIC_ID" => $taskData["FORUM_TOPIC_ID"],
                            "FORUM_ID" => $taskData["FORUM_ID"],
                            "XML_ID" => "TASK_".$taskData["ID"]
                        ),
                        "PUBLIC_MODE" => false,//$arParams["PUBLIC_MODE"]
                    ),
                    false,
                    array("HIDE_ICONS" => "Y", "ACTIVE_COMPONENT" => "Y"));
                $UF_DOCUMENTS .= ob_get_contents();
                ob_end_clean();
            }
        }
    }

    $arResult['DEAL_UF'][$deal_id]['UF_DOCUMENTS'] = $UF_DOCUMENTS;
}
