<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

$requiredModules = array('crm', 'report', 'intranet', 'socialnetwork');

foreach ($requiredModules as $requiredModule)
{
	if (!CModule::IncludeModule($requiredModule))
	{
		ShowError(strtoupper($requiredModule).'_MODULE_NOT_INSTALLED');
		return 0;
	}
}

if(!CCrmCurrency::EnsureReady())
{
	ShowError(CCrmCurrency::GetLastError());
}

if($_SERVER['REQUEST_METHOD'] === 'POST' && check_bitrix_sessid())
{
	global $APPLICATION;
	$reportCurrencyID = isset($_POST['crmReportCurrencyID']) ? $_POST['crmReportCurrencyID'] : '';
	if(isset($reportCurrencyID[0]))
	{
		CCrmReportHelper::SetReportCurrencyID($reportCurrencyID);
		LocalRedirect($APPLICATION->GetCurPage());
	}
}

$reportID = $arResult['REPORT_ID'] = isset($arParams['REPORT_ID']) ? intval($arParams['REPORT_ID']) : 0;
$reportData = $arResult['REPORT_DATA'] = CCrmReportManager::getReportData($reportID);
$reportOwnerID = $arResult['REPORT_OWNER_ID'] = $reportData && isset($reportData['OWNER_ID']) ? $reportData['OWNER_ID'] : '';
$arResult['REPORT_HELPER_CLASS'] = $reportOwnerID !== '' ? CCrmReportManagerCustom::getOwnerHelperClassName($reportOwnerID) : '';
$arResult['NAME_TEMPLATE'] = empty($arParams['NAME_TEMPLATE']) ? CSite::GetNameFormat(false) : str_replace(array("#NOBR#","#/NOBR#"), array("",""), $arParams["NAME_TEMPLATE"]);
$arString = unserialize($arResult['REPORT_DATA']['SETTINGS']);
$this->IncludeComponentTemplate();

$arTemp = Array(
    "entity" => "Bitrix\Crm\Invoice",
    "period" => Array (
        "type" => "month",
        "value" => "",
    ),
    "select" => Array (
        Array(
            "name" => "ASSIGNED_BY.SHORT_NAME",
            "alias" => "",
        ),
        Array(
            "name" => "IS_PAYED",
            "alias" => "",
            "aggr" => "SUM",
        ),
        Array(
            "name" => "PRICE_PAYED",
            "alias" => "",
            "aggr" => "SUM",
        ),
        Array(
            "name" => "IS_CANCELED",
            "alias" => "",
            "aggr" => "SUM",
        ),
        Array(
            "name" => "PRICE_CANCELED",
            "alias" => "",
            "aggr" => "SUM",
        ),
    ),
    "filter" => Array (
        Array(
            Array(
                "type" => "field",
                "name" => "ASSIGNED_BY",
                "compare" => "EQUAL",
                "value" => "",
                "changeable" => "1",
            ),
            "LOGIC" => "AND",
        )
    ),
    "sort" => "6",
    "sort_type" => "DESC",
    "limit" => "",
    "red_neg_vals" => "",
    "grouping_mode" => "",
    "chart" => Array(
        "display" => "1",
        "type" => "bar",
        "x_column" => "4",
        "y_columns" => Array(
            "0" => "6"
        ),
    )
);
//echo 'local/components/bitrix/crm.report.view/component.php $arStringTemp = <pre>'.print_r($arResult['viewColumns'], true).'</pre>';
//$stringTemp = serialize($arTemp);
//echo 'local/components/bitrix/crm.report.view/component.php $stringTemp = '.$stringTemp.'<br />';
?>