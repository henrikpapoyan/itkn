<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

// добавляем поля в фильтр
$arResult['FILTER'][] = ['id' => 'PROPERTY_498', 'name' => GetMessage('FILTER_FIELD_TEXT_MESS')];

    $arSpoiledFields = [
        ["section" => "data", "fieldName" => "NAME"],
        ["section" => "columns", "fieldName" => "NAME"],
        ["section" => "columns", "fieldName" => "PROPERTY_242"],
        ["section" => "columns", "fieldName" => "PROPERTY_241"],
        ["section" => "columns", "fieldName" => "PROPERTY_239"],
    ];

    foreach ($arResult["ELEMENTS_ROWS"] as $k => $row) {
        foreach ($arSpoiledFields as $arSpoiledField) {

            $section = $arSpoiledField["section"];
            $field   = $arSpoiledField["fieldName"];

            $arResult["ELEMENTS_ROWS"][$k][$section]["~$field"] =
            $arResult["ELEMENTS_ROWS"][$k][$section][$field] =
                preg_replace(
                    [
                        0 => "/&amp;/",
                        1 => "/amp;/",
                        2 => "/ am$/",
                        3 => "/ am</",
                        4 => "/&quot;/",
                        5 => "/quot;/"
                    ],
                    [
                        0 => "",
                        1 => "",
                        2 => "",
                        3 => "<",
                        4 => "'",
                        5 => "",
                    ],
                    $arResult["ELEMENTS_ROWS"][$k][$section][$field]
                );
        }
    }