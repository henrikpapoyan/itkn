<?php
/**
 * User: German
 * Date: 13.10.2017
 * Time: 13:39
 */

if (array_search($arResult['IBLOCK_ID'], gk_GetBpmIBlock()) !== false) //GK
{
    $tabHistory = array();
    foreach ($arResult["FIELDS"] as $fieldId => $field) {
        if ($field['CODE'] == 'ISTORIYA_RABOTY_S_OBRASHCHENIEM')
        {
            $field["LIST_SECTIONS_URL"] = $arParams["~LIST_SECTIONS_URL"];
//            $field["SOCNET_GROUP_ID"] = $socnetGroupId;
            $field["LIST_ELEMENT_URL"] = $arParams["~LIST_ELEMENT_URL"];
            $field["LIST_FILE_URL"] = $arParams["~LIST_FILE_URL"];
            $field["IBLOCK_ID"] = $arResult["IBLOCK_ID"];
            $field["SECTION_ID"] = intval($arParams["~SECTION_ID"]);
            $field["ELEMENT_ID"] = $arResult["ELEMENT_ID"];
            $field["FIELD_ID"] = $fieldId;
            $field["VALUE"] = $arResult["FORM_DATA"]["~".$fieldId];
            $field["COPY_ID"] = $arResult["COPY_ID"];
            $preparedData = \Bitrix\Lists\Field::prepareFieldDataForEditForm($field);
            if($preparedData)
            {
                $tabHistory[] = $preparedData;
//                if(!empty($preparedData["customHtml"]))
//                {
//                    $cuctomHtml .= $preparedData["customHtml"];
//                }
            }

            unset($arResult["FIELDS"][$fieldId]);
        }elseif($field['CODE'] == 'NUM_OBRASHCHENIYA_V_SERVICEDESK')
        {
            $arResult['NUM_OBRASHCHENIYA'] = $arResult['ELEMENT_PROPS'][$field['ID']]['VALUE'];
//            unset($arResult["FIELDS"][$fieldId]);

        }elseif($field['FIELD_ID'] == 'NAME') {
//            unset($arResult["FIELDS"][$fieldId]);
        }elseif($field['CODE'] ==  'KOMPANIYA_KLIENTA')
        {
            $arResult['KOMPANIYA_KLIENTA'] = $arResult['ELEMENT_PROPS'][$field['ID']]['VALUE'];
        }

    }
    $arResult["TAB_HISTORY"] = $tabHistory;
    $TabHtml = '';
    if ($arResult['KOMPANIYA_KLIENTA'] >0)
    {

        ob_start();
        $APPLICATION->IncludeComponent("itk:lists.list", ".default", array(
            "IBLOCK_TYPE_ID" => 'lists_socnet',
            "IBLOCK_ID" => 1002,
            "SECTION_ID" => 0,
    //        "LISTS_URL" => '/crm/bpm/',
    //        "LIST_EDIT_URL" => '/crm/bpm/#list_id#/edit/',
    //        "LIST_URL" => '/crm/bpm/#list_id#/view/#section_id#/',
    //        "LIST_SECTIONS_URL" => '/crm/bpm/#list_id#/edit/#section_id#/',
    //        "LIST_ELEMENT_URL" => '/crm/bpm/#list_id#/element/#section_id#/#element_id#/',
    //        "LIST_FILE_URL" => '/crm/bpm/#list_id#/file/#section_id#/#element_id#/#field_id#/#file_id#/',
    //        "LIST_FIELDS_URL" => '/crm/bpm/#list_id#/fields/',
    //        "EXPORT_EXCEL_URL" => '/crm/bpm/#list_id#/excel/',
    //        "BIZPROC_LOG_URL" => '/crm/bpm/#list_id#/bp_log/#document_state_id#/',
    //        "BIZPROC_TASK_URL" => '/crm/bpm/#list_id#/bp_task/#section_id#/#element_id#/#task_id#/',
    //        "BIZPROC_WORKFLOW_START_URL" => '/crm/bpm/#list_id#/bp_start/#element_id#/',
    //        "BIZPROC_WORKFLOW_ADMIN_URL" => '/crm/bpm/#list_id#/bp_list/',
            "CACHE_TYPE" => 'A',
            "CACHE_TIME" => 36000000,
            'KOMPANIYA_KLIENTA' => $arResult['KOMPANIYA_KLIENTA'],
        ),
            null
        );
        $TabHtml = ob_get_clean();
    }


    $tabConnect = array(
        array(
            'id' => 'PROPERTY_CONNECT',
            'name' => 'Обращения',
//            'required' => '',
            'type' => 'custom',
//            "colspan" => true,
//            'customHtml' => 'yyyy',//$TabHtml,
            'value' => '<tr><td colspan="2">'.$TabHtml.'</td></tr>'
        )
    );
    $arResult["TAB_CONNECT"] = $tabConnect;
}
