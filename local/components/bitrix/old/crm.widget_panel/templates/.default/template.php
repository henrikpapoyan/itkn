<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
global $USER;

//GK
/*$arTrueWidget = array(
    "start_widget",
    "lead_widget",
    "deal_widget",
    "invoice_widget",
    "company_widget",
    "contact_widget",
    "company_portrait",
);
if (in_array($arResult['GUID'] , $arTrueWidget))
{
    $fname = __DIR__.'/'.$arResult['GUID'].'.json';
	if ($USER->GetID() == 372)
	{
		$data = array();
		$data['ROWS'] = $arResult['ROWS'];
		$data['LAYOUT'] = $arResult['LAYOUT'];
		file_put_contents($fname, json_encode($data));
	} else {
		$data = file_get_contents($fname);
		$data = json_decode($data, true);
		$arResult['ROWS'] = $data['ROWS'];
		$arResult['LAYOUT'] = $data['LAYOUT'];
	}
}*/
global $APPLICATION;
use Bitrix\Crm;
CJSCore::Init(array("amcharts", "amcharts_funnel", "amcharts_serial", "amcharts_pie", "fx", "jquery", "date"));
$APPLICATION->AddHeadScript("/bitrix/js/main/core/core_dragdrop.js");
$APPLICATION->SetAdditionalCSS("/bitrix/themes/.default/crm-entity-show.css");

$quid = $arResult['GUID'];
$prefix = strtolower($quid);
$containerID = "{$prefix}_container";
$settingButtonID = "{$prefix}_settings";
$disableDemoModeButtonID = "{$prefix}_disable_demo";
$demoModeInfoCloseButtonID = "{$prefix}_demo_info_close";
$demoModeInfoContainerID = "{$prefix}_demo_info";
if($arResult['ENABLE_DEMO']):
	?><div id="<?=htmlspecialcharsbx($demoModeInfoContainerID)?>" class="crm-widg-white-tooltip">
		<div class="crm-widg-white-text"><?=$arResult['DEMO_TITLE']?></div>
		<div class="crm-widg-white-text"><?=$arResult['DEMO_CONTENT']?></div>
		<div class="crm-widg-white-text">
			<div id="<?=htmlspecialcharsbx($disableDemoModeButtonID)?>" class="crm-widg-white-bottom-link"><?=GetMessage('CRM_WGT_DISABLE_DEMO')?></div>
		</div>
		<div id="<?=htmlspecialcharsbx($demoModeInfoCloseButtonID)?>" class="crm-widg-white-close"></div>
	</div><?
endif;

$listUrl = $arResult['PATH_TO_LIST'];
$widgetUrl = $arResult['PATH_TO_WIDGET'];
$switchToListButtonID = "{$prefix}_list";
$reloadButtonID = "{$prefix}_widget";
$settings = array(
	'defaultEntityType' => $arResult['DEFAULT_ENTITY_TYPE'],
	'entityTypes' => $arResult['ENTITY_TYPES'],
	'layout' => $arResult['LAYOUT'],
	'rows' => $arResult['ROWS'],
	'prefix' => $prefix,
	'containerId' => $containerID,
	'settingButtonId' => $settingButtonID,
	'serviceUrl' => '/local/components/bitrix/crm.widget_panel/settings.php?'.bitrix_sessid_get(),
	'listUrl' => $listUrl,
	'widgetUrl' => $widgetUrl,
	'currencyFormat' => $arResult['CURRENCY_FORMAT'],
	'maxGraphCount' => $arResult['MAX_GRAPH_COUNT'],
	'maxWidgetCount' => $arResult['MAX_WIDGET_COUNT'],
	'isDemoMode' => $arResult['ENABLE_DEMO'],
	'useDemoMode' => $arResult['USE_DEMO'],
	'demoModeInfoContainerId'=> $demoModeInfoContainerID,
	'disableDemoModeButtonId' => $disableDemoModeButtonID,
	'demoModeInfoCloseButtonId' => $demoModeInfoCloseButtonID,
	'isAjaxMode' => \Bitrix\Main\Page\Frame::isAjaxRequest()
);

$filterFieldInfos = array();
$filterFildCount = count($arResult['FILTER']);
for($i = 0; $i < $filterFildCount; $i++)
{
	$filterField = $arResult['FILTER'][$i];
	$filterID = $filterField['id'];
	$filterType = $filterField['type'];

	if($filterType === 'user')
	{
		$userID = isset($arResult['FILTER_FIELDS'][$filterID])
			? (intval(is_array($arResult['FILTER_FIELDS'][$filterID])
				? $arResult['FILTER_FIELDS'][$filterID][0]
				: $arResult['FILTER_FIELDS'][$filterID]))
			: 0;
		$userName = $userID > 0 ? CCrmViewHelper::GetFormattedUserName($userID) : '';

		ob_start();
		CCrmViewHelper::RenderUserCustomSearch(
			array(
				'ID' => "{$prefix}_{$filterID}_SEARCH",
				'SEARCH_INPUT_ID' => "{$prefix}_{$filterID}_NAME",
				'SEARCH_INPUT_NAME' => "{$filterID}_name",
				'DATA_INPUT_ID' => "{$prefix}_{$filterID}",
				'DATA_INPUT_NAME' => $filterID,
				'COMPONENT_NAME' => "{$prefix}_{$filterID}_SEARCH",
				'SITE_ID' => SITE_ID,
				'NAME_FORMAT' => $arParams['NAME_TEMPLATE'],
				'USER' => array('ID' => $userID, 'NAME' => $userName),
				'DELAY' => 100
			)
		);
		$val = ob_get_clean();

		$arResult['FILTER'][$i]['type'] = 'custom';
		$arResult['FILTER'][$i]['value'] = $val;

		$filterFieldInfo = array(
			'typeName' => 'USER',
			'id' => $filterID,
			'params' => array(
				'data' => array(
					'paramName' => "{$filterID}",
					'elementId' => "{$prefix}_{$filterID}"
				),
				'search' => array(
					'paramName' => "{$filterID}_name",
					'elementId' => "{$prefix}_{$filterID}_NAME"
				)
			)
		);
		$filterFieldInfos[] = $filterFieldInfo;
	}
	elseif($filterType === 'period')
	{
		$periodEditorID = $quid.'_filter_'.strtolower($filterID).'_editor';
		$elementID = $quid.'_filter_'.strtolower($filterID);
		ob_start();
		CCrmViewHelper::RenderWidgetFilterPeriod(
			array(
				'CONFIG' => $arResult['WIDGET_FILTER'],
				'EDITOR_ID' => $periodEditorID,
				'PARAM_ID' => $elementID,
				'PARAM_NAME' => $filterID
			)
		);
		$val = ob_get_clean();
		$arResult['FILTER'][$i]['type'] = 'custom';
		$arResult['FILTER'][$i]['enableWrapper'] = false;
		$arResult['FILTER'][$i]['value'] = $val;

		$filterFieldInfo = array(
			'typeName' => 'WIDGET_PERIOD',
			'id' => $filterID,
			'params' => array(
				'data' => array(
					'paramName' => "{$filterID}",
					'elementId' => $elementID
				),
				'editor' => array('id' => $periodEditorID)
			)
		);
		$filterFieldInfos[] = $filterFieldInfo;
	}
}

$headViewID =  isset($arParams['~RENDER_HEAD_INTO_VIEW']) ? $arParams['~RENDER_HEAD_INTO_VIEW'] : false;
if($headViewID && is_string($headViewID))
	$this->SetViewTarget($headViewID, 100);
?><div class="crm-btn-panel"><span id="<?=htmlspecialcharsbx($settingButtonID)?>" class="crm-btn-panel-btn"></span></div>
<div class="crm-filter-wrap"><?
$APPLICATION->IncludeComponent(
	'bitrix:crm.interface.filter',
	'flat',
	array(
		'GRID_ID' => $quid,
		'FILTER' => $arResult['FILTER'],
		'FILTER_ROWS' => $arResult['FILTER_ROWS'],
		'FILTER_FIELDS' => $arResult['FILTER_FIELDS'],
		'FILTER_PRESETS' => $arResult['FILTER_PRESETS'],
		'RENDER_FILTER_INTO_VIEW' => false,
		'OPTIONS' => $arResult['OPTIONS'],
		'ENABLE_PROVIDER' => true,
		'NAVIGATION_BAR' => array(
			'ITEMS' => array(
				array(
					'icon' => 'table',
					'id' => 'list',
					'active' => false,
					'counter' => $arResult['NAVIGATION_COUNTER'],
					'url' => $listUrl,
					'hint' => array(
						'title' => GetMessage('CRM_WGT_LIST_HINT_TITLE'),
						'content' => GetMessage('CRM_WGT_LIST_HINT_CONTENT'),
						'disabling' => GetMessage('CRM_WGT_DISABLE_LIST_HINT')
					)
				),
				array('icon' => 'chart', 'id' => 'widget', 'active' => true, 'url' => $widgetUrl)
			),
			'BINDING' => array(
				'category' => 'crm.navigation',
				'name' => 'index',
				'key' => strtolower($arResult['NAVIGATION_CONTEXT_ID'])
			)
		)
	),
	$component,
	array('HIDE_ICONS' => true)
);
if($headViewID && is_string($headViewID))
	$this->EndViewTarget();
?></div><?
if(!empty($arResult['BUILDERS'])):
	?><div id="rebuildMessageWrapper" ></div><?
endif;
?><div class="crm-widget" id="<?=htmlspecialcharsbx($containerID)?>"></div>
<script type="text/javascript">
	BX.ready(
		function()
		{
			BX.InterfaceFilterFieldInfoProvider.create("<?=CUtil::JSEscape("{$quid}")?>",
				{ infos: <?=CUtil::PhpToJSObject($filterFieldInfos)?> }
			);

			BX.CrmWidgetManager.serviceUrl = "<?='/bitrix/components/bitrix/crm.widget_panel/ajax.php?'.bitrix_sessid_get()?>";
			BX.CrmWidgetManager.filter = <?=CUtil::PhpToJSObject($arResult['WIDGET_FILTER'])?>;
			BX.CrmWidgetManager.contextData = <?=CUtil::PhpToJSObject($arResult['CONTEXT_DATA'])?>;
			BX.CrmWidgetManager.contextEntityTypeName = "<?=CUtil::JSEscape($arResult['DEFAULT_ENTITY_TYPE'])?>";
			BX.CrmWidgetManager.contextEntityID = <?=(int)$arResult['DEFAULT_ENTITY_ID']?>;

			BX.CrmWidgetDataPreset.items = <?=CUtil::PhpToJSObject(Crm\Widget\Data\DataSourceFactory::getPresets())?>;
			BX.CrmWidgetDataPreset.notSelected = "<?=GetMessageJS("CRM_WGT_PRESET_NOT_SELECTED")?>";

			BX.CrmWidgetDataCategory.items = <?=CUtil::PhpToJSObject(Crm\Widget\Data\DataSourceFactory::getCategiries())?>;
			BX.CrmWidgetDataCategory.notSelected = "<?=GetMessageJS("CRM_WGT_CATEGORY_NOT_SELECTED")?>";
			BX.CrmWidgetDataCategory.groupTitle = "<?=GetMessageJS("CRM_WGT_CATEGORY_GROUP_TITLE")?>";

			BX.CrmPhaseSemantics.descriptions = <?=CUtil::PhpToJSObject(Crm\PhaseSemantics::getAllDescriptions())?>;
			BX.CrmPhaseSemantics.detailedInfos = <?=CUtil::PhpToJSObject(Crm\PhaseSemantics::getEntityDetailInfos($arResult['ENTITY_TYPES']))?>;
			BX.CrmWidgetColorScheme.descriptions =
			{
				red: "<?=GetMessageJS("CRM_WGT_COLOR_RED")?>",
				green: "<?=GetMessageJS("CRM_WGT_COLOR_GREEN")?>",
				blue: "<?=GetMessageJS("CRM_WGT_COLOR_BLUE")?>",
				cyan: "<?=GetMessageJS("CRM_WGT_COLOR_CYAN")?>",
				yellow: "<?=GetMessageJS("CRM_WGT_COLOR_YELLOW")?>"
			};

			BX.CrmWidgetDataContext.descriptions = <?=CUtil::PhpToJSObject(Crm\Widget\Data\DataContext::getAllDescriptions())?>;

			BX.CrmWidgetDataGroup.descriptions = <?=CUtil::PhpToJSObject(Crm\Widget\Data\DataGrouping::getAllDescriptions())?>;
			BX.CrmWidgetDataGroup.extras = <?=CUtil::PhpToJSObject(Crm\Widget\Data\DataSourceFactory::getGroupingExtras())?>;

			BX.CrmWidgetFilterPeriod.descriptions = <?=CUtil::PhpToJSObject($arResult["filterPeriodType"])?>;
			BX.CrmWidgetExpressionOperation.descriptions = <?=CUtil::PhpToJSObject(Crm\Widget\Data\ExpressionOperation::getAllDescriptions())?>;

			BX.CrmWidgetExpressionOperation.messages =
			{
				"diffLegend": "<?=GetMessageJS("CRM_WGT_EXPR_LEGEND_DIFF")?>",
				"sumLegend": "<?=GetMessageJS("CRM_WGT_EXPR_LEGEND_SUM")?>",
				"percentLegend": "<?=GetMessageJS("CRM_WGT_EXPR_LEGEND_PERCENT")?>",
				"hint": "<?=GetMessageJS("CRM_WGT_EXPR_HINT")?>"
			};

			BX.CrmWidget.messages =
			{
				"legend": "<?=GetMessageJS("CRM_WGT_RATING_LEGEND")?>",
				"nomineeRatingPosition": "<?=GetMessageJS("CRM_WGT_RATING_NOMINEE_POSITION")?>",
				"ratingPosition": "<?=GetMessageJS("CRM_WGT_RATING_POSITION")?>",
				"configDialogTitle": "<?=GetMessageJS("CRM_WGT_CONFIG_DLG_TITLE")?>",
				"configDialogSaveButton": "<?=GetMessageJS("CRM_WGT_CONFIG_DLG_SAVE_BTN")?>",
				"configDialogCancelButton": "<?=GetMessageJS("CRM_WGT_CONFIG_DLG_CANCEL_BTN")?>",
				"periodCaption": "<?=GetMessageJS("CRM_WGT_PERIOD_CAPTION")?>",
				"removalConfirmation": "<?=GetMessageJS("CRM_WGT_REMOVAL_CONFIRMATION")?>",
				"menuItemConfigure": "<?=GetMessageJS("CRM_WGT_MENU_ITEM_CONFIGURE")?>",
				"menuItemRemove": "<?=GetMessageJS("CRM_WGT_MENU_ITEM_REMOVE")?>",
				"untitled": "<?=GetMessageJS("CRM_WGT_UNTITLED")?>"
			};

			BX.CrmWidgetConfigEditor.messages =
			{
				"dialogTitle": "<?=GetMessageJS("CRM_WGT_CONFIG_DLG_TITLE")?>",
				"dialogSaveButton": "<?=GetMessageJS("CRM_WGT_CONFIG_DLG_SAVE_BTN")?>",
				"dialogCancelButton": "<?=GetMessageJS("CRM_WGT_CONFIG_DLG_CANCEL_BTN")?>"
			};

			BX.CrmGraphWidgetConfigEditor.messages =
			{
				"addGraph": "<?=GetMessageJS("CRM_WGT_CONFIG_ADD_GRAPH")?>",
				"maxGraphError": "<?=GetMessageJS("CRM_WGT_CONFIG_ERROR_MAX_GRAPH_COUNT")?>"
			};

			BX.CrmWidgetConfigPeriodEditor.messages =
			{
				"yearDescription": "<?=GetMessageJS("CRM_WGT_PERIOD_DESCR_YEAR")?>",
				"quarterDescription": "<?=GetMessageJS("CRM_WGT_PERIOD_DESCR_QUARTER")?>",
				"monthDescription": "<?=GetMessageJS("CRM_WGT_PERIOD_DESCR_MONTH")?>",
				"lastDaysDescription": "<?=GetMessageJS("CRM_WGT_PERIOD_DESCR_LAST_DAYS")?>",
				"accordingToFilter": "<?=GetMessageJS("CRM_WGT_PERIOD_ACCORDING_TO_FILTER")?>",
				"caption": "<?=GetMessageJS("CRM_WGT_PERIOD_CAPTION")?>"
			};

			BX.CrmWidgetConfigPresetEditor.messages =
			{
				"semanticsCaption": "<?=GetMessageJS("CRM_WGT_PRESET_SEMANTICS")?>",
				"categoryCaption": "<?=GetMessageJS("CRM_WGT_PRESET_CATEGORY")?>",
				"nameCaption": "<?=GetMessageJS("CRM_WGT_PRESET_NAME")?>",
				"notSelected": "<?=GetMessageJS("CRM_WGT_PRESET_NOT_SELECTED")?>"
			};

			BX.CrmWidgetConfigTitleEditor.messages =
			{
				"placeholder": "<?=GetMessageJS("CRM_WGT_FIELD_TITLE_PLACEHOLDER")?>",
				"untitled": "<?=GetMessageJS("CRM_WGT_UNTITLED")?>"
			};

			BX.CrmWidgetConfigGroupingEditor.messages =
			{
				"caption": "<?=GetMessageJS("CRM_WGT_GROUPING_CAPTION")?>"
			};

			BX.CrmWidgetTypeSelector.messages =
			{
				"dialogTitle": "<?=GetMessageJS("CRM_WGT_TYPE_SELECTOR_DLG_TITLE")?>",
				"dialogSaveButton": "<?=GetMessageJS("CRM_WGT_CONFIG_DLG_SAVE_BTN")?>",
				"dialogCancelButton": "<?=GetMessageJS("CRM_WGT_CONFIG_DLG_CANCEL_BTN")?>"
			};

			BX.CrmWidgetPanelLayoutTypeSelector.messages =
			{
				"dialogTitle": "<?=GetMessageJS("CRM_WGT_LAYOUT_TYPE_SELECTOR_DLG_TITLE")?>",
				"dialogSaveButton": "<?=GetMessageJS("CRM_WGT_CONFIG_DLG_SAVE_BTN")?>",
				"dialogCancelButton": "<?=GetMessageJS("CRM_WGT_CONFIG_DLG_CANCEL_BTN")?>"
			};

			BX.CrmWidgetPanel.messages =
			{
				"menuItemReset": "<?=GetMessageJS("CRM_WGT_MENU_ITEM_RESET")?>",
				"menuItemAdd": "<?=GetMessageJS("CRM_WGT_MENU_ITEM_ADD")?>",
				"menuChangeLayout": "<?=GetMessageJS("CRM_WGT_MENU_CHANGE_LAYOUT")?>",
				"menuItemEnableDemoMode": "<?=GetMessageJS("CRM_WGT_MENU_ITEM_ENABLE_DEMO_MODE")?>",
				"maxWidgetError": "<?=GetMessageJS("CRM_WGT_CONFIG_ERROR_MAX_WIDGET_COUNT")?>"
			};

			BX.CrmWidgetTypeSelector.entityTypeInfos =
			[
				{ name: "<?=CCrmOwnerType::DealName?>", description: "<?=CCrmOwnerType::GetDescription(CCrmOwnerType::Deal)?>" },
				{ name: "<?=CCrmOwnerType::InvoiceName?>", description: "<?=CCrmOwnerType::GetDescription(CCrmOwnerType::Invoice)?>" },
				{ name: "<?=CCrmOwnerType::LeadName?>", description: "<?=CCrmOwnerType::GetDescription(CCrmOwnerType::Lead)?>" },
				{ name: "<?=CCrmOwnerType::ContactName?>", description: "<?=CCrmOwnerType::GetDescription(CCrmOwnerType::Contact)?>" },
				{ name: "<?=CCrmOwnerType::CompanyName?>", description: "<?=CCrmOwnerType::GetDescription(CCrmOwnerType::Company)?>" },
				{ name: "<?=CCrmOwnerType::ActivityName?>", description: "<?=CCrmOwnerType::GetDescription(CCrmOwnerType::Activity)?>" }
			];

			BX.CrmWidgetTypeSelector.infos =
			[
				{
					name: "number",
					title: "<?=GetMessageJS('CRM_WGT_SELECTOR_TYPE_NUMBER')?>",
					logo: "<?=$templateFolder?>/images/view-number.jpg",
					params:
					{
						rowHeight: 180,
						config: { typeName: "number", configs: [ { name: "param1" } ] },
						data: { items: [ {} ] }
					}
				},
				{
					name: "numberBlock",
					title: "<?=GetMessageJS('CRM_WGT_SELECTOR_TYPE_NUMBER_BLOCK')?>",
					logo: "<?=$templateFolder?>/images/view-number-block.jpg",
					params:
					{
						rowHeight: 380,
						config:
							{
								typeName: "number",
								configs: [ { name: "param1" }, { name: "param2" }, { name: "param3" } ],
								layout:"tiled"
							},
						data: { items: [ {}, {}, {} ] }
					}
				},
				{
					name: "numberBlockExpr",
					title: "<?=GetMessageJS('CRM_WGT_SELECTOR_TYPE_NUMBER_BLOCK_EXPR')?>",
					logo: "<?=$templateFolder?>/images/view-number-block-expr.jpg",
					params:
					{
						rowHeight: 380,
						config:
							{
								typeName: "number",
								configs:
									[
										{ name: "param1" },
										{ name: "param2" },
										{
											name: "expr",
											dataSource:
												{
													name: "EXPRESSION",
													operation: "sum",
													arguments: [ "%param1%", "%param2%" ]
												}
										}
									],
								layout:"tiled"
							},
						data: { items: [ {}, {}, {} ] }
					}
				},
				{
					name: "rating",
					title: "<?=GetMessageJS('CRM_WGT_SELECTOR_TYPE_RATING')?>",
					logo: "<?=$templateFolder?>/images/view-rating.jpg",
					params:
					{
						rowHeight: 180,
						config:
							{
								typeName: "rating",
								group: "USER",
								nominee: <?=$arResult['CURRENT_USER_ID']?>,
								configs: [ { name: "param1" } ]
							},
						data: { items: [ {} ] }
					}
				},
				{
					name: "funnel",
					title: "<?=GetMessageJS('CRM_WGT_SELECTOR_TYPE_FUNNEL')?>",
					logo: "<?=$templateFolder?>/images/view-funnel.jpg",
					params:
					{
						rowHeight: 380,
						config: { typeName: "funnel" }
					}
				},
				{
					name: "barCluster",
					title: "<?=GetMessageJS('CRM_WGT_SELECTOR_TYPE_BAR_CLUSTERED')?>",
					logo: "<?=$templateFolder?>/images/view-bar.jpg",
					params:
					{
						rowHeight: 380,
						config:
							{
								typeName: "bar",
								group: "DATE",
								combineData: "Y",
								enableStack: "N",
								configs: [ { name: "param1" } ]
							}
					}
				},
				{
					name: "barStack",
					title: "<?=GetMessageJS('CRM_WGT_SELECTOR_TYPE_BAR_STACKED')?>",
					logo: "<?=$templateFolder?>/images/view-bar-stack.jpg",
					params:
					{
						rowHeight: 380,
						config:
							{
								typeName: "bar",
								group: "DATE",
								combineData: "Y",
								enableStack: "Y",
								configs: [ { name: "param1", display: { graph: { clustered: 'N' } } } ]
							}
					}
				},
				{
					name: "graph",
					title: "<?=GetMessageJS('CRM_WGT_SELECTOR_TYPE_GRAPH')?>",
					logo: "<?=$templateFolder?>/images/view-graph.jpg",
					params:
					{
						rowHeight: 380,
						config:
							{
								typeName: "graph",
								group: "DATE",
								combineData: "Y",
								configs: [ { name: "param1" } ]
							}
					}
				},
				{
					name: "pie",
					title: "<?=GetMessageJS('CRM_WGT_SELECTOR_TYPE_PIE')?>",
					//TODO: change logo
					logo: "<?=$templateFolder?>/images/view-pie.jpg",
					params:
					{
						rowHeight: 380,
						config:
						{
							typeName: "pie",
							group: "USER",
							configs: [ { name: "param1" } ]
						}
					}
				}
			];

			BX.CrmWidgetPanel.isAjaxMode = <?=\Bitrix\Main\Page\Frame::isAjaxRequest() ? 'true' : 'false'?>;
			BX.CrmWidgetPanel.current = BX.CrmWidgetPanel.create("<?=CUtil::JSEscape("{$quid}")?>", <?=CUtil::PhpToJSObject($settings)?>);
			BX.CrmWidgetPanel.current.layout();
		}
	);
</script>
<?if(!empty($arResult['BUILDERS'])):?>
<script type="text/javascript">
	BX.ready(
		function()
		{
			BX.CrmLongRunningProcessDialog.messages =
			{
				startButton: "<?=GetMessageJS('CRM_WGT_LRP_DLG_BTN_START')?>",
				stopButton: "<?=GetMessageJS('CRM_WGT_LRP_DLG_BTN_STOP')?>",
				closeButton: "<?=GetMessageJS('CRM_WGT_LRP_DLG_BTN_CLOSE')?>",
				wait: "<?=GetMessageJS('CRM_WGT_LRP_DLG_WAIT')?>",
				requestError: "<?=GetMessageJS('CRM_WGT_LRP_DLG_REQUEST_ERR')?>"
			};

			var builderData, builderSettings, builderPanel, builderId, builderPrefix;
			var prefix = "<?=CUtil::JSEscape($prefix)?>";
			<?foreach($arResult['BUILDERS'] as $builderData):?>
			builderData = <?=CUtil::PhpToJSObject($builderData)?>;
			builderId = BX.type.isNotEmptyString(builderData["ID"]) ? builderData["ID"] : "";
			builderPrefix = builderId !== "" ? (prefix + "_" + builderId.toLowerCase()) : prefix;
			builderSettings = BX.type.isPlainObject(builderData["SETTINGS"]) ? builderData["SETTINGS"] : {};
			builderPanel = BX.CrmLongRunningProcessPanel.create(
				builderId,
				{
					"containerId": "rebuildMessageWrapper",
					"prefix": builderPrefix,
					"active": true,
					"message": BX.type.isNotEmptyString(builderData["MESSAGE"]) ? builderData["MESSAGE"] : "",
					"manager":
						{
							dialogTitle: BX.type.isNotEmptyString(builderSettings["TITLE"]) ? builderSettings["TITLE"] : "",
							dialogSummary: BX.type.isNotEmptyString(builderSettings["SUMMARY"]) ? builderSettings["SUMMARY"] : "",
							actionName: BX.type.isNotEmptyString(builderSettings["ACTION"]) ? builderSettings["ACTION"] : "",
							serviceUrl: BX.type.isNotEmptyString(builderSettings["URL"]) ? builderSettings["URL"] : ""
						}
				}
			);
			builderPanel.layout();
			<?endforeach;?>
		}
	);
</script>
<?endif;?>
<?if(!$USER->IsAdmin()):?>
    <style>
        span.crm-widget-settings {display: none;}
    </style>
<?endif;?>
<?if($arResult["GUID"] and $arResult["WIDGET_FILTER"]["periodType"]):
    if("LN" == $arResult["WIDGET_FILTER"]["periodType"]):
?>
    <script>
        $(document).ready(function(){
          $('form[name=filter_<?=$arResult["GUID"];?>] #for_period_ln').show();
        });
    </script>
<?
    elseif("SD" == $arResult["WIDGET_FILTER"]["periodType"]):
?>
    <script>
        $(document).ready(function(){
            $('form[name=filter_<?=$arResult["GUID"];?>] #for_period_sd').show();
        });
    </script>
<?
    elseif("PD" == $arResult["WIDGET_FILTER"]["periodType"]):
?>
    <script>
        $(document).ready(function(){
            $('form[name=filter_<?=$arResult["GUID"];?>] #for_period_pd_start').show();
            $('form[name=filter_<?=$arResult["GUID"];?>] #for_period_pd_separate').show();
            $('form[name=filter_<?=$arResult["GUID"];?>] #for_period_pd_end').show();
        });
    </script>
<?
    endif;
endif;?>

<?
// отображение доп. полей для добавленных периодов (при перезагрузки страницы)
$request = Bitrix\Main\Application::getInstance()->getContext()->getRequest();
$periodType = $request->getQuery("PERIOD");
$nDate = $request->getQuery("BEGINDATE_days");
$sinceDate = $request->getQuery("BEGINDATE_since");
$startDate = $request->getQuery("BEGINDATE_from");
$endDate = $request->getQuery("BEGINDATE_to");

switch($periodType)
{
    case "LN":
    echo '<script>$(document).ready(function()
    {
      $("#for_period_ln").show();
      $("#for_period_ln input[name=BEGINDATE_days]").val('.$nDate.');
    });</script>';
        break;

    case "SD":
        echo '<script>$(document).ready(function()
    {
      $("#for_period_sd").show();
      $("#for_period_sd input[name=BEGINDATE_since]").val("'.$sinceDate.'");
    });</script>';
        break;

    case "PD":
        echo '<script>$(document).ready(function()
    {
      $("#for_period_pd_start, #for_period_pd_separate, #for_period_pd_end").show();
      $("#for_period_pd_start input[name=BEGINDATE_from]").val("'.$startDate.'");
      $("#for_period_pd_end input[name=BEGINDATE_to]").val("'.$endDate.'");
    });</script>';
        break;
}
?>
