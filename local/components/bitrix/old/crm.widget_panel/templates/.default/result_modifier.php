<?
use Bitrix\Crm\Widget\Filter;
use Bitrix\Crm\Widget\FilterPeriodType;
use Bitrix\Main\Type\Date;
if (!CModule::IncludeModule('crm'))
{
    ShowError(GetMessage('CRM_MODULE_NOT_INSTALLED'));
    return;
}
/**
 * @param $periodStart
 * @param $periodEnd
 * @param $statusID
 * @return bool|int
 * Считаем кол-во лидов за период
 */
function GetLeadsCountDayStatus($periodStart, $periodEnd, $statusID)
{
    $CCrmLead = new CCrmLead();
    $arFilter = array(
        ">=DATE_CREATE" => $periodStart,
        "<DATE_CREATE" => $periodEnd,
        "STATUS_ID" => $statusID,
        'SOURCE_ID' => 24, //[TITLE] => регистрация на сайте www.1ofd.ru
    );
    $dbCrmLead = $CCrmLead->GetList(
        array('DATE_CREATE' => 'DESC'),
        $arFilter,
        //array('SOURCE_ID','TITLE','STATUS_ID','DATE_CREATE')
        array('ID')
    );
    return $dbCrmLead->SelectedRowsCount();
}

function GetLeadsForDinamikaPopup($periodStart, $periodEnd, $code){
    $CCrmLead = new CCrmLead();
    $arFilter = array(
       ">=DATE_CREATE" => $periodStart,
       "<DATE_CREATE" => $periodEnd,
        'SOURCE_ID' => 24,
        );

    $dbCrmLead = $CCrmLead->GetList(
        array('DATE_CREATE' => 'DESC'),
        $arFilter,
//        array('ID','STATUS_ID'));
        array('ID','STATUS_ID','DATE_CREATE'));

    while ($ar = $dbCrmLead->Fetch()){
        $arLead[$ar['ID']] = $ar['STATUS_ID'];
        $arTemp[$ar['ID']] = $ar['DATE_CREATE'];
    }

    $arSort = array('id'=>'asc');
    $arFilter = array(
        'ENTITY_TYPE' => 'LEAD',
        'ENTITY_ID' => array_keys($arLead),
    );

    $arOptions = array();
    $obRes = CCrmEvent::GetListEx($arSort, $arFilter, false, false, array(), $arOptions);
    while ($arEvent = $obRes->Fetch()){
        $arLeadEvents[$arEvent['ENTITY_ID']][] = $arEvent;
    }
    $arValues = [];
    $count = 0;
    $count1 = 0;

    foreach ($arLead as $leadID => $leadInfo) {
        $flag = false;
        foreach ($arLeadEvents[$leadID] as $event) {
            if ($event['EVENT_TEXT_1'] == 'Распределение' && $event['EVENT_TEXT_2'] == 'Квалифицирован') {
                $flag = true;
                break;
            }
        }
        if ((bool)($arLead[$leadID] == 'NEW')) {
            $arIdNew[] = $leadID . ' - '. $arTemp[$leadID];
            $arIdNewLog[] = $leadID;
        }

        if ($flag) {
            $arIdPerehod[] = $leadID . ' - ' . $event['EVENT_TEXT_1'].' - '.$event['EVENT_TEXT_2'];
            $arIdPerehodLog[] = $leadID;
        }
        if ($flag || $arLead[$leadID] == 'NEW') {
            if (!in_array($leadID, $arValues['RASPRED'])) {
                $arValues['RASPRED'][] = $leadID;
            }
        }
        else if (!$flag) {
            if (!in_array($leadID, $arValues['LIUBOY_STATUS'])) {
                $arValues['LIUBOY_STATUS'][] = $leadID;
                $arValLuiboyStatus[] = $leadID . ' - ' . $arTemp[$leadID];
                $arValLuiboyStatusLog[] = $leadID;
            }
        }
    }

    return count($arValues[$code]);
}

function GetLeadsCountBySource($periodStart, $periodEnd, $sourceID)
{
    $CCrmLead = new CCrmLead();
    $arFilter = array(
        ">=DATE_CREATE" => $periodStart,
        "<DATE_CREATE" => $periodEnd,
        'SOURCE_ID' => $sourceID, //[TITLE] => регистрация на сайте www.1ofd.ru
    );
    $dbCrmLead = $CCrmLead->GetList(
        array('DATE_CREATE' => 'DESC'),
        $arFilter,
        //array('SOURCE_ID','TITLE','STATUS_ID','DATE_CREATE')
        array('ID')
    );
    return $dbCrmLead->SelectedRowsCount();
}

function leadsCountByDay($periodStart, $periodEnd)
{
    $CCrmLead = new CCrmLead();
    $arFilter = array(
        ">=DATE_CREATE" => $periodStart,
        "<DATE_CREATE" => $periodEnd,
    );
    $dbCrmLead = $CCrmLead->GetList(
        array('DATE_CREATE' => 'DESC'),
        $arFilter,
        //array('SOURCE_ID','TITLE','STATUS_ID','DATE_CREATE')
        array('ID')
    );
    return $dbCrmLead->SelectedRowsCount();
}

function GetEmployersPosition($periodStart, $periodEnd)
{
    global $DB;
    $strSql = "SELECT `b_sale_order`.`RESPONSIBLE_ID` `id_user`, SUM(`b_sale_order`.`SUM_PAID`) `inSum` FROM `b_sale_order` `b_sale_order` WHERE DATE(`b_sale_order`.`PAY_VOUCHER_DATE`) >= '$periodStart' AND DATE(`b_sale_order`.`PAY_VOUCHER_DATE`) < '$periodEnd' AND `b_sale_order`.`STATUS_ID`='P' GROUP BY `b_sale_order`.`RESPONSIBLE_ID` ORDER BY inSum DESC LIMIT 3";
    //echo $strSql.'<br />';
    $err_mess = "<br>GetEmployersPosition <br>Line: ";
    $rsEmployersRating = $DB->Query($strSql, false, $err_mess.__LINE__);
    $position = 1;
    $arResult = array();
    //echo '$rsEmployersRating->SelectedRowsCount() = '.$rsEmployersRating->SelectedRowsCount().'<br />';
    while($arEmployerRating = $rsEmployersRating->Fetch()) {
        $rsUser = CUser::GetByID($arEmployerRating["id_user"]);
        $arUser = $rsUser->Fetch();
        $auther = $arUser["NAME"] . " " . $arUser["LAST_NAME"];
        $arResult[] = array(
            "ID" => $arEmployerRating["id_user"],
            "value" => $position,
            "legend" => CurrencyFormat($arEmployerRating["inSum"], 'RUB'),
            "legendType" => "html",
            "auther" => $auther
        );
        $position++;
    }
    return $arResult;
}

function GetInvoicesCount($periodStart, $periodEnd, $type)
{

    $CCrmInvoice = new CCrmInvoice();
    $arFilter = array(
        ">=DATE_INSERT" => $periodStart,
        "<DATE_INSERT" => $periodEnd,
    );
    if($type == '1C'){
        $arFilter['EXTERNAL_ORDER'] = 'Y'; //выставлены из 1С
    }
    else{
        $arFilter['EXTERNAL_ORDER'] = 'N'; //выставлены из Битрикс
    }
    $dbCrmInvoice = $CCrmInvoice->GetList(
        array('DATE_INSERT' => 'DESC'),
        $arFilter,
        array('ID'));
    return $dbCrmInvoice->SelectedRowsCount();
}
/**
 * @param $periodStart
 * @param $periodEnd
 * @param $type
 * @return bool|int
 * Формируем массив для гистограммы кол-ва выставленных счетов, с учетом источника
 * выставления счета (из 1С или нет)
 */
function GetInvoicesDateCount($periodStart, $periodEnd, $type)
{

    $CCrmInvoice = new CCrmInvoice();
    $arFilter = array(
        ">=DATE_INSERT" => $periodStart,
        "<DATE_INSERT" => $periodEnd,
    );
    if ($type == '1C') {
        $arFilter['EXTERNAL_ORDER'] = 'Y'; //выставлены из 1С
    } else {
        $arFilter['EXTERNAL_ORDER'] = 'N'; //выставлены из Битрикс
    }
    $dbCrmInvoice = $CCrmInvoice->GetList(
        array('DATE_INSERT' => 'DESC'),
        $arFilter,
        array('ID'));
    return $dbCrmInvoice->SelectedRowsCount();
}

/**
 * @param $periodStart
 * @param $periodEnd
 * @param $type
 * @return mixed
 * Формируем массивы для гистограмм количества и сумм счетов по менеджерам
 */
function GetInvoicesArray($periodStart, $periodEnd, $type)
{
    global $DB;
    //$arFilter = array();
    $whereText = '';
    $curDate = new DateTime();
    $curDateText = $curDate->format("Y-m-d H:i:s");
    $dateStart = new DateTime($periodStart);
    $dateEnd = new DateTime($periodEnd);
    $sortParam = '';
    //pre(array("type" => $type, '$periodStart' => $periodStart, '$periodEnd' => $periodEnd));
    /** ОПЛАЧЕННЫЕ СЧЕТА. сумма счетов, дата оплаты которых попадает в выбранный в фильтре диапазон, не зависимо от статуса*/
    if ($type == 'PAYED') {
        $whereText = "`PAY_VOUCHER_DATE` >= '$periodStart' AND `PAY_VOUCHER_DATE` < '$periodEnd' AND `STATUS_ID` = 'P'";
        $sortParam = 'ORDER BY `PAY_VOUCHER_DATE` ASC';
        /*$arFilter = array(
            '>=PAY_VOUCHER_DATE' => $periodStart,
            '< PAY_VOUCHER_DATE' => $periodEnd,
            'STATUS_ID' => 'P',
        );*/
    }
    /** ОЖИДАЕМЫЕ К ОПЛАТЕ. сумма счетов со всеми статусами кроме "оплачен" и "отклонен", срок оплаты которых не истек в выбранном периоде*/
    else if ($type == 'EXPECTED_PAYMENT'){
        if($dateStart > $curDate && $dateEnd > $curDate){
            $whereText = "`DATE_PAY_BEFORE` >= '$periodStart' AND `CANCELED` = 'N' AND `STATUS_ID` != 'D' AND `STATUS_ID` != 'Y' AND `STATUS_ID` != 'P'";
        } elseif($dateStart <= $curDate && $dateEnd > $curDate){
            $whereText = "`DATE_PAY_BEFORE` > '$curDateText' AND `CANCELED` = 'N' AND `STATUS_ID` != 'D' AND `STATUS_ID` != 'Y' AND `STATUS_ID` != 'P'";
        } else { //if($dateEnd <= $curDate)
            $whereText = "`DATE_PAY_BEFORE` > '$periodEnd' AND `CANCELED` = 'N' AND `STATUS_ID` != 'D' AND `STATUS_ID` != 'Y' AND `STATUS_ID` != 'P'";
        }
        $sortParam = 'ORDER BY `DATE_PAY_BEFORE` ASC';
        /*$arFilter = array(
            ">DATE_PAY_BEFORE" => $periodEnd,
            'PAYED' => 'N',  //кроме оплаченных
            'CANCELED'=>'N', //и не отклоненные
            '!STATUS_ID' => 'D', //убрана проверка на заполненность DATE_CANCELED, т.к. после отмены счет могли перевести в др статус, а дата оставалась заполнена
        );
        $arFilterEmptyDate = array(
            "DATE_PAY_BEFORE" => false,
            'PAYED' => 'N',  //кроме оплаченных
            'CANCELED'=>'N', //и не отклоненные
            '!STATUS_ID' => 'D',
        );*/
    }
    /** ПРОСРОЧЕННЫЕ СЧЕТА. сумма счетов со всеми статусами, кроме "оплачен" и "отклонен", срок оплаты которых истек в выбранном периоде */
    else if( $type == 'OVERDUE_NEW'){
        if($dateStart > $curDate && $dateEnd > $curDate){
            $whereText = "((`DATE_PAY_BEFORE` > '$curDateText' AND `DATE_PAY_BEFORE` < '$curDateText') OR `DATE_PAY_BEFORE` = '') AND `CANCELED` = 'N' AND `STATUS_ID` != 'D' AND `STATUS_ID` != 'Y' AND `STATUS_ID` != 'P'";
        } elseif ($dateStart <= $curDate && $dateEnd > $curDate){
            $whereText = "((`DATE_PAY_BEFORE` >= '$periodStart' AND `DATE_PAY_BEFORE` <= '$curDateText') OR `DATE_PAY_BEFORE` = '') AND `CANCELED` = 'N' AND `STATUS_ID` != 'D' AND `STATUS_ID` != 'Y' AND `STATUS_ID` != 'P'";
        } else {
            $whereText = "((`DATE_PAY_BEFORE` >= '$periodStart' AND `DATE_PAY_BEFORE` <= '$periodEnd') OR `DATE_PAY_BEFORE` = '') AND `CANCELED` = 'N' AND `STATUS_ID` != 'D' AND `STATUS_ID` != 'Y' AND `STATUS_ID` != 'P'";
        }
        $sortParam = 'ORDER BY `DATE_PAY_BEFORE` ASC';
        /*$arFilter = array(
            '>=DATE_PAY_BEFORE' => $periodStart,
            "<DATE_PAY_BEFORE" => $periodEnd,
            '!STATUS_ID' => 'D',
            'PAYED' => 'N',  //кроме оплаченных
            'CANCELED'=>'N', //и не отклоненные
        );*/
    }
    $strSql = "SELECT `ID`, `STATUS_ID`, `PAY_VOUCHER_DATE`, `DATE_PAY_BEFORE`, `PAYED`, `CANCELED`, `PRICE`, `RESPONSIBLE_ID` FROM `b_sale_order` WHERE ($whereText) $sortParam";
    //echo $type.' strSql = '.$strSql.'<br />';
    echo '<div style="display: none">'.$type.' | '.$strSql.'</div>';
    $rsInvoices = $DB->Query($strSql, false, $err_mess.__LINE__);
    $arUsers = array();
    $arInvoicesFromDebug = array();
    while($arInvoice = $rsInvoices->Fetch()){
        $userIDtemp = $arInvoice['RESPONSIBLE_ID'];
        if(!isset($arRes[$userIDtemp])){
            $rsUser = CUSER::GetByID($userIDtemp);
            $arUser = $rsUser->Fetch();
            $arUsers[$arUser["ID"]] = $arUser;
        }
        $arInvoice["RESPONSIBLE_NAME"] = $arUsers[$userIDtemp]["NAME"];
        $arInvoice["RESPONSIBLE_LAST_NAME"] = $arUsers[$userIDtemp]["LAST_NAME"];
        $arRes[$arInvoice['RESPONSIBLE_ID']][] = $arInvoice;
        $arInvoicesFromDebug[] = $arInvoice;
    }
    echo '<pre style="display: none">'.$type.' | '.print_r($arInvoicesFromDebug, true).'</pre>';
    echo '<pre style="display: none">'.$type.' | '.print_r($arRes, true).'</pre>';
    //echo $type.' найдено счетов: '.$rsCompany->SelectedRowsCount().'<br />';
    /*$CCrmInvoice = new CCrmInvoice();
    $dbCrmInvoice = $CCrmInvoice->GetList(
        array('RESPONSIBLE_LAST_NAME'=>'asc'),
        $arFilter,
        array('ID','IBLOCK_ID','PAY_VOUCHER_DATE','DATE_PAY_BEFORE','PAYED','CANCELED','PRICE','RESPONSIBLE_ID','RESPONSIBLE_NAME','RESPONSIBLE_LAST_NAME')
    );
    while ($ar = $dbCrmInvoice->Fetch()){
        $arRes[$ar['RESPONSIBLE_ID']][] = $ar;
    }*/
    //ужасный костыль из-за отсутствия составного фильтра для CCrmInvoice GetList
    //(фильтр LOGIC=OR не работает)
    //и необходимости выбрать счета с пустой датой плановой оплаты
    /*if ($type == 'EXPECTED_PAYMENT'){
        $dbCrmInvoice = $CCrmInvoice->GetList(
            array('RESPONSIBLE_LAST_NAME'=>'asc'),
            $arFilterEmptyDate,
            array('ID','IBLOCK_ID','PAY_VOUCHER_DATE','DATE_PAY_BEFORE','PAYED','CANCELED','PRICE','RESPONSIBLE_ID','RESPONSIBLE_NAME','RESPONSIBLE_LAST_NAME')
        );
        while ($ar = $dbCrmInvoice->Fetch()){
            $arRes[$ar['RESPONSIBLE_ID']][] = $ar;
        }
    }
    pre($arRes);*/
    return $arRes;
}

/**
 * @param $arWidgetFilter
 * @return mixed
 * Формируем фильтр для выборки периода
 */
function MakeDateFilter($arWidgetFilter)
{
    foreach ($arWidgetFilter as $periodName => $value) {
        if ($periodName != 'enableEmpty' && $periodName != 'defaultPeriodType') {
            $arFilter[$periodName] = $value;
        }
    }
    return $arFilter;
}

/**
 * @param $arFilter
 * @return array
 * Формируем объект типа DateTime
 */
function GetDateObjectsFromFilter($arFilter)
{
    $filterPeriod = new Filter($arFilter);
    $arFilterPeriod = $filterPeriod->getPeriod();
    $periodTypeID = $filterPeriod->getPeriodTypeID();

    $currDay = new DateTime("now");
    $dateStart = new DateTime($arFilterPeriod["START"]->format("d.m.Y"));
    $dateEnd = new DateTime($arFilterPeriod["END"]->format("d.m.Y"));
    return array('START' => $dateStart, 'END' => $dateEnd, 'CUR' => $currDay, 'PERIOD_TYPE' => $periodTypeID);
}

/**
 * @param $arrToShow
 * @param $dataPreset
 * @param $dataSource
 * @return array
 * формирование массива для передачи в JS и armchats плагин
 */
function makeCellArray($arWidget, $arFilter, $arPeriodForFilter)
{
    $counter = 0;
    $elemIndex = 1;
    $arGraphs = $arConfigs = [];
    $color = $arWidget['COLOR'];
    if($arWidget['TYPE_NAME'] == 'bar' || $arWidget['TYPE_NAME'] == 'graph'){
        foreach ($arWidget['ITEMS'] as $id => $titleToShow) {
            $dataPreset = (is_array($arWidget['DATAPRESET']))?$arWidget['DATAPRESET'][$elemIndex-1]:$arWidget['DATAPRESET'];
            $dataSource = (is_array($arWidget['DATASOURCE']))?$arWidget['DATASOURCE'][$elemIndex-1]:$arWidget['DATASOURCE'];
            $arGraphs[] = array(
                "name" => "param" . $elemIndex,
                "title" => $titleToShow,
                "selectField" => "PARAM" . $elemIndex . "_COUNT",
                "display" => Array(
                    "graph" => Array(
                        "clustered" => "Y"
                    ),
                    "colorScheme" => $color,
                )
            );
            $arConfigs[] = Array(
                "name" => "param" . $elemIndex,
                "display" => Array(
                    "graph" => Array(
                        "clustered" => "N"
                    ),
                    "colorScheme" => $color,
                ),
                "title" => $titleToShow,
                "dataPreset" => $dataPreset,
                "dataSource" => $dataSource,
                "select" => Array(
                    "name" => "COUNT"
                )
            );
            $counter ++;
            $elemIndex++;
        }
        $arCellData = ['GRAPHS' => $arGraphs, 'CONFIGS' => $arConfigs];
    }
    else if($arWidget['TYPE_NAME'] == 'number'){
        $counter = 0;
        $elemIndex = 1;
        foreach ($arWidget['ITEMS'] as $id => $titleToShow){
            $dataPreset = (is_array($arWidget['DATAPRESET']))?$arWidget['DATAPRESET'][$counter]:$arWidget['DATAPRESET'];
            $dataSource = (is_array($arWidget['DATASOURCE']))?$arWidget['DATASOURCE'][$counter]:$arWidget['DATASOURCE'];
            $arConfigs[$counter] = Array(
                "name" => "param" . $elemIndex,
                "title" => $titleToShow,
                'dataPreset' => $dataPreset,
                'dataSource' => $dataSource,
            );
            if ($arWidget['X-POINTS'] == 'COUNT'){
                $arConfigs[$counter]['select'] = Array(
                    "name" => 'COUNT',
                    "aggregate" => 'COUNT'
                );
            }
            else
            {
                $temp = array(
                    "select" => Array(
                        "name" => 'SUM_TOTAL',
                        "aggregate" => 'SUM',
                    ),
                    "format" => Array(
                        "isCurrency" => 'Y',
                        "enableDecimals" => 'N',
                    ),
                );
                $arConfigs[$counter]=array_merge($arConfigs[$counter],$temp);
            }
            $counter ++;
            $elemIndex++;
        }

        $control = Array(
            "typeName" => $arWidget['TYPE_NAME'],
            "configs" => $arConfigs,
            'layout' => 'tiled',
            "entityTypeName" => 'INVOICE',
            "filter" => Array(
                "periodType" => $arPeriodForFilter['PERIOD_TYPE']),
            "customWidget" => 'Y');

        $filterPeriod = new Filter($arFilter);
        $widget = Bitrix\Crm\Widget\WidgetFactory::create($control, $filterPeriod, array('maxGraphCount'=>'6'));
        $arGraphs[] = $widget->prepareData();
        $arCellData = ['GRAPHS' => $arGraphs, 'CONFIGS' => $arConfigs];
    }
    elseif($arWidget['TYPE_NAME'] == 'pie')
    {
        $arConfigs[] = Array(
            "name" => "param1",
            "title" => $arWidget['TITLE'],
            "dataPreset" => $arWidget['DATAPRESET'],
            "dataSource" => $arWidget['DATASOURCE'],
            "select" => Array(
                "name" => "COUNT",
                "aggregate" => "COUNT"
            )
        );
        $arItems = array();
        $dateStart = $arPeriodForFilter['START'];
        $dateEnd = $arPeriodForFilter['END'];
        $filterDateStart = $dateStart->format('d.m.Y H:i:s');
        $filterDateEnd = $dateEnd->modify('+1 day')->format('d.m.Y H:i:s');
        //$totalCount = 0;
        foreach ($arWidget['ITEMS'] as $sourceID => $sourceTitle)
        {
            $leadsCount = GetLeadsCountBySource($filterDateStart, $filterDateEnd, $sourceID);
            //$totalCount += $leadsCount;
            if($leadsCount>0)
            {
                $arItems[] = array(
                    "COUNT" => $leadsCount,
                    "SOURCE_ID" => $sourceID,
                    "SOURCE" => $sourceTitle
                );
            }
        }
        //$arItems["TOTAL_COUNT"] = $totalCount;

        $arCellData = ['ITEMS' => $arItems, 'CONFIGS' => $arConfigs];
    }
    else if($arWidget['TYPE_NAME'] == 'rating')
    {
        $arConfigs[] = Array(
            "name" => "param1",
            "filter" => array(
                "semanticID" => "S"
            ),
            "dataPreset" => $arWidget['DATAPRESET'],
            "dataSource" => $arWidget['DATASOURCE'],
            "select" => Array(
                "name" => "SUM_TOTAL",
                "aggregate" => "SUM"
            ),
            "format" => array(
                "isCurrency" => "Y",
                "enableDecimals" => "N"
            )
        );
        $arItems = array();
        $arItems[] = array(
            "name" => "param1",
            "title" => "param1",
            "nomineeId" => CUser::GetId(),
            "positions" => array()
        );
        $arPositions = array();
        //var_dump($arPeriodForFilter);
        $dateStart = $arPeriodForFilter['START'];
        $dateEnd = $arPeriodForFilter['END'];
        $filterDateStart = $dateStart->format('Y-m-d H:i:s');
        $filterDateEnd = $dateEnd->modify('+1 day')->format('Y-m-d H:i:s');
        //$totalCount = 0;
        $arPositions = GetEmployersPosition($filterDateStart, $filterDateEnd);
        //$arItems["TOTAL_COUNT"] = $totalCount;
        $arItems[0]["positions"] = $arPositions;
        $arCellData = ['ITEMS' => $arItems, 'CONFIGS' => $arConfigs];
    }

    return $arCellData;
}

/**
 * @param $arPeriodForFilter
 * @param $arWidget
 * @param $__COUNT_FUNCTION_NAME - имя внутренней функции для подсчета значений по Y
 * @return array
 * Формирование точек графика (массив Values) для передачи в JS
 */
function makePointsForDateOnX($arPeriodForFilter, $arWidget, $__COUNT_FUNCTION_NAME){
    $dateStart = $arPeriodForFilter['START'];
    $dateEnd = $arPeriodForFilter['END'];
    $currDay = $arPeriodForFilter['CUR'];
    $arValues = array();
    while ($dateStart <= $dateEnd && $dateStart <= $currDay) {
        $filterDateStart = $dateStart->format('d.m.Y H:i:s'); //объект в строку
        $objDeltaDate = new DateTime($filterDateStart);
        $filterDateEnd = $objDeltaDate->modify('+1 day')->format('d.m.Y H:i:s'); //сдвиг на 1 день, итератор
        $elemIndex = 1;
        foreach ($arWidget['ITEMS'] as $keyWidget => $nameWidget) {
            $arDateValue["PARAM" . $elemIndex . "_COUNT"] = $__COUNT_FUNCTION_NAME($filterDateStart, $filterDateEnd, $keyWidget, $arWidget['TYPE_NAME']);
            $elemIndex++;
        }
        $arDateValue["DATE"] = $dateStart->format("Y-m-d");
        $dateStart->modify('+1 day');   //шаг цикла
        $arValues[] = $arDateValue;     //записываем все точки в общий массив
    }
    return $arValues;
}

function getLeadsCountByDay($arPeriodForFilter)
{
    $dateStart = $arPeriodForFilter['START'];
    $dateEnd = $arPeriodForFilter['END'];
    $currDay = $arPeriodForFilter['CUR'];
    $arValues = array();
    $arCountLead = array();
    while ($dateStart <= $dateEnd && $dateStart <= $currDay) {
        $filterDateStart = $dateStart->format('d.m.Y H:i:s'); //объект в строку
        $objDeltaDate = new DateTime($filterDateStart);
        $filterDateEnd = $objDeltaDate->modify('+1 day')->format('d.m.Y H:i:s'); //сдвиг на 1 день, итератор
        $leadCountByStatus = leadsCountByDay($filterDateStart, $filterDateEnd);
        /** формируем массив для конкретной даты (точка на оси Х):*/
        $arDateValue["PARAM1_COUNT"] = $leadCountByStatus;
        $arDateValue["DATE"] = $dateStart->format("Y-m-d");
        $dateStart->modify('+1 day');   //шаг цикла
        $arValues[] = $arDateValue;     //записываем все точки в общий массив
    }
    return $arValues;
}

function makeGraphPointsForInvoicesCount($arPeriodForFilter, $arObjectsInOneWidget)
{
    $dateStart = $arPeriodForFilter['START'];
    $dateEnd = $arPeriodForFilter['END'];
    $currDay = $arPeriodForFilter['CUR'];
    $arValues = array();
    $arCountInvoices = array();
    while ($dateStart <= $dateEnd && $dateStart <= $currDay) {
        $filterDateStart = $dateStart->format('d.m.Y H:i:s'); //объект в строку
        $objDeltaDate = new DateTime($filterDateStart);
        $filterDateEnd = $objDeltaDate->modify('+1 day')->format('d.m.Y H:i:s'); //сдвиг на 1 день, итератор

        $elemIndex = 1;
        $arCountInvoices = [];
        foreach ($arObjectsInOneWidget as $code => $statusText) {
            $InvoicesCountByStatus = GetInvoicesCount($filterDateStart, $filterDateEnd, $code);
            /** cчитаем общее кол-во лидов в точке времени, группировка по статусам */
            $arCountInvoices[$code] = $InvoicesCountByStatus;
            /** формируем массив для конкретной даты (точка на оси Х): набор сегментов с кол-вом  */
            $arDateValue["PARAM" . $elemIndex . "_COUNT"] = $InvoicesCountByStatus;
            $elemIndex++;
        }

        $arDateValue["DATE"] = $dateStart->format("Y-m-d");
        $dateStart->modify('+1 day');   //шаг цикла
        $arValues[] = $arDateValue;     //записываем все точки в общий массив
    }
    return $arValues;
}
/**
 * @param $arPeriodForFilter
 * @param $arWidgetItems
 * @return array
 * Формируем массив для графика кол-ва счетов за конкретную дату
 */
function makeGraphPointsForInvoicesDateCount($arPeriodForFilter, $arWidgetItems)
{
    $dateStart = $arPeriodForFilter['START'];
    $dateEnd = $arPeriodForFilter['END'];
    $currDay = $arPeriodForFilter['CUR'];
    $arValues = array();
    $arCountInvoices = array();
    while ($dateStart <= $dateEnd && $dateStart <= $currDay) {
        $filterDateStart = $dateStart->format('d.m.Y H:i:s'); //объект в строку
        $objDeltaDate = new DateTime($filterDateStart);
        $filterDateEnd = $objDeltaDate->modify('+1 day')->format('d.m.Y H:i:s'); //сдвиг на 1 день, итератор

        $elemIndex = 1;
        $arCountInvoices = [];
        foreach ($arWidgetItems as $code => $statusText) {
            $InvoicesCountByStatus = GetInvoicesDateCount($filterDateStart, $filterDateEnd, $code);
            /** cчитаем общее кол-во счетов в точке времени, группировка по статусам */
            $arCountInvoices[$code] = $InvoicesCountByStatus;
            /** формируем массив для конкретной даты (точка на оси Х): набор сегментов с кол-вом  */
            $arDateValue["PARAM" . $elemIndex . "_COUNT"] = $InvoicesCountByStatus;
            $elemIndex++;
        }

        $arDateValue["DATE"] = $dateStart->format("Y-m-d");
        $dateStart->modify('+1 day');   //шаг цикла
        $arValues[] = $arDateValue;     //записываем все точки в общий массив
    }
    return $arValues;
}
/**
 * @param $arPeriodForFilter
 * @param $arWidgetItems
 * @param $widgetTypeName
 * @return array
 * Формирование точек графика (массив Values) для передачи в JS (Контрагенты)
 */
function makeGraphPointsForCompanies($arPeriodForFilter, $arWidgetItems, $widgetTypeName)
{
    $dateStart = $arPeriodForFilter['START'];
    $dateEnd = $arPeriodForFilter['END'];
    $currDay = $arPeriodForFilter['CUR'];
    $arValues = array();
    $arCountLead = array();
    $arValues = array();
    while ($dateStart <= $dateEnd && $dateStart <= $currDay) {
        $filterDateStart = $dateStart->format('d.m.Y H:i:s'); //объект в строку
        $objDeltaDate = new DateTime($filterDateStart);
        $filterDateEnd = $objDeltaDate->modify('+1 day')->format('d.m.Y H:i:s'); //сдвиг на 1 день, итератор
        $elemIndex = 1;
        /**цикл сделан в целях масштабирования, на случай если в дальнейшем на виджет понадобится
         * вывести не один график, а несколько*/
        foreach ($arWidgetItems as $code => $title) {
            $countCompanies = GetCountCustomerCompanies($filterDateStart, $filterDateEnd, $code, $widgetTypeName);
            /** cчитаем общее кол-во компаний в точке времени */
            $arCountCompanies[$filterDateStart . '_' . $filterDateEnd] = $countCompanies;
            /** формируем массив для конкретной даты (точка на оси Х) */
            $arDateValue["PARAM" . $elemIndex . "_COUNT"] = $countCompanies;
            $elemIndex++;
        }
        $arDateValue["DATE"] = $dateStart->format("Y-m-d"); //для Y
        $arValues[] = $arDateValue;     //записываем все точки в общий массив
        $dateStart->modify('+1 day');   //шаг цикла
    }
    return $arValues;
}

/**
 * @param $arAllInvoiceInfo
 * @param $arProducts
 * @param $widgetTypeName
 * @param $arNewWidget
 * @return array
 * Формирование точек графика (массив Values) для передачи в JS (Сумма счетов)
 */
function makeGraphPointsForInvoicesProductSum($arAllInvoiceInfo, $arProducts, $widgetTypeName, $arNewWidget)
{
    foreach ($arAllInvoiceInfo as $userId => $arUserInvoiceInfo) {
        $elemIndex = 1;
        foreach ($arProducts as $productId => $productName) {
            if (!empty($arUserInvoiceInfo['PRODUCTS_SUM'][$productId])) {
                $arPayed[$userId][$productId] = $arUserInvoiceInfo['PRODUCTS_SUM'][$productId];
            } else {
                $arPayed[$userId][$productId] = $arUserInvoiceInfo['PRODUCTS_SUM'][$productId];
            }
            $elemIndex++;
        }
    }

    $arDateValue = [];
    foreach ($arPayed as $userId => $arUserProducts) {
        $elemIndex = 1;
        $arDateValue = [];
        $flag = false;
        foreach ($arNewWidget['WIDGET'] as $commonID => $name) {
            foreach ($arNewWidget['KEY_MAP'][$commonID] as $productID) {
                $arDateValue["PARAM" . $elemIndex . "_COUNT"] += $arUserProducts[$productID];
                if($arDateValue["PARAM" . $elemIndex . "_COUNT"] > 0) $flag =true;
            }
            $elemIndex++;
        }
        $arDateValue['USER'] = $arAllInvoiceInfo[$userId]['NAME'];
        //не пишем нулевых сотрудников
        if ($flag){
            $arValues[] = $arDateValue;
        }
    }
    SortValuesByFio($arValues);
    return $arValues;
}

/**
 * @param $arPeriodForFilter
 * @param $arWidgetItems
 * @param $widgetTypeName
 * @param $typeOfGraph
 * @return array
 */
function makeGraphPointsForInvoices($arPeriodForFilter, $arWidgetItems, $widgetTypeName, $typeOfGraph)
{
    $dateStart = $arPeriodForFilter['START'];
    $dateEnd = $arPeriodForFilter['END'];
    $filterDateStart = $dateStart->format('Y-m-d H:i:s');
    $filterDateEnd = $dateEnd->modify('+1 day')->format('Y-m-d H:i:s');
    $arValues = array();

    foreach ($arWidgetItems as $code => $statusText) {
        $arInvoices[$code] = GetInvoicesArray($filterDateStart, $filterDateEnd, $code);
    }

    $arTemp = [];
    foreach($arInvoices as $code => $arOneStatusArray){
        foreach($arOneStatusArray as $userID => $arInvoiceList){
            $arTemp[$userID][$code]['LIST'] = $arInvoiceList;
            $arTemp[$userID][$code]['COUNT'] = count($arInvoiceList);
            $arTemp[$userID][$code]['SUM'] = 0;
            foreach($arInvoiceList as $invoice){
                $arTemp[$userID][$code]['SUM'] += $invoice['PRICE'];
            }
            if(!empty($invoice['RESPONSIBLE_NAME']) || !empty($invoice['RESPONSIBLE_LAST_NAME'])){
                $name = $invoice['RESPONSIBLE_LAST_NAME'].' '.$invoice['RESPONSIBLE_NAME'];
            }
            else
                $name = $invoice['RESPONSIBLE_ID'];
            $arTemp[$userID][$code]['USER'] = $name;
        }
    }
    $arDateValue = [];
    $arStatusMap = array_keys($arWidgetItems);
    foreach($arTemp as $userID => $arStatus){
        $elemIndex = 1;
        foreach($arStatusMap as $code){
            if(array_key_exists($code,$arStatus)){
                $arDateValue["PARAM" . $elemIndex . "_COUNT"] = $arStatus[$code][$typeOfGraph];
            }
            else{
                $arDateValue["PARAM" . $elemIndex . "_COUNT"] = 0;
            }
            $elemIndex++;
        }
        $differStatus = current($arStatus);
        $arDateValue['USER'] = $differStatus['USER'];
        $arValues[] = $arDateValue;
    }
    SortValuesByFio($arValues);
    return $arValues;
}

/**
 * @param $arPeriodForFilter
 * @param $arWidgetItems
 * @param $widgetTypeName
 * @param $typeOfGraph
 * делаем выборку под дашборды типа number,где выводим сумму и кол-во счетов по периоду
 * разделеннные по статусам (выставленные, ожидаемые, пог)
 */
function GetCountForInvoicesDashbord($arPeriodForFilter, $arWidgetItems, $widgetTypeName, $typeOfGraph){
    $dateStart = $arPeriodForFilter['START'];
    $dateEnd = $arPeriodForFilter['END'];
    $filterDateStart = $dateStart->format('Y-m-d H:i:s');
    $filterDateEnd = $dateEnd->modify('+1 day')->format('Y-m-d H:i:s');
    //echo 'function GetCountForInvoicesDashbord<br />';
    //pre(array("filterDateStart" => $filterDateStart, "filterDateEnd" =>$filterDateEnd));
    foreach ($arWidgetItems as $code => $statusText) {
        $arInvoices[$code] = GetInvoicesArray($filterDateStart, $filterDateEnd, $code);
    }
    return $arInvoices;
}
/**
 * @param $a
 * @param $b
 * Сравнение строк для сортировки массива Values по ФИО
 */
function compareFio($a, $b)
{
    return strcmp($a["USER"], $b["USER"]);
}
function SortValuesByFio(&$arValues){
    usort($arValues, "compareFio");
}
/**
 * @param $arAllLeadsPeriods
 * @return float
 * Считаем конверсию в срезе одного столбика(т.е. периода, дня), результат в %:
 * (отношение кол-ва архивных лидов / к общему  колв-ву лидов всех статусов за этот день) *100%
 * (т.е отношение высоты столбца Архивные к общей высоте столбца гистограммы) *100%.
 * Если в расчетный день нет лидов (ни в одном статусе), то эту точку не берем в расчет  вообще
 */
function CalculateConversionOfLead($arAllLeadsPeriods)
{
    foreach ($arAllLeadsPeriods as $arCountLead) {
        unset($arCountLead['DATE']);
        if (array_sum($arCountLead) != 0) {
            $conversPeriod[] = $arCountLead['PARAM4_COUNT'] / array_sum($arCountLead) * 100;
        }
    }
    /**
     * Средняя конверсия на интервале времени, выставленном в фильтре - считаеся как отношение:
     *  конверсия в каждый расчитываемый период (%) /кол-во расчетных периодов
     *  т.е (ср.конверсия по столбцу в % / кол-во столбцов гистограммы.)
     * */
    return round((array_sum($conversPeriod) / count($conversPeriod)), 2);
}

/**
 * @param $arCellData
 * @param $typeName
 * @param $entityTypeName
 * @return array
 * формирование массива для вставки в $arResult нового виджета
 */
function makeNewWidget($arCellData, $typeName, $entityTypeName, $periodTypeID, $groupField)
{
    if (empty($typeName)) {
        $typeName = 'graph';
    }

    if($typeName == 'number' ){
        return $arNewWidget = Array(
            "controls" => Array(
                "0" => Array(
                    "typeName" => $typeName,
                    "configs" => $arCellData['CONFIGS'],
                    'layout' => 'tiled',
                    "entityTypeName" => $entityTypeName,
                    "filter" => Array(
                        "periodType" => $periodTypeID),
                    ),
                    "customWidget" => 'Y'
                ),
            "data" =>
                $arCellData['GRAPHS'],
                "myWidget" => "Y",
            );
    }
    elseif ($typeName == 'bar' || $typeName == 'graph')
    {
        if(empty($arCellData['ENABLE_STACK'])){
            $arCellData['ENABLE_STACK'] = 'Y';
        }
        return $arNewWidget = Array(
            "controls" => Array(
                "0" => Array(
                    "typeName" => $typeName,
                    "group" => (empty($groupField)) ? "DATE" : $groupField,
                    "combineData" => "Y",
                    "enableStack" => $arCellData['ENABLE_STACK'],
                    "configs" => $arCellData['CONFIGS'],
                    "entityTypeName" => $entityTypeName,
                    "title" => $arCellData['TITLE'],
                    "filter" => Array(
                        "periodType" => $periodTypeID,
                    ),
                    "context" => "E",
                    "customWidget" => 'Y'
                )
            ),
            "data" => Array(
                "0" => Array(
                    "items" => Array(
                        "0" => Array(
                            "graphs" => $arCellData['GRAPHS'],
                            "groupField" => (empty($groupField)) ? "DATE" : $groupField,
                            "values" => $arCellData['VALUES']
                        )
                    ),
                    "dateFormat" => "YYYY-MM-DD",
                )
            ),
            "myWidget" => "Y",
        );
    }
    elseif($typeName == 'pie')
    {
        return $arNewWidget = Array(
            "controls" => Array(
                "0" => Array(
                    "typeName" => $typeName,
                    "group" => (empty($groupField)) ? "DATE" : $groupField,
                    "configs" => $arCellData['CONFIGS'],
                    "entityTypeName" => $entityTypeName,
                    "filter" => Array(
                        "periodType" => $periodTypeID,
                    ),
                    "customWidget" => 'Y'
                )
            ),
            "data" => Array(
                "0" => Array(
                    "items" => $arCellData['ITEMS'],
                    "valueField" => "COUNT",
                    "titleField" => "SOURCE"
                )
            ),
            "myWidget" => "Y",
        );
    }
    elseif($typeName == 'rating')
    {
        return $arNewWidget = Array(
            "controls" => Array(
                "0" => Array(
                    "typeName" => $typeName,
                    "group" => (empty($groupField)) ? "DATE" : $groupField,
                    "nominee" => CUser::GetID(),
                    "configs" => $arCellData['CONFIGS'],
                    "entityTypeName" => $entityTypeName,
                    "filter" => Array(
                        "periodType" => $periodTypeID,
                    ),
                    "title" => $arCellData['TITLE'],
                    "customWidget" => 'Y'
                )
            ),
            "data" => Array(
                "0" => Array(
                    "items" => $arCellData['ITEMS']
                )
            ),
            "myWidget" => "Y",
        );
    }
    else
    {
        return array();
    }

}

/**
 * @param $periodStart
 * @param $periodEnd
 * @param $code
 * @param $widgetTypeName
 * @return bool|int
 * Подсчет кол-ва контрагентов
 */
function GetCountCustomerCompanies($periodStart, $periodEnd, $code, $widgetTypeName)
{
    $arFilter = [];
    if ($code != 'ALL') {
        $arFilter['COMPANY_TYPE'] = $code;
    }

    if ($widgetTypeName == 'bar') {
        $arFilter[">=DATE_CREATE"] = $periodStart;
    }
    $arFilter['<DATE_CREATE'] = $periodEnd;
    $arSelect = array('ID', 'COMPANY_TYPE');

    $CCrmCompany = new CCrmCompany();
    $dbCrmCompany = $CCrmCompany->GetList(array(), $arFilter, $arSelect);

    return $dbCrmCompany->SelectedRowsCount();
}

/**
 * @param $arRes
 * @param $newWidget
 * @param $arCellData
 * @param $arShowWidgets
 * @return array|int Добавляем новый виджет к списку виджетов
 */
function AddNewWidgetToList(&$arRes, $newWidget, $arCellData, $arShowWidgets)
{
    $updateFlag = false;
    foreach ($arRes as $index => &$row) {
        //контролируем чтобы не сбивалась сетка, по которой виджеты уже расположили
        //если колонок несколько, то их нужно обойти в цикле и проверить, были ли них наши графики
        //сопоставляем по тайтлу (названию графика), иначе их не отличить
        foreach ($row["cells"] as $key => &$cell) {
            if($cell["controls"]["0"]['typeName'] == 'number'){
                $curTitle = $cell["controls"]["0"]["configs"][0]['title'];
                $newTitle = $arCellData['GRAPHS'][0]['items'][0]['title'];
                if (stristr($curTitle, $newTitle) !== false) {
                    unset($arRes[$index]);
                }
            }
            else
            {
                $curTitle = $cell["controls"]["0"]["title"];
                $newTitle = (!empty($arCellData['TITLE_FIND'])) ? $arCellData['TITLE_FIND'] : $arCellData['TITLE'];
                if (stristr($curTitle, $newTitle) !== false) {
                    //обновление значений для графиков (нам в $arResult['ROWS'] остается и затем
                    //приходит пустой график, и ожидается, что мы ему передадим содержимое (точки) заново)
                    $cell = $newWidget;
                    $updateFlag = true;

                    //решаем проблему с автоизмением высоты наших виджетов
                    if ($row['height'] < 380) {
                        $row['height'] = 380;
                    }
                }
            }
        }
    }
    $blockHeight = 380;
    if($arCellData["TITLE_FIND"]=="Источники создания лидов"){
        if(sizeof($arCellData["ITEMS"])>12){
            $dopCount = sizeof($arCellData["ITEMS"])-12;
            $dopHeight = $dopCount*50;
            $blockHeight += $dopHeight;
        }
    }

    if($arCellData["TITLE"]=="Рейтинг сотрудников по оплаченным счетам"){
        $blockHeight = 225;
    }

    //кейс: часть виджетов отображается, часть мы удалили.
    //Необходимо, чтобы они выстроились в правильном порядке при обновлении страницы
    //ищем номер места вставки следующего виджета (он должен вставится под последним кастомным)
    $idSearchPlace = ResultHasPartofCustomWidgets($arRes, $arShowWidgets);

    //если виджет уже закреплен за каким-то местом и мы проапдейтили значения
    if ($updateFlag) {
        return $arRes;
    }
    //если виджеты часть видж была удалена - вставляем остальные под последним кастомным
    else if ($idSearchPlace !== false)
    {
        $arResPart1 = array_slice($arRes, 0, $idSearchPlace + 1);
        $arResPart2 = array_slice($arRes, $idSearchPlace + 1);
        $arNewValue = [
            Array(
                "cells" => Array(
                    "0" => $newWidget,
                ),
                "height" => $blockHeight
            )
        ];
        return $arRes = array_merge($arResPart1, $arNewValue, $arResPart2);
    }
    //иначе просто ставим виджет в начало
    else
    {
        $arNewValue = Array(
            "cells" => Array(
                "0" => $newWidget,
            ),
            "height" => $blockHeight
        );
        return array_unshift($arRes, $arNewValue);
    }
}

/**
 * @param $arRes
 * @param $arShowWidgets
 * @return bool|int|string
 * ищем индекс позиции последнего кастомного виджета в текущем массиве
 */
function ResultHasPartofCustomWidgets($arRes, $arShowWidgets)
{
    $lastKey = false;
    foreach ($arRes as $index => $row) {
        foreach ($row["cells"] as $key => &$cell) {
            $curTitle = $cell["controls"]["0"]["title"];
            if (compareTitles($curTitle, $arShowWidgets)) {
                $lastKey = $index;
            }
        }
    }
    return $lastKey;
}

/**
 * @param $curTitle
 * @param $arShowWidgets
 * @return bool
 * Сравниваем заголовки текущего виджета из массива с заголовками кастомных виджетов
 * Если соответствие найдено возвращаем true
 */
function compareTitles($curTitle, $arShowWidgets)
{
    foreach ($arShowWidgets as $arWidget) {
        if (stristr($curTitle, $arWidget['TITLE']) !== false) {
            return true;
        }
    }
    return false;
}

/**
 * @param $arOrderProducts
 * @param $arOrdersSum
 * @return mixed
 */
function GetProducsSumPrice($arOrderProducts, &$arOrdersSum)
{
    foreach ($arOrderProducts as $arProduct) {
        $arOrdersSum[$arProduct['PRODUCT_ID']] += $arProduct['PRICE'] * $arProduct['QUANTITY'];
    }
    return $arOrdersSum;
}

/**
 * @param $arPeriodForFilter
 * @return array
 */
function GetProductsInvoicesArray($arPeriodForFilter)
{
    $dateStart = $arPeriodForFilter['START'];
    $dateEnd = $arPeriodForFilter['END'];
    $arOrder = array('RESPONSIBLE_LAST_NAME'=>'asc');
    $arFilter = array(
        'STATUS_ID' => 'P',
        '>=PAY_VOUCHER_DATE' => $dateStart->format('d.m.Y H:i:s'),
        '<=PAY_VOUCHER_DATE' => $dateEnd->format('d.m.Y H:i:s'),
        //"PAYED" => 'Y',
    );
    $arSelect = array(
        'ID',
        'PAY_VOUCHER_SUM',
        'PRICE',
        'TAX_VALUE',
        'DATE_INSERT',
        'DATE_MARKED',
        'DATE_STATUS',
        'PAY_VOUCHER_DATE',
        'DATE_PAY_BEFORE',
        'STATUS_ID',
        'RESPONSIBLE_ID',
        'RESPONSIBLE_NAME',
        'RESPONSIBLE_LAST_NAME'
    );
    $dbCrmInvoice = CCrmInvoice::GetList($arOrder, $arFilter, $arSelect);
    $arRes = [];
    $arProducts = [];
    while ($ar = $dbCrmInvoice->Fetch()) {
        $ownerID = $ar['RESPONSIBLE_ID'];
        $orderID = $ar['ID'];
        if (!array_key_exists($ownerID, $arRes)) {
            $arRes[$ownerID]['NAME'] = $ar['RESPONSIBLE_LAST_NAME'] . ' ' . $ar['RESPONSIBLE_NAME'];
        }
        $arOrderProducts = [];
        $arOrderProducts = CCrmInvoice::GetProductRows($orderID);
        foreach ($arOrderProducts as $arOrderProduct) {
            if (!array_key_exists($arOrderProduct['PRODUCT_ID'], $arProducts)) {
                $arProducts[$arOrderProduct['PRODUCT_ID']] = $arOrderProduct['PRODUCT_NAME'];
            }
        }
        $arRes[$ownerID]['INVOICES'][$orderID] = $ar;
        $arRes[$ownerID]['PRODUCTS'][$orderID] = $arOrderProducts;
        $arRes[$ownerID]['PRODUCTS_SUM'] = GetProducsSumPrice($arOrderProducts, $arRes[$ownerID]['PRODUCTS_SUM']);
    }
    return ['INFO' => $arRes, 'PRODUCTS_TO_GRAPH' => $arProducts];
}

/**
 * @return mixed
 */
function GetProductsNameForBar()
{
    $arFilter = array('IBLOCK_CODE' => 'groups_for_producs', 'ACTIVE' => 'Y');
    $arSelect = array('ID', 'NAME');
    $arSort = array('sort' => 'asc');
    $rsSections = CIBlockSection::GetList($arSort, $arFilter, false, $arSelect);
    while ($ar = $rsSections->Fetch()) {
        $arSections[$ar['ID']] = $ar;
    }
    return $arSections;
}

/**
 * @return mixed
 */
function GetProductsSubstr()
{
    $arFilter = array('IBLOCK_CODE' => 'groups_for_producs', 'ACTIVE' => 'Y');
    $arSelect = array('ID', 'NAME', 'IBLOCK_SECTION_ID');
    $arSort = array('sort' => 'asc');
    $rsElements = CIBlockElement::GetList($arSort, $arFilter, false, false, $arSelect);
    while ($ar = $rsElements->Fetch()) {
        $arElements[$ar['ID']] = $ar;
    }
    return $arElements;
}

/**
 * @param $arItems
 * @return array
 */
function getProductsCustomArray($arItems)
{
    $arElemSubstr = GetProductsSubstr();
    $arSection = GetProductsNameForBar();
    $arNewProduct = array();
    foreach ($arItems as $productID => $name) {
        $temp = '';
        foreach ($arElemSubstr as $elemId => $arElem) {
            if (strpos($name, $arElem['NAME']) !== false) {
                $temp = trim($arSection[$arElem['IBLOCK_SECTION_ID']]['NAME']);
            }
        }
        if (!$temp) {
            $temp = 'Другое';
        }
        $key = array_search($temp, $arNewProduct);
        if ($key) {
            $arMap[$key][] = $productID;
        } else {
            $arMap[$productID][] = $productID;
        }
        if (!in_array($temp, $arNewProduct)) {
            $arNewProduct[$productID] = $temp;
        }
    }
    return ['KEY_MAP' => $arMap, 'WIDGET' => $arNewProduct];
}

/**--------------
 * ЛИДЫ
 * task-2935
 * БП по статусам:
 * 1. Потенциальный контрагент заходит на сайт и заполняет форму запроса регистрации, создается лид со статусом
 * "Распределение".
 * 2. Далее если лид заполняет форму предрегистрации - меняется статус на "Квалифицирован". Иначе статус не меняется.
 * 3. Далее подписание договора. После присвоения статуса "Архивный" лид становится контрагентом.
 * Для гистограммы берем три статуса: Распределение, Квалифицирован, Архивный
 * ---------------*/

/** task-3143
 * В разделе "Лиды" необходимо создать дашборд:
 * название - "Динамика заполнения формы Pop Up"
 * вид - гистограмма
 * данные:
 * ось х - дата создания лида
 * ось у - два ряда:
 * 1- суммарное кол-во лидов со статусом "Распределение" на текущий момент и лидов с любым статусом на текущий момент, у которых был зафиксирован переход со статуса "Распределение" на статус "Квалифицирован".
 * 2 - суммарное кол-во лидов с любым статусом на текущий момент, у которых не был зафиксирован переход со статуса "Распределение" на статус "Квалифицирован".
 */
if ($arResult['GUID'] == 'lead_widget') {
    $arrSourceList = CCrmStatus::GetStatusList('SOURCE');
    //echo '$arrSourceList = <pre>'.print_r($arrSourceList, true).'</pre>';
    // $arrStatusList = CCrmStatus::GetStatusList('STATUS'); //все статусы лида
    $arShowWidgets = array(
        'KKT'=> array(
            'ITEMS' => array(
                "NEW" => "1-й запрос с сайта",      //Распределение
                "ASSIGNED" => "Предрегистрация",    //Квалифицирован
                "2" => "Регистрация в ЛК",          //зарегистрирован в ЛК
                "3" => 'Выбор тарифа ККТ',          //Выбран тариф
                "1" => "Активация ККТ",             //Архивный
            ),
            'TITLE' => "Клиенты, которые выбрали тариф ККТ, после заполнения формы на сайте",//наименование виджета
            'TYPE_NAME' => 'bar',
            'DATAPRESET' => 'LEAD_IN_WORK::OVERALL_COUNT',
            'DATASOURCE' => 'LEAD_IN_WORK',
        ),
        'POP_UP' => array(
            'ITEMS' => [
                'RASPRED' => 'Заполнение формы Pop Up',
                'LIUBOY_STATUS' => 'Заполнение формы регистрации в ЛК без формы Pop Up'
            ],
            'TITLE' => "Динамика заполнения формы Pop Up на сайте 1-ofd",//наименование виджета
            'TYPE_NAME' => 'bar',
            'DATAPRESET' => 'LEAD_IN_WORK::OVERALL_COUNT',
            'DATASOURCE' => 'LEAD_IN_WORK',
        ),
        'SOURCE_CREATE_LEAD' => array(
            'ITEMS' => $arrSourceList,
            'TITLE' => "Источники создания лидов",//наименование виджета
            'TYPE_NAME' => 'pie',
            'DATAPRESET' => 'LEAD_SUM_STATS::OVERALL_COUNT',
            'DATASOURCE' => 'LEAD_SUM_STATS',
        ),
        'COUNT_CREATE_LEAD' => array(
            'ITEMS' => array("Количество созданных лидов"),
            'TITLE' => "Количество созданных лидов",
            'TYPE_NAME' => 'bar',
            'DATAPRESET' => 'LEAD_SUM_STATS::OVERALL_COUNT',
            'DATASOURCE' => 'LEAD_SUM_STATS',
        )
    );
    foreach ($arShowWidgets as $key=>$arWidget) {
        /** формируем фильтр по заданному временному интервалу */
        $arFilter = MakeDateFilter($arResult['WIDGET_FILTER']);
        /** Получаем массив объектов DateTime для дальнейшей работы  */
        $arPeriodForFilter = GetDateObjectsFromFilter($arFilter);

        if (!empty($arWidget['ITEMS'])) {
            $arCellData = [];
            if($key == 'KKT'){
                /** формируем массивы $arGraphs и $arConfigs для передачи в шаблон */
                $arCellData = makeCellArray($arWidget);
                $arCellData['VALUES'] = makePointsForDateOnX($arPeriodForFilter, $arWidget,'GetLeadsCountDayStatus');
                $middelLeadsСonversPerc = CalculateConversionOfLead($arCellData['VALUES']);
                $arCellData['TITLE'] = $arWidget['TITLE'].' '. $middelLeadsСonversPerc . "%";
                $arCellData['TITLE_FIND'] = $arWidget['TITLE'];
            }
            else if($key == 'POP_UP'){
                $arCellData = makeCellArray($arWidget);
                $arCellData['VALUES'] = makePointsForDateOnX($arPeriodForFilter, $arWidget, 'GetLeadsForDinamikaPopup');
                $arCellData['TITLE'] = $arWidget['TITLE'];
            }
            else if($key == 'SOURCE_CREATE_LEAD'){
                $arCellData = makeCellArray($arWidget, $arFilter, $arPeriodForFilter);

                // удаляем все источники, не входящие в список разрешенных (по ним статистику не выводим)
                if (!empty($arCellData["ITEMS"]))
                {
                    if (is_array($arCellData["ITEMS"]))
                    {
                        $arAllowSourceId = array(
                            1,
                            2,
                            3,
                            4,
                            5,
                            6,
                            7,
                            8,
                            9,
                            10,
                            11,
                            12,
                            13,
                            14,
                            15,
                            16,
                            17,
                            18,
                            19,
                            20,
                            21,
                            22,
                            23,
                            24,
                            25,
                            26,
                            27,
                            28,
                            29,
                            30,
                            31,
                            32,
                            33,
                            34,
                            35,
                            36,
                            "SELF",
                            "WEB",
                            "EMAIL",
                            "WEBFORM",
                            "OTHER",
                            "CALLBACK"
                        );

                        foreach ($arCellData["ITEMS"] as $cellDataKey =>  $cellDataValue)
                        {
                            if (!in_array($cellDataValue["SOURCE_ID"], $arAllowSourceId))
                            {
                                unset($arCellData["ITEMS"][$cellDataKey]);
                            }
                        }
                    }
                    if (!empty($arCellData["ITEMS"]))
                    {
                        sort($arCellData["ITEMS"]);
                    }
                }

                $arCellData['TITLE'] = $arWidget['TITLE'];
                $arCellData['TITLE_FIND'] = $arWidget['TITLE'];
                $newWidget = makeNewWidget($arCellData, 'pie', 'LEAD', $arPeriodForFilter['PERIOD_TYPE'], "SOURCE");
            }
            else if($key == 'COUNT_CREATE_LEAD'){
                $arCellData = makeCellArray($arWidget, $arFilter, $arPeriodForFilter);
                $arCellData['VALUES'] = getLeadsCountByDay($arPeriodForFilter);
                $arCellData['TITLE'] = $arWidget['TITLE'];
                $arCellData['TITLE_FIND'] = $arWidget['TITLE'];
                $newWidget = makeNewWidget($arCellData, 'bar', 'LEAD', $arPeriodForFilter['PERIOD_TYPE'], "DATE");
            }

            if($key != "SOURCE_CREATE_LEAD" && $key != "COUNT_CREATE_LEAD") {
                $newWidget = makeNewWidget($arCellData, $arWidget['TYPE_NAME'], 'LEAD', $arPeriodForFilter['PERIOD_TYPE']);
            }
            AddNewWidgetToList($arResult['ROWS'], $newWidget, $arCellData);
        }
    }

}
/**КОНТРАГЕНТЫ
 * task-2821 - отчеты, добавление новой гистограммы
 * 1.2. Рост клиентской базы (в «Старт»).
 * График за выбранный период с изменением кол-ва контрагентов типа «Клиент».
 * Каждый след. период по оси считается как сумма кол-ва контрагентов за предыдущий период + кол-во
 * контрагентов за текущий период
 */
else if ($arResult['GUID'] == 'company_widget') {
    $arShowWidgets = array(
        array(
            'ITEMS' => ["ALL" => "Контрагенты"],  //графики на одном виджете (массив), в данном случае график один
            'TITLE' => "Динамика количества контрагентов",//наименование виджета
            'TYPE_NAME' => 'graph',  //граф
            'COLOR' => 'red',
            'DATAPRESET' => 'COMPANY_GROWTH_STATS::TOTAL_COUNT',
            'DATASOURCE' => 'COMPANY_GROWTH_STATS',
        ),
        array(
            'ITEMS' => ["ALL" => "Новые контрагенты"],
            'TITLE' => "Количество новых контрагентов",
            'TYPE_NAME' => 'bar',         //гистограма
            'COLOR' => 'red',
            'DATAPRESET' => 'COMPANY_GROWTH_STATS::TOTAL_COUNT',
            'DATASOURCE' => 'COMPANY_GROWTH_STATS',
        ),
        array(
            'ITEMS' => ["CUSTOMER" => "Клиенты"],
            'TITLE' => "Динамика количества клиентов",
            'TYPE_NAME' => 'graph',
            'COLOR' => 'green',
            'DATAPRESET' => 'COMPANY_GROWTH_STATS::TOTAL_COUNT',
            'DATASOURCE' => 'COMPANY_GROWTH_STATS',
        ),
        array(
            'ITEMS' => ["CUSTOMER" => "Новые клиенты"],
            'TITLE' => "Количество новых клиентов",
            'TYPE_NAME' => 'bar',
            'COLOR' => 'green',
            'DATAPRESET' => 'COMPANY_GROWTH_STATS::TOTAL_COUNT',
            'DATASOURCE' => 'COMPANY_GROWTH_STATS',
        ),
        array(
            'ITEMS' => ["PARTNER" => "Партнеры"],
            'TITLE' => "Динамика количества партнеров",
            'TYPE_NAME' => 'graph',
            'COLOR' => 'blue',
            'DATAPRESET' => 'COMPANY_GROWTH_STATS::TOTAL_COUNT',
            'DATASOURCE' => 'COMPANY_GROWTH_STATS',
        ),
        array(
            'ITEMS' => ["PARTNER" => "Новые партнеры"],
            'TITLE' => "Количество новых партнеров",
            'TYPE_NAME' => 'bar',
            'COLOR' => 'blue',
            'DATAPRESET' => 'COMPANY_GROWTH_STATS::TOTAL_COUNT',
            'DATASOURCE' => 'COMPANY_GROWTH_STATS',
        ),
    );
    foreach ($arShowWidgets as $arWidget) {
        /** формируем фильтр по заданному временному интервалу */
        $arFilter = MakeDateFilter($arResult['WIDGET_FILTER']);
        /** Получаем массив объектов DateTime для дальнейшей работы  */
        $arPeriodForFilter = GetDateObjectsFromFilter($arFilter);
        /** формируем массивы $arGraphs и $arConfigs для передачи в шаблон */
        $arCellData = makeCellArray($arWidget);
        /** обходим по датам, делаем выборку количества */
        $arCellData['VALUES'] = makePointsForDateOnX($arPeriodForFilter, $arWidget, 'GetCountCustomerCompanies');
        $arCellData['TITLE'] = $arWidget['TITLE'];
        $arAllCellData[] = $arCellData;
        $newWidget = makeNewWidget($arCellData, $arWidget['TYPE_NAME'], 'COMPANY', $arPeriodForFilter['PERIOD_TYPE']);
        AddNewWidgetToList($arResult['ROWS'], $newWidget, $arCellData, $arShowWidgets);
    }
}
else if ($arResult['GUID'] == 'invoice_widget') {

    /** task-3029 Суммы оплаченных товаров
     * Вид дашборда - Гистограмма с наполнением
     * Ось х - Фамилия, Имя менеджеров
     * Ось y - сумма счетов с разделением на сумму по каждому товару (из счетов со статусом "оплачен", с
     * датой оплаты, попадающей в период, установленный в фильтре)
     */

    /** task-3045 Количество выставленных счетов
     * Вид дашборда: гистограмма
     * Название : "Количество выставленных счетов"
     * Название ряда - "Выставленные счета"
     * Ось х: дата выставления
     * Ось y: количество счетов на дату выставления
     * Разделены на выставленные из 1С и остальные
     */

    /** task-3009 Настройка гистограммы счетов по менеджерам
     * Суммы/кол-во счетов менеджеров по статусам счетов
     * Три ряда по каждому менеджеру:
     * - сумма/кол-во оплаченных счетов
     * - сумма/кол-во ожидаемых к оплате счетов (роме "оплачен" и "отклонен" с неистекшим сроком)
     * - сумма/кол-во просроченных счетов (кроме "оплачен" и "отклонен" c истекшим сроком)
     */

    $arShowWidgets = array(
        'SUM_PAYED_INVOICES_4' => array(
            'ITEMS' => [
                'Агентское вознаграждение',
                'Другое'
            ],
            'TITLE' => "Суммы оплаченных товаров", //автоматизировано дописываем в тайтл перечисление ITEMS
            'TYPE_NAME' => 'bar',
            'X-POINTS' => 'USER',
            'DATAPRESET' => 'INVOICE_SUM_STATS::OVERALL_COUNT',
            'DATASOURCE' => 'INVOICE_SUM_STATS',
        ),
        'SUM_PAYED_INVOICES_3' => array(
            'ITEMS' => [
                'ККТ',
                'Кабель ФН',
                'Макет ФН-1',
                'ФН-1',
                'ФД-Ридер',
            ],
            'TITLE' => "Суммы оплаченных товаров",
            'TYPE_NAME' => 'bar',
            'X-POINTS' => 'USER',
            'DATAPRESET' => 'INVOICE_SUM_STATS::OVERALL_COUNT',
            'DATASOURCE' => 'INVOICE_SUM_STATS',
        ),
        'SUM_PAYED_INVOICES_2' => array(
            'ITEMS' => [
                'Услуга передачи данных в ФНС',
                'Услуга "Электронный чек"',
            ],
            'TITLE' => "Суммы оплаченных товаров",
            'TYPE_NAME' => 'bar',
            'X-POINTS' => 'USER',
            'DATAPRESET' => 'INVOICE_SUM_STATS::OVERALL_COUNT',
            'DATASOURCE' => 'INVOICE_SUM_STATS',
        ),
        'SUM_PAYED_INVOICES_1' => array(
            'ITEMS' => [
                'Код активации'
            ],
            'TITLE' => "Суммы оплаченных товаров",
            'TYPE_NAME' => 'bar',
            'X-POINTS' => 'USER',
            'COLOR' => 'green',
            'DATAPRESET' => 'INVOICE_SUM_STATS::OVERALL_COUNT',
            'DATASOURCE' => 'INVOICE_SUM_STATS',
        ),
        'COUNT_BILL_INVOICE' => array(
            'ITEMS' => [
                "1C" => "Выставленные счета из 1С",
                'OTHER' => "Выставленные счета"
            ],
            'TITLE' => "Количество выставленных счетов",
            'TYPE_NAME' => 'bar',
            'X-POINTS' => 'DATE',
            'DATAPRESET' => 'INVOICE_SUM_STATS::OVERALL_COUNT',
            'DATASOURCE' => 'INVOICE_SUM_STATS',
        ),
        'SUMM_PAYED_INVOICE' => array(
            'ITEMS' => [
                "OVERDUE_NEW" => 'Cумма просроченных счетов',
                'EXPECTED_PAYMENT' => "Cумма ожидаемых к оплате счетов",
                "PAYED" => "Сумма оплаченных счетов",
            ],
            'TITLE' => "Суммы счетов менеджеров по статусам",
            'TYPE_NAME' => 'bar',
            'X-POINTS' => 'USER',
            'DATAPRESET' => [
                'INVOICE_OVERDUE::OVERALL_SUM',
                'INVOICE_IN_WORK::OVERALL_SUM_OWED',
                'INVOICE_SUM_STATS::OVERALL_SUM',
            ],
            'DATASOURCE' => [
                'INVOICE_OVERDUE',
                'INVOICE_IN_WORK',
                'INVOICE_SUM_STATS',
            ],
        ),
        'COUNT_PAYED_INVOICE' => array(
            'ITEMS' => [
                "OVERDUE_NEW" => "Кол-во просроченных счетов",
                'EXPECTED_PAYMENT' => "Кол-во ожидаемых к оплате счетов",
                "PAYED" => "Кол-во оплаченных счетов",
            ],
            'TITLE' => "Количество счетов менеджеров по статусам",
            'TYPE_NAME' => 'bar',
            'X-POINTS' => 'USER',
            'DATAPRESET' => [
                'INVOICE_OVERDUE::OVERALL_COUNT',
                'INVOICE_IN_WORK::OVERALL_COUNT_OWED',
                'INVOICE_SUM_STATS::OVERALL_COUNT',
            ],
            'DATASOURCE' => [
                'INVOICE_OVERDUE',
                'INVOICE_IN_WORK',
                'INVOICE_SUM_STATS',
            ],
        ),
        'NUM_SUMM_PAYED_INVOICE' =>array(
            'ITEMS' => [
                "PAYED" => "  Сумма оплаченных счетов",
                'EXPECTED_PAYMENT' => "  Сумма  ожидаемых к оплате счетов",
                "OVERDUE_NEW" => "  Сумма  просроченных счетов",
            ],
            'TYPE_NAME' => 'number',
            'X-POINTS' => 'SUMM',
            'DATAPRESET' => [
                'INVOICE_SUM_STATS::OVERALL_SUM',
                'INVOICE_IN_WORK::OVERALL_SUM_OWED',
                'INVOICE_OVERDUE::OVERALL_SUM'
            ],
            'DATASOURCE' => [
                'INVOICE_SUM_STATS',
                'INVOICE_IN_WORK',
                'INVOICE_OVERDUE'
            ],
        ),
        'NUM_COUNT_PAYED_INVOICE' =>array(
            'ITEMS' => [
                "PAYED" => "  Кол-во оплаченных счетов",
                'EXPECTED_PAYMENT' => "  Кол-во ожидаемых к оплате счетов",
                "OVERDUE_NEW" => "  Кол-во просроченных счетов",
            ],
            'TYPE_NAME' => 'number',
            'X-POINTS' => 'COUNT',
            'DATAPRESET' => [
                'INVOICE_SUM_STATS::OVERALL_COUNT',
                'INVOICE_IN_WORK::OVERALL_COUNT_OWED',
                'INVOICE_OVERDUE::OVERALL_COUNT'
            ],
            'DATASOURCE' => [
                'INVOICE_SUM_STATS',
                'INVOICE_IN_WORK',
                'INVOICE_OVERDUE',
            ],
        ),
        'EMPOLYERS_INVOICE_SUM_RATING' =>array(
            'ITEMS' => array("Рейтинг сотрудников по оплаченным счетам"),
            'TYPE_NAME' => 'rating',
            'DATAPRESET' => "INVOICE_SUM_STATS::OVERALL_SUM",
            'DATASOURCE' => "INVOICE_SUM_STATS",
            'TITLE' => "Рейтинг сотрудников по оплаченным счетам",
            'X-POINTS' => "USER",
        ),

    );
    foreach ($arShowWidgets as $key => $arWidget) {
        /** формируем фильтр по заданному временному интервалу */
        $arFilter = MakeDateFilter($arResult['WIDGET_FILTER']);
        /** Получаем массив объектов DateTime для дальнейшей работы  */
        $arPeriodForFilter = GetDateObjectsFromFilter($arFilter);
        if (strpos($key, 'SUM_PAYED_INVOICES') !== false) {
            $arInfo = GetProductsInvoicesArray($arPeriodForFilter);
            $arNewWidget = getProductsCustomArray($arInfo['PRODUCTS_TO_GRAPH']);
            $arNewWidget['WIDGET'] = $arWidget['ITEMS'] = array_intersect($arNewWidget['WIDGET'], $arWidget['ITEMS']);
        }

        if (!empty($arWidget['ITEMS'])) {
            $arCellData = [];
            /** обходим по сотрудникам, делаем выборку количества */
            if (strpos($key, 'SUM_PAYED_INVOICES') !== false) {
                /** формируем массивы $arGraphs и $arConfigs для передачи в шаблон */
                $arCellData = makeCellArray($arWidget);
                $arCellData['VALUES'] = makeGraphPointsForInvoicesProductSum($arInfo['INFO'], $arInfo['PRODUCTS_TO_GRAPH'], $arWidget['TYPE_NAME'], $arNewWidget);
                $arCellData['TITLE'] = $arWidget['TITLE'] . ' (' . implode(", ", $arWidget['ITEMS']) . ')';
            }
            else if ($key == 'COUNT_BILL_INVOICE') {
                $arCellData = makeCellArray($arWidget);
                $arCellData['VALUES'] = makePointsForDateOnX($arPeriodForFilter, $arWidget, 'GetInvoicesDateCount');
                $arCellData['TITLE'] = $arWidget['TITLE'];
            }
            else if ($key == 'SUMM_PAYED_INVOICE') {
                $arCellData = makeCellArray($arWidget);
                $arCellData['VALUES'] = makeGraphPointsForInvoices($arPeriodForFilter, $arWidget['ITEMS'], $arWidget['TYPE_NAME'], 'SUM');
                $arCellData['ENABLE_STACK'] = 'N';
                $arCellData['TITLE'] = $arWidget['TITLE'];
            }
            else if ($key == 'COUNT_PAYED_INVOICE') {
                $arCellData = makeCellArray($arWidget);
                $arCellData['VALUES'] = makeGraphPointsForInvoices($arPeriodForFilter, $arWidget['ITEMS'], $arWidget['TYPE_NAME'],'COUNT');
                $arCellData['ENABLE_STACK'] = 'N';
                $arCellData['TITLE'] = $arWidget['TITLE'];
            }
            else if ($key == 'NUM_COUNT_PAYED_INVOICE'){
                //echo "$key<br />";
                $arCellData = makeCellArray($arWidget,$arFilter,$arPeriodForFilter);
                $arInvoicesStatus = GetCountForInvoicesDashbord($arPeriodForFilter, $arWidget['ITEMS'], $arWidget['TYPE_NAME']);
                //pre(array('arInvoicesStatus' => $arInvoicesStatus));
                //заносим кол-во счетов в соответствующие ячейки массива графика
                $counter = 0;

                foreach($arInvoicesStatus as $statusCode => $arUserInvoices){
                    $tempCount = 0;
                    foreach($arUserInvoices as $userId=> $arInvoice) {
                        $tempCount += count($arInvoice);
                    }
                    $arCellData['GRAPHS'][0]['items'][$counter]['value'] = $tempCount;
                    $arUrl = parse_url(str_replace('widget','list', $arCellData['GRAPHS'][0]['items'][$counter]['url']));
                    $arUrl['path'] = $arUrl['path'].'index.php?';
                    $arCellData['GRAPHS'][0]['items'][$counter]['url'] = $arUrl['path'].str_replace($arWidget['DATASOURCE'][$counter],"INVOICE_".$statusCode,$arUrl['query']);
                    $counter++;
                }
            }
            else if ($key == 'NUM_SUMM_PAYED_INVOICE')
            {
                $arCellData = makeCellArray($arWidget,$arFilter,$arPeriodForFilter);
                $arInvoicesStatus = GetCountForInvoicesDashbord($arPeriodForFilter, $arWidget['ITEMS'], $arWidget['TYPE_NAME']);
                //заносим кол-во счетов в соответствующие ячейки массива графика
                $counter = 0;
                foreach($arInvoicesStatus as $statusCode => $arUserInvoices){
                    $tempCost = 0;
                    foreach($arUserInvoices as $userId=> $arInvoice){
                        foreach($arInvoice as $invoice){
                            $tempCost += $invoice['PRICE'];
                        }
                    }
                    $arCellData['GRAPHS'][0]['items'][$counter]['value'] = $tempCost;
                    $arCellData['GRAPHS'][0]['items'][$counter]['html'] =  CurrencyFormat($arCellData['GRAPHS'][0]['items'][$counter]['value'], 'RUB');
                    $arUrl = parse_url(str_replace('widget','list', $arCellData['GRAPHS'][0]['items'][$counter]['url']));
                    $arUrl['path'] = $arUrl['path'].'index.php?';
                    $arCellData['GRAPHS'][0]['items'][$counter]['url'] = $arUrl['path'].str_replace($arWidget['DATASOURCE'][$counter],"INVOICE_".$statusCode,$arUrl['query']);
                    $counter++;
                }
            }
            else if($key == "EMPOLYERS_INVOICE_SUM_RATING")
            {
                $arCellData = makeCellArray($arWidget,$arFilter,$arPeriodForFilter);
                $arCellData['TITLE'] = $arWidget['TITLE'];
                //echo '$arCellData <pre>'.print_r($arCellData, true).'</pre>';
            }

            $newWidget = makeNewWidget($arCellData, $arWidget['TYPE_NAME'], 'INVOICE', $arPeriodForFilter['PERIOD_TYPE'], $arWidget['X-POINTS']);
            AddNewWidgetToList($arResult['ROWS'], $newWidget, $arCellData,$arWidget['TYPE_NAME']);
        }
    }
}

else if ($arResult['GUID'] == 'order_widget')
{
    $arrSourceList = CCrmStatus::GetStatusList('SOURCE');

    $arShowWidgets = array(
        'SOURCE_CREATE_ORDER' => array(
            'ITEMS' => $arrSourceList,
            'TITLE' => "Источники создания заказов",//наименование виджета
            'TYPE_NAME' => 'pie',
            'DATAPRESET' => 'LEAD_SUM_STATS::OVERALL_COUNT',
            'DATASOURCE' => 'LEAD_SUM_STATS',
        ),
    );

    foreach ($arShowWidgets as $key => $arWidget)
    {
        /** формируем фильтр по заданному временному интервалу */
        $arFilter = MakeDateFilter($arResult['WIDGET_FILTER']);
        /** Получаем массив объектов DateTime для дальнейшей работы  */
        $arPeriodForFilter = GetDateObjectsFromFilter($arFilter);

        if (!empty($arWidget['ITEMS'])) {
            $arCellData = [];
            if($key == 'SOURCE_CREATE_ORDER'){
                $arCellData = makeCellArray($arWidget, $arFilter, $arPeriodForFilter);

                // удаляем все источники, не входящие в список разрешенных (по ним статистику не выводим)
                if (!empty($arCellData["ITEMS"]))
                {
                    if (is_array($arCellData["ITEMS"]))
                    {
                        $arAllowSourceId = array(37, 38, 41, 42, 43, 44);
                        foreach ($arCellData["ITEMS"] as $cellDataKey =>  $cellDataValue)
                        {
                            if (!in_array($cellDataValue["SOURCE_ID"], $arAllowSourceId))
                            {
                                unset($arCellData["ITEMS"][$cellDataKey]);
                            }
                        }
                    }
                    if (!empty($arCellData["ITEMS"]))
                    {
                        sort($arCellData["ITEMS"]);
                    }
                }

                $arCellData['TITLE'] = $arWidget['TITLE'];
                $arCellData['TITLE_FIND'] = $arWidget['TITLE'];
                $newWidget = makeNewWidget($arCellData, 'pie', 'LEAD', $arPeriodForFilter['PERIOD_TYPE'], "SOURCE");
            }

            AddNewWidgetToList($arResult['ROWS'], $newWidget, $arCellData);
        }
    }
}

// удаляем поле ответственный из фильтров
if (!empty($arResult['FILTER']))
{
    foreach ($arResult['FILTER'] as $keyFilter => $valueFilter)
    {
        if ($valueFilter["id"] == "RESPONSIBLE_ID")
        {
            $arResult['FILTER'][$keyFilter] = [];
        }
    }
}

// правка списка периода
$arFilterPeriodType = Bitrix\Crm\Widget\FilterPeriodType::getAllDescriptions();

//$arResult["filterPeriodType"] = [];
//$arFilterPeriodTypeTmp = [];
//
//$arFilterPeriodTypeTmp["0"] = ["0" => "", "1" => GetMessage("FILTER_PERIOD_EMPTY")];
//$arFilterPeriodTypeTmp["1"] = ["0" => "YS", "1" => GetMessage("FILTER_PERIOD_YESTERDAY")];
//$arFilterPeriodTypeTmp["3"] = ["0" => "W0", "1" => GetMessage("FILTER_PERIOD_CURRENT_WEEK")];
//$arFilterPeriodTypeTmp["10"] = ["0" => "LN", "1" => GetMessage("FILTER_PERIOD_LAST_N_DAYS")];
//$arFilterPeriodTypeTmp["14"] = ["0" => "SD", "1" => GetMessage("FILTER_PERIOD_SET_DATE")];
//$arFilterPeriodTypeTmp["15"] = ["0" => "WL", "1" => GetMessage("FILTER_PERIOD_LAST_WEEK")];
//$arFilterPeriodTypeTmp["16"] = ["0" => "ML", "1" => GetMessage("FILTER_PERIOD_LAST_MONTH")];
//$arFilterPeriodTypeTmp["17"] = ["0" => "PD", "1" => GetMessage("FILTER_PERIOD_DIAPAZON")];


//if(!empty($arFilterPeriodType))
//{
//    foreach($arFilterPeriodType as $filterPeriodKey => $filterPeriodValue)
//    {
//        if("D0" == $filterPeriodKey)
//        {
//            $arFilterPeriodTypeTmp["2"] = ["0" => $filterPeriodKey, "1" => GetMessage("FILTER_PERIOD_TODAY")];
//        }
//        elseif("M0" == $filterPeriodKey)
//        {
//            $arFilterPeriodTypeTmp["4"] = ["0" => $filterPeriodKey, "1" => $filterPeriodValue];
//        }
//        elseif("Q0" == $filterPeriodKey)
//        {
//            $arFilterPeriodTypeTmp["5"] = ["0" => $filterPeriodKey, "1" => $filterPeriodValue];
//        }
//        elseif("D7" == $filterPeriodKey)
//        {
//            $arFilterPeriodTypeTmp["6"] = ["0" => $filterPeriodKey, "1" => $filterPeriodValue];
//        }
//        elseif("D30" == $filterPeriodKey)
//        {
//            $arFilterPeriodTypeTmp["7"] = ["0" => $filterPeriodKey, "1" => $filterPeriodValue];
//        }
//        elseif("D60" == $filterPeriodKey)
//        {
//            $arFilterPeriodTypeTmp["8"] = ["0" => $filterPeriodKey, "1" => $filterPeriodValue];
//        }
//        elseif("D90" == $filterPeriodKey)
//        {
//            $arFilterPeriodTypeTmp["9"] = ["0" => $filterPeriodKey, "1" => $filterPeriodValue];
//        }
//        elseif("M" == $filterPeriodKey)
//        {
//            $arFilterPeriodTypeTmp["11"] = ["0" => $filterPeriodKey, "1" => $filterPeriodValue];
//        }
//        elseif("Q" == $filterPeriodKey)
//        {
//            $arFilterPeriodTypeTmp["12"] = ["0" => $filterPeriodKey, "1" => $filterPeriodValue];
//        }
//        elseif("Y" == $filterPeriodKey)
//        {
//            $arFilterPeriodTypeTmp["13"] = ["0" => $filterPeriodKey, "1" => $filterPeriodValue];
//        }
//    }
//}
//
//ksort($arFilterPeriodTypeTmp);
//vdump($arFilterPeriodTypeTmp);

//if(!empty($arFilterPeriodTypeTmp))
//{
//    foreach($arFilterPeriodTypeTmp as $filterPeriodKey => $filterPeriodValue)
//    {
//        $arResult["filterPeriodType"][$filterPeriodValue["0"]] = $filterPeriodValue["1"];
//    }
//}
if(!empty($arFilterPeriodType)){
    unset($arFilterPeriodType['B']);
    $arResult["filterPeriodType"] = $arFilterPeriodType;
}