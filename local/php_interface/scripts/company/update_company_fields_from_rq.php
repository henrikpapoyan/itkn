<?php
    /**
    Тип запуска: командная строка

    Скрипт производит актуализацию полей ИНН и КПП во всех компаниях битрикс.
    Список полей актуализации: UF_CRM_INN, UF_CRM_KPP

     */

    include_once(__DIR__ . "/../../.config.php");
    require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

    if ( ! CModule::IncludeModule('crm')) {
        return;
    }

    global $USER;
    $USER->Authorize(372);

    $strSql = "SELECT
				b_crm_company.ID,
				b_crm_company.TITLE,
				b_crm_requisite.RQ_INN,
				b_crm_requisite.RQ_KPP,
				b_crm_requisite.ENTITY_ID,	
				b_uts_crm_company.UF_CRM_INN,
				b_uts_crm_company.UF_CRM_KPP
			  FROM b_crm_company 
			  LEFT JOIN b_crm_requisite ON b_crm_company.ID = b_crm_requisite.ENTITY_ID
			  LEFT JOIN b_uts_crm_company ON b_crm_company.ID = b_uts_crm_company.VALUE_ID	
				ORDER BY b_crm_company.ID ASC";

    global $DB;
    $resCompany = $DB->Query($strSql, false, $err_mess . __LINE__);

    $arResult = [];
    while ($arCompany = $resCompany->Fetch()) {
        $arResult[] = array(
            "ID"         => $arCompany['ID'],
            "RQ_INN"     => $arCompany['RQ_INN'],
            "RQ_KPP"     => $arCompany['RQ_KPP'],
            "UF_CRM_INN" => $arCompany['UF_CRM_INN'],
            "UF_CRM_KPP" => $arCompany['UF_CRM_KPP'],
            "NAME"       => $arCompany['TITLE'],
        );
    }

    $CCrmCompany = new CCrmCompany();

    //$arResult = array_slice($arResult,0,5000);

    $START_TIME = mktime();
    $i = 0;
    foreach ($arResult as $k => $v) {
        if ((empty($v["RQ_INN"]) && empty($v["RQ_KPP"]) ||
             ($v["UF_CRM_INN"] == $v["RQ_INN"] && $v["UF_CRM_KPP"] == $v["RQ_KPP"]))
        ) {
            continue;
        }

        SetUserField("CRM_COMPANY", $v["ID"], "UF_CRM_INN", $v["RQ_INN"]);
        SetUserField("CRM_COMPANY", $v["ID"], "UF_CRM_KPP", $v["RQ_KPP"]);

        $i++;
    }

    $TIME = (mktime() - $START_TIME);
    echo "\n\nВремя обработки, сек: $TIME \n\n";
    echo "\n\nОбновлено компаний: $i \n\n";