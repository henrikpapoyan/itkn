<?php
/**
 Тип запуска: командная строка

 Скрипт производит актуализацию полей, по состоянию л/с, в контрагенте.
 Список полей актуализации: UF_LAST_OP_DATA, UF_LAST_OP_TYPE,UF_ACCOUNT_STATE,UF_LAST_OP_SUMM

 Сценарий работы:
 1. Берем компанию
 2. Поиск последней записи в инфоблоке "Реестр платежей"
 3. Берем информацию по интересующим нам поля и производим Update соответствующих полей в контрагенте.
 */
include_once(__DIR__ . "/../../.config.php");

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('CHK_EVENT', true);
ini_set('memory_limit', '-1');

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

@set_time_limit(0);
@ignore_user_abort(true);

global $USER;
$USER->Authorize(372);

CModule::IncludeModule('crm');
CModule::IncludeModule('iblock');

$oCompany   = new CCrmCompany();
$oIblockEl  = new CIBlockElement();

$START_TIME = mktime();

//Получение информации по всем элементам Реестра операция по л/с
$arAllReestrPay = array();

$dbCurrentOperationFields = CIBlockElement::GetList(
    array('DATE_CREATE' => 'ASC'),
    array("IBLOCK_CODE" => "reestr_pay"),
    false,
    false,
    array(
        "ID",
        "PROPERTY_ACTIVATION_DATE",
        "PROPERTY_COMPANY",
        "PROPERTY_LAST",
        "PROPERTY_TYPE",
        "PROPERTY_SUMM",
        "PROPERTY_SOSTOYANIE"
    )
);

while ($obElement = $dbCurrentOperationFields->GetNextElement()) {
    $arElement = $obElement->GetFields();

    if( empty($arElement['PROPERTY_COMPANY_VALUE']) ){
        continue;
    }

    $arAllReestrPay[$arElement['PROPERTY_COMPANY_VALUE']] = array(
        "UF_LAST_OP_DATA"  => $arElement["PROPERTY_ACTIVATION_DATE_VALUE"],
        "UF_LAST_OP_TYPE"  => $arElement["PROPERTY_TYPE_VALUE"],
        "UF_ACCOUNT_STATE" => $arElement["PROPERTY_SOSTOYANIE_VALUE"],
        "UF_LAST_OP_SUMM"  => $arElement["PROPERTY_SUMM_VALUE"]
    );
}

//Получение информации по всем компаниям
$resCompany = $oCompany->GetList(array("ID" => "ASC"), array(), false, false, array("ID"));
while ($fieldsCompany = $resCompany->Fetch()) {

    if( !empty($arAllReestrPay[$fieldsCompany['ID']]) ){
        $oCompany->Update(
            $fieldsCompany['ID'],
            $arAllReestrPay[$fieldsCompany['ID']],
            true,
            true,
            array('DISABLE_USER_FIELD_CHECK' => true)
        );
    }
}

$TIME = (mktime() - $START_TIME);
echo "\n\nВремя обработки, сек: $TIME \n\n";