<?php
/*
 * Запуск из командной строки
 * Выполняет замену Ответственного на Родионову Тамару в Контрагентах на основание данных из CSV
 */

include_once(__DIR__ . "/../../.config.php");

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('CHK_EVENT', true);
define('NEW_ASSIGNED_ID', 750); //Родионова	Тамара	Александровна
ini_set('memory_limit', '-1');

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
$requisite = new \Bitrix\Crm\EntityRequisite();
$CCrmCompany = new CCrmCompany();

$row = 0;
$arResult = [];
if (($handle = fopen(__DIR__."/replacement_responsibility_company.csv", "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ";", '"')) !== FALSE) {
        $searchedReqCount = 0;
        $inn = $data[0];
        $rsReq = $requisite->getList(
            array(
                "filter" => array("RQ_INN" => $inn),
            )
        );
        while( $arReq = $rsReq->Fetch() ){
            $searchedReqCount++;
            $arResult["SEARCHED"][$inn][] = $arReq["ENTITY_ID"];
        }
        if( $searchedReqCount == 0 ) {
            $arResult["NOT_SEARCHED"][] = $inn;
        }
        $row++;
    }
    pre($arResult);
    fclose($handle);
    $updateCompanyCount = 0;
    foreach($arResult["SEARCHED"] as $inn => $arCompanyIDs) {
        foreach($arCompanyIDs as $companyID) {
            $arFields = array("ASSIGNED_BY_ID" => NEW_ASSIGNED_ID);
            $bUpdateResult = $CCrmCompany->Update($companyID, $arFields);
            if ( !$bUpdateResult ) {
                echo "<pre>";
                var_dump($CCrmCompany->LAST_ERROR);
                echo "</pre>";
            } else {
                echo "Успешно обновлен ответственный по компании [$companyID]<br />";
                $updateCompanyCount++;
            }
        }
    }
    echo "Успешно обновлен ответственный по $updateCompanyCount компаниям<br />";
}
?>