<?php
/**
 Тип запуска: командная строка

 Скрипт очищает значение поля "UF_ID_PROD" для всех контрагентов в системе.

 Сценарий работы:
 1. Берем все компании
 2. Очистка значения поля "UF_ID_PROD
 */
include_once(__DIR__ . "/../../.config.php");

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('CHK_EVENT', true);
ini_set('memory_limit', '-1');

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

@set_time_limit(0);
@ignore_user_abort(true);

global $USER;
$USER->Authorize(372);

CModule::IncludeModule('crm');
$oCompany   = new CCrmCompany();

$START_TIME = mktime();

//Получение информации по всем компаниям
$resCompany = $oCompany->GetList(array("ID" => "ASC"), array(), false, false, array("ID"));
var_dump($resCompany->SelectedRowsCount());
while ($fieldsCompany = $resCompany->Fetch()) {
    SetUserField("CRM_COMPANY", $fieldsCompany['ID'], "UF_ID_PROD", "");
}

$TIME = (mktime() - $START_TIME);
echo "\n\nВремя обработки, сек: $TIME \n\n";