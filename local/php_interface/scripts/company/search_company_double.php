<?php
/*
 * Запуск из командной строки
 * Выполняет поиск компаний с одинаковым UF_LINK
 */

include_once(__DIR__ . "/../../.config.php");

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('CHK_EVENT', true);
ini_set('memory_limit', '-1');

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

global $USER;
$USER->Authorize(372);
$requisite = new \Bitrix\Crm\EntityRequisite();
$CCrmCompany = new CCrmCompany();
$rsCompany = $CCrmCompany->GetList(
    array("ID" => "DESC"),
    array("!UF_LINK" => false)
);
$arUfLink = [];
$arDouble = [];
echo "Найдено компаний с заполненным UF_LINK, шт.: ".$rsCompany->SelectedRowsCount()." \n\r";
while($arCompany = $rsCompany->Fetch()){
    $xmlID = $arCompany["UF_LINK"];
    $companyID = $arCompany["ID"];
    if($xmlID == "378#anonymous_EekAmlZjw# Анонимный пользователь ")
        continue;

    $arUfLink[$xmlID][$companyID] = $arCompany;
}

$fpDouble = fopen(__DIR__."/company_double.csv", 'w');
$doubleCount = 0;
fputcsv($fpDouble, array("XML_ID", "COMPANY_ID", "COMPANY_TITLE", "RQ_INN", "RQ_KPP"), ';', '"');
foreach( $arUfLink as $xmlID => $arCompanys ) {
    if( sizeof($arCompanys) > 1 ) {
        foreach( $arCompanys as $companyID => $arCompany) {
            $rsReq = $requisite->getList([
                "filter" => ["ENTITY_ID" => $companyID, "ENTITY_TYPE_ID" => CCrmOwnerType::Company,
                ]
            ]);
            if( $arReq = $rsReq->Fetch() ){
                $arDouble[$xmlID][$companyID] = array(
                    "TITLE" => $arCompany["TITLE"],
                    "RQ_INN" => $arReq["RQ_INN"],
                    "RQ_KPP" => $arReq["RQ_KPP"],
                );
                fputcsv($fpDouble, array($xmlID, $companyID, $arCompany["TITLE"], $arReq["RQ_INN"], $arReq["RQ_KPP"]), ';', '"');
                $doubleCount++;
            }
        }
        fputcsv($fpDouble, array("", "", "", "", ""), ';', '"');
    }
}
fclose($fpDouble);

echo "Найдено дублей компаний по UF_LINK, шт: ".($doubleCount)." \n\r";