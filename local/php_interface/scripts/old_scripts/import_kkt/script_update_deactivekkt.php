<?
/*
 * Выбирает элементы из ИБ с кодом “registry_kkt” удовлетворяющие след. условиям:
 * $arFilter = Array(  "<=PROPERTY_DATE_OFF_CSV" => $dateNow->format('Y-m-d H:i:s'),  "!=PROPERTY_ACTIVE" => "1305")
 * и устанавливает для найденных элементов свойство “ACTIVE” в значение “AC_DEACTIVE”
 */

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
ini_set('memory_limit', '-1');

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");
global $USER;
global $DB;
$USER->Authorize(372);
global $DB;

$dateNow = new DateTime('now', new DateTimeZone('UTC'));
//$dateNow->modify('-31 day');



$START_TIME_ONE = mktime();
$IBLOCK_ID = getIblockIDByCode('registry_kkt');
echo date($DB->DateFormatToPHP(CLang::GetDateFormat("SHORT")), time());
$arFilter = Array(
    "<=PROPERTY_DATE_OFF_CSV" => $dateNow->format('Y-m-d H:i:s'),
	"!=PROPERTY_ACTIVE" => "1305",
    "IBLOCK_ID" => $IBLOCK_ID,
);

$oIBlockElement = new CIBlockElement();
$IBLOCK_ID = getIblockIDByCode('registry_kkt');
$arSelect = Array("ID", "IBLOCK_ID", "PROPERTY_RNM_KKT", "PROPERTY_COST_TARIF", "PROPERTY_DEAL", "PROPERTY_NUM_KKT", "PROPERTY_COMPANY");
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
//echo $res->SelectedRowsCount();

while ($arKKT = $res->Fetch()) {
    //echo $arKKT["ID"];
    $arProperty = [];
    $arProperty['ACTIVE'] = getPropertyEnumIdByXmlId($IBLOCK_ID, "AC_DEACTIVE");
    $oIBlockElement->SetPropertyValuesEx(
        $arKKT["ID"],
        $IBLOCK_ID,
        $arProperty
    );
	//break;
}


$TIME_ONE = mktime() - $START_TIME_ONE;
echo "ОБработка ККТ , сек: $TIME_ONE \n";

?>