<?php
/*
 * Выбирает данные из ИБ с ID = 76 по условию:
 * "=PROPERTY_ACTIVE_VALUE"=>false
 * и устанавливает свойство array("ACTIVE" => "1304") для найденных элементов
 */

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
define('CHK_EVENT', true);
ini_set('memory_limit', '-1');
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

@set_time_limit(0);
@ignore_user_abort(true);
global $USER;
$USER->Authorize(372);
CModule::IncludeModule('crm');
CModule::IncludeModule('iblock');

$start = microtime(true);
$iblockId = "76";
$arFilter = Array("IBLOCK_ID"=>IntVal($iblockId),"=PROPERTY_ACTIVE_VALUE"=>false);
$arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM","PROPERTY_*");
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
while($ob = $res->GetNextElement()){
    $arFields = $ob->GetFields();
    //$arProps = $ob->GetProperties();
	//print_r($arFields['ID']);
	//print_r($arProps['ACTIVE']);
	CIBlockElement::SetPropertyValuesEx($arFields['ID'], "76", array("ACTIVE" => "1304"));
	//break;	

}
$time = microtime(true) - $start;
printf('Скрипт выполнялся %.4F сек.', $time);
