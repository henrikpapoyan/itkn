<?php
/*
 * Обновляет свойство “STATUS” для элементов ИБ “import_company”
 */

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
define('CHK_EVENT', true);
ini_set('memory_limit', '-1');
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

@set_time_limit(0);
@ignore_user_abort(true);
global $USER;
$USER->Authorize(372);
CModule::IncludeModule('crm');
CModule::IncludeModule('iblock');

$offset = 0;
if (array_key_exists(1, $argv)) {
    $offset = $argv[1];
}

$oIBlockElement = new CIBlockElement;
$IBLOCK_ID_CSV = getIblockIDByCode('import_company');

$START_TIME = mktime();
$n = 0;
while ($n < 300) {

    //Проверяем время. Если время работы превышает 55 сек - делаем break.
    $passedTime = (mktime() - $START_TIME);
    if($passedTime > 55) {
        break;
    }

    $arResult = getOneElement($offset);
    echo "\n--- $arResult[ID] ---\n";
    if ($arResult === false) {
        break;
    }

    if (!empty($arResult['RNM_KKT'])) {
		$res = parserRow($arResult);
        if ($res === false) {
            $oIBlockElement->SetPropertyValueCode($arResult['ID'], "STATUS", 3); //Устанавливаем статус 3 - ошибка
        } elseif($res === "not_kkt") {
            $oIBlockElement->SetPropertyValueCode($arResult['ID'], "STATUS", 5); //Устанавливаем статус 5 - ККТ не найден в Битрикс
        } else {
			$oIBlockElement->SetPropertyValueCode($arResult['ID'], "STATUS", 2); //Устанавливаем статус 2 - успех
		}
    } else {
        $oIBlockElement->SetPropertyValueCode($arResult['ID'], "STATUS", 4); //Устанавливаем статус 4 - пустой ККТ в CSV
    }
    $n++;
}
$TIME = (mktime() - $START_TIME);
echo "\n\nВремя обработки, сек: $TIME \n\n";
echo "\n\nКол-во обработанных элементов: $n \n\n";

function getOneElement($offset = 1)
{
    $oIBlockElement = new CIBlockElement;
    $IBLOCK_ID_CSV = getIblockIDByCode('import_company');

    $arFilter = array(
        "IBLOCK_ID" => $IBLOCK_ID_CSV,
        "PROPERTY_STATUS" => 0 // берем записи со статусом 0 - не обработан
    );

    $res = $oIBlockElement->GetList(array(), $arFilter, false, array('iNumPage' => $offset, 'nPageSize' => 1));

    if ($res->SelectedRowsCount() <= $offset) {
        return false;
    }

    if ($ob = $res->GetNextElement()) {
        $arElementFields = $ob->GetFields();
        $oIBlockElement->SetPropertyValueCode($arElementFields['ID'], "STATUS", 1); //устанавливаем статус 1 - в работе
        $arElement = $ob->GetProperties();
        $arResult = [];
        $arResult['ID'] = $arElementFields['ID'];
        $arResult['ID_BPM'] = $arElement['ID_BPM']['VALUE'];
        $arResult['INN'] = $arElement['INN']['VALUE'];
        $arResult['KPP'] = $arElement['KPP']['VALUE'];
        $arResult['COMPANY_TYPE'] = $arElement['COMPANY_TYPE']['VALUE']; //TID, Тенант, тип контрагента
        $arResult['NAME_KKT'] = $arElement['NAME_KKT']['VALUE']; //EXTERNAL_CODE, имя ККТ
        $arResult['DATE_CREATE'] = $arElement['DATE_CREATE']['VALUE']; //created_at, Дата добавления ККТ
        $arResult['MODEL'] = $arElement['MODEL']['VALUE'];
        $arResult['NUM_KKT'] = $arElement['NUM_KKT']['VALUE']; //serial_number, Заводской номер ККТ
        $arResult['RNM_KKT'] = $arElement['RNM_KKT']['VALUE']; //fns_ecr_id, РНМ, RegID
        $arResult['NUM_FN'] = $arElement['NUM_FN']['VALUE']; //fiscal_drive_number, Номер ФН
        $arResult['ADDRESS_KKT'] = $arElement['ADDRESS_KKT']['VALUE']; //full_address, Адрес ККТ
        $arResult['NAME_TARIFF'] = $arElement['NAME_TARIFF']['VALUE']; //name_tariff , Тариф
        $arResult['COST_TARIFF'] = $arElement['COST_TARIFF']['VALUE']; //full_period_price, Стоимость тарифа
        $arResult['DATE_FROM'] = $arElement['DATE_FROM']['VALUE']; //from_date, Дата начала тарифа
        $arResult['DATE_TO'] = $arElement['DATE_TO']['VALUE']; //to_date, Дата завершения тарифа
        $arResult['CODE_AGENT'] = $arElement['CODE_AGENT']['VALUE']; //agent_code, Код агента
        $arResult['PROMO_KOD'] = $arElement['PROMO_KOD']['VALUE']; //promo_code, ID Промокода
        $arResult['ACCOUNT_ID'] = $arElement['ACCOUNT_ID']['VALUE']; //promo_code, ID Промокода
        $arResult['BALANCE'] = $arElement['BALANCE']['VALUE']; //promo_code, ID Промокода
        $arResult['BALANCE'] = str_replace(",", ".", $arResult['BALANCE']);
        $arResult['ASSIGNED_LAST_NAME'] = $arElement['ASSIGNED_LAST_NAME']['VALUE']; //promo_code, ID Промокода
        $arResult['EMAIL'] = $arElement['EMAIL']['VALUE']; //responsible_person_email, Email
        $arResult['PHONE'] = $arElement['PHONE']['VALUE']; //contact_phone, Телефон
        $arResult['EMAIL'] = (empty($arResult['EMAIL'])) ? "" : $arResult['EMAIL'];
        $arResult['EMAIL'] = ($arResult['EMAIL'] == "null") ? "" : $arResult['EMAIL'];
        if (check_email($arResult['EMAIL']) === false) {
            $arResult['EMAIL'] = "";
        }
        $arResult['PHONE'] = (empty($arResult['PHONE'])) ? "" : $arResult['PHONE'];
        $arResult['PHONE'] = ($arResult['PHONE'] == "null") ? "" : $arResult['PHONE'];
        return $arResult;
    }
    return false;
}


function parserRow($arParams)
{
    $oIBlockElement = new CIBlockElement;
    $IBLOCK_ID_KKT = getIblockIDByCode('registry_kkt');
    $arKKTID = findDublicateKKT($arParams['RNM_KKT']);
    $companyID = "-1";
    $res = CCrmCompany::GetList(array(), array("CHECK_PERMISSIONS" => "N", "=UF_ID_PROD" => $arParams['ID_BPM']));
    if ($arRes = $res->Fetch()) {
        $companyID = $arRes['ID'];
    }

    if (!empty($arKKTID)) {
		$arLoadProductArray = Array(
			"NAME" => $arParams['NAME_KKT'] . ' Модель (' . $arParams['MODEL'] . ') C\Н (' . $arParams['NUM_KKT'] . ') Р\Н (' . $arParams['RNM_KKT'] . ')',
        );
        $el = new CIBlockElement;
        $res = $el->Update($arKKTID, $arLoadProductArray);
		
		$arProperty = array(
			"MODEL_KKT" => $arParams['MODEL'],
			"NUM_KKT" => $arParams['NUM_KKT'],
			"NUM_FN" => $arParams['NUM_FN'],
			"ADDR_KKT" => $arParams['ADDRESS_KKT'],
			"TARIF" => $arParams['NAME_TARIFF'],
			"COST_TARIF" => $arParams['COST_TARIFF'],
			"NAIMENOVANIE_KKT" => $arParams['NAME_KKT'],
			"CODE_AGENT" => $arParams['CODE_AGENT'],
			"PROMO_KOD" => $arParams['PROMO_KOD'],
			"DATE_CONNECT" => (!empty($arParams['DATE_CREATE'])) ? $arParams['DATE_CREATE'] : "",
			"DATE_ACTIVE" => (!empty($arParams['DATE_FROM'])) ? $arParams['DATE_FROM'] : "",
			"DATE_OFF" => (!empty($arParams['DATE_TO'])) ? $arParams['DATE_TO'] : "",			
		);
		
		if ($companyID != "-1") {
			$arProperty["COMPANY"] = $companyID;          
        }
		
		CIBlockElement::SetPropertyValuesEx($arKKTID, $IBLOCK_ID_KKT, $arProperty);
        
		return true;
    } else {
		return "not_kkt";//ККТ не найден в BD Bitrix 
	}
	
    return false;
}

//Поиск дубликата ККТ
function findDublicateKKT($kkmRegId)
{
    if ($kkmRegId) {
        $IBLOCK_ID_KKT = getIblockIDByCode('registry_kkt');
        $arSelect = Array("ID", "IBLOCK_ID", "PROPERTY_RNM_KKT", "PROPERTY_COST_TARIF", "PROPERTY_DEAL");
        $arFilter = array("IBLOCK_ID" => $IBLOCK_ID_KKT, "PROPERTY_RNM_KKT" => $kkmRegId);
        $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
        if ($arKKT = $res->Fetch()) {
            return $arKKT['ID'];
        }
    }

    return false;
}
