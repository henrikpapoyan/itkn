<?php
/*
 * Ищет все счета по которым ответственный пользователь c ID 372,
 * по счету определяется контрагент, к которому относится этот счет.
 * В случае если найдена компания и для этой компании установлен ответственный пользователь не с ID = 372,
 * то для счета устанавливается найденный ответственный
 */



define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
define('CHK_EVENT', true);
ini_set('memory_limit', '-1');

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

@set_time_limit(0);
@ignore_user_abort(true);
global $USER;
$USER->Authorize(372);
CModule::IncludeModule('crm');

$START_TIME = mktime();
$UserID = 372;
$oCrmInvoice = new CCrmInvoice;
$dbInvoice = $oCrmInvoice->GetList(array('ID' => 'DESC'), array('RESPONSIBLE_ID' => $UserID,), false, false, array('ID', 'RESPONSIBLE_ID', 'UF_COMPANY_ID'));
$countinvoice = 0;
while ($arInvoice = $dbInvoice->Fetch()) {
	if(!empty($arInvoice['UF_COMPANY_ID'])) {
		$resultcompany = CCrmCompany::GetByID($arInvoice['UF_COMPANY_ID']);
		$newUserIDCompany = $resultcompany['ASSIGNED_BY_ID'];
		if(!empty($newUserIDCompany) && ($UserID != $newUserIDCompany) ){					
			$resUp = $oCrmInvoice->Update($arInvoice['ID'], array("RESPONSIBLE_ID" => $newUserIDCompany));			
		}
	}
}

$TIME = (mktime() - $START_TIME) / 60;
echo "Время обработки, мин: $TIME \n\n";






/*//Неотсортированные остатки.
$arFilter = array("=UF_CHANNEL" => '');
$arSelect = array('ID', 'UF_CHANNEL', 'SOURCE_ID');
$CCrmLead = new CCrmLead();
$rsLead = $CCrmLead->GetList(array('ID' => 'ASC'), $arFilter, $arSelect);
while ($arLead = $rsLead->Fetch()) {
    pre($arLead);
}*/
