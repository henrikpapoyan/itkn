<?php
/*
 * Выполняет обновление свойств для ИБ-ов “Реестр ККТ” и “Реестр договоров”
 * полей с привязкой к сделки и/или компании,
 * у которых в данном поле используется значение с “.0000”.
 */

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
ini_set('memory_limit', '-1');

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");
global $USER;
global $DB;
$USER->Authorize(372);
global $DB;

//Обновление данных полей для ИБ "Реестр ККТ"
$kktIBlockID = GetIBlockIDByCode("registry_kkt");
$strSql = "SELECT
                  ID, CODE
                FROM
                  b_iblock_property
                WHERE
                  (CODE = 'COMPANY' OR CODE = 'DEAL') AND IBLOCK_ID=$kktIBlockID
            ";
$START_TIME_ONE = mktime();
$rsProp = $DB->Query($strSql, false, $err_mess.__LINE__);
$TIME_ONE = mktime() - $START_TIME_ONE;
echo "\n\rЗапрос свойств ИБ ККТ , сек: $TIME_ONE \n";
while($arProp = $rsProp->Fetch()){
    if($arProp["CODE"] == "COMPANY"){
        $propCompanyID = $arProp["ID"];
    }
    if($arProp["CODE"] == "DEAL"){
        $propDEALID = $arProp["ID"];
    }
}
echo '$propCompanyID = '.$propCompanyID.' | $propDEALID = '.$propDEALID.'<br />';
if($propCompanyID > 0 && $propDEALID > 0){
    $strSql = "SELECT
                    b_iblock_element.ID,
                    b_iblock_element_prop_s$kktIBlockID.PROPERTY_$propCompanyID,
                    b_iblock_element_prop_s$kktIBlockID.PROPERTY_$propDEALID
                  FROM
                    b_iblock_element,
                    b_iblock_element_prop_s$kktIBlockID
                  WHERE
                    b_iblock_element.IBLOCK_ID=$kktIBlockID AND
                    b_iblock_element_prop_s$kktIBlockID.IBLOCK_ELEMENT_ID=b_iblock_element.ID
                  ORDER BY b_iblock_element.ID DESC";
    $START_TIME_ONE = mktime();
    $rsKKT = $DB->Query($strSql, false, $err_mess.__LINE__);
    $TIME_ONE = mktime() - $START_TIME_ONE;
    echo "\n\rЗапрос ККТ SQL, сек: $TIME_ONE \n";
    $arKKTS = array();
    $selKKTCount = $rsKKT->SelectedRowsCount();
    echo "rsKKT->SelectedRowsCount() = ".$rsKKT->SelectedRowsCount()."\n\r";
    $START_TIME_ONE = mktime();
    $updCount = 0;
    while($arKKT = $rsKKT->Fetch()){
        $updateFields = '';
        if(strpos($arKKT["PROPERTY_$propCompanyID"], ".0000")){
            $updateFields = "PROPERTY_$propCompanyID = ".intval($arKKT["PROPERTY_$propCompanyID"]);
        }
        if(strpos($arKKT["PROPERTY_$propDEALID"], ".0000")) {
            if($updateFields==''){
                $updateFields = "PROPERTY_$propDEALID = ".intval($arKKT["PROPERTY_$propDEALID"]);
            } else {
                $updateFields .= ", PROPERTY_$propDEALID = ".intval($arKKT["PROPERTY_$propDEALID"]);
            }
        }
        if($updateFields != ''){
            $updSql = "UPDATE  b_iblock_element_prop_s$kktIBlockID SET $updateFields WHERE IBLOCK_ELEMENT_ID = ".$arKKT["ID"];
            //echo "updSql = ".$updSql."\n\r";
            $rsUpd = $DB->Query($updSql, false, $err_mess.__LINE__);
            $updCount++;
        }
    }
    $TIME_ONE = mktime() - $START_TIME_ONE;
    echo "\n\rОБработка $selKKTCount ККТ изменено записей $updCount, сек: $TIME_ONE \n";
}

//Обновление данных полей для ИБ "Реестр договоров"
$kktIBlockID = GetIBlockIDByCode("registry_contracts");
$strSql = "SELECT
                  ID, CODE
                FROM
                  b_iblock_property
                WHERE
                  (CODE = 'KONTRAGENT') AND IBLOCK_ID=$kktIBlockID
            ";
$START_TIME_ONE = mktime();
$rsProp = $DB->Query($strSql, false, $err_mess.__LINE__);
$TIME_ONE = mktime() - $START_TIME_ONE;
echo "\n\rЗапрос свойств ИБ ККТ , сек: $TIME_ONE \n";
while($arProp = $rsProp->Fetch()){
    if($arProp["CODE"] == "KONTRAGENT"){
        $propCompanyID = $arProp["ID"];
    }
}
echo '$propCompanyID = '.$propCompanyID.'<br />';
if($propCompanyID > 0){
    $strSql = "SELECT
                    b_iblock_element.ID,
                    b_iblock_element_prop_s$kktIBlockID.PROPERTY_$propCompanyID
                  FROM
                    b_iblock_element,
                    b_iblock_element_prop_s$kktIBlockID
                  WHERE
                    b_iblock_element.IBLOCK_ID=$kktIBlockID AND
                    b_iblock_element_prop_s$kktIBlockID.IBLOCK_ELEMENT_ID=b_iblock_element.ID
                  ORDER BY b_iblock_element.ID DESC";
    $START_TIME_ONE = mktime();
    $rsKKT = $DB->Query($strSql, false, $err_mess.__LINE__);
    $TIME_ONE = mktime() - $START_TIME_ONE;
    echo "\n\rЗапрос Договоров SQL, сек: $TIME_ONE \n";
    $arKKTS = array();
    $selKKTCount = $rsKKT->SelectedRowsCount();
    echo "rsKKT->SelectedRowsCount() = ".$rsKKT->SelectedRowsCount()."\n\r";
    $START_TIME_ONE = mktime();
    $updCount = 0;
    while($arKKT = $rsKKT->Fetch()){
        $updateFields = '';
        if(strpos($arKKT["PROPERTY_$propCompanyID"], ".0000")){
            $updateFields = "PROPERTY_$propCompanyID = ".intval($arKKT["PROPERTY_$propCompanyID"]);
        }
        if($updateFields != ''){
            $updSql = "UPDATE  b_iblock_element_prop_s$kktIBlockID SET $updateFields WHERE IBLOCK_ELEMENT_ID = ".$arKKT["ID"];
            //echo "updSql = ".$updSql."\n\r";
            $rsUpd = $DB->Query($updSql, false, $err_mess.__LINE__);
            $updCount++;
        }
    }
    $TIME_ONE = mktime() - $START_TIME_ONE;
    echo "\n\rОБработка $selKKTCount Реестр договоров изменено записей $updCount, сек: $TIME_ONE \n";
}
?>