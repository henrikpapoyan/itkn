<?
/*
 * Выполняет обновление информации по счетам.
 * Данные для сравнения счетов в Битрикс берутся из schet.csv
 */


require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule('sale');
CModule::IncludeModule('iblock');
global $USER_FIELD_MANAGER;
$row = 0;


$arUserProp = getUserPropArray('XML_ID', 'UF_INVOICE_SUMM_FROM_1C');
$fieldName = $arUserProp["FIELD_NAME"];
//pre($arUserProp);

/*$arFilter = Array(
    $fieldName => false,
);
$db_sales = CSaleOrder::GetList(array("DATE_INSERT" => "ASC"), $arFilter, false, false, array("ID", "ACCOUNT_NUMBER", "UF_CRM_1514198556", "PRICE"));
$showCount = 0;
$neededCount = $db_sales->SelectedRowsCount();
echo 'SelectedRowsCount() = '.$neededCount.'<br />';
while ($ar_sales = $db_sales->Fetch())
{
    $invoiceSummFrom1c = $ar_sales["PRICE"];
    $USER_FIELD_MANAGER->Update(
        'ORDER',
        $ar_sales["ID"],
        array(
            $fieldName => $invoiceSummFrom1c
        )
    );
    $showCount++;
}
echo date("d.m.Y H:i:s").'<br />';
echo "Всего счетов с незаполненным полем 'Сумма счета из 1с' (или равным 0) выбрано и обработано: $neededCount <br />";*/


/*$showCount = 0;
$db_sales = CSaleOrder::GetList(
    array("ID" => "DESC"),
    array("ID" => '1742'),
    false,
    false,
    array("ID", "ACCOUNT_NUMBER", "DATE_BILL", "UF_CRM_1514198556", "PRICE")
);
while ($ar_sales = $db_sales->Fetch())
{
    if($showCount<5){
        pre($ar_sales);
    }
    $showCount++;
}*/


$updateIndex = 0;
$errorIndex = 0;
$notYear = 0;

if (($fp = fopen($_SERVER["DOCUMENT_ROOT"]."/local/php_interface/scripts/sync_invoice_from_1c/syncLogNotConcurenceInvoice.csv", 'w')) === FALSE) {
    echo "Ошибка открытия файла для записи ошибок синхронизации /n/r";
}

//Формируем массив с инфой о заказе.
$db_sales = CSaleOrder::GetList(
    array("ID" => "ASC"),
    array(),
    false,
    false,
    array("ID", "ACCOUNT_NUMBER", "DATE_BILL", "UF_CRM_1514198556", "PRICE")
);
$arOrders = array();
//echo 'SelectedRowsCount() = '.$db_sales->SelectedRowsCount().'<br />';
while($arOrderNew = $db_sales->Fetch()){
    $arTemp = explode(' (', $arOrderNew['ACCOUNT_NUMBER']);
    $accountNumber = trim($arTemp[0]);
    if(!empty($accountNumber) && !empty($arOrderNew["DATE_BILL"])) {
        $arDate = explode(".", $arOrderNew["DATE_BILL"]);
        $arOrders[$accountNumber][$arDate[2]] = $arOrderNew["ID"];
    }
}

//pre($arOrders);
//echo '$arOrders sizeOf = '.sizeOf($arOrders).'<br />';
$showErrText = false;
if (($handle = fopen($_SERVER["DOCUMENT_ROOT"]."/local/php_interface/scripts/sync_invoice_from_1c/schet.csv", "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 10000, ";")) !== FALSE) {
        $num = count($data);
        $arDateIssue = explode(" ", $data[1]);
        $arDateIssue = explode(".", $arDateIssue[0]);
        $dateIssue = $arDateIssue[2]; //Год выставления
        $orderID = intval($data[7]); //ID Заказа
        $nomerFrom1c = trim($data[0]); //ACCOUNT_NUMBER
        $invoiceSummFrom1c = $data[6]; //Сумма счета
        if(empty($orderID) && !empty($nomerFrom1c)) {
            $orderID = $arOrders[$nomerFrom1c][$dateIssue];
        }
        if(!empty($orderID)){
            if(!empty($arUserProp)) {
                $USER_FIELD_MANAGER->Update(
                    'ORDER',
                    $orderID,
                    array(
                        $fieldName => $invoiceSummFrom1c
                    )
                );
                $updateIndex++;
            }
        } else {
            if(!$showErrText){
                echo "Ну удалось определить ID счетов Битрикс24 по данным из 1с!<br />";
                $showErrText = true;
            }
            $errorIndex++;
            echo "$errorIndex. № счета = '$nomerFrom1c', ID счета = '$orderID', Год выставления = $dateIssue ($data[1]) <br />";
            if(isset($arOrders[$nomerFrom1c])){
                echo "<b>$errorIndex.1.</b> № счета = '$nomerFrom1c' НЕ найдено совпадение по ГОДУ в массиве arOrders<br />";
                $notYear++;
            }
             //pre($data);
            fputcsv($fp, $data, ';');
        }
        $row++;
    }
    fclose($fp);
    echo date("d.m.Y H:i:s").'<br />';
    echo "Всего записей: $row <br />";
    echo "Всего записей обновлено: $updateIndex <br />";
    echo "Всего не найдено совпадений: $errorIndex <br />";
    echo "Из них не найдено совпадений по году: $notYear <br />";
    fclose($handle);
}
?>