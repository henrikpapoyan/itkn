<?php
/*
 * Перебирает лиды со значением 5276, 5275, 3336 в пользовательском свойстве ‘UF_CHANNEL’.
 * Для найденных лидов проверяется заполненность поля ‘UF_AUTO’.
 * В случае если данное поле не указано, устанавливает 'UF_AUTO' => 'N'.
 * Если у лида поле ‘STATUS_ID’== 'NEW', то  ‘STATUS_ID’ устанавливается в значение '4'
 */



define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('CHK_EVENT', true);
ini_set('memory_limit', '-1');

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

@set_time_limit(0);
@ignore_user_abort(true);

global $USER;
$USER->Authorize(372);

CModule::IncludeModule('crm');

$arFilter = array(
    'UF_CHANNEL'    => array(5276, 5275, 3336),
);

$CCrmLead = new CCrmLead();
$rsLead = $CCrmLead->GetList(array('ID' => 'ASC'), $arFilter);

$n = 0;
$START_TIME = mktime();
while($arLead = $rsLead->Fetch() )
{
    $n++;	
    if(!$arLead['UF_AUTO'])
    {
        $arFields = array('UF_AUTO' => 'N');
        if ($arLead['STATUS_ID'] == 'NEW')
        {
            $arFields['STATUS_ID'] = '4';
        }
        $ret = $CCrmLead->Update($arLead['ID'], $arFields);
    }

    if ($n == 1)
    {
        //break;
    }
}

$TIME = (mktime() - $START_TIME) / 60;
echo "Общее кол-во строк: $n \n";
echo "Время обработки, мин: $TIME \n\n";