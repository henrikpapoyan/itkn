<?php
/*
 * Обновляет значение поля “STATUS_ID”. Выборка лидов происходит по полю “SOURCE_ID”
 */



define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('CHK_EVENT', true);
ini_set('memory_limit', '-1');

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

@set_time_limit(0);
@ignore_user_abort(true);

global $USER;
$USER->Authorize(372);

CModule::IncludeModule('crm');

$arFilter = array();
$arFilter['SOURCE_ID'] = gk_GetOrderFilter();

$CCrmLead = new CCrmLead();
$rsLead = $CCrmLead->GetList(array('ID' => 'ASC'), $arFilter);

//NEW заполнена форма Поп Ап
//ASSIGNED - заполнена форма регистрации в ЛК
//2 - заключен договор
//3 - добавлена ККТ
//1 - активирована ККТ

//4 - новый
//5 - в работе
//6 - консультация

$n = 0;
$START_TIME = mktime();
while($arLead = $rsLead->Fetch() )
{
    $n++;
	
	$arFields = array();
	
	if ($arLead['STATUS_ID'] == 'NEW')
	{
		$arFields['STATUS_ID'] = '4'; //
	}
	
	if ($arLead['STATUS_ID'] == 'ASSIGNED' || $arLead['STATUS_ID'] == '2')
	{
		$arFields['STATUS_ID'] = '5';
	}
	
	//Неуспешные статусы
	if ($arLead['STATUS_ID'] == 'ON_HOLD' || $arLead['STATUS_ID'] == 'RESTORED')
	{
		$arFields['STATUS_ID'] = 'JUNK'; //Дисквалифицирован
	}
	
    if (count($arFields) > 0)
    {			
		$ret = $CCrmLead->Update($arLead['ID'], $arFields);			
    }

    if ($n == 10)
    {
        //break;
    }
}

$TIME = (mktime() - $START_TIME) / 60;
echo "Общее кол-во строк: $n \n";
echo "Время обработки, мин: $TIME \n\n";
