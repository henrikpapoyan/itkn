<?php
/*
 * Обновляет пользовательское свойство лида “UF_CHANNEL” для лидов с
 * незаполненным данным свойством в зависимости от значения в “SOURCE_ID”
 */



define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
define('CHK_EVENT', true);
ini_set('memory_limit', '-1');

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

@set_time_limit(0);
@ignore_user_abort(true);
global $USER;
$USER->Authorize(372);
CModule::IncludeModule('crm');

function filter_One_Ofd()//!!5276
{
    return array(
        '36',    //Структурное подразделение
    );
}

function filter_Contact_Center()//!!5275
{
    return array(
        '35',    //Письмо на info
        '34',     //Входящий звонок
    );
}

function filter_Marketing()//!!3336
{
    return array(
'29',//Соцсети
'28',//Реклама
'30',     //E-mail рассылка
'31',     //СМИ
'9',     //CNEWS FORUM
'25',     //Поисковики
'26',     //Внешнее мероприятие
'27', //Внутреннее мероприятие
'32', //Рекомендация
'33' //Не распределено


    );
}

function filter_Site()//!!!5274
{
    return array(
        '24',//Форма регистрации на сайте 1-ofd
        '21',//Форма запроса на сайте 1-ofd
        '22',//Форма запроса ФН на сайте 1-ofd
        '23',//Форма запроса сертификата на сайте 1-ofd
        '37',//Форма покупки ККТ на сайте 1-ofd
        '38',//Форма запроса заказа доп. услуг на сайте 1-ofd
        '41',//Форма аренды ККТ на сайте 1-ofd
        '44',     //Форма запроса на анализ (Биг дата)
        '43',     //Форма заказа обратного звонка
        '42'        //Форма запроса информации в чате


    );
}


$START_TIME = mktime();
//UPDATE Контакт-центр
$arFilter = array("=UF_CHANNEL" => '');
$arFilter['SOURCE_ID'] = filter_Contact_Center();
$arSelect = array('ID', 'UF_CHANNEL', 'SOURCE_ID');
$CCrmLead = new CCrmLead();
$rsLead = $CCrmLead->GetList(array('ID' => 'ASC'), $arFilter, $arSelect);
while ($arLead = $rsLead->Fetch()) {	
    SetUserField("CRM_LEAD", $arLead['ID'], "UF_CHANNEL", "5275");
}

//UPDATE 1-OFD
$arFilter = array("=UF_CHANNEL" => '');
$arFilter['SOURCE_ID'] = filter_One_Ofd();
$arSelect = array('ID', 'UF_CHANNEL', 'SOURCE_ID');
$CCrmLead = new CCrmLead();
$rsLead = $CCrmLead->GetList(array('ID' => 'ASC'), $arFilter, $arSelect);
while ($arLead = $rsLead->Fetch()) {
    SetUserField("CRM_LEAD", $arLead['ID'], "UF_CHANNEL", "5276");
}

//UPDATE Маркетинг
$arFilter = array("=UF_CHANNEL" => '');
$arFilter['SOURCE_ID'] = filter_Marketing();
$arSelect = array('ID', 'UF_CHANNEL', 'SOURCE_ID');
$CCrmLead = new CCrmLead();
$rsLead = $CCrmLead->GetList(array('ID' => 'ASC'), $arFilter, $arSelect);
while ($arLead = $rsLead->Fetch()) {	
    SetUserField("CRM_LEAD", $arLead['ID'], "UF_CHANNEL", "3336");
}

//UPDATE Сайт
$arFilter = array("=UF_CHANNEL" => '');
$arFilter['SOURCE_ID'] = filter_Site();
$arSelect = array('ID', 'UF_CHANNEL', 'SOURCE_ID');
$CCrmLead = new CCrmLead();
$rsLead = $CCrmLead->GetList(array('ID' => 'ASC'), $arFilter, $arSelect);
while ($arLead = $rsLead->Fetch()) {
    SetUserField("CRM_LEAD", $arLead['ID'], "UF_CHANNEL", "5274");
}

$TIME = (mktime() - $START_TIME) / 60;
echo "Время обработки, мин: $TIME \n\n";



/*//Неотсортированные остатки.
$arFilter = array("=UF_CHANNEL" => '');
$arSelect = array('ID', 'UF_CHANNEL', 'SOURCE_ID');
$CCrmLead = new CCrmLead();
$rsLead = $CCrmLead->GetList(array('ID' => 'ASC'), $arFilter, $arSelect);
while ($arLead = $rsLead->Fetch()) {
    pre($arLead);
}*/
