<?php
/*
 * Выбирает компании по условию "CHECK_PERMISSIONS" => "N","=COMPANY_TYPE"=>"WITHOUTTYPE".
 * По найденным компаниям проверяются заполненность реквизитов.
 * Если реквизиты найдены, информация о данной компании записывается в “foundreq.txt”,
 * если не найдены - “notfoundreq.txt”
 */

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('CHK_EVENT', true);
ini_set('memory_limit', '-1');

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

@set_time_limit(0);
@ignore_user_abort(true);

global $USER;
$USER->Authorize(372);

CModule::IncludeModule('crm');
CModule::IncludeModule('iblock');

$oLead          = new CCrmLead;
$oContact       = new CCrmContact;
$oDeal          = new CCrmDeal;
$oCompany       = new CCrmCompany;
$CCrmEvent      = new CCrmEvent();
$oIBlockElement = new CIBlockElement();
$oUser          = new CUser;
$requisite = new \Bitrix\Crm\EntityRequisite();
$fieldsInfo = $requisite->getFormFieldsInfo();
$select = array_keys($fieldsInfo);
$entity = new CCrmCompany(false);
$filepathtxt="notfoundreq.txt";


$res = CCrmCompany::GetList(array(), array("CHECK_PERMISSIONS" => "N","=COMPANY_TYPE"=>"WITHOUTTYPE"));
while ($arRes = $res->Fetch()) 
{
  $companyID=$arRes['ID'];
   $arFilter = array(
        'ENTITY_ID' => $companyID,
		'ENTITY_TYPE_ID'    => 4
    );

    $result = $requisite->getList(
        array(
            'order' => array('SORT' => 'ASC', 'ID' => 'ASC'),
            'filter' => $arFilter,
            'select' => $select
        )
    );
	if ($row = $result->fetch()) 
	{
		$filepathtxt="foundreq.txt";
		file_put_contents($filepathtxt, $companyID . "\r\n", FILE_APPEND | LOCK_EX);
	}
	else
	{
		$filepathtxt="notfoundreq.txt";
		file_put_contents($filepathtxt, $companyID . "\r\n", FILE_APPEND | LOCK_EX);
		$successed = $entity->Delete($companyID);
	}
}
