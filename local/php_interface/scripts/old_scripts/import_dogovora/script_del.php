<?php
/*
 * Выполняет удаление всех элементов в ИБ с символьным кодом “import_dogovor”.
 */



define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('CHK_EVENT', true);
ini_set('memory_limit', '-1');

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

@set_time_limit(0);
@ignore_user_abort(true);

global $USER;
$USER->Authorize(372);

CModule::IncludeModule('iblock');
$oIBlockElement = new CIBlockElement();
$IBLOCK_ID = getIblockIDByCode('import_dogovor');

$arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM");
$arFilter = Array("IBLOCK_ID"=>$IBLOCK_ID);
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
$count = intval($res->SelectedRowsCount());
echo $count;
while($ob = $res->GetNextElement())
{
    $arFields = $ob->GetFields();
    CIBlockElement::Delete($arFields['ID']);
}