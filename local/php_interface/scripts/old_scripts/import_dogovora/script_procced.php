<?php
/*
 * Не до конца разобрался для чего используется.
 * На вход принимает параметры крона, обновляется свойство “STATUS”
 */

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('CHK_EVENT', true);
ini_set('memory_limit', '-1');

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

@set_time_limit(0);
@ignore_user_abort(true);

global $USER;
$USER->Authorize(372);

CModule::IncludeModule('crm');
CModule::IncludeModule('iblock');

$oLead          = new CCrmLead;
$oContact       = new CCrmContact;
$oDeal          = new CCrmDeal;
$oCompany       = new CCrmCompany;
$CCrmEvent      = new CCrmEvent();
$oIBlockElement = new CIBlockElement();

$IBLOCK_ID_CSV      = getIblockIDByCode('import_dogovor');
$IBLOCK_ID_DOGOVORA = getIblockIDByCode('registry_contracts');

$offset = 0;
if(array_key_exists(1, $argv)){
    $offset = $argv[1];
}

$START_TIME = mktime();
$n = 0;
while ($n < 50) {
	//break;
    $n++;

    $arResult = getOneElement($offset);
    if($arResult === false){
        break;
    }

    if(parserRow($arResult) === false){
        $oIBlockElement->SetPropertyValueCode($arResult['ID'], "STATUS", 3); //Устанавливаем статус 3 - ошибка
    } else {
        $oIBlockElement->SetPropertyValueCode($arResult['ID'], "STATUS", 2); //Устанавливаем статус 2 - успех
    }	
}
$TIME = (mktime() - $START_TIME) / 60;
echo "Время обработки, мин: $TIME \n\n";


function getOneElement($offset = 1){
    global $oIBlockElement,
           $IBLOCK_ID_CSV;

    $arFilter = array(
        "IBLOCK_ID"         => $IBLOCK_ID_CSV,
        "PROPERTY_STATUS"   => 0 // берем записи со статусом 0 - не обработан
    );

    $res = $oIBlockElement->GetList(array("ID"=>"ASC"), $arFilter, false, array('iNumPage' => $offset, 'nPageSize' => 1));
    if($res->SelectedRowsCount() <= $offset){
        return false;
    }

    if($ob = $res->GetNextElement()){
        $arElementFields    = $ob->GetFields();
        $oIBlockElement->SetPropertyValueCode($arElementFields['ID'], "STATUS", 1); //устанавливаем статус 1 - в работе

        $arElement          = $ob->GetProperties();

        $arResult = [];
        $arResult['ID']             = $arElementFields['ID'];
        $arResult['INN']            = $arElement['INN']['VALUE'];
        $arResult['KPP']            = $arElement['KPP']['VALUE'];
        $arResult['DOGOVOR_NUMBER'] = $arElement['DOGOVOR_NUMBER']['VALUE'];
        $arResult['DOGOVOR_DATE']   = $arElement['DOGOVOR_DATE']['VALUE']; //TID, Тенант, тип контрагента

        return $arResult;
    }
    return false;
}

function parserRow($arParams){
    global $oLead,
           $oContact,
           $oDeal,
           $oCompany,
           $CCrmEvent,
           $oIBlockElement,
           $IBLOCK_ID_DOGOVORA;

    $isLead             = false;
    $assignedDogovor    = 372;

    $arParams['INN'] = str_replace("OBSOLETE", "", $arParams['INN']);

    //Проверка на валидность ИНН
    switch (strlen($arParams['INN'])) {
        case 10:
            break;
        case 12:
            break;
        default:
            echo "Не валидный ИНН: " . $arParams['INN'];
            return false;
            break;
    }

    /** Поиск и обновление Лида */

    //Поиск Лида по ИНН
    $resMultiADD = $oLead->GetList(array('ID' => 'asc'), array(
        'TITLE'             => 'регистрация на сайте www.1ofd.ru',
        'UF_CRM_1499414186' => $arParams['INN']
    ));
    if ($arMultiADD = $resMultiADD->Fetch()) {
        //echo "Найден лид по ИНН\n"; //DEL
        $isLead = true;
        $arSelectedLead = array(
            "ID"            => $arMultiADD['ID'],
            "LAST_NAME"     => ($arMultiADD['LAST_NAME'] == "noname")? "" : $arMultiADD['LAST_NAME'],
            "NAME"          => ($arMultiADD['NAME'] == "noname")? "" : $arMultiADD['NAME'],
            "SECOND_NAME"   => ($arMultiADD['SECOND_NAME'] == "noname")? "" : $arMultiADD['SECOND_NAME'],
        );
    }

    //Получение Email и Телефона лида
    $arSelectedLead['EMAIL'] = false;
    $arSelectedLead['PHONE'] = false;
    if($isLead){
        $ResSelectLeadEmail = CCrmFieldMulti::GetList(
            array('ID' => 'asc'), array('ENTITY_ID' => 'LEAD', 'ELEMENT_ID' => $arSelectedLead['ID'])
        );
        while ($arSelectLeadEmail = $ResSelectLeadEmail->Fetch()) {
            if($arSelectLeadEmail['TYPE_ID'] == 'EMAIL'){
                $arSelectedLead['EMAIL'] = $arSelectLeadEmail['VALUE'];
            }
            if($arSelectLeadEmail['TYPE_ID'] == 'PHONE'){
                $arSelectedLead['PHONE'] = $arSelectLeadEmail['VALUE'];
            }
        }
    }

    //Переводим лид в статус зарегестрирован в лк
    if(!empty($isLead) && ($arMultiADD['STATUS_ID'] == "NEW" || $arMultiADD['STATUS_ID'] == "ASSIGNED")) {
        $arFields = Array(
            "STATUS_ID" => "2", //зарегистрирован в ЛК
        );

        $oLead->Update($arSelectedLead['ID'], $arFields);
        //echo "Лид переведён в статус зарегистрирован в ЛК ID (" . $arSelectedLead['ID'] . ")\n";
    }


    /** Поиск/создание компании по реквизитам ИНН и КПП */

    $requisite = new \Bitrix\Crm\EntityRequisite();
    $fieldsInfo = $requisite->getFormFieldsInfo();
    $select = array_keys($fieldsInfo);

    $arFilter = array(
        'RQ_INN' => $arParams['INN'],
        'ENTITY_TYPE_ID' => 4
    );
    if(!empty($arParams['KPP'])){
        $arFilter['RQ_KPP'] = $arParams['KPP'];
    }

    $res = $requisite->getList(
        array(
            'order' => array('SORT' => 'ASC', 'ID' => 'ASC'),
            'filter' => $arFilter,
            'select' => $select
        )
    );

    //Компания найдена
    if ($row = $res->fetch()) {
        $companyID = $row['ENTITY_ID'];
        //echo "Найдена существующая компания ID (" . $companyID . ")\n";

        //Получаем ответственного пользователя за компанию
        $assignedDogovor;
        $resCompany = CCrmCompany::GetListEx(array(), array('ID' => $companyID));
        if ($arCompanyDB = $resCompany->fetch()){
            if(!empty($arCompanyDB['ASSIGNED_BY_ID'])){
                $assignedDogovor = $arCompanyDB['ASSIGNED_BY_ID'];
            }
        }

    }
    else //Компания не найдена, создаем компанию
    {
        //Получение информации от налоговой по ИНН компании
        $arCompanyInfo = GetInfo($arParams['INN']);

        //Добавление компании на основании данных от Налоговой
        if(array_key_exists(0, $arCompanyInfo) && !empty($arCompanyInfo)){
            $info = Array(
                'fields' => Array(
                    'RQ_INN'            => $arParams['INN'],
                    'RQ_KPP'            => (empty($arParams['KPP'])) ? $arCompanyInfo[0]['fields']['RQ_KPP'] : $arParams['KPP'],
                    'RQ_OKVED'          => $arCompanyInfo[0]['fields']['RQ_OKVED'],
                    'SORT'              => 500,
                    'ENTITY_TYPE_ID'    => 4,
                )
            );

            $ogrn = (!empty($arCompanyInfo[0]['fields']['RQ_OGRNIP'])) ? $arCompanyInfo[0]['fields']['RQ_OGRNIP'] : $arCompanyInfo[0]['fields']['RQ_OGRN'];
            $ogrn = trim($ogrn);
            switch (strlen($ogrn)){
                case 15: //ИП
                    $info['fields']['NAME']                     = 'ИП';
                    $info['fields']['RQ_OGRNIP']                = $ogrn;
                    $info['fields']['PRESET_ID']                = 2;
                    $info['fields']['RQ_LAST_NAME']             = $arCompanyInfo[0]['fields']['RQ_LAST_NAME'];
                    $info['fields']['RQ_FIRST_NAME']            = $arCompanyInfo[0]['fields']['RQ_FIRST_NAME'];
                    $info['fields']['RQ_SECOND_NAME']           = $arCompanyInfo[0]['fields']['RQ_SECOND_NAME'];

                    $arFields['TITLE']          = $arCompanyInfo[0]['fields']['RQ_NAME'];
                    $arFields['UF_FULL_NAME']   = $arCompanyInfo[0]['fields']['RQ_NAME'];
                    $arFields['COMPANY_TITLE']  = $arCompanyInfo[0]['fields']['RQ_NAME'];
                    break;
                default: //Организация
                    $info['fields']['NAME']                     = 'Организация';
                    $info['fields']['RQ_COMPANY_NAME']          = (!empty($arCompanyInfo[0]['fields']['RQ_COMPANY_NAME'])) ? $arCompanyInfo[0]['fields']['RQ_COMPANY_NAME'] : $arCompanyInfo[0]['fields']['RQ_COMPANY_FULL_NAME'];
                    $info['fields']['RQ_COMPANY_FULL_NAME']     = (!empty($arCompanyInfo[0]['fields']['RQ_COMPANY_FULL_NAME'])) ? $arCompanyInfo[0]['fields']['RQ_COMPANY_FULL_NAME'] : "";
                    $info['fields']['RQ_OGRN']                  = $ogrn;
                    $info['fields']['PRESET_ID']                = 1;

                    $arFields['TITLE']          = $arCompanyInfo[0]['fields']['RQ_COMPANY_NAME'];
                    $arFields['UF_FULL_NAME']   = $arCompanyInfo[0]['fields']['RQ_COMPANY_FULL_NAME'];
                    $arFields['COMPANY_TITLE']  = $arCompanyInfo[0]['fields']['RQ_COMPANY_NAME'];
                    break;
            }

            $arFields["ASSIGNED_BY_ID"] = 372;
            $arFields['COMPANY_TYPE']   = 'CUSTOMER';

            //Создаем компанию
            if($companyID = $oCompany->Add($arFields)){
                //echo "Создали компанию: $companyID \n";
            } else {
                echo "Не удалось создать компанию \n";
                return false;
            }

            $info['fields']['ENTITY_ID'] = $companyID;

            if($resGk = gk_AddRQ($info)){
                //echo "Успешно добавлены реквизиты для компании $companyID\n";
            } else {
                echo "Ошибка добавления реквизитов для компании $companyID\n";
            }
        }

        //Добавляем компанию, если нет информации от Налоговой
        if(empty($arCompanyInfo)){
            //echo "Нет информации от налоговой, создаем компанию на основании ИНН/КПП.\n";
            $arFields = array(
                "TITLE" => "Организация",
                "ASSIGNED_BY_ID" => 372,
                "COMPANY_TYPE" => 'CUSTOMER'
            );

            //Создаем компанию
            if($companyID = $oCompany->Add($arFields)){
                //echo "Создали компанию: $companyID \n";
            } else {
                echo "Не удалось создать компанию \n";
                return false;
            }

            //Добавляем реквизиты
            $info = Array(
                'fields' => Array(
                    'NAME'              => "Организация",
                    'RQ_INN'            => $arParams['INN'],
                    'RQ_KPP'            => $arParams['KPP'],
                    'SORT'              => 500,
                    'ENTITY_TYPE_ID'    => 4,
                    'ENTITY_ID'         => $companyID,
                    'PRESET_ID'         => 1,
                )
            );

            if($resGk = gk_AddRQ($info)){
                //echo "Успешно добавлены реквизиты для компании $companyID\n";
            } else {
                echo "Ошибка добавления реквизитов для компании $companyID\n";
            }
        }
    }

    /** Поиск/создание контакта по Email */
    //Поиск контакта по ФИО и Email из лида
    if( !empty($arSelectedLead['EMAIL']) &&
        ( !empty($arSelectedLead['LAST_NAME']) || !empty($arSelectedLead['NAME']) || !empty($arSelectedLead['SECOND_NAME'])))
    {
        //Поиск дубликата
        $resMultiDublicateContact = CCrmFieldMulti::GetList(
            array('ID' => 'asc'), array('ENTITY_ID' => 'CONTACT', 'TYPE_ID' => 'EMAIL', 'VALUE' => $arSelectedLead['EMAIL'])
        );
        while ($arMultiDublicateContact = $resMultiDublicateContact->Fetch()) {
            //echo "Найден существующий контакт по Email лида (" . $arMultiDublicateContact['ELEMENT_ID'] . ")\n";

            //Проверяем на совпадение по ФИО
            $arFilter = array(
                "ID"            => $arMultiDublicateContact['ELEMENT_ID'],
                "LAST_NAME"     => $arSelectedLead['LAST_NAME'],
                "NAME"          => $arSelectedLead['NAME'],
                "SECOND_NAME"   => $arSelectedLead['SECOND_NAME'],
            );
            $arResContact = $oContact->GetList(array(), $arFilter);

            if(intval($arResContact->SelectedRowsCount()) > 0) {
                //echo "Найдено ТОЧНОЕ совпадение по контакту: Email + ФИО $arMultiDublicateContact[ELEMENT_ID] \n";
                $contactID = $arMultiDublicateContact['ELEMENT_ID'];
                break;
            }
        }
    }

    //Контакт не найден, создаем контакт на основании данных из лида
    if(empty($contactID)){
        //Создаем контакт на основании данных из лида
        if( !empty($arSelectedLead['EMAIL']) &&
            ( !empty($arSelectedLead['LAST_NAME']) || !empty($arSelectedLead['NAME']) || !empty($arSelectedLead['SECOND_NAME'])))
        {
            $arContact = Array(
                'NAME'              => $arSelectedLead['NAME'],
                'SECOND_NAME'       => $arSelectedLead['SECOND_NAME'],
                'LAST_NAME'         => $arSelectedLead['LAST_NAME'],
                'ASSIGNED_BY_ID'    => 372,
                'COMPANY_ID'        => $companyID,
                "FM" => Array(
                    "EMAIL" => Array(
                        "n0" => Array(
                            "VALUE"         => (empty($arSelectedLead['EMAIL']))? "" : $arSelectedLead['EMAIL'],
                            "VALUE_TYPE"    => "WORK",
                        ),
                    ),
                    "PHONE" => Array(
                        "n0" => Array(
                            "VALUE"         => (empty($arSelectedLead['PHONE']))? "" : $arSelectedLead['PHONE'],
                            "VALUE_TYPE"    => "WORK",
                        ),
                    ),
                ),
            );
            if($contactID = $oContact->Add($arContact)){
                //echo "Создан новый контакт на основании данных из лида: $contactID \n";
            } else {
                echo "Ошибка создания нового контакта \n";
            }
        }
    }


    /** Поиск/создание сделки по ID компании */
    $arDeal = findOptimalDealByCompanyID($companyID);
    //Сделка найдена
    $arDogovor = array();
    if ($arDeal && $arDeal['ID']) {
        $idDeal     = $arDeal['ID'];
        $arDogovor  = (empty($arDeal['UF_DOGOVOR']))? array() : $arDeal['UF_DOGOVOR'] ;
        //echo "Найдена сделка ID: $idDeal \n";//DEL
    }
    else //Сделка не найдена
    {
        $arFields = Array(
            "TITLE"                => "Основная услуга ОФД",
            "STAGE_ID"             => "C1:NEGOTIATION",
            "ASSIGNED_BY_ID"       => $assignedDogovor,
            "CATEGORY_ID"          => 1,
            'TYPE_ID'              => 2, //Тип: Основная услуга ОФД
            'COMPANY_ID'           => $companyID,
            "UF_KKT"                => 0, //кол-во ККТ
        );
        $idDeal = $oDeal->Add($arFields);
        if($idDeal){
            //echo "Создали сделку ID: $idDeal \n";
        }else{
            echo "Ошибка создания сделки. \n";
            return false;
        }
    }

    /** Поиск/создание Договора */

    //Если договора есть, проверяем на дубликат
    if(!empty($arDogovor)){
        $resDogovorBD = $oIBlockElement->GetList(array(), array("ID" => $arDogovor, "PROPERTY__DOGOVORA" => $arParams['DOGOVOR_NUMBER']));
        if($arDog = $resDogovorBD->Fetch()){
            //echo "Найден дубликат договора ($arDog[ID]). Завершаем работу\n";
            return true;
        }
    }

    //Договора нет или нет дубликата договора. Добавляем договор
    $dateDogovor = date("d.m.Y", strtotime($arParams['DOGOVOR_DATE']));

    //Получение ID свойстава типа список по XML_ID
    $property_enums = CIBlockPropertyEnum::GetList(Array(), Array("IBLOCK_ID"=>'40', "XML_ID"=>"GENERATE_LK"));
    while($enum_fields = $property_enums->GetNext())
    {
        $ENUM_ID = $enum_fields["ID"];
    }
    $PROP = array(
        "TIP_KONTRAGENTA"           => "Клиент",
        "KOMPANIYA_ZAKAZCHIK"       => "ЭСК",
        "KONTRAGENT"                => $companyID,
        "TIP_DOGOVORA_NEW"          => "Оферта",
        "OTVETSTVENNYY_MENEDZHER"   => $assignedDogovor,
        "_DOGOVORA"                 => $arParams['DOGOVOR_NUMBER'],
        "DATA_DOGOVORA"             => $dateDogovor,
        "STATUS_DOGOVORA"           => array("VALUE" => $ENUM_ID)
    );

    $arLoadProductArray = Array(
        "MODIFIED_BY"       => $assignedDogovor, // элемент изменен текущим пользователем
        "IBLOCK_SECTION_ID" => false,          // элемент лежит в корне раздела
        "IBLOCK_ID"         => $IBLOCK_ID_DOGOVORA,
        "PROPERTY_VALUES"   => $PROP,
        "NAME"              => "Оферта основная услуга, номер договора $arParams[DOGOVOR_NUMBER]"
    );

    if($dogovorID = $oIBlockElement->Add($arLoadProductArray)) {
        //echo "New ID: " . $dogovorID . "\n";
    } else {
        echo "Error: " . $oIBlockElement->LAST_ERROR . "\n";
        return false;
    }


    /** Обновляем сделку, создаем запись в истории */
    array_push($arDogovor, $dogovorID);
    $arFields = Array(
        'UF_DOGOVOR' => $arDogovor,
        'TYPE_ID'   => 2, //Тип: Основная услуга ОФД
    );
	
    $resUpdate = $oDeal->Update($idDeal, $arFields);
    if ($resUpdate) {
        //echo "Обновлена привязка к договору у сделки ID: $idDeal\n";
    } else {
        echo "Ошибка обновления привязки к договру у сделки ID: $idDeal \n";
        return false;
    }

    //Создание истории для сделки
    $idEventHistory = $CCrmEvent->Add(
        array(
            'ENTITY_TYPE' => 'DEAL',
            'ENTITY_ID' => $idDeal,
            'EVENT_ID' => 'INFO',
            'EVENT_TEXT_1' => 'Добавлен договор «Оферта основная услуга», с номером: ' . $arParams['DOGOVOR_NUMBER'],
        )
    );

    if ($idEventHistory) {
        //echo "История сделки успешно создана\n";
    } else {
        echo "Ошибка создания истории сделки\n";
    }

    return true;
}

//Функция поиска дубликатов лидов по TITLE и EMAIL
function leadDublicateCheck($title, $email) {
    $ResMulti = CCrmFieldMulti::GetList(
        array('ID' => 'desc'), array('ENTITY_ID' => 'LEAD', 'TYPE_ID' => 'EMAIL', 'VALUE' => $email)
    );
    if(intval($ResMulti->SelectedRowsCount()) <= 0 ) {
        return false;
    }
    while ($arMulti = $ResMulti->Fetch()) {
        $res = CCrmLead::GetList(array(), array("ID" => $arMulti['ELEMENT_ID'], "TITLE" => $title));
        if(intval($res->SelectedRowsCount()) > 0 ) {
            return $arMulti['ELEMENT_ID'];
        }
    }
    return false;
}

//Поиск дубликата сделки
function findDublicateDeal($arFilter){
    $resDublicateDeal = CCrmDeal::GetListEx(array('ID'=>'ASC'),$arFilter);
    if ($arDublicateDeal = $resDublicateDeal->Fetch()) {
        return $arDublicateDeal;
    }else{
        return false;
    }
}

function findOptimalDealByCompanyID($companyID){
    $arFilter = array(
        'CATEGORY_ID'   => '1', //Продажа клиент
        'COMPANY_ID'    => $companyID,
    );

    $resDublicateDeal = CCrmDeal::GetList(array('ID'=>'DESC'), $arFilter);
    $n = 0;
    $arResult = [];
    while ($arDublicateDeal = $resDublicateDeal->Fetch()) {
        if($n == 0){
            $arResult = $arDublicateDeal;
        }

        if(!empty($arDublicateDeal['UF_DOGOVOR'])){
            return $arResult;
        }
        $n++;
    }

    if(!empty($arResult)){
        return $arResult;
    }
    return false;
}

//Поиск дубликата ККТ
function findDublicateKKT($kkmRegId){
    if($kkmRegId){
        global $IBLOCK_ID_KKT;
        //$IBLOCK_ID_KKT = getIblockIDByCode('registry_kkt');
        $arSelect = Array("ID", "IBLOCK_ID","PROPERTY_RNM_KKT", "PROPERTY_COST_TARIF", "PROPERTY_DEAL");
        $arFilter = array("IBLOCK_ID" => $IBLOCK_ID_KKT,"PROPERTY_RNM_KKT" =>$kkmRegId);
        $res = CIBlockElement::GetList(Array(), $arFilter, false, false,$arSelect);
        if($arKKT = $res->Fetch()){
            return $arKKT;
        }
        return false;
    }else{
        return false;
    }
}

//Получение информации с налоговой
function GetInfo($inn)
{
    $propertyTypeID = 'ITIN';
    $propertyValue = $inn;
    $countryID = 1;

    $result = \Bitrix\Crm\Integration\ClientResolver::resolve(
        $propertyTypeID,
        $propertyValue,
        $countryID
    );
    if (count($result) == 0)
    {
        $result = GetInfoOGRNOnline($inn);
    }
    return $result;
}

function GetInfoOGRNOnline($inn)
{
    $dataJSON = file_get_contents('https://xn--c1aubj.xn--80asehdb/%D0%B8%D0%BD%D1%82%D0%B5%D0%B3%D1%80%D0%B0%D1%86%D0%B8%D1%8F/%D0%BA%D0%BE%D0%BC%D0%BF%D0%B0%D0%BD%D0%B8%D0%B8/?%D0%B8%D0%BD%D0%BD='.$inn);
    $data = json_decode($dataJSON, true);

    if (strlen($inn) == 10 && count($data) > 0)
    {
        $arFields = array(
            'RQ_COMPANY_NAME' => 'shortName',
            'RQ_COMPANY_FULL_NAME' => 'name',
            'RQ_INN' => 'inn',
            'RQ_KPP' => 'kpp',
            'RQ_OGRN' => 'ogrn');
        $result = array();
        foreach ($data as $k => $v) {
            $rq = array();
            foreach ($arFields as $kf => $vf) {
                $rq[$kf] = $v[$vf];
            }
            $result[] = array('fields' => $rq);

        }
        return $result;

    }
    else
    {
        $data = array();
    }

    return $data;
}

function getFmId($elementID, $typeID){
    $dbResFM = CCrmFieldMulti::GetList(
        array('ID' => 'asc'),
        array('ENTITY_ID' => 'COMPANY', 'TYPE_ID' => $typeID, 'ELEMENT_ID' => $elementID)
    );

    if($arFM = $dbResFM->Fetch())
    {
        return $arFM['ID'];
    }

    return "n0";
}
