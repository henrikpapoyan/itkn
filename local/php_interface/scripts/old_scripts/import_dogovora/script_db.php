<?php
/*
 * Выполняет добавление элементов в ИБ
 * с символьным кодом “import_dogovor”
 * из файла “dogovor_agent.csv”
 */



define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('CHK_EVENT', true);
ini_set('memory_limit', '-1');

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

@set_time_limit(0);
@ignore_user_abort(true);

global $USER;
$USER->Authorize(372);

CModule::IncludeModule('iblock');
$oIBlockElement = new CIBlockElement();
$IBLOCK_ID = getIblockIDByCode('import_dogovor');


//CLIENT
/*$row = 1;
if (($handle = fopen("dogovora.csv", "r")) !== FALSE) {
    $START_TIME = mktime();
    while (($data = fgetcsv($handle, 0, "\t")) !== FALSE) {
        $arResult = [];
        $arResult['INN']            = $data[1];
        $arResult['KPP']            = $data[2];
        $arResult['DOGOVOR_NUMBER'] = $data[3];
        $arResult['DOGOVOR_DATE']   = $data[4];

        addElementImportDogovor($arResult);

        if($row == 10){
			//addElementImportDogovor($arResult);
            //break;
        }

        $row++;
    }
    $TIME = (mktime() - $START_TIME);
    echo "Общее кол-во строк: $row \n";
    echo "Время обработки, сек: $TIME \n\n";

    fclose($handle);
}*/

//AGENT
$row = 1;
if (($handle = fopen("dogovor_agent.csv", "r")) !== FALSE) {
    $START_TIME = mktime();
    while (($data = fgetcsv($handle, 0, ";")) !== FALSE) {
        $arResult = [];
        $arResult['INN']            = $data[1];
        $arResult['KPP']            = $data[2];
        $arResult['DOGOVOR_DATE']   = $data[3];
        $arResult['DOGOVOR_NUMBER'] = $data[4];

        addElementImportDogovor($arResult);

        /*if($row == 3){
            break;
        }*/

        $row++;
    }
    $TIME = (mktime() - $START_TIME);
    echo "Общее кол-во строк: $row \n";
    echo "Время обработки, сек: $TIME \n\n";

    fclose($handle);
}

function addElementImportDogovor($arParams){
    global $oIBlockElement,
           $IBLOCK_ID;

    /** Проверка на наличе обязательных полей */
    //Проверка на валидность ИНН
    $arParams['INN'] = str_replace("OBSOLETE", "", $arParams['INN']);

    switch (strlen($arParams['INN'])) {
        case 10:
            break;
        case 12:
            break;
        default:
            echo "Не валидный ИНН: " . $arParams['INN'] . "\n";
            return false;
            break;
    }

    //Проверка на наличие номера договора
    if(empty($arParams['DOGOVOR_NUMBER'])){
        echo "Отсутствует номер договора\n";
        return false;
    }

    $PROP = array(
        "STATUS"            => 0,
        "INN"               => $arParams['INN'],
        "KPP"               => $arParams['KPP'],
        "DOGOVOR_NUMBER"    => $arParams['DOGOVOR_NUMBER'],
        "DOGOVOR_DATE"      => $arParams['DOGOVOR_DATE']
    );

    $arLoadProductArray = Array(
        "MODIFIED_BY"       => 372, // элемент изменен текущим пользователем
        "IBLOCK_SECTION_ID" => false,          // элемент лежит в корне раздела
        "IBLOCK_ID"         => $IBLOCK_ID,
        "PROPERTY_VALUES"   => $PROP,
        "NAME"              => "INN: $arParams[INN] | KPP: $arParams[KPP] | DOGOVOR_NUMBER: $arParams[DOGOVOR_NUMBER]"
    );

    if($PRODUCT_ID = $oIBlockElement->Add($arLoadProductArray)) {
        //echo "New ID: " . $PRODUCT_ID . "\n";
    }
    else {
        echo "Error: " . $oIBlockElement->LAST_ERROR . "\n";
    }
}