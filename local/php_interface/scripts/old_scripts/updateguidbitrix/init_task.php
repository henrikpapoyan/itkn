<?
/*
 * Не понял для чего используется.
 * Работает с вручную добавленной таблицей “synchbitrixtobpmcompany”.
 * Обновляет пользовательское свойство “UF_CRM_COMPANY_GUID” для контрагента.
 */

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('CHK_EVENT', true);
ini_set('memory_limit', '-1');
@set_time_limit(0);
@ignore_user_abort(true);

// закачка данных в базу
// Шаг 1 пилим в базу BITRIX Данные.
$start = microtime(true);
$connection = Bitrix\Main\Application::getConnection();
$resultbitrix=Reports::getAllCompanyBitrix();
foreach($resultbitrix as $index=>$contrbitrix)
{
    $sql = 'INSERT INTO synchbitrixtobpmcompany (ID_BITRIX,GUID_BPM_COMPANY,INN_BPM_COMPANY,KPP_BPM_COMPANY,STATYS_ANSWER_BPM_COMPANY)
            VALUES (\''.$contrbitrix["ID"].'\',\''.$contrbitrix["GUID"].'\',\''.$contrbitrix["INN"].'\',\''.$contrbitrix["KPP"].'\',\''. "BITRIX" . '\');';
    $connection->queryExecute($sql);
}
unset($resultbitrix);
$time = microtime(true) - $start;
printf('Скрипт выполнялся %.4F сек.', $time);
echo "<br>";

// Шаг 2 пилим в базу BPM Данные.
$start = microtime(true);
$connection = Bitrix\Main\Application::getConnection();
$resultbpm=Reports::getAllCompanyBpm();
foreach($resultbpm as $index=>$contrbpm)
{
    $sql = 'INSERT INTO synchbitrixtobpmcompany (GUID_BPM_COMPANY,INN_BPM_COMPANY,KPP_BPM_COMPANY,STATYS_ANSWER_BPM_COMPANY)
            VALUES (\''.$contrbpm["GUID"].'\',\''.$contrbpm["INN"].'\',\''.$contrbpm["KPP"].'\',\''. "BPM" . '\');';
   $connection->queryExecute($sql);
}
unset($resultbpm);
$time = microtime(true) - $start;
printf('Скрипт выполнялся %.4F сек.', $time);
