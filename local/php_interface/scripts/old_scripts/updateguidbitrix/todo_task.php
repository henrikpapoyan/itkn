<?
/*
 * Не понял для чего используется.
 * Работает с вручную добавленной таблицей “synchbitrixtobpmcompany”.
 * Судя по всему записывает в данную таблицу информацию обо всех контрагентах в Битрикс24 и Контрагентов BPM.
 */

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('CHK_EVENT', true);
ini_set('memory_limit', '-1');
@set_time_limit(0);
@ignore_user_abort(true);

$start = microtime(true);
$connection = Bitrix\Main\Application::getConnection();
$sql = 'SELECT ID_BITRIX,GUID_BPM_COMPANY,INN_BPM_COMPANY,KPP_BPM_COMPANY FROM synchbitrixtobpmcompany WHERE STATYS_ANSWER_BPM_COMPANY = \'BITRIX\'';
$recordset = $connection->query($sql, "1000");
while ($record = $recordset->fetch()) {
    $idcompanybitrix=$record['ID_BITRIX'];
    $inncompanybitrix=$record['INN_BPM_COMPANY'];
    $kppcompanybitrix=$record['KPP_BPM_COMPANY'];
    $guidcompanybitrix=$record['GUID_BPM_COMPANY'];
    $guidcompanybpm="";
    $sqlbpm = 'SELECT GUID_BPM_COMPANY FROM synchbitrixtobpmcompany WHERE STATYS_ANSWER_BPM_COMPANY = \'BPM\' AND INN_BPM_COMPANY = \''.$inncompanybitrix.'\' AND KPP_BPM_COMPANY = \''.$kppcompanybitrix.'\' ';
    $recordsetbpm = $connection->query($sqlbpm);
    $countfoundelement=0;
    while ($recordbpm = $recordsetbpm->fetch()) {
        $guidcompanybpm=$recordbpm['GUID_BPM_COMPANY'];
        $countfoundelement++;
    }
    if( $countfoundelement=="1")
    {
        if ($guidcompanybitrix != $guidcompanybpm) {
            $entity_id = "CRM_COMPANY";
            $uf_guid = "UF_CRM_COMPANY_GUID";			
            SetUserField($entity_id, $idcompanybitrix, $uf_guid, $guidcompanybpm);
            $sqlupdate = 'UPDATE synchbitrixtobpmcompany SET STATYS_ANSWER_BPM_COMPANY = \'' . "BITRIXUPDATEDONE" . '\' WHERE ID_BITRIX=\'' . $idcompanybitrix . '\' ';
            $connection->queryExecute($sqlupdate);
        } else {

            $sqlupdate = 'UPDATE synchbitrixtobpmcompany SET STATYS_ANSWER_BPM_COMPANY = \'' . "BITRIXUPDATENOTNEED" . '\' WHERE ID_BITRIX=\'' . $idcompanybitrix . '\' ';
            $connection->queryExecute($sqlupdate);
        }
    }
	elseif($countfoundelement>1)
    {
	    $sqlupdate = 'UPDATE synchbitrixtobpmcompany SET STATYS_ANSWER_BPM_COMPANY = \'' . "BITRIXMANYCOUNT" . '\' WHERE ID_BITRIX=\'' . $idcompanybitrix . '\' ';
        $connection->queryExecute($sqlupdate);
	}
    else
    {
        $sqlupdate = 'UPDATE synchbitrixtobpmcompany SET STATYS_ANSWER_BPM_COMPANY = \'' . "BITRIXNOTFOUND" . '\' WHERE ID_BITRIX=\'' . $idcompanybitrix . '\' ';
        $connection->queryExecute($sqlupdate);
    }
}
$time = microtime(true) - $start;
printf('Скрипт выполнялся %.4F сек.', $time);
echo "\n";

