<?php
/*
 * Не до конца разобрался. Похоже, что выбирает все компании из Битрикса и БМП,
 * сравнивает “guid” и если они совпадают записывает информацию в файл “notfoundcompany.txt”
 * иначе в файл “foundcompany.txt”
 */

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('CHK_EVENT', true);
ini_set('memory_limit', '-1');

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

@set_time_limit(0);
@ignore_user_abort(true);

global $USER;
$USER->Authorize(372);

CModule::IncludeModule('crm');
CModule::IncludeModule('iblock');

$oLead          = new CCrmLead;
$oContact       = new CCrmContact;
$oDeal          = new CCrmDeal;
$oCompany       = new CCrmCompany;
$CCrmEvent      = new CCrmEvent();
$oIBlockElement = new CIBlockElement();
$oUser          = new CUser;




$resBitrix = Reports::getAllCompanyBitrix();
$resBpm = Reports::getAllCompanyBpm();


$arResultByCategory = array(
            "BITRIX_NOT_IN_BPM" => [], //!!!
            "BPM_NOT_IN_BITRIX" => [], //!!!
        );


$newresBitrix=[];
foreach ($resBitrix as $key => $val){
	$newresBitrix[$val['GUID']]=$val;
	$newresBitrix[$val['GUID']]['GUID_BPM']="NOTFOUND";
}

$newresBpm=[];
foreach ($resBpm as $key => $val){

	$newresBpm[$key]=$val;
	$newresBpm[$key]['ID_BITRIX']="NOTFOUND";
}

        foreach ($newresBitrix as $key => $val){
            if(!empty($newresBpm[$key])){
                $newresBpm[$key]['ID_BITRIX']=$val['ID_BITRIX'];
            }
            if( empty($newresBpm[$key]) ) {
                $arResultByCategory['BITRIX_NOT_IN_BPM'][] = $val;
            }
        }

        foreach ($newresBpm as $key => $val){
            if($val['ID_BITRIX']=='NOTFOUND')
            {
                $arResultByCategory['BPM_NOT_IN_BITRIX'][] = $val;//break;
            }
        }

//pre($arResultByCategory['BPM_NOT_IN_BITRIX']);		

$requisite = new \Bitrix\Crm\EntityRequisite();
$fieldsInfo = $requisite->getFormFieldsInfo();
$select = array_keys($fieldsInfo);


foreach ($arResultByCategory['BPM_NOT_IN_BITRIX'] as $arrayelement)
{
	$innbpmcompany=$arrayelement['INN'];
	$kppbpmcompany=$arrayelement['KPP'];
	$namebpmcompany=$arrayelement['NAME'];
	$guidbpmcompany=$arrayelement['GUID'];
	
	
	if(empty($innbpmcompany)||($innbpmcompany=="0"))
	{
		$filepathtxt="notfoundcompany.txt";
		file_put_contents($filepathtxt, $innbpmcompany.";".$kppbpmcompany.";".$namebpmcompany.";".$guidbpmcompany. "\r\n", FILE_APPEND | LOCK_EX);
	}
	else{
	$arFilter = array(
        'RQ_INN' => $innbpmcompany,
		'RQ_KPP' => $kppbpmcompany,
		'ENTITY_TYPE_ID'    => 4
    );
	
    $result = $requisite->getList(
        array(
            'order' => array('SORT' => 'ASC', 'ID' => 'ASC'),
            'filter' => $arFilter,
            'select' => $select
        )
    );
	
	if ($row = $result->fetch()) 
	{
		$companyID=$row['ENTITY_ID'];
		
		$entity_id = "CRM_COMPANY";
		$uf_guid = "UF_CRM_COMPANY_GUID";
		$guidcompanybitrix=GetUserField($entity_id, $companyID, $uf_guid);
		

	if(strtoupper($guidcompanybitrix)!=strtoupper($guidbpmcompany)){
		$filepathtxt="foundcompany.txt";
		file_put_contents($filepathtxt, $innbpmcompany.";".$kppbpmcompany.";".$namebpmcompany.";bpmguid;".$guidbpmcompany.";bitrixguid;".$guidcompanybitrix.";bitrixidcompany;".$companyID."\r\n", FILE_APPEND | LOCK_EX);
	}
	
	}
	else
	{   // pre($arrayelement);break;
		$filepathtxt="notfoundcompany.txt";
		file_put_contents($filepathtxt, $innbpmcompany.";".$kppbpmcompany.";".$namebpmcompany.";".$guidbpmcompany. "\r\n", FILE_APPEND | LOCK_EX);
	}
	}
	
}




echo "Работа выполнена.";