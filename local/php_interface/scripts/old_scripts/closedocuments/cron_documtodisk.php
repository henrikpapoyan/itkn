<?php
/*
 * Выполняется проверка раздела /close_docs/ на наличие файлов с расширением xlsx и загружает в Битрикс.Диск.
 */

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
define('CHK_EVENT', true);
ini_set('memory_limit', '-1');

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

@set_time_limit(0);
@ignore_user_abort(true);

global $USER;
$USER->Authorize(372);

if (empty($path)) {
    $path = $_SERVER["DOCUMENT_ROOT"] . "/close_docs/";
}

if ($handle = opendir($path)) {
    while (false !== ($entry = readdir($handle))) {
        if ($entry != "." && $entry != "..") {
            //Обработка только xlsx файлов
            $info = new SplFileInfo($entry);
            $extensionFile = $info->getExtension();			
            if($extensionFile == "xlsx"){				
                $fileName[] = $entry;
				echo "Файл есть: $entry \n";
            }
        }
    }
    closedir($handle);
}

if (\Bitrix\Main\Loader::includeModule('disk'))
{
    $driver = \Bitrix\Disk\Driver::getInstance();
    $storage = $driver->getStorageByGroupId(37);	
	
    if (!empty($storage)) {
        //Сохранение файлов в Bitrix Диск		
        $folder = $storage->getRootObject($storage['ROOT_OBJECT_ID']);
        foreach ($fileName as $key=>$name) {
            $fileArray = \CFile::MakeFileArray($path . $name);
            $file = $folder->uploadFile($fileArray, array(
                'CREATED_BY' => 372
            ));

            unlink($path.$name);
        }
    }
}