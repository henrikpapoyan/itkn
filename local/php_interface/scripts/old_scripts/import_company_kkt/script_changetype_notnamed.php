<?php
/*
 * Выполняет обновление “Тип компании” для контрагента по различным условиям.
 * Не до конца разобрался от чего зависит устанавливаемый тип компании.
 */

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
define('CHK_EVENT', true);
ini_set('memory_limit', '-1');

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

@set_time_limit(0);
@ignore_user_abort(true);

global $USER;
$USER->Authorize(372);

CModule::IncludeModule('crm');
CModule::IncludeModule('iblock');

$oLead = new CCrmLead;
$oContact = new CCrmContact;
$oDeal = new CCrmDeal;
$oCompany = new CCrmCompany;
$CCrmEvent = new CCrmEvent();
$oIBlockElement = new CIBlockElement();
$oUser = new CUser;


$START_TIME = mktime();
$lines = file($_SERVER["DOCUMENT_ROOT"] . '/local/php_interface/scripts/import_company_kkt/idelement.txt');
foreach ($lines as $line_num => $line) {
    parserRow($line);
   //if($line_num>1000){ break;}
}
$TIME = (mktime() - $START_TIME);
echo $TIME;

function parserRow($companyID)
{

$filepathtxt= $_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/scripts/import_company_kkt/resultstatus.txt";

$companyID = str_replace("\r\n", "", $companyID);
    global $oLead,
           $oContact,
           $oDeal,
           $oCompany,
           $oUser,
           $CCrmEvent,
           $oIBlockElement,
           $IBLOCK_ID_KKT,
           $IBLOCK_ID_PAY;


    $fieldtenant = GetUserField("CRM_COMPANY", $companyID, "UF_TENANT");
	$resultcompany = CCrmCompany::GetByID($companyID);
    $companytype = $resultcompany['COMPANY_TYPE'];	
	file_put_contents($filepathtxt, $companyID . ";old;$companytype\r\n", FILE_APPEND | LOCK_EX);

    switch ($fieldtenant) {
        case "286":
            $arraystatus = [];


            $resultcompany = CCrmCompany::GetByID($companyID);
            $companytype = $resultcompany['COMPANY_TYPE'];

            if ($companytype != "") {
                //echo "Если тип компании какой-либо, кроме Клиент, то проверка наличия сделок.\n";
                $arFields["COMPANY_TYPE"] = "CUSTOMER";
                $arOrder = Array('DATE_CREATE' => 'DESC');
                $arFilter = Array("COMPANY_ID" => $companyID);
                $arFilter["!=STAGE_ID"] = Array("C1:EXIT", "C5:LOSE", "C4:LOSE", "C3:LOSE", "C2:LOSE", "C1:LOSE", "LOSE", "WON", "C1:WON", "C2:WON", "C3:WON", "C4:WON", "C5:WON");
                $arSelect = Array("*");
                $result = $oDeal->GetListEx($arOrder, $arFilter, $arSelect, false);
                while ($fields = $result->Fetch()) {
                    $DealTypeArray = $fields;
                    $dealid = $DealTypeArray['ID'];
                    $dealstatus = $DealTypeArray['TYPE_ID'];
                    //echo "Сделка $dealid статус $dealstatus.\n";
                    if ($DealTypeArray['TYPE_ID'] == "COMPLEX") {
                        $newstatuscompany = "19";
                        //echo "Новый статус компании равен $newstatuscompany.\n";
                        $arFields["COMPANY_TYPE"] = $newstatuscompany;
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    } else if ($DealTypeArray['TYPE_ID'] == "GOODS") {
                        $newstatuscompany = "19";
                        //echo "Новый статус компании равен $newstatuscompany.\n";
                        $arFields["COMPANY_TYPE"] = $newstatuscompany;
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    } else if ($DealTypeArray['TYPE_ID'] == "SALE") {
                        $newstatuscompany = "2";
                        //echo "Новый статус компании равен $newstatuscompany.\n";
                        $arFields["COMPANY_TYPE"] = $newstatuscompany;
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    } else if ($DealTypeArray['TYPE_ID'] == "SERVICE") {
                        $newstatuscompany = "20";
                        //echo "Новый статус компании равен $newstatuscompany.\n";
                        $arFields["COMPANY_TYPE"] = $newstatuscompany;
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    } else if ($DealTypeArray['TYPE_ID'] == "SERVICES") {
                        $newstatuscompany = "19";
                        //echo "Новый статус компании равен $newstatuscompany.\n";
                        $arFields["COMPANY_TYPE"] = $newstatuscompany;
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    } else if ($DealTypeArray['TYPE_ID'] == "2") {
                        $newstatuscompany = "CUSTOMER";
                        //echo "Новый статус компании равен $newstatuscompany.\n";
                        $arFields["COMPANY_TYPE"] = $newstatuscompany;
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    } else {
                        $arFields["COMPANY_TYPE"] = "косячнаякомпания";
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    }
                }
                ksort($arraystatus);$resultstring = "";
                foreach ($arraystatus as $index => $elementvalue) {
                    if ($elementvalue > 0) {
                        $resultstring .= "+$index";
                    }
                }
                if (!empty($resultstring)) {
					
					$newcompanytype = str_replace("+", "", $resultstring);
					if($companytype!=$newcompanytype)
					{
						 file_put_contents($filepathtxt, $companyID . ";new;$resultstring" . "\r\n", FILE_APPEND | LOCK_EX);
						 if($newcompanytype=="CUSTOMER"){$arFields_new["COMPANY_TYPE"] = "CUSTOMER";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="CUSTOMERкосячнаякомпания"){$arFields_new["COMPANY_TYPE"] = "CUSTOMERNOTKNOWN";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="2"){$arFields_new["COMPANY_TYPE"] = "2";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="косячнаякомпания"){$arFields_new["COMPANY_TYPE"] = "CUSTOMERNOTKNOWN";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="WITHOUTTYPE"){$arFields_new["COMPANY_TYPE"] = "WITHOUTTYPE";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="WITHOUTTYPEкосячнаякомпания"){$arFields_new["COMPANY_TYPE"] = "CUSTOMERNOTKNOWN";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="косячнаякомпания2"){$arFields_new["COMPANY_TYPE"] = "2NOTKNOWN";$oCompany->Update($companyID, $arFields_new);}						 
					}
					
                   
                }
            }
            break;
        case "287"://5
            //echo "Статус компании 3\n";
            $arraystatus = [];


            //Получаем тип компании
            $resultcompany = CCrmCompany::GetByID($companyID);
            $companytype = $resultcompany['COMPANY_TYPE'];

            if ($companytype != "") {
                //echo "Если тип компании какой-либо, кроме Клиент, то проверка наличия сделок.\n";
                $arFields["COMPANY_TYPE"] = "CUSTOMER";
                $arOrder = Array('DATE_CREATE' => 'DESC');
                $arFilter = Array("COMPANY_ID" => $companyID);
                // $arFilter["!=TYPE_ID"] = Array("2");
                $arSelect = Array("*");
                $result = $oDeal->GetListEx($arOrder, $arFilter, $arSelect, false);
                while ($fields = $result->Fetch()) {
                    $DealTypeArray = $fields;
                    if ($DealTypeArray['TYPE_ID'] == "COMPLEX") {
                        $newstatuscompany = "19";
                        $arFields["COMPANY_TYPE"] = $newstatuscompany;
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    } else if ($DealTypeArray['TYPE_ID'] == "GOODS") {
                        $newstatuscompany = "19";
                        $arFields["COMPANY_TYPE"] = $newstatuscompany;
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    } else if ($DealTypeArray['TYPE_ID'] == "SALE") {
                        $newstatuscompany = "2";
                        $arFields["COMPANY_TYPE"] = $newstatuscompany;
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    } else if ($DealTypeArray['TYPE_ID'] == "SERVICE") {
                        $newstatuscompany = "20";
                        $arFields["COMPANY_TYPE"] = $newstatuscompany;
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    } else if ($DealTypeArray['TYPE_ID'] == "SERVICES") {
                        $newstatuscompany = "19";
                        $arFields["COMPANY_TYPE"] = $newstatuscompany;
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    } else if ($DealTypeArray['TYPE_ID'] == "2") {
                        $newstatuscompany = "CUSTOMER";
                        //echo "Новый статус компании равен $newstatuscompany.\n";
                        $arFields["COMPANY_TYPE"] = $newstatuscompany;
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    } else {
                        $arFields["COMPANY_TYPE"] = "косячнаякомпания";
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    }

                }

                ksort($arraystatus);$resultstring = "";
                foreach ($arraystatus as $index => $elementvalue) {
                    if ($elementvalue > 0) {
                        $resultstring .= "+$index";
                    }
                }
                if (!empty($resultstring)) {
					
					$newcompanytype = str_replace("+", "", $resultstring);
					if($companytype!=$newcompanytype)
					{
						 file_put_contents($filepathtxt, $companyID . ";new;$resultstring" . "\r\n", FILE_APPEND | LOCK_EX);
						 if($newcompanytype=="CUSTOMER"){$arFields_new["COMPANY_TYPE"] = "CUSTOMER";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="CUSTOMERкосячнаякомпания"){$arFields_new["COMPANY_TYPE"] = "CUSTOMERNOTKNOWN";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="2"){$arFields_new["COMPANY_TYPE"] = "2";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="косячнаякомпания"){$arFields_new["COMPANY_TYPE"] = "CUSTOMERNOTKNOWN";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="WITHOUTTYPE"){$arFields_new["COMPANY_TYPE"] = "WITHOUTTYPE";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="WITHOUTTYPEкосячнаякомпания"){$arFields_new["COMPANY_TYPE"] = "CUSTOMERNOTKNOWN";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="косячнаякомпания2"){$arFields_new["COMPANY_TYPE"] = "2NOTKNOWN";$oCompany->Update($companyID, $arFields_new);}						 
					}
					
                   
                }


            }
            break;
        case "288"://6
            //echo "Статус компании 4\n";
            $arraystatus = [];
            //Установка тенанта компании
            // //SetUserField("CRM_COMPANY", $companyID, "UF_TENANT", "288");
            // //echo "Компании поставили в поле тенант Клиенты, с которыми расторгнут договор\n";

            //Получаем тип компании
            $resultcompany = CCrmCompany::GetByID($companyID);
            $companytype = $resultcompany['COMPANY_TYPE'];

            if ($companytype != "") {
                //echo "Если тип компании какой-либо, кроме Клиент, то проверка наличия сделок.\n";
                $arFields["COMPANY_TYPE"] = "CUSTOMER";
                $findflag = false;
                $arOrder = Array('DATE_CREATE' => 'DESC');
                $arFilter = Array("COMPANY_ID" => $companyID);
                $arFilter["!=STAGE_ID"] = Array("C1:EXIT", "C5:LOSE", "C4:LOSE", "C3:LOSE", "C2:LOSE", "C1:LOSE", "LOSE", "WON", "C1:WON", "C2:WON", "C3:WON", "C4:WON", "C5:WON");
                //$arFilter["!=TYPE_ID"] = Array("2");
                $arSelect = Array("*");
                $result = $oDeal->GetListEx($arOrder, $arFilter, $arSelect, false);
                while ($fields = $result->Fetch()) {
                    $DealTypeArray = $fields;
                    if (($DealTypeArray['TYPE_ID'] != "2") && ($DealTypeArray['TYPE_ID'] != "")) {
                        if ($DealTypeArray['TYPE_ID'] == "COMPLEX") {
                            $newstatuscompany = "19";
                            //echo "Новый статус компании равен $newstatuscompany.\n";
                            $arFields["COMPANY_TYPE"] = $newstatuscompany;
                            if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                                $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                            } else {
                                $arraystatus[$arFields["COMPANY_TYPE"]]++;
                            }
                        } else if ($DealTypeArray['TYPE_ID'] == "GOODS") {
                            $newstatuscompany = "19";
                            //echo "Новый статус компании равен $newstatuscompany.\n";
                            $arFields["COMPANY_TYPE"] = $newstatuscompany;
                            if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                                $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                            } else {
                                $arraystatus[$arFields["COMPANY_TYPE"]]++;
                            }
                        } else if ($DealTypeArray['TYPE_ID'] == "SALE") {
                            $newstatuscompany = "2";
                            //echo "Новый статус компании равен $newstatuscompany.\n";
                            $arFields["COMPANY_TYPE"] = $newstatuscompany;
                            if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                                $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                            } else {
                                $arraystatus[$arFields["COMPANY_TYPE"]]++;
                            }
                        } else if ($DealTypeArray['TYPE_ID'] == "SERVICE") {
                            $newstatuscompany = "20";
                            //echo "Новый статус компании равен $newstatuscompany.\n";
                            $arFields["COMPANY_TYPE"] = $newstatuscompany;
                            if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                                $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                            } else {
                                $arraystatus[$arFields["COMPANY_TYPE"]]++;
                            }
                        } else if ($DealTypeArray['TYPE_ID'] == "SERVICES") {
                            $newstatuscompany = "19";
                            //echo "Новый статус компании равен $newstatuscompany.\n";
                            $arFields["COMPANY_TYPE"] = $newstatuscompany;
                            if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                                $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                            } else {
                                $arraystatus[$arFields["COMPANY_TYPE"]]++;
                            }
                        } else if ($DealTypeArray['TYPE_ID'] == "2") {
                            $newstatuscompany = "CUSTOMER";
                            //echo "Новый статус компании равен $newstatuscompany.\n";
                            $arFields["COMPANY_TYPE"] = $newstatuscompany;
                            if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                                $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                            } else {
                                $arraystatus[$arFields["COMPANY_TYPE"]]++;
                            }
                        } else {
                            $arFields["COMPANY_TYPE"] = "косячнаякомпания";
                            if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                                $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                            } else {
                                $arraystatus[$arFields["COMPANY_TYPE"]]++;
                            }
                            //echo "Новый статус компании равен CUSTOMER.\n";
                            $arFields["COMPANY_TYPE"] = "CUSTOMER";
                            $entity_id = "CRM_COMPANY";
                            $uf_guid = "UF_STATUS";
                            //SetUserField($entity_id, $companyID, $uf_guid, "266");
                        }

                    }


                }
                ksort($arraystatus);$resultstring = "";
                foreach ($arraystatus as $index => $elementvalue) {
                    if ($elementvalue > 0) {
                        $resultstring .= "+$index";
                    }
                }
                if (!empty($resultstring)) {
					
					$newcompanytype = str_replace("+", "", $resultstring);
					if($companytype!=$newcompanytype)
					{
						 file_put_contents($filepathtxt, $companyID . ";new;$resultstring" . "\r\n", FILE_APPEND | LOCK_EX);
						 if($newcompanytype=="CUSTOMER"){$arFields_new["COMPANY_TYPE"] = "CUSTOMER";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="CUSTOMERкосячнаякомпания"){$arFields_new["COMPANY_TYPE"] = "CUSTOMERNOTKNOWN";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="2"){$arFields_new["COMPANY_TYPE"] = "2";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="косячнаякомпания"){$arFields_new["COMPANY_TYPE"] = "CUSTOMERNOTKNOWN";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="WITHOUTTYPE"){$arFields_new["COMPANY_TYPE"] = "WITHOUTTYPE";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="WITHOUTTYPEкосячнаякомпания"){$arFields_new["COMPANY_TYPE"] = "CUSTOMERNOTKNOWN";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="косячнаякомпания2"){$arFields_new["COMPANY_TYPE"] = "2NOTKNOWN";$oCompany->Update($companyID, $arFields_new);}						 
					}
					
                   
                }
            }
            break;
        case "289"://8
            //echo "Статус компании 6\n";
            $arraystatus = [];
            //Установка тенанта компании
            //SetUserField("CRM_COMPANY", $companyID, "UF_TENANT", "289");
            //echo "Компании поставили в поле тенант Тензор\n";


            //Получаем тип компании
            $resultcompany = CCrmCompany::GetByID($companyID);
            $companytype = $resultcompany['COMPANY_TYPE'];


            if ($companytype != "") {
                //echo "Если тип компании какой-либо, кроме Клиент, то проверка наличия сделок.\n";
                $arFields["COMPANY_TYPE"] = "CUSTOMER";
                $arOrder = Array('DATE_CREATE' => 'DESC');
                $arFilter = Array("COMPANY_ID" => $companyID);
                $arSelect = Array("*");
                //$arFilter["!=TYPE_ID"] = Array("CLIENTTYPE");
                $result = $oDeal->GetListEx($arOrder, $arFilter, $arSelect, false);
                while ($fields = $result->Fetch()) {
                    $DealTypeArray = $fields;
                    if ($DealTypeArray['TYPE_ID'] == "COMPLEX") {
                        $newstatuscompany = "19";
                        //echo "Новый статус компании равен $newstatuscompany.\n";
                        $arFields["COMPANY_TYPE"] = $newstatuscompany;
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    } else if ($DealTypeArray['TYPE_ID'] == "GOODS") {
                        $newstatuscompany = "19";
                        //echo "Новый статус компании равен $newstatuscompany.\n";
                        $arFields["COMPANY_TYPE"] = $newstatuscompany;
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    } else if ($DealTypeArray['TYPE_ID'] == "SALE") {
                        $newstatuscompany = "2";
                        //echo "Новый статус компании равен $newstatuscompany.\n";
                        $arFields["COMPANY_TYPE"] = $newstatuscompany;
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    } else if ($DealTypeArray['TYPE_ID'] == "SERVICE") {
                        $newstatuscompany = "20";
                        //echo "Новый статус компании равен $newstatuscompany.\n";
                        $arFields["COMPANY_TYPE"] = $newstatuscompany;
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    } else if ($DealTypeArray['TYPE_ID'] == "SERVICES") {
                        $newstatuscompany = "19";
                        //echo "Новый статус компании равен $newstatuscompany.\n";
                        $arFields["COMPANY_TYPE"] = $newstatuscompany;
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    } else if ($DealTypeArray['TYPE_ID'] == "2") {
                        $newstatuscompany = "CUSTOMER";
                        //echo "Новый статус компании равен $newstatuscompany.\n";
                        $arFields["COMPANY_TYPE"] = $newstatuscompany;
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    } else {
                        $arFields["COMPANY_TYPE"] = "косячнаякомпания";
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    }

                }
                ksort($arraystatus);$resultstring = "";
                foreach ($arraystatus as $index => $elementvalue) {
                    if ($elementvalue > 0) {
                        $resultstring .= "+$index";
                    }
                }
                if (!empty($resultstring)) {
					
					$newcompanytype = str_replace("+", "", $resultstring);
					if($companytype!=$newcompanytype)
					{
						 file_put_contents($filepathtxt, $companyID . ";new;$resultstring" . "\r\n", FILE_APPEND | LOCK_EX);
						 if($newcompanytype=="CUSTOMER"){$arFields_new["COMPANY_TYPE"] = "CUSTOMER";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="CUSTOMERкосячнаякомпания"){$arFields_new["COMPANY_TYPE"] = "CUSTOMERNOTKNOWN";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="2"){$arFields_new["COMPANY_TYPE"] = "2";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="косячнаякомпания"){$arFields_new["COMPANY_TYPE"] = "CUSTOMERNOTKNOWN";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="WITHOUTTYPE"){$arFields_new["COMPANY_TYPE"] = "WITHOUTTYPE";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="WITHOUTTYPEкосячнаякомпания"){$arFields_new["COMPANY_TYPE"] = "CUSTOMERNOTKNOWN";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="косячнаякомпания2"){$arFields_new["COMPANY_TYPE"] = "2NOTKNOWN";$oCompany->Update($companyID, $arFields_new);}						 
					}
					
                   
                }


            }
            break;
        case "290"://10
            //echo "Статус компании 8\n";
            //Установка тенанта компании
            //SetUserField("CRM_COMPANY", $companyID, "UF_TENANT", "290");
            //echo "Компании поставили в поле тенант Без блокировки касс\n";
            $arraystatus = [];
            $arFields["COMMENTS"] = "у клиента нет блокировки касс";
            //Получаем тип компании
            $resultcompany = CCrmCompany::GetByID($companyID);
            $companytype = $resultcompany['COMPANY_TYPE'];


            if ($companytype != "") {
                //echo "Если тип компании какой-либо, кроме Клиент, то проверка наличия сделок.\n";
                $arFields["COMPANY_TYPE"] = "CUSTOMER";
                $arOrder = Array('DATE_CREATE' => 'DESC');
                $arFilter = Array("COMPANY_ID" => $companyID);
                //$arFilter["!=TYPE_ID"] = Array("2");
                $arSelect = Array("*");
                $result = $oDeal->GetListEx($arOrder, $arFilter, $arSelect, false);
                while ($fields = $result->Fetch()) {
                    $DealTypeArray = $fields;
                    if ($DealTypeArray['TYPE_ID'] == "COMPLEX") {
                        $newstatuscompany = "19";
                        //echo "Новый статус компании равен $newstatuscompany.\n";
                        $arFields["COMPANY_TYPE"] = $newstatuscompany;
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    } else if ($DealTypeArray['TYPE_ID'] == "GOODS") {
                        $newstatuscompany = "19";
                        //echo "Новый статус компании равен $newstatuscompany.\n";
                        $arFields["COMPANY_TYPE"] = $newstatuscompany;
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    } else if ($DealTypeArray['TYPE_ID'] == "SALE") {
                        $newstatuscompany = "2";
                        //echo "Новый статус компании равен $newstatuscompany.\n";
                        $arFields["COMPANY_TYPE"] = $newstatuscompany;
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    } else if ($DealTypeArray['TYPE_ID'] == "SERVICE") {
                        $newstatuscompany = "20";
                        //echo "Новый статус компании равен $newstatuscompany.\n";
                        $arFields["COMPANY_TYPE"] = $newstatuscompany;
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    } else if ($DealTypeArray['TYPE_ID'] == "SERVICES") {
                        $newstatuscompany = "19";
                        //echo "Новый статус компании равен $newstatuscompany.\n";
                        $arFields["COMPANY_TYPE"] = $newstatuscompany;
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    } else if ($DealTypeArray['TYPE_ID'] == "2") {
                        $newstatuscompany = "CUSTOMER";
                        //echo "Новый статус компании равен $newstatuscompany.\n";
                        $arFields["COMPANY_TYPE"] = $newstatuscompany;
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    } else {
                        $arFields["COMPANY_TYPE"] = "косячнаякомпания";
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    }

                }

                ksort($arraystatus);$resultstring = "";
                foreach ($arraystatus as $index => $elementvalue) {
                    if ($elementvalue > 0) {
                        $resultstring .= "+$index";
                    }
                }
                if (!empty($resultstring)) {
					
					$newcompanytype = str_replace("+", "", $resultstring);
					if($companytype!=$newcompanytype)
					{
						 file_put_contents($filepathtxt, $companyID . ";new;$resultstring" . "\r\n", FILE_APPEND | LOCK_EX);
						 if($newcompanytype=="CUSTOMER"){$arFields_new["COMPANY_TYPE"] = "CUSTOMER";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="CUSTOMERкосячнаякомпания"){$arFields_new["COMPANY_TYPE"] = "CUSTOMERNOTKNOWN";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="2"){$arFields_new["COMPANY_TYPE"] = "2";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="косячнаякомпания"){$arFields_new["COMPANY_TYPE"] = "CUSTOMERNOTKNOWN";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="WITHOUTTYPE"){$arFields_new["COMPANY_TYPE"] = "WITHOUTTYPE";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="WITHOUTTYPEкосячнаякомпания"){$arFields_new["COMPANY_TYPE"] = "CUSTOMERNOTKNOWN";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="косячнаякомпания2"){$arFields_new["COMPANY_TYPE"] = "2NOTKNOWN";$oCompany->Update($companyID, $arFields_new);}						 
					}
					
                   
                }


            }
            break;
        case "291"://11
            //echo "Статус компании PARTNER\n";
            $arraystatus = [];
            //Установка тенанта компании
            //SetUserField("CRM_COMPANY", $companyID, "UF_TENANT", "291");
            //echo "Компании поставили в поле тенант Агент\n";

            $resultcompany = CCrmCompany::GetByID($companyID);
            $companytype = $resultcompany['COMPANY_TYPE'];


            if ($companytype != "") {
                //echo "Если тип компании какой-либо, кроме Агент, то проверка наличия сделок.\n";
                $newstatuscompany = "AGENT";
                $findflag = false;
                $arOrder = Array('DATE_CREATE' => 'DESC');
                $arFilter = Array("COMPANY_ID" => $companyID);
                // $arFilter["!=TYPE_ID"] = Array("SALE");
                $arSelect = Array("*");
                $result = $oDeal->GetListEx($arOrder, $arFilter, $arSelect, false);
                while ($fields = $result->Fetch()) {
                    $DealTypeArray = $fields;
                    if ($DealTypeArray['TYPE_ID'] == "2") {
                        $newstatuscompany = "2";
                        $findflag = true;
                        //echo "Новый статус компании равен $newstatuscompany.\n";
                        $arFields["COMPANY_TYPE"] = $newstatuscompany;
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    } else if ($DealTypeArray['TYPE_ID'] == "COMPLEX") {
                        $newstatuscompany = "AGENTBYUER";
                        $findflag = true;
                        //echo "Новый статус компании равен $newstatuscompany.\n";
                        $arFields["COMPANY_TYPE"] = $newstatuscompany;
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    } else if ($DealTypeArray['TYPE_ID'] == "GOODS") {
                        $newstatuscompany = "AGENTBYUER";
                        $findflag = true;
                        //echo "Новый статус компании равен $newstatuscompany.\n";
                        $arFields["COMPANY_TYPE"] = $newstatuscompany;
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    } else if ($DealTypeArray['TYPE_ID'] == "SERVICE") {
                        $newstatuscompany = "AGENTPOST";
                        $findflag = true;
                        //echo "Новый статус компании равен $newstatuscompany.\n";
                        $arFields["COMPANY_TYPE"] = $newstatuscompany;
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    } else if ($DealTypeArray['TYPE_ID'] == "SERVICES") {
                        $newstatuscompany = "AGENTBYUER";
                        $findflag = true;
                        //echo "Новый статус компании равен $newstatuscompany.\n";
                        $arFields["COMPANY_TYPE"] = $newstatuscompany;
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    } else if ($DealTypeArray['TYPE_ID'] == "SALE") {
                        $newstatuscompany = "AGENT";
                        //echo "Новый статус компании равен $newstatuscompany.\n";
                        $arFields["COMPANY_TYPE"] = $newstatuscompany;
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    } else {
                        $arFields["COMPANY_TYPE"] = "косячнаякомпания";
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    }

                }


                ksort($arraystatus);$resultstring = "";
                foreach ($arraystatus as $index => $elementvalue) {
                    if ($elementvalue > 0) {
                        $resultstring .= "+$index";
                    }
                }
                if (!empty($resultstring)) {
					
					$newcompanytype = str_replace("+", "", $resultstring);
					if($companytype!=$newcompanytype)
					{
						 file_put_contents($filepathtxt, $companyID . ";new;$resultstring" . "\r\n", FILE_APPEND | LOCK_EX);
						 if($newcompanytype=="CUSTOMER"){$arFields_new["COMPANY_TYPE"] = "CUSTOMER";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="CUSTOMERкосячнаякомпания"){$arFields_new["COMPANY_TYPE"] = "CUSTOMERNOTKNOWN";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="2"){$arFields_new["COMPANY_TYPE"] = "2";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="косячнаякомпания"){$arFields_new["COMPANY_TYPE"] = "AGENTNOTKNOWN";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="WITHOUTTYPE"){$arFields_new["COMPANY_TYPE"] = "WITHOUTTYPE";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="AGENT"){$arFields_new["COMPANY_TYPE"] = "AGENT";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="WITHOUTTYPEкосячнаякомпания"){$arFields_new["COMPANY_TYPE"] = "CUSTOMERNOTKNOWN";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="косячнаякомпания2"){$arFields_new["COMPANY_TYPE"] = "2NOTKNOWN";$oCompany->Update($companyID, $arFields_new);}						 
					}
					
                   
                }

            }
            break;
        case "297"://12 TODO
            //echo "Статус компании 9\n";
            $resultcompany = CCrmCompany::GetByID($companyID);
            $companytype = $resultcompany['COMPANY_TYPE'];
            $arraystatus = [];


            if (($companytype != "")
                && ($companytype != "")
                && ($companytype != "")
            ) {
                $arFields["COMPANY_TYPE"] = "WITHOUTTYPE";
                $findflag = false;
                $arOrder = Array('DATE_CREATE' => 'DESC');
                $arFilter = Array("COMPANY_ID" => $companyID);
                // $arFilter["!=TYPE_ID"] = Array("2","SALE");
                $arSelect = Array("*");
                $result = $oDeal->GetListEx($arOrder, $arFilter, $arSelect, false);
                while ($fields = $result->Fetch()) {
                    $DealTypeArray = $fields;
                    if ($DealTypeArray['TYPE_ID'] == "COMPLEX") {
                        $newstatuscompany = "19";
                        //echo "Новый статус компании равен $newstatuscompany.\n";
                        $arFields["COMPANY_TYPE"] = $newstatuscompany;
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    } else if ($DealTypeArray['TYPE_ID'] == "GOODS") {
                        $newstatuscompany = "19";
                        //echo "Новый статус компании равен $newstatuscompany.\n";
                        $arFields["COMPANY_TYPE"] = $newstatuscompany;
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    } else if ($DealTypeArray['TYPE_ID'] == "SALE") {
                        $newstatuscompany = "2";
                        //echo "Новый статус компании равен $newstatuscompany.\n";
                        $arFields["COMPANY_TYPE"] = $newstatuscompany;
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    } else if ($DealTypeArray['TYPE_ID'] == "SERVICE") {
                        $newstatuscompany = "20";
                        //echo "Новый статус компании равен $newstatuscompany.\n";
                        $arFields["COMPANY_TYPE"] = $newstatuscompany;
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    } else if ($DealTypeArray['TYPE_ID'] == "SERVICES") {
                        $newstatuscompany = "19";
                        //echo "Новый статус компании равен $newstatuscompany.\n";
                        $arFields["COMPANY_TYPE"] = $newstatuscompany;
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    } else if ($DealTypeArray['TYPE_ID'] == "2") {
                        $newstatuscompany = "WITHOUTTYPE";
                        //echo "Новый статус компании равен $newstatuscompany.\n";
                        $arFields["COMPANY_TYPE"] = $newstatuscompany;
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    } else if ($DealTypeArray['TYPE_ID'] == "SALE") {
                        $newstatuscompany = "WITHOUTTYPE";
                        //echo "Новый статус компании равен $newstatuscompany.\n";
                        $arFields["COMPANY_TYPE"] = $newstatuscompany;
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    } else {
                        $arFields["COMPANY_TYPE"] = "косячнаякомпания";
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    }

                }
                ksort($arraystatus);$resultstring = "";
                foreach ($arraystatus as $index => $elementvalue) {
                    if ($elementvalue > 0) {
                        $resultstring .= "+$index";
                    }
                }
                if (!empty($resultstring)) {
					
					$newcompanytype = str_replace("+", "", $resultstring);
					if($companytype!=$newcompanytype)
					{
						 file_put_contents($filepathtxt, $companyID . ";new;$resultstring" . "\r\n", FILE_APPEND | LOCK_EX);
						 if($newcompanytype=="CUSTOMER"){$arFields_new["COMPANY_TYPE"] = "CUSTOMER";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="CUSTOMERкосячнаякомпания"){$arFields_new["COMPANY_TYPE"] = "CUSTOMERNOTKNOWN";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="2"){$arFields_new["COMPANY_TYPE"] = "2";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="косячнаякомпания"){$arFields_new["COMPANY_TYPE"] = "CUSTOMERNOTKNOWN";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="WITHOUTTYPE"){$arFields_new["COMPANY_TYPE"] = "WITHOUTTYPE";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="WITHOUTTYPEкосячнаякомпания"){$arFields_new["COMPANY_TYPE"] = "CUSTOMERNOTKNOWN";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="косячнаякомпания2"){$arFields_new["COMPANY_TYPE"] = "2NOTKNOWN";$oCompany->Update($companyID, $arFields_new);}						 
					}
					
                   
                }


            }
            break;
        case "292"://13
            //echo "Статус компании 13\n";
            $arraystatus = [];
            //Установка тенанта компании
            //SetUserField("CRM_COMPANY", $companyID, "UF_TENANT", "292");
            //echo "Компании поставили в поле тенант 1c\n";

            $resultcompany = CCrmCompany::GetByID($companyID);
            $companytype = $resultcompany['COMPANY_TYPE'];


            if ($companytype != "") {
                //echo "Если тип компании какой-либо, кроме Клиент, то проверка наличия сделок.\n";
                $arFields["COMPANY_TYPE"] = "CUSTOMER";
                $arOrder = Array('DATE_CREATE' => 'DESC');
                $arFilter = Array("COMPANY_ID" => $companyID);
                $arSelect = Array("*");
                $result = $oDeal->GetListEx($arOrder, $arFilter, $arSelect, false);
                while ($fields = $result->Fetch()) {
                    $DealTypeArray = $fields;
                    if ($DealTypeArray['TYPE_ID'] == "COMPLEX") {
                        $newstatuscompany = "19";
                        //echo "Новый статус компании равен $newstatuscompany.\n";
                        $arFields["COMPANY_TYPE"] = $newstatuscompany;
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    } else if ($DealTypeArray['TYPE_ID'] == "GOODS") {
                        $newstatuscompany = "19";
                        //echo "Новый статус компании равен $newstatuscompany.\n";
                        $arFields["COMPANY_TYPE"] = $newstatuscompany;
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    } else if ($DealTypeArray['TYPE_ID'] == "SALE") {
                        $newstatuscompany = "2";
                        //echo "Новый статус компании равен $newstatuscompany.\n";
                        $arFields["COMPANY_TYPE"] = $newstatuscompany;
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    } else if ($DealTypeArray['TYPE_ID'] == "SERVICE") {
                        $newstatuscompany = "20";
                        //echo "Новый статус компании равен $newstatuscompany.\n";
                        $arFields["COMPANY_TYPE"] = $newstatuscompany;
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    } else if ($DealTypeArray['TYPE_ID'] == "SERVICES") {
                        $newstatuscompany = "19";
                        //echo "Новый статус компании равен $newstatuscompany.\n";
                        $arFields["COMPANY_TYPE"] = $newstatuscompany;
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    } else if ($DealTypeArray['TYPE_ID'] == "2") {
                        $newstatuscompany = "CUSTOMER";
                        //echo "Новый статус компании равен $newstatuscompany.\n";
                        $arFields["COMPANY_TYPE"] = $newstatuscompany;
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    } else {
                        $arFields["COMPANY_TYPE"] = "косячнаякомпания";
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    }
                }

                ksort($arraystatus);$resultstring = "";
                foreach ($arraystatus as $index => $elementvalue) {
                    if ($elementvalue > 0) {
                        $resultstring .= "+$index";
                    }
                }
                if (!empty($resultstring)) {
					
					$newcompanytype = str_replace("+", "", $resultstring);
					if($companytype!=$newcompanytype)
					{
						 file_put_contents($filepathtxt, $companyID . ";new;$resultstring" . "\r\n", FILE_APPEND | LOCK_EX);
						 if($newcompanytype=="CUSTOMER"){$arFields_new["COMPANY_TYPE"] = "CUSTOMER";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="CUSTOMERкосячнаякомпания"){$arFields_new["COMPANY_TYPE"] = "CUSTOMERNOTKNOWN";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="2"){$arFields_new["COMPANY_TYPE"] = "2";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="косячнаякомпания"){$arFields_new["COMPANY_TYPE"] = "CUSTOMERNOTKNOWN";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="WITHOUTTYPE"){$arFields_new["COMPANY_TYPE"] = "WITHOUTTYPE";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="WITHOUTTYPEкосячнаякомпания"){$arFields_new["COMPANY_TYPE"] = "CUSTOMERNOTKNOWN";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="косячнаякомпания2"){$arFields_new["COMPANY_TYPE"] = "2NOTKNOWN";$oCompany->Update($companyID, $arFields_new);}						 
					}
					
                   
                }


            }
            break;
        case "293"://14
            //echo "Статус компании 2\n";
            $arraystatus = [];
            //Установка тенанта компании
            //SetUserField("CRM_COMPANY", $companyID, "UF_TENANT", "293");
            //echo "Компании поставили в поле тенант Клиент+агент\n";

            //Получаем тип компании
            $resultcompany = CCrmCompany::GetByID($companyID);
            $companytype = $resultcompany['COMPANY_TYPE'];


            if ($companytype != "") {
                //echo "Если тип компании какой-либо, кроме Клиент, то проверка наличия сделок.\n";
                $arFields["COMPANY_TYPE"] = "2";
                $findflag = false;
                $arOrder = Array('DATE_CREATE' => 'DESC');
                $arFilter = Array("COMPANY_ID" => $companyID);
                //$arFilter["!=TYPE_ID"] = Array("2");
                $arSelect = Array("*");
                $result = $oDeal->GetListEx($arOrder, $arFilter, $arSelect, false);
                while ($fields = $result->Fetch()) {
                    $DealTypeArray = $fields;
                    if ($DealTypeArray['TYPE_ID'] == "COMPLEX") {
                        $newstatuscompany = "CLIENTAGENTBUYER";
                        $findflag = true;
                        //echo "Новый статус компании равен $newstatuscompany.\n";
                        $arFields["COMPANY_TYPE"] = $newstatuscompany;
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    } else if ($DealTypeArray['TYPE_ID'] == "GOODS") {
                        $newstatuscompany = "CLIENTAGENTBUYER";
                        $findflag = true;
                        //echo "Новый статус компании равен $newstatuscompany.\n";
                        $arFields["COMPANY_TYPE"] = $newstatuscompany;
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    } else if ($DealTypeArray['TYPE_ID'] == "SALE") {
                        $newstatuscompany = "2";
                        $findflag = true;
                        //echo "Новый статус компании равен $newstatuscompany.\n";
                        $arFields["COMPANY_TYPE"] = $newstatuscompany;
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    } else if ($DealTypeArray['TYPE_ID'] == "SERVICE") {
                        $newstatuscompany = "CLIENTAGENTPOST";
                        $findflag = true;
                        //echo "Новый статус компании равен $newstatuscompany.\n";
                        $arFields["COMPANY_TYPE"] = $newstatuscompany;
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    } else if ($DealTypeArray['TYPE_ID'] == "SERVICES") {
                        $newstatuscompany = "CLIENTAGENTBUYER";
                        $findflag = true;
                        //echo "Новый статус компании равен $newstatuscompany.\n";
                        $arFields["COMPANY_TYPE"] = $newstatuscompany;
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
						
                    }else if ($DealTypeArray['TYPE_ID'] == "2") {
                                $newstatuscompany = "2";
                                //echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"]   = $newstatuscompany;
									if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
									else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}
                    }else {
                        $arFields["COMPANY_TYPE"] = "косячнаякомпания";
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    }

                }


                ksort($arraystatus);$resultstring = "";
                foreach ($arraystatus as $index => $elementvalue) {
                    if ($elementvalue > 0) {
                        $resultstring .= "+$index";
                    }
                }
                if (!empty($resultstring)) {
					
					$newcompanytype = str_replace("+", "", $resultstring);
					if($companytype!=$newcompanytype)
					{
						 file_put_contents($filepathtxt, $companyID . ";new;$resultstring" . "\r\n", FILE_APPEND | LOCK_EX);
						 if($newcompanytype=="CUSTOMER"){$arFields_new["COMPANY_TYPE"] = "CUSTOMER";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="CUSTOMERкосячнаякомпания"){$arFields_new["COMPANY_TYPE"] = "CUSTOMERNOTKNOWN";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="2"){$arFields_new["COMPANY_TYPE"] = "2";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="косячнаякомпания"){$arFields_new["COMPANY_TYPE"] = "2NOTKNOWN";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="WITHOUTTYPE"){$arFields_new["COMPANY_TYPE"] = "WITHOUTTYPE";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="WITHOUTTYPEкосячнаякомпания"){$arFields_new["COMPANY_TYPE"] = "CUSTOMERNOTKNOWN";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="косячнаякомпания2"){$arFields_new["COMPANY_TYPE"] = "2NOTKNOWN";$oCompany->Update($companyID, $arFields_new);}						 
					}
					
                   
                }

            }
            break;
        case "294"://15
            //echo "Статус компании 10\n";
            $arraystatus = [];
            //Установка тенанта компании
            //SetUserField("CRM_COMPANY", $companyID, "UF_TENANT", "294");
            //echo "Компании поставили в поле тенант Корпорация\n";

            //Получаем тип компании
            $resultcompany = CCrmCompany::GetByID($companyID);
            $companytype = $resultcompany['COMPANY_TYPE'];


            if ($companytype != "") {
                //echo "Если тип компании какой-либо, кроме Клиент, то проверка наличия сделок.\n";
                $arFields["COMPANY_TYPE"] = "CUSTOMER";
                $arOrder = Array('DATE_CREATE' => 'DESC');
                $arFilter = Array("COMPANY_ID" => $companyID);
                $arFilter["!=TYPE_ID"] = Array("2");
                $arSelect = Array("*");
                $result = $oDeal->GetListEx($arOrder, $arFilter, $arSelect, false);
                while ($fields = $result->Fetch()) {
                    $DealTypeArray = $fields;
                    if ($DealTypeArray['TYPE_ID'] == "COMPLEX") {
                        $newstatuscompany = "19";
                        //echo "Новый статус компании равен $newstatuscompany.\n";
                        $arFields["COMPANY_TYPE"] = $newstatuscompany;
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    } else if ($DealTypeArray['TYPE_ID'] == "GOODS") {
                        $newstatuscompany = "19";
                        //echo "Новый статус компании равен $newstatuscompany.\n";
                        $arFields["COMPANY_TYPE"] = $newstatuscompany;
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    } else if ($DealTypeArray['TYPE_ID'] == "SALE") {
                        $newstatuscompany = "2";
                        //echo "Новый статус компании равен $newstatuscompany.\n";
                        $arFields["COMPANY_TYPE"] = $newstatuscompany;
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    } else if ($DealTypeArray['TYPE_ID'] == "SERVICE") {
                        $newstatuscompany = "20";
                        //echo "Новый статус компании равен $newstatuscompany.\n";
                        $arFields["COMPANY_TYPE"] = $newstatuscompany;
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    } else if ($DealTypeArray['TYPE_ID'] == "SERVICES") {
                        $newstatuscompany = "19";
                        //echo "Новый статус компании равен $newstatuscompany.\n";
                        $arFields["COMPANY_TYPE"] = $newstatuscompany;
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    }else if ($DealTypeArray['TYPE_ID'] == "2") {
                                $newstatuscompany = "CUSTOMER";
                                //echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"]   = $newstatuscompany;
									if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
									else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}
                    }else {
                        $arFields["COMPANY_TYPE"] = "косячнаякомпания";
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    }
                }

                ksort($arraystatus);$resultstring = "";
                foreach ($arraystatus as $index => $elementvalue) {
                    if ($elementvalue > 0) {
                        $resultstring .= "+$index";
                    }
                }
                if (!empty($resultstring)) {
					
					$newcompanytype = str_replace("+", "", $resultstring);
					if($companytype!=$newcompanytype)
					{
						 file_put_contents($filepathtxt, $companyID . ";new;$resultstring" . "\r\n", FILE_APPEND | LOCK_EX);
						 if($newcompanytype=="CUSTOMER"){$arFields_new["COMPANY_TYPE"] = "CUSTOMER";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="CUSTOMERкосячнаякомпания"){$arFields_new["COMPANY_TYPE"] = "CUSTOMERNOTKNOWN";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="2"){$arFields_new["COMPANY_TYPE"] = "2";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="косячнаякомпания"){$arFields_new["COMPANY_TYPE"] = "CUSTOMERNOTKNOWN";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="WITHOUTTYPE"){$arFields_new["COMPANY_TYPE"] = "WITHOUTTYPE";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="WITHOUTTYPEкосячнаякомпания"){$arFields_new["COMPANY_TYPE"] = "CUSTOMERNOTKNOWN";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="косячнаякомпания2"){$arFields_new["COMPANY_TYPE"] = "2NOTKNOWN";$oCompany->Update($companyID, $arFields_new);}						 
					}
					
                   
                }


            }
            break;
        case "295"://16
            //echo "Статус компании 16\n";
            //Установка тенанта компании
            //SetUserField("CRM_COMPANY", $companyID, "UF_TENANT", "295");
            //echo "Компании поставили в поле тенант Атол-онлайн\n";
            $arraystatus = [];
            //Получаем тип компании
            $resultcompany = CCrmCompany::GetByID($companyID);
            $companytype = $resultcompany['COMPANY_TYPE'];


            if ($companytype != "") {
                //echo "Если тип компании какой-либо, кроме Клиент, то проверка наличия сделок.\n";
                $arFields["COMPANY_TYPE"] = "CUSTOMER";
                $findflag = false;
                $arOrder = Array('DATE_CREATE' => 'DESC');
                $arFilter = Array("COMPANY_ID" => $companyID);
                $arSelect = Array("*");
                $arFilter["!=TYPE_ID"] = Array("2");
                $result = $oDeal->GetListEx($arOrder, $arFilter, $arSelect, false);
                while ($fields = $result->Fetch()) {
                    $DealTypeArray = $fields;
                    if ($DealTypeArray['TYPE_ID'] == "COMPLEX") {
                        $newstatuscompany = "19";
                        //echo "Новый статус компании равен $newstatuscompany.\n";
                        $arFields["COMPANY_TYPE"] = $newstatuscompany;
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    } else if ($DealTypeArray['TYPE_ID'] == "GOODS") {
                        $newstatuscompany = "19";
                        //echo "Новый статус компании равен $newstatuscompany.\n";
                        $arFields["COMPANY_TYPE"] = $newstatuscompany;
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    } else if ($DealTypeArray['TYPE_ID'] == "SALE") {
                        $newstatuscompany = "2";
                        //echo "Новый статус компании равен $newstatuscompany.\n";
                        $arFields["COMPANY_TYPE"] = $newstatuscompany;
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    } else if ($DealTypeArray['TYPE_ID'] == "SERVICE") {
                        $newstatuscompany = "20";
                        //echo "Новый статус компании равен $newstatuscompany.\n";
                        $arFields["COMPANY_TYPE"] = $newstatuscompany;
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    } else if ($DealTypeArray['TYPE_ID'] == "SERVICES") {
                        $newstatuscompany = "19";
                        //echo "Новый статус компании равен $newstatuscompany.\n";
                        $arFields["COMPANY_TYPE"] = $newstatuscompany;
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    }else if ($DealTypeArray['TYPE_ID'] == "2") {
                                $newstatuscompany = "CUSTOMER";
                                //echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"]   = $newstatuscompany;
									if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
									else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}
                    }
					else {
                        $arFields["COMPANY_TYPE"] = "косячнаякомпания";
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    }
                }

                ksort($arraystatus);$resultstring = "";
                foreach ($arraystatus as $index => $elementvalue) {
                    if ($elementvalue > 0) {
                        $resultstring .= "+$index";
                    }
                }
                if (!empty($resultstring)) {
					
					$newcompanytype = str_replace("+", "", $resultstring);
					if($companytype!=$newcompanytype)
					{
						 file_put_contents($filepathtxt, $companyID . ";new;$resultstring" . "\r\n", FILE_APPEND | LOCK_EX);
						 if($newcompanytype=="CUSTOMER"){$arFields_new["COMPANY_TYPE"] = "CUSTOMER";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="CUSTOMERкосячнаякомпания"){$arFields_new["COMPANY_TYPE"] = "CUSTOMERNOTKNOWN";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="2"){$arFields_new["COMPANY_TYPE"] = "2";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="косячнаякомпания"){$arFields_new["COMPANY_TYPE"] = "CUSTOMERNOTKNOWN";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="WITHOUTTYPE"){$arFields_new["COMPANY_TYPE"] = "WITHOUTTYPE";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="WITHOUTTYPEкосячнаякомпания"){$arFields_new["COMPANY_TYPE"] = "CUSTOMERNOTKNOWN";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="косячнаякомпания2"){$arFields_new["COMPANY_TYPE"] = "2NOTKNOWN";$oCompany->Update($companyID, $arFields_new);}						 
					}
					
                   
                }


            }
            break;
        case "296"://17
            //echo "Статус компании 17\n";
            $arraystatus = [];
            //Установка тенанта компании
            //SetUserField("CRM_COMPANY", $companyID, "UF_TENANT", "296");
            //echo "Компании поставили в поле тенант Старрус\n";

            //Получаем тип компании
            $resultcompany = CCrmCompany::GetByID($companyID);
            $companytype = $resultcompany['COMPANY_TYPE'];


            if ($companytype != "") {
                //echo "Если тип компании какой-либо, кроме Клиент, то проверка наличия сделок.\n";
                $arFields["COMPANY_TYPE"] = "CUSTOMER";
                $arOrder = Array('DATE_CREATE' => 'DESC');
                $arFilter = Array("COMPANY_ID" => $companyID);
                //$arFilter["!=TYPE_ID"] = Array("2");
                $arSelect = Array("*");
                $result = $oDeal->GetListEx($arOrder, $arFilter, $arSelect, false);
                while ($fields = $result->Fetch()) {
                    $DealTypeArray = $fields;
                    if ($DealTypeArray['TYPE_ID'] == "COMPLEX") {
                        $newstatuscompany = "19";
                        //echo "Новый статус компании равен $newstatuscompany.\n";
                        $arFields["COMPANY_TYPE"] = $newstatuscompany;
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    } else if ($DealTypeArray['TYPE_ID'] == "GOODS") {
                        $newstatuscompany = "19";
                        //echo "Новый статус компании равен $newstatuscompany.\n";
                        $arFields["COMPANY_TYPE"] = $newstatuscompany;
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    } else if ($DealTypeArray['TYPE_ID'] == "SALE") {
                        $newstatuscompany = "2";
                        //echo "Новый статус компании равен $newstatuscompany.\n";
                        $arFields["COMPANY_TYPE"] = $newstatuscompany;
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    } else if ($DealTypeArray['TYPE_ID'] == "SERVICE") {
                        $newstatuscompany = "20";
                        //echo "Новый статус компании равен $newstatuscompany.\n";
                        $arFields["COMPANY_TYPE"] = $newstatuscompany;
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    } else if ($DealTypeArray['TYPE_ID'] == "SERVICES") {
                        $newstatuscompany = "19";
                        //echo "Новый статус компании равен $newstatuscompany.\n";
                        $arFields["COMPANY_TYPE"] = $newstatuscompany;
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    } else if ($DealTypeArray['TYPE_ID'] == "2") {
                        $newstatuscompany = "CUSTOMER";
                        //echo "Новый статус компании равен $newstatuscompany.\n";
                        $arFields["COMPANY_TYPE"] = $newstatuscompany;
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    } else {
                        $arFields["COMPANY_TYPE"] = "косячнаякомпания";
                        if (empty($arraystatus[$arFields["COMPANY_TYPE"]])) {
                            $arraystatus[$arFields["COMPANY_TYPE"]] = 1;
                        } else {
                            $arraystatus[$arFields["COMPANY_TYPE"]]++;
                        }
                    }
                }

                ksort($arraystatus);$resultstring = "";
                foreach ($arraystatus as $index => $elementvalue) {
                    if ($elementvalue > 0) {
                        $resultstring .= "+$index";
                    }
                }

                if (!empty($resultstring)) {
					
					$newcompanytype = str_replace("+", "", $resultstring);
					if($companytype!=$newcompanytype)
					{
						 file_put_contents($filepathtxt, $companyID . ";new;$resultstring" . "\r\n", FILE_APPEND | LOCK_EX);
						 if($newcompanytype=="CUSTOMER"){$arFields_new["COMPANY_TYPE"] = "CUSTOMER";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="CUSTOMERкосячнаякомпания"){$arFields_new["COMPANY_TYPE"] = "CUSTOMERNOTKNOWN";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="2"){$arFields_new["COMPANY_TYPE"] = "2";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="косячнаякомпания"){$arFields_new["COMPANY_TYPE"] = "CUSTOMERNOTKNOWN";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="WITHOUTTYPE"){$arFields_new["COMPANY_TYPE"] = "WITHOUTTYPE";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="WITHOUTTYPEкосячнаякомпания"){$arFields_new["COMPANY_TYPE"] = "CUSTOMERNOTKNOWN";$oCompany->Update($companyID, $arFields_new);}
						 else if($newcompanytype=="косячнаякомпания2"){$arFields_new["COMPANY_TYPE"] = "2NOTKNOWN";$oCompany->Update($companyID, $arFields_new);}						 
					}
					
                   
                }


            }
            break;
        default:
            break;
    }


}
