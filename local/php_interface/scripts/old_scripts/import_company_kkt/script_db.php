<?php
/*
 * Выполняет добавление новых элементов в ИБ
 * с символьным кодом “import_company“ (сам ИБ найти не смог).
 * Данные берутся из файла import_company_bpm.csv
 */


define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('CHK_EVENT', true);
ini_set('memory_limit', '-1');

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

@set_time_limit(0);
@ignore_user_abort(true);

global $USER;
$USER->Authorize(372);

CModule::IncludeModule('iblock');
$oIBlockElement = new CIBlockElement();
$IBLOCK_ID = getIblockIDByCode('import_company');

$arTenantCompanyType = array(
	"0"     => "0", //Тест
    "1"     => "CUSTOMER", //ОФД RU. Клиенты
    "5"     => "3", //ОФД RU. Клиенты, анонсировавшие расторжение
    "6"     => "4", //ОФД RU. Бывшие клиенты
    "7"     => "5", //ОФД RU. Бизнес Администрирование
    "8"     => "6", //Тензор
    "9"     => "7", //Рапкат
    "10"    => "8", //Без блокировки касс
    "11"    => "PARTNER", //Агент/Партнер
    "12"    => "9", //Временные пользователи
	"13"    => "13", //1c
    "14"    => "2", //Клиент + Агент
    "15"    => "10", //Корпорации
	"16"    => "16", //Атол-онлайн
    "17"    => "17", //Старрус
    "1000"  => "11", //Trashcan
);

$row = 1;
if (($handle = fopen("import_company_bpm.csv", "r")) !== FALSE) {
    $START_TIME = mktime();
    while (($data = fgetcsv($handle, 0, "\t")) !== FALSE) {
        $arResult = [];
        $arResult['ID_BPM']             = $data[0];
        $arResult['INN']                = $data[1];
        $arResult['KPP']                = $data[2];
        $arResult['COMPANY_TYPE']       = $data[3]; //TID, Тенант, тип контрагента
        $arResult['NAME_KKT']           = $data[4]; //EXTERNAL_CODE, имя ККТ
        $arResult['DATE_CREATE']        = $data[5]; //created_at, Дата добавления ККТ
        $arResult['MODEL']              = $data[6];
        $arResult['NUM_KKT']            = $data[7]; //serial_number, Заводской номер ККТ
        $arResult['RNM_KKT']            = $data[8]; //fns_ecr_id, РНМ, RegID
        $arResult['NUM_FN']             = $data[9]; //fiscal_drive_number, Номер ФН
        $arResult['ADDRESS_KKT']        = $data[10]; //full_address, Адрес ККТ
        $arResult['NAME_TARIFF']        = $data[11]; //name_tariff , Тариф
        $arResult['COST_TARIFF']        = $data[12]; //full_period_price, Стоимость тарифа
        $arResult['DATE_FROM']          = $data[13]; //from_date, Дата начала тарифа
        $arResult['DATE_TO']            = $data[14]; //to_date, Дата завершения тарифа
        $arResult['CODE_AGENT']         = $data[15]; //agent_code, Код агента
        $arResult['PROMO_KOD']          = $data[16]; //promo_code, ID Промокода
        $arResult['EMAIL']              = $data[17]; //responsible_person_email, Email
        $arResult['PHONE']              = $data[18]; //contact_phone, Телефон
        $arResult['ACCOUNT_ID']         = $data[19]; //account_id, Телефон
        $arResult['BALANCE']            = $data[20]; //balance, баланс реестра платежей
        $arResult['ASSIGNED_LAST_NAME'] = $data[21]; //Менеджер договора, Ответственный за контрагента и платежный реестр

        addElementImportCompany($arResult);

       /* if($row > 20){            
            break;
        }
		*/

        $row++;		
    } 
    $TIME = (mktime() - $START_TIME) / 60;
    echo "Общее кол-во строк: $row \n";
    echo "Время обработки, мин: $TIME \n\n";

    fclose($handle);
}

function addElementImportCompany($arParams){
    global $arTenantCompanyType,
           $oIBlockElement,
           $IBLOCK_ID;

    $arParams['INN'] = str_replace("OBSOLETE", "", $arParams['INN']);

    //Проверка на валидность ИНН
    switch (strlen($arParams['INN'])) {
        case 10:
            break;
        case 12:
            break;
        default:
            echo "Не валидный ИНН: " . $arParams['INN'] . "\n";
            return false;
            break;
    }

	 switch ($arParams['COMPANY_TYPE']) {
        case "0": //Название тенанта (прод) из выгрузки -ТЕСТ
            echo "Не валидный тенант ТЕСТ";
            return false;
            break;
        case "7": //Название тенанта (прод) из выгрузки -Бизнес администрирование
            echo "Не валидный тенант Бизнес администрирование";
            return false;
            break;
        case "9": //Название тенанта (прод) из выгрузки -ЛК для тестирования Рапкат
            echo "Не валидный тенант ЛК для тестирования Рапкат";
            return false;
            break;
        case "1000": //Название тенанта (прод) из выгрузки -Trashcan
            echo "Не валидный тенант Trashcan";
            return false;
            break;
        default:
            break;
    }
	
	
    //Определение ID тенант
    if(array_key_exists($arParams['COMPANY_TYPE'], $arTenantCompanyType)){
        $idTenant = $arTenantCompanyType[$arParams['COMPANY_TYPE']];
    } else {
        $idTenant = "ERROR";
    }

    $PROP = array(
        "STATUS" => 0,
        "ID_BPM" => $arParams['ID_BPM'],
        "INN" => $arParams['INN'],
        "KPP" => $arParams['KPP'],
        "COMPANY_TYPE" => $idTenant,
        "NAME_KKT" => $arParams['NAME_KKT'],
        "DATE_CREATE" => $arParams['DATE_CREATE'],
        "MODEL" => $arParams['MODEL'],
        "NUM_KKT" => $arParams['NUM_KKT'],
        "RNM_KKT" => $arParams['RNM_KKT'],
        "NUM_FN" => $arParams['NUM_FN'],
        "ADDRESS_KKT" => $arParams['ADDRESS_KKT'],
        "NAME_TARIFF" => $arParams['NAME_TARIFF'],
        "COST_TARIFF" => $arParams['COST_TARIFF'],
        "DATE_FROM" => $arParams['DATE_FROM'],
        "DATE_TO" => $arParams['DATE_TO'],
        "CODE_AGENT" => $arParams['CODE_AGENT'],
        "PROMO_KOD" => $arParams['PROMO_KOD'],
        "EMAIL" => $arParams['EMAIL'],
        "PHONE" => $arParams['PHONE'],
        "ACCOUNT_ID" => $arParams['ACCOUNT_ID'],
        "BALANCE" => (!empty($arParams['BALANCE']))? $arParams['BALANCE'] : 0,
        "ASSIGNED_LAST_NAME" => $arParams['ASSIGNED_LAST_NAME']
    );

    $arLoadProductArray = Array(
        "MODIFIED_BY"       => 372, // элемент изменен текущим пользователем
        "IBLOCK_SECTION_ID" => false,          // элемент лежит в корне раздела
        "IBLOCK_ID"         => $IBLOCK_ID,
        "PROPERTY_VALUES"   => $PROP,
        "NAME"              => "INN: $arParams[INN] | KPP: $arParams[KPP] | RNM_KKT: $arParams[RNM_KKT]"
    );

    if($PRODUCT_ID = $oIBlockElement->Add($arLoadProductArray)) {
        echo "New ID: " . $PRODUCT_ID . "\n";
    }
    else {
        echo "Error: " . $oIBlockElement->LAST_ERROR . "\n";
    }
}