<?php
/*
 * Выполняет удаление элементов из Highload-блока “MessagesPostfix” по условию
 * "<=UF_DATE" => "16.01.2018",Array("UF_EVENT_TYPE" => 'TopUp_personal_accaunt')
 */

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
define('CHK_EVENT', true);
ini_set('memory_limit', '-1');

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

@set_time_limit(0);
@ignore_user_abort(true);

global $USER;
$USER->Authorize(372);

use Bitrix\Highloadblock as HL;

$HL_Infoblock_ID = 3;
$hlblock = HL\HighloadBlockTable::getById($HL_Infoblock_ID)->fetch();
$entity = HL\HighloadBlockTable::compileEntity($hlblock);
$entity_data_class = $entity->getDataClass();
$rsData = $entity_data_class::getList(array(
    "order" => array("ID" => "DESC"),
    'select' => array('*'),
    'filter' => array(
        "<=UF_DATE" => "16.01.2018",
		 Array(
            "UF_EVENT_TYPE" => 'TopUp_personal_accaunt'
        ),
		/*Array(
            "UF_EVENT_TYPE" => 'Back_personal_accaunt'
        ),*/
       /* "LOGIC" => "OR",
        Array(
            "UF_EVENT_TYPE" => 'Back_personal_accaunt'
        ),
        Array(
            "UF_EVENT_TYPE" => 'Down_personal_accaunt'
        ),
        Array(
            "UF_EVENT_TYPE" => 'TopUp_personal_accaunt'
        )*/
    ),
));
while ($el = $rsData->fetch()) {
    var_dump($el['ID']);
    $result = $entity_data_class::delete($el['ID']);
    //break;
}






