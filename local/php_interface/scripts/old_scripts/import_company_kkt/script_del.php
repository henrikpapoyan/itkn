<?php
/*
 * Выполняет удаление всех элементов из ИБ
 * с символьными кодами “import_company” и “id_created_company_contact”
 */
include_once(__DIR__ . "/../../../.config.php");

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('CHK_EVENT', true);
ini_set('memory_limit', '-1');

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

@set_time_limit(0);
@ignore_user_abort(true);

global $USER;
$USER->Authorize(372);

CModule::IncludeModule('iblock');
$oIBlockElement = new CIBlockElement();

//Удаление записей CSV файла
$IBLOCK_ID = getIblockIDByCode('import_company');

$arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM");
$arFilter = Array("IBLOCK_ID"=>$IBLOCK_ID);
$res = CIBlockElement::GetList(Array("ID"=> "DESC"), $arFilter, false, false, $arSelect);
$count = intval($res->SelectedRowsCount());
echo "Count elements import_company: $count \n";
while($ob = $res->GetNextElement())
{
    $arFields = $ob->GetFields();
    $resDel = CIBlockElement::Delete($arFields['ID']);
}

//Удаление записей о созданных компаниях и контактах
/*$IBLOCK_ID = getIblockIDByCode('id_created_company_contact');

$arSelect = Array("ID");
$arFilter = Array("IBLOCK_ID" => $IBLOCK_ID);
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
$count = intval($res->SelectedRowsCount());
echo "Count elements id_created_company_contact: $count \n";
while($ob = $res->GetNextElement())
{
    $arFields = $ob->GetFields();
    $resDel = CIBlockElement::Delete($arFields['ID']);
}*/