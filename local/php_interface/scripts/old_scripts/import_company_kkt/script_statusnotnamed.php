<?php
/*
 * /import_company_kkt/script_procced.php,
 * /import_company_kkt/script_procced_bpm.php,
 * /import_company_kkt/script_proccedwithtenand.php,
 * /import_company_kkt/script_statusnotnamed.php.
 * Не до конца разобрался для чего используется.
 * Все функции похожи друг на друга. С небольшими отличиями.
 * На вход принимает параметры крона. Затрагивает многие модули Битрикса.
 */

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('CHK_EVENT', true);
ini_set('memory_limit', '-1');

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

@set_time_limit(0);
@ignore_user_abort(true);

global $USER;
$USER->Authorize(372);

CModule::IncludeModule('crm');
CModule::IncludeModule('iblock');

$oLead          = new CCrmLead;
$oContact       = new CCrmContact;
$oDeal          = new CCrmDeal;
$oCompany       = new CCrmCompany;
$CCrmEvent      = new CCrmEvent();
$oIBlockElement = new CIBlockElement();
$oUser          = new CUser;

$IBLOCK_ID_KKT = getIblockIDByCode('registry_kkt');
$IBLOCK_ID_CSV = getIblockIDByCode('import_company');
$IBLOCK_ID_PAY = getIblockIDByCode('reestr_pay');
$IBLOCK_ID_CREATED_COMPANY_CONTACT = getIblockIDByCode('id_created_company_contact');

$offset = 0;
if (array_key_exists(1, $argv)) {
    $offset = $argv[1];
}

//die; //DELETE STRING

$START_TIME = mktime();
$n = 0;
while ($n < 50) {
    //break;
    $arResult = getOneElement($offset);
    ////echo "\n--- $arResult[ID] ---\n";
    if ($arResult === false) {
        break;
    }
//pre($arResult);
	//break;
	
    if (parserRow($arResult) === false) {
        $oIBlockElement->SetPropertyValueCode($arResult['ID'], "STATUS", 1001); //Устанавливаем статус 3 - ошибка
    } else {
        $oIBlockElement->SetPropertyValueCode($arResult['ID'], "STATUS", 1000); //Устанавливаем статус 2 - успех
    }
    $n++;

    //break;
}
$TIME = (mktime() - $START_TIME);
//echo "\n\nВремя обработки, сек: $TIME \n\n";


function getOneElement($offset = 1){
    global $oIBlockElement,
           $IBLOCK_ID_CSV;

    $arFilter = array(
        "IBLOCK_ID"         => $IBLOCK_ID_CSV,
        "PROPERTY_STATUS" => array(2) // берем записи со статусом 0 - не обработан
    );

    $res = $oIBlockElement->GetList(array(), $arFilter, false, array('iNumPage' => $offset, 'nPageSize' => 1));

    if($res->SelectedRowsCount() <= $offset){
        return false;
    }

    if($ob = $res->GetNextElement()){
        $arElementFields    = $ob->GetFields();
        $oIBlockElement->SetPropertyValueCode($arElementFields['ID'], "STATUS", 1); //устанавливаем статус 1 - в работе

        $arElement          = $ob->GetProperties();

        $arResult                       = [];
        $arResult['ID']                 = $arElementFields['ID'];
        $arResult['ID_BPM']             = $arElement['ID_BPM']['VALUE'];
        $arResult['INN']                = $arElement['INN']['VALUE'];
        $arResult['KPP']                = $arElement['KPP']['VALUE'];
        $arResult['COMPANY_TYPE']       = $arElement['COMPANY_TYPE']['VALUE']; //TID, Тенант, тип контрагента
        $arResult['NAME_KKT']           = $arElement['NAME_KKT']['VALUE']; //EXTERNAL_CODE, имя ККТ
        $arResult['DATE_CREATE']        = $arElement['DATE_CREATE']['VALUE']; //created_at, Дата добавления ККТ
        $arResult['MODEL']              = $arElement['MODEL']['VALUE'];
        $arResult['NUM_KKT']            = $arElement['NUM_KKT']['VALUE']; //serial_number, Заводской номер ККТ
        $arResult['RNM_KKT']            = $arElement['RNM_KKT']['VALUE']; //fns_ecr_id, РНМ, RegID
        $arResult['NUM_FN']             = $arElement['NUM_FN']['VALUE']; //fiscal_drive_number, Номер ФН
        $arResult['ADDRESS_KKT']        = $arElement['ADDRESS_KKT']['VALUE']; //full_address, Адрес ККТ
        $arResult['NAME_TARIFF']        = $arElement['NAME_TARIFF']['VALUE']; //name_tariff , Тариф
        $arResult['COST_TARIFF']        = $arElement['COST_TARIFF']['VALUE']; //full_period_price, Стоимость тарифа
        $arResult['DATE_FROM']          = $arElement['DATE_FROM']['VALUE']; //from_date, Дата начала тарифа
        $arResult['DATE_TO']            = $arElement['DATE_TO']['VALUE']; //to_date, Дата завершения тарифа
        $arResult['CODE_AGENT']         = $arElement['CODE_AGENT']['VALUE']; //agent_code, Код агента
        $arResult['PROMO_KOD']          = $arElement['PROMO_KOD']['VALUE']; //promo_code, ID Промокода
        $arResult['ACCOUNT_ID']         = $arElement['ACCOUNT_ID']['VALUE']; //promo_code, ID Промокода
        $arResult['BALANCE']            = $arElement['BALANCE']['VALUE']; //promo_code, ID Промокода
        $arResult['BALANCE']            = str_replace(",", ".", $arResult['BALANCE']);
        $arResult['ASSIGNED_LAST_NAME'] = $arElement['ASSIGNED_LAST_NAME']['VALUE']; //promo_code, ID Промокода

        $arResult['EMAIL']          = $arElement['EMAIL']['VALUE']; //responsible_person_email, Email
        $arResult['PHONE']          = $arElement['PHONE']['VALUE']; //contact_phone, Телефон

        $arResult['EMAIL'] = (empty($arResult['EMAIL']))? "" : $arResult['EMAIL'];
        $arResult['EMAIL'] = ($arResult['EMAIL'] == "null")? "" : $arResult['EMAIL'];
        if(check_email($arResult['EMAIL']) === false){
            $arResult['EMAIL'] = "";
        }
        $arResult['PHONE'] = (empty($arResult['PHONE']))? "" : $arResult['PHONE'];
        $arResult['PHONE'] = ($arResult['PHONE'] == "null")? "" : $arResult['PHONE'];

        return $arResult;
    }
    return false;
}

function parserRow($arParams){

$filepathtxt = $_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/scripts/import_company_kkt/resulthref.txt";
//$rsFile1 = fopen($filepathtxt, "a");



    //Тип сделки по умолчанию
    $typedeal=2;

    global $oLead,
           $oContact,
           $oDeal,
           $oCompany,
           $oUser,
           $CCrmEvent,
           $oIBlockElement,
           $IBLOCK_ID_KKT,
           $IBLOCK_ID_PAY;

    $isKKTDefault       = false;
    $isLead             = false;
    $isReestPayDefault  = false;

    $arParams['INN'] = str_replace("OBSOLETE", "", $arParams['INN']);


    switch ($arParams['COMPANY_TYPE']) {
        case "0": //Название тенанта (прод) из выгрузки -ТЕСТ
            //echo "Не валидный тенант ТЕСТ";
            return false;
            break;
        case "5": //Название тенанта (прод) из выгрузки -Бизнес администрирование
            //echo "Не валидный тенант Бизнес администрирование";
            return false;
            break;
        case "7": //Название тенанта (прод) из выгрузки -ЛК для тестирования Рапкат
            //echo "Не валидный тенант ЛК для тестирования Рапкат";
            return false;
            break;
        case "11": //Название тенанта (прод) из выгрузки -Trashcan
            //echo "Не валидный тенант Trashcan";
            return false;
            break;
        default:
            break;
    }

    //Проверка на валидность ИНН
    switch (strlen($arParams['INN'])) {
        case 10:
            break;
        case 12:
            break;
        default:
            //echo "Не валидный ИНН: " . $arParams['INN'];
            return false;
            break;
    }

    //Проверка на наличие RegID
    if(!empty($arParams['RNM_KKT'])){
        $isKKTDefault = true;
    }

    //Проверка на наличие данных Реестра платежей.
    if(!empty($arParams['ACCOUNT_ID'])){
        $isReestPayDefault = true;
    }

    /**  Поиск ID пользователя по LAST_NAME */
    $userId = 372; //По умолчанию Bitrix
    if(!empty($arParams['ASSIGNED_LAST_NAME'])) {
        $rsUsers = $oUser->GetList(($by = "ID"), ($order = "desc"), array('LAST_NAME' => $arParams['ASSIGNED_LAST_NAME']), array());
        if ($arResult['USER'] = $rsUsers->Fetch()) {
            $userId = $arResult['USER']['ID'];
            //echo "Нашли пользователя по фамилии $arParams[ASSIGNED_LAST_NAME], ID: $userId.\n";
        }
    }

    /** Поиск и обновление Лида */

    //Поиск Лида по ИНН
    $resMultiADD = $oLead->GetList(array('ID' => 'asc'), array(
        'TITLE'             => 'регистрация на сайте www.1ofd.ru',
        'UF_CRM_1499414186' => $arParams['INN']
    ));
    if ($arMultiADD = $resMultiADD->Fetch()) {
        //echo "Найден лид по ИНН\n"; //DEL
        $isLead = true;
        $arSelectedLead = array(
            "ID"            => $arMultiADD['ID'],
            "LAST_NAME"     => ($arMultiADD['LAST_NAME'] == "noname")? "" : $arMultiADD['LAST_NAME'],
            "NAME"          => ($arMultiADD['NAME'] == "noname")? "" : $arMultiADD['NAME'],
            "SECOND_NAME"   => ($arMultiADD['SECOND_NAME'] == "noname")? "" : $arMultiADD['SECOND_NAME'],
        );
    }

    // Поиск лида по Email, если не удалось найти лида по ИНН
    if(empty($isLead) && !empty($arParams['EMAIL'])) {
        $idLead = leadDublicateCheck('регистрация на сайте www.1ofd.ru', $arParams['EMAIL']);
        if(!empty($idLead)) {
            //echo "Найден лид по Email\n"; //DEL
            $isLead = true;

            $resMultiADD = $oLead->GetList(array('ID' => 'asc'), array('ID' => $idLead));
            if ($arMultiADD = $resMultiADD->Fetch()) {
                $arSelectedLead = array(
                    "ID"            => $arMultiADD['ID'],
                    "LAST_NAME"     => ($arMultiADD['LAST_NAME'] == "noname")? "" : $arMultiADD['LAST_NAME'],
                    "NAME"          => ($arMultiADD['NAME'] == "noname")? "" : $arMultiADD['NAME'],
                    "SECOND_NAME"   => ($arMultiADD['SECOND_NAME'] == "noname")? "" : $arMultiADD['SECOND_NAME'],
                    "EMAIL"         => $arParams['EMAIL'],
                );
            }

            //Обновляем Лид, добавляем ИНН.
            $arFields = array("UF_CRM_1499414186" => $arParams['INN']);
            $resUpdate = $oLead->Update($arSelectedLead['ID'], $arFields);
            if($resUpdate) {
                //echo "Лид успешно обновлен\n"; //DEL
            } else {
                //echo  "Ошибка обновления лида: (" . $arSelectedLead['ID'] . "). ";
            }
        }
    }

    //Переводим лид в статус зарегестрирован в лк или Архивный
    if(!empty($isLead)) {
        $arFields = Array(
            "STATUS_ID" => "2", //зарегистрирован в ЛК
        );

        //Если есть ККТ, переводим в архивный статус
        if($isKKTDefault){
            $arFields['STATUS_ID'] = 1;
        }

        $oLead->Update($arSelectedLead['ID'], $arFields);
        //echo "Лид переведён в статус зарегистрирован в ЛК ID (" . $arSelectedLead['ID'] . ")\n";
    }

    /** Поиск/создание компании по реквизитам ИНН и КПП */

    $requisite = new \Bitrix\Crm\EntityRequisite();
    $fieldsInfo = $requisite->getFormFieldsInfo();
    $select = array_keys($fieldsInfo);

    $arFilter = array(
        'RQ_INN' => $arParams['INN'],
        'ENTITY_TYPE_ID' => 4
    );
    if(!empty($arParams['KPP'])){
        $arFilter['RQ_KPP'] = $arParams['KPP'];
    }

    $res = $requisite->getList(
        array(
            'order' => array('SORT' => 'ASC', 'ID' => 'ASC'),
            'filter' => $arFilter,
            'select' => $select
        )
    );

    //Компания найдена
    if ($row = $res->fetch()) {
        $companyID = $row['ENTITY_ID'];
        //echo "Найдена существующая компания ID (" . $companyID . ")\n";

        $emailFmId  = getFmId($companyID, 'EMAIL');
        $phoneFmId  = getFmId($companyID, 'PHONE');

        //Обновляем тип компании в зависимости от поля Тенант
       // $arFields["COMPANY_TYPE"] = $arParams['COMPANY_TYPE']; //Тип компании(Тенант)
        $arFields["UF_ID_PROD"]     = $arParams['ID_BPM']; //ID компании в BPM

        //Обновление ID ответственного пользователя
        if($userId != 372) {
            $arFields ["ASSIGNED_BY_ID"] = $userId;
        }

        if(!empty($arParams['EMAIL'])){
            $arFields['FM']['EMAIL'] =  Array(
                "$emailFmId" => Array(
                    "VALUE"         => $arParams['EMAIL'],
                    "VALUE_TYPE"    => "WORK",
                ),
            );
        }

        if(!empty($arParams['PHONE'])){
            $arFields['FM']['PHONE'] =  Array(
                "$phoneFmId" => Array(
                    "VALUE"         => $arParams['PHONE'],
                    "VALUE_TYPE"    => "WORK",
                ),
            );
        }

		//fwrite($rsFile1,$companyID.";найдена"."\r\n");
		file_put_contents($filepathtxt, $companyID.";найдена"."\r\n", FILE_APPEND | LOCK_EX);
        switch ($arParams['COMPANY_TYPE']) {
            case "CUSTOMER"://1
                //echo "Статус компании CUSTOMER\n";
                //Установка тенанта компании
                ////SetUserField("CRM_COMPANY", $companyID, "UF_TENANT", "286");
                //echo "Компании поставили в поле тенант Клиент\n";

				$arraystatus=[];
				
				
                $resultcompany = CCrmCompany::GetByID($companyID);
                $companytype = $resultcompany['COMPANY_TYPE'];
				
                if ($companytype != "") {
                    //echo "Если тип компании какой-либо, кроме Клиент, то проверка наличия сделок.\n";
                    $arFields["COMPANY_TYPE"]   = "CUSTOMER";
                    $arOrder = Array('DATE_CREATE' => 'DESC');
                    $arFilter = Array("COMPANY_ID" => $companyID);
                    $arFilter["!=STAGE_ID"] = Array("C1:EXIT","C5:LOSE","C4:LOSE","C3:LOSE","C2:LOSE","C1:LOSE","LOSE","WON","C1:WON","C2:WON","C3:WON","C4:WON","C5:WON");
                    $arFilter["!=TYPE_ID"] = Array("2","");
                    $arSelect = Array("*");
                    $result = $oDeal->GetListEx($arOrder, $arFilter, $arSelect, false);
                    while ($fields = $result->Fetch()) {
                        $DealTypeArray = $fields;
                        $dealid=$DealTypeArray['ID'];
                        $dealstatus=$DealTypeArray['TYPE_ID'];
                        //echo "Сделка $dealid статус $dealstatus.\n";
                            if ($DealTypeArray['TYPE_ID'] == "COMPLEX") {
                                $newstatuscompany = "19";
                                //echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"]   = $newstatuscompany;
									if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
									else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}
                            } else if ($DealTypeArray['TYPE_ID'] == "GOODS") {
                                $newstatuscompany = "19";
                                //echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"]   = $newstatuscompany;
									if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
									else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}
                            } else if ($DealTypeArray['TYPE_ID'] == "SALE") {
                                $newstatuscompany = "2";
                                //echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"]   = $newstatuscompany;
									if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
									else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}
                            } else if ($DealTypeArray['TYPE_ID'] == "SERVICE") {
                                $newstatuscompany = "20";
                                //echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"]   = $newstatuscompany;
									if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
									else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}
                            } else if ($DealTypeArray['TYPE_ID'] == "SERVICES") {
                                $newstatuscompany = "19";
                                //echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"]   = $newstatuscompany;
									if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
									else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}
                            }
							else
							{
								echo "$companyID 6\n";file_put_contents($filepathtxt, $companyID.";косячнаякомпания"."\r\n", FILE_APPEND | LOCK_EX);
									$arFields["COMPANY_TYPE"]   = "косячнаякомпания";
									if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
									else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}
							}
                    }
					$resultstring="";
					foreach($arraystatus as $index=>$elementvalue)
					{
						if($elementvalue>0){$resultstring=$resultstring+" "+$index;} 
					}
if(!empty($resultstring)){file_put_contents($filepathtxt, $companyID.";$resultstring"."\r\n", FILE_APPEND | LOCK_EX);}	
                }
                break;
            case "3"://5
                //echo "Статус компании 3\n";
$arraystatus=[];


                //Получаем тип компании
                $resultcompany = CCrmCompany::GetByID($companyID);
                $companytype = $resultcompany['COMPANY_TYPE'];

                if ($companytype != "") {
                    //echo "Если тип компании какой-либо, кроме Клиент, то проверка наличия сделок.\n";
                    $arFields["COMPANY_TYPE"]   = "CUSTOMER";
                    $arOrder = Array('DATE_CREATE' => 'DESC');
                    $arFilter = Array("COMPANY_ID" => $companyID);
                    $arFilter["!=TYPE_ID"] = Array("2","");
                    $arSelect = Array("*");
                    $result = $oDeal->GetListEx($arOrder, $arFilter, $arSelect, false);
                    while ($fields = $result->Fetch()) {
                        $DealTypeArray = $fields;
                             if ($DealTypeArray['TYPE_ID'] == "COMPLEX") {
                                $newstatuscompany = "19";
                                 $arFields["COMPANY_TYPE"]   = $newstatuscompany;
if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}	
                            } else if ($DealTypeArray['TYPE_ID'] == "GOODS") {
                                $newstatuscompany = "19";
                                 $arFields["COMPANY_TYPE"]   = $newstatuscompany;
if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}	
                            } else if ($DealTypeArray['TYPE_ID'] == "SALE") {
                                $newstatuscompany = "2";
                                 $arFields["COMPANY_TYPE"]   = $newstatuscompany;
if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}	
                            } else if ($DealTypeArray['TYPE_ID'] == "SERVICE") {
                                $newstatuscompany = "20";
                                 $arFields["COMPANY_TYPE"]   = $newstatuscompany;
if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}	
                            } else if ($DealTypeArray['TYPE_ID'] == "SERVICES") {
                                $newstatuscompany = "19";
                                 $arFields["COMPANY_TYPE"]   = $newstatuscompany;
if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}	
                            }							
else
{
echo "$companyID 6\n";file_put_contents($filepathtxt, $companyID.";косячнаякомпания"."\r\n", FILE_APPEND | LOCK_EX);
$arFields["COMPANY_TYPE"]   = "косячнаякомпания";
if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}
}

                    }
					
		$resultstring="";
foreach($arraystatus as $index=>$elementvalue)
{
if($elementvalue>0){$resultstring=$resultstring+" "+$index;} 
}
if(!empty($resultstring)){file_put_contents($filepathtxt, $companyID.";$resultstring"."\r\n", FILE_APPEND | LOCK_EX);}		
					
					
                }
                break;
            case "4"://6
                //echo "Статус компании 4\n";
$arraystatus=[];
                //Установка тенанта компании
               // //SetUserField("CRM_COMPANY", $companyID, "UF_TENANT", "288");
               // //echo "Компании поставили в поле тенант Клиенты, с которыми расторгнут договор\n";

                //Получаем тип компании
                $resultcompany = CCrmCompany::GetByID($companyID);
                $companytype = $resultcompany['COMPANY_TYPE'];

                if ($companytype != "") {
                    //echo "Если тип компании какой-либо, кроме Клиент, то проверка наличия сделок.\n";
                    $arFields["COMPANY_TYPE"]   = "CUSTOMER";
                    $findflag = false;
                    $arOrder = Array('DATE_CREATE' => 'DESC');
                    $arFilter = Array("COMPANY_ID" => $companyID);
                    $arFilter["!=STAGE_ID"] = Array("C1:EXIT","C5:LOSE","C4:LOSE","C3:LOSE","C2:LOSE","C1:LOSE","LOSE","WON","C1:WON","C2:WON","C3:WON","C4:WON","C5:WON");
                    $arFilter["!=TYPE_ID"] = Array("2","");
                    $arSelect = Array("*");
                    $result = $oDeal->GetListEx($arOrder, $arFilter, $arSelect, false);
                    while ($fields = $result->Fetch()) {
                        $DealTypeArray = $fields;
                        if (($DealTypeArray['TYPE_ID'] != "2")&&($DealTypeArray['TYPE_ID'] != "")){
                            if ($DealTypeArray['TYPE_ID'] == "COMPLEX") {
                                $newstatuscompany = "19";
                                //echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"]   = $newstatuscompany;
if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}	
                            } else if ($DealTypeArray['TYPE_ID'] == "GOODS") {
                                $newstatuscompany = "19";
                                //echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"]   = $newstatuscompany;
if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}	
                            } else if ($DealTypeArray['TYPE_ID'] == "SALE") {
                                $newstatuscompany = "2";
                                //echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"]   = $newstatuscompany;
if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}	
                            } else if ($DealTypeArray['TYPE_ID'] == "SERVICE") {
                                $newstatuscompany = "20";
                                //echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"]   = $newstatuscompany;
if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}	
                            } else if ($DealTypeArray['TYPE_ID'] == "SERVICES") {
                                $newstatuscompany = "19";
                                //echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"]   = $newstatuscompany;
if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}	
                            }
                            else
                            {
								echo "$companyID 6\n";file_put_contents($filepathtxt, $companyID.";косячнаякомпания"."\r\n", FILE_APPEND | LOCK_EX);
$arFields["COMPANY_TYPE"]   = "косячнаякомпания";
if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}
                                //echo "Новый статус компании равен CUSTOMER.\n";
                                $arFields["COMPANY_TYPE"]   = "CUSTOMER";
                                $entity_id = "CRM_COMPANY";
                                $uf_guid = "UF_STATUS";
                                //SetUserField($entity_id, $companyID, $uf_guid, "266");
                            }

                        }
						
						
						
                    }
					$resultstring="";
foreach($arraystatus as $index=>$elementvalue)
{
	if($elementvalue>0){$resultstring=$resultstring+" "+$index;} 
}
if(!empty($resultstring)){file_put_contents($filepathtxt, $companyID.";$resultstring"."\r\n", FILE_APPEND | LOCK_EX);}	
					
					
                }
                break;
            case "6"://8
                //echo "Статус компании 6\n";
$arraystatus=[];
                //Установка тенанта компании
                //SetUserField("CRM_COMPANY", $companyID, "UF_TENANT", "289");
                //echo "Компании поставили в поле тенант Тензор\n";


                //Получаем тип компании
                $resultcompany = CCrmCompany::GetByID($companyID);
                $companytype = $resultcompany['COMPANY_TYPE'];



                if ($companytype != "") {
                    //echo "Если тип компании какой-либо, кроме Клиент, то проверка наличия сделок.\n";
                    $arFields["COMPANY_TYPE"] = "CUSTOMER";
                    $arOrder = Array('DATE_CREATE' => 'DESC');
                    $arFilter = Array("COMPANY_ID" => $companyID);
                    $arSelect = Array("*");
                    $arFilter["!=TYPE_ID"] = Array("CLIENTTYPE","");
                    $result = $oDeal->GetListEx($arOrder, $arFilter, $arSelect, false);
                    while ($fields = $result->Fetch()) {
                        $DealTypeArray = $fields;
                            if ($DealTypeArray['TYPE_ID'] == "COMPLEX") {
                                $newstatuscompany = "19";
                                //echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"]   = $newstatuscompany;
if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}	
                            } else if ($DealTypeArray['TYPE_ID'] == "GOODS") {
                                $newstatuscompany = "19";
                                //echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"]   = $newstatuscompany;
if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}	
                            } else if ($DealTypeArray['TYPE_ID'] == "SALE") {
                                $newstatuscompany = "2";
                                //echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"]   = $newstatuscompany;
if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}	
                            } else if ($DealTypeArray['TYPE_ID'] == "SERVICE") {
                                $newstatuscompany = "20";
                                //echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"]   = $newstatuscompany;
if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}	
                            } else if ($DealTypeArray['TYPE_ID'] == "SERVICES") {
                                $newstatuscompany = "19";
                                //echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"]   = $newstatuscompany;
if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}	
                            }
else
{
echo "$companyID 6\n";file_put_contents($filepathtxt, $companyID.";косячнаякомпания"."\r\n", FILE_APPEND | LOCK_EX);
$arFields["COMPANY_TYPE"]   = "косячнаякомпания";
if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}
}

                    }
					$resultstring="";
foreach($arraystatus as $index=>$elementvalue)
{
if($elementvalue>0){$resultstring=$resultstring+" "+$index;} 
}
if(!empty($resultstring)){file_put_contents($filepathtxt, $companyID.";$resultstring"."\r\n", FILE_APPEND | LOCK_EX);}	
					
					
					
                }
                break;
            case "8"://10
                //echo "Статус компании 8\n";
                //Установка тенанта компании
                //SetUserField("CRM_COMPANY", $companyID, "UF_TENANT", "290");
                //echo "Компании поставили в поле тенант Без блокировки касс\n";
$arraystatus=[];
                $arFields["COMMENTS"] = "у клиента нет блокировки касс";
                //Получаем тип компании
                $resultcompany = CCrmCompany::GetByID($companyID);
                $companytype = $resultcompany['COMPANY_TYPE'];


                if ($companytype != "") {
                    //echo "Если тип компании какой-либо, кроме Клиент, то проверка наличия сделок.\n";
                    $arFields["COMPANY_TYPE"] = "CUSTOMER";
                    $arOrder = Array('DATE_CREATE' => 'DESC');
                    $arFilter = Array("COMPANY_ID" => $companyID);
                    $arFilter["!=TYPE_ID"] = Array("2","");
                    $arSelect = Array("*");
                    $result = $oDeal->GetListEx($arOrder, $arFilter, $arSelect, false);
                    while ($fields = $result->Fetch()) {
                        $DealTypeArray = $fields;
                            if ($DealTypeArray['TYPE_ID'] == "COMPLEX") {
                                $newstatuscompany = "19";
                                //echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}	
                            } else if ($DealTypeArray['TYPE_ID'] == "GOODS") {
                                $newstatuscompany = "19";
                                //echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}	
                            } else if ($DealTypeArray['TYPE_ID'] == "SALE") {
                                $newstatuscompany = "2";
                                //echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}	
                            } else if ($DealTypeArray['TYPE_ID'] == "SERVICE") {
                                $newstatuscompany = "20";
                                //echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}	
                            } else if ($DealTypeArray['TYPE_ID'] == "SERVICES") {
                                $newstatuscompany = "19";
                                //echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}	
                            }
else
{
echo "$companyID 6\n";file_put_contents($filepathtxt, $companyID.";косячнаякомпания"."\r\n", FILE_APPEND | LOCK_EX);
$arFields["COMPANY_TYPE"]   = "косячнаякомпания";
if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}
}

                    }
					
					$resultstring="";
foreach($arraystatus as $index=>$elementvalue)
{
if($elementvalue>0){$resultstring=$resultstring+" "+$index;} 
}
if(!empty($resultstring)){file_put_contents($filepathtxt, $companyID.";$resultstring"."\r\n", FILE_APPEND | LOCK_EX);}	
					
					
                }
                break;
            case "PARTNER"://11
                //echo "Статус компании PARTNER\n";
$arraystatus=[];
                //Установка тенанта компании
                //SetUserField("CRM_COMPANY", $companyID, "UF_TENANT", "291");
                //echo "Компании поставили в поле тенант Агент\n";

                $resultcompany = CCrmCompany::GetByID($companyID);
                $companytype = $resultcompany['COMPANY_TYPE'];


                if ($companytype != "") {
                    //echo "Если тип компании какой-либо, кроме Агент, то проверка наличия сделок.\n";
                    $newstatuscompany = "AGENT";
                    $findflag = false;
                    $arOrder = Array('DATE_CREATE' => 'DESC');
                    $arFilter = Array("COMPANY_ID" => $companyID);
                    $arFilter["!=TYPE_ID"] = Array("SALE","");
                    $arSelect = Array("*");
                    $result = $oDeal->GetListEx($arOrder, $arFilter, $arSelect, false);
                    while ($fields = $result->Fetch()) {
                        $DealTypeArray = $fields;
                            if ($DealTypeArray['TYPE_ID'] == "2") {
                                $newstatuscompany = "2";$findflag = true;
                                //echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}	
                            } else if ($DealTypeArray['TYPE_ID'] == "COMPLEX") {
                                $newstatuscompany = "AGENTBYUER";$findflag = true;
                                //echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}	
                            } else if ($DealTypeArray['TYPE_ID'] == "GOODS") {
                                $newstatuscompany = "AGENTBYUER";$findflag = true;
                                //echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}	
                            } else if ($DealTypeArray['TYPE_ID'] == "SERVICE") {
                                $newstatuscompany = "AGENTPOST";$findflag = true;
                                //echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}	
                            } else if ($DealTypeArray['TYPE_ID'] == "SERVICES") {
                                $newstatuscompany = "AGENTBYUER";$findflag = true;
                                //echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}	
                            }
							else
{
echo "$companyID 6\n";file_put_contents($filepathtxt, $companyID.";косячнаякомпания"."\r\n", FILE_APPEND | LOCK_EX);
$arFields["COMPANY_TYPE"]   = "косячнаякомпания";
if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}
}
							
                    }

					
					$resultstring="";
foreach($arraystatus as $index=>$elementvalue)
{
if($elementvalue>0){$resultstring=$resultstring+" "+$index;} 
}
if(!empty($resultstring)){file_put_contents($filepathtxt, $companyID.";$resultstring"."\r\n", FILE_APPEND | LOCK_EX);}	
					
					
					
                }
                break;
            case "9"://12
                //echo "Статус компании 9\n";
                $resultcompany = CCrmCompany::GetByID($companyID);
                $companytype = $resultcompany['COMPANY_TYPE'];
$arraystatus=[];


                if (($companytype != "")
                    &&($companytype != "")
                    &&($companytype != "")) {
                    $arFields["COMPANY_TYPE"] = "WITHOUTTYPE";
                    $findflag = false;
                    $arOrder = Array('DATE_CREATE' => 'DESC');
                    $arFilter = Array("COMPANY_ID" => $companyID);
                    $arFilter["!=TYPE_ID"] = Array("2","SALE","");
                    $arSelect = Array("*");
                    $result = $oDeal->GetListEx($arOrder, $arFilter, $arSelect, false);
                    while ($fields = $result->Fetch()) {
                        $DealTypeArray = $fields;
                            if ($DealTypeArray['TYPE_ID'] == "COMPLEX") {
                                $newstatuscompany = "19";
                                //echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}	
                            } else if ($DealTypeArray['TYPE_ID'] == "GOODS") {
                                $newstatuscompany = "19";
                                //echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}	
                            } else if ($DealTypeArray['TYPE_ID'] == "SALE") {
                                $newstatuscompany = "2";
                                //echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}	
                            } else if ($DealTypeArray['TYPE_ID'] == "SERVICE") {
                                $newstatuscompany = "20";
                                //echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}	
                            } else if ($DealTypeArray['TYPE_ID'] == "SERVICES") {
                                $newstatuscompany = "19";
                                //echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}	
                            }
else
{
echo "$companyID 6\n";file_put_contents($filepathtxt, $companyID.";косячнаякомпания"."\r\n", FILE_APPEND | LOCK_EX);
$arFields["COMPANY_TYPE"]   = "косячнаякомпания";
if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}
}

                    }
$resultstring="";
foreach($arraystatus as $index=>$elementvalue)
{
if($elementvalue>0){$resultstring=$resultstring+" "+$index;} 
}
if(!empty($resultstring)){file_put_contents($filepathtxt, $companyID.";$resultstring"."\r\n", FILE_APPEND | LOCK_EX);}				
					
					
					
                }
                break;
            case "13"://13
                //echo "Статус компании 13\n";
$arraystatus=[];
                //Установка тенанта компании
                //SetUserField("CRM_COMPANY", $companyID, "UF_TENANT", "292");
                //echo "Компании поставили в поле тенант 1c\n";

                $resultcompany = CCrmCompany::GetByID($companyID);
                $companytype = $resultcompany['COMPANY_TYPE'];


                if ($companytype != "") {
                    //echo "Если тип компании какой-либо, кроме Клиент, то проверка наличия сделок.\n";
                    $arFields["COMPANY_TYPE"] = "CUSTOMER";
                    $arOrder = Array('DATE_CREATE' => 'DESC');
                    $arFilter = Array("COMPANY_ID" => $companyID);
                    $arFilter["!=TYPE_ID"] = Array("CLIENTTYPE","");
                    $arSelect = Array("*");
                    $result = $oDeal->GetListEx($arOrder, $arFilter, $arSelect, false);
                    while ($fields = $result->Fetch()) {
                        $DealTypeArray = $fields;
                            if ($DealTypeArray['TYPE_ID'] == "COMPLEX") {
                                $newstatuscompany = "19";
                                //echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}	
                            } else if ($DealTypeArray['TYPE_ID'] == "GOODS") {
                                $newstatuscompany = "19";
                                //echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}	
                            } else if ($DealTypeArray['TYPE_ID'] == "SALE") {
                                $newstatuscompany = "2";
                                //echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}	
                            } else if ($DealTypeArray['TYPE_ID'] == "SERVICE") {
                                $newstatuscompany = "20";
                                //echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}	
                            } else if ($DealTypeArray['TYPE_ID'] == "SERVICES") {
                                $newstatuscompany = "19";
                                //echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}	
                            }
                            else if ($DealTypeArray['TYPE_ID'] == "2") {
                                $newstatuscompany = "CUSTOMER";
                                //echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}	
                            }
else
{
echo "$companyID 6\n";file_put_contents($filepathtxt, $companyID.";косячнаякомпания"."\r\n", FILE_APPEND | LOCK_EX);
$arFields["COMPANY_TYPE"]   = "косячнаякомпания";
if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}
}
                    }
					
$resultstring="";
foreach($arraystatus as $index=>$elementvalue)
{
if($elementvalue>0){$resultstring=$resultstring+" "+$index;} 
}
if(!empty($resultstring)){file_put_contents($filepathtxt, $companyID.";$resultstring"."\r\n", FILE_APPEND | LOCK_EX);}					
					
					
					
                }
                break;
            case "2"://14
                //echo "Статус компании 2\n";
$arraystatus=[];
                //Установка тенанта компании
                //SetUserField("CRM_COMPANY", $companyID, "UF_TENANT", "293");
                //echo "Компании поставили в поле тенант Клиент+агент\n";

                //Получаем тип компании
                $resultcompany = CCrmCompany::GetByID($companyID);
                $companytype = $resultcompany['COMPANY_TYPE'];


                if ($companytype != "") {
                    //echo "Если тип компании какой-либо, кроме Клиент, то проверка наличия сделок.\n";
                    $arFields["COMPANY_TYPE"]   = "2";
                    $findflag = false;
                    $arOrder = Array('DATE_CREATE' => 'DESC');
                    $arFilter = Array("COMPANY_ID" => $companyID);
                    $arFilter["!=TYPE_ID"] = Array("2","");
                    $arSelect = Array("*");
                    $result = $oDeal->GetListEx($arOrder, $arFilter, $arSelect, false);
                    while ($fields = $result->Fetch()) {
                        $DealTypeArray = $fields;
                            if ($DealTypeArray['TYPE_ID'] == "COMPLEX") {
                                $newstatuscompany = "CLIENTAGENTBUYER";
                                $findflag = true;
                                //echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}	
                            } else if ($DealTypeArray['TYPE_ID'] == "GOODS") {
                                $newstatuscompany = "CLIENTAGENTBUYER";
                                $findflag = true;
                                //echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}	
                            } else if ($DealTypeArray['TYPE_ID'] == "SALE") {
                                $newstatuscompany = "2";
                                $findflag = true;
                                //echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}	
                            } else if ($DealTypeArray['TYPE_ID'] == "SERVICE") {
                                $newstatuscompany = "CLIENTAGENTPOST";
                                $findflag = true;
                                //echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}	
                            } else if ($DealTypeArray['TYPE_ID'] == "SERVICES") {
                                $newstatuscompany = "CLIENTAGENTBUYER";
                                $findflag = true;
                                //echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}	
                            }
							else
{
echo "$companyID 6\n";file_put_contents($filepathtxt, $companyID.";косячнаякомпания"."\r\n", FILE_APPEND | LOCK_EX);
$arFields["COMPANY_TYPE"]   = "косячнаякомпания";
if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}
}

                    }
					
					
					
$resultstring="";
foreach($arraystatus as $index=>$elementvalue)
{
if($elementvalue>0){$resultstring=$resultstring+" "+$index;} 
}
if(!empty($resultstring)){file_put_contents($filepathtxt, $companyID.";$resultstring"."\r\n", FILE_APPEND | LOCK_EX);}				
					
                }
                break;
            case "10"://15
                //echo "Статус компании 10\n";
$arraystatus=[];
                //Установка тенанта компании
                //SetUserField("CRM_COMPANY", $companyID, "UF_TENANT", "294");
                //echo "Компании поставили в поле тенант Корпорация\n";

                //Получаем тип компании
                $resultcompany = CCrmCompany::GetByID($companyID);
                $companytype = $resultcompany['COMPANY_TYPE'];


                if ($companytype != "") {
                    //echo "Если тип компании какой-либо, кроме Клиент, то проверка наличия сделок.\n";
                    $arFields["COMPANY_TYPE"] = "CUSTOMER";
                    $arOrder = Array('DATE_CREATE' => 'DESC');
                    $arFilter = Array("COMPANY_ID" => $companyID);
                    $arFilter["!=TYPE_ID"] = Array("2","");
                    $arSelect = Array("*");
                    $result = $oDeal->GetListEx($arOrder, $arFilter, $arSelect, false);
                    while ($fields = $result->Fetch()) {
                        $DealTypeArray = $fields;
                            if ($DealTypeArray['TYPE_ID'] == "COMPLEX") {
                                $newstatuscompany = "19";
                                //echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}	
                            } else if ($DealTypeArray['TYPE_ID'] == "GOODS") {
                                $newstatuscompany = "19";
                                //echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}	
                            } else if ($DealTypeArray['TYPE_ID'] == "SALE") {
                                $newstatuscompany = "2";
                                //echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}	
                            } else if ($DealTypeArray['TYPE_ID'] == "SERVICE") {
                                $newstatuscompany = "20";
                                //echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}	
                            } else if ($DealTypeArray['TYPE_ID'] == "SERVICES") {
                                $newstatuscompany = "19";
                                //echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}	
                            }
else
{
echo "$companyID 6\n";file_put_contents($filepathtxt, $companyID.";косячнаякомпания"."\r\n", FILE_APPEND | LOCK_EX);
$arFields["COMPANY_TYPE"]   = "косячнаякомпания";
if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}
}
                    }
					
$resultstring="";
foreach($arraystatus as $index=>$elementvalue)
{
if($elementvalue>0){$resultstring=$resultstring+" "+$index;} 
}
if(!empty($resultstring)){file_put_contents($filepathtxt, $companyID.";$resultstring"."\r\n", FILE_APPEND | LOCK_EX);}					
					
					
					
                }
                break;
            case "16"://16
                //echo "Статус компании 16\n";
                //Установка тенанта компании
                //SetUserField("CRM_COMPANY", $companyID, "UF_TENANT", "294");
                //echo "Компании поставили в поле тенант Атол-онлайн\n";
$arraystatus=[];
                //Получаем тип компании
                $resultcompany = CCrmCompany::GetByID($companyID);
                $companytype = $resultcompany['COMPANY_TYPE'];



                if ($companytype != "") {
                    //echo "Если тип компании какой-либо, кроме Клиент, то проверка наличия сделок.\n";
                    $arFields["COMPANY_TYPE"] = "CUSTOMER";
                    $findflag = false;
                    $arOrder = Array('DATE_CREATE' => 'DESC');
                    $arFilter = Array("COMPANY_ID" => $companyID);
                    $arSelect = Array("*");
                    $arFilter["!=TYPE_ID"] = Array("2","");
                    $result = $oDeal->GetListEx($arOrder, $arFilter, $arSelect, false);
                    while ($fields = $result->Fetch()) {
                        $DealTypeArray = $fields;
                            if ($DealTypeArray['TYPE_ID'] == "COMPLEX") {
                                $newstatuscompany = "19";
                                //echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}	
                            } else if ($DealTypeArray['TYPE_ID'] == "GOODS") {
                                $newstatuscompany = "19";
                                //echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}	
                            } else if ($DealTypeArray['TYPE_ID'] == "SALE") {
                                $newstatuscompany = "2";
                                //echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}	
                            } else if ($DealTypeArray['TYPE_ID'] == "SERVICE") {
                                $newstatuscompany = "20";
                                //echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}	
                            } else if ($DealTypeArray['TYPE_ID'] == "SERVICES") {
                                $newstatuscompany = "19";
                                //echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}	
                            }
else
{
echo "$companyID 6\n";file_put_contents($filepathtxt, $companyID.";косячнаякомпания"."\r\n", FILE_APPEND | LOCK_EX);
$arFields["COMPANY_TYPE"]   = "косячнаякомпания";
if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}
}
                    }
					
$resultstring="";
foreach($arraystatus as $index=>$elementvalue)
{
if($elementvalue>0){$resultstring=$resultstring+" "+$index;} 
}
if(!empty($resultstring)){file_put_contents($filepathtxt, $companyID.";$resultstring"."\r\n", FILE_APPEND | LOCK_EX);}					
					
					
					
                 }
                break;
            case "17"://17
                //echo "Статус компании 17\n";
$arraystatus=[];
                //Установка тенанта компании
                //SetUserField("CRM_COMPANY", $companyID, "UF_TENANT", "296");
                //echo "Компании поставили в поле тенант Старрус\n";

                //Получаем тип компании
                $resultcompany = CCrmCompany::GetByID($companyID);
                $companytype = $resultcompany['COMPANY_TYPE'];


                if ($companytype != "") {
                    //echo "Если тип компании какой-либо, кроме Клиент, то проверка наличия сделок.\n";
                    $arFields["COMPANY_TYPE"] = "CUSTOMER";
                    $arOrder = Array('DATE_CREATE' => 'DESC');
                    $arFilter = Array("COMPANY_ID" => $companyID);
                    $arFilter["!=TYPE_ID"] = Array("2","");
                    $arSelect = Array("*");
                    $result = $oDeal->GetListEx($arOrder, $arFilter, $arSelect, false);
                    while ($fields = $result->Fetch()) {
                        $DealTypeArray = $fields;
                            if ($DealTypeArray['TYPE_ID'] == "COMPLEX") {
                                $newstatuscompany = "19";
                                //echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}	
                            } else if ($DealTypeArray['TYPE_ID'] == "GOODS") {
                                $newstatuscompany = "19";
                                //echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}	
                            } else if ($DealTypeArray['TYPE_ID'] == "SALE") {
                                $newstatuscompany = "2";
                                //echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}	
                            } else if ($DealTypeArray['TYPE_ID'] == "SERVICE") {
                                $newstatuscompany = "20";
                                //echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}	
                            } else if ($DealTypeArray['TYPE_ID'] == "SERVICES") {
                                $newstatuscompany = "19";
                                //echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}	
                            }
else
{
echo "$companyID 6\n";file_put_contents($filepathtxt, $companyID.";косячнаякомпания"."\r\n", FILE_APPEND | LOCK_EX);
$arFields["COMPANY_TYPE"]   = "косячнаякомпания";
if(empty($arraystatus[$arFields["COMPANY_TYPE"]])){$arraystatus[$arFields["COMPANY_TYPE"]]=1;}
else{$arraystatus[$arFields["COMPANY_TYPE"]]++;}
}
                    }
					
$resultstring="";
foreach($arraystatus as $index=>$elementvalue)
{
if($elementvalue>0){$resultstring=$resultstring+" "+$index;} 
}

if(!empty($resultstring)){file_put_contents($filepathtxt, $companyID.";$resultstring"."\r\n", FILE_APPEND | LOCK_EX);}	
					
					
                 }
                break;
            default:
                break;
        }


    }
 // fclose($rsFile1);	
  

}

//Функция поиска дубликатов лидов по TITLE и EMAIL
function leadDublicateCheck($title, $email) {
    $ResMulti = CCrmFieldMulti::GetList(
        array('ID' => 'desc'), array('ENTITY_ID' => 'LEAD', 'TYPE_ID' => 'EMAIL', 'VALUE' => $email)
    );
    if(intval($ResMulti->SelectedRowsCount()) <= 0 ) {
        return false;
    }
    while ($arMulti = $ResMulti->Fetch()) {
        $res = CCrmLead::GetList(array(), array("ID" => $arMulti['ELEMENT_ID'], "TITLE" => $title));
        if(intval($res->SelectedRowsCount()) > 0 ) {
            return $arMulti['ELEMENT_ID'];
        }
    }
    return false;
}

//Поиск дубликата сделки
function findDublicateDeal($arFilter){
    $resDublicateDeal = CCrmDeal::GetListEx(array('ID'=>'ASC'),$arFilter);
    if ($arDublicateDeal = $resDublicateDeal->Fetch()) {
        return $arDublicateDeal;
    }else{
        return false;
    }
}

//Поиск реестра платежей по ID компании
function getReestrPayByCompanyID($companyID) {
    global $IBLOCK_ID_PAY;

    $arSelect = Array("ID", "IBLOCK_ID", "PROPERTY_COMPANY", "PROPERTY_SOSTOYANIE");
    $arFilter = array("IBLOCK_ID" => $IBLOCK_ID_PAY, "PROPERTY_COMPANY" => $companyID);
    $res = CIBlockElement::GetList(Array("DATE_CREATE" => "DESC"), $arFilter, false, false,$arSelect);
    if($arPay = $res->Fetch()){
        return $arPay;
    }
    return false;
}

function findOptimalDealByCompanyID($companyID){
    $arFilter = array(
        'CATEGORY_ID'   => '1', //Продажа клиент
        'COMPANY_ID'    => $companyID,
    );

    $resDublicateDeal = CCrmDeal::GetList(array('ID'=>'DESC'), $arFilter);
    $n = 0;
    $arResult = [];
    while ($arDublicateDeal = $resDublicateDeal->Fetch()) {
        if($n == 0){
            $arResult = $arDublicateDeal;
        }

        if(!empty($arDublicateDeal['UF_DOGOVOR'])){
            return $arResult;
        }
        $n++;
    }

    if(!empty($arResult)){
        return $arResult;
    }
    return false;
}

//Поиск дубликата ККТ
function findDublicateKKT($kkmRegId){
    if($kkmRegId){
        global $IBLOCK_ID_KKT;
        //$IBLOCK_ID_KKT = getIblockIDByCode('registry_kkt');
        $arSelect = Array("ID", "IBLOCK_ID","PROPERTY_RNM_KKT", "PROPERTY_COST_TARIF", "PROPERTY_DEAL");
        $arFilter = array("IBLOCK_ID" => $IBLOCK_ID_KKT,"PROPERTY_RNM_KKT" =>$kkmRegId);
        $res = CIBlockElement::GetList(Array(), $arFilter, false, false,$arSelect);
        if($arKKT = $res->Fetch()){
            return $arKKT;
        }
        return false;
    }else{
        return false;
    }
}

//Получение информации с налоговой
function GetInfo($inn)
{
    $propertyTypeID = 'ITIN';
    $propertyValue = $inn;
    $countryID = 1;

    $result = \Bitrix\Crm\Integration\ClientResolver::resolve(
        $propertyTypeID,
        $propertyValue,
        $countryID
    );
    if (count($result) == 0)
    {
        $result = GetInfoOGRNOnline($inn);
    }
    return $result;
}

function GetInfoOGRNOnline($inn)
{
    $dataJSON = file_get_contents('https://xn--c1aubj.xn--80asehdb/%D0%B8%D0%BD%D1%82%D0%B5%D0%B3%D1%80%D0%B0%D1%86%D0%B8%D1%8F/%D0%BA%D0%BE%D0%BC%D0%BF%D0%B0%D0%BD%D0%B8%D0%B8/?%D0%B8%D0%BD%D0%BD='.$inn);
    $data = json_decode($dataJSON, true);

    if (strlen($inn) == 10 && count($data) > 0)
    {
        $arFields = array(
            'RQ_COMPANY_NAME' => 'shortName',
            'RQ_COMPANY_FULL_NAME' => 'name',
            'RQ_INN' => 'inn',
            'RQ_KPP' => 'kpp',
            'RQ_OGRN' => 'ogrn');
        $result = array();
        foreach ($data as $k => $v) {
            $rq = array();
            foreach ($arFields as $kf => $vf) {
                $rq[$kf] = $v[$vf];
            }
            $result[] = array('fields' => $rq);

        }
        return $result;

    }
    else
    {
        $data = array();
    }

    return $data;
}

function getFmId($elementID, $typeID){
    $dbResFM = CCrmFieldMulti::GetList(
        array('ID' => 'asc'),
        array('ENTITY_ID' => 'COMPANY', 'TYPE_ID' => $typeID, 'ELEMENT_ID' => $elementID)
    );

    if($arFM = $dbResFM->Fetch())
    {
        return $arFM['ID'];
    }

    return "n0";
}

function addIdCreatedCompany($idCompany){
    global $oIBlockElement,
           $IBLOCK_ID_CREATED_COMPANY_CONTACT;

    if(intval($idCompany) <= 0) {
        return false;
    }

    $PROP = array(
        "TYPE_ENTITY"   => array("VALUE" => getPropertyEnumIdByXmlId($IBLOCK_ID_CREATED_COMPANY_CONTACT, "COMPANY")),
        "ID_ENTITY"     => $idCompany,
        "STATUS"        => 0,
    );

    $arLoadProductArray = Array(
        "IBLOCK_ID"         => $IBLOCK_ID_CREATED_COMPANY_CONTACT,
        "PROPERTY_VALUES"   => $PROP,
        "NAME"              => "Новая компания, ID: $idCompany",
    );

    if($PRODUCT_ID = $oIBlockElement->Add($arLoadProductArray)) {
        //echo "New ID: " . $PRODUCT_ID . "\n";
        return $PRODUCT_ID;
    }
    else {
        //echo "Error: " . $oIBlockElement->LAST_ERROR . "\n";
        return false;
    }
}

function addIdCreatedContact($idContact){
    global $oIBlockElement,
           $IBLOCK_ID_CREATED_COMPANY_CONTACT;

    if(intval($idContact) <= 0) {
        return false;
    }

    $PROP = array(
        "TYPE_ENTITY"   => array("VALUE" => getPropertyEnumIdByXmlId($IBLOCK_ID_CREATED_COMPANY_CONTACT, "CONTACT")),
        "ID_ENTITY"     => $idContact,
        "STATUS"        => 0,
    );

    $arLoadProductArray = Array(
        "IBLOCK_ID"         => $IBLOCK_ID_CREATED_COMPANY_CONTACT,
        "PROPERTY_VALUES"   => $PROP,
        "NAME"              => "Новый контакт, ID: $idContact",
    );

    if($PRODUCT_ID = $oIBlockElement->Add($arLoadProductArray)) {
        //echo "New ID: " . $PRODUCT_ID . "\n";
        return $PRODUCT_ID;
    }
    else {
        //echo "Error: " . $oIBlockElement->LAST_ERROR . "\n";
        return false;
    }
}