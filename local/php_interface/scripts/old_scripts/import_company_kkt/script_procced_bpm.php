<?php
/*
 * /import_company_kkt/script_procced.php,
 * /import_company_kkt/script_procced_bpm.php,
 * /import_company_kkt/script_proccedwithtenand.php,
 * /import_company_kkt/script_statusnotnamed.php.
 * Не до конца разобрался для чего используется.
 * Все функции похожи друг на друга. С небольшими отличиями.
 * На вход принимает параметры крона. Затрагивает многие модули Битрикса.
 */

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('CHK_EVENT', true);
ini_set('memory_limit', '-1');

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

@set_time_limit(0);
@ignore_user_abort(true);

global $USER;
$USER->Authorize(372);

CModule::IncludeModule('crm');
CModule::IncludeModule('iblock');

$oLead          = new CCrmLead;
$oContact       = new CCrmContact;
$oDeal          = new CCrmDeal;
$oCompany       = new CCrmCompany;
$CCrmEvent      = new CCrmEvent();
$oIBlockElement = new CIBlockElement();
$oUser          = new CUser;

$IBLOCK_ID_KKT = getIblockIDByCode('registry_kkt');
$IBLOCK_ID_CSV = getIblockIDByCode('import_company');
$IBLOCK_ID_PAY = getIblockIDByCode('reestr_pay');
$IBLOCK_ID_CREATED_COMPANY_CONTACT = getIblockIDByCode('id_created_company_contact');

$offset = 0;
if(array_key_exists(1, $argv)){
    $offset = $argv[1];
}

/*//Ограничение работы скрипта по времени
$hour = date('H');
if($hour == "06"){	
	die;
}*/

echo "\n**************************************************************************************\n";
$START_TIME = mktime();
$n = 0;
while ($n < 100000) {
    //break;
	
	//Проверяем время. Если время работы превышает 55 сек - делаем break.
	$passedTime = (mktime() - $START_TIME);	
	if($passedTime > 55) {
		break;
	}

    $arResult = getOneElement($offset);
	echo "\n--- $arResult[ID] ---\n";
    if ($arResult === false) {
        break;
    }
	
	//break;

    if (parserRow($arResult) === false) {
        $oIBlockElement->SetPropertyValueCode($arResult['ID'], "STATUS", 3); //Устанавливаем статус 3 - ошибка
    } else {
        $oIBlockElement->SetPropertyValueCode($arResult['ID'], "STATUS", 2); //Устанавливаем статус 2 - успех
    }
	$n++;

	//break;
}
$TIME = (mktime() - $START_TIME);
echo "\n\nВремя обработки, сек: $TIME \n";
echo "Всего обработано элементов: $n \n\n";


function getOneElement($offset = 1){
    global $oIBlockElement,
           $IBLOCK_ID_CSV;

    $arFilter = array(
        "IBLOCK_ID"         => $IBLOCK_ID_CSV,
        "PROPERTY_STATUS"   => 0 // берем записи со статусом 0 - не обработан
    );

    $res = $oIBlockElement->GetList(array(), $arFilter, false, array('iNumPage' => $offset, 'nPageSize' => 1));
    
    if($res->SelectedRowsCount() <= $offset){
        return false;
    }

    if($ob = $res->GetNextElement()){
        $arElementFields    = $ob->GetFields();
        $oIBlockElement->SetPropertyValueCode($arElementFields['ID'], "STATUS", 1); //устанавливаем статус 1 - в работе

        $arElement          = $ob->GetProperties();

        $arResult                       = [];
        $arResult['ID']                 = $arElementFields['ID'];
        $arResult['ID_BPM']             = $arElement['ID_BPM']['VALUE'];
        $arResult['INN']                = $arElement['INN']['VALUE'];
        $arResult['KPP']                = $arElement['KPP']['VALUE'];
        $arResult['COMPANY_TYPE']       = $arElement['COMPANY_TYPE']['VALUE']; //TID, Тенант, тип контрагента
        $arResult['NAME_KKT']           = $arElement['NAME_KKT']['VALUE']; //EXTERNAL_CODE, имя ККТ
        $arResult['DATE_CREATE']        = $arElement['DATE_CREATE']['VALUE']; //created_at, Дата добавления ККТ
        $arResult['MODEL']              = $arElement['MODEL']['VALUE'];
        $arResult['NUM_KKT']            = $arElement['NUM_KKT']['VALUE']; //serial_number, Заводской номер ККТ
        $arResult['RNM_KKT']            = $arElement['RNM_KKT']['VALUE']; //fns_ecr_id, РНМ, RegID
        $arResult['NUM_FN']             = $arElement['NUM_FN']['VALUE']; //fiscal_drive_number, Номер ФН
        $arResult['ADDRESS_KKT']        = $arElement['ADDRESS_KKT']['VALUE']; //full_address, Адрес ККТ
        $arResult['NAME_TARIFF']        = $arElement['NAME_TARIFF']['VALUE']; //name_tariff , Тариф
        $arResult['COST_TARIFF']        = $arElement['COST_TARIFF']['VALUE']; //full_period_price, Стоимость тарифа
        $arResult['DATE_FROM']          = $arElement['DATE_FROM']['VALUE']; //from_date, Дата начала тарифа
        $arResult['DATE_TO']            = $arElement['DATE_TO']['VALUE']; //to_date, Дата завершения тарифа
        $arResult['CODE_AGENT']         = $arElement['CODE_AGENT']['VALUE']; //agent_code, Код агента
        $arResult['PROMO_KOD']          = $arElement['PROMO_KOD']['VALUE']; //promo_code, ID Промокода
        $arResult['ACCOUNT_ID']         = $arElement['ACCOUNT_ID']['VALUE']; //promo_code, ID Промокода
        $arResult['BALANCE']            = $arElement['BALANCE']['VALUE']; //promo_code, ID Промокода
		$arResult['BALANCE']            = str_replace(",", ".", $arResult['BALANCE']);
        $arResult['ASSIGNED_LAST_NAME'] = $arElement['ASSIGNED_LAST_NAME']['VALUE']; //promo_code, ID Промокода

        $arResult['EMAIL']          = $arElement['EMAIL']['VALUE']; //responsible_person_email, Email
        $arResult['PHONE']          = $arElement['PHONE']['VALUE']; //contact_phone, Телефон
		
		$arResult['EMAIL'] = (empty($arResult['EMAIL']))? "" : $arResult['EMAIL'];
		$arResult['EMAIL'] = ($arResult['EMAIL'] == "null")? "" : $arResult['EMAIL'];
		if(check_email($arResult['EMAIL']) === false){
			$arResult['EMAIL'] = "";
		}
		$arResult['PHONE'] = (empty($arResult['PHONE']))? "" : $arResult['PHONE'];
		$arResult['PHONE'] = ($arResult['PHONE'] == "null")? "" : $arResult['PHONE'];

        return $arResult;
    }
    return false;
}

function parserRow($arParams){
    global $oLead,
           $oContact,
           $oDeal,
           $oCompany,
           $oUser,
           $CCrmEvent,
           $oIBlockElement,
           $IBLOCK_ID_KKT,
           $IBLOCK_ID_PAY;

    $isKKTDefault       = false;
    $isLead             = false;
    $isReestPayDefault  = false;

    $arParams['INN'] = str_replace("OBSOLETE", "", $arParams['INN']);

    //Проверка на валидность ИНН
    switch (strlen($arParams['INN'])) {
        case 10:
            break;
        case 12:
            break;
        default:
            echo "Не валидный ИНН: " . $arParams['INN'];
            return false;
            break;
    }

    //Проверка на наличие RegID
    if(!empty($arParams['RNM_KKT'])){
        $isKKTDefault = true;
    }

    //Проверка на наличие данных Реестра платежей.
    if(!empty($arParams['ACCOUNT_ID'])){
        $isReestPayDefault = true;
    }

    /**  Поиск ID пользователя по LAST_NAME ответственного */
    $userId = 372; //По умолчанию Bitrix
    if(!empty($arParams['ASSIGNED_LAST_NAME'])) {
        $rsUsers = $oUser->GetList(($by = "ID"), ($order = "desc"), array('LAST_NAME' => $arParams['ASSIGNED_LAST_NAME']), array());
        if ($arResult['USER'] = $rsUsers->Fetch()) {
            $userId = $arResult['USER']['ID'];
            //echo "Нашли пользователя по фамилии $arParams[ASSIGNED_LAST_NAME], ID: $userId.\n";
        }
    }


    /** Поиск/создание компании по реквизитам ИНН и КПП */

    $requisite = new \Bitrix\Crm\EntityRequisite();
    $fieldsInfo = $requisite->getFormFieldsInfo();
    $select = array_keys($fieldsInfo);

    $arFilter = array(
        'RQ_INN' => $arParams['INN'],
        'RQ_KPP' => $arParams['KPP'],
        'ENTITY_TYPE_ID' => 4
    );

    $res = $requisite->getList(
        array(
            'order' => array('SORT' => 'ASC', 'ID' => 'ASC'),
            'filter' => $arFilter,
            'select' => $select
        )
    );

    //Компания найдена
    if ($row = $res->fetch()) {
        $companyID = $row['ENTITY_ID'];
        echo "Найдена существующая компания ID (" . $companyID . ")\n";

        $emailFmId  = getFmId($companyID, 'EMAIL');
        $phoneFmId  = getFmId($companyID, 'PHONE');

        //Обновляем тип компании в зависимости от поля Тенант
        $arFields["COMPANY_TYPE"] = $arParams['COMPANY_TYPE']; //Тип компании(Тенант)

        //Обновляем ID_PROD
        $arFields["UF_ID_PROD"] = $arParams['ID_BPM']; //ID компании с прода

        //Обновление ID ответственного пользователя
        if($userId != 372) {
            $arFields ["ASSIGNED_BY_ID"] = $userId;
        }
		
		if(!empty($arParams['EMAIL'])){
            $arFields['FM']['EMAIL'] =  Array(
                "$emailFmId" => Array(
                    "VALUE"         => $arParams['EMAIL'],
                    "VALUE_TYPE"    => "WORK",
                ),
            );
        }

        if(!empty($arParams['PHONE'])){
            $arFields['FM']['PHONE'] =  Array(
                "$phoneFmId" => Array(
                    "VALUE"         => $arParams['PHONE'],
                    "VALUE_TYPE"    => "WORK",
                ),
            );
        }
		
        if($oCompany->Update($companyID, $arFields)){
            echo "Обновили компанию: $companyID \n";
        } else {
            var_dump($arFields);
            echo "Не удалось обновить компанию \n";
			echo $oCompany->LAST_ERROR;
            return false;
        }

     /*   //ВРЕМЕННЫЙ КОД! ЕСЛИ НАШЛИ КОМПАНИЮ - ЗАВЕРШАЕМ ОБРАБОТКУ ЭЛЕМЕНТА
        //Нужно для быстрой работы скрипта
        return true;*/
    }
    else //Компания не найдена, создаем компанию
    {
        //Получение информации от налоговой по ИНН компании
        $arCompanyInfo = GetInfo($arParams['INN']);

        //Добавление компании на основании данных от Налоговой
        if(array_key_exists(0, $arCompanyInfo) && !empty($arCompanyInfo)){
            $info = Array(
                'fields' => Array(
                    'RQ_INN'            => $arParams['INN'],
                    'RQ_KPP'            => $arParams['KPP'],
                    'RQ_OKVED'          => $arCompanyInfo[0]['fields']['RQ_OKVED'],
                    'SORT'              => 500,
                    'ENTITY_TYPE_ID'    => 4,
                )
            );

            $ogrn = (!empty($arCompanyInfo[0]['fields']['RQ_OGRNIP'])) ? $arCompanyInfo[0]['fields']['RQ_OGRNIP'] : $arCompanyInfo[0]['fields']['RQ_OGRN'];
            $ogrn = trim($ogrn);
            switch (strlen($ogrn)){
                case 15: //ИП
                    $info['fields']['NAME']                     = 'ИП';
                    $info['fields']['RQ_OGRNIP']                = $ogrn;
                    $info['fields']['PRESET_ID']                = 2;
                    $info['fields']['RQ_LAST_NAME']             = $arCompanyInfo[0]['fields']['RQ_LAST_NAME'];
                    $info['fields']['RQ_FIRST_NAME']            = $arCompanyInfo[0]['fields']['RQ_FIRST_NAME'];
                    $info['fields']['RQ_SECOND_NAME']           = $arCompanyInfo[0]['fields']['RQ_SECOND_NAME'];

                    $arFields['TITLE']          = $arCompanyInfo[0]['fields']['RQ_NAME'];
                    $arFields['UF_FULL_NAME']   = $arCompanyInfo[0]['fields']['RQ_NAME'];
                    $arFields['COMPANY_TITLE']  = $arCompanyInfo[0]['fields']['RQ_NAME'];
                    break;
                default: //Организация
                    $info['fields']['NAME']                     = 'Организация';
                    $info['fields']['RQ_COMPANY_NAME']          = (!empty($arCompanyInfo[0]['fields']['RQ_COMPANY_NAME'])) ? $arCompanyInfo[0]['fields']['RQ_COMPANY_NAME'] : $arCompanyInfo[0]['fields']['RQ_COMPANY_FULL_NAME'];
                    $info['fields']['RQ_COMPANY_FULL_NAME']     = (!empty($arCompanyInfo[0]['fields']['RQ_COMPANY_FULL_NAME'])) ? $arCompanyInfo[0]['fields']['RQ_COMPANY_FULL_NAME'] : "";
                    $info['fields']['RQ_OGRN']                  = $ogrn;
                    $info['fields']['PRESET_ID']                = 1;

                    $arFields['TITLE']          = (empty($arCompanyInfo[0]['fields']['RQ_COMPANY_NAME']))? $arCompanyInfo[0]['fields']['RQ_COMPANY_FULL_NAME'] : $arCompanyInfo[0]['fields']['RQ_COMPANY_NAME'];
                    $arFields['UF_FULL_NAME']   = $arCompanyInfo[0]['fields']['RQ_COMPANY_FULL_NAME'];
                    $arFields['COMPANY_TITLE']  = (empty($arCompanyInfo[0]['fields']['RQ_COMPANY_NAME']))? $arCompanyInfo[0]['fields']['RQ_COMPANY_FULL_NAME'] : $arCompanyInfo[0]['fields']['RQ_COMPANY_NAME'];
                    break;
            }

            $arFields["ASSIGNED_BY_ID"] = $userId;
            $arFields['COMPANY_TYPE']   = $arParams['COMPANY_TYPE'];
            $arFields["UF_CRM_INN"]     = $arParams['INN'];
            $arFields["UF_CRM_KPP"]     = $arParams['KPP'];
            $arFields["UF_ID_PROD"]     = $arParams['ID_BPM']; //ID компании с прода

            if(!empty($arParams['EMAIL'])){
				$arFields['FM']['EMAIL'] =  Array(
					"n0" => Array(
						"VALUE"         => $arParams['EMAIL'],
						"VALUE_TYPE"    => "WORK",
					),
				);
			}

			if(!empty($arParams['PHONE'])){
				$arFields['FM']['PHONE'] =  Array(
					"n0" => Array(
						"VALUE"         => $arParams['PHONE'],
						"VALUE_TYPE"    => "WORK",
					),
				);
			}

            //Создаем компанию
            if($companyID = $oCompany->Add($arFields)){
                echo "Создали компанию: $companyID \n";
				
				//Delete BLOCK add file log
				$file = $_SERVER["DOCUMENT_ROOT"] . '/log/reports/csv/id_add_company.txt';
				$current = file_get_contents($file);
				$current .= $companyID . "\n";
				file_put_contents($file, $current);

                //Добавляем запись о новой компании в инфоблок "ID созданных компаний/контактов"
                addIdCreatedCompany($companyID);
            } else {				
                echo "Не удалось создать компанию \n";
				echo $oCompany->LAST_ERROR;
                return false;
            }

            $info['fields']['ENTITY_ID'] = $companyID;

            if($resGk = gk_AddRQ($info)){
                echo "Успешно добавлены реквизиты для компании $companyID\n";
            } else {
                echo "Ошибка добавления реквизитов для компании $companyID\n";
            }
        }

        //Добавляем компанию, если нет информации от Налоговой
        if(empty($arCompanyInfo)){
            echo "Нет информации от налоговой, создаем компанию на основании ИНН/КПП.\n";
            $arFields = array(
                "TITLE"             => "Организация",
                "ASSIGNED_BY_ID"    => $userId,
                "COMPANY_TYPE"      => $arParams['COMPANY_TYPE'],
                "UF_CRM_INN"        => $arParams['INN'],
                "UF_CRM_KPP"        => $arParams['KPP'],
                "UF_ID_PROD"        => $arParams['ID_BPM'], //ID компании с прода
                "FM" => Array(
                    "EMAIL" => Array(
                        "n0" => Array(
                            "VALUE"         => $arParams['EMAIL'],
                            "VALUE_TYPE"    => "WORK",
                        ),
                    ),
                    "PHONE" => Array(
                        "n0" => Array(
                            "VALUE"         => $arParams['PHONE'],
                            "VALUE_TYPE"    => "WORK",
                        ),
                    ),
                )
            );

            //Создаем компанию
            if($companyID = $oCompany->Add($arFields)){
                echo "Создали компанию: $companyID \n";
				
				//Delete BLOCK add file log
				$file = $_SERVER["DOCUMENT_ROOT"] . '/log/reports/csv/id_add_company.txt';
				$current = file_get_contents($file);
				$current .= $companyID . "\n";
				file_put_contents($file, $current);

                //Добавляем запись о новой компании в инфоблок "ID созданных компаний/контактов"
                addIdCreatedCompany($companyID);
            } else {				
                echo "Не удалось создать компанию \n";
				echo $oCompany->LAST_ERROR;
                return false;
            }

            //Добавление реквизитов: Организации или ИП
            if(strlen($arParams['INN']) == 10){
                $info = Array(
                    'fields' => Array(
                        'NAME'              => "Организация",
                        'RQ_INN'            => $arParams['INN'],
                        'RQ_KPP'            => $arParams['KPP'],
                        'SORT'              => 500,
                        'ENTITY_TYPE_ID'    => 4,
                        'ENTITY_ID'         => $companyID,
                        'PRESET_ID'         => 1,
                    )
                );
            } else {
                $info = Array(
                    'fields' => Array(
                        'NAME'              => "ИП",
                        'RQ_INN'            => $arParams['INN'],
                        'RQ_KPP'            => $arParams['KPP'],
                        'SORT'              => 500,
                        'ENTITY_TYPE_ID'    => 4,
                        'ENTITY_ID'         => $companyID,
                        'PRESET_ID'         => 2,
                    )
                );
            }

            //Добавляем реквизиты
            if($resGk = gk_AddRQ($info)){
                echo "Успешно добавлены реквизиты для компании $companyID\n";
            } else {
                echo "Ошибка добавления реквизитов для компании $companyID\n";
            }
        }
    }


    /** Поиск и обновление Лида */

    //Поиск Лида по ИНН
    $resMultiADD = $oLead->GetList(array('ID' => 'asc'), array(
        'TITLE'             => 'регистрация на сайте www.1ofd.ru',
        'UF_CRM_1499414186' => $arParams['INN']
    ));
    if ($arMultiADD = $resMultiADD->Fetch()) {
        echo "Найден лид по ИНН\n"; //DEL
        $isLead = true;
        $arSelectedLead = array(
            "ID"            => $arMultiADD['ID'],
            "LAST_NAME"     => ($arMultiADD['LAST_NAME'] == "noname")? "" : $arMultiADD['LAST_NAME'],
            "NAME"          => ($arMultiADD['NAME'] == "noname")? "" : $arMultiADD['NAME'],
            "SECOND_NAME"   => ($arMultiADD['SECOND_NAME'] == "noname")? "" : $arMultiADD['SECOND_NAME'],
        );
    }

    // Поиск лида по Email, если не удалось найти лида по ИНН
    if(empty($isLead) && !empty($arParams['EMAIL'])) {
        $idLead = leadDublicateCheck('регистрация на сайте www.1ofd.ru', $arParams['EMAIL']);
        if(!empty($idLead)) {
            echo "Найден лид по Email\n"; //DEL
            $isLead = true;

            $resMultiADD = $oLead->GetList(array('ID' => 'asc'), array('ID' => $idLead));
            if ($arMultiADD = $resMultiADD->Fetch()) {
                $arSelectedLead = array(
                    "ID"            => $arMultiADD['ID'],
                    "LAST_NAME"     => ($arMultiADD['LAST_NAME'] == "noname")? "" : $arMultiADD['LAST_NAME'],
                    "NAME"          => ($arMultiADD['NAME'] == "noname")? "" : $arMultiADD['NAME'],
                    "SECOND_NAME"   => ($arMultiADD['SECOND_NAME'] == "noname")? "" : $arMultiADD['SECOND_NAME'],
                    "EMAIL"         => $arParams['EMAIL'],
                );
            }

            //Обновляем Лид, добавляем ИНН/КПП.
            $arFields = array(
                "UF_CRM_1499414186" => $arParams['INN'],
                "UF_KPP" => $arParams['KPP'],

            );

            $resUpdate = $oLead->Update($arSelectedLead['ID'], $arFields);
            if($resUpdate) {
                echo "Лид успешно обновлен\n"; //DEL
            } else {
                echo  "Ошибка обновления лида: (" . $arSelectedLead['ID'] . "). ";
            }
        }
    }

    //Переводим лид в статус зарегестрирован в лк или Архивный
    /*if(!empty($isLead)) {
        $arFields = Array(
            "STATUS_ID" => "2", //зарегистрирован в ЛК
        );

        //Если есть ККТ, переводим в архивный статус
        if($isKKTDefault){
            $arFields['STATUS_ID'] = 1;
        }

        $oLead->Update($arSelectedLead['ID'], $arFields);
        echo "Лид переведён в статус зарегистрирован в ЛК ID (" . $arSelectedLead['ID'] . ")\n";
    }*/

    //Обновление ИНН/КПП у лида
    if(!empty($arSelectedLead['ID'])){
        $arFields = Array(
            "UF_CRM_1499414186" => $arParams['INN'],
            "UF_KPP" => $arParams['KPP'],
        );
        $oLead->Update($arSelectedLead['ID'], $arFields);
        echo "Обновление ИНН/КПП у лида (" . $arSelectedLead['ID'] . ")\n";
    }


    /** Поиск/создание контакта по Email */
    //Поиск контакта по Email
    if(!empty($arParams['EMAIL'])){
        $resMultiDublicateContact = CCrmFieldMulti::GetList(
            array('ID' => 'asc'), array('ENTITY_ID' => 'CONTACT', 'TYPE_ID' => 'EMAIL', 'VALUE' => $arParams['EMAIL'])
        );
        if ($arMultiDublicateContact = $resMultiDublicateContact->Fetch()) {
            echo "Найден существующий контакт по Email сообщения, ContactID $arMultiDublicateContact[ELEMENT_ID] \n";
            $contactID = $arMultiDublicateContact['ELEMENT_ID'];
        }
    }

    //Контакт не найден, создаем контакт на основании данных из лида
    if(empty($contactID)){
        //Создаем контакт на основании данных из лида
        if( !empty($arParams['EMAIL']) &&
            ( !empty($arSelectedLead['LAST_NAME']) || !empty($arSelectedLead['NAME']) || !empty($arSelectedLead['SECOND_NAME'])))
        {
            $arContact = Array(
                'NAME'              => $arSelectedLead['NAME'],
                'SECOND_NAME'       => $arSelectedLead['SECOND_NAME'],
                'LAST_NAME'         => $arSelectedLead['LAST_NAME'],
                'ASSIGNED_BY_ID'    => $userId,
                'COMPANY_ID'        => $companyID,
                "FM" => Array(
                    "EMAIL" => Array(
                        "n0" => Array(
                            "VALUE"         => $arParams['EMAIL'],
                            "VALUE_TYPE"    => "WORK",
                        ),
                    ),
                    "PHONE" => Array(
                        "n0" => Array(
                            "VALUE"         => $arParams['PHONE'],
                            "VALUE_TYPE"    => "WORK",
                        ),
                    ),
                ),
            );
            if($contactID = $oContact->Add($arContact)){
                echo "Создан новый контакт на основании данных из лида: $contactID \n";

                //Добавляем запись о новом контакте в инфоблок "ID созданных компаний/контактов"
                addIdCreatedContact($contactID);

                $guidperson = strtolower(RestBpmBitrix::generate_guid());
                $entity_id = "CRM_CONTACT";
                $uf_guid = "UF_BPMCONTACTID";
                $name = $arSelectedLead['NAME'] . " " . $arSelectedLead['LAST_NAME'];
                SetUserField($entity_id, $contactID, $uf_guid, $guidperson);
                $guidcompany = GetUserField("CRM_COMPANY", $companyID, "UF_CRM_COMPANY_GUID");
                ContactBpmBitrix::contactInsertToBMP($contactID, $guidperson, $guidcompany, $name, $arParams['PHONE'], $arParams['EMAIL']);
            } else {
                echo "Ошибка создания нового контакта \n";
            }
        }
    }

    /** Поиск/создание сделки по ID компании */
    $priceCurrentDeal = 0;

    $arDeal = findOptimalDealByCompanyID($companyID);

    //Сделка найдена
    if ($arDeal && $arDeal['ID']) {
        $idDeal = $arDeal['ID'];
        $priceCurrentDeal = (int)$arDeal['OPPORTUNITY'];
        echo "Найдена сделка ID: $idDeal \n";//DEL

        //Обновление ID ответственного пользователя за сделку
        if($userId != 372) {
            $arFields ["ASSIGNED_BY_ID"] = $userId;

            if($oDeal->Update($idDeal, $arFields)){
                echo "Обновили ответственного за сделку: $idDeal \n";
            } else {
                echo "Не удалось обновить ответственного за сделку \n";
                echo $oDeal->LAST_ERROR;
            }
        }


    }
    else //Сделка не найдена
    {
        $arFields = Array(
            "TITLE"                => "Основная услуга ОФД",
            "STAGE_ID"             => "C1:NEGOTIATION",
            "ASSIGNED_BY_ID"       => $userId,
            "CATEGORY_ID"          => 1,
            'TYPE_ID'              => 2, //Тип: Основная услуга ОФД
            'COMPANY_ID'           => $companyID,
            "UF_KKT" => 0, //кол-во ККТ
        );
        $idDeal = $oDeal->Add($arFields);
        if($idDeal){
            echo "Создали сделку ID: $idDeal \n";
        }else{
            echo "Ошибка создания сделки. \n";
            return false;
        }
    }

    /** Поиск/удаление/создание ККТ по RegID и DealID */
    //Если есть RegID
    if(!empty($isKKTDefault)){
        $priceDeletedKKT    = 0;
        $priceNewKKT        = (int)$arParams['COST_TARIFF'];

        $arKKT = findDublicateKKT($arParams['RNM_KKT']);

        //Удаление дубликат ККТ
        $isDeleteKKT = false;
        /*if (!empty($arKKT) && !empty($arKKT['ID'])) {
            CIBlockElement::Delete($arKKT['ID']);
            echo "Найден дубликат в реестре ККТ, дубликат удалён. \n";
            //Если ID сделки равняется ID сделки в реестре ККТ, значит делаем дикримент цены в сделке.
            if($idDeal == $arKKT['PROPERTY_DEAL_VALUE']){
                $priceDeletedKKT = (int)$arKKT['PROPERTY_COST_TARIF_VALUE'];
            }
            $isDeleteKKT = true;
        }*/

        //Добавляем ККТ
        if($arKKT === false) {
            $arFields = array(
                "ACTIVE" => "Y",
                "IBLOCK_ID" => $IBLOCK_ID_KKT,
                "NAME" => $arParams['NAME_KKT'] . ' Модель (' . $arParams['MODEL'] . ') C\Н (' . $arParams['NUM_KKT'] . ') Р\Н (' . $arParams['RNM_KKT'] . ')',
                "PROPERTY_VALUES" => array(
                    "COMPANY" => $companyID,
                    "DEAL" => $idDeal,
                    "RNM_KKT" => $arParams['RNM_KKT'],
                    "MODEL_KKT" => $arParams['MODEL'],
                    "NUM_KKT" => $arParams['NUM_KKT'],
                    "NUM_FN" => $arParams['NUM_FN'],
                    "ADDR_KKT" => $arParams['ADDRESS_KKT'],
                    "TARIF" => $arParams['NAME_TARIFF'],
                    "COST_TARIF" => $arParams['COST_TARIFF'],
                    "NAIMENOVANIE_KKT" => $arParams['NAME_KKT'],
                    "CODE_AGENT" => $arParams['CODE_AGENT'],
                    "PROMO_KOD" => $arParams['PROMO_KOD'],
                    "DATE_CONNECT" => (!empty($arParams['DATE_CREATE'])) ? $arParams['DATE_CREATE'] : "",
                    "DATE_ACTIVE" => (!empty($arParams['DATE_FROM'])) ? $arParams['DATE_FROM'] : "",
                    "DATE_OFF" => (!empty($arParams['DATE_TO'])) ? $arParams['DATE_TO'] : "",
                )
            );

            if ($kktID = $oIBlockElement->Add($arFields)) {
                echo "Добавили ККТ ID: $kktID \n";
                $verify = KktBitrixBpm::eventSelectKkt($kktID,null);
                KktBitrixBpm::addKkt($verify);
            } else {
                echo "Ошибка добавления ККТ\n";
                return false;
            }

            //Формируем актуальное кол-во ККТ
            $countKKT = 0;
            $resDealCountKKT = $oDeal->GetList(array('ID' => 'desc'), array('ID' => $idDeal), array("ID", "UF_KKT"));
            if ($arDealKKT = $resDealCountKKT->Fetch()) {
                $countKKT = (int)$arDealKKT['UF_KKT'];
                $countKKT = (empty($countKKT)) ? 0 : $countKKT;
                if (empty($isDeleteKKT)) {
                    $countKKT++;
                }
            }

            $arFields = Array(
                'UF_KKT' => $countKKT,
                'TYPE_ID' => 2
            );

            //Обновление суммы сделки
            $newPriceDeal = $priceCurrentDeal + $priceNewKKT - $priceDeletedKKT;
            $newPriceDeal = ((int)$newPriceDeal < 0) ? "0" : (int)$newPriceDeal;

            if ($priceCurrentDeal != $newPriceDeal) {
                $arFields['OPPORTUNITY'] = $newPriceDeal;
            }

            $resUpdate = $oDeal->Update($idDeal, $arFields);
            if ($resUpdate) {
                echo "Обновлена сумма/кол-во_ККТ сделки ID: $idDeal\n";
            } else {
                echo "Ошибка обновления суммы/кол-ва_ККТ сделки ID: $idDeal \n";
            }

            //Создание истории для сделки
            $idEventHistory = $CCrmEvent->Add(
                array(
                    'ENTITY_TYPE' => 'DEAL',
                    'ENTITY_ID' => $idDeal,
                    'EVENT_ID' => 'INFO',
                    'EVENT_TEXT_1' => 'Подключён ККТ с регистрационным номером ' . $arParams['RNM_KKT'],
                )
            );

            if ($idEventHistory) {
                echo "История сделки успешно создана\n";
            } else {
                echo "Ошибка создания истории сделки\n";
            }

        }
    }

    /** Поиск/Создание реестра платежей*/
    $isReestPayAdd = true;
    if(!empty($isReestPayDefault)){
        //Поиск реестра платежей по ID компании(сортировка по ID)
        if($arReestrPay = getReestrPayByCompanyID($companyID)){
            //Если баланс одинаковый - добавление реестра платежей не происходит
            if($arReestrPay['PROPERTY_SOSTOYANIE_VALUE'] == $arParams['BALANCE']){
                $isReestPayAdd = false;
            }
        }

        if($isReestPayAdd === true){
            $arFields = array(
                "ACTIVE" => "Y",
                "IBLOCK_ID" => $IBLOCK_ID_PAY,
                "NAME" => "Актуализация Л/С",
                "PROPERTY_VALUES" => array(
                    "NUM_LIC_SCHET"   => $arParams["ACCOUNT_ID"], //Номер лицевого счета
                    "RNM_KKT"         => (!empty($arParams["RNM_KKT"]))? $arParams["RNM_KKT"] : "",
                    "INN"             => $arParams["INN"],
                    "KPP"             => (!empty($arParams["KPP"]))? $arParams["KPP"] : "",
                    "LAST"            => getPropertyEnumIdByXmlId($IBLOCK_ID_PAY, "LAST_ACTUALIZATION"), //Последнее событие
                    "TYPE"            => getPropertyEnumIdByXmlId($IBLOCK_ID_PAY, "TYPE_ACTUALIZE"), //Тип операции
                    "SUMM"            => 0, //Сумма операции
                    "SOSTOYANIE"      => $arParams['BALANCE'], //Текущее состояние Л/С
                    "COMPANY"         => $companyID,
                    "ASSIGNED"        => $userId,
                    "ACTIVATION_DATE" => date("27.11.2017"),
                )
            );
            if($idElement = $oIBlockElement->Add($arFields, false, false, true)){
                echo "Создан элемент в реестре платежей id " . $idElement . ". ";
            }else{
                echo "Ошибка создания элемента в реестре платежей. ";
            }
        }
    }
}

//Функция поиска дубликатов лидов по TITLE и EMAIL
function leadDublicateCheck($title, $email) {
    $ResMulti = CCrmFieldMulti::GetList(
        array('ID' => 'desc'), array('ENTITY_ID' => 'LEAD', 'TYPE_ID' => 'EMAIL', 'VALUE' => $email)
    );
    if(intval($ResMulti->SelectedRowsCount()) <= 0 ) {
        return false;
    }
    while ($arMulti = $ResMulti->Fetch()) {
        $res = CCrmLead::GetList(array(), array("ID" => $arMulti['ELEMENT_ID'], "TITLE" => $title));
        if(intval($res->SelectedRowsCount()) > 0 ) {
            return $arMulti['ELEMENT_ID'];
        }
    }
    return false;
}

//Поиск дубликата сделки
function findDublicateDeal($arFilter){
    $resDublicateDeal = CCrmDeal::GetListEx(array('ID'=>'ASC'),$arFilter);
    if ($arDublicateDeal = $resDublicateDeal->Fetch()) {
        return $arDublicateDeal;
    }else{
        return false;
    }
}

//Поиск реестра платежей по ID компании
function getReestrPayByCompanyID($companyID) {
    global $IBLOCK_ID_PAY;

    $arSelect = Array("ID", "IBLOCK_ID", "PROPERTY_COMPANY", "PROPERTY_SOSTOYANIE");
    $arFilter = array("IBLOCK_ID" => $IBLOCK_ID_PAY, "PROPERTY_COMPANY" => $companyID);
    $res = CIBlockElement::GetList(Array("DATE_CREATE" => "DESC"), $arFilter, false, false,$arSelect);
    if($arPay = $res->Fetch()){
        return $arPay;
    }
    return false;
}

function findOptimalDealByCompanyID($companyID){
    $arFilter = array(
        'CATEGORY_ID'   => '1', //Продажа клиент
        'COMPANY_ID'    => $companyID,
    );

    $resDublicateDeal = CCrmDeal::GetList(array('ID'=>'DESC'), $arFilter);
    $n = 0;
    $arResult = [];
    while ($arDublicateDeal = $resDublicateDeal->Fetch()) {
        if($n == 0){
            $arResult = $arDublicateDeal;
        }

        if(!empty($arDublicateDeal['UF_DOGOVOR'])){
            return $arResult;
        }
        $n++;
    }

    if(!empty($arResult)){
        return $arResult;
    }
    return false;
}

//Поиск дубликата ККТ
function findDublicateKKT($kkmRegId){
    if($kkmRegId){
        global $IBLOCK_ID_KKT;
        //$IBLOCK_ID_KKT = getIblockIDByCode('registry_kkt');
        $arSelect = Array("ID", "IBLOCK_ID","PROPERTY_RNM_KKT", "PROPERTY_COST_TARIF", "PROPERTY_DEAL");
        $arFilter = array("IBLOCK_ID" => $IBLOCK_ID_KKT,"PROPERTY_RNM_KKT" =>$kkmRegId);
        $res = CIBlockElement::GetList(Array(), $arFilter, false, false,$arSelect);
        if($arKKT = $res->Fetch()){
            return $arKKT;
        }
        return false;
    }else{
        return false;
    }
}

//Получение информации с налоговой
function GetInfo($inn)
{
    $propertyTypeID = 'ITIN';
    $propertyValue = $inn;
    $countryID = 1;

    $result = \Bitrix\Crm\Integration\ClientResolver::resolve(
        $propertyTypeID,
        $propertyValue,
        $countryID
    );
    if (count($result) == 0)
    {
        $result = GetInfoOGRNOnline($inn);
    }
    return $result;
}

function GetInfoOGRNOnline($inn)
{
    $dataJSON = file_get_contents('https://xn--c1aubj.xn--80asehdb/%D0%B8%D0%BD%D1%82%D0%B5%D0%B3%D1%80%D0%B0%D1%86%D0%B8%D1%8F/%D0%BA%D0%BE%D0%BC%D0%BF%D0%B0%D0%BD%D0%B8%D0%B8/?%D0%B8%D0%BD%D0%BD='.$inn);
    $data = json_decode($dataJSON, true);

    if (strlen($inn) == 10 && count($data) > 0)
    {
        $arFields = array(
            'RQ_COMPANY_NAME' => 'shortName',
            'RQ_COMPANY_FULL_NAME' => 'name',
            'RQ_INN' => 'inn',
            'RQ_KPP' => 'kpp',
            'RQ_OGRN' => 'ogrn');
        $result = array();
        foreach ($data as $k => $v) {
            $rq = array();
            foreach ($arFields as $kf => $vf) {
                $rq[$kf] = $v[$vf];
            }
            $result[] = array('fields' => $rq);

        }
        return $result;

    }
    else
    {
        $data = array();
    }

    return $data;
}

function getFmId($elementID, $typeID){
    $dbResFM = CCrmFieldMulti::GetList(
        array('ID' => 'asc'),
        array('ENTITY_ID' => 'COMPANY', 'TYPE_ID' => $typeID, 'ELEMENT_ID' => $elementID)
    );

    if($arFM = $dbResFM->Fetch())
    {
        return $arFM['ID'];
    }

    return "n0";
}

function addIdCreatedCompany($idCompany){
    global $oIBlockElement,
           $IBLOCK_ID_CREATED_COMPANY_CONTACT;

    if(intval($idCompany) <= 0) {
        return false;
    }

    $PROP = array(
        "TYPE_ENTITY"   => array("VALUE" => getPropertyEnumIdByXmlId($IBLOCK_ID_CREATED_COMPANY_CONTACT, "COMPANY")),
        "ID_ENTITY"     => $idCompany,
        "STATUS"        => 0,
    );

    $arLoadProductArray = Array(
        "IBLOCK_ID"         => $IBLOCK_ID_CREATED_COMPANY_CONTACT,
        "PROPERTY_VALUES"   => $PROP,
        "NAME"              => "Новая компания, ID: $idCompany",
    );

    if($PRODUCT_ID = $oIBlockElement->Add($arLoadProductArray)) {
        echo "New ID: " . $PRODUCT_ID . "\n";
        return $PRODUCT_ID;
    }
    else {
        echo "Error: " . $oIBlockElement->LAST_ERROR . "\n";
        return false;
    }
}

function addIdCreatedContact($idContact){
    global $oIBlockElement,
           $IBLOCK_ID_CREATED_COMPANY_CONTACT;

    if(intval($idContact) <= 0) {
        return false;
    }

    $PROP = array(
        "TYPE_ENTITY"   => array("VALUE" => getPropertyEnumIdByXmlId($IBLOCK_ID_CREATED_COMPANY_CONTACT, "CONTACT")),
        "ID_ENTITY"     => $idContact,
        "STATUS"        => 0,
    );

    $arLoadProductArray = Array(
        "IBLOCK_ID"         => $IBLOCK_ID_CREATED_COMPANY_CONTACT,
        "PROPERTY_VALUES"   => $PROP,
        "NAME"              => "Новый контакт, ID: $idContact",
    );

    if($PRODUCT_ID = $oIBlockElement->Add($arLoadProductArray)) {
        echo "New ID: " . $PRODUCT_ID . "\n";
        return $PRODUCT_ID;
    }
    else {
        echo "Error: " . $oIBlockElement->LAST_ERROR . "\n";
        return false;
    }
}