<?php
/*
 * Не до конца разобрался для чего используется. На вход принимает параметры крона,
 * обновляется свойство “STATUS” для ИБ с кодом “id_created_company_contact”
 * также как то связан с работой bpm
 */

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('CHK_EVENT', true);
ini_set('memory_limit', '-1');

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

@set_time_limit(0);
@ignore_user_abort(true);

global $USER;
$USER->Authorize(372);

CModule::IncludeModule('iblock');

$oIBlockElement = new CIBlockElement();

$IBLOCK_ID_CREATED_COMPANY_CONTACT = getIblockIDByCode('id_created_company_contact');

$offset = 0;
if(array_key_exists(1, $argv)){
    $offset = $argv[1];
}

$START_TIME = mktime();
$n = 0;

while ($n < 10) {

    $arResult = getOneElement($offset);
    echo "--- $arResult[ID] ---\n";
    if ($arResult === false) {
        break;
    }

    if (parserRow($arResult) === false) {
        $oIBlockElement->SetPropertyValueCode($arResult['ID'], "STATUS", 3); //Устанавливаем статус 3 - ошибка
    } else {
        $oIBlockElement->SetPropertyValueCode($arResult['ID'], "STATUS", 2); //Устанавливаем статус 2 - успех
    }
    $n++;

    //break;
}
$TIME = (mktime() - $START_TIME);
echo "\n\nВремя обработки, сек: $TIME \n\n";

function getOneElement($offset = 1){
    global $oIBlockElement,
           $IBLOCK_ID_CREATED_COMPANY_CONTACT;

    $arFilter = array(
        "IBLOCK_ID"         => $IBLOCK_ID_CREATED_COMPANY_CONTACT,
        "PROPERTY_STATUS"   => 0 // берем записи со статусом 0 - не обработан
    );

    $res = $oIBlockElement->GetList(array(), $arFilter, false, array('iNumPage' => $offset, 'nPageSize' => 1));

    if( ($res->SelectedRowsCount() <= $offset) && ($offset != "1") ){
        return false;
    }

    if($ob = $res->GetNextElement()){
        $arElementFields    = $ob->GetFields();
        $arElement          = $ob->GetProperties();

        $oIBlockElement->SetPropertyValueCode($arElementFields['ID'], "STATUS", 1); //устанавливаем статус 1 - в работе

        $arResult = [];
        $arResult['ID']             = $arElementFields['ID'];
        $arResult['ID_ENTITY']      = $arElement['ID_ENTITY']['VALUE'];
        $arResult['TYPE_ENTITY']    = $arElement['TYPE_ENTITY']['VALUE_XML_ID'];

        return $arResult;
    }
    return false;
}

function parserRow($arParams)
{
    //Проверка на валидность ID_ENTITY
    if(intval($arParams['ID_ENTITY']) <= 0) {
        return false;
    }

    //Обработка компаний, синхронизация с BPM
    if($arParams['TYPE_ENTITY'] == "COMPANY") {
        echo "Обработка компаний, синхронизация с BPM\n";
        $result = RestBpmBitrix::verify(null, $arParams['ID_ENTITY']);
        CrmBitrixBpm::contragenAdd($result);
    }

    //Обработка контактов, синхронизация с BPM
    if($arParams['TYPE_ENTITY'] == "CONTACT") {
        echo "Обработка контактов, синхронизация с BPM\n";
        ContactBpmBitrix::contactInsertToBmpUsingIdContact($arParams['ID_ENTITY']);
    }
}