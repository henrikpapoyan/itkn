<?php

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
define('CHK_EVENT', true);

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
require($_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/Classes/common/DecrimentPrice.class.php");

\Bitrix\Main\Loader::includeModule('highloadblock');

@set_time_limit(0);
@ignore_user_abort(true);

global $USER;
$USER->Authorize(372);

$AM = new DecrimentPrice();

//Получаем письмо из HigloadBlock
use Bitrix\Highloadblock as HL;

$HL_Infoblock_ID = 3; //ID предварительно созданного HL ИБ
$hlblock = HL\HighloadBlockTable::getById($HL_Infoblock_ID)->fetch();
$entity = HL\HighloadBlockTable::compileEntity($hlblock);
$entity_data_class = $entity->getDataClass();

$START_TIME = mktime();

$offset = 0;
if(array_key_exists(1, $argv)){
    $offset = $argv[1];
}

$rsData = $entity_data_class::getList(array(
    "order" => array("ID" => "DESC"),
    'select' => array('*'),
    'filter' => array(
        'UF_PROCESSED' => '1', //Берем отработанные письма
        'UF_EVENT_TYPE' => 'add_one_kkt'
    ),
	'limit' => 10,
    'offset' => $offset
));

$count = 0;
while ($el = $rsData->fetch()) {		
    print_r("Event Type: " . $el['UF_EVENT_TYPE'] . "\n");
    print_r("ID Element: " . $el['ID'] . "\n");	
	
	//break;
	
    $msg = $el['UF_TEXTMAIL'];
    if ($AM->parseOne($msg, $el['ID'], $entity_data_class) === false) {
        continue;
    }

    $data = array();
    $data['UF_PROCESSED'] = "0";
    $data['UF_IN_WORK'] = "0";
    $data['UF_RESULT'] = "";
    $data['UF_ERROR'] = "0";
    $data['UF_ERROR_TEXT'] = "";

    $result = $entity_data_class::update($el['ID'], $data);

    $count++;
    if ($count >= 11) {
        break;
    }
}

$TIME = (mktime() - $START_TIME);
echo "Время обработки, сек: $TIME \n\n";

$USER->Logout(); //делегируем пользователя
