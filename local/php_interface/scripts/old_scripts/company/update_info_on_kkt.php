<?
/*
 * Запускается по крон, выбирает данные из ИБ “Реестр ККТ”
 * у которых “Дата деактивации из CSV” (PROPERTY_DATE_OFF_CSV) <= “Текущей даты”
 * и устанавливает для найденных элементов свойство “Активирован” в значение “AC_DEACTIVE”
 */

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
ini_set('memory_limit', '-1');

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");
global $USER;
global $DB;
$USER->Authorize(372);

$START_TIME_ONE = mktime();
$IBLOCK_ID = getIblockIDByCode('registry_kkt');
echo date($DB->DateFormatToPHP(CLang::GetDateFormat("SHORT")), time());
$arFilter = Array(
    "<=PROPERTY_DATE_OFF_CSV" => date($DB->DateFormatToPHP(CLang::GetDateFormat("SHORT")), time()),
    "IBLOCK_ID" => $IBLOCK_ID,
);

$oIBlockElement = new CIBlockElement();
$IBLOCK_ID = getIblockIDByCode('registry_kkt');
$arSelect = Array("ID", "IBLOCK_ID", "PROPERTY_RNM_KKT", "PROPERTY_COST_TARIF", "PROPERTY_DEAL", "PROPERTY_NUM_KKT", "PROPERTY_COMPANY");
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
//echo $res->SelectedRowsCount();

if ($arKKT = $res->Fetch()) {
    echo $arKKT["ID"];
    $arProperty = [];
    $arProperty['ACTIVE'] = getPropertyEnumIdByXmlId($IBLOCK_ID, "AC_DEACTIVE");
    $oIBlockElement->SetPropertyValuesEx(
        $arKKT["ID"],
        $IBLOCK_ID,
        $arProperty
    );
}


$TIME_ONE = mktime() - $START_TIME_ONE;
echo "ОБработка ККТ , сек: $TIME_ONE \n";

?>