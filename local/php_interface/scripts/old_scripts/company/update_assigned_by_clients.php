<?php
/*
 * Обновляет ответственного за контрагента, данные берет из файла clients.csv.
 */


define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('CHK_EVENT', true);
ini_set('memory_limit', '-1');

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

@set_time_limit(0);
@ignore_user_abort(true);

global $USER;
$USER->Authorize(372);

CModule::IncludeModule('crm');
$oCompany = new CCrmCompany;
$oUser = new CUser;

$row = 1;
if (($handle = fopen("clients.csv", "r")) !== FALSE) {
    $START_TIME = mktime();
    while (($data = fgetcsv($handle, 0, ";")) !== FALSE) {
        $arResult = [];
        $arResult['COMPANY_NAME']   = $data[0];
        $arResult['INN']            = $data[1];
        $arResult['LAST_NAME']      = $data[2];

        setAssignedCompany($arResult);

        /*if($row == 2){
            var_dump($arResult);
            setAssignedCompany($arResult);
            break;
        }*/

        $row++;
    }
    $TIME = (mktime() - $START_TIME);
    echo "Общее кол-во строк: $row \n";
    echo "Время обработки, сек: $TIME \n\n";

    fclose($handle);
}

function setAssignedCompany($arParams){
    global $oCompany, $oUser;

    /** Проверка на наличие обязательных полей */

    //Проверка на валидность ИНН
    switch (strlen($arParams['INN'])) {
        case 10:
            break;
        case 12:
            break;
        default:
            echo "Не валидный ИНН: " . $arParams['INN'] . "\n";
            return false;
            break;
    }

    //Проверка на наличие фамилии
    if(empty($arParams['LAST_NAME'])){
        echo "Фамилия отсутствует. \n";
        return false;
    }


    /**  Поиск ID пользователя по LAST_NAME */

    $rsUsers = $oUser->GetList(($by="ID"), ($order="desc"), array('LAST_NAME' => $arParams['LAST_NAME']), array());

    if ($arResult['USER'] = $rsUsers->Fetch())
    {
        $userId = $arResult['USER']['ID'];
        //echo "Нашли пользователя по фамилии $arParams[LAST_NAME], ID: $userId.\n";
    } else {
        echo "Пользователь по фамилии не найден: $arParams[LAST_NAME].\n";
        return false;
    }


    /** Поиск компании по реквизиту ИНН */

    $requisite = new \Bitrix\Crm\EntityRequisite();
    $fieldsInfo = $requisite->getFormFieldsInfo();
    $select = array_keys($fieldsInfo);

    $arFilter = array(
        'RQ_INN' => $arParams['INN'],
        'ENTITY_TYPE_ID' => 4 //Компания
    );

    $res = $requisite->getList(
        array(
            'order' => array('SORT' => 'ASC', 'ID' => 'ASC'),
            'filter' => $arFilter,
            'select' => $select
        )
    );

    if(intval($res->getSelectedRowsCount()) <= 0){
        echo "Компания по ИНН не найдена, ИНН: $arParams[INN].\n";
    }

    //Компания(и) найдена
    while ($row = $res->fetch()) {
        $companyID = $row['ENTITY_ID'];
        $arFields = array(
            "ASSIGNED_BY_ID" => $userId
        );
        if($oCompany->Update($companyID, $arFields)){
            ;;
        } else {
            echo "Не удалось обновить ответственного($userId) у компани $companyID.\n";
            return true;
        }
    }

    return true;
}