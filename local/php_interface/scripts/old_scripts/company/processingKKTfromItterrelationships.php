<?
/*
 * Устанавливает взаимосвязь для агента и контрагента.
 */

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
ini_set('memory_limit', '-1');

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

function relationshipAgentKontragent2 ($agentID = 0, $kontragentID = 0) {
    CModule::IncludeModule("iblock");
    $logger = Logger::getLogger('KKTHandler','KKTHandler/handlerRelat.log');
    $logger->log('relationshipAgentKontragent $agentID= '.$agentID.' | $kontragentID = '.$kontragentID);
    $el = new CIBlockElement();
    //Типы взаимосвязей
    $rsLinkType = $el->GetList(
        array("ID"=>"ASC"),
        array("IBLOCK_ID"=>GetIBlockIDByCode("company_link_type"), "ACTIVE"=>"Y"),
        false,
        false,
        array("ID", "NAME")
    );
    $materinskayaID = 0;
    $dochernayaID = 0;
    while($arLinkType = $rsLinkType->GetNext()){
        if($arLinkType["NAME"]=="Материнская"){
            $materinskayaID = $arLinkType["ID"];
        }
        if($arLinkType["NAME"]=="Дочерняя"){
            $dochernayaID = $arLinkType["ID"];
        }
    }
    $logger->log('relationshipAgentKontragent $materinskayaID= '.$materinskayaID.' | $dochernayaID = '.$dochernayaID);
    //Добавление взаимосвязи для АГЕНТА
    $dbDublikate = $el->GetList(
        array("ID" => "ASC"),
        array("IBLOCK_ID" => GetIBlockIDByCode("company_interrelationships"), "ACTIVE" => "Y", "PROPERTY_MAIN_COMPANY" => $agentID),
        false,
        false,
        array("ID", "IBLOCK_ID", "NAME", "PROPERTY_MAIN_COMPANY", "PROPERTY_COMPANY_LINK", "PROPERTY_COMPANY_LINK_TYPE")
    );
    if ($dbDublikate->SelectedRowsCount() == 0) {
        $PROP = array(
            "MAIN_COMPANY" => $agentID,
            "COMPANY_LINK" => $kontragentID,
            "COMPANY_LINK_TYPE" => $dochernayaID,
        );
        $arLoadProductArray = Array(
            "IBLOCK_SECTION_ID" => false,          // элемент лежит в корне раздела
            "IBLOCK_ID" => GetIBlockIDByCode("company_interrelationships"),
            "PROPERTY_VALUES" => $PROP,
            "NAME" => "Взаимосвязь для компании " . $agentID,
            "ACTIVE" => "Y",
        );

        if ($PRODUCT_ID = $el->Add($arLoadProductArray)) {
            $logger->log('relationshipAgentKontragent АГЕНТ add ID = '.$PRODUCT_ID);
        } else {
            $logger->log('relationshipAgentKontragent АГЕНТ add ERROR= '.$el->LAST_ERROR);
        }
    } else {
        $arInterrelationships = $dbDublikate->GetNext();
        $arCurCompanys = $arInterrelationships["PROPERTY_COMPANY_LINK_VALUE"];
        $arCurLinkTypes = $arInterrelationships["PROPERTY_COMPANY_LINK_TYPE_VALUE"];
        if (!in_array($kontragentID, $arCurCompanys)) {
            $arCurCompanys[] = $kontragentID;
            $arCurLinkTypes[] = $dochernayaID;
            CIBlockElement::SetPropertyValuesEx(
                $arInterrelationships["ID"],
                $arInterrelationships["IBLOCK_ID"],
                array(
                    "COMPANY_LINK" => $arCurCompanys,
                    "COMPANY_LINK_TYPE" => $arCurLinkTypes,
                )
            );
            $logger->log('relationshipAgentKontragent АГЕНТ Новая взаимосвязь успешно добавлена');
        } else {
            $logger->log('relationshipAgentKontragent АГЕНТ Данная компания уже добавлена');
        }
    }

    //Добавление взаимосвязи для КОНТРАГЕНТА
    $dbDublikate = $el->GetList(
        array("ID" => "ASC"),
        array("IBLOCK_ID" => GetIBlockIDByCode("company_interrelationships"), "ACTIVE" => "Y", "PROPERTY_MAIN_COMPANY" => $kontragentID),
        false,
        false,
        array("ID", "IBLOCK_ID", "NAME", "PROPERTY_MAIN_COMPANY", "PROPERTY_COMPANY_LINK", "PROPERTY_COMPANY_LINK_TYPE")
    );
    if ($dbDublikate->SelectedRowsCount() == 0) {
        $PROP = array(
            "MAIN_COMPANY" => $kontragentID,
            "COMPANY_LINK" => $agentID,
            "COMPANY_LINK_TYPE" => $materinskayaID,
        );
        $arLoadProductArray = Array(
            "IBLOCK_SECTION_ID" => false,          // элемент лежит в корне раздела
            "IBLOCK_ID" => GetIBlockIDByCode("company_interrelationships"),
            "PROPERTY_VALUES" => $PROP,
            "NAME" => "Взаимосвязь для компании " . $kontragentID,
            "ACTIVE" => "Y",
        );

        if ($PRODUCT_ID = $el->Add($arLoadProductArray)) {
            $logger->log('relationshipAgentKontragent КОНТРАГЕНТ add ID = '.$PRODUCT_ID);
        } else {
            $logger->log('relationshipAgentKontragent КОНТРАГЕНТ add ERROR= '.$el->LAST_ERROR);
        }
    } else {
        $arInterrelationships = $dbDublikate->GetNext();
        $arCurCompanys = $arInterrelationships["PROPERTY_COMPANY_LINK_VALUE"];
        $arCurLinkTypes = $arInterrelationships["PROPERTY_COMPANY_LINK_TYPE_VALUE"];
        if (!in_array($agentID, $arCurCompanys)) {
            $arCurCompanys[] = $agentID;
            $arCurLinkTypes[] = $materinskayaID;
            CIBlockElement::SetPropertyValuesEx(
                $arInterrelationships["ID"],
                $arInterrelationships["IBLOCK_ID"],
                array(
                    "COMPANY_LINK" => $arCurCompanys,
                    "COMPANY_LINK_TYPE" => $arCurLinkTypes,
                )
            );
            $logger->log('relationshipAgentKontragent КОНТРАГЕНТ Новая взаимосвязь успешно добавлена');
        } else {
            $logger->log('relationshipAgentKontragent КОНТРАГЕНТ Данная компания уже добавлена');
        }
    }

}

CModule::IncludeModule("iblock");
global $USER;
global $DB;
$USER->Authorize(372);
global $DB;
$kktIBlockID = GetIBlockIDByCode("registry_kkt");
//$strSql ='ALTER TABLE b_crm_requisite CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci';
//$rsProp = $DB->Query($strSql, false, $err_mess.__LINE__);
$strSql = "SELECT
                  ID, CODE
                FROM
                  b_iblock_property
                WHERE
                  (CODE = 'COMPANY' OR CODE = 'CODE_AGENT') AND IBLOCK_ID=$kktIBlockID
            ";
$START_TIME_ONE = mktime();
$rsProp = $DB->Query($strSql, false, $err_mess.__LINE__);
$TIME_ONE = mktime() - $START_TIME_ONE;
echo "\n\rЗапрос свойств ИБ ККТ , сек: $TIME_ONE \n";
while($arProp = $rsProp->Fetch()){
    if($arProp["CODE"]=="COMPANY"){
        $propCompanyID = $arProp["ID"];
    }
    if($arProp["CODE"]=="CODE_AGENT"){
        $propCodeAgentID = $arProp["ID"];
    }
}
echo '$propCompanyID = '.$propCompanyID.' | $propCodeAgentID = '.$propCodeAgentID.'<br />';
if($propCompanyID>0&&$propCodeAgentID>0){
    $strSql = "SELECT
                    b_iblock_element.ID,
                    b_iblock_element_prop_s$kktIBlockID.PROPERTY_$propCompanyID,
                    b_iblock_element_prop_s$kktIBlockID.PROPERTY_$propCodeAgentID,
                    b_crm_requisite.ENTITY_ID
                  FROM
                    b_iblock_element,
                    b_iblock_element_prop_s$kktIBlockID,
                    b_crm_requisite
                  WHERE
                    b_iblock_element.IBLOCK_ID=$kktIBlockID AND
                    b_iblock_element_prop_s$kktIBlockID.PROPERTY_$propCompanyID!='' AND
                    b_iblock_element_prop_s$kktIBlockID.PROPERTY_$propCodeAgentID!='' AND
                    b_iblock_element_prop_s$kktIBlockID.IBLOCK_ELEMENT_ID=b_iblock_element.ID AND 
                    b_crm_requisite.ENTITY_TYPE_ID = 4 AND 
                    b_crm_requisite.RQ_INN = b_iblock_element_prop_s$kktIBlockID.PROPERTY_$propCodeAgentID
                  ORDER BY b_iblock_element.DATE_CREATE ASC";
    $START_TIME_ONE = mktime();
    $rsKKT = $DB->Query($strSql, false, $err_mess.__LINE__);
    $TIME_ONE = mktime() - $START_TIME_ONE;
    echo "\n\rЗапрос ККТ SQL, сек: $TIME_ONE \n";
    $arKKTS = array();
    $selKKTCount = $rsKKT->SelectedRowsCount();
    echo '$rsKKT->SelectedRowsCount() = '.$rsKKT->SelectedRowsCount().'<br />';
    $KKT_SUMM = 0;
    $LeadsFromKKTCount = 0;
    $addedRNM_KKT = array();
    $START_TIME_ONE = mktime();
    $showCount = 0;
    while($arKKT = $rsKKT->Fetch()){
        relationshipAgentKontragent2($arKKT["ENTITY_ID"], intval($arKKT["PROPERTY_487"]));
    }
    $TIME_ONE = mktime() - $START_TIME_ONE;
    echo "\n\rОБработка $selKKTCount ККТ , сек: $TIME_ONE \n";
}
?>