<?php
/*
 * Выполняется выборка всех контрагентов у которых “Тип компании” установлен “Партнер” или “Клиент+Агент”
 * и устанавливает значение “30%” для пользовательского свойства контрагента  “Ставка вознаграждения”
 */



define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('CHK_EVENT', true);
ini_set('memory_limit', '-1');

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

@set_time_limit(0);
@ignore_user_abort(true);

global $USER;
$USER->Authorize(372);

CModule::IncludeModule('crm');

$arFilter = array(
    'COMPANY_TYPE' => array('PARTNER', '2')
);

$CCrmCompany = new CCrmCompany();
$rsCompany = $CCrmCompany->GetList(array('ID' => 'ASC'), $arFilter);

$n = 0;
$START_TIME = mktime();
while($arCompany = $rsCompany->Fetch() )
{
    $n++;    
    if (empty($arCompany['UF_RATE']))
    {
        $arFields = array();
        $arFields['UF_RATE'] = 109; //30%		
        $ret = $CCrmCompany->Update($arCompany['ID'], $arFields);		
    }

    if ($n == 1)
    {
        //break;
    }
}

$TIME = (mktime() - $START_TIME) / 60;
echo "Общее кол-во строк: $n \n";
echo "Время обработки, мин: $TIME \n\n";
