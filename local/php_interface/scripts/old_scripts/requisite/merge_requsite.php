<?php
/*
 * Выполняет проверку реквизитов компании.
 * Удаляет, обновляет или создает другую компанию в случае, если для контрагента указано несколько реквизитов.
 */

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('CHK_EVENT', true);
ini_set('memory_limit', '-1');

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

@set_time_limit(0);
@ignore_user_abort(true);

global $USER;
$USER->Authorize(372);

CModule::IncludeModule('crm');

$resBitrix = Reports::getAllCompanyBitrix();

foreach ($resBitrix as $key => $val){
    //Компании, которые имеют более 1-го реквизита
    if($val['COUNT_REQUISITE'] > 1) {
        $arrCompamy[] = $val;
    }
}

$START = mktime();
$res = Reports::mergeRequisite($arrCompamy);
$END = mktime() - $START;
echo "Время mergeRequisite, сек: $END \n\n";

