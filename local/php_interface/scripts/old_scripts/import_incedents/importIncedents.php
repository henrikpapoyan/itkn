<?
/*
 * Выполняет добавление элементов в ИБ с символьным кодом
 * “incedent_list” из файла
 * “spisok_incidentov2.csv”
 */


require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
//die();
$el = new CIBlockElement();
$arEmployers = array(
    "Vadim Izvekov" => "241",
    "Ilya Bukanov" => "190",
    "Ilya Dubov" => "279",
    "Vlad Rudkovskiy" => "189",
    "Valentin Bakarinov" => "267",
    "Arsen Kamensky" => "304",
    "Alex Lebedev" => "180",
    "Sergey Gavrilin" => "255",
    "Аксенов" => "336",
    "Мурьянов Анатолий" => "214",
    "Dmitriy Osipov" => "292",
    "Alexey Kukushkin" => "438",
    "Anton Chelnokov" => "219",
    "Dmitry Ostromogilsky" => "146",
    "Denis Eresko" => "309",
    "Anton Kolomytsev" => "426",
    "Aleksey Derevyankin" => "185",
    "Artemii Sergeev" => "373",
    "Rotmistov Vitaliy" => "414",
    "Dmitry Sorokin" => "303",
    "Dmitriy Sorokin" => "303",
    "Seleznev Konstantin" => "381",
    "Виктор Богатырев" => "173",
    "Vladislav Rudkovskiy" => "189",
    "Oleg Tarasov" => "425",
    "Alexander Vanteev" => "239",
    "Alexey Aksenov" => "336",
    "Denis Zayashnikov" => "222",
    "Andrian Bezdolniy" => "251",
    "Александр Шемякин" => "275",
    "Ilya Ignatyev" => "415",
    "Алексей Аксенов" => "336",
    "Арсен Каменский" => "304",
    "Дмитрий Осипов" => "292",
    "Извеков Вадим" => "241",
    "Каменский Арсен" => "304",
    "Артемий Сергеев" => "373",
    "Sergey Levadsky" => "232",
    "Artem Zhirkov" => "210",
    "Andrian Bezdolny" => "251",
);
if (($handle = fopen("spisok_incidentov2.csv", "r")) !== FALSE) {
    $showCount = 0;
    $addCount = 0;
    while (($data = fgetcsv($handle, 10000, "$", '"')) !== FALSE) {
        if($showCount != 0) {
            $arOtvet = explode(',', $data[10]);
            $arProp = array(
                "DATA_I_VREMYA_VOZNIKNOVENIYA_INTSIDENTA" => $data[1],
                "DATA_I_VREMYA_OBNARUZHENIYA_INTSIDENTA" => $data[2],
                "ISTOCHNIK_INFORMATSII_OB_INTSIDENTE" => $data[3],
                "TEKHNICHESKOE_OPISANIE" => $data[5],
                "RABOTY_DLYA_USTRANENIYA" => $data[6],
                "DATA_I_VREMYA_USTRANENIYA_INTSIDENTA" => $data[7],
                "JIRA" => $data[8],
                "RCA" => $data[9],
                "OTVETSTVENNYY" => array(),
                "VYVODY" => $data[11],
            );
            foreach($arOtvet as $otvetstvenney){
                $otvetstvenneyText = trim($otvetstvenney);
                if($otvetstvenneyText != '')
                    $arProp["OTVETSTVENNYY"][] = $arEmployers[$otvetstvenneyText];
            };
            $arFields = array(
                "ACTIVE" => "Y",
                "IBLOCK_ID" => GetIBlockIDByCode("incedent_list"),
                "NAME" => $data[4],
                "DETAIL_TEXT" => $data[4],
                "PROPERTY_VALUES" => $arProp,
            );
            if($arFields["NAME"]==''){
                $arFields["NAME"] = 'Без названия';
            }

            if($elemID = $el->add($arFields)){
                echo "$addCount. Добавлен новый элемент: $elemID <br />";
                $addCount ++;
            } else {
                echo "Ошибка добавления элемента:".$el->LAST_ERROR." ".pre(['$arFields' => $arFields])."<br />";
            }
        }
        $showCount++;
    }
    fclose($handle);
}
?>