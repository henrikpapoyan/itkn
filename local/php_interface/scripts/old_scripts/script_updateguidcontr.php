<?php
/*
 * Сравнивает текущий GUID и какой то новый ГУИД (логику его формирование не разобрал),
 * если они не совпадают, выполняет обновление пользовательского свойства “UF_CRM_COMPANY_GUID” для контрагента.
 * Информацию об обновленных значениях записывает в файл “newguidresult1.txt”
*/

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('CHK_EVENT', true);
ini_set('memory_limit', '-1');

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

@set_time_limit(0);
@ignore_user_abort(true);

global $USER;
$USER->Authorize(372);

CModule::IncludeModule('crm');
CModule::IncludeModule('iblock');

$oLead          = new CCrmLead;
$oContact       = new CCrmContact;
$oDeal          = new CCrmDeal;
$oCompany       = new CCrmCompany;
$CCrmEvent      = new CCrmEvent();
$oIBlockElement = new CIBlockElement();
$oUser          = new CUser;
    $requisite = new \Bitrix\Crm\EntityRequisite();
    $fieldsInfo = $requisite->getFormFieldsInfo();
    $select = array_keys($fieldsInfo);
		$entity_id = "CRM_COMPANY";
		$uf_guid = "UF_CRM_COMPANY_GUID";


$res = CCrmCompany::GetList(array(), array("CHECK_PERMISSIONS" => "N"));
while ($arRes = $res->Fetch()) 
{
  $companyID=$arRes['ID'];
   $arFilter = array(
        'ENTITY_ID' => $companyID,
		'ENTITY_TYPE_ID'    => 4
    );

    $result = $requisite->getList(
        array(
            'order' => array('SORT' => 'ASC', 'ID' => 'ASC'),
            'filter' => $arFilter,
            'select' => $select
        )
    );
	if ($row = $result->fetch()) 
	{
		$inncompany=$row['RQ_INN'];
		$kppcompany=$row['RQ_KPP'];
		$oldguidcompany=GetUserField($entity_id, $companyID, $uf_guid);
		
		$newguidcompany=SelectDateCreateFromBPMusingINNKPP($inncompany,$kppcompany);
		
		if((strtoupper($newguidcompany)!=strtoupper($oldguidcompany))&&(!empty($newguidcompany)))
		{
				SetUserField($entity_id, $companyID, $uf_guid,strtoupper($newguidcompany));		
				$filepathtxt="newguidresult1.txt";
			    file_put_contents($filepathtxt, "$companyID;$inncompany;$kppcompany;old;$oldguidcompany;new;$newguidcompany". "\r\n", FILE_APPEND | LOCK_EX);
		}
		

	}
//break;
}


function SelectDateCreateFromBPMusingINNKPP($inncompany,$kppcompany)
{
  $tiket = array (
            'RootSchemaName' => 'Account',
            'QueryType' => 0,
            'Columns' =>
                array (
                    'Items' =>
                        array (
                            'Id' =>
                                array (
                                    'Expression' =>
                                        array (
                                            'ColumnPath' => 'Id',
                                        ),
                                ),
								'CreatedOn' =>
                                array (
                                    'Expression' =>
                                        array (
                                            'ColumnPath' => 'CreatedOn',
                                        ),
                                ),

                        ),
                ),

            'Filters' =>
              array (
                    'FilterType' => 6,
                    'ComparisonType' => 0,
                    'Items' =>
                       array (
                            'FilterINN' =>
                                array (
                                    'FilterType' => 1,
                                    'ComparisonType' => 3,
                                    'LeftExpression' =>
                                        array (
                                            'ExpressionType' => 0,
                                            'ColumnPath' => 'Usraidiagent',
                                        ),
                                    'RightExpression' =>
                                        array (
                                            'ExpressionType' => 2,
                                            'Parameter' =>
                                                array (
                                                    'DataValueType' => 1,
                                                    'Value' => $inncompany,
                                                ),
                                        ),
                                ),
                           'KppFilter' =>
                               array (
                                   'FilterType' => 1,
                                   'ComparisonType' => 3,
                                   'LeftExpression' =>
                                       array (
                                           'ExpressionType' => 0,
                                           'ColumnPath' => 'UsrKPP',
                                       ),
                                   'RightExpression' =>
                                       array (
                                           'ExpressionType' => 2,
                                           'Parameter' =>
                                               array (
                                                   'DataValueType' => 1,
                                                   'Value' => $kppcompany,
                                               ),
                                       ),
                               ),
                        ),
                ),
        );

		$newguid="";$timeold="";
        $arrTeket = QueryBpm::jsonDataBpm($tiket, BPM_URL_SELECT);
        foreach($arrTeket['success']['rows'] as $arrelement)
		{
		 if(empty($timeold))
		 {
			 $timeold=$arrelement['CreatedOn'];$newguid=$arrelement['Id'];
		 }
		 else
		 {
			if(strtotime($timeold)>strtotime($arrelement['CreatedOn'])) {$timeold=$arrelement['CreatedOn'];$newguid=$arrelement['Id'];}
		 }
		
		}
		
		//pre($arrTeket);
		
		return $newguid;
}
