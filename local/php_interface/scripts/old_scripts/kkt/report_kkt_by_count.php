<?
/*
 * Формирует и записывает в файл /local/php_interface/scripts/kkt/reportKktByCount.csv след. информацию
 * 'Кол-во ККТ на контрагента', 'Всего контрагентов', 'Всего ККТ', 'Активированных ККТ'
 */

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
ini_set('memory_limit', '-1');

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");
global $USER;
global $DB;
$USER->Authorize(372);
global $DB;
$kktIBlockID = GetIBlockIDByCode("registry_kkt");
$strSql = "SELECT
                  ID, CODE
                FROM
                  b_iblock_property
                WHERE
                  (CODE = 'COMPANY' OR CODE = 'DATE_ACTIVE') AND IBLOCK_ID=$kktIBlockID
            ";
$START_TIME_ONE = mktime();
$rsProp = $DB->Query($strSql, false, $err_mess.__LINE__);
$TIME_ONE = mktime() - $START_TIME_ONE;
echo "\n\rЗапрос свойств ИБ ККТ , сек: $TIME_ONE \n";
while($arProp = $rsProp->Fetch()){
    if($arProp["CODE"]=="COMPANY"){
        $propCompanyID = $arProp["ID"];
    }
    if($arProp["CODE"]=="DATE_ACTIVE"){
        $propDateActiveID = $arProp["ID"];
    }
}
echo '$propCompanyID = '.$propCompanyID.' | $propDateActiveID = '.$propDateActiveID.'<br />';
if($propCompanyID > 0 && $propDateActiveID > 0){
    $strSql = "SELECT
                    b_iblock_element.ID,
                    b_iblock_element_prop_s$kktIBlockID.PROPERTY_$propCompanyID,
                    b_iblock_element_prop_s$kktIBlockID.PROPERTY_$propDateActiveID
                  FROM
                    b_iblock_element,
                    b_iblock_element_prop_s$kktIBlockID
                  WHERE
                    b_iblock_element.IBLOCK_ID=$kktIBlockID AND
                    b_iblock_element_prop_s$kktIBlockID.PROPERTY_$propCompanyID!='' AND
                    b_iblock_element_prop_s$kktIBlockID.IBLOCK_ELEMENT_ID=b_iblock_element.ID
                  ORDER BY b_iblock_element.DATE_CREATE ASC";
    $START_TIME_ONE = mktime();
    $rsKKT = $DB->Query($strSql, false, $err_mess.__LINE__);
    $TIME_ONE = mktime() - $START_TIME_ONE;
    echo "\n\rЗапрос ККТ SQL, сек: $TIME_ONE \n";
    $arKKTS = array();
    $selKKTCount = $rsKKT->SelectedRowsCount();
    echo '$rsKKT->SelectedRowsCount() = '.$rsKKT->SelectedRowsCount().'<br />';
    $KKT_SUMM = 0;
    $LeadsFromKKTCount = 0;
    $addedRNM_KKT = array();
    $START_TIME_ONE = mktime();
    $showCount = 0;
    $arCompanyKKTCount = array();
    $arResult = array(
        "BEFORE_2" => array(
            "NAME" => 'до 2',
            "CONTRAGENTS_COUNT" => 0,
            "ALL_KKT_COUNT" => 0,
            "ACTIVATE_KKT_COUNT" => 0
        ),
        "BEFORE_5" => array(
            "NAME" => 'от 3 до 5',
            "CONTRAGENTS_COUNT" => 0,
            "ALL_KKT_COUNT" => 0,
            "ACTIVATE_KKT_COUNT" => 0
        ),
        "BEFORE_10" => array(
            "NAME" => 'от 6 до 10',
            "CONTRAGENTS_COUNT" => 0,
            "ALL_KKT_COUNT" => 0,
            "ACTIVATE_KKT_COUNT" => 0
        ),
        "BEFORE_25" => array(
            "NAME" => 'от 11 до 25',
            "CONTRAGENTS_COUNT" => 0,
            "ALL_KKT_COUNT" => 0,
            "ACTIVATE_KKT_COUNT" => 0
        ),
        "BEFORE_50" => array(
            "NAME" => 'от 26 до 50',
            "CONTRAGENTS_COUNT" => 0,
            "ALL_KKT_COUNT" => 0,
            "ACTIVATE_KKT_COUNT" => 0
        ),
        "BEFORE_100" => array(
            "NAME" => 'от 51 до 100',
            "CONTRAGENTS_COUNT" => 0,
            "ALL_KKT_COUNT" => 0,
            "ACTIVATE_KKT_COUNT" => 0
        ),
        "MORE_100" => array(
            "NAME" => 'более 100',
            "CONTRAGENTS_COUNT" => 0,
            "ALL_KKT_COUNT" => 0,
            "ACTIVATE_KKT_COUNT" => 0
        ),
    );
    while($arKKT = $rsKKT->Fetch()){
        $companyID = intval($arKKT["PROPERTY_487"]);
        $arKKT["PROPERTY_487"] = $companyID;
        if(!isset($arCompanyKKTCount[$companyID])){
            $arCompanyKKTCount[$companyID] = array(
                "KKT_COUNT" => 0,
                "ACTIVE_KKT_COUNT" => 0
            );
        }
        $arCompanyKKTCount[$companyID]["KKT_COUNT"]++;
        if($arKKT["PROPERTY_480"] != ''){
            $arCompanyKKTCount[$companyID]["ACTIVE_KKT_COUNT"]++;
        }
        $showCount++;
    }
    foreach($arCompanyKKTCount as $companyID => $arCompanyInfo){
        if($arCompanyInfo["KKT_COUNT"] <= 2){
            $arResult["BEFORE_2"]["CONTRAGENTS_COUNT"]++;
            $arResult["BEFORE_2"]["ALL_KKT_COUNT"]+=$arCompanyInfo["KKT_COUNT"];
            $arResult["BEFORE_2"]["ACTIVATE_KKT_COUNT"]+=$arCompanyInfo["ACTIVE_KKT_COUNT"];
        }
        if($arCompanyInfo["KKT_COUNT"] > 2 && $arCompanyInfo["KKT_COUNT"] <= 5){
            $arResult["BEFORE_5"]["CONTRAGENTS_COUNT"]++;
            $arResult["BEFORE_5"]["ALL_KKT_COUNT"]+=$arCompanyInfo["KKT_COUNT"];
            $arResult["BEFORE_5"]["ACTIVATE_KKT_COUNT"]+=$arCompanyInfo["ACTIVE_KKT_COUNT"];
        }
        if($arCompanyInfo["KKT_COUNT"] > 5 && $arCompanyInfo["KKT_COUNT"] <= 10){
            $arResult["BEFORE_10"]["CONTRAGENTS_COUNT"]++;
            $arResult["BEFORE_10"]["ALL_KKT_COUNT"]+=$arCompanyInfo["KKT_COUNT"];
            $arResult["BEFORE_10"]["ACTIVATE_KKT_COUNT"]+=$arCompanyInfo["ACTIVE_KKT_COUNT"];
        }
        if($arCompanyInfo["KKT_COUNT"] > 10 && $arCompanyInfo["KKT_COUNT"] <= 25){
            $arResult["BEFORE_25"]["CONTRAGENTS_COUNT"]++;
            $arResult["BEFORE_25"]["ALL_KKT_COUNT"]+=$arCompanyInfo["KKT_COUNT"];
            $arResult["BEFORE_25"]["ACTIVATE_KKT_COUNT"]+=$arCompanyInfo["ACTIVE_KKT_COUNT"];
        }
        if($arCompanyInfo["KKT_COUNT"] > 25 && $arCompanyInfo["KKT_COUNT"] <= 50){
            $arResult["BEFORE_50"]["CONTRAGENTS_COUNT"]++;
            $arResult["BEFORE_50"]["ALL_KKT_COUNT"]+=$arCompanyInfo["KKT_COUNT"];
            $arResult["BEFORE_50"]["ACTIVATE_KKT_COUNT"]+=$arCompanyInfo["ACTIVE_KKT_COUNT"];
        }
        if($arCompanyInfo["KKT_COUNT"] > 50 && $arCompanyInfo["KKT_COUNT"] <= 100){
            $arResult["BEFORE_100"]["CONTRAGENTS_COUNT"]++;
            $arResult["BEFORE_100"]["ALL_KKT_COUNT"]+=$arCompanyInfo["KKT_COUNT"];
            $arResult["BEFORE_100"]["ACTIVATE_KKT_COUNT"]+=$arCompanyInfo["ACTIVE_KKT_COUNT"];
        }
        if($arCompanyInfo["KKT_COUNT"] > 100){
            $arResult["MORE_100"]["CONTRAGENTS_COUNT"]++;
            $arResult["MORE_100"]["ALL_KKT_COUNT"]+=$arCompanyInfo["KKT_COUNT"];
            $arResult["MORE_100"]["ACTIVATE_KKT_COUNT"]+=$arCompanyInfo["ACTIVE_KKT_COUNT"];
        }
    }
    //echo "arResult = <pre>".print_r($arResult, true)."</pre>\n\r";
    $fp = fopen($DOCUMENT_ROOT.'/local/php_interface/scripts/kkt/reportKktByCount.csv', 'w');
    fputcsv($fp, array('Кол-во ККТ на контрагента', 'Всего контрагентов', 'Всего ККТ', 'Активированных ККТ'), ';', '"');
    foreach($arResult as $arLine){
        fputcsv($fp, $arLine, ';', '"');
    }
    fclose($fp);
    $TIME_ONE = mktime() - $START_TIME_ONE;
    echo "\n\rОБработка $selKKTCount ККТ , сек: $TIME_ONE \n";
}
?>