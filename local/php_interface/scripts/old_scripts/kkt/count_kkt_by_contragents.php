<?
/*
 * Обновляет пользовательские поля контрагента “UF_KKT2” и “UF_KKT”,
 * данные по ККТ берутся из ИБ с симв. кодом "registry_kkt".
 */

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
ini_set('memory_limit', '-1');

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");
global $USER;
global $DB;
$USER->Authorize(372);
global $DB;
$kktIBlockID = GetIBlockIDByCode("registry_kkt");
$strSql = "SELECT
                  ID, CODE
                FROM
                  b_iblock_property
                WHERE
                  (CODE = 'COMPANY' OR CODE = 'DATE_OFF') AND IBLOCK_ID=$kktIBlockID
            ";
$START_TIME_ONE = mktime();
$rsProp = $DB->Query($strSql, false, $err_mess.__LINE__);
$TIME_ONE = mktime() - $START_TIME_ONE;
echo "\n\rЗапрос свойств ИБ ККТ , сек: $TIME_ONE \n";
while($arProp = $rsProp->Fetch()){
    if($arProp["CODE"]=="COMPANY"){
        $propCompanyID = $arProp["ID"];
    }
    if($arProp["CODE"]=="DATE_OFF"){
        $propDateOffID = $arProp["ID"];
    }
}
//echo '$propCompanyID = '.$propCompanyID.' | $propDateActiveID = '.$propDateOffID.'<br />';
if($propCompanyID > 0 && $propDateOffID > 0){
    $strSql = "SELECT
                    b_iblock_element.ID,
                    b_iblock_element_prop_s$kktIBlockID.PROPERTY_$propCompanyID,
                    b_iblock_element_prop_s$kktIBlockID.PROPERTY_$propDateOffID
                  FROM
                    b_iblock_element,
                    b_iblock_element_prop_s$kktIBlockID
                  WHERE
                    b_iblock_element.IBLOCK_ID=$kktIBlockID AND
                    b_iblock_element_prop_s$kktIBlockID.PROPERTY_$propCompanyID!='' AND
                    b_iblock_element_prop_s$kktIBlockID.IBLOCK_ELEMENT_ID=b_iblock_element.ID
                  ORDER BY b_iblock_element_prop_s$kktIBlockID.PROPERTY_$propDateOffID ASC";
    $START_TIME_ONE = mktime();
    $rsKKT = $DB->Query($strSql, false, $err_mess.__LINE__);
    $TIME_ONE = mktime() - $START_TIME_ONE;
    echo "\n\rЗапрос ККТ SQL, сек: $TIME_ONE \n";
    $arKKTS = array();
    $selKKTCount = $rsKKT->SelectedRowsCount();
    echo "rsKKT->SelectedRowsCount() = ".$rsKKT->SelectedRowsCount()."\n\r";
    $KKT_SUMM = 0;
    $LeadsFromKKTCount = 0;
    $addedRNM_KKT = array();
    $START_TIME_ONE = mktime();
    $showCount = 0;
    $arCompanyKKTCount = array();
    while($arKKT = $rsKKT->Fetch()){
        $companyID = intval($arKKT["PROPERTY_487"]);
        $arKKT["PROPERTY_487"] = $companyID;
        if(!isset($arCompanyKKTCount[$companyID])){
            $arCompanyKKTCount[$companyID] = 0;
        }
        $arCompanyKKTCount[$companyID]++;
        /*if($showCount <= 10){
            echo "arKKT = <pre>".print_r($arKKT, true)."</pre>\n\r";
        }*/
        $showCount++;
    }
    $arKktPeriods = array();
    $CCrmCompany = new CCrmCompany();
    global $USER_FIELD_MANAGER;
    $CUserFieldEnum = new CUserFieldEnum();
    $resUserField = $CUserFieldEnum->GetList(array(), array("USER_FIELD_NAME"=>"UF_KKT2"));
    while($arUserField = $resUserField->GetNext()){
        //echo "arKktPeriods = <pre>".print_r($arKktPeriods, true)."</pre>\n\r";
        if($arUserField['~VALUE']=="0>10"){
            $arKktPeriods["BEFORE_10"] = $arUserField['ID'];
        }
        if($arUserField['~VALUE']=="10>50"){
            $arKktPeriods["BEFORE_50"] = $arUserField['ID'];
        }
        if($arUserField['~VALUE']=="50>100"){
            $arKktPeriods["BEFORE_100"] = $arUserField['ID'];
        }
        if($arUserField['~VALUE']=="100>300"){
            $arKktPeriods["BEFORE_300"] = $arUserField['ID'];
        }
        if($arUserField['~VALUE']=="300>500"){
            $arKktPeriods["BEFORE_500"] = $arUserField['ID'];
        }
        if($arUserField['~VALUE']=="500>1000"){
            $arKktPeriods["BEFORE_1000"] = $arUserField['ID'];
        }
        if($arUserField['~VALUE']==">1000"){
            $arKktPeriods["MORE_1000"] = $arUserField['ID'];
        }
    }
    //echo "arKktPeriods = <pre>".print_r($arKktPeriods, true)."</pre>\n\r";
    $companyCount = 0;
    foreach($arCompanyKKTCount as $companyID => $kktCount){
        $periodType = $arKktPeriods["BEFORE_10"];
        if($kktCount > 10 && $kktCount<=50){
            $periodType = $arKktPeriods["BEFORE_50"];
        }
        if($kktCount > 50 && $kktCount<=100){
            $periodType = $arKktPeriods["BEFORE_100"];
        }
        if($kktCount > 100 && $kktCount<=300){
            $periodType = $arKktPeriods["BEFORE_300"];
        }
        if($kktCount > 300 && $kktCount<=500){
            $periodType = $arKktPeriods["BEFORE_500"];
        }
        if($kktCount > 500 && $kktCount<=1000){
            $periodType = $arKktPeriods["BEFORE_1000"];
        }
        if($kktCount>1000){
            $periodType = $arKktPeriods["MORE_1000"];
        }
        //echo "Для компании с ID = $companyID найдено ККТ = $kktCount, обновлено значение пользовательского поля UF_KKT2 на $periodType\n\r";
        $USER_FIELD_MANAGER->Update(
            'CRM_COMPANY',
            $companyID,
            array(
                'UF_KKT2'  => $periodType,
                'UF_KKT'  => $kktCount,
            )
        );
        $companyCount++;
    }

    $TIME_ONE = mktime() - $START_TIME_ONE;
    echo "\n\rОБработка $selKKTCount ККТ , сек: $TIME_ONE \n";
}
?>