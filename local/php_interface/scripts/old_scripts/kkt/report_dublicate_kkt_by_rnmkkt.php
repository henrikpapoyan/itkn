<?
/*
 *  Ищет дубликаты РНМ ККТ информацию по найденным дублям записывает в reportDublicateKKTByRNM_KKT.csv.
 */

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
ini_set('memory_limit', '-1');

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");
global $USER;
global $DB;
$USER->Authorize(372);
global $DB;
$kktIBlockID = GetIBlockIDByCode("registry_kkt");
$strSql = "SELECT
                  ID, CODE
                FROM
                  b_iblock_property
                WHERE
                  (CODE = 'RNM_KKT') AND IBLOCK_ID=$kktIBlockID
            ";
$START_TIME_ONE = mktime();
$rsProp = $DB->Query($strSql, false, $err_mess.__LINE__);
$TIME_ONE = mktime() - $START_TIME_ONE;
echo "\n\rЗапрос свойств ИБ ККТ , сек: $TIME_ONE \n";
while($arProp = $rsProp->Fetch()){
    if($arProp["CODE"]=="RNM_KKT"){
        $propRnmKktID = $arProp["ID"];
    }
}
echo '$propRnmKktID = '.$propRnmKktID.'<br />';
if($propRnmKktID > 0){
    $strSql = "SELECT
                    b_iblock_element.ID,
                    b_iblock_element_prop_s$kktIBlockID.PROPERTY_$propRnmKktID
                  FROM
                    b_iblock_element,
                    b_iblock_element_prop_s$kktIBlockID
                  WHERE
                    b_iblock_element.IBLOCK_ID=$kktIBlockID AND
                    b_iblock_element_prop_s$kktIBlockID.PROPERTY_$propRnmKktID!='' AND
                    b_iblock_element_prop_s$kktIBlockID.IBLOCK_ELEMENT_ID=b_iblock_element.ID
                  ORDER BY b_iblock_element.ID DESC";
    $START_TIME_ONE = mktime();
    $rsKKT = $DB->Query($strSql, false, $err_mess.__LINE__);
    $TIME_ONE = mktime() - $START_TIME_ONE;
    echo "\n\rЗапрос ККТ SQL, сек: $TIME_ONE \n";
    $arKKTS = array();
    $selKKTCount = $rsKKT->SelectedRowsCount();
    echo '$rsKKT->SelectedRowsCount() = '.$selKKTCount.'<br />';
    $START_TIME_ONE = mktime();
    $showCount = 0;
    $arRnmKKT = array();
    while($arKKT = $rsKKT->Fetch()){
        $rnmKKT = intval($arKKT["PROPERTY_$propRnmKktID"]);
        $arRnmKKT[$rnmKKT][] = $arKKT["ID"];
        $showCount++;
    }
    $fp = fopen('reportDublicateKKTByRNM_KKT.csv', 'w');
    fputcsv($fp, array('РНМ ККТ по которому найдены дубли', 'ID ККТ'), ';', '"');
    $dublicateCount = 0;
    foreach($arRnmKKT as $rnmKKT => $arKktID){
        if(sizeof($arKktID)>1){
            fputcsv($fp, array($rnmKKT, implode(',', $arKktID)), ';', '"');
            $dublicateCount++;
        }
    }
    fclose($fp);
    $TIME_ONE = mktime() - $START_TIME_ONE;
    echo "\n\rОБработка $selKKTCount ККТ. Найдено дубликатов: $dublicateCount, сек: $TIME_ONE \n";
}
?>