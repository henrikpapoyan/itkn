<?
    require_once("handlers/handlers_crm.php");
    require_once("handlers/handlers_tasks.php");
    require_once("handlers/handlers_sale.php");
    require_once("handlers/handlers_iblock.php");
    require_once("handlers/handlers_main.php");
    require_once("handlers/handlers_forum.php");
    require_once("handlers/handler_socialnetwork.php");
    require_once("handlers/handler_common.php");
    require_once("handlers/handler_catalog.php");

    //Events
    AddEventHandler("tasks", "OnTaskAdd", 'OnTaskAddHandler');
    AddEventHandler("tasks", "OnBeforeTaskAdd", "OnBeforeTaskAddHandler");
    AddEventHandler("tasks", "OnBeforeTaskUpdate", "OnBeforeTaskUpdateHandler");

    AddEventHandler('main', 'OnBeforeEventSend', Array("MyForm", "my_OnBeforeEventSend"));
    AddEventHandler('main', 'OnProlog', "OnPrologHandler");

    AddEventHandler("crm", "OnAfterCrmInvoiceUpdate", "OnAfterOrderUpdateHandlerGK");

    AddEventHandler("crm", "OnAfterCrmDealAdd", 'OnAfterCrmDealAddHandler');
    AddEventHandler("crm", "OnAfterCrmCompanyAdd", array("ProcessingCompany", "onAfterCompanyAdd"));
    AddEventHandler("crm", "OnAfterCrmCompanyUpdate", array("ProcessingCompany", "onAfterCompanyAdd"));
    AddEventHandler("crm", "OnBeforeCrmCompanyUpdate", array("ProcessingCompany", "OnBeforeCrmCompanyUpdate"));
    //AddEventHandler("crm", "OnAfterCrmCompanyUpdate", array("ProcessingCompany", "OnAfterCrmCompanyUpdate"));
    AddEventHandler("crm", "OnAfterCrmLeadUpdate", array("ProcessingLead", "ChangeStateLead"));
    AddEventHandler("crm", "OnAfterCrmLeadUpdate", "updateObracheniye");
    AddEventHandler("crm", "OnAfterCrmInvoiceAdd", array("ProcessingInvoice", "onAfterCrmInvoiceAddUpdate"));
    AddEventHandler("crm", "OnAfterCrmInvoiceUpdate", array("ProcessingInvoice", "onAfterCrmInvoiceAddUpdate"));
    //AddEventHandler("crm", "OnAfterCrmInvoiceAdd", "updateProblemeInvoice", 50);
    //AddEventHandler("crm", "OnBeforeCrmInvoiceUpdate", "updateProblemeInvoice", 50);
    AddEventHandler("crm", "OnAfterCrmInvoiceAdd", "updateAccountNumber", 200);
    //AddEventHandler("crm", "OnAfterCrmInvoiceUpdate", "updateAccountNumber", 200);
    AddEventHandler('crm', 'OnAfterCrmLeadAdd', "OnAfterCrmLeadAddHandler");
    //AddEventHandler('crm', 'OnAfterCrmInvoiceUpdate', "OnAfterCrmInvoiceUpdateHandlerLog");
    //AddEventHandler('crm', 'OnAfterCrmInvoiceAdd', "OnAfterCrmInvoiceUpdateHandlerLog");
    //AddEventHandler('crm', 'OnAfterCrmInvoiceSetStatus', "OnAfterCrmInvoiceSetStatusHandlerLog");
    //AddEventHandler('crm', 'OnBeforeCrmInvoiceDelete', "OnBeforeCrmInvoiceDeleteHandlerLog");
    //События из файла local/php_interface/crm/events/handler.php
    AddEventHandler("crm", "OnAfterCrmDealAdd", array('DealNew', 'AddPropertyIBlockDeal'));
    AddEventHandler("crm", "OnAfterCrmDealUpdate", array('DealNew', 'AddPropertyIBlockDeal'));

    //События связанные с добавлением/изменением/удаление в ИБ "Замена сотрудников при уходе в отпуск"
    //Удаление внесенных изменений в БП при удаление элемента
    AddEventHandler("iblock", "OnBeforeIBlockElementDelete", Array("vacationReplacement", "OnBeforeIBlockElementDeleteHandler"));
    AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", Array("vacationReplacement", "OnBeforeIBlockElementUpdateHandler"));
    AddEventHandler("iblock", "OnBeforeIBlockElementAdd", Array("vacationReplacement", "OnBeforeIBlockElementAddHandler"));

    //Добавление взаимосвязей для контрагента при добавление/изменение/удаления ККТ.
    AddEventHandler("iblock", "OnAfterIBlockElementAdd", Array("kktHandler", "OnAfterIBlockElementAddHandler"));
    AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", Array("kktHandler", "OnBeforeIBlockElementUpdateHandler"));
    AddEventHandler("iblock", "OnBeforeIBlockElementDelete", Array("kktHandler", "OnBeforeIBlockElementDeleteHandler"));
    //Добавление свойств в карточку контрагента.
    AddEventHandler("iblock", "OnAfterIBlockElementAdd", "updateCompanyFields");
    //События из файла local/php_interface/crm/events/handler.php
    AddEventHandler("iblock", "OnAfterIBlockElementAdd", array('DealNew', 'dealAddPropertyIBlock'));
    AddEventHandler("iblock", "OnAfterIBlockElementUpdate", array('DealNew', 'dealAddPropertyIBlock'));

    //Отслеживаем добавление и изменение элемента БП "Запрос ФНС", чтобы по ИНН заполнить поле "Наименование налогоплательщика"
    AddEventHandler("iblock", "OnAfterIBlockElementUpdate", Array("bpRequestFns", "SendFNSNotice"));
    AddEventHandler("iblock", "OnAfterIBlockElementAdd", Array("bpRequestFns", "SendFNSNotice"));
    AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", Array("bpRequestFns", "SetParamNameTaxpayer"));
    AddEventHandler("iblock", "OnBeforeIBlockElementAdd", Array("bpRequestFns", "SetParamNameTaxpayer"));

    //Добавление свойств в карточку контрагента.
    AddEventHandler("iblock", "OnAfterIBlockElementAdd", "changeAuthorityVacation");
    AddEventHandler("iblock", "OnAfterIBlockElementUpdate", "changeAuthorityVacation");

    //Отлавливаю события добавления комментария
    AddEventHandler('forum', 'onAfterMessageAdd', "MyonAfterMessageAddHandler");

    AddEventHandler("socialnetwork", "OnFillSocNetMenu", "OnFillSocNetMenuHandlerGK", 200);
    
    //Проверяем установлени ли галочка "НДС включен в цену" и если нет значения для "Ставка НДС" устанавливаем значение из свойств,
    //А также конвертируем код единицы измерения из соответствующего свойства
    AddEventHandler("catalog", "OnBeforeProductAdd", Array("CatalogHandler", "setMeasureNdsForProduct"), 550);
    AddEventHandler("catalog", "OnBeforeProductUpdate", Array("CatalogHandler", "setMeasureNdsForProductUpdate"), 550);

