<?php
    // for AddMessage2Log
    define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"] . "/log/bx_log.log");
    
    //Iblock Ids
    define('GOODS_IBLOCK_ID', 30);
    
    //Property codenames
    define('MEASURE_PROPERTY_CODE', 'CML2_BASE_UNIT');
    define('VAT_PROPERTY_CODE', 'CML2_TAXES');
    
    //Other
    define('DEFAULT_MEASURE_ID', 5); //Шт.

    //NDS vatt values
    define('NDS_VAT_0', 1);
    define('NDS_VAT_10', 4);
    define('NDS_VAT_18', 2);
    define('NDS_VAT_20', 3);