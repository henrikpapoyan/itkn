<?php
/**
 * Description of UniSenderIntegration
 *  Класс интеграции с UniSender
 * @author Klyopov Roman
 */

class UniSenderIntegration
{
	// переменные класса
	private $apiKey = "6xky3ftud3q33diujrxm3a8hx8dwsdi6d6xcub4e";
	private $apiUrl = "https://api.unisender.com/ru/api/";
	private $apiMethods = array(
		"addToList" => "subscribe",
		"sendTest"  => "sendTestEmail",
		"send"      => "sendEmail",
	);

	// запрос и возврат результата
	public function action($method, $params = "")
	{
		$fullPathRequest = "";
		switch($method)
		{
			case "addToList":
				$fullPathRequest = $this->apiUrl . $this->apiMethods[$method] . "?format=json&api_key=" . $this->apiKey . "&tags=List1&double_optin=3&overwrite=0" . $params;
				break;
			default:
				break;
		}
		try {
			$result = "";

			if($fullPathRequest)
			{
				$result = file_get_contents($fullPathRequest);
			}
			return $result;
		}
		catch(Exception $e){}
	}
}