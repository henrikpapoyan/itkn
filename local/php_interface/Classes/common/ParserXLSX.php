<?
use Bitrix\Main\Application;

class ParserXLSX
{
    const ID_GROUP = 37; //ID группы "Менеджеры отдела сопровождения" 39

    public static function cronFileExis($path = null)
    {
        if (empty($path)) {
            $path = Application::getDocumentRoot() . "/close_docs/";
        }
        if ($handle = opendir($path)) {
            while (false !== ($entry = readdir($handle))) {
                if ($entry != "." && $entry != "..") {
                    $fileName[] = $entry;
                    echo "Файл есть: $entry <br>";
                }
            }
            closedir($handle);
        }

        if (\Bitrix\Main\Loader::includeModule('disk'))
        {
            $driver = \Bitrix\Disk\Driver::getInstance();
            $storage = $driver->getStorageByGroupId(self::ID_GROUP);
            if ($storage) {
                //корневая директория для сохранения
                $folder = $storage->getRootObject($storage['ROOT_OBJECT_ID']);
                foreach ($fileName as $key=>$name) {
                    $fileArray = \CFile::MakeFileArray($path . $name);
                    $file = $folder->uploadFile($fileArray, array(
                        'CREATED_BY' => 372
                    ));

                    unlink($path.$name);
                }
            }
        }
    }

    //Парсинг Excel файла
    public static function parseFile($inputFile){
        $time = time();
        $dir = \Bitrix\Main\Application::getDocumentRoot() . OFD_BITRIX_TMP . "parseExel/" . $time . "/";

        // Unzip
        $zip = new ZipArchive();
        $zip->open($inputFile);
        $zip->extractTo($dir);

        // Open up shared strings & the first worksheet
        if(file_exists($dir . '/xl/SharedStrings.xml')){
            $strings = simplexml_load_file($dir . '/xl/SharedStrings.xml');
        } else {
            $strings = simplexml_load_file($dir . '/xl/sharedStrings.xml');
        }
        $sheet   = simplexml_load_file($dir . '/xl/worksheets/sheet1.xml');

        // Parse the rows
        $xlrows = $sheet->sheetData->row;
        $row = 0;
        $out = array();
        foreach ($xlrows as $xlrow) {
            $col = 0;
            // In each row, grab it's value
            foreach ($xlrow->c as $kk => $cell) {
                $v = (string) $cell->v;

                // If it has a "t" (type?) of "s" (string?), use the value to look up string value
                if (isset($cell['t']) && $cell['t'] == 's') {
                    $s  = array();
                    $si = $strings->si[(int) $v];

                    // Register & alias the default namespace or you'll get empty results in the xpath query
                    $si->registerXPathNamespace('n', 'http://schemas.openxmlformats.org/spreadsheetml/2006/main');

                    // Cat together all of the 't' (text?) node values
                    foreach($si->xpath('.//n:t') as $t) {
                        if(!empty($t)){
                            $s[] = (string) $t;
                        }

                    }

                    $v = implode($s);
                }
                if(empty($v) or  $v == "--"){
                    continue;
                }
                if(empty($out[$row][$col]) or  $out[$row][$col] == "--"){
                    unset($out[$row][$col]);
                }

                $out[$row][$col] = $v;

                $newOut = array_values($out);

                if($v == "###################################################################################"){
                    $break = 1;

                }

                ++$col;
            }
//Массив.
            if($break == 1){
                $newOut[1][0] = "УПД";$elementnumber=0;
                foreach ($newOut as $k=>$val) {
                    if($k == 1){
                        $newAut["schet_factura_number_date"] = $val[1]." ".$val[2]." ".$val[3]." ".$val[4];
                    }
                    if (strpos($val[0], 'Универсальный передаточный документ') !== false)
                    {
                        $docNumDate =  explode(" ", $val[0]);
                        $newAut["doc_number"] = $docNumDate[4];
                        $newAut["doc_data"] = $docNumDate[6]." ".$docNumDate[7]." ".$docNumDate[8]." ".$docNumDate[9];
                    }

                    if (strpos($val[1], 'БП-00001077') !== false)
                    {
                        $ktt[] = $val[2];
                        $collich[] = $val[5];
                        $newAut["usluga_kkt"] = $val[2];
                        $per = explode(" ", $ktt[0]);
                        krsort($per);
                        $newPer = array_values($per);
                        $newAut['otchet_period_s'] = $newPer[2];
                        $newAut['otchet_period_po'] = $newPer[0];

                        $newAut['USLUGA'][$elementnumber]["name"] = 'XML_FNS';
                        $newAut['USLUGA'][$elementnumber]["count"] = $val[5];
                        $newAut['USLUGA'][$elementnumber]["tarif"] = round($val[6], 2);
                        $newAut['USLUGA'][$elementnumber]["price_not_nds"] = round($val[7], 2);
                        $newAut['USLUGA'][$elementnumber]["price_nds"] = round($val[11], 2);
                        $elementnumber++;

                    }
                    if (strpos($val[1], 'БП-00001745') !== false)
                    {
                        $sms[] = $val[2];
                        $collich_sms[] = $val[5];
                        $newAut["usluga_sms"] = $val[2];
                        $persms = explode(" ", $sms[0]);
                        krsort($persms);
                        $newPersms = array_values($persms);
                        $newAut['otchet_period_s'] = $newPersms[2];
                        $newAut['otchet_period_po'] = $newPersms[0];

                        $newAut['USLUGA'][$elementnumber]["name"] = 'XML_SMS';
                        $newAut['USLUGA'][$elementnumber]["count"] = $val[5];
                        $newAut['USLUGA'][$elementnumber]["tarif"] = round($val[6], 2);
                        $newAut['USLUGA'][$elementnumber]["price_not_nds"] = round($val[7], 2);
                        $newAut['USLUGA'][$elementnumber]["price_nds"] = round($val[11], 2);
                        $elementnumber++;

                    }
                    if (strpos($val[0], 'ИНН/КПП покупателя:') !== false)
                    {
                        $innKpp =  explode("/", $val[1]);
                        $newAut['inn'] = $innKpp[0];
                        $newAut['kpp'] = $innKpp[1];
                    }
                    if (strpos($val[0], 'Договор №') !== false)
                    {
                        $newAut['number_date_dogovor'] = $val[0];
                    }
                    if(strpos($val[0], 'Всего к оплате') !== false){
                        $newAut["stoimost"] = round($val[1], 2);
                        $newAut["stoimost_nds"] = round($val[4], 2);
                    }else{
                        $newOut[$k] = $val;
                    }

                }
                //кол-во ккт
                $col_ktt = array_sum($collich);
                $newAut['col_ktt'] = $col_ktt;
                unset($collich);
                unset($col_ktt);
                unset($ktt);
                //кол-во sms
                $col_sms = array_sum($collich_sms);
                $newAut['col_sms'] = $col_sms;
                unset($collich_sms);
                unset($col_sms);
                unset($sms);

                $array[] = $newAut;
                unset($row);
                unset($break);
                unset($out);
                unset($newOut);
                unset($newAut);
            }

            $row++;

        }
        $arrayOut = array_diff($array, array(''));

        $zip->close();
        rmdir($dir);

        if(is_array($arrayOut)){
            return $arrayOut;
        }else{
            return $inputFile;
        }
    }

    //Функция для обработчика события onAfterAddFile
    public static function addFile(\Bitrix\Main\Event $event){
        list($file) = $event->getParameters();

        if($file instanceof \Bitrix\Disk\File)
        {
            $storageID = $file->getStorageId();
        }

        if (\Bitrix\Main\Loader::includeModule('disk'))
        {
            if ($storageID == 622) { //610

                if($file)
                {
                    $nameFile = $file->getName();
                    $fid = $file->getFileId();
                    $pathFile = CFile::GetPath($fid);
                    $url =  Application::getDocumentRoot() . $pathFile;
                    $result = self::parseFile($url);

                    $logger = Logger::getLogger('ParserXLSX','ofd.bitrix24/ParserXLSX.txt');
                    $logger->log(array($result));

                    foreach ($result as $updelement) {
                        $req = new \Bitrix\Crm\EntityRequisite();
                        $rser = $req->getList(array(
                            "filter" => array(
                                'RQ_INN' => $updelement['inn'],
                                'RQ_KPP' => $updelement['kpp'],
                                "ENTITY_TYPE_ID" => CCrmOwnerType::Company,
                                'PRESET_ID' => array(1, 2)
                            )
                        ));
                        $rows = $rser->fetchAll();
                        foreach ($rows as $company) {
                            $id_company = $company["ENTITY_ID"];
                        }

                        if (!empty($id_company)) {

                            global $DB;
                            if($DB->IsDate($updelement['otchet_period_s'], "DD.MM.YYYY")===false)
                            {
                                $updelement['otchet_period_s']=$DB->FormatDate($updelement['otchet_period_s'],"YYYY-MM-DD", "DD.MM.YYYY" );
                            }
                            if($DB->IsDate($updelement['otchet_period_po'], "DD.MM.YYYY")===false)
                            {
                                $updelement['otchet_period_po']=$DB->FormatDate($updelement['otchet_period_po'],"YYYY-MM-DD", "DD.MM.YYYY");
                            }


                            $perem_COUNT_KKT = 0;
                            $perem_PRICE = 0;
                            $perem_PRICE_NDS = 0;
                            $perem_PRICE_NOT_NDS = 0;

                            $arValue = array();
                            foreach ($updelement['USLUGA'] as $indexelement => $companyuslugaelement) {
                                $indexname = "n" + $indexelement;
                                $arValue[$indexname] = array(
                                    "NAME_USLUGY_ONE" => $companyuslugaelement['name'],
                                    "COUNT" => $companyuslugaelement['count'],
                                    "PRICE_ONE" => $companyuslugaelement['tarif'],
                                    "PRICE_NDS_ONE" => $companyuslugaelement['price_nds'],
                                    "PRICE_NOT_NDS_ONE" => $companyuslugaelement['price_not_nds']);

                                $perem_COUNT_KKT += $companyuslugaelement['count'];
                                $perem_PRICE += $companyuslugaelement['tarif'];
                                $perem_PRICE_NDS += $companyuslugaelement['price_nds'];
                                $perem_PRICE_NOT_NDS += $companyuslugaelement['price_not_nds'];
                            }

                            //Добавление множественного свойства типа таблица
                           /* if(!empty($updelement['usluga_kkt']) and !empty($updelement['usluga_sms'])){
                                $arValue = array(
                                    "n0" => array(
                                        "NAME_USLUGY_ONE" => 'XML_FNS',
                                        "COUNT" => $updelement['col_ktt'],
                                        "PRICE_ONE" => $updelement['tarif_kkt'],
                                        "PRICE_NDS_ONE" => $updelement['price_nds_kkt'],
                                        "PRICE_NOT_NDS_ONE" => $updelement['price_not_nds_kkt']
                                    ),
                                    "n1" => array(
                                        "NAME_USLUGY_ONE" => 'XML_SMS',
                                        "COUNT" => $updelement['col_sms'],
                                        "PRICE_ONE" => $updelement['tarif_sms'],
                                        "PRICE_NDS_ONE" => $updelement['price_nds_sms'],
                                        "PRICE_NOT_NDS_ONE" => $updelement['price_not_nds_sms']
                                    )
                                );
                                $perem_COUNT_KKT+=$updelement['col_ktt'];
                                $perem_COUNT_KKT+=$updelement['col_sms'];
                                $perem_PRICE=$updelement['tarif_kkt']+$updelement['tarif_sms'];
                                $perem_PRICE_NDS=$updelement['price_nds_kkt']+$updelement['price_nds_sms'];
                                $perem_PRICE_NOT_NDS=$updelement['price_not_nds_kkt']+$updelement['price_not_nds_sms'];

                            }elseif ($updelement['usluga_kkt'] and empty($updelement['usluga_sms'])){
                                $arValue = array(
                                    "n0" => array(
                                        "NAME_USLUGY_ONE" => 'XML_FNS',
                                        "COUNT" => $updelement['col_ktt'],
                                        "PRICE_ONE" => $updelement['tarif_kkt'],
                                        "PRICE_NDS_ONE" => $updelement['price_nds_kkt'],
                                        "PRICE_NOT_NDS_ONE" => $updelement['price_not_nds_kkt']
                                    )
                                );

                                $perem_COUNT_KKT+=$updelement['col_ktt'];
                                $perem_PRICE+=$updelement['tarif_kkt'];
                                $perem_PRICE_NDS+=$updelement['price_nds_kkt'];
                                $perem_PRICE_NOT_NDS+=$updelement['price_not_nds_kkt'];

                            }elseif ($updelement['usluga_sms'] and empty($updelement['usluga_kkt'])){
                                if($updelement['usluga_sms']){
                                    $arValue = array(
                                        "n0" => array(
                                            "NAME_USLUGY_ONE" => 'XML_SMS',
                                            "COUNT" => $updelement['col_sms'],
                                            "PRICE_ONE" => $updelement['tarif_sms'],
                                            "PRICE_NDS_ONE" => $updelement['price_nds_sms'],
                                            "PRICE_NOT_NDS_ONE" => $updelement['price_not_nds_sms']
                                        )
                                    );
                                    $perem_COUNT_KKT+=$updelement['col_sms'];
                                    $perem_PRICE+=$updelement['tarif_sms'];
                                    $perem_PRICE_NDS+=$updelement['price_nds_sms'];
                                    $perem_PRICE_NOT_NDS+=$updelement['price_not_nds_sms'];
                                }
                            }*/


                            $IBLOCK_ID_UPD = getIblockIDByCode('CLOSING_DOCUMENTS_UPD');
                            $arFields = array(
                                "ACTIVE" => "Y",
                                "IBLOCK_ID" => $IBLOCK_ID_UPD,
                                "NAME" => $updelement['schet_factura_number_date'],
                                "PROPERTY_VALUES" => array(
                                    "TYP_DOCS" => 1401,
                                    "NUM_DATE_DOGOVOR" => $updelement['number_date_dogovor'],
                                    "START_REPORTING_PERIOD" => $updelement['otchet_period_s'],
                                    "END_REPORTING_PERIOD" => $updelement['otchet_period_po'],

                                   // "COUNT_KKT" => $perem_COUNT_KKT,
                                    "PRICE" => round($perem_PRICE, 2),
                                    "PRICE_NDS" => round($perem_PRICE_NDS, 2),
                                    "PRICE_NOT_NDS" => round($perem_PRICE_NOT_NDS, 2),

                                    "NUM_DAT_SCHETFACTUR" => $updelement['schet_factura_number_date'],
                                    "RESPONSIBLE" => "372",
                                    "TREKER" => "",
                                    "COMMENTS" => "",
                                    "NAME_FILE_ISTOK" => $nameFile,
                                    "INN" => $updelement['inn'],
                                    "KPP" => $updelement['kpp'],
                                    "DATE_DOCS" => $updelement['doc_data'],
                                    "NUM_DOCS" => $updelement['doc_number'],
                                    "CONTRAGENT" => $id_company,
                                    'SERVICES' => $arValue
                                )
                            );
                            $oElement = new CIBlockElement();

                            if(!$id=$oElement->Add($arFields, false, false, true)) {
                                $logger = Logger::getLogger('disk', 'disk/events.txt');
                                $logger->log("Error add element UPD:");
                                $logger->log(array($oElement->LAST_ERROR));
                            }
//                            $logger = Logger::getLogger('AddXLSXClosDoc', 'disk/AddXLSXClosDoc.txt');
//                            $logger->log(array($arFields,$id));
                        }
                    }

                    //Переименование файла
                    $date = date("Y.m.d H-i-m");
                    $extensionFile = $file->getExtension();
                    $newName = str_replace(".$extensionFile", "", $nameFile);
                    $newName = $newName . "(" . $date . ")" . ".$extensionFile";
                    $file->rename("$newName");
                }
            }
        }
    }
}