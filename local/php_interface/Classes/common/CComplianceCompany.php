<?php
    require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

    class CComplianceCompany
    {
        const UF_PROD_C_STATUS = "UF_PROD_C_STATUS";
        const UF_PROD_FILE_DATE = "UF_PROD_FILE_DATE";
        const UF_FULL_PROC_DATE = "UF_FULL_PROC_DATE";
        const UF_UPLOAD_TYPE = "UF_UPLOAD_TYPE";
        const ID_FAST_UPLOAD = 318;
        const COMPLIANCE_IBLOCK_CODE = 'import_company';
        const PATH_TO_UPLOAD_FILE = '/upload/prod_file/company_prod.csv';
        const MODIFIER_BITRIX_USER_ID = 372;
        const MAX_SIZE_CSV_FILE = 524288000;
        const CSV_EXTENSION = 'csv';
        const IMPORT_CSV_ENCODING = "UTF-16LE";
        const UPLOAD_INPUT_NAME = 'uploadCurrentCSVfile';
        const STATUS_DELETE_OLD_ELEMENTS_VALUE = 'DELETE_OLD_ELEMENTS';
        const STATUS_DELETE_OLD_ELEMENTS_ID = 306;
        const STATUS_DELETE_OLD_ELEMENTS_IN_PROGRESS_VALUE = 'DELETE_OLD_ELEMENTS_IN_PROGRESS';
        const STATUS_DELETE_OLD_ELEMENTS_IN_PROGRESS_ID = 307;
        const STATUS_IMPORT_CSV_VALUE = 'IMPORT_CSV';
        const STATUS_IMPORT_CSV_ID = 308;
        const STATUS_IMPORT_CSV_IN_PROGRESS_VALUE = 'IMPORT_CSV_IN_PROGRESS';
        const STATUS_IMPORT_CSV_IN_PROGRESS_ID = 309;
        const STATUS_PARSE_CSV_VALUE = 'PARSE_CSV';
        const STATUS_PARSE_CSV_ID = 310;
        const STATUS_PARSE_CSV_IN_PROGRESS_VALUE = 'PARSE_CSV_IN_PROGRESS';
        const STATUS_PARSE_CSV_IN_PROGRESS_ID = 311;
        const STATUS_UPDATE_ALL_ELEMENTS_VALUE = 'UPDATE_ALL_ELEMENTS';
        const STATUS_UPDATE_ALL_ELEMENTS_ID = 312;
        const STATUS_UPDATE_ALL_ELEMENTS_IN_PROGRESS_VALUE = 'UPDATE_ALL_ELEMENTS_IN_PROGRESS';
        const STATUS_UPDATE_ALL_ELEMENTS_IN_PROGRESS_ID = 313;
        const STATUS_FAST_UPDATE_ELEMENTS_VALUE = 'FAST_UPDATE_ELEMENTS';
        const STATUS_FAST_UPDATE_ELEMENTS_ID = 315;
        const STATUS_FAST_UPDATE_ELEMENTS_IN_PROGRESS_VALUE = 'FAST_UPDATE_ELEMENTS_IN_PROGRESS';
        const STATUS_FAST_UPDATE_ELEMENTS_IN_PROGRESS_ID = 316;
        const STATUS_IMPORT_CSV_COMPLETE_VALUE = "IMPORT_CSV_COMPLETE";
        const STATUS_IMPORT_CSV_COMPLETE_ID = 314;
        const NOTIFY_MESSAGE = "Обработка компаний завершена";
        const READY_PROGRESS_STATUS_VALUE = 2;

        private $CAskaronSettings;
        private $oIBlockElement;
        private $arTenantCompanyType;

        function __construct()
        {
            CModule::IncludeModule('main');
            CModule::IncludeModule('iblock');
            CModule::IncludeModule("askaron.include");
            CModule::IncludeModule("askaron.settings");
            CModule::IncludeModule("im");
            $this->CAskaronSettings = new CAskaronSettings;
            $this->oIBlockElement   = new CIBlockElement();
        }

        public function updateElementsCsv2Db()
        {
            if ($this->getCurrentStatus() !== self::STATUS_PARSE_CSV_VALUE) {
                return;
            }

            $this->setStatus(self::STATUS_PARSE_CSV_IN_PROGRESS_ID);

            $this->importAllElementsFromCSV2Db();

            $this->setStatus(self::STATUS_IMPORT_CSV_COMPLETE_ID);
        }

        public function importCSVFile()
        {
            if ($this->getCurrentStatus() !== self::STATUS_IMPORT_CSV_VALUE ||
                empty($_FILES[self::UPLOAD_INPUT_NAME])) {
                return "Wrong status or empty files";
            }

            if ($_FILES[self::UPLOAD_INPUT_NAME]["size"] > self::MAX_SIZE_CSV_FILE) {
                return "File is out of size";
            }

            $ext = end(explode(".", $_FILES[self::UPLOAD_INPUT_NAME]["name"]));

            if ($ext !== self::CSV_EXTENSION || empty($_REQUEST[self::UF_PROD_FILE_DATE])) {
                return "Wrong file ext";
            }

            $this->setDate(self::UF_PROD_FILE_DATE, $this->formatDate($_REQUEST[self::UF_PROD_FILE_DATE]));
            $this->setUploadType(self::UF_UPLOAD_TYPE, $_REQUEST[self::UF_UPLOAD_TYPE]);

            $this->setStatus(self::STATUS_IMPORT_CSV_IN_PROGRESS_ID);
            move_uploaded_file($_FILES[self::UPLOAD_INPUT_NAME]['tmp_name'],
                $_SERVER["DOCUMENT_ROOT"] . self::PATH_TO_UPLOAD_FILE);
            $this->setStatus(self::STATUS_DELETE_OLD_ELEMENTS_ID);

            return true;
        }

        public function deleteAllIblockElements($iblockID)
        {
            global $DB;
            if ($this->getCurrentStatus() !== self::STATUS_DELETE_OLD_ELEMENTS_VALUE) {
                return;
            }

            $this->setStatus(self::STATUS_DELETE_OLD_ELEMENTS_IN_PROGRESS_ID);

            if($iblockID > 0){
                $START_TIME = mktime();
                $strSql = "DELETE FROM b_iblock_element WHERE IBLOCK_ID = $iblockID";
                $results = $DB->Query($strSql);
                $TIME = (mktime() - $START_TIME);
                echo "\n\nВремя DELETE FROM обработки , сек: $TIME \n\n";
                $strSql = "TRUNCATE TABLE b_iblock_element_prop_s$iblockID";
                $START_TIME = mktime();
                $results = $DB->Query($strSql);
                $TIME = (mktime() - $START_TIME);
                echo "\n\nВремя TRUNCATE TABLE обработки , сек: $TIME \n\n";

                $this->setStatus(self::STATUS_PARSE_CSV_ID);
            }
        }

        public function setStatus($newStatusID)
        {
            return $this->CAskaronSettings->Update(
                array(
                    self::UF_PROD_C_STATUS => $newStatusID,
                )
            );
        }

        public function setDate($propCode, $date)
        {
            return $this->CAskaronSettings->Update(
                array(
                    $propCode => $date,
                )
            );
        }

        public function setUploadType($propCode, $uploadType)
        {
            return $this->CAskaronSettings->Update(
                array(
                    $propCode => $uploadType,
                )
            );
        }

        public function getCurrentStatus()
        {
            $currentStatusID  = COption::GetOptionString("askaron.settings", self::UF_PROD_C_STATUS);
            $dbStatus         = CUserFieldEnum::GetList(array(), array(
                "ID" => $currentStatusID,
            ));
            $arStatus         = $dbStatus->Fetch();
            $currentStatusVal = $arStatus["VALUE"];

            return $currentStatusVal;
        }

        public function getLastUpdateDate()
        {
            return COption::GetOptionString("askaron.settings", self::UF_FULL_PROC_DATE);
        }

        public function getUploadType()
        {
            return COption::GetOptionString("askaron.settings", self::UF_UPLOAD_TYPE);
        }

        public function getLastFileDate()
        {
            return COption::GetOptionString("askaron.settings", self::UF_PROD_FILE_DATE);
        }

        private function importAllElementsFromCSV2Db()
        {
            define("NO_KEEP_STATISTIC", true);
            define("NOT_CHECK_PERMISSIONS", true);
            define('CHK_EVENT', true);
            ini_set('memory_limit', '-1');
            @set_time_limit(0);
            @ignore_user_abort(true);

            global $USER;
            $USER->Authorize(self::MODIFIER_BITRIX_USER_ID);

            $this->arTenantCompanyType = array(
                "0"    => "0", //Тест
                "1"    => "CUSTOMER", //ОФД RU. Клиенты
                "5"    => "3", //ОФД RU. Клиенты, анонсировавшие расторжение
                "6"    => "4", //ОФД RU. Бывшие клиенты
                "7"    => "5", //ОФД RU. Бизнес Администрирование
                "8"    => "6", //Тензор
                "9"    => "7", //Рапкат
                "10"   => "8", //Без блокировки касс
                "11"   => "PARTNER", //Агент/Партнер
                "12"   => "9", //Временные пользователи
                "13"   => "13", //1c
                "14"   => "2", //Клиент + Агент
                "15"   => "10", //Корпорации
                "16"   => "16", //Атол-онлайн
                "17"   => "17", //Старрус
                "1000" => "11", //Trashcan
            );

            /*$cont = file_get_contents($_SERVER["DOCUMENT_ROOT"] . self::PATH_TO_UPLOAD_FILE);
            $cont = iconv(self::IMPORT_CSV_ENCODING, "UTF-8", $cont);
            $fw = fopen($_SERVER["DOCUMENT_ROOT"] . self::PATH_TO_UPLOAD_FILE, "w");
            fwrite($fw, $cont);
            fclose($fw);*/

            $row = 1;
            $fastUpdateAdd = 0;

            if($this->getUploadType() == self::ID_FAST_UPLOAD){
                $arrayBitrix = Reports::getAllCompanyBitrixWithTenant();
                foreach($arrayBitrix as $arVal) {
                    $arCompareBitrix[$arVal["ID_PROD"].'_'.$arVal["INN"].'_'.$arVal["KPP"]] = $arVal["ID"];
                }
            }

            if (($handle = fopen($_SERVER["DOCUMENT_ROOT"] . self::PATH_TO_UPLOAD_FILE, "r")) !== false) {
                $START_TIME = mktime();
                while (($data = fgetcsv($handle, 0, "\t")) !== false) {
                    $arResult = array(
                        "ID_PROD"            => $data[0],
                        "INN"                => $data[1],
                        "KPP"                => $data[2],
                        "COMPANY_TYPE"       => $data[3],  //TID, Тенант, тип контрагента
                        "NAME_KKT"           => $data[4],  //EXTERNAL_CODE, имя ККТ
                        "DATE_CREATE"        => $data[5],  //created_at, Дата добавления ККТ
                        "MODEL"              => $data[6],
                        "NUM_KKT"            => $data[7],  //serial_number, Заводской номер ККТ
                        "RNM_KKT"            => $data[8],  //fns_ecr_id, РНМ, RegID
                        "NUM_FN"             => $data[9],  //fiscal_drive_number, Номер ФН
                        "ADDRESS_KKT"        => $data[10], //full_address, Адрес ККТ
                        "NAME_TARIFF"        => $data[11], //name_tariff , Тариф
                        "COST_TARIFF"        => $data[12], //full_period_price, Стоимость тарифа
                        "DATE_FROM"          => $data[13], //from_date, Дата начала тарифа
                        "DATE_TO"            => $data[14], //to_date, Дата завершения тарифа
                        "CODE_AGENT"         => $data[15], //agent_code, Код агента
                        "PROMO_KOD"          => $data[16], //promo_code, ID Промокода
                        "EMAIL"              => $data[17], //responsible_person_email, Email
                        "PHONE"              => $data[18], //contact_phone, Телефон
                        "ACCOUNT_ID"         => $data[19], //account_id, Телефон
                        "BALANCE"            => $data[20], //balance, баланс реестра платежей
                        "ASSIGNED_LAST_NAME" => $data[21], //Менеджер договора
                    );

                    //$this->addElementImportCompany($arResult);
                    $searchCombination = $arResult["ID_PROD"].'_'.$arResult["INN"].'_'.$arResult["KPP"];
                    if( $this->getUploadType() == self::ID_FAST_UPLOAD ) {
                        if( !isset($arCompareBitrix[$searchCombination]) ) {
                            $arResult["FAST_UPDATE"] = getPropertyEnumIdByXmlId(GetIBlockIDByCode('import_company'), 'FAST_UPDATE_LIST_ELEM');
                            $this->addElementImportCompany($arResult);
                            $fastUpdateAdd++;
                        }
                    } else {
                        $this->addElementImportCompany($arResult);
                    }

                    $row++;
                }
                $TIME = (mktime() - $START_TIME) / 60;
                echo "\n\nВремя обработки , мин: $TIME \n\n";
                if( $this->getUploadType() == self::ID_FAST_UPLOAD ){
                    echo "\n\nКол-во обработанных элементов при быстрой обработке: $fastUpdateAdd \n\n";
                }
                echo "\n\nОбщее кол-во строк: $row \n";

                fclose($handle);
            }

            return $USER;
        }

        private function addElementImportCompany($arParams)
        {
            global $USER;
            $arParams['INN'] = str_replace("OBSOLETE", "", $arParams['INN']);

            //Проверка на валидность ИНН
            switch (strlen($arParams['INN'])) {
                case 10:
                    break;
                case 12:
                    break;
                default:
                    echo "Не валидный ИНН: " . $arParams['INN'] . "\n";

                    return false;
                    break;
            }

            switch ($arParams['COMPANY_TYPE']) {
                case "0": //Название тенанта (прод) из выгрузки -ТЕСТ
                    echo "Не валидный тенант ТЕСТ";

                    return false;
                    break;
                case "7": //Название тенанта (прод) из выгрузки -Бизнес администрирование
                    echo "Не валидный тенант Бизнес администрирование";

                    return false;
                    break;
                case "9": //Название тенанта (прод) из выгрузки -ЛК для тестирования Рапкат
                    echo "Не валидный тенант ЛК для тестирования Рапкат";

                    return false;
                    break;
                case "1000": //Название тенанта (прод) из выгрузки -Trashcan
                    echo "Не валидный тенант Trashcan";

                    return false;
                    break;
                default:
                    break;
            }


            //Определение ID тенант
            if (array_key_exists($arParams['COMPANY_TYPE'], $this->arTenantCompanyType)) {
                $idTenant = $this->arTenantCompanyType[$arParams['COMPANY_TYPE']];
            } else {
                $idTenant = "ERROR";
            }

            $props = array(
                "STATUS"             => 0,
                "ID_PROD"            => $arParams['ID_PROD'],
                "INN"                => $arParams['INN'],
                "KPP"                => $arParams['KPP'],
                "COMPANY_TYPE"       => $idTenant,
                "NAME_KKT"           => $arParams['NAME_KKT'],
                "DATE_CREATE"        => $arParams['DATE_CREATE'],
                "MODEL"              => $arParams['MODEL'],
                "NUM_KKT"            => $arParams['NUM_KKT'],
                "RNM_KKT"            => $arParams['RNM_KKT'],
                "NUM_FN"             => $arParams['NUM_FN'],
                "ADDRESS_KKT"        => $arParams['ADDRESS_KKT'],
                "NAME_TARIFF"        => $arParams['NAME_TARIFF'],
                "COST_TARIFF"        => $arParams['COST_TARIFF'],
                "DATE_FROM"          => $arParams['DATE_FROM'],
                "DATE_TO"            => $arParams['DATE_TO'],
                "CODE_AGENT"         => $arParams['CODE_AGENT'],
                "PROMO_KOD"          => $arParams['PROMO_KOD'],
                "EMAIL"              => $arParams['EMAIL'],
                "PHONE"              => $arParams['PHONE'],
                "ACCOUNT_ID"         => $arParams['ACCOUNT_ID'],
                "BALANCE"            => ( ! empty($arParams['BALANCE'])) ? $arParams['BALANCE'] : 0,
                "ASSIGNED_LAST_NAME" => $arParams['ASSIGNED_LAST_NAME']
            );

            if(isset($arParams["FAST_UPDATE"])){
                $props["FAST_UPDATE"] = $arParams["FAST_UPDATE"];
            }

            $arLoadProductArray = Array(
                "MODIFIED_BY"       => self::MODIFIER_BITRIX_USER_ID, // элемент изменен текущим пользователем
                "IBLOCK_SECTION_ID" => false,          // элемент лежит в корне раздела
                "IBLOCK_ID"         => getIblockIDByCode(self::COMPLIANCE_IBLOCK_CODE),
                "PROPERTY_VALUES"   => $props,
                "NAME"              => "INN: $arParams[INN] | KPP: $arParams[KPP] | RNM_KKT: $arParams[RNM_KKT]"
            );


            $START_TIME = mktime();

            if ($PRODUCT_ID = $this->oIBlockElement->Add($arLoadProductArray, false, false, false)) {
                //echo "New ID: " . $PRODUCT_ID . "\n";
            } else {
                echo "Error: " . $this->oIBlockElement->LAST_ERROR . "\n";
            }
        }

        private function formatDate($inputDate)
        {
            return date("d.m.Y", strtotime($inputDate));
        }

        public function sendNotify($userIDs)
        {
            foreach ($userIDs as $id) {
                $arMessageFields = array(
                    "TO_USER_ID"     => $id,
                    "FROM_USER_ID"   => 0,
                    "NOTIFY_TYPE"    => IM_NOTIFY_SYSTEM,
                    "NOTIFY_MODULE"  => "im",
                    "NOTIFY_TAG"     => "IM_CONFIG_NOTICE",
                    "NOTIFY_MESSAGE" => self::NOTIFY_MESSAGE,
                );
                CIMNotify::Add($arMessageFields);
            }
        }

        public function getProgressStatus()
        {
            $arFilter = array(
                'IBLOCK_CODE' => self::COMPLIANCE_IBLOCK_CODE,
            );


            if($this->getCurrentStatus() == self::STATUS_FAST_UPDATE_ELEMENTS_VALUE || $this->getCurrentStatus() == self::STATUS_FAST_UPDATE_ELEMENTS_IN_PROGRESS_VALUE) {
                $arFilter["!PROPERTY_FAST_UPDATE"] = false;
            }

            $dbAllElements = CIBlockElement::GetList(
                false,
                $arFilter,
                false,
                false,
                array('IBLOCK_ID', 'ID', 'PROPERTY_STATUS', 'PROPERTY_FAST_UPDATE')
            );

            $countElementsProgress = [
                "new"   => 0,
                "ready" => 0
            ];

            while ($element = $dbAllElements->GetNext()) {
                $element["PROPERTY_STATUS_VALUE"] != self::READY_PROGRESS_STATUS_VALUE ? $countElementsProgress["new"]++ : $countElementsProgress["ready"]++;
            };


            return $countElementsProgress;
        }

        public function getImportReadyPercent() {
            $readyPercent = 0;
            $obFile = new SplFileObject($_SERVER["DOCUMENT_ROOT"] . self::PATH_TO_UPLOAD_FILE, 'r');
            $obFile->seek(PHP_INT_MAX);
            $fileLinesCount = $obFile->key() + 1;
            $el = new CIBlockElement();
            $rsData = $el->GetList(
                array("ID" => "DESC"),
                array("IBLOCK_ID" => getIblockIDByCode(self::COMPLIANCE_IBLOCK_CODE)),
                false,
                false,
                array()
            );
            $elementsCount = $rsData->SelectedRowsCount();
            $readyPercent = round($elementsCount/($fileLinesCount/100), 2);
            return $readyPercent;
        }
    }