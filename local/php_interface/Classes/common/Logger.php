<?php

class Logger
{
    protected static $loggers=array();

    protected $name;
    protected $file;
    protected $fp;

    public function __construct($name, $file = null){
        $this->name = $name;
        $this->file = $file;

        $this->open();
    }

    public function open(){
        $path = ($this->file == null) ? $_SERVER["DOCUMENT_ROOT"] . '/' . 'log' . '/' . $this->name . '.log' : $_SERVER["DOCUMENT_ROOT"] . '/' . 'log' . '/' . $this->file;
        if (!file_exists($path)) {
            $info = new SplFileInfo($path);
            mkdir($info->getPath(), 0755, true);
        }
        $this->fp = fopen($path,'a+');
    }

    public static function getLogger($name = 'log', $file = null){
        if(!isset(self::$loggers[$name])){
            self::$loggers[$name] = new Logger($name, $file);
        }

        return self::$loggers[$name];
    }

    public function log($message){
        if(!is_string($message)){
            $this->logPrint($message);
            return ;
        }

        $log='';
        $log .= '[' . date('D M d H:i:s Y',time()) . '] ';
        if(func_num_args() > 1){
            $params = func_get_args();
            $message = call_user_func_array('sprintf',$params);
        }

        $log .= $message;
        $log .= "\n";

        $this->_write($log);
    }

    public function logPrint($obj){
        ob_start();

        print_r($obj);

        $ob = ob_get_clean();
        $this->log($ob);
    }

    protected function _write($string){
        fwrite($this->fp, $string);
    }

    public function __destruct(){
        fclose($this->fp);
    }
}