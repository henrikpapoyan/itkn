<?php

class DecrimentPrice
{
    private $CCrmEvent;
    private $oDeal;
    private $oIBlock;
    private $IBLOCK_ID;

    function DecrimentPrice(){
        CModule::IncludeModule('crm');
        CModule::IncludeModule('iblock');
        $this->CCrmEvent  = new CCrmEvent();
        $this->oDeal      = new CCrmDeal;
        $this->oIBlock    = new CIBlockElement;
        $this->IBLOCK_ID  = getIblockIDByCode('registry_kkt');
    }

    function parseOne($onemsg, $idPostfix, $entity_data_class) {
        if (substr_count($onemsg, 'add_one_kkt')) {
            if (!empty($this->checkInWork($idPostfix, $entity_data_class))) {
                return false;
            }
            return $this->addOneKKT($onemsg);
        }
        return false;
    }

    function checkInWork($idPostfix, $entity_data_class){
        //Проверка письма по статусу "НЕ в работе"
        $res = $entity_data_class::getById($idPostfix);
        $arLetter = $res->fetch();
        if($arLetter['UF_IN_WORK'] == "1"){
            return true;
        }

        //Проставляем статус в Работе
        $entity_data_class::update($idPostfix, array("UF_IN_WORK" => "1"));
        return false;
    }

    //Функция обработки сообщения с типом add_one_kkt
    function addOneKKT($onemsg)
    {
        $kkmRegId = $this->parseParamsString("KKMRegId", $onemsg);

        if (!empty($kkmRegId)) {
            echo "Получен регистрационный номер  ККТ [$kkmRegId] . \n";
            $arSelect = Array("ID", "IBLOCK_ID", "PROPERTY_*");
            $arFilter = array("IBLOCK_ID" => $this->IBLOCK_ID, "PROPERTY_RNM_KKT" => $kkmRegId);
            $res = $this->oIBlock->GetList(Array(), $arFilter, false, false, $arSelect);

            //ККТ не найден
            if(intval($res->SelectedRowsCount()) <= 0 ) {
                echo "ККТ не найден\n";
                return true;
            }

            //Нашли ККТ
            if ($arKKT = $res->GetNextElement()) {
                $arFields = $arKKT->GetFields();
                $arProps = $arKKT->GetProperties();
                $idKKT  = $arFields['ID'];
                $idDeal = $arProps['DEAL']['VALUE'];
                $priceKKT = (int)$arProps['COST_TARIF']['VALUE'];
            }

            //Удаляем реестр ККТ
            $this->oIBlock->Delete($idKKT);

            echo "IdKKT: $idKKT \n";
            echo "idDeal: $idDeal \n";

            //Проврка на наличие ID сделки
            if(empty($idDeal)){
                return true;
            }

            $res = $this->oDeal->GetListEx(array('ID' => 'desc'), array('ID' => $idDeal));
            if($arDeal = $res->Fetch()){
                //Если Title = 'Основная услуга ОФД' - удаляем сделку
                if($arDeal['TITLE'] == "Основная услуга ОФД") {
                    $this->oDeal->Delete($idDeal);
                    return true;
                }

                $priceDeal = (int)$arDeal['OPPORTUNITY'];
            } else {
                return true;
            }

            //Актуализация цены сделки
            $actualPrice = $priceDeal - $priceKKT;
            $actualPrice = ($actualPrice < 0) ? '0' : $actualPrice;

            echo "priceKKT: $priceKKT \n";
            echo "priceDEAL: $priceDeal \n";
            echo "actualDEAL: $actualPrice \n";

            //Обновление сделки
            $arFields = Array(
                "TYPE_ID" => 2,
                'OPPORTUNITY' => $actualPrice,
                'UF_KKT'  => 0
            );

            if($this->oDeal->Update($idDeal, $arFields)) {
                echo "Успешно обновили сделку.\n";
            } else {
                echo "Ошибка обновления сделки.\n";
            }

            $res = $this->CCrmEvent->GetList(array('ID' => 'asc'), array('ENTITY_TYPE' => 'DEAL', 'ENTITY_ID' => $idDeal));
            while ($arHistory = $res->fetch()) {
                if(substr_count($arHistory['EVENT_TEXT_1'], "Подключён ККТ с регистрационным номером")) {
                    $this->CCrmEvent->Delete($arHistory['ID']);
                }

                if(substr_count($arHistory['EVENT_NAME'], 'Изменено поле "Сумма"')) {
                    $this->CCrmEvent->Delete($arHistory['ID']);
                }
            }

        } else {
            echo "При работе скрипта возникла ошибка: Не заполнено поле регистрационного номера ККТ!\n";
        }

        return true;
    }

    function parseParamsString($nameParametr, $strSearch){
        preg_match("/" . $nameParametr . ": (.*?)\n/i", $strSearch , $out);

        if(array_key_exists(1,$out)) {
            $name = str_replace(array("\r\n", "\r", "\n"), '',  strip_tags($out[1]));
            $name = trim($name);
            return $name;
        } else {
            return false;
        }
    }
}