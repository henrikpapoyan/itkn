<?
if (!CModule::IncludeModule('report'))
    return;

use Bitrix\Main;
use Bitrix\Crm;
class CCrmInvoiceReportHelperCustom extends CCrmInvoiceReportHelper
{
    public static function getDefaultReports()
    {
        IncludeModuleLangFile(__FILE__);

        $reports = array(
            '14.1.0' => array(
                array(
                    'title' => GetMessage('CRM_REPORT_DEFAULT_INVOICES_BY_MANAGER'),
                    'description' => GetMessage('CRM_REPORT_DEFAULT_INVOICES_BY_MANAGER_DESCR'),
                    'mark_default' => 1,
                    'settings' => unserialize('a:10:{s:6:"entity";s:18:"Bitrix\Crm\Invoice";s:6:"period";a:2:{s:4:"type";s:5:"month";s:5:"value";N;}s:6:"select";a:5:{i:4;a:2:{s:4:"name";s:22:"ASSIGNED_BY.SHORT_NAME";s:5:"alias";s:0:"";}i:8;a:3:{s:4:"name";s:8:"IS_PAYED";s:5:"alias";s:0:"";s:4:"aggr";s:3:"SUM";}i:6;a:3:{s:4:"name";s:11:"PRICE_PAYED";s:5:"alias";s:0:"";s:4:"aggr";s:3:"SUM";}i:9;a:3:{s:4:"name";s:11:"IS_CANCELED";s:5:"alias";s:0:"";s:4:"aggr";s:3:"SUM";}i:7;a:3:{s:4:"name";s:14:"PRICE_CANCELED";s:5:"alias";s:0:"";s:4:"aggr";s:3:"SUM";}}s:6:"filter";a:1:{i:0;a:2:{i:0;a:5:{s:4:"type";s:5:"field";s:4:"name";s:11:"ASSIGNED_BY";s:7:"compare";s:5:"EQUAL";s:5:"value";s:0:"";s:10:"changeable";s:1:"1";}s:5:"LOGIC";s:3:"AND";}}s:4:"sort";i:6;s:9:"sort_type";s:4:"DESC";s:5:"limit";N;s:12:"red_neg_vals";b:0;s:13:"grouping_mode";b:0;s:5:"chart";a:4:{s:7:"display";b:1;s:4:"type";s:3:"bar";s:8:"x_column";i:4;s:9:"y_columns";a:1:{i:0;i:6;}}}')
                ),
                array(
                    'title' => GetMessage('CRM_REPORT_DEFAULT_INVOICES_BY_COMPANY'),
                    'description' => GetMessage('CRM_REPORT_DEFAULT_INVOICES_BY_COMPANY_DESCR'),
                    'mark_default' => 2,
                    'settings' => unserialize('a:10:{s:6:"entity";s:18:"Bitrix\Crm\Invoice";s:6:"period";a:2:{s:4:"type";s:5:"month";s:5:"value";N;}s:6:"select";a:3:{i:12;a:2:{s:4:"name";s:28:"INVOICE_UTS.COMPANY_BY.TITLE";s:5:"alias";s:0:"";}i:8;a:3:{s:4:"name";s:8:"IS_PAYED";s:5:"alias";s:0:"";s:4:"aggr";s:3:"SUM";}i:6;a:3:{s:4:"name";s:11:"PRICE_PAYED";s:5:"alias";s:0:"";s:4:"aggr";s:3:"SUM";}}s:6:"filter";a:1:{i:0;a:4:{i:0;a:5:{s:4:"type";s:5:"field";s:4:"name";s:25:"INVOICE_UTS.COMPANY_BY.ID";s:7:"compare";s:7:"GREATER";s:5:"value";s:1:"0";s:10:"changeable";s:1:"0";}i:1;a:5:{s:4:"type";s:5:"field";s:4:"name";s:11:"PRICE_PAYED";s:7:"compare";s:7:"GREATER";s:5:"value";s:1:"0";s:10:"changeable";s:1:"0";}i:2;a:5:{s:4:"type";s:5:"field";s:4:"name";s:11:"ASSIGNED_BY";s:7:"compare";s:5:"EQUAL";s:5:"value";s:0:"";s:10:"changeable";s:1:"1";}s:5:"LOGIC";s:3:"AND";}}s:4:"sort";i:6;s:9:"sort_type";s:4:"DESC";s:5:"limit";N;s:12:"red_neg_vals";b:0;s:13:"grouping_mode";b:0;s:5:"chart";a:4:{s:7:"display";b:1;s:4:"type";s:3:"bar";s:8:"x_column";i:12;s:9:"y_columns";a:1:{i:0;i:6;}}}')
                ),
                array(
                    'title' => GetMessage('CRM_REPORT_DEFAULT_INVOICES_BY_CONTACT'),
                    'description' => GetMessage('CRM_REPORT_DEFAULT_INVOICES_BY_CONTACT_DESCR'),
                    'mark_default' => 3,
                    'settings' => unserialize('a:10:{s:6:"entity";s:18:"Bitrix\Crm\Invoice";s:6:"period";a:2:{s:4:"type";s:5:"month";s:5:"value";N;}s:6:"select";a:3:{i:15;a:1:{s:4:"name";s:33:"INVOICE_UTS.CONTACT_BY.SHORT_NAME";}i:8;a:3:{s:4:"name";s:8:"IS_PAYED";s:5:"alias";s:0:"";s:4:"aggr";s:3:"SUM";}i:6;a:3:{s:4:"name";s:11:"PRICE_PAYED";s:5:"alias";s:0:"";s:4:"aggr";s:3:"SUM";}}s:6:"filter";a:1:{i:0;a:4:{i:0;a:5:{s:4:"type";s:5:"field";s:4:"name";s:25:"INVOICE_UTS.CONTACT_BY.ID";s:7:"compare";s:7:"GREATER";s:5:"value";s:1:"0";s:10:"changeable";s:1:"0";}i:1;a:5:{s:4:"type";s:5:"field";s:4:"name";s:11:"PRICE_PAYED";s:7:"compare";s:7:"GREATER";s:5:"value";s:1:"0";s:10:"changeable";s:1:"0";}i:2;a:5:{s:4:"type";s:5:"field";s:4:"name";s:11:"ASSIGNED_BY";s:7:"compare";s:5:"EQUAL";s:5:"value";s:0:"";s:10:"changeable";s:1:"1";}s:5:"LOGIC";s:3:"AND";}}s:4:"sort";i:6;s:9:"sort_type";s:4:"DESC";s:5:"limit";N;s:12:"red_neg_vals";b:0;s:13:"grouping_mode";b:0;s:5:"chart";a:4:{s:7:"display";b:1;s:4:"type";s:3:"bar";s:8:"x_column";i:15;s:9:"y_columns";a:1:{i:0;i:6;}}}')
                ),
                array(
                    'title' => GetMessage('CRM_REPORT_DEFAULT_SUMM_REPORTS'),
                    'description' => GetMessage('CRM_REPORT_DEFAULT_SUMM_REPORTS_DESCR'),
                    'mark_default' => 4,
                    'settings' => unserialize('a:10:{s:6:"entity";s:18:"Bitrix\Crm\Invoice";s:6:"period";a:2:{s:4:"type";s:5:"month";s:5:"value";N;}s:6:"select";a:3:{i:15;a:1:{s:4:"name";s:33:"INVOICE_UTS.CONTACT_BY.SHORT_NAME";}i:8;a:3:{s:4:"name";s:8:"IS_PAYED";s:5:"alias";s:0:"";s:4:"aggr";s:3:"SUM";}i:6;a:3:{s:4:"name";s:11:"PRICE_PAYED";s:5:"alias";s:0:"";s:4:"aggr";s:3:"SUM";}}s:6:"filter";a:1:{i:0;a:4:{i:0;a:5:{s:4:"type";s:5:"field";s:4:"name";s:25:"INVOICE_UTS.CONTACT_BY.ID";s:7:"compare";s:7:"GREATER";s:5:"value";s:1:"0";s:10:"changeable";s:1:"0";}i:1;a:5:{s:4:"type";s:5:"field";s:4:"name";s:11:"PRICE_PAYED";s:7:"compare";s:7:"GREATER";s:5:"value";s:1:"0";s:10:"changeable";s:1:"0";}i:2;a:5:{s:4:"type";s:5:"field";s:4:"name";s:11:"ASSIGNED_BY";s:7:"compare";s:5:"EQUAL";s:5:"value";s:0:"";s:10:"changeable";s:1:"1";}s:5:"LOGIC";s:3:"AND";}}s:4:"sort";i:6;s:9:"sort_type";s:4:"DESC";s:5:"limit";N;s:12:"red_neg_vals";b:0;s:13:"grouping_mode";b:0;s:5:"chart";a:4:{s:7:"display";b:1;s:4:"type";s:3:"bar";s:8:"x_column";i:15;s:9:"y_columns";a:1:{i:0;i:6;}}}')
                )
            )
        );

        foreach ($reports as &$reportByVersion)
        {
            foreach ($reportByVersion as &$report)
            {
                if ($report['mark_default'] === 1)
                {
                    $report['settings']['select'][4]['alias'] = GetMessage('CRM_REPORT_DEFAULT_INVOICES_BY_MANAGER_ALIAS_4');
                    $report['settings']['select'][6]['alias'] = GetMessage('CRM_REPORT_DEFAULT_INVOICES_BY_MANAGER_ALIAS_6');
                    $report['settings']['select'][7]['alias'] = GetMessage('CRM_REPORT_DEFAULT_INVOICES_BY_MANAGER_ALIAS_7');
                    $report['settings']['select'][8]['alias'] = GetMessage('CRM_REPORT_DEFAULT_INVOICES_BY_MANAGER_ALIAS_8');
                    $report['settings']['select'][9]['alias'] = GetMessage('CRM_REPORT_DEFAULT_INVOICES_BY_MANAGER_ALIAS_9');
                }
                else if ($report['mark_default'] === 2)
                {
                    $report['settings']['select'][6]['alias'] = GetMessage('CRM_REPORT_DEFAULT_INVOICES_BY_COMPANY_ALIAS_6');
                    $report['settings']['select'][8]['alias'] = GetMessage('CRM_REPORT_DEFAULT_INVOICES_BY_COMPANY_ALIAS_8');
                    $report['settings']['select'][12]['alias'] = GetMessage('CRM_REPORT_DEFAULT_INVOICES_BY_COMPANY_ALIAS_12');
                }
                else if ($report['mark_default'] === 3)
                {
                    $report['settings']['select'][6]['alias'] = GetMessage('CRM_REPORT_DEFAULT_INVOICES_BY_CONTACT_ALIAS_6');
                    $report['settings']['select'][8]['alias'] = GetMessage('CRM_REPORT_DEFAULT_INVOICES_BY_CONTACT_ALIAS_8');
                }
                else if ($report['mark_default'] === 4)
                {
                    $report['settings']['select'][6]['alias'] = GetMessage('CRM_REPORT_DEFAULT_INVOICES_BY_CONTACT_ALIAS_6');
                    $report['settings']['select'][8]['alias'] = GetMessage('CRM_REPORT_DEFAULT_INVOICES_BY_CONTACT_ALIAS_8');
                }
            }
            unset($report);
        }
        unset($reportByVersion);
        return $reports;
    }
}
?>