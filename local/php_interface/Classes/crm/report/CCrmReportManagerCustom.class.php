<?php
if (!CModule::IncludeModule('report'))
    return;

use Bitrix\Main;
use Bitrix\Crm;

class CCrmReportManagerCustom extends CCrmReportManager
{
    private static $OWNER_INFOS = null;
    private static function createOwnerInfo($ID, $className, $title)
    {
        return array(
            'ID' => $ID,
            'HELPER_CLASS' => $className,
            'TITLE' => $title
        );
    }
    public static function getOwnerInfos()
    {
        if(self::$OWNER_INFOS)
        {
            return self::$OWNER_INFOS;
        }

        IncludeModuleLangFile(__FILE__);

        self::$OWNER_INFOS = array();
        self::$OWNER_INFOS[] = self::createOwnerInfo(
            CCrmReportHelper::getOwnerId(),
            'CCrmReportHelper',
            GetMessage('CRM_REPORT_OWNER_TITLE_'.strtoupper(CCrmReportHelper::getOwnerId()))
        );
        self::$OWNER_INFOS[] = self::createOwnerInfo(
            CCrmProductReportHelper::getOwnerId(),
            'CCrmProductReportHelper',
            GetMessage('CRM_REPORT_OWNER_TITLE_'.strtoupper(CCrmProductReportHelper::getOwnerId()))
        );
        self::$OWNER_INFOS[] = self::createOwnerInfo(
            CCrmLeadReportHelper::getOwnerId(),
            'CCrmLeadReportHelper',
            GetMessage('CRM_REPORT_OWNER_TITLE_'.strtoupper(CCrmLeadReportHelper::getOwnerId()))
        );
        self::$OWNER_INFOS[] = self::createOwnerInfo(
            CCrmInvoiceReportHelper::getOwnerId(),
            'CCrmInvoiceReportHelper',
            GetMessage('CRM_REPORT_OWNER_TITLE_'.strtoupper(CCrmInvoiceReportHelper::getOwnerId()))
        );
        self::$OWNER_INFOS[] = self::createOwnerInfo(
            CCrmActivityReportHelper::getOwnerId(),
            'CCrmActivityReportHelper',
            GetMessage('CRM_REPORT_OWNER_TITLE_'.strtoupper(CCrmActivityReportHelper::getOwnerId()))
        );
        self::$OWNER_INFOS[] = self::createOwnerInfo(
            CCrmSummaryReportHelper::getOwnerId(),
            'CCrmSummaryReportHelper',
            GetMessage('CRM_REPORT_OWNER_TITLE_'.strtoupper(CCrmSummaryReportHelper::getOwnerId()))
        );
        return self::$OWNER_INFOS;
    }
    public static function getOwnerInfo($ownerID)
    {
        $ownerID = strval($ownerID);
        if($ownerID === '')
        {
            return null;
        }

        $infos = self::getOwnerInfos();
        foreach($infos as $info)
        {
            if($info['ID'] === $ownerID)
            {
                return $info;
            }
        }
        return null;
    }
    public static function getOwnerHelperClassName($ownerID)
    {
        $info = self::getOwnerInfo($ownerID);
        return $info ? $info['HELPER_CLASS'] : '';
    }
}
?>