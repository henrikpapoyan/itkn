<?php
if (!CModule::IncludeModule('report'))
    return;

use Bitrix\Main;
use Bitrix\Crm;

class CCrmSummaryReportHelper extends CCrmReportHelperBase
{
    protected static function prepareUFInfo()
    {
        if (is_array(self::$arUFId))
            return;

        self::$arUFId = array('ORDER');
        parent::prepareUFInfo();
    }

    public static function GetReportCurrencyID()
    {
        return CCrmReportManager::GetReportCurrencyID();
    }

    public static function SetReportCurrencyID($currencyID)
    {
        CCrmReportManager::SetReportCurrencyID($currencyID);
    }

    public static function getEntityName()
    {
        return 'Bitrix\Crm\Invoice';
    }
    public static function getOwnerId()
    {
        return 'crm_summary';
    }
    public static function getColumnList()
    {
        IncludeModuleLangFile(__FILE__);

        $columnList = array(
            'ID',
            'ACCOUNT_NUMBER',
            'ORDER_TOPIC',
            'STATUS_ID',
            'STATUS_SUB' => array(
                'IS_WORK',
                'IS_PAYED',
                'IS_CANCELED'
            ),
            'DATE_BILL_SHORT',
            'DATE_FINISHED_SHORT',
            'DATE_PAY_BEFORE_SHORT',
            'PAY_VOUCHER_DATE_SHORT',
            'PAY_VOUCHER_NUM',
            'DATE_MARKED_SHORT',
            'REASON_MARKED',
            'PRICE',
            'PRICE_WORK',
            'PRICE_PAYED',
            'PRICE_CANCELED',
            'CURRENCY',
            'PERSON_TYPE_ID',
            'PAY_SYSTEM_ID',
            'ASSIGNED_BY' => array(
                'ID',
                'SHORT_NAME',
                'NAME',
                'LAST_NAME',
                'WORK_POSITION'
            ),
            'INVOICE_UTS.DEAL_BY' => array(
                'ID',
                'TITLE'
            ),
            'INVOICE_UTS.CONTACT_BY' => array(
                'ID',
                'SHORT_NAME',
                'NAME',
                'LAST_NAME',
                'SECOND_NAME'
            ),
            'INVOICE_UTS.COMPANY_BY' => array(
                'ID',
                'TITLE'
            ),
            'InvoiceSpec:INVOICE' => array(
                'ID',
                'PRODUCT_ID',
                'NAME',
                'IBLOCK_ELEMENT.NAME',
                'QUANTITY',
                'PRICE',
                'VAT_RATE_PRC',
                'SUMMARY_PRICE'
            )
        );

        // Append user fields
        self::prepareUFInfo();
        if (is_array(self::$ufInfo) && count(self::$ufInfo) > 0)
        {
            if (isset(self::$ufInfo['ORDER']) && is_array(self::$ufInfo['ORDER'])
                && count(self::$ufInfo['ORDER']) > 0)
            {
                foreach (self::$ufInfo['ORDER'] as $ufKey => $uf)
                {
                    if ($uf['USER_TYPE_ID'] !== 'datetime' || $uf['MULTIPLE'] === 'Y'
                        || substr($ufKey, -strlen(self::UF_DATETIME_SHORT_POSTFIX)) === self::UF_DATETIME_SHORT_POSTFIX)
                    {
                        $columnList[] = $ufKey;
                    }
                }
            }
        }

        return $columnList;
    }

    public static function setRuntimeFields(\Bitrix\Main\Entity\Base $entity, $sqlTimeInterval)
    {
        global $DB, $DBType;

        $options = array();

        Crm\InvoiceTable::processQueryOptions($options);

        $entity->addField(array(
            'data_type' => 'float',
            'expression' => array(
                'CASE WHEN %s IN '.$options['WORK_STATUS_IDS'].' THEN %s ELSE 0 END',
                'STATUS_ID', 'PRICE'
            ),
            'values' => array(0, 1)
        ), 'PRICE_WORK');

        $entity->addField(array(
            'data_type' => 'float',
            'expression' => array(
                'CASE WHEN %s IN '.$options['CANCEL_STATUS_IDS'].' THEN %s ELSE 0 END',
                'STATUS_ID', 'PRICE'
            ),
            'values' => array(0, 1)
        ), 'PRICE_CANCELED');

        $entity->addField(array(
            'data_type' => 'boolean',
            'expression' => array(
                'CASE WHEN %s IN '.$options['WORK_STATUS_IDS'].' THEN 1 ELSE 0 END',
                'STATUS_ID'
            ),
            'values' => array(0, 1)
        ), 'IS_WORK');

        $entity->addField(array(
            'data_type' => 'boolean',
            'expression' => array(
                'CASE WHEN %s IN '.$options['CANCEL_STATUS_IDS'].' THEN 1 ELSE 0 END',
                'STATUS_ID'
            ),
            'values' => array(0, 1)
        ), 'IS_CANCELED');

        $datetimeNull = (ToUpper($DBType) === 'MYSQL') ? 'CAST(NULL AS DATETIME)' : 'NULL';

        $entity->addField(array(
            'data_type' => 'datetime',
            'expression' => array(
                'CASE WHEN %s = \'P\' THEN '.$DB->datetimeToDateFunction($DB->IsNull('%s', '%s')).
                ' WHEN %s IN '.$options['CANCEL_STATUS_IDS'].' THEN '.$DB->datetimeToDateFunction($DB->IsNull('%s', '%s')).
                ' ELSE '.$datetimeNull.' END',
                'STATUS_ID', 'PAY_VOUCHER_DATE', 'DATE_INSERT',
                'STATUS_ID', 'DATE_MARKED', 'DATE_INSERT'
            )
        ), 'DATE_FINISHED_SHORT');

        self::appendDateTimeUserFieldsAsShort($entity);
        self::appendTextUserFieldsAsTrimmed($entity);
    }

    public static function getCustomSelectFields($select, $fList)
    {
        global $DBType;

        $customFields = array();

        $bAggr = false;
        foreach ($select as $elem)
        {
            if (isset($elem['aggr']) && !empty($elem['aggr']))
            {
                $bAggr = true;
                break;
            }
        }

        if ($bAggr)
        {
            $dbType = ToUpper(strval($DBType));

            if ($dbType === 'ORACLE' || $dbType === 'MSSQL')
            {
                foreach ($select as $k => $elem)
                {
                    $fName = $elem['name'];
                    $field = $fList[$fName];
                    $arUF = self::detectUserField($field);
                    if ($arUF['isUF'])
                    {
                        if ($arUF['ufInfo']['MULTIPLE'] === 'Y')
                        {
                            $customField = $elem;
                            $customField['name'] .= self::UF_TEXT_TRIM_POSTFIX;
                            $customFields[$k] = $customField;
                        }
                    }
                }
            }
        }

        return $customFields;
    }

    public static function getCustomColumnTypes()
    {
        return array(
            'STATUS_ID' => 'string',
            'PAY_SYSTEM_ID' => 'string',
            'PERSON_TYPE_ID' => 'string',
            'CURRENCY' => 'string'
        );
    }
    public static function getDefaultColumns()
    {
        return array(
            array('name' => 'ACCOUNT_NUMBER'),
            array('name' => 'ORDER_TOPIC'),
            array('name' => 'DATE_INS'),
            array('name' => 'STATUS_ID'),
            array('name' => 'ASSIGNED_BY.SHORT_NAME')
        );
    }
    public static function getCalcVariations()
    {
        return array_merge(
            parent::getCalcVariations(),
            array(
                'IS_WORK' => array('SUM'),
                'IS_CANCELED' => array('SUM'),
                'IS_PAYED' => array('SUM'),
                'InvoiceSpec:INVOICE.ID' => array('COUNT_DISTINCT', 'GROUP_CONCAT'),
                'InvoiceSpec:INVOICE.PRODUCT_ID' => array('COUNT_DISTINCT', 'GROUP_CONCAT'),
                'InvoiceSpec:INVOICE.NAME' => array('GROUP_CONCAT'),
                'InvoiceSpec:INVOICE.IBLOCK_ELEMENT.NAME' => array('GROUP_CONCAT')
            )
        );
    }
    public static function getCompareVariations()
    {
        return array_merge(
            parent::getCompareVariations(),
            array(
                'STATUS_ID' => array(
                    'EQUAL',
                    'NOT_EQUAL'
                ),
                'PAY_SYSTEM_ID' => array(
                    'EQUAL',
                    'NOT_EQUAL'
                ),
                'PERSON_TYPE_ID' => array(
                    'EQUAL',
                    'NOT_EQUAL'
                ),
                'CURRENCY' => array(
                    'EQUAL',
                    'NOT_EQUAL'
                ),
                'INVOICE_UTS.DEAL_BY' => array(
                    'EQUAL'
                ),
                'INVOICE_UTS.CONTACT_BY' => array(
                    'EQUAL'
                ),
                'INVOICE_UTS.COMPANY_BY' => array(
                    'EQUAL'
                )
            )
        );
    }
    public static function beforeViewDataQuery(&$select, &$filter, &$group, &$order, &$limit, &$options, &$runtime = null)
    {
        if(!isset($select['CRM_INVOICE_INVOICE_UTS_COMPANY_BY_ID']))
        {
            foreach($select as $k => $v)
            {
                if(strpos($k, 'CRM_INVOICE_INVOICE_UTS_COMPANY_BY_') === 0)
                {
                    $select['CRM_INVOICE_INVOICE_UTS_COMPANY_BY_ID'] = 'INVOICE_UTS.COMPANY_BY.ID';
                    break;
                }
            }
        }
        if(!isset($select['CRM_INVOICE_INVOICE_UTS_CONTACT_BY_ID']))
        {
            foreach($select as $k => $v)
            {
                if(strpos($k, 'CRM_INVOICE_INVOICE_UTS_CONTACT_BY_') === 0)
                {
                    $select['CRM_INVOICE_INVOICE_UTS_CONTACT_BY_ID'] = 'INVOICE_UTS.CONTACT_BY.ID';
                    break;
                }
            }
        }

        // HACK: Switch to order by SATTUS_BY.SORT instead STATUS_BY.STATUS_ID
        // We are trying to adhere user defined sort rules.
        if(isset($order['STATUS_ID']))
        {
            $select['CRM_INVOICE_STATUS_BY_SORT'] = 'STATUS_BY.SORT';
            $order['CRM_INVOICE_STATUS_BY_SORT'] = $order['STATUS_ID'];
            unset($order['STATUS_ID']);
        }

        if(!isset($select['CRM_INVOICE_INVOICE_UTS_CONTACT_BY_ID']))
        {
            foreach($select as $k => $v)
            {
                if(strpos($k, 'CRM_INVOICE_INVOICE_UTS_CONTACT_BY_') === 0)
                {
                    $select['CRM_INVOICE_INVOICE_UTS_CONTACT_BY_ID'] = 'INVOICE_UTS.CONTACT_BY.ID';
                    break;
                }
            }
        }

        if(!isset($select['CRM_INVOICE_INVOICE_UTS_COMPANY_BY_ID']))
        {
            foreach($select as $k => $v)
            {
                if(strpos($k, 'CRM_INVOICE_INVOICE_UTS_COMPANY_BY_') === 0)
                {
                    $select['CRM_INVOICE_INVOICE_UTS_COMPANY_BY_ID'] = 'INVOICE_UTS.COMPANY_BY.ID';
                    break;
                }
            }
        }

        // permission
        $addClause = CCrmInvoice::BuildPermSql('crm_invoice');
        if(!empty($addClause))
        {
            global $DB;
            // HACK: add escape chars for ORM
            $addClause = str_replace('crm_invoice.ID', $DB->escL.'crm_invoice'.$DB->escR.'.ID', $addClause);

            $filter = array($filter,
                '=IS_ALLOWED' => '1'
            );

            $runtime['IS_ALLOWED'] = array(
                'data_type' => 'integer',
                'expression' => array('CASE WHEN '.$addClause.' THEN 1 ELSE 0 END')
            );
        }
    }

    public static function formatResultValue($k, &$v, &$row, &$cInfo, $total, &$customChartValue = null)
    {
        // HACK: detect if 'report.view' component is rendering excel spreadsheet
        $isHtml = !(isset($_GET['EXCEL']) && $_GET['EXCEL'] === 'Y');

        $field = $cInfo['field'];
        $fieldName = isset($cInfo['fieldName']) ? $cInfo['fieldName'] : $field->GetName();
        $prcnt = isset($cInfo['prcnt']) ? $cInfo['prcnt'] : '';
        $aggr = (!empty($cInfo['aggr']) && $cInfo['aggr'] !== 'GROUP_CONCAT');

        if (!isset($prcnt[0])
            && ($k === 'PRICE'
                || preg_match('/.PRICE$/', $k)
                || $k === 'PRICE_WORK' || preg_match('/_PRICE_WORK$/', $k)
                || $k === 'PRICE_PAYED' || preg_match('/_PRICE_PAYED$/', $k)
                || $k === 'PRICE_CANCELED' || preg_match('/_PRICE_CANCELED/', $k)))
        {
            // unformatted value for charts
            $customChartValue['exist'] = true;
            $customChartValue['type'] = 'float';
            $customChartValue['value'] = doubleval($v);

            $v = self::MoneyToString(doubleval($v), $isHtml);
        }
        elseif (!isset($prcnt[0]) && preg_match('/_VAT_RATE_PRC$/', $k))
        {
            $v = number_format(doubleval($v), 2, '.', '');
            if ($isHtml)
                $v = str_replace(' ', '&nbsp;', $v);
        }
        elseif (!isset($prcnt[0]) && preg_match('/_QUANTITY$/', $k))
        {
            $v = round(doubleval($v), 4);
        }
        elseif(!$aggr && $fieldName === 'ORDER_TOPIC')
        {
            if($isHtml && strlen($v) > 0 && self::$CURRENT_RESULT_ROW && isset(self::$CURRENT_RESULT_ROW['ID']))
            {
                $v = self::prepareInvoiceTitleHtml(self::$CURRENT_RESULT_ROW['ID'], $v);
            }
        }
        elseif(!$aggr && $fieldName === 'PAY_SYSTEM_ID')
        {
            if($v !== '')
            {
                $personTypeId = intval(self::$CURRENT_RESULT_ROW['PERSON_TYPE_ID']);
                $v = self::getInvoicePaySystemName($v, $personTypeId, $isHtml);
            }
        }
        elseif(!$aggr && $fieldName === 'PERSON_TYPE_ID')
        {
            if($v !== '')
            {
                $v = self::getInvoicePersonTypeName($v, $isHtml);
            }
        }
        elseif(!$aggr && $fieldName === 'STATUS_ID')
        {
            if($v !== '')
            {
                $v = self::getInvoiceStatusName($v, $isHtml);
            }
        }
        elseif(!$aggr && $fieldName === 'CURRENCY')
        {
            if($v !== '')
            {
                $v = self::getCurrencyName($v, $isHtml);
            }
        }
        elseif(!$aggr && strpos($fieldName, 'ASSIGNED_BY.') === 0)
        {
            // unset HREF for empty value
            if (empty($v) || trim($v) === '.' || $v === '&nbsp;')
                unset($row['__HREF_'.$k]);
            if((strlen($v) === 0 || trim($v) === '.') && strpos($fieldName, '.WORK_POSITION') !== strlen($fieldName) - 14)
            {
                $v = GetMessage('CRM_INVOICE_RESPONSIBLE_NOT_ASSIGNED');
            }
            if($isHtml)
            {
                $v = htmlspecialcharsbx($v);
            }
        }
        elseif(!$aggr && strpos($fieldName, 'ProductRow:LEAD_OWNER.IBLOCK_ELEMENT.') === 0)
        {
            static $defaultCatalogID;
            if(!isset($defaultCatalogID))
            {
                $defaultCatalogID = CCrmCatalog::GetDefaultID();
            }

            if($isHtml)
            {
                if($defaultCatalogID > 0 && self::$CURRENT_RESULT_ROW)
                {
                    $iblockID = isset(self::$CURRENT_RESULT_ROW['CRM_LEAD_CRM_PRODUCT_ROW_LEAD_OWNER_IBLOCK_ELEMENT_IBLOCK_ID'])
                        ? intval(self::$CURRENT_RESULT_ROW['CRM_LEAD_CRM_PRODUCT_ROW_LEAD_OWNER_IBLOCK_ELEMENT_IBLOCK_ID']) : 0;;
                    $iblockElementID = isset(self::$CURRENT_RESULT_ROW['CRM_LEAD_CRM_PRODUCT_ROW_LEAD_OWNER_IBLOCK_ELEMENT_ID'])
                        ? intval(self::$CURRENT_RESULT_ROW['CRM_LEAD_CRM_PRODUCT_ROW_LEAD_OWNER_IBLOCK_ELEMENT_ID']) : 0;
                }
                else
                {
                    $iblockID = 0;
                    $iblockElementID = 0;
                }

                if($iblockElementID > 0 && $iblockID === $defaultCatalogID)
                {
                    $v = self::prepareProductNameHtml($iblockElementID, $v);
                }
                else
                {
                    $v = htmlspecialcharsbx($v);
                }
            }
        }
        elseif($fieldName !== 'COMMENTS') // Leave 'COMMENTS' as is for HTML display.
        {
            parent::formatResultValue($k, $v, $row, $cInfo, $total, $customChartValue);
        }
    }

    public static function formatResultsTotal(&$total, &$columnInfo, &$customChartTotal = null)
    {
        parent::formatResultsTotal($total, $columnInfo);

        // Suppress total values
        if (isset($total['TOTAL_PAY_SYSTEM_ID']))
        {
            unset($total['TOTAL_PAY_SYSTEM_ID']);
        }
        if (isset($total['TOTAL_PERSON_TYPE_ID']))
        {
            unset($total['TOTAL_PERSON_TYPE_ID']);
        }
        if (isset($total['TOTAL_CRM_INVOICE_CRM_INVOICE_SPEC_INVOICE_ID']))
        {
            unset($total['TOTAL_CRM_INVOICE_CRM_INVOICE_SPEC_INVOICE_ID']);
        }
        if (isset($total['TOTAL_CRM_INVOICE_CRM_INVOICE_SPEC_INVOICE_PRODUCT_ID']))
        {
            unset($total['TOTAL_CRM_INVOICE_CRM_INVOICE_SPEC_INVOICE_PRODUCT_ID']);
        }
        if (isset($total['TOTAL_CRM_INVOICE_CRM_INVOICE_SPEC_INVOICE_VAT_RATE_PRC']))
        {
            unset($total['TOTAL_CRM_INVOICE_CRM_INVOICE_SPEC_INVOICE_VAT_RATE_PRC']);
        }
    }

    public static function getPeriodFilter($date_from, $date_to)
    {
        if(is_null($date_from) && is_null($date_to))
        {
            return array();
        }

        $filter = array('LOGIC' => 'AND');
        if(!is_null($date_to))
        {
            $filter[] = array(
                'LOGIC' => 'OR',
                '<=DATE_BEGIN_SHORT' => $date_to,
                '=DATE_BEGIN_SHORT' => null
            );
        }

        if(!is_null($date_from))
        {
            $filter[] = array(
                'LOGIC' => 'OR',
                '>=DATE_FINISHED_SHORT' => $date_from,
                '=DATE_FINISHED_SHORT' => null
            );
        }

        return $filter;
    }

    public static function getDefaultReports()
    {
        IncludeModuleLangFile(__FILE__);

        $reports = array(
            '14.1.0' => array(
                array(
                    'title' => 'Отчет по количеству продаж и получению средств',
                    'description' => GetMessage('CRM_REPORT_DEFAULT_INVOICES_BY_MANAGER_DESCR'),
                    'mark_default' => 1,
                    'settings' => unserialize('a:10:{s:6:"entity";s:18:"Bitrix\Crm\Invoice";s:6:"period";a:2:{s:4:"type";s:5:"month";s:5:"value";N;}s:6:"select";a:5:{i:4;a:2:{s:4:"name";s:22:"ASSIGNED_BY.SHORT_NAME";s:5:"alias";s:0:"";}i:8;a:3:{s:4:"name";s:8:"IS_PAYED";s:5:"alias";s:0:"";s:4:"aggr";s:3:"SUM";}i:6;a:3:{s:4:"name";s:11:"PRICE_PAYED";s:5:"alias";s:0:"";s:4:"aggr";s:3:"SUM";}i:9;a:3:{s:4:"name";s:11:"IS_CANCELED";s:5:"alias";s:0:"";s:4:"aggr";s:3:"SUM";}i:7;a:3:{s:4:"name";s:14:"PRICE_CANCELED";s:5:"alias";s:0:"";s:4:"aggr";s:3:"SUM";}}s:6:"filter";a:1:{i:0;a:2:{i:0;a:5:{s:4:"type";s:5:"field";s:4:"name";s:11:"ASSIGNED_BY";s:7:"compare";s:5:"EQUAL";s:5:"value";s:0:"";s:10:"changeable";s:1:"1";}s:5:"LOGIC";s:3:"AND";}}s:4:"sort";i:6;s:9:"sort_type";s:4:"DESC";s:5:"limit";N;s:12:"red_neg_vals";b:0;s:13:"grouping_mode";b:0;s:5:"chart";a:4:{s:7:"display";b:0;s:4:"type";s:3:"bar";s:8:"x_column";i:4;s:9:"y_columns";a:1:{i:0;i:6;}}}')
                ),
                array(
                    'title' => 'Отчет по проданным товарам',
                    'description' => GetMessage('CRM_REPORT_DEFAULT_INVOICES_BY_COMPANY_DESCR'),
                    'mark_default' => 2,
                    'settings' => unserialize('a:10:{s:6:"entity";s:18:"Bitrix\Crm\Invoice";s:6:"period";a:2:{s:4:"type";s:5:"month";s:5:"value";N;}s:6:"select";a:3:{i:12;a:2:{s:4:"name";s:28:"INVOICE_UTS.COMPANY_BY.TITLE";s:5:"alias";s:0:"";}i:8;a:3:{s:4:"name";s:8:"IS_PAYED";s:5:"alias";s:0:"";s:4:"aggr";s:3:"SUM";}i:6;a:3:{s:4:"name";s:11:"PRICE_PAYED";s:5:"alias";s:0:"";s:4:"aggr";s:3:"SUM";}}s:6:"filter";a:1:{i:0;a:4:{i:0;a:5:{s:4:"type";s:5:"field";s:4:"name";s:25:"INVOICE_UTS.COMPANY_BY.ID";s:7:"compare";s:7:"GREATER";s:5:"value";s:1:"0";s:10:"changeable";s:1:"0";}i:1;a:5:{s:4:"type";s:5:"field";s:4:"name";s:11:"PRICE_PAYED";s:7:"compare";s:7:"GREATER";s:5:"value";s:1:"0";s:10:"changeable";s:1:"0";}i:2;a:5:{s:4:"type";s:5:"field";s:4:"name";s:11:"ASSIGNED_BY";s:7:"compare";s:5:"EQUAL";s:5:"value";s:0:"";s:10:"changeable";s:1:"1";}s:5:"LOGIC";s:3:"AND";}}s:4:"sort";i:6;s:9:"sort_type";s:4:"DESC";s:5:"limit";N;s:12:"red_neg_vals";b:0;s:13:"grouping_mode";b:0;s:5:"chart";a:4:{s:7:"display";b:0;s:4:"type";s:3:"bar";s:8:"x_column";i:12;s:9:"y_columns";a:1:{i:0;i:6;}}}')
                ),
				array(
                    'title' => 'Топ партнеров',
                    'description' => GetMessage('CRM_REPORT_DEFAULT_INVOICES_BY_COMPANY_DESCR'),
                    'mark_default' => 3,
                    'settings' => unserialize('a:10:{s:6:"entity";s:18:"Bitrix\Crm\Invoice";s:6:"period";a:2:{s:4:"type";s:5:"month";s:5:"value";N;}s:6:"select";a:3:{i:12;a:2:{s:4:"name";s:28:"INVOICE_UTS.COMPANY_BY.TITLE";s:5:"alias";s:0:"";}i:8;a:3:{s:4:"name";s:8:"IS_PAYED";s:5:"alias";s:0:"";s:4:"aggr";s:3:"SUM";}i:6;a:3:{s:4:"name";s:11:"PRICE_PAYED";s:5:"alias";s:0:"";s:4:"aggr";s:3:"SUM";}}s:6:"filter";a:1:{i:0;a:4:{i:0;a:5:{s:4:"type";s:5:"field";s:4:"name";s:25:"INVOICE_UTS.COMPANY_BY.ID";s:7:"compare";s:7:"GREATER";s:5:"value";s:1:"0";s:10:"changeable";s:1:"0";}i:1;a:5:{s:4:"type";s:5:"field";s:4:"name";s:11:"PRICE_PAYED";s:7:"compare";s:7:"GREATER";s:5:"value";s:1:"0";s:10:"changeable";s:1:"0";}i:2;a:5:{s:4:"type";s:5:"field";s:4:"name";s:11:"ASSIGNED_BY";s:7:"compare";s:5:"EQUAL";s:5:"value";s:0:"";s:10:"changeable";s:1:"1";}s:5:"LOGIC";s:3:"AND";}}s:4:"sort";i:6;s:9:"sort_type";s:4:"DESC";s:5:"limit";N;s:12:"red_neg_vals";b:0;s:13:"grouping_mode";b:0;s:5:"chart";a:4:{s:7:"display";b:0;s:4:"type";s:3:"bar";s:8:"x_column";i:12;s:9:"y_columns";a:1:{i:0;i:6;}}}')
                )
				
            )
        );

        foreach ($reports as &$reportByVersion)
        {
            foreach ($reportByVersion as &$report)
            {
                if ($report['mark_default'] === 1)
                {
                    $report['settings']['select'][4]['alias'] = GetMessage('CRM_REPORT_DEFAULT_INVOICES_BY_MANAGER_ALIAS_4');
                    $report['settings']['select'][6]['alias'] = GetMessage('CRM_REPORT_DEFAULT_INVOICES_BY_MANAGER_ALIAS_6');
                    $report['settings']['select'][7]['alias'] = GetMessage('CRM_REPORT_DEFAULT_INVOICES_BY_MANAGER_ALIAS_7');
                    $report['settings']['select'][8]['alias'] = GetMessage('CRM_REPORT_DEFAULT_INVOICES_BY_MANAGER_ALIAS_8');
                    $report['settings']['select'][9]['alias'] = GetMessage('CRM_REPORT_DEFAULT_INVOICES_BY_MANAGER_ALIAS_9');
                }
                else if ($report['mark_default'] === 2)
                {
                    $report['settings']['select'][6]['alias'] = GetMessage('CRM_REPORT_DEFAULT_INVOICES_BY_COMPANY_ALIAS_6');
                    $report['settings']['select'][8]['alias'] = GetMessage('CRM_REPORT_DEFAULT_INVOICES_BY_COMPANY_ALIAS_8');
                    $report['settings']['select'][12]['alias'] = GetMessage('CRM_REPORT_DEFAULT_INVOICES_BY_COMPANY_ALIAS_12');
                }
            }
            unset($report);
        }
        unset($reportByVersion);
        var_dump($reports);

        return $reports;
    }

    public static function getFirstVersion()
    {
        return '14.0.0';
    }

    public static function getDefaultElemHref($elem, $fList)
    {
        $href = '';
        if (empty($elem['aggr']))
        {
            $field = $fList[$elem['name']];

            if ($field->getEntity()->getName() == 'User')
            {
                if (in_array($elem['name'], array(
                    'ASSIGNED_BY.SHORT_NAME'), true))
                {
                    $strID = str_replace('.SHORT_NAME', '.ID', $elem['name']);
                    $href = array('pattern' => '/company/personal/user/#'.$strID.'#/');
                }
            }
            else if ($field->getEntity()->getName() == 'Deal')
            {
                if (in_array($elem['name'], array(
                    'INVOICE_UTS.DEAL_BY.TITLE'), true))
                {
                    $href = array('pattern' => '/crm/deal/show/#INVOICE_UTS.DEAL_BY.ID#/');
                }
            }
            else if ($field->getEntity()->getName() == 'Company')
            {
                if (in_array($elem['name'], array(
                    'INVOICE_UTS.COMPANY_BY.TITLE'), true))
                {
                    $href = array('pattern' => '/crm/company/show/#INVOICE_UTS.COMPANY_BY.ID#/');
                }
            }
            else if ($field->getEntity()->getName() == 'Contact')
            {
                if (in_array($elem['name'], array(
                    'INVOICE_UTS.CONTACT_BY.SHORT_NAME'), true))
                {
                    $href = array('pattern' => '/crm/contact/show/#INVOICE_UTS.CONTACT_BY.ID#/');
                }
            }
        }

        return $href;
    }
}
?>