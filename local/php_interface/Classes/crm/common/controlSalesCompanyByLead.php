<?
/*
 * Метод для формирования данных “Контроль продаж компании” для страницы start раздела crm
 */


use Bitrix\Crm\Widget\Filter;
use Bitrix\Crm\Widget\FilterPeriodType;
use Bitrix\Main\Type\Date;
use Bitrix\Crm;
class controlSalesCompanyByLead {
    protected $arrLeads = null;
    protected $arrStatusList = null;
    protected $arrSourceList = null;
    protected $arLeadsConvert = null;
    protected $arrKKTs = null;
    protected $arrKKTCompany = null;
    protected $arrKKTDeal = null;
    protected $arrCompanys = null;
    protected $arrDeals = null;
    protected $periodTypeID = null;
    protected $periodStart = null;
    protected $periodEnd = null;
    protected $arINN_Source = null;
    protected $notLeadsFromKKTCount = null;
    protected $LeadsFromKKTCount = null;
    protected $selCompanyCount = null;
    protected $periodTypeString = null;
    protected $year = null;
    protected $month = null;
    protected $quarter = null;
    protected $newElemID = null;
    protected $arElemData = null;
    protected $arSourceSort = null;
    protected $addedRNM_KKT = null;
    protected $arPeriodIgnorePeriodEnd = null;

    public function __construct($periodTypeString='D30'){
        $this->periodTypeString = $periodTypeString;
        $this->arPeriodIgnorePeriodEnd = array(
            "D0",
            "D7",
            "D30",
            "D60",
            "D90",
            "Q0",
            "M0",
            "BEFORE",
            "YS",
            "W0",
            "LN",
            "WL",
            "ML",
        );

        $arPeriodType = explode('-', $periodTypeString);
        $arFilter = array();
        if(sizeof($arPeriodType)==1) {
            $arFilter = array(
                'periodType' => $arPeriodType[0],
            );
        } else {
            if($arPeriodType[0]=="Y"){
                $arFilter = array(
                    'periodType' => $arPeriodType[0],
                    'year' => $arPeriodType[1],
                );
            }
            if($arPeriodType[0]=="Q"){
                $arFilter = array(
                    'periodType' => $arPeriodType[0],
                    'year' => $arPeriodType[1],
                    'quarter' => $arPeriodType[2],
                );
            }
            if($arPeriodType[0]=="M"){
                $arFilter = array(
                    'periodType' => $arPeriodType[0],
                    'year' => $arPeriodType[1],
                    'month' => $arPeriodType[2],
                );
            }
            if($arPeriodType[0]=="LN"
            || $arPeriodType[0]=="SD"
                || $arPeriodType[0]=="PD"){
                $arFilter = array(
                    'periodType' => $arPeriodType[0],
                );
            }
        }

        $el = new CIBlockElement();
        $arFilterStat = array(
            "IBLOCK_ID"=>GetIBlockIDByCode("statistic_sale_control"),
            "ACTIVE"=>"Y",
            "PROPERTY_PERIOD_TYPE_TEXT"=>$periodTypeString,
        );
        $rsStat = $el->GetList(
            array("ID"=>"DESC"),
            $arFilterStat,
            false,
            false,
            array("ID", "NAME", "PROPERTY_LINK_TABLE", "PROPERTY_STAT_FROM_DAY")
        );
        //echo '$arFilter[] = <pre>'.print_r($arFilter, true).'</pre><br/>';
        $filter = new Filter($arFilter);
        $arFilterPeriod = array();
        $arFilterPeriod = $filter->getPeriod();
        $this->year = $filter->getYear();
        $this->month = $filter->getMonth();
        $this->quarter = $filter->getQuarter();
        /*$endPeriod = new DateTime($arFilterPeriod["END"]->format("d.m.Y"));
        $endPeriod->modify("+1 day");*/
        $this->setPeriodTypeID($filter->getPeriodTypeID());
        $this->setPeriodStart($arFilterPeriod["START"]->format("d.m.Y") . ' 00:00:00');
        $this->setPeriodEnd($arFilterPeriod["END"]->format("d.m.Y") . ' 23:59:59');

        if("SD" != $filter->getPeriodTypeID())
        {
            if($rsStat->SelectedRowsCount()>0){
                $arStat = $rsStat->GetNext();
                if($arStat["PROPERTY_STAT_FROM_DAY_VALUE"]==date("d.m.Y")){
                    $this->arElemData = $arStat["PROPERTY_LINK_TABLE_VALUE"];
                } else {
                    $this->setLeads();
                    $this->setKKT();
                    $this->setElemData($arStat["ID"]);
                }
            } else {
                $this->setLeads();
                $this->setKKT();
                $this->setElemData();
            }
        }
        else
        {
            if($rsStat->SelectedRowsCount()>0){
                $arStat = $rsStat->GetNext();
                $this->setLeads();
                $this->setKKT();
                $this->setElemData($arStat["ID"]);
            } else {
                $this->setLeads();
                $this->setKKT();
                $this->setElemData();
            }
        }

    }

    public function setPeriodTypeID($periodTypeID){
        $this->periodTypeID = $periodTypeID;
    }

    public function getPeriodTypeID(){
        return $this->periodTypeID;
    }

    public function setPeriodStart($periodStart){
        $this->periodStart = $periodStart;
    }

    public function getPeriodStart(){
        return $this->periodStart;
    }

    public function setPeriodEnd($periodEnd){
        $this->periodEnd = $periodEnd;
    }

    public function getPeriodEnd(){
        return $this->periodEnd;
    }

    public function getFilterParams(){
        $arFilter = Array(
            '>=DATE_CREATE' => $this->getPeriodStart(),
            '<=DATE_CREATE' => $this->getPeriodEnd(),
            'ACTIVE' => "Y",
        );
        $arPeriodIgnorePeriodEnd = array(
            "D0",
            "D7",
            "D30",
            "D60",
            "D90",
            "Q0",
            "M0",
            "BEFORE",
            "YS",
            "W0",
            "LN",
            "WL",
            "ML",
        );
        if(in_array($this->getPeriodTypeID(), $arPeriodIgnorePeriodEnd)){
            unset($arFilter['<=DATE_CREATE']);
        }
        return $arFilter;
    }

    public function getLeads(){
        return $this->arrLeads;
    }

    public function getLeadsConvert(){
        return $this->arLeadsConvert;
    }

    public function setLeads(){
        if (CModule::IncludeModule('crm')){
            $this->arrStatusList = CCrmStatus::GetStatusList('STATUS');
            $this->arrSourceList = CCrmStatus::GetStatusList('SOURCE');
            global $DB;
            $filterDateStart = $DB->CharToDateFunction($this->getPeriodStart());
            $filterDateEnd = $DB->CharToDateFunction($this->getPeriodEnd());
            $filterPeriod = "b_crm_lead.DATE_CREATE>=$filterDateStart";
            if(!in_array($this->getPeriodTypeID(), $this->arPeriodIgnorePeriodEnd)){
                $filterPeriod .= "AND b_crm_lead.DATE_CREATE<=$filterDateEnd";
            }
            $arSourceSort = array();
            $arLeads = array();
            $arLeadsConvert = array();
            $arLeadsConvert["STATUS_LIST"] = $this->arrStatusList;
            $arLeadsConvert["SOURCE_LIST"] = $this->arrSourceList;
            foreach($arLeadsConvert["SOURCE_LIST"] as $sourceID => $sourceText){
                $arLeadsConvert["SOURCE_KKT"][$sourceID] = array(
                    "KKT_COUNT" => 0,
                    "KKT_SUMM" => 0,
                );
                $arSourceSort[$sourceID] = 0;
            }
            $arINN_Source = array();
            //$arFilter["!UF_CRM_1499414186"]=false;
            $strSql = "SELECT 
                b_crm_lead.ID,
                b_crm_lead.STATUS_ID,
                b_crm_lead.SOURCE_ID,
                b_crm_lead.DATE_CREATE,
                b_uts_crm_lead.UF_CRM_1499414186
              FROM 
                b_crm_lead, 
                b_uts_crm_lead
              WHERE 
                $filterPeriod AND
                b_crm_lead.ID = b_uts_crm_lead.VALUE_ID
              ORDER BY b_crm_lead.DATE_CREATE ASC";
            //echo $strSql."\n\r";
            $err_mess = "<br>LEAD <br>Line: ";
            $START_TIME_ONE = mktime();
            //$chanelInfo = new controlSalesCompanyByLead($enterPeriod);
            $rsLeads = $DB->Query($strSql, false, $err_mess.__LINE__);
            $TIME_ONE = mktime() - $START_TIME_ONE;
            echo "\n\rЗапрос лидов SQL [".$this->getPeriodTypeID()."], сек: $TIME_ONE \n";
            $START_TIME_ONE = mktime();
            $leadsCount = $rsLeads->SelectedRowsCount();
            //echo '$dbLeads->SelectedRowsCount() = '.$dbLeads->SelectedRowsCount().'<br />';
            while($arLead = $rsLeads->Fetch()){
                $sourceID = $arLead["SOURCE_ID"];
                $statusID = $arLead["STATUS_ID"];
                $inn = $arLead["UF_CRM_1499414186"];
                $arLeads[$arLead["ID"]] = array(
                    "SOURCE_ID"=>$sourceID,
                    "STATUS_ID"=>$statusID,
                    "INN"=>$inn,
                );
                $arSourceSort[$sourceID]++;
                if($inn!=''&&($sourceID!='OTHER'||$sourceID!='36')) {
                    if(!isset($arINN_Source[$inn][$sourceID])){
                        $arINN_Source[$inn][$sourceID] = 1;
                    } else {
                        $arINN_Source[$inn][$sourceID]++;
                    }
                }
                if(!isset($arLeadsConvert["LEADS"][$sourceID][$statusID])){
                    $arLeadsConvert["LEADS"][$sourceID][$statusID]["LEADS_COUNT"] = 1;
                } else {
                    $arLeadsConvert["LEADS"][$sourceID][$statusID]["LEADS_COUNT"]++;
                }
                if(!isset($arLeadsConvert["LEADS_COUNT"][$statusID]))
                    $arLeadsConvert["LEADS_COUNT"][$statusID] = 0;
                $arLeadsConvert["LEADS_COUNT"][$statusID]++;
            }
            $TIME_ONE = mktime() - $START_TIME_ONE;
            echo "\n\rОбработка $leadsCount лидов [".$this->getPeriodTypeID()."], сек: $TIME_ONE \n";
            arsort($arSourceSort);
            //echo '<pre style="display: none;">$arINN_Source'.print_r($arINN_Source, true).'</pre>';
            //echo '<pre>$arLeadsConvert = '.print_r($arLeadsConvert, true).'</pre>';
            $this->arSourceSort = $arSourceSort;
            $this->arLeadsConvert = $arLeadsConvert;
            $this->arrLeads = $arLeads;
            $this->arINN_Source = $arINN_Source;
        }
    }

    public function setKKT(){
        //echo "setKKT\r\n";
        if (CModule::IncludeModule('iblock')) {
            global $DB;
            $kktIBlockID = GetIBlockIDByCode("registry_kkt");
            $propCompanyID = 0;
            $propCostID = 0;
            $propRNM_KKT_ID = 0;
            $strSql = "SELECT
                  ID, CODE
                FROM
                  b_iblock_property
                WHERE
                  (CODE = 'COST_TARIF' OR CODE = 'COMPANY' OR CODE = 'RNM_KKT') AND IBLOCK_ID=$kktIBlockID
            ";
            $START_TIME_ONE = mktime();
            $rsProp = $DB->Query($strSql, false, $err_mess.__LINE__);
            $TIME_ONE = mktime() - $START_TIME_ONE;
            echo "\n\rЗапрос свойств ИБ ККТ [".$this->getPeriodTypeID()."], сек: $TIME_ONE \n";
            while($arProp = $rsProp->Fetch()){
                if($arProp["CODE"]=="COMPANY"){
                    $propCompanyID = $arProp["ID"];
                }
                if($arProp["CODE"]=="COST_TARIF"){
                    $propCostID = $arProp["ID"];
                }
                if($arProp["CODE"]=="RNM_KKT"){
                    $propRNM_KKT_ID = $arProp["ID"];
                }
            }
            //echo '$propCompanyID = '.$propCompanyID.' | $propCostID = '.$propCostID.' | $propRNM_KKT_ID = '.$propRNM_KKT_ID.'<br />';
            //echo '<hr />';
            if($propCompanyID>0&&$propCostID>0&&$propRNM_KKT_ID>0){
                $filterDateStart = $DB->CharToDateFunction($this->getPeriodStart());
                $filterDateEnd = $DB->CharToDateFunction($this->getPeriodEnd());
                $filterPeriod = "b_iblock_element.DATE_CREATE>=$filterDateStart";
                if(!in_array($this->getPeriodTypeID(), $this->arPeriodIgnorePeriodEnd)){
                    $filterPeriod .= "AND b_iblock_element.DATE_CREATE<=$filterDateEnd";
                }
                $strSql = "SELECT
                    b_iblock_element.ID,
                    b_iblock_element.DATE_CREATE,
                    b_iblock_element_prop_s$kktIBlockID.PROPERTY_$propCompanyID,
                    b_iblock_element_prop_s$kktIBlockID.PROPERTY_$propCostID,
                    b_iblock_element_prop_s$kktIBlockID.PROPERTY_$propRNM_KKT_ID,
                    b_crm_requisite.RQ_INN
                  FROM
                    b_iblock_element,
                    b_iblock_element_prop_s$kktIBlockID,
                    b_crm_requisite
                  WHERE
                    b_iblock_element.IBLOCK_ID=$kktIBlockID AND
                    $filterPeriod AND
                    b_iblock_element_prop_s$kktIBlockID.IBLOCK_ELEMENT_ID=b_iblock_element.ID AND 
                    b_crm_requisite.ENTITY_TYPE_ID = 4 AND 
                    b_crm_requisite.ENTITY_ID = b_iblock_element_prop_s$kktIBlockID.PROPERTY_$propCompanyID
                  ORDER BY b_iblock_element.DATE_CREATE ASC";
                $START_TIME_ONE = mktime();
                $rsKKT = $DB->Query($strSql, false, $err_mess.__LINE__);
                $TIME_ONE = mktime() - $START_TIME_ONE;
                echo "\n\rЗапрос ККТ SQL [".$this->getPeriodTypeID()."], сек: $TIME_ONE \n";
                $arKKTS = array();
                $selKKTCount = $rsKKT->SelectedRowsCount();
                //echo '$rsKKT->SelectedRowsCount() = '.$rsKKT->SelectedRowsCount().'<br />';
                $KKT_SUMM = 0;
                $LeadsFromKKTCount = 0;
                $arINN_Source = $this->arINN_Source;
                $arLeadsConvert = $this->arLeadsConvert;
                $addedRNM_KKT = array();
                $START_TIME_ONE = mktime();
                while($arKKT = $rsKKT->Fetch()){
                    //if (!in_array($arKKT["PROPERTY_$propRNM_KKT_ID"], $addedRNM_KKT)) {
                        $inn = $arKKT["RQ_INN"];
                        $kktPrice = $arKKT["PROPERTY_$propCostID"];
                        if (isset($arINN_Source[$inn]) && $inn != '') {
                            //echo "[" . $kktID . "] =>" . $inn . " | ".$kktPrice."\r\n";
                            //$sourceID = $arINN_Source[$inn][0];
                            foreach ($arINN_Source[$inn] as $sourceID => $leadsCount) {
                                $arLeadsConvert["SOURCE_KKT"][$sourceID]["KKT_COUNT"]++;
                                $arLeadsConvert["SOURCE_KKT"][$sourceID]["KKT_SUMM"] += $kktPrice;
                                break;
                            }
                            $LeadsFromKKTCount++;
                            $KKT_SUMM += $kktPrice;
                        }
                        //$addedRNM_KKT[] = $arKKT["PROPERTY_$propRNM_KKT_ID"];
                    //}
                }
                $TIME_ONE = mktime() - $START_TIME_ONE;
                echo "\n\rОБработка $selKKTCount ККТ [".$this->getPeriodTypeID()."], сек: $TIME_ONE \n";
                $arLeadsConvert["KKT_COUNT"] += $LeadsFromKKTCount;
                $arLeadsConvert["KKT_SUMM"] += $KKT_SUMM;
            }
            $this->arLeadsConvert = $arLeadsConvert;
            //echo "$this->periodTypeID page = $page\n\r";
            /*$nPageSize = 5000;
            $el = new CIBlockElement();
            $arFilter = $this->getFilterParams();
            $arFilter["IBLOCK_ID"] = GetIBlockIDByCode('registry_kkt');
            $arFilter["!PROPERTY_DEAL"] = false;
            //echo '<pre>$arFilter = '.print_r($arFilter, true).'</pre><br />';
            //Подсчитываем количество и сумму ККТ
            $dbKKT = $el->GetList(
                array("ID" => "DESC"),
                $arFilter,
                false,
                array("nPageSize" => $nPageSize, "iNumPage" => $page), //false,
                array("ID", "NAME", "PROPERTY_DEAL", "PROPERTY_COMPANY", "PROPERTY_RNM_KKT", "PROPERTY_COST_TARIF")
            );
            $arLeadsConvert = $this->arLeadsConvert;
            $arKKTs = array();
            $arSelCompanysID = array();
            $arSelDealsID = array();
            if (!is_array($this->addedRNM_KKT)){
                $addedRNM_KKT = array();
            } else {
                $addedRNM_KKT = $this->addedRNM_KKT;
            }
            //echo '$dbKKT->SelectedRowsCount() ='.$dbKKT->SelectedRowsCount().'<br />';
            $allKKTCount = $dbKKT->SelectedRowsCount();
            if($allKKTCount>0) {
                $this->arrKKTCompany = array();
                $this->arrKKTDeal = array();
                while ($arKKT = $dbKKT->GetNext()) {
                    if (!in_array($arKKT["PROPERTY_RNM_KKT_VALUE"], $addedRNM_KKT)) {
                        $companyID = intval($arKKT["PROPERTY_COMPANY_VALUE"]);
                        $dealID = intval($arKKT["PROPERTY_DEAL_VALUE"]);
                        $kktPrice = $arKKT["PROPERTY_COST_TARIF_VALUE"] > 0 ? $arKKT["PROPERTY_COST_TARIF_VALUE"] : 0;
                        //echo '$kktPrice для ККТ['.$arKKT["ID"].'] = '.$kktPrice.'<br />';
                        $arKKTs[$arKKT["ID"]] = array(
                            "COMPANY_ID" => $companyID,
                            "DEAL_ID" => $dealID,
                            "INN" => '',
                            "SUMM" => $kktPrice,
                        );
                        if (!in_array($companyID, $arSelCompanysID) && $companyID > 0) {
                            $arSelCompanysID[] = $companyID;
                        }
                        if (!in_array($dealID, $arSelDealsID) && $dealID > 0) {
                            $arSelDealsID[] = $dealID;
                        }
                        $this->setLinkKKT($arKKT["ID"], $companyID, $dealID);
                        $addedRNM_KKT[] = $arKKT["PROPERTY_RNM_KKT_VALUE"];
                    }
                }
                $this->arLeadsConvert = $arLeadsConvert;
                $this->arrKKTs = $arKKTs;
                $this->addedRNM_KKT = $addedRNM_KKT;
                $this->setCompany($arSelCompanysID);
                //$this->setDeal($arSelDealsID);
                if($allKKTCount>($nPageSize*$page)){
                    $page++;
                    $this->setKKT($page);
                }
            }*/
        }
    }

    public function setLinkKKT($kktID, $companyID, $dealID){
        $this->arrKKTCompany[$kktID] = $companyID;
        $this->arrKKTDeal[$kktID] = $dealID;
    }

    public function getKKT(){
        return $this->arrKKTs;
    }

    public function setCompany(array $arSelCompanysID){
        //echo "setCompany\r\n";
        if (CModule::IncludeModule('crm')) {
            if (sizeof($arSelCompanysID) > 0) {
                $arINN_Source = $this->arINN_Source;
                $arLeadsConvert = $this->arLeadsConvert;
                $KKT_SUMM = 0;
                $LeadsFromKKTCount = 0;
                $req = new \Bitrix\Crm\EntityRequisite();
                $rs = $req->getList([
                    "filter" => [
                        "ENTITY_ID" => $arSelCompanysID,
                        "ENTITY_TYPE_ID" => CCrmOwnerType::Company,
                    ]
                ]);
                $reqData = $rs->fetchAll();
                //echo '<pre>$reqData = '.print_r($reqData, true).'</pre>';
                foreach($reqData as $arReq){
                    if($arReq["RQ_INN"]!=''){
                        $CompanyID = $arReq["ENTITY_ID"];
                        $inn = $arReq["RQ_INN"];
                        $arTempKKTsID = array_keys($this->arrKKTCompany, $CompanyID);
                        //echo '<pre style="display: none">$arTempKKTsID ='.print_r($arTempKKTsID, true).'</pre>';
                        foreach ($arTempKKTsID as $kktID) {
                            $this->arrKKTs[$kktID]["INN"] = $inn;
                            $kktPrice = $this->arrKKTs[$kktID]["SUMM"];
                            if (isset($arINN_Source[$inn])) {
                                //echo "[" . $kktID . "] =>" . $inn . " | ".$kktPrice."\r\n";
                                //$sourceID = $arINN_Source[$inn][0];
                                foreach($arINN_Source[$inn] as $sourceID=>$leadsCount) {
                                    $arLeadsConvert["SOURCE_KKT"][$sourceID]["KKT_COUNT"]++;
                                    $arLeadsConvert["SOURCE_KKT"][$sourceID]["KKT_SUMM"] += $kktPrice;
                                    break;
                                }
                                $LeadsFromKKTCount++;
                                $KKT_SUMM += $kktPrice;
                            }
                        }
                    }
                }
                $arLeadsConvert["KKT_COUNT"] += $LeadsFromKKTCount;
                $arLeadsConvert["KKT_SUMM"] += $KKT_SUMM;
                $this->arLeadsConvert = $arLeadsConvert;
            }
        }
    }

    public function getSelCompanyCount () {
        return $this->selCompanyCount;
    }

    public function setDeal($arSelDealsID){
        if (CModule::IncludeModule('crm')) {
            if (sizeof($arSelDealsID) > 0) {
                $CCrmDeal = new CCrmDeal();
                $dbDeal = $CCrmDeal->GetList(
                    array("ID" => "DESC"),
                    array("ID" => $arSelDealsID, ">OPPORTUNITY" => 0),
                    array("ID", "OPPORTUNITY")
                );
                $kktSumm = 0;
                $arINN_Source = $this->arINN_Source;
                $arLeadsConvert = $this->arLeadsConvert;
                while ($arDeal = $dbDeal->GetNext()) {
                    $summ = $arDeal["OPPORTUNITY"];
                    $arTempKKTsID = array_keys($this->arrKKTDeal, $arDeal["ID"]);
                    foreach ($arTempKKTsID as $kktID) {
                        $this->arrKKTs[$kktID]["SUMM"]=$summ;
                        $kktSumm+=$summ;
                        $inn = $this->arrKKTs[$kktID]["INN"];

                        if(isset($arINN_Source[$inn])&&$inn!=''){
                            //echo $inn.'<br />';
                            foreach($arINN_Source[$inn] as $sourceID){
                                $arLeadsConvert["SOURCE_KKT"][$sourceID]["KKT_SUMM"]+=$summ;
                            }
                        }
                    }
                }
                $arLeadsConvert["KKT_SUMM"] = $kktSumm;
                $this->arLeadsConvert = $arLeadsConvert;
            }
        }
    }

    public function getINNSource(){
        return $this->arINN_Source;
    }

    public function setElemData($elemID=0){
        $el = new CIBlockElement();
        $arLeadsConvert = $this->arLeadsConvert;
        $arLines = array();
        $START_TIME_ONE = mktime();
        $arLineText = 'Источники';
        foreach($arLeadsConvert["STATUS_LIST"] as $statusID => $statusText) {
            $arLineText .= "|$statusText";
        }
        $arLineText .= "|Кол-во ККТ|Сумма ККТ|";
        $arLines[] = $arLineText;

        $arLineText = sizeof($arLeadsConvert["LEADS"]).' из '.sizeof($arLeadsConvert["SOURCE_LIST"]);
        foreach($arLeadsConvert["STATUS_LIST"] as $statusID => $statusText) {
            $arLineText .= isset($arLeadsConvert["LEADS_COUNT"][$statusID])?'|'.$arLeadsConvert["LEADS_COUNT"][$statusID]:'|0';
        }
        $arLineText .= '|'.$arLeadsConvert["KKT_COUNT"].'|'.CurrencyFormat($arLeadsConvert["KKT_SUMM"], "RUB").'|';
        $arLines[] = $arLineText;

        $itemLeads = $arLeadsConvert["LEADS"];
        $arSourceSort = $this->arSourceSort;
        foreach($arSourceSort as $sourceID => $leadCount):
            $arLineText = $arLeadsConvert["SOURCE_LIST"][$sourceID].' ('.$leadCount.')';
            foreach($arLeadsConvert["STATUS_LIST"] as $statusID => $statusText):
                $leadsCount = isset($itemLeads[$sourceID][$statusID])?'|'.$itemLeads[$sourceID][$statusID]["LEADS_COUNT"]:'|0';
                $arLineText .= $leadsCount;
            endforeach;
            $arLineText .= isset($arLeadsConvert["SOURCE_KKT"][$sourceID])?'|'.$arLeadsConvert["SOURCE_KKT"][$sourceID]["KKT_COUNT"]:'|н/д';
            $arLineText .= isset($arLeadsConvert["SOURCE_KKT"][$sourceID])?'|'.CurrencyFormat($arLeadsConvert["SOURCE_KKT"][$sourceID]["KKT_SUMM"], "RUB"):'|н/д';
            $arLineText .= '|';
            if($leadCount==0){
                $arLineText .= 'hidden';
            }
            $arLines[] = $arLineText;
        endforeach;
        $arLines = $arLines;
        $TIME_ONE = mktime() - $START_TIME_ONE;
        echo "\n\rФормирование данных для записи в ИБ [".$this->getPeriodTypeID()."], сек: $TIME_ONE \n";

        $START_TIME_ONE = mktime();
        if(intval($elemID)>0){
            $el->SetPropertyValuesEx(
                $elemID,
                GetIBlockIDByCode("statistic_sale_control"),
                array("LINK_TABLE" => $arLines, "STAT_FROM_DAY"=>date("d.m.Y"))
            );
            $arElemData = $arLines;
            //echo '$elemID['.$elemID.'] = информация обновлена<br />';
        } else {
            $PROP = array();
            $PROP["PERIOD_TYPE"] = $this->getPeriodTypeID();
            $PROP["YEAR"] = $this->year;
            $PROP["QUARTER"] = $this->quarter;
            $PROP["MONTH"] = $this->month;
            $PROP["LINK_TABLE"] = $arLines;
            $PROP["STAT_FROM_DAY"] = date("d.m.Y");
            $PROP["PERIOD_TYPE_TEXT"] = $this->periodTypeString;

            $arLoadProductArray = Array(
                "IBLOCK_ID"      => GetIBlockIDByCode("statistic_sale_control"),
                "PROPERTY_VALUES"=> $PROP,
                "NAME"           => "Статистика ".$this->periodTypeString,
                "ACTIVE"         => "Y",
            );

            if($PRODUCT_ID = $el->Add($arLoadProductArray)) {
                $arElemData = $arLines;
            } else {
                $arElemData = $el->LAST_ERROR;
            }
        }
        $TIME_ONE = mktime() - $START_TIME_ONE;
        echo "\n\rЗапись данных в ИБ [".$this->getPeriodTypeID()."], сек: $TIME_ONE \n";
        $this->arElemData = $arElemData;
    }
    public function getElemData(){
        return $this->arElemData;
    }
}?>
