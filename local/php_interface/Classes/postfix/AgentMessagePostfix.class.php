<?php
use Bitrix\Main\Loader;

Loader::includeModule("iblock");
class AgentMessagePostfix {

    var $errors = array(), //Массив ошибок
        $error = "",
        $status,
        $mbox, //Объект-ссылка на текущее IMAP соединение
        $emails, //Объект-массив с сообщениями
        $emails_del,//массив с сообщениями на удаление
        $msg_num = array(), //Массив с номерами всех соощений
        $messages = array(), //Стандартный массив с полными сообщениями
        $messagesText = array(), //Стандартный массив с текстами сообщений
        $arTenantCompanyType = array(
        "ОФД RU. Клиенты."                              => "CUSTOMER",
        "ОФД RU. Клиенты, анонсировавшие расторжение."  => "3",
        "ОФД RU. Бывшие клиенты."                       => "4",
        "ОФД RU. Бизнес Администрирование."             => "5",
        "Тензор"                                        => "6",
        "Рапкат"                                        => "7",
        "Без блокировки касс"                           => "8",
        "Агент"                                         => "PARTNER",
        "Временные пользователи"                        => "9",
        "Клиент + Агент"                                => "2",
        "Корпорации"                                    => "10",
        "Trashcan"                                      => "11",
    );

    const PDF_TEMPLATE_ID = 42;
    const PDF_SECTION_DISK_ID = 92876;
    const ORDERS_EDO_IBLOCK_ID = 100;
    const DEF_USER_ID_FROM_TASK_RESPONSIBLE = 396;
    const AUDITORS_DEPARTMENT_ID_FROM_TASK = 67;
    //const DOCX_TEMPLATE_ID = 7013646; //test
    const DOCX_TEMPLATE_ID = 7441754; //prod

    function AgentMessagePostfix()
    {
    }

    function parseOne($onemsg, $idPostfix, $entity_data_class) {
        if (substr_count($onemsg, 'Event Type NewLeadSer')) {
            if(!empty($this->checkInWork($idPostfix, $entity_data_class))){
                return false;
            }
            return $this->eventTypeNewLeadSer($onemsg);

        } elseif (substr_count($onemsg, 'NewLeadFN')) {
            if(!empty($this->checkInWork($idPostfix, $entity_data_class))){
                return false;
            }
            return $this->newLeadFN($onemsg);

        } elseif (substr_count($onemsg, 'Event Type NewLeadBD')) {
            if(!empty($this->checkInWork($idPostfix, $entity_data_class))){
                return false;
            }
            return $this->newLeadBD($onemsg);

        } elseif (substr_count($onemsg, 'Event Type NewLead')){
            if(!empty($this->checkInWork($idPostfix, $entity_data_class))){
                return false;
            }
            return $this->eventTypeNewLead($onemsg);


        } elseif (substr_count($onemsg, 'Temporary_account')) {
            if(!empty($this->checkInWork($idPostfix, $entity_data_class))){
                return false;
            }
            return $this->temporaryAccount($onemsg);

        }elseif (substr_count($onemsg, 'add_one_kkt')) {
            if(!empty($this->checkInWork($idPostfix, $entity_data_class))){
                return false;
            }
            return $this->addOneKKT($onemsg);

        } elseif (substr_count($onemsg, 'Full_account')) {
            if(!empty($this->checkInWork($idPostfix, $entity_data_class))){
                return false;
            }
            return $this->fullAccount($onemsg);

        } elseif (substr_count($onemsg, 'Activation_KKT')) {
            if(!empty($this->checkInWork($idPostfix, $entity_data_class))){
                return false;
            }
            return $this->activationKKT($onemsg);

        } elseif (substr_count($onemsg, 'Dectivation_KKT')) {
            if (!empty($this->checkInWork($idPostfix, $entity_data_class))) {
                return false;
            }
            return $this->deactivationKKT($onemsg);

        } elseif (substr_count($onemsg, 'kkt_tu_tariff')) {
            if(!empty($this->checkInWork($idPostfix, $entity_data_class))){
                return false;
            }
            return $this->kktTuTariff($onemsg);

        } elseif (substr_count($onemsg, 'TopUp_personal_accaunt')) {
            if (!empty($this->checkInWork($idPostfix, $entity_data_class))) {
                return false;
            }
            return $this->topUpPersonalAccount($onemsg);
        } elseif (substr_count($onemsg, 'Down_personal_accaunt')) {
            if (!empty($this->checkInWork($idPostfix, $entity_data_class))) {
                return false;
            }
            return $this->DownPersonalAccount($onemsg);
        } elseif (substr_count($onemsg, 'Back_personal_accaunt')) {
            if (!empty($this->checkInWork($idPostfix, $entity_data_class))) {
                return false;
            }
            return $this->BackPersonalAccount($onemsg);


        }elseif (substr_count($onemsg, 'OrderKKT')) {
            if(!empty($this->checkInWork($idPostfix, $entity_data_class))){
                return false;
            }
            return $this->orderKKT($onemsg);

        } elseif (substr_count($onemsg, 'OrderEDO')) {
            if ( ! empty($this->checkInWork($idPostfix, $entity_data_class))) {
                return false;
            }

            return $this->orderEDO($onemsg);

        }elseif (substr_count($onemsg, 'dopserv')) {
            if(!empty($this->checkInWork($idPostfix, $entity_data_class))){
                return false;
            }
            return $this->dopServ($onemsg);

        }elseif (substr_count($onemsg, 'RentKKT')) {
            if(!empty($this->checkInWork($idPostfix, $entity_data_class))){
                return false;
            }
            return $this->rentKKT($onemsg);

        }elseif (substr_count($onemsg, 'RequestClosingDoc')) {
            if(!empty($this->checkInWork($idPostfix, $entity_data_class))){
                return false;
            }
            return $this->requestClosingDoc($onemsg);

        }
        return false;
    }

    function checkInWork($idPostfix, $entity_data_class){
        //Проверка письма по статусу "НЕ в работе"
        $res = $entity_data_class::getById($idPostfix);
        $arLetter = $res->fetch();
        if($arLetter['UF_IN_WORK'] == "1"){
            return true;
        }

        //Проставляем статус в Работе
        $entity_data_class::update($idPostfix, array("UF_IN_WORK" => "1"));
        return false;
    }


    ///     /Функция обработки сообщения с типом TopUp_personal_accaunt
    function topUpPersonalAccount($onemsg)
    {
        $CCrmEvent       = new CCrmEvent();
        $oCompany        = new CCrmCompany;

        $arParams = array(
            "INN"           => parseParamsString("ИНН", $onemsg),
            "KPP"           => parseParamsString("KPP", $onemsg),
            "PERSONAL_ACC"  => parseParamsString("NumPersonal_accaunt", $onemsg),
            "SUMMA"         => parseParamsString("TopUp Summ", $onemsg),
            "TENANT" => parseParamsString("Tenant", $onemsg),
            "ID_PROD"       => parseParamsString("ID", $onemsg),
            "EVENT"         => parseParamsString("Event Type",$onemsg),
            "SOSTOYANIE_LC" => parseParamsString("Accaunt",$onemsg)
        );

        //pre($arParams);
        //return true;


        if (array_key_exists($arParams['TENANT'], $this->arTenantCompanyType)) {
            $arParams['TENANT'] = $this->arTenantCompanyType[$arParams['TENANT']];
        } else {
            $arParams['TENANT'] = "CUSTOMER";
        }
        switch (strlen($arParams["INN"])) {
            case 10: //Организация
                break;
            case 12: //ИП
                break;
            default:
                $this->error .= "Не валидный ИНН: " . $arParams["INN"];
                return $arParams["INN"];
                break;
        }

        /** Поиск компании по реквизитам ИНН и КПП */
        $requisite = new \Bitrix\Crm\EntityRequisite();
        $fieldsInfo = $requisite->getFormFieldsInfo();
        $select = array_keys($fieldsInfo);
        $arFilter = array(
            'RQ_INN' => $arParams["INN"],
            'RQ_KPP' => $arParams["KPP"],
            'ENTITY_TYPE_ID' => 4
        );

        $res = $requisite->getList(
            array(
                'order' => array('SORT' => 'ASC', 'ID' => 'ASC'),
                'filter' => $arFilter,
                'select' => $select
            )
        );
        //компания найдена
        if ($row = $res->fetch()) {
            $companyID = $row['ENTITY_ID'];
            $this->status .= "Компания найдена [$companyID]. ";
        }

        $guid = RestBpmBitrix::generate_guid();
        if (empty($companyID)) { //Если нет компании создаём её через addOneKKT
            //$companyID = $this->addOneKKT($onemsg, false);
            // $isCreateInAddOneKKT = true;
            $arCompanyInfo = $this->GetInfo($arParams["INN"]);
            //Добавление компании на основании данных от Налоговой
            if (array_key_exists(0, $arCompanyInfo) && !empty($arCompanyInfo)) {
                $info = Array(
                    'fields' => Array(
                        'RQ_INN' => $arParams["INN"],
                        'RQ_KPP' => (!empty($arParams["KPP"])) ? $arParams["KPP"] : "",
                        'SORT' => 500,
                        'ENTITY_TYPE_ID' => 4,
                    )
                );

                $ogrn = (!empty($arCompanyInfo[0]['fields']['RQ_OGRNIP'])) ? $arCompanyInfo[0]['fields']['RQ_OGRNIP'] : $arCompanyInfo[0]['fields']['RQ_OGRN'];
                $ogrn = trim($ogrn);
                switch (strlen($ogrn)) {
                    case 15: //ИП
                        $info['fields']['NAME'] = 'ИП';
                        $info['fields']['RQ_OGRNIP'] = $ogrn;
                        $info['fields']['PRESET_ID'] = 2;
                        $info['fields']['RQ_LAST_NAME'] = $arCompanyInfo[0]['fields']['RQ_LAST_NAME'];
                        $info['fields']['RQ_FIRST_NAME'] = $arCompanyInfo[0]['fields']['RQ_FIRST_NAME'];
                        $info['fields']['RQ_SECOND_NAME'] = $arCompanyInfo[0]['fields']['RQ_SECOND_NAME'];

                        $arFields['TITLE'] = $arCompanyInfo[0]['fields']['RQ_NAME'];
                        $arFields['UF_FULL_NAME'] = $arCompanyInfo[0]['fields']['RQ_NAME'];
                        $arFields['COMPANY_TITLE'] = $arCompanyInfo[0]['fields']['RQ_NAME'];
                        $arFields["UF_CRM_INN"] = $arParams["INN"];
                        $arFields["UF_CRM_COMPANY_GUID"] = $guid;
                        $arFields["UF_CRM_OGRN_API"] = $ogrn;
                        break;
                    default: //Организация
                        $info['fields']['NAME'] = 'Организация';
                        $info['fields']['RQ_COMPANY_NAME'] = (!empty($arCompanyInfo[0]['fields']['RQ_COMPANY_NAME'])) ? $arCompanyInfo[0]['fields']['RQ_COMPANY_NAME'] : $arCompanyInfo[0]['fields']['RQ_COMPANY_FULL_NAME'];
                        $info['fields']['RQ_COMPANY_FULL_NAME'] = (!empty($arCompanyInfo[0]['fields']['RQ_COMPANY_FULL_NAME'])) ? $arCompanyInfo[0]['fields']['RQ_COMPANY_FULL_NAME'] : "";
                        $info['fields']['RQ_OGRN'] = $ogrn;
                        $info['fields']['PRESET_ID'] = 1;

                        $arFields['TITLE'] = $arCompanyInfo[0]['fields']['RQ_COMPANY_NAME'];
                        $arFields['UF_FULL_NAME'] = $arCompanyInfo[0]['fields']['RQ_COMPANY_FULL_NAME'];
                        $arFields['COMPANY_TITLE'] = $arCompanyInfo[0]['fields']['RQ_COMPANY_NAME'];
                        $arFields["UF_CRM_INN"] = $arParams["INN"];
                        $arFields["UF_CRM_KPP"] = $arParams["KPP"];
                        $arFields["UF_CRM_COMPANY_GUID"] = $guid;
                        $arFields["UF_CRM_OGRN_API"] = $ogrn;
                        break;
                }

                $arFields["ASSIGNED_BY_ID"] = 372;
                $arFields['COMPANY_TYPE'] = $arParams['TENANT'];
                //$arFields['UF_ID_PROD']     = $idProd;
                $arFields['UF_CRM_INN'] = $arParams["INN"];
                $arFields['UF_CRM_KPP'] = $arParams["KPP"];

                //Создаем компанию
                $arFields['UF_PAYMENT'] = 269; //GK из биллинга

                if ($companyID = $oCompany->Add($arFields)) {
                    $this->status .= "Создали компанию: $companyID. ";
                } else {
                    $this->error .= "Не удалось создать компанию. ";
                    return true;
                }

                $info['fields']['ENTITY_ID'] = $companyID;

                if ($resGk = gk_AddRQ($info)) {
                    $this->status .= "Успешно добавлены реквизиты для компании $companyID. ";
                    addRelationship($arParams["INN"], $companyID);
                } else {
                    $this->error .= "Ошибка добавления реквизитов для компании $companyID. ";
                }

            }

            //Добавляем компанию, если нет информации от Налоговой
            if (empty($arCompanyInfo)) {
                $arFields = array(
                    "TITLE" => "Организация",
                    "UF_CRM_INN" => $arParams["INN"],
                    "UF_CRM_KPP" => $arParams["KPP"],
                    "UF_CRM_COMPANY_GUID" => $guid,
                    "ASSIGNED_BY_ID" => 372,
                    "COMPANY_TYPE" => $arParams['TENANT'],
                    //"UF_ID_PROD"            => $idProd
                );

                //Создаем компанию
                $arFields['UF_PAYMENT'] = 269; //GK из биллинга

                if ($companyID = $oCompany->Add($arFields)) {
                    $this->status .= "Создали компанию: $companyID. ";
                } else {
                    $this->error .= "Не удалось создать компанию. ";
                    return true;
                }

                //TODO сделать проверку по длине ИНН и добавлять соответственный реквизит.

                //Добавляем реквизиты
                $info = Array(
                    'fields' => Array(
                        'NAME' => "Организация",
                        'RQ_INN' => $arParams["INN"],
                        'RQ_KPP' => $arParams["KPP"],
                        'SORT' => 500,
                        'ENTITY_TYPE_ID' => 4,
                        'ENTITY_ID' => $companyID,
                        'PRESET_ID' => 1,
                    )
                );

                if ($resGk = gk_AddRQ($info)) {
                    $this->status .= "Успешно добавлены реквизиты для компании $companyID. ";
                    addRelationship($arParams["INN"], $companyID);
                } else {
                    $this->error .= "Ошибка добавления реквизитов для компании $companyID. ";
                }

            }
        }


        if ($companyID === true) {
            $this->error .= "Ошибка создания Компании/Сделки/ККТ";
            return true;
        }

        $resCompany = $oCompany->GetListEx(array("ID" => "ASC"), array("ID" => $companyID), array("ASSIGNED_BY_ID"));
        $assignedID = 372; //по-умолчанию bitrix
        if ($fieldsCompany = $resCompany->Fetch()) {
            $assignedID = $fieldsCompany['ASSIGNED_BY_ID'];
            $this->status .= "Получен id ответственного [$assignedID]. ";
        }

        $IBLOCK             = 86;
        $sostoynie_lc       = $arParams["SOSTOYANIE_LC"];
        $accaunt            = $arParams["PERSONAL_ACC"];
        $summa              = $arParams["SUMMA"];
        $name_plat_rekvizit = "Пополнение Л/С";

        $propPlat = Array();
        $propPlat["SOSTOYANIE"] = $sostoynie_lc;
        $propPlat["ACTIVATION_DATE"] = date("d.m.Y H:i:s");
        $propPlat["ASSIGNED"] = $assignedID;
        $propPlat["COMPANY"] = $companyID;
        $propPlat["SUMM"] = $summa;
        $propPlat["NUM_LIC_SCHET"] = $accaunt;
        $propPlat["INN"] = $arParams['INN'];
        $propPlat["KPP"] = $arParams['KPP'];
        $propPlat["LAST"] = Array("VALUE" => getPropertyEnumIdByXmlId($IBLOCK, "LAST_ADD_PAY"));
        $propPlat["TYPE"] = Array("VALUE" => getPropertyEnumIdByXmlId($IBLOCK, "TYPE_RECHARGE"));

        $arFieldsPlat = Array(
            "DATE_CREATE" => date("d.m.Y H:i:s"),
            "IBLOCK_ID" => $IBLOCK,
            "PROPERTY_VALUES" => $propPlat,
            "NAME" => $name_plat_rekvizit,
            "ACTIVE" => "Y"
        );
        $el = new CIBlockElement;
        if ($ELEMENT_ID = $el->Add($arFieldsPlat)) {
            $this->status .= "Создали платежный реквизит: ($ELEMENT_ID), № $accaunt. ";
        } else {
            $this->status .= "Ошибка создания платежного реквизита: " . $el->LAST_ERROR;
            return false;
        }
        $idEventHistory = $CCrmEvent->Add(
            array(
                'ENTITY_TYPE' => 'COMPANY',
                'ENTITY_ID' => $companyID,
                'EVENT_ID' => 'INFO',
                'EVENT_TEXT_1' => "Пополнение № $accaunt лицевого счета на сумму: $summa руб.",
            )
        );

        if ($idEventHistory) {
            $this->status .= "Успешно создана история для Компании. ";
        } else {
            $this->error .= "Ошибка создания история для Компании. ";
        }


        return true;
    }


    ///     /Функция обработки сообщения с типом Back_personal_accaunt
    function BackPersonalAccount($onemsg)
    {
        $CCrmEvent = new CCrmEvent();
        $oCompany = new CCrmCompany;

        $arParams = array(
            "INN" => parseParamsString("ИНН", $onemsg),
            "KPP" => parseParamsString("KPP", $onemsg),
            "PERSONAL_ACC" => parseParamsString("NumPersonal_accaunt", $onemsg),
            "SUMMA" => parseParamsString("Back_Summ", $onemsg),
            "TENANT" => parseParamsString("Tenant", $onemsg),
            "ID_PROD" => parseParamsString("ID", $onemsg),
            "EVENT" => parseParamsString("Event Type", $onemsg),
            "SOSTOYANIE_LC" => parseParamsString("Accaunt", $onemsg)
        );

		if( empty($arParams["SOSTOYANIE_LC"]) && !empty( parseParamsString("Текущий баланс лицевого счёта", $onemsg) ) ){
			$arParams["SOSTOYANIE_LC"] = parseParamsString("Текущий баланс лицевого счёта", $onemsg);
		}


        if (array_key_exists($arParams['TENANT'], $this->arTenantCompanyType)) {
            $arParams['TENANT'] = $this->arTenantCompanyType[$arParams['TENANT']];
        } else {
            $arParams['TENANT'] = "CUSTOMER";
        }
        switch (strlen($arParams["INN"])) {
            case 10: //Организация
                break;
            case 12: //ИП
                break;
            default:
                $this->error .= "Не валидный ИНН: " . $arParams["INN"];
                return $arParams["INN"];
                break;
        }

        /** Поиск компании по реквизитам ИНН и КПП */
        $requisite  = new \Bitrix\Crm\EntityRequisite();
        $fieldsInfo = $requisite->getFormFieldsInfo();
        $select     = array_keys($fieldsInfo);
        $arFilter = array(
            'RQ_INN' => $arParams["INN"],
            'RQ_KPP' => $arParams["KPP"],
            'ENTITY_TYPE_ID' => 4
        );

        $res = $requisite->getList(
            array(
                'order' => array('SORT' => 'ASC', 'ID' => 'ASC'),
                'filter' => $arFilter,
                'select' => $select
            )
        );
        //компания найдена
        if ($row = $res->fetch()) {
            $companyID = $row['ENTITY_ID'];
            $this->status .= "Компания найдена [$companyID]. ";
        }


        if (empty($companyID)) { //Если нет компании создаём её через addOneKKT
            $guid = RestBpmBitrix::generate_guid();
            if (empty($companyID)) { //Если нет компании создаём её через addOneKKT
                //$companyID = $this->addOneKKT($onemsg, false);
                // $isCreateInAddOneKKT = true;
                $arCompanyInfo = $this->GetInfo($arParams["INN"]);
                //Добавление компании на основании данных от Налоговой
                if (array_key_exists(0, $arCompanyInfo) && !empty($arCompanyInfo)) {
                    $info = Array(
                        'fields' => Array(
                            'RQ_INN' => $arParams["INN"],
                            'RQ_KPP' => (!empty($arParams["KPP"])) ? $arParams["KPP"] : "",
                            'SORT' => 500,
                            'ENTITY_TYPE_ID' => 4,
                        )
                    );

                    $ogrn = (!empty($arCompanyInfo[0]['fields']['RQ_OGRNIP'])) ? $arCompanyInfo[0]['fields']['RQ_OGRNIP'] : $arCompanyInfo[0]['fields']['RQ_OGRN'];
                    $ogrn = trim($ogrn);
                    switch (strlen($ogrn)) {
                        case 15: //ИП
                            $info['fields']['NAME'] = 'ИП';
                            $info['fields']['RQ_OGRNIP'] = $ogrn;
                            $info['fields']['PRESET_ID'] = 2;
                            $info['fields']['RQ_LAST_NAME'] = $arCompanyInfo[0]['fields']['RQ_LAST_NAME'];
                            $info['fields']['RQ_FIRST_NAME'] = $arCompanyInfo[0]['fields']['RQ_FIRST_NAME'];
                            $info['fields']['RQ_SECOND_NAME'] = $arCompanyInfo[0]['fields']['RQ_SECOND_NAME'];

                            $arFields['TITLE'] = $arCompanyInfo[0]['fields']['RQ_NAME'];
                            $arFields['UF_FULL_NAME'] = $arCompanyInfo[0]['fields']['RQ_NAME'];
                            $arFields['COMPANY_TITLE'] = $arCompanyInfo[0]['fields']['RQ_NAME'];
                            $arFields["UF_CRM_INN"] = $arParams["INN"];
                            $arFields["UF_CRM_COMPANY_GUID"] = $guid;
                            $arFields["UF_CRM_OGRN_API"] = $ogrn;
                            break;
                        default: //Организация
                            $info['fields']['NAME'] = 'Организация';
                            $info['fields']['RQ_COMPANY_NAME'] = (!empty($arCompanyInfo[0]['fields']['RQ_COMPANY_NAME'])) ? $arCompanyInfo[0]['fields']['RQ_COMPANY_NAME'] : $arCompanyInfo[0]['fields']['RQ_COMPANY_FULL_NAME'];
                            $info['fields']['RQ_COMPANY_FULL_NAME'] = (!empty($arCompanyInfo[0]['fields']['RQ_COMPANY_FULL_NAME'])) ? $arCompanyInfo[0]['fields']['RQ_COMPANY_FULL_NAME'] : "";
                            $info['fields']['RQ_OGRN'] = $ogrn;
                            $info['fields']['PRESET_ID'] = 1;

                            $arFields['TITLE'] = $arCompanyInfo[0]['fields']['RQ_COMPANY_NAME'];
                            $arFields['UF_FULL_NAME'] = $arCompanyInfo[0]['fields']['RQ_COMPANY_FULL_NAME'];
                            $arFields['COMPANY_TITLE'] = $arCompanyInfo[0]['fields']['RQ_COMPANY_NAME'];
                            $arFields["UF_CRM_INN"] = $arParams["INN"];
                            $arFields["UF_CRM_KPP"] = $arParams["KPP"];
                            $arFields["UF_CRM_COMPANY_GUID"] = $guid;
                            $arFields["UF_CRM_OGRN_API"] = $ogrn;
                            break;
                    }

                    $arFields["ASSIGNED_BY_ID"] = 372;
                    $arFields['COMPANY_TYPE'] = $arParams['TENANT'];
                    //$arFields['UF_ID_PROD']     = $idProd;
                    $arFields['UF_CRM_INN'] = $arParams["INN"];
                    $arFields['UF_CRM_KPP'] = $arParams["KPP"];

                    //Создаем компанию
                    $arFields['UF_PAYMENT'] = 269; //GK из биллинга

                    if ($companyID = $oCompany->Add($arFields)) {
                        $this->status .= "Создали компанию: $companyID. ";
                    } else {
                        $this->error .= "Не удалось создать компанию. ";
                        return true;
                    }

                    $info['fields']['ENTITY_ID'] = $companyID;

                    if ($resGk = gk_AddRQ($info)) {
                        $this->status .= "Успешно добавлены реквизиты для компании $companyID. ";
                        addRelationship($arParams["INN"], $companyID);
                    } else {
                        $this->error .= "Ошибка добавления реквизитов для компании $companyID. ";
                    }

                }

                //Добавляем компанию, если нет информации от Налоговой
                if (empty($arCompanyInfo)) {
                    $arFields = array(
                        "TITLE" => "Организация",
                        "UF_CRM_INN" => $arParams["INN"],
                        "UF_CRM_KPP" => $arParams["KPP"],
                        "UF_CRM_COMPANY_GUID" => $guid,
                        "ASSIGNED_BY_ID" => 372,
                        "COMPANY_TYPE" => $arParams['TENANT'],
                        //"UF_ID_PROD"            => $idProd
                    );

                    //Создаем компанию
                    $arFields['UF_PAYMENT'] = 269; //GK из биллинга

                    if ($companyID = $oCompany->Add($arFields)) {
                        $this->status .= "Создали компанию: $companyID. ";
                    } else {
                        $this->error .= "Не удалось создать компанию. ";
                        return true;
                    }

                    //TODO сделать проверку по длине ИНН и добавлять соответственный реквизит.

                    //Добавляем реквизиты
                    $info = Array(
                        'fields' => Array(
                            'NAME' => "Организация",
                            'RQ_INN' => $arParams["INN"],
                            'RQ_KPP' => $arParams["KPP"],
                            'SORT' => 500,
                            'ENTITY_TYPE_ID' => 4,
                            'ENTITY_ID' => $companyID,
                            'PRESET_ID' => 1,
                        )
                    );

                    if ($resGk = gk_AddRQ($info)) {
                        $this->status .= "Успешно добавлены реквизиты для компании $companyID. ";
                        addRelationship($arParams["INN"], $companyID);
                    } else {
                        $this->error .= "Ошибка добавления реквизитов для компании $companyID. ";
                    }

                }
            }
        }
        if ($companyID === true) {
            $this->error .= "Ошибка создания Компании/Сделки/ККТ";
            return true;
        }

        $resCompany = $oCompany->GetListEx(array("ID" => "ASC"), array("ID" => $companyID), array("ASSIGNED_BY_ID"));
        $assignedID = 372; //по-умолчанию bitrix
        if ($fieldsCompany = $resCompany->Fetch()) {
            $assignedID = $fieldsCompany['ASSIGNED_BY_ID'];
            $this->status .= "Получен id ответственного [$assignedID]. ";
        }

        $IBLOCK = 86;
        $sostoynie_lc = $arParams["SOSTOYANIE_LC"];
        $accaunt = $arParams["PERSONAL_ACC"];
        $summa = $arParams["SUMMA"];
        $name_plat_rekvizit = "Возврат средств на Л/С";

        $propPlat = Array();
        $propPlat["SOSTOYANIE"] = $sostoynie_lc;
        $propPlat["ACTIVATION_DATE"] = date("d.m.Y H:i:s");
        $propPlat["ASSIGNED"] = $assignedID;
        $propPlat["COMPANY"] = $companyID;
        $propPlat["SUMM"] = $summa;
        $propPlat["NUM_LIC_SCHET"] = $accaunt;
        $propPlat["INN"] = $arParams['INN'];
        $propPlat["KPP"] = $arParams['KPP'];
        $propPlat["LAST"] = Array("VALUE" => getPropertyEnumIdByXmlId($IBLOCK, "LAST_AMOUNTRETURNED"));
        $propPlat["TYPE"] = Array("VALUE" => getPropertyEnumIdByXmlId($IBLOCK, "TYPE_RECHARGE"));

        $arFieldsPlat = Array(
            "DATE_CREATE" => date("d.m.Y H:i:s"),
            "IBLOCK_ID" => $IBLOCK,
            "PROPERTY_VALUES" => $propPlat,
            "NAME" => $name_plat_rekvizit,
            "ACTIVE" => "Y"
        );
        $el = new CIBlockElement;
        if ($ELEMENT_ID = $el->Add($arFieldsPlat)) {
            $this->status .= "Создали платежный реквизит: ($ELEMENT_ID), № $accaunt. ";
        } else {
            $this->status .= "Ошибка создания платежного реквизита: " . $el->LAST_ERROR;
            return false;
        }
        $idEventHistory = $CCrmEvent->Add(
            array(
                'ENTITY_TYPE' => 'COMPANY',
                'ENTITY_ID' => $companyID,
                'EVENT_ID' => 'INFO',
                'EVENT_TEXT_1' => "Возврат средств № $accaunt лицевого счета на сумму: $summa руб.",
            )
        );

        if ($idEventHistory) {
            $this->status .= "Успешно создана история для Компании. ";
        } else {
            $this->error .= "Ошибка создания история для Компании. ";
        }


        return true;
    }


    ///     /Функция обработки сообщения с типом Down_personal_accaunt
    function DownPersonalAccount($onemsg)
    {
        $CCrmEvent = new CCrmEvent();
        $oCompany = new CCrmCompany;

        $arParams = array(
            "INN" => parseParamsString("ИНН", $onemsg),
            "KPP" => parseParamsString("KPP", $onemsg),
            "PERSONAL_ACC" => parseParamsString("NumPersonal_accaunt", $onemsg),
            "SUMMA" => parseParamsString("Down_Summ", $onemsg),
            "TENANT" => parseParamsString("Tenant", $onemsg),
            "ID_PROD" => parseParamsString("ID", $onemsg),
            "EVENT" => parseParamsString("Event Type", $onemsg),
            "SOSTOYANIE_LC" => parseParamsString("Accaunt", $onemsg)
        );

        //pre($arParams);
        //return true;


        if (array_key_exists($arParams['TENANT'], $this->arTenantCompanyType)) {
            $arParams['TENANT'] = $this->arTenantCompanyType[$arParams['TENANT']];
        } else {
            $arParams['TENANT'] = "CUSTOMER";
        }
        switch (strlen($arParams["INN"])) {
            case 10: //Организация
                break;
            case 12: //ИП
                break;
            default:
                $this->error .= "Не валидный ИНН: " . $arParams["INN"];
                return $arParams["INN"];
                break;
        }

        /** Поиск компании по реквизитам ИНН и КПП */
        $requisite = new \Bitrix\Crm\EntityRequisite();
        $fieldsInfo = $requisite->getFormFieldsInfo();
        $select = array_keys($fieldsInfo);
        $arFilter = array(
            'RQ_INN' => $arParams["INN"],
            'RQ_KPP' => $arParams["KPP"],
            'ENTITY_TYPE_ID' => 4
        );

        $res = $requisite->getList(
            array(
                'order' => array('SORT' => 'ASC', 'ID' => 'ASC'),
                'filter' => $arFilter,
                'select' => $select
            )
        );
        //компания найдена
        if ($row = $res->fetch()) {
            $companyID = $row['ENTITY_ID'];
            $this->status .= "Компания найдена [$companyID]. ";
        }


        if (empty($companyID)) { //Если нет компании создаём её через addOneKKT
            $guid = RestBpmBitrix::generate_guid();
            if (empty($companyID)) { //Если нет компании создаём её через addOneKKT
                //$companyID = $this->addOneKKT($onemsg, false);
                // $isCreateInAddOneKKT = true;
                $arCompanyInfo = $this->GetInfo($arParams["INN"]);
                //Добавление компании на основании данных от Налоговой
                if (array_key_exists(0, $arCompanyInfo) && !empty($arCompanyInfo)) {
                    $info = Array(
                        'fields' => Array(
                            'RQ_INN' => $arParams["INN"],
                            'RQ_KPP' => (!empty($arParams["KPP"])) ? $arParams["KPP"] : "",
                            'SORT' => 500,
                            'ENTITY_TYPE_ID' => 4,
                        )
                    );

                    $ogrn = (!empty($arCompanyInfo[0]['fields']['RQ_OGRNIP'])) ? $arCompanyInfo[0]['fields']['RQ_OGRNIP'] : $arCompanyInfo[0]['fields']['RQ_OGRN'];
                    $ogrn = trim($ogrn);
                    switch (strlen($ogrn)) {
                        case 15: //ИП
                            $info['fields']['NAME'] = 'ИП';
                            $info['fields']['RQ_OGRNIP'] = $ogrn;
                            $info['fields']['PRESET_ID'] = 2;
                            $info['fields']['RQ_LAST_NAME'] = $arCompanyInfo[0]['fields']['RQ_LAST_NAME'];
                            $info['fields']['RQ_FIRST_NAME'] = $arCompanyInfo[0]['fields']['RQ_FIRST_NAME'];
                            $info['fields']['RQ_SECOND_NAME'] = $arCompanyInfo[0]['fields']['RQ_SECOND_NAME'];

                            $arFields['TITLE'] = $arCompanyInfo[0]['fields']['RQ_NAME'];
                            $arFields['UF_FULL_NAME'] = $arCompanyInfo[0]['fields']['RQ_NAME'];
                            $arFields['COMPANY_TITLE'] = $arCompanyInfo[0]['fields']['RQ_NAME'];
                            $arFields["UF_CRM_INN"] = $arParams["INN"];
                            $arFields["UF_CRM_COMPANY_GUID"] = $guid;
                            $arFields["UF_CRM_OGRN_API"] = $ogrn;
                            break;
                        default: //Организация
                            $info['fields']['NAME'] = 'Организация';
                            $info['fields']['RQ_COMPANY_NAME'] = (!empty($arCompanyInfo[0]['fields']['RQ_COMPANY_NAME'])) ? $arCompanyInfo[0]['fields']['RQ_COMPANY_NAME'] : $arCompanyInfo[0]['fields']['RQ_COMPANY_FULL_NAME'];
                            $info['fields']['RQ_COMPANY_FULL_NAME'] = (!empty($arCompanyInfo[0]['fields']['RQ_COMPANY_FULL_NAME'])) ? $arCompanyInfo[0]['fields']['RQ_COMPANY_FULL_NAME'] : "";
                            $info['fields']['RQ_OGRN'] = $ogrn;
                            $info['fields']['PRESET_ID'] = 1;

                            $arFields['TITLE'] = $arCompanyInfo[0]['fields']['RQ_COMPANY_NAME'];
                            $arFields['UF_FULL_NAME'] = $arCompanyInfo[0]['fields']['RQ_COMPANY_FULL_NAME'];
                            $arFields['COMPANY_TITLE'] = $arCompanyInfo[0]['fields']['RQ_COMPANY_NAME'];
                            $arFields["UF_CRM_INN"] = $arParams["INN"];
                            $arFields["UF_CRM_KPP"] = $arParams["KPP"];
                            $arFields["UF_CRM_COMPANY_GUID"] = $guid;
                            $arFields["UF_CRM_OGRN_API"] = $ogrn;
                            break;
                    }

                    $arFields["ASSIGNED_BY_ID"] = 372;
                    $arFields['COMPANY_TYPE'] = $arParams['TENANT'];
                    //$arFields['UF_ID_PROD']     = $idProd;
                    $arFields['UF_CRM_INN'] = $arParams["INN"];
                    $arFields['UF_CRM_KPP'] = $arParams["KPP"];

                    //Создаем компанию
                    $arFields['UF_PAYMENT'] = 269; //GK из биллинга

                    if ($companyID = $oCompany->Add($arFields)) {
                        $this->status .= "Создали компанию: $companyID. ";
                    } else {
                        $this->error .= "Не удалось создать компанию. ";
                        return true;
                    }

                    $info['fields']['ENTITY_ID'] = $companyID;

                    if ($resGk = gk_AddRQ($info)) {
                        $this->status .= "Успешно добавлены реквизиты для компании $companyID. ";
                        addRelationship($arParams["INN"], $companyID);
                    } else {
                        $this->error .= "Ошибка добавления реквизитов для компании $companyID. ";
                    }

                }

                //Добавляем компанию, если нет информации от Налоговой
                if (empty($arCompanyInfo)) {
                    $arFields = array(
                        "TITLE" => "Организация",
                        "UF_CRM_INN" => $arParams["INN"],
                        "UF_CRM_KPP" => $arParams["KPP"],
                        "UF_CRM_COMPANY_GUID" => $guid,
                        "ASSIGNED_BY_ID" => 372,
                        "COMPANY_TYPE" => $arParams['TENANT'],
                        //"UF_ID_PROD"            => $idProd
                    );

                    //Создаем компанию
                    $arFields['UF_PAYMENT'] = 269; //GK из биллинга

                    if ($companyID = $oCompany->Add($arFields)) {
                        $this->status .= "Создали компанию: $companyID. ";
                    } else {
                        $this->error .= "Не удалось создать компанию. ";
                        return true;
                    }

                    //TODO сделать проверку по длине ИНН и добавлять соответственный реквизит.

                    //Добавляем реквизиты
                    $info = Array(
                        'fields' => Array(
                            'NAME' => "Организация",
                            'RQ_INN' => $arParams["INN"],
                            'RQ_KPP' => $arParams["KPP"],
                            'SORT' => 500,
                            'ENTITY_TYPE_ID' => 4,
                            'ENTITY_ID' => $companyID,
                            'PRESET_ID' => 1,
                        )
                    );

                    if ($resGk = gk_AddRQ($info)) {
                        $this->status .= "Успешно добавлены реквизиты для компании $companyID. ";
                        addRelationship($arParams["INN"], $companyID);
                    } else {
                        $this->error .= "Ошибка добавления реквизитов для компании $companyID. ";
                    }

                }
            }
        }
        if ($companyID === true) {
            $this->error .= "Ошибка создания Компании/Сделки/ККТ";
            return true;
        }

        $resCompany = $oCompany->GetListEx(array("ID" => "ASC"), array("ID" => $companyID), array("ASSIGNED_BY_ID"));
        $assignedID = 372; //по-умолчанию bitrix
        if ($fieldsCompany = $resCompany->Fetch()) {
            $assignedID = $fieldsCompany['ASSIGNED_BY_ID'];
            $this->status .= "Получен id ответственного [$assignedID]. ";
        }

        $IBLOCK = 86;
        $sostoynie_lc = $arParams["SOSTOYANIE_LC"];
        $accaunt = $arParams["PERSONAL_ACC"];
        $summa = $arParams["SUMMA"];
        $name_plat_rekvizit = "Списание с Л/С";

        $propPlat = Array();
        $propPlat["SOSTOYANIE"] = $sostoynie_lc;
        $propPlat["ACTIVATION_DATE"] = date("d.m.Y H:i:s");
        $propPlat["ASSIGNED"] = $assignedID;
        $propPlat["COMPANY"] = $companyID;
        $propPlat["SUMM"] = $summa;
        $propPlat["NUM_LIC_SCHET"] = $accaunt;
        $propPlat["INN"] = $arParams['INN'];
        $propPlat["KPP"] = $arParams['KPP'];
        $propPlat["LAST"] = Array("VALUE" => getPropertyEnumIdByXmlId($IBLOCK, "LAST_PAYFORSERVICE"));
        $propPlat["TYPE"] = Array("VALUE" => getPropertyEnumIdByXmlId($IBLOCK, "TYPE_DECOMMISSION"));

        $arFieldsPlat = Array(
            "DATE_CREATE" => date("d.m.Y H:i:s"),
            "IBLOCK_ID" => $IBLOCK,
            "PROPERTY_VALUES" => $propPlat,
            "NAME" => $name_plat_rekvizit,
            "ACTIVE" => "Y"
        );
        $el = new CIBlockElement;
        if ($ELEMENT_ID = $el->Add($arFieldsPlat)) {
            $this->status .= "Создали платежный реквизит: ($ELEMENT_ID), № $accaunt. ";
        } else {
            $this->status .= "Ошибка создания платежного реквизита: " . $el->LAST_ERROR;
            return false;
        }
        $idEventHistory = $CCrmEvent->Add(
            array(
                'ENTITY_TYPE' => 'COMPANY',
                'ENTITY_ID' => $companyID,
                'EVENT_ID' => 'INFO',
                'EVENT_TEXT_1' => "Списание № $accaunt лицевого счета на сумму: $summa руб.",
            )
        );

        if ($idEventHistory) {
            $this->status .= "Успешно создана история для Компании. ";
        } else {
            $this->error .= "Ошибка создания история для Компании. ";
        }


        return true;
    }


    //Функция обработки сообщения с типом Activation_KKT
    function activationKKT($onemsg) {
        //подключаемые модули
        CModule::IncludeModule('crm');
        CModule::IncludeModule('iblock');

        $CCrmEvent                   = new CCrmEvent();
        $oIBlockElement              = new CIBlockElement();
        $oCompany                    = new CCrmCompany;
        $oLead                       = new CCrmLead;

        $IBLOCK_ID_KKT               = getIblockIDByCode('registry_kkt');
        $IBLOCK_ID_Pay               = getIblockIDByCode('reestr_pay');

        $isCreateInAddOneKKT         = false;
        $isLead                      = false;

        $arParams = array(
            "INN"                        => $this->parseParamsString("ИНН", $onemsg),
            "KPP"                        => $this->parseParamsString("KPP", $onemsg),
            "KKM_REG_ID"                 => $this->parseParamsString("KKMRegId", $onemsg),
            "TARIFF_NAME"                => $this->parseParamsString("TariffName", $onemsg),
            "ACTIVATION_DATE"            => $this->parseParamsString("Activation Date", $onemsg),
            "AGENT_CODE"                 => $this->parseParamsString("AgentCode", $onemsg),
            "PROMO_CODE"                 => $this->parseParamsString("PromoCode", $onemsg),
            "ACTIVATION_PERIOD"          => $this->parseParamsString("ActivationPeriod", $onemsg),
            "TENANT"                     => $this->parseParamsString("Tenant", $onemsg),
            "ACCOUNT"                    => $this->parseParamsString("Accaunt", $onemsg),
            "ID"                         => $this->parseParamsString("ID", $onemsg),
        );
        $arParams["ACTIVATION_DATE"] = $this->dateCut($arParams["ACTIVATION_DATE"]);

        //Определение ID тенант
        if(array_key_exists($arParams['TENANT'], $this->arTenantCompanyType)){
            $arParams['TENANT'] = $this->arTenantCompanyType[$arParams['TENANT']];
        } else {
            $arParams['TENANT'] = "CUSTOMER";
        }

        /** Проверка на наличие обязательных полей */

        switch (strlen($arParams["INN"])) {
            case 10: //Организация
                break;
            case 12: //ИП
                break;
            default:
                $this->error .= "Не валидный ИНН: " . $arParams["INN"];
                return $arParams["INN"];
                break;
        }

        if (empty($arParams["KKM_REG_ID"])) {
            $this->error .= "Отсутствует KKMRegId. Завершаем обработку. ";
            return true;
        }

        /** Поиск компании по реквизитам ИНН и КПП */

        $requisite  = new \Bitrix\Crm\EntityRequisite();
        $fieldsInfo = $requisite->getFormFieldsInfo();
        $select     = array_keys($fieldsInfo);

        $arFilter = array(
            'RQ_INN'            => $arParams["INN"],
            'RQ_KPP'            => $arParams["KPP"],
            'ENTITY_TYPE_ID'    => 4
        );

        $res = $requisite->getList(
            array(
                'order'     => array('SORT' => 'ASC', 'ID' => 'ASC'),
                'filter'    => $arFilter,
                'select'    => $select
            )
        );

        //компания найдена
        if ($row = $res->fetch()) {
            $companyID = $row['ENTITY_ID'];
            $this->status .= "Компания найдена [$companyID]. ";
        }

        /** Поиск/обновление/создание ККТ по RegID */

        $arKKT = $this->findDublicateKKT($arParams["KKM_REG_ID"]);

        if(empty($companyID) || empty($arKKT["ID"])) { //Если нет компании создаём её через addOneKKT
            $companyID = $this->addOneKKT($onemsg, false);
            $isCreateInAddOneKKT = true;
        }

        if ($companyID === true) {
            $this->error .= "Ошибка создания Компании/Сделки/ККТ";
            return true;
        }


        /** Получаем id отвественного на текущий момент по контрагенту */

        $resCompany = $oCompany->GetListEx(array("ID" => "ASC"), array("ID" => $companyID), array("ASSIGNED_BY_ID"));
        $assignedID = 372; //по-умолчанию bitrix
        if ($fieldsCompany = $resCompany->Fetch()) {
            $assignedID = $fieldsCompany['ASSIGNED_BY_ID'];
            $this->status .= "Получен id ответственного [$assignedID]. ";
        }


        /** Добавление записи в реестр платежей*/

        $arFields = array(
            "ACTIVE"              => "Y",
            "IBLOCK_ID"           => $IBLOCK_ID_Pay,
            "NAME"                => "Активация ККТ",
            "PROPERTY_VALUES"     => array(
                "INN"             => (!empty($arParams["INN"])) ? $arParams["INN"] : "",
                "KPP"             => (!empty($arParams["KPP"])) ? $arParams["KPP"] : "",
                "COMPANY"         => $companyID,
                "LAST"            => getPropertyEnumIdByXmlId($IBLOCK_ID_Pay, "LAST_ACTIVATION_KKT"),
                "TYPE"            => getPropertyEnumIdByXmlId($IBLOCK_ID_Pay, "TYPE_DECOMMISSION"),
                "ASSIGNED"        => $assignedID,
                "RNM_KKT"         => (!empty($arParams["KKM_REG_ID"])) ? $arParams["KKM_REG_ID"] : "",
                "ACTIVATION_DATE" => (!empty($arParams["ACTIVATION_DATE"])) ? $arParams["ACTIVATION_DATE"] : date("d.m.Y"),
                "SUMM"            => "0",
                "SOSTOYANIE"      => (!empty($arParams["ACCOUNT"])) ? $arParams["ACCOUNT"] : "0.00",
            )
        );

        if ($idElement = $oIBlockElement->Add($arFields)) {
            $this->status .= "Создан элемент в реестре платежей id " . $idElement . ". ";
        } else {
            $this->error .= "Ошибка создания элемента в реестре платежей. ";
        }

        //Обновление статуса активирован
        $arKKT = $this->findDublicateKKT($arParams["KKM_REG_ID"]);
        if(!empty($arKKT['ID'])) {
            $arProperty = [];
            $arProperty['ACTIVE']      = getPropertyEnumIdByXmlId($IBLOCK_ID_KKT, "AC_YES");
            $arProperty["DATE_ACTIVE"] = (!empty($arParams["ACTIVATION_DATE"])) ? $arParams["ACTIVATION_DATE"] : date("d.m.Y");

            $oIBlockElement->SetPropertyValuesEx(
                $arKKT["ID"],
                $IBLOCK_ID_KKT,
                $arProperty
            );
            $this->status .= "ККТ обновлёно " . $arKKT['ID'] . ". ";
        }

        //Если вызвали addOneKKT, то:
        // - ККТ не обновляем
        // - Историю не создаем.
        if($isCreateInAddOneKKT === true){
            return true;
        }

        //Update idProd
        $arFields = array(
            "UF_ID_PROD" => $arParams["ID"]
        );
        $oCompany->Update($companyID, $arFields);

        //Обновление ККТ
        $arProperty = [];
        (!empty($arParams['TARIFF_NAME'])) ?         ($arProperty['TARIF']           = $arParams['TARIFF_NAME'])         : "";
        (!empty($arParams['ACTIVATION_DATE'])) ?     ($arProperty['DATE_ACTIVE']     = $arParams['ACTIVATION_DATE'])     : "";
        (!empty($arParams['AGENT_CODE'])) ?          ($arProperty['CODE_AGENT']      = $arParams['AGENT_CODE'])          : "";
        (!empty($arParams['PROMO_CODE'])) ?          ($arProperty['PROMO_KOD']       = $arParams['PROMO_CODE'])          : "";
        (!empty($arParams['ACTIVATION_PERIOD'])) ?   ($arProperty['PERIOD_ACTIVE']   = $arParams['ACTIVATION_PERIOD'])   : "";

        $oIBlockElement->SetPropertyValuesEx(
            $arKKT["ID"],
            $IBLOCK_ID_KKT,
            $arProperty
        );
        $this->status .= "ККТ обновлёно " . $arKKT['ID'] . ". ";


        /** Создание истории для сделки */

        $idEventHistory = $CCrmEvent->Add(
            array(
                'ENTITY_TYPE' => 'DEAL',
                'ENTITY_ID' => $arKKT['PROPERTY_DEAL_VALUE'],
                'EVENT_ID' => 'INFO',
                'EVENT_TEXT_1' => 'Активирована ККТ №: ' . $arKKT["PROPERTY_NUM_KKT_VALUE"] . ', Дата: ' . date("d.m.Y") . " "
            )
        );

        if ($idEventHistory) {
            $this->status .= "История сделки успешно создана. ";
        } else {
            $this->error .= "Ошибка создания истории сделки. ";
        }


        /** Поиск Лида по ИНН */

        $resMultiADD = $oLead->GetList(array('ID' => 'asc'), array(
            'TITLE'             => 'регистрация на сайте www.1ofd.ru',
            'UF_CRM_1499414186' => $arParams["INN"]
        ));
        if ($arMultiADD = $resMultiADD->Fetch()) {
            $isLead = true;
            $arSelectedLead = array(
                "ID"            => $arMultiADD['ID'],
                "LAST_NAME"     => ($arMultiADD['LAST_NAME'] == "noname")? "" : $arMultiADD['LAST_NAME'],
                "NAME"          => ($arMultiADD['NAME'] == "noname")? "" : $arMultiADD['NAME'],
                "SECOND_NAME"   => ($arMultiADD['SECOND_NAME'] == "noname")? "" : $arMultiADD['SECOND_NAME'],
            );
        } else {
            $this->status .= "Лид по ИНН не найден. ";
        }

        //Переводим лид в статус Архивный и устанавливаем состояние лида [обработанный]
        if(!empty($isLead)) {
            $idXML = 'XML_STATE_LEAD_PROCESSED'; //Состояние "Обработанный"
            $idElementList = getIdElementListState ($idXML);
            $arFields = Array(
                "STATUS_ID"     => "1", //архивный статус
                "UF_STATE_LEAD" => $idElementList,
            );

            $oLead->Update($arSelectedLead['ID'], $arFields);
            $this->status .= "Лид переведён в статус 'Архивный' ID (" . $arSelectedLead['ID']."). ";
        }

        //Добавление истории Лида
        if(!empty($isLead)) {
            $idEventHistory = $CCrmEvent->Add(
                array(
                    'ENTITY_TYPE' => 'LEAD',
                    'ENTITY_ID' => $arSelectedLead['ID'],
                    'EVENT_ID' => 'INFO',
                    'EVENT_TEXT_1' => 'Активирована ККТ №: ' . $arKKT["PROPERTY_NUM_KKT_VALUE"] . ', Дата: ' . date("d.m.Y") . " ",
                )
            );
            if ($idEventHistory) {
                $this->status .= "История в Лиде успешно создана. ";
            } else {
                $this->error .= "Ошибка создания истории в Лиде. ";
            }
        }

        return true;
    }


    //Функция обработки сообщения с типом Activation_KKT
    function deactivationKKT($onemsg)
    {
        //подключаемые модули
        CModule::IncludeModule('crm');
        CModule::IncludeModule('iblock');

        $oIBlockElement = new CIBlockElement();
        $oCompany = new CCrmCompany;
        $IBLOCK_ID_KKT = getIblockIDByCode('registry_kkt');

        $arParams = array(
            "INN" => $this->parseParamsString("ИНН", $onemsg),
            "KPP" => $this->parseParamsString("KPP", $onemsg),
            "KKM_REG_ID" => $this->parseParamsString("KKMRegId", $onemsg),
            "DEACTIVATION_DATE" => $this->parseParamsString("Deactivation Date", $onemsg),
            "ID" => $this->parseParamsString("ID", $onemsg),
            "TENANT" => $this->parseParamsString("Tenant", $onemsg),
            "ACCOUNT" => $this->parseParamsString("Accaunt", $onemsg),
            "DEACTIVATION_PERIOD" => $this->parseParamsString("DeativationPeriod", $onemsg),
        );


        $arParams["DEACTIVATION_DATE"] = $this->dateCut($arParams["DEACTIVATION_DATE"]);

        //Определение ID тенант
        if (array_key_exists($arParams['TENANT'], $this->arTenantCompanyType)) {
            $arParams['TENANT'] = $this->arTenantCompanyType[$arParams['TENANT']];
        } else {
            $arParams['TENANT'] = "CUSTOMER";
        }

        /** Проверка на наличие обязательных полей */

        switch (strlen($arParams["INN"])) {
            case 10: //Организация
                break;
            case 12: //ИП
                break;
            default:
                $this->error .= "Не валидный ИНН: " . $arParams["INN"];
                return $arParams["INN"];
                break;
        }

        if (empty($arParams["KKM_REG_ID"])) {
            $this->error .= "Отсутствует KKMRegId. Завершаем обработку. ";
            return true;
        }

        /** Поиск компании по реквизитам ИНН и КПП */

        $requisite = new \Bitrix\Crm\EntityRequisite();
        $fieldsInfo = $requisite->getFormFieldsInfo();
        $select = array_keys($fieldsInfo);

        $arFilter = array(
            'RQ_INN' => $arParams["INN"],
            'RQ_KPP' => $arParams["KPP"],
            'ENTITY_TYPE_ID' => 4
        );

        $res = $requisite->getList(
            array(
                'order' => array('SORT' => 'ASC', 'ID' => 'ASC'),
                'filter' => $arFilter,
                'select' => $select
            )
        );

        //компания найдена
        if ($row = $res->fetch()) {
            $companyID = $row['ENTITY_ID'];
            $this->status .= "Компания найдена [$companyID]. ";
        }

        /** Поиск/обновление/создание ККТ по RegID */

        $arKKT = $this->findDublicateKKT($arParams["KKM_REG_ID"]);
        if (empty($companyID) || empty($arKKT["ID"])) { //Если нет компании создаём её через addOneKKT
            $companyID = $this->addOneKKT($onemsg, false);
            $isCreateInAddOneKKT = true;
        }

        if ($companyID === true) {
            $this->error .= "Ошибка создания Компании/Сделки/ККТ";
            return true;
        }

        /** Получаем id отвественного на текущий момент по контрагенту */

        $resCompany = $oCompany->GetListEx(array("ID" => "ASC"), array("ID" => $companyID), array("ASSIGNED_BY_ID"));
        $assignedID = 372; //по-умолчанию bitrix
        if ($fieldsCompany = $resCompany->Fetch()) {
            $assignedID = $fieldsCompany['ASSIGNED_BY_ID'];
            $this->status .= "Получен id ответственного [$assignedID]. ";
        }
        //Обновление ККТ
        $arProperty = [];
        (!empty($arParams['DEACTIVATION_DATE'])) ? ($arProperty['DATE_OFF'] = $arParams['DEACTIVATION_DATE']) : "";
        (!empty($arParams['DEACTIVATION_PERIOD'])) ? ($arProperty['PERIOD_OFF'] = $arParams['DEACTIVATION_PERIOD']) : "";

        $oIBlockElement->SetPropertyValuesEx(
            $arKKT["ID"],
            $IBLOCK_ID_KKT,
            $arProperty
        );
        $this->status .= "ККТ обновлёно " . $arKKT['ID'] . ". ";


        //Отправка письма ответсвенному- о деактивации ККТ.
        $rsUser = CUser::GetByID($assignedID);
        $arUser = $rsUser->Fetch();
        $assignedEmail = $arUser['EMAIL'];


//Подготовка к отправки письма
        $companydata = CCrmCompany::GetByID($companyID);
        $to = $assignedEmail;
// тема письма
        $subject = 'Деактивация ККТ';
        mail($to, $subject,
            '<p>Контрагент <a href="https://bitrix.energocomm.ru/crm/company/show/' . $companyID . '/">' . $companydata['TITLE'] . '</a>  инициировал деактивацию ККТ ' . $arParams["KKM_REG_ID"] . '.</p>',
            "From: help_bitrix@energocomm.ru\r\n"
            . "Content-type: text/html; charset=utf-8\r\n"
            . "X-Mailer: PHP mail script"
        );
        return true;
    }

    //Функция обработки сообщения с типом kkt_tu_tariff
    function kktTuTariff($onemsg)
    {
        //подключаемые модули
        CModule::IncludeModule('crm');
        CModule::IncludeModule('iblock');

        $oDeal          = new CCrmDeal;
        $CCrmEvent      = new CCrmEvent();
        $oIBlockElement = new CIBlockElement();
        $oCompany       = new CCrmCompany;

        $IBLOCK_ID_KKT  = getIblockIDByCode('registry_kkt');
        $IBLOCK_ID_Pay  = getIblockIDByCode('reestr_pay');

        $inn            = $this->parseParamsString("ИНН", $onemsg);
        $kpp            = $this->parseParamsString("KPP", $onemsg);
        $kkmRegId       = $this->parseParamsString("KKMRegId", $onemsg);
        $tariffName     = $this->parseParamsString("TariffName", $onemsg);
        $tariffPrice    = $this->parseParamsString("TariffPrice", $onemsg);
        $tariffPrice    = (int)$tariffPrice;
        $account        = $this->parseParamsString("Accaunt", $onemsg);
        $idProd         = $this->parseParamsString("ID", $onemsg);
        $tenant         = $this->parseParamsString("Tenant", $onemsg);


        //Определение ID тенант
        if(array_key_exists($tenant, $this->arTenantCompanyType)){
            $tenant = $this->arTenantCompanyType[$tenant];
        } else {
            $tenant = "CUSTOMER";
        }

        /** Проверка на наличие обязательных полей */

        switch (strlen($inn)) {
            case 10: //Организация
                break;
            case 12: //ИП
                break;
            default:
                $this->error .= "Не валидный ИНН: " . $inn;
                return $inn;
                break;
        }

        if (empty($kkmRegId)) {
            $this->error .= "Отсутствует KKMRegId. Завершаем обработку. ";
            return true;
        }


        /** Поиск компании по реквизитам ИНН и КПП */

        $requisite = new \Bitrix\Crm\EntityRequisite();
        $fieldsInfo = $requisite->getFormFieldsInfo();
        $select = array_keys($fieldsInfo);

        $arFilter = array(
            'RQ_INN'            => $inn,
            'RQ_KPP'            => $kpp,
            'ENTITY_TYPE_ID'    => 4
        );

        $res = $requisite->getList(
            array(
                'order'     => array('SORT' => 'ASC', 'ID' => 'ASC'),
                'filter'    => $arFilter,
                'select'    => $select
            )
        );

        //компания найдена
        if ($row = $res->fetch()) {
            $companyID  = $row['ENTITY_ID'];
        }


        /** Поиск/обновление/создание ККТ по RegID */

        $arKKT = $this->findDublicateKKT($kkmRegId);

        if(empty($companyID)){
            $companyID = $this->addOneKKT($onemsg,false);
            $isCreateInAddOneKKT = true;
        }

        if($companyID === true){
            $this->error .= "Ошибка создания Компании/Сделки/ККТ";
            return true;
        }

        /**Получаем данные с последнего события в компании из реестра платежей*/

        $arSelect = Array("ID", "NAME","PROPERTY_*");
        $arFilter = Array("IBLOCK_ID" => $IBLOCK_ID_Pay, "PROPERTY_COMPANY" => $companyID);
        $res = CIBlockElement::GetList(Array("ID"=>"DESC"), $arFilter, false, false, $arSelect);

        if($ob = $res->GetNextElement()) {

            $arFields                    = $ob->GetFields();
            $arProps                     = $ob->GetProperties();

            $arTable['ID']              = $arFields['ID'];
            $arTable['NAME']            = $arFields['NAME'];
            $arTable['SOSTOYANIE']      = $arProps['SOSTOYANIE']['VALUE'];
        }


        /** Получаем id отвественного на текущий момент по контрагенту */

        $resCompany = $oCompany->GetListEx(array("ID" => "ASC"), array("ID" => $companyID), array("ASSIGNED_BY_ID"));
        $assignedID = 372; //по-умолчанию bitrix
        if ($fieldsCompany = $resCompany->Fetch()) {
            $assignedID = $fieldsCompany['ASSIGNED_BY_ID'];
            $this->status .= "Получен id ответственного [$assignedID]. ";
        }
        if(!isset($arTable['SOSTOYANIE']) || $account != $arTable['SOSTOYANIE']) {
            $arFields = array(
                "ACTIVE"              => "Y",
                "IBLOCK_ID"           => $IBLOCK_ID_Pay,
                "NAME"                => "Привязка ККТ",
                "PROPERTY_VALUES"     => array(
                    "RNM_KKT"         => $kkmRegId,
                    "INN"             => (!empty($inn)) ? $inn : "",
                    "KPP"             => (!empty($kpp)) ? $kpp : "",
                    "LAST"            => getPropertyEnumIdByXmlId($IBLOCK_ID_Pay, "LAST_ACTUALIZATION"),
                    "TYPE"            => getPropertyEnumIdByXmlId($IBLOCK_ID_Pay, "TYPE_ACTUALIZE"),
                    "COMPANY"         => $companyID,
                    "ASSIGNED"        => $assignedID,
                    "ACTIVATION_DATE" => date("d.m.Y"),
                    "SUMM"            => "0",
                    "SOSTOYANIE"      => (!empty($account)) ? $account : "0",
                )
            );

            if ($idElement = $oIBlockElement->Add($arFields)) {
                $this->status .= "Создан элемент в реестре платежей id " . $idElement . ". ";
            } else {
                $this->error .= "Ошибка создания элемента в реестре платежей. ";
            }
        }

        //Если вызвали addOneKKT, то:
        // - ККТ не обновляем
        // - Историю не создаем.
        if($isCreateInAddOneKKT === true){
            return true;
        }

        //Update idProd
        $arFields = array(
            "UF_ID_PROD" => $idProd
        );
        $oCompany->Update($companyID, $arFields);


        /** Обновление ККТ */
        $arProperty = [];
        (!empty($tariffName)) ?     ($arProperty['TARIF']       = $tariffName)  : "";
        (!empty($tariffPrice)) ?    ($arProperty['COST_TARIF']  = $tariffPrice) : "";

        $oIBlockElement->SetPropertyValuesEx(
            $arKKT["ID"],
            $IBLOCK_ID_KKT,
            $arProperty
        );
        $this->status .= "ККТ обновлёно " . $arKKT['ID'] . ". ";


        /** Обновление цены сделки*/

        $priceOldKKT = (int)$arKKT['PROPERTY_COST_TARIF_VALUE'];

        //Получение суммы сделки
        $arFilter = array(
            'ID' => $arKKT['PROPERTY_DEAL_VALUE'],
        );
        $resDublicateDeal = $oDeal->GetList(array('ID' => 'DESC'), $arFilter);

        if ($arDeal = $resDublicateDeal->Fetch()) {
            $priceCurrentDeal = (int)$arDeal['OPPORTUNITY'];
        }

        //Расчет  суммы сделки
        $newPriceDeal = $priceCurrentDeal - $priceOldKKT + $tariffPrice;
        $newPriceDeal = ((int)$newPriceDeal < 0) ? "0" : (int)$newPriceDeal;
        if ($priceCurrentDeal != $newPriceDeal) {
            $arFields['OPPORTUNITY'] = $newPriceDeal;

            //Обновление сделки
            $resUpdate = $oDeal->Update($arKKT['PROPERTY_DEAL_VALUE'], $arFields);
            if ($resUpdate) {
                $this->status .= "Обновлена сделка (сумма) ID:" . $arKKT['PROPERTY_DEAL_VALUE'] . ". ";
            } else {
                $this->error .= "Ошибка обновления суммы сделки ID:" . $arKKT['PROPERTY_DEAL_VALUE'] . ". ";
            }
        }


        /** Создание истории для сделки */

        $idEventHistory = $CCrmEvent->Add(
            array(
                'ENTITY_TYPE' => 'DEAL',
                'ENTITY_ID' => $arKKT['PROPERTY_DEAL_VALUE'],
                'EVENT_ID' => 'INFO',
                'EVENT_TEXT_1' => 'Привязан ККТ с регистрационным номером: ' . $kkmRegId .
                    ', Названием тарифа: ' . $tariffName . ', Ценой тарифа: ' . $tariffPrice,
            )
        );

        if ($idEventHistory) {
            $this->status .= "История сделки успешно создана. ";
        } else {
            $this->error .= "Ошибка создания истории сделки. ";
        }

        return true;
    }

    //Функция обработки сообщения с типом add_one_kkt
    function addOneKKT($onemsg,$isAddReestrPay = true)
    {
        //подключаемые модули
        CModule::IncludeModule('crm');
        CModule::IncludeModule('iblock');

        $oLead            = new CCrmLead;
        $oDeal            = new CCrmDeal;
        $oCompany         = new CCrmCompany;
        $CCrmEvent        = new CCrmEvent();
        $oIBlockElement   = new CIBlockElement();

        $isLead           = false;

        $IBLOCK_ID_KKT    = getIblockIDByCode('registry_kkt');
        $IBLOCK_ID_Pay    = getIblockIDByCode('reestr_pay');

        $inn              = $this->parseParamsString("ИНН", $onemsg);
        $inn              = (!empty($inn)) ? $inn : $this->parseParamsString("INN", $onemsg);
        $kpp              = $this->parseParamsString("КПП", $onemsg);
        $kpp              = (!empty($kpp)) ? $kpp : $this->parseParamsString("KPP", $onemsg);
        $kpp			  = (empty($kpp)) ? "" : $kpp;
        $modelKKT         = $this->parseParamsString("Model_KKT", $onemsg);
        $kkmSerialNumber  = $this->parseParamsString("KkmSerialNumber", $onemsg);
        $activationDate   = $this->parseParamsString("Activation Date", $onemsg);
        $activationDate   = $this->dateCut($activationDate);
        $agentCode        = $this->parseParamsString("AgentCode", $onemsg);
        $promoCode        = $this->parseParamsString("PromoCode", $onemsg);
        $activationPeriod = $this->parseParamsString("ActivationPeriod", $onemsg);
        $kkmRegId         = $this->parseParamsString("KKMRegId", $onemsg);
        $numFN            = $this->parseParamsString("Num_FN", $onemsg);
        $nameKKT          = $this->parseParamsString("Name_KKT", $onemsg);
        $setupAddress     = $this->parseParamsString("SetupAddres", $onemsg);
        $tariffName       = $this->parseParamsString("TariffName", $onemsg);
        $tariffPrice      = $this->parseParamsString("TariffPrice", $onemsg);
        $tariffPrice      = (int)$tariffPrice;
        $accaunt          = $this->parseParamsString("Accaunt", $onemsg);
        $idProd           = $this->parseParamsString("ID", $onemsg);
        $tenant           = $this->parseParamsString("Tenant", $onemsg);


        //Определение ID тенант
        if(array_key_exists($tenant, $this->arTenantCompanyType)){
            $tenant = $this->arTenantCompanyType[$tenant];
        } else {
            $tenant = "CUSTOMER";
        }

        /** Проверка на наличие обязательных полей */

        switch (strlen($inn)) {
            case 10:
                break;
            case 12:
                break;
            default:
                $this->error = "Не валидный ИНН: " . $inn;
                return $inn;
                break;
        }

        if(empty($kkmRegId)){
            return true;
        }


        /** Поиск и обновление Лида */

        //Поиск Лида по ИНН
        $resMultiADD = $oLead->GetList(array('ID' => 'asc'), array(
            'TITLE'             => 'регистрация на сайте www.1ofd.ru',
            'UF_CRM_1499414186' => $inn
        ));
        if ($arMultiADD = $resMultiADD->Fetch()) {
            $isLead = true;
            $arSelectedLead = array(
                "ID"            => $arMultiADD['ID'],
                "LAST_NAME"     => ($arMultiADD['LAST_NAME'] == "noname")? "" : $arMultiADD['LAST_NAME'],
                "NAME"          => ($arMultiADD['NAME'] == "noname")? "" : $arMultiADD['NAME'],
                "SECOND_NAME"   => ($arMultiADD['SECOND_NAME'] == "noname")? "" : $arMultiADD['SECOND_NAME'],
            );
        } else {
            $this->status .= "Лид по ИНН не найден. ";
        }

        //Переводим лид в статус Выбран Тариф
        if(!empty($isLead)) {
            $arFields = Array(
                "STATUS_ID"     => "3", // статус @Выбран Тариф@
            );

            $oLead->Update($arSelectedLead['ID'], $arFields);
            $this->status .= "Лид переведён в статус «Выбран Тариф» ID (" . $arSelectedLead['ID']."). ";
        }

        //Добавление истории Лида
        if(!empty($isLead)) {
            $idEventHistory = $CCrmEvent->Add(
                array(
                    'ENTITY_TYPE' => 'LEAD',
                    'ENTITY_ID' => $arSelectedLead['ID'],
                    'EVENT_ID' => 'INFO',
                    'EVENT_TEXT_1' => 'Подключён ККТ с регистрационным номером ' . $kkmRegId,
                )
            );
            if ($idEventHistory) {
                $this->status .= "История в Лиде успешно создана. ";
            } else {
                $this->error .= "Ошибка создания истории в Лиде. ";
            }
        }


        /** Поиск/создание компании по реквизитам ИНН и КПП */

        $requisite = new \Bitrix\Crm\EntityRequisite();
        $fieldsInfo = $requisite->getFormFieldsInfo();
        $select = array_keys($fieldsInfo);

        $arFilter = array(
            'RQ_INN' => $inn,
            'RQ_KPP' => $kpp,
            'ENTITY_TYPE_ID' => 4
        );

        $res = $requisite->getList(
            array(
                'order' => array('SORT' => 'ASC', 'ID' => 'ASC'),
                'filter' => $arFilter,
                'select' => $select
            )
        );

        //Компания найдена
        $assignedIDCompany = 372;
        if ($row = $res->fetch()) {
            $companyID = $row['ENTITY_ID'];
            $this->status .= "Найдена существующая компания ID (" . $companyID . "). ";

            //Получаем id отвественного по компании
            $resCompany = $oCompany->GetListEx(array("ID" => "ASC"), array("ID" => $companyID), array("ASSIGNED_BY_ID"));
            if ($fieldsCompany = $resCompany->Fetch()) {
                $assignedIDCompany = $fieldsCompany['ASSIGNED_BY_ID'];
            }

            //Обновление компании: обновление ID_PROD и Тенант.
            $arFields = array(
                "COMPANY_TYPE"      => $tenant,
                "UF_ID_PROD"        => $idProd,
                "UF_CRM_INN"        => $inn,
                "UF_CRM_KPP"        => $kpp,
            );
            if($oCompany->Update($companyID, $arFields)){
                $this->status .= "Обновление компании($companyID): обновление ID_PROD и Тенант. ";
            } else {
                $this->error .= "Ошибка обновления компании($companyID). ";
            }
        }
        else //Компания не найдена, создаем компанию
        {
            //Получение информации от налоговой по ИНН компании
            $arCompanyInfo = $this->GetInfo($inn);
            //Добавление компании на основании данных от Налоговой
            if(array_key_exists(0, $arCompanyInfo) && !empty($arCompanyInfo)){
                $info = Array(
                    'fields' => Array(
                        'RQ_INN'            => $inn,
                        'RQ_KPP'            => (!empty($kpp)) ? $kpp : "",
                        'RQ_OKVED'          => $arCompanyInfo[0]['fields']['RQ_OKVED'],
                        'SORT'              => 500,
                        'ENTITY_TYPE_ID'    => 4,
                    )
                );

                $ogrn = (!empty($arCompanyInfo[0]['fields']['RQ_OGRNIP'])) ? $arCompanyInfo[0]['fields']['RQ_OGRNIP'] : $arCompanyInfo[0]['fields']['RQ_OGRN'];
                $ogrn = trim($ogrn);
                $guid = RestBpmBitrix::generate_guid();
                switch (strlen($ogrn)){
                    case 15: //ИП
                        $info['fields']['NAME']                     = 'ИП';
                        $info['fields']['RQ_OGRNIP']                = $ogrn;
                        $info['fields']['PRESET_ID']                = 2;
                        $info['fields']['RQ_LAST_NAME']             = $arCompanyInfo[0]['fields']['RQ_LAST_NAME'];
                        $info['fields']['RQ_FIRST_NAME']            = $arCompanyInfo[0]['fields']['RQ_FIRST_NAME'];
                        $info['fields']['RQ_SECOND_NAME']           = $arCompanyInfo[0]['fields']['RQ_SECOND_NAME'];

                        $arFields['TITLE']          = $arCompanyInfo[0]['fields']['RQ_NAME'];
                        $arFields['UF_FULL_NAME']   = $arCompanyInfo[0]['fields']['RQ_NAME'];
                        $arFields['COMPANY_TITLE']  = $arCompanyInfo[0]['fields']['RQ_NAME'];
                        $arFields["UF_CRM_INN"] = $inn;
                        $arFields["UF_CRM_COMPANY_GUID"] = $guid;
                        $arFields["UF_CRM_OGRN_API"] = $ogrn;
                        break;
                    default: //Организация
                        $info['fields']['NAME']                     = 'Организация';
                        $info['fields']['RQ_COMPANY_NAME']          = (!empty($arCompanyInfo[0]['fields']['RQ_COMPANY_NAME'])) ? $arCompanyInfo[0]['fields']['RQ_COMPANY_NAME'] : $arCompanyInfo[0]['fields']['RQ_COMPANY_FULL_NAME'];
                        $info['fields']['RQ_COMPANY_FULL_NAME']     = (!empty($arCompanyInfo[0]['fields']['RQ_COMPANY_FULL_NAME'])) ? $arCompanyInfo[0]['fields']['RQ_COMPANY_FULL_NAME'] : "";
                        $info['fields']['RQ_OGRN']                  = $ogrn;
                        $info['fields']['PRESET_ID']                = 1;

                        $arFields['TITLE']          = $arCompanyInfo[0]['fields']['RQ_COMPANY_NAME'];
                        $arFields['UF_FULL_NAME']   = $arCompanyInfo[0]['fields']['RQ_COMPANY_FULL_NAME'];
                        $arFields['COMPANY_TITLE']  = $arCompanyInfo[0]['fields']['RQ_COMPANY_NAME'];
                        $arFields["UF_CRM_INN"] = $inn;
                        $arFields["UF_CRM_KPP"] = $kpp;
                        $arFields["UF_CRM_COMPANY_GUID"] = $guid;
                        $arFields["UF_CRM_OGRN_API"] = $ogrn;
                        break;
                }

                $arFields["ASSIGNED_BY_ID"] = 372;
                $arFields["UF_STATUS"] = 264;
                $arFields['COMPANY_TYPE']   = $tenant;
                $arFields['UF_ID_PROD']     = $idProd;
                $arFields['UF_CRM_INN']     = $inn;
                $arFields['UF_CRM_KPP']     = $kpp;

                //Создаем компанию
                $arFields['UF_PAYMENT'] = 269; //GK из биллинга

                if($companyID = $oCompany->Add($arFields)){
                    $this->status .= "Создали компанию: $companyID. ";
                } else {
                    $this->error .= "Не удалось создать компанию. ";
                    return true;
                }

                $info['fields']['ENTITY_ID'] = $companyID;

                if($resGk = gk_AddRQ($info)){
                    $this->status .= "Успешно добавлены реквизиты для компании $companyID. ";
                    addRelationship($inn, $companyID);
                } else {
                    $this->error .= "Ошибка добавления реквизитов для компании $companyID. ";
                }

            }

            //Добавляем компанию, если нет информации от Налоговой
            if(empty($arCompanyInfo)){
                $arFields = array(
                    "TITLE"                 => "Организация",
                    "UF_CRM_INN"            => $inn,
                    "UF_CRM_KPP"            => $kpp,
                    "UF_CRM_COMPANY_GUID"   => $guid,
                    "ASSIGNED_BY_ID"        => 372,
                    "COMPANY_TYPE"          => $tenant,
                    "UF_ID_PROD"            => $idProd
                );

                //Создаем компанию
                $arFields['UF_PAYMENT'] = 269; //GK из биллинга

                if($companyID = $oCompany->Add($arFields)){
                    $this->status .= "Создали компанию: $companyID. ";
                } else {
                    $this->error .= "Не удалось создать компанию. ";
                    return true;
                }

                //TODO сделать проверку по длине ИНН и добавлять соответственный реквизит.

                //Добавляем реквизиты
                $info = Array(
                    'fields' => Array(
                        'NAME'              => "Организация",
                        'RQ_INN'            => $inn,
                        'RQ_KPP'            => $kpp,
                        'SORT'              => 500,
                        'ENTITY_TYPE_ID'    => 4,
                        'ENTITY_ID'         => $companyID,
                        'PRESET_ID'         => 1,
                    )
                );

                if($resGk = gk_AddRQ($info)){
                    $this->status .= "Успешно добавлены реквизиты для компании $companyID. ";
                    addRelationship($inn, $companyID);
                } else {
                    $this->error .= "Ошибка добавления реквизитов для компании $companyID. ";
                }

            }
        }


        /** Поиск/создание сделки по ID компании */

        $priceCurrentDeal = 0;
        $countKKT = 0;

        $arDeal = $this->findOptimalDealKktByCompanyID($companyID);

        //Сделка найдена
        if ($arDeal && $arDeal['ID']) {
            $idDeal = $arDeal['ID'];
            $priceCurrentDeal = (int)$arDeal['OPPORTUNITY'];
            $countKKT = (int)$arDeal['UF_KKT'];
            $countKKT = (empty($countKKT)) ? 0 : $countKKT;
            $this->status .= "Найдена сделка ID: $idDeal. ";
        }
        else //Сделка не найдена
        {
            $arFields = Array(
                "TITLE"                => "Основная услуга ОФД",
                "STAGE_ID"             => "C1:NEGOTIATION", //Подписан договор
                "ASSIGNED_BY_ID"       => 372,
                "CATEGORY_ID"          => 1, //Направление  - клиент
                'TYPE_ID'              => 2, //Тип: Основная услуга ОФД
                'COMPANY_ID'           => $companyID,
                "UF_KKT"               => 0, //кол-во ККТ
            );
            $idDeal = $oDeal->Add($arFields);
            if($idDeal){
                $this->status .= "Создали сделку ID: $idDeal.";
            }else{
                $this->error .= "Ошибка создания сделки. ";
                return true;
            }
        }


        /** Поиск/удаление/создание ККТ по RegID и DealID */

//При пустом ID компании пропускаем работу с ККТ
        if (!empty($companyID)) {
            $priceDeletedKKT = 0;
            $priceNewKKT = $tariffPrice;
            $arKKT = $this->findDublicateKKT($kkmRegId);
            //Удаление дубликата ККТ
            $isDeleteKKT = false;
            //TODO заменить удаление ККТ на обновление.
            if (!empty($arKKT) && !empty($arKKT['ID'])) {
                CIBlockElement::Delete($arKKT['ID']);
                $this->status .= "Найден дубликат в реестре ККТ, дубликат удалён. ";
                //Если ID сделки равняется ID сделки в реестре ККТ, значит делаем инкримент цены в сделке.
                if ($idDeal == $arKKT['PROPERTY_DEAL_VALUE']) {
                    $priceDeletedKKT = (int)$arKKT['PROPERTY_COST_TARIF_VALUE'];
                    $isDeleteKKT = true;
                }
            }
            //Если ID текущей сделки не равен ID сделки из реестра ККТ, тогда делаем дикримент суммы и кол-ва ККТ у старой сделки.
            if (($idDeal != $arKKT['PROPERTY_DEAL_VALUE']) && !empty($arKKT['PROPERTY_DEAL_VALUE'])) {
                //Получаем сумму и кол-во ККТ старой сделки
                $arFilter = array(
                    'ID' => $arKKT['PROPERTY_DEAL_VALUE'],
                );
                $resDublicateDeal = CCrmDeal::GetList(array('ID' => 'DESC'), $arFilter);

                if ($arDeal = $resDublicateDeal->Fetch()) {
                    $priceOldDeal = $arDeal['OPPORTUNITY'];
                    $actualPriceOldDeal = (int)$priceOldDeal - (int)$arKKT['PROPERTY_COST_TARIF_VALUE'];
                    $actualPriceOldDeal = ((int)$actualPriceOldDeal < 0) ? "0" : (int)$actualPriceOldDeal;

                    $countKktOldDeal = $arDeal['UF_KKT'];
                    $actualCountKktOldDeal = (int)$countKktOldDeal - 1;
                    $actualCountKktOldDeal = ((int)$actualCountKktOldDeal < 0) ? "0" : (int)$actualCountKktOldDeal;


                    //Обновляем сделки
                    $arFields = Array(
                        'UF_KKT' => $actualCountKktOldDeal,
                        'OPPORTUNITY' => $actualPriceOldDeal
                    );

                    $resUpdate = $oDeal->Update($arKKT['PROPERTY_DEAL_VALUE'], $arFields);
                    if ($resUpdate) {
                        $this->status .= "Обновлена старая сделка (сумма/кол-во_ККТ) ID: $arKKT[PROPERTY_DEAL_VALUE].";
                    } else {
                        $this->error .= "Ошибка обновления суммы/кол-ва_ККТ старой сделки ID: $arKKT[PROPERTY_DEAL_VALUE]. ";
                    }
                }
            }
            if (empty($kkmRegId)) {
                $this->error .= "Работа с ККТ прерввана. У ККТ нет kkmRegId. ";
                return true;
            }

            //Добавляем ККТ
            $arFields = array(
                "ACTIVE" => "Y",
                "IBLOCK_ID" => $IBLOCK_ID_KKT,
                "NAME" => $nameKKT . ' Модель (' . $modelKKT . ') C\Н (' . $kkmSerialNumber . ') Р\Н (' . $kkmRegId . ')',
                "PROPERTY_VALUES" => array(
                    "COMPANY" => $companyID,
                    "DEAL" => (!empty($idDeal)) ? $idDeal : "",
                    "RNM_KKT" => $kkmRegId,
                    "MODEL_KKT" => (!empty($modelKKT)) ? $modelKKT : "",
                    "NUM_KKT" => (!empty($kkmSerialNumber)) ? $kkmSerialNumber : "",
                    "NUM_FN" => (!empty($numFN)) ? $numFN : "",
                    "ADDR_KKT" => (!empty($setupAddress)) ? $setupAddress : "",
                    'ACTIVE' => getPropertyEnumIdByXmlId($IBLOCK_ID_KKT, "AC_NO"),
                    "TARIF" => (!empty($tariffName)) ? $tariffName : "",
                    "COST_TARIF" => (!empty($tariffPrice)) ? $tariffPrice : "",
                    "NAIMENOVANIE_KKT" => (!empty($nameKKT)) ? $nameKKT : "",
                    "DATE_ACTIVE" => "",
                    "DATE_CONNECT" => (!empty($activationDate)) ? $activationDate : date("d.m.Y"),
                    "CODE_AGENT" => (!empty($agentCode)) ? $agentCode : "",
                    "PROMO_KOD" => (!empty($promoCode)) ? $promoCode : "",
                    "PERIOD_ACTIVE" => (!empty($activationPeriod)) ? $activationPeriod : "",
                )
            );

            //Формирование актуального кол-ва ККТ
            if (empty($isDeleteKKT)) {
                $countKKT++;
            }

            if ($kktID = $oIBlockElement->Add($arFields)) {
                $this->status .= "Добавили ККТ ID: $kktID. ";
                $verify = KktBitrixBpm::eventSelectKkt($kktID, null);
                KktBitrixBpm::addKkt($verify);
            } else {
                $this->error .= "Ошибка добавления ККТ. ";
                return true;
            }
        } else {
            $this->error .= "Работа с ККТ прерввана. У ККТ нет контрагента. ";
            return true;
        }

        //Обновление сделки: Сумма, кол-во ККТ, тип сделки(основная услуга ОФД)
        $arFields = Array(
            'UF_KKT'  => $countKKT,
            'TYPE_ID' => 2 //тип сделки - основная услуга ОФД
        );

        //Расчет  суммы сделки
        $newPriceDeal = $priceCurrentDeal + $priceNewKKT - $priceDeletedKKT;
        $newPriceDeal = ((int)$newPriceDeal < 0)? "0" : (int)$newPriceDeal;

        if($priceCurrentDeal != $newPriceDeal) {
            $arFields['OPPORTUNITY'] =  $newPriceDeal;
        }

        $resUpdate = $oDeal->Update($idDeal, $arFields);
        if ($resUpdate) {
            $this->status .= "Обновлена сделка (сумма/кол-во_ККТ) ID: $idDeal. ";
        } else {
            $this->error .= "Ошибка обновления суммы/кол-ва_ККТ сделки ID: $idDeal. ";
        }


        /**Получаем данные с последнего события в контрагенте из реестра платежей*/

        $arSelect = Array("ID", "NAME","PROPERTY_*");
        $arFilter = Array("IBLOCK_ID" => $IBLOCK_ID_Pay, "PROPERTY_COMPANY" => $companyID);
        $res = CIBlockElement::GetList(Array("ID"=>"DESC"), $arFilter, false, false, $arSelect);

        if($ob = $res->GetNextElement()) {

            $arFields                    = $ob->GetFields();
            $arProps                     = $ob->GetProperties();

            $arTable['ID']              = $arFields['ID'];
            $arTable['NAME']            = $arFields['NAME'];
            $arTable['SOSTOYANIE']      = $arProps['SOSTOYANIE']['VALUE'];
        }


        if(empty($arTable['SOSTOYANIE']) || $accaunt != $arTable['SOSTOYANIE']) {

            /** Создание реестра платежей */
            if ($isAddReestrPay === true) {

                $arFields = array(
                    "ACTIVE"              => "Y",
                    "IBLOCK_ID"           => $IBLOCK_ID_Pay,
                    "NAME"                => "Добавление ККТ",
                    "PROPERTY_VALUES"     => array(
                        "INN"             => $inn,
                        "KPP"             => $kpp,
                        "LAST"            => getPropertyEnumIdByXmlId($IBLOCK_ID_Pay, "LAST_ACTUALIZATION"),//"1222",
                        "TYPE"            => getPropertyEnumIdByXmlId($IBLOCK_ID_Pay, "TYPE_ACTUALIZE"),//"1227",
                        "COMPANY"         => $companyID,
                        "ASSIGNED"        => $assignedIDCompany,
                        "RNM_KKT"         => $kkmRegId,
                        "ACTIVATION_DATE" => date("d.m.Y"),
                        "SUMM"            => "0",
                        "SOSTOYANIE"      => (!empty($accaunt)) ? $accaunt : "0.00",
                    )
                );
                if ($idElement = $oIBlockElement->Add($arFields)) {
                    $this->status .= "Создан элемент в реестре платежей id " . $idElement . ". ";
                } else {
                    $this->error .= "Ошибка создания элемента в реестре платежей. ";
                }
            }
        }


        /** Создание истории для сделки */

        $idEventHistory = $CCrmEvent->Add(
            array(
                'ENTITY_TYPE' => 'DEAL',
                'ENTITY_ID' => $idDeal,
                'EVENT_ID' => 'INFO',
                'EVENT_TEXT_1' => 'Подключён ККТ с регистрационным номером ' . $kkmRegId,
            )
        );

        if ($idEventHistory) {
            $this->status .= "История сделки успешно создана. ";
        } else {
            $this->status .= "Ошибка создания истории сделки. ";
        }

        return $companyID;
    }

    //Функция обработки сообщения с типом Full Account
    function fullAccount($onemsg) {
        CModule::IncludeModule('crm');

        $this->status = "";
        $this->error = "";

        $oLead      = new CCrmLead;
        $oContact   = new CCrmContact;
        $oDeal      = new CCrmDeal;
        $oCompany   = new CCrmCompany;
        $CCrmEvent  = new CCrmEvent();

        $isLead     = false;
        $isDeal     = false;
        $isContact  = false;
        $isCompany  = false;

        $singer = $this->parseParamsString("Singer", $onemsg);

        $arParams = array(
            "INN"               => $this->parseParamsString("ИНН", $onemsg),
            "EMAIL"             => $this->parseParamsString("Email", $onemsg), //Пока не приходит в FullAccount
            "PHONE"             => false, //Пока не приходит в FullAccount
            "KPP"               => (empty($this->parseParamsString("KPP", $onemsg)))? "" : $this->parseParamsString("KPP", $onemsg),
            "ORG_NAME"          => $this->parseParamsString("Org_name", $onemsg),
            "TITLE"             => $this->parseParamsString("Title", $onemsg),
            "OGRN"              => $this->parseParamsString("OGRN\/OGRNIP", $onemsg),
            "OKVD"              => $this->parseParamsString("OKVD", $onemsg),
            "REG_ADDRESS"       => $this->parseParamsString("Reg_address", $onemsg),
            "FACT_ADDRESS"      => $this->parseParamsString("Fact_address", $onemsg),
            "CORR_ADDRESS"      => $this->parseParamsString("Corr_address", $onemsg),
            "SINGER"            => $singer,
            "LAST_NAME"         => $this->getLastName($singer),
            "NAME"              => $this->getName($singer),
            "SECOND_NAME"       => $this->getSecondName($singer),
            "OFFER_NUM"         => $this->parseParamsString("Offer_Num", $onemsg),
            "OFFER_START_DATE"  => $this->parseParamsString("Offer_start_date", $onemsg),
            "TENANT"            => $this->parseParamsString("Tenant", $onemsg),
            "ACCAUNT"           => $this->parseParamsString("Accaunt", $onemsg),
            "ID_PROD"           => $this->parseParamsString("ID", $onemsg),
        );

        //Определение ID тенант
        if(array_key_exists($arParams['Tenant'], $this->arTenantCompanyType)){
            $arParams['TENANT'] = $this->arTenantCompanyType[$arParams['TENANT']];
        } else {
            $arParams['TENANT'] = "CUSTOMER";
        }

        $idElementList = getIdElementListState ('XML_STATE_LEAD_PAUSE');

        //Проверка на валидность ИНН
        switch (strlen($arParams['INN'])) {
            case 10:
                break;
            case 12:
                break;
            default:
                $this->error = "Не валидный ИНН: " . $arParams['INN'];
                return $arParams['INN'];
                break;
        }

        //Поиск Лида по ИНН
        $resMultiADD = $oLead->GetList(array('ID' => 'asc'), array(
            'TITLE'             => 'регистрация на сайте www.1ofd.ru',
            'UF_CRM_1499414186' => $arParams['INN']
        ));
        if ($arMultiADD = $resMultiADD->Fetch()) {
            $isLead = true;
            $arSelectedLead = array(
                "ID"            => $arMultiADD['ID'],
                "LAST_NAME"     => ($arMultiADD['LAST_NAME'] == "noname")? "" : $arMultiADD['LAST_NAME'],
                "NAME"          => ($arMultiADD['NAME'] == "noname")? "" : $arMultiADD['NAME'],
                "SECOND_NAME"   => ($arMultiADD['SECOND_NAME'] == "noname")? "" : $arMultiADD['SECOND_NAME'],
            );
        }

        // Поиск лида по Email. Ищем если не удалось найти лида по ИНН
        if(empty($isLead) && !empty($arParams['EMAIL'])) {
            $idLead = $this->leadDublicateCheck('регистрация на сайте www.1ofd.ru', $arParams['EMAIL']);
            if(!empty($idLead)) {
                $isLead = true;

                $resMultiADD = $oLead->GetList(array('ID' => 'asc'), array('ID' => $idLead));
                if ($arMultiADD = $resMultiADD->Fetch()) {
                    $arSelectedLead = array(
                        "ID"            => $arMultiADD['ID'],
                        "LAST_NAME"     => ($arMultiADD['LAST_NAME'] == "noname")? "" : $arMultiADD['LAST_NAME'],
                        "NAME"          => ($arMultiADD['NAME'] == "noname")? "" : $arMultiADD['NAME'],
                        "SECOND_NAME"   => ($arMultiADD['SECOND_NAME'] == "noname")? "" : $arMultiADD['SECOND_NAME'],
                    );
                }

                //Обновляем Лид, добавляем ИНН.
                $arFields = array("UF_CRM_1499414186" => $arParams['INN']);
                $resUpdate = $oLead->Update($arSelectedLead['ID'], $arFields);
                if($resUpdate) {
                    $this->status .= "Лид успешно обновлен (" . $arSelectedLead['ID'] . "). ";
                } else {
                    $this->error .= "Ошибка обновления лида: (" . $arSelectedLead['ID'] . ")";
                }
            }
        }

        //Лида нет, создаем лид и добавляем всю необходимую информацию.
        if(empty($isLead) && !empty($arParams['EMAIL'])){
            $arFields = Array(
                "TITLE" 			=> "регистрация на сайте www.1ofd.ru",
                "NAME" 				=> $arParams['NAME'],
                "LAST_NAME" 		=> $arParams['LAST_NAME'],
                "SECOND_NAME" 		=> $arParams['SECOND_NAME'],
                "UF_AUTO"           => 'Y',
                "UF_TYPE" 			=> "3414",
                "UF_CHANNEL"        => "5274",
                "SOURCE_ID" 		=> "24",
                "STATUS_ID" 		=> "2", //Статус - Заключен договор
                "ASSIGNED_BY_ID" 	=> 372,
                "UF_CRM_1499414186" => $arParams['INN'],
                "UF_STATE_LEAD"     => $idElementList,
                "FM" => Array(
                    "EMAIL" => Array(
                        "n0" => Array(
                            "VALUE" 		=> $arParams['EMAIL'],
                            "VALUE_TYPE" 	=> "WORK",
                        ),
                    ),
                    //"PHONE" => Array(
                    //    "n0" => Array(
                    //        "VALUE" 		=> $phone,
                    //        "VALUE_TYPE" 	=> "WORK",
                    //    ),
                    //),
                ),
            );
            if($LEAD_ID = $oLead->Add($arFields)){
                $isLead = true;
                $arSelectedLead = array(
                    "ID"            => $LEAD_ID,
                    "LAST_NAME"     => $arParams['LAST_NAME'],
                    "NAME"          => $arParams['NAME'],
                    "SECOND_NAME"   => $arParams['SECOND_NAME'],
                );
                $this->status .= "Лид успешно добавлен (" . $LEAD_ID . "). ";
            } else {
                $this->error .= "Ошибка создания лида. ";
            }
        }

        //Лида и Email нет, создать лид не можем.
        if(empty($isLead) && empty($arParams['EMAIL'])){
            $this->status .= "Лида найти не удалось. ";
        }

        $arSelectedLead['EMAIL'] = false;
        $arSelectedLead['PHONE'] = false;
        if($isLead){
            $ResSelectLeadEmail = CCrmFieldMulti::GetList(
                array('ID' => 'asc'), array('ENTITY_ID' => 'LEAD', 'ELEMENT_ID' => $arSelectedLead['ID'])
            );
            while ($arSelectLeadEmail = $ResSelectLeadEmail->Fetch()) {
                if($arSelectLeadEmail['TYPE_ID'] == 'EMAIL'){
                    $arSelectedLead['EMAIL'] = $arSelectLeadEmail['VALUE'];
                }
                if($arSelectLeadEmail['TYPE_ID'] == 'PHONE'){
                    $arSelectedLead['PHONE'] = $arSelectLeadEmail['VALUE'];
                }
            }
        }

        //Переводим лид в статус зарегестрирован в лк
        if(!empty($isLead)) {
            $arFields = Array(
                "STATUS_ID" => "2", //Заключен договор
            );

            $oLead->Update($arSelectedLead['ID'], $arFields);
            $this->status .= "Лид переведён в статус зарегистрирован в ЛК ID(" . $arSelectedLead['ID'] . "). ";
        }


        /** Поиск компании по реквизитам ИНН и КПП */

        $requisite = new \Bitrix\Crm\EntityRequisite();
        $fieldsInfo = $requisite->getFormFieldsInfo();
        $select = array_keys($fieldsInfo);

        $arFilter = array(
            'RQ_INN' => $arParams['INN'],
            'RQ_KPP' => $arParams['KPP'],
            'ENTITY_TYPE_ID' => 4
        );

        $res = $requisite->getList(
            array(
                'order' => array('SORT' => 'ASC', 'ID' => 'ASC'),
                'filter' => $arFilter,
                'select' => $select
            )
        );

        $guid = RestBpmBitrix::generate_guid();
        //Компания найдена
        if ($row = $res->fetch()) {

            $RQ_ID = $row['ID'];
            $companyID = $row['ENTITY_ID'];
            $this->status .= "Найден существующий контрагент ID (" . $companyID . "). ";

            //Обновление компании: обновление ID_PROD и Тенант.
            $arFields = array(
                "COMPANY_TYPE"      => $arParams['TENANT'],
                "UF_ID_PROD"        => $arParams["ID_PROD"],
                "UF_CRM_INN"        => $arParams["INN"],
                "UF_CRM_KPP"        => $arParams["KPP"],
            );
            if($oCompany->Update($companyID, $arFields)){
                $this->status .= "Обновление компании($companyID): обновление ID_PROD и Тенант. ";
            } else {
                $this->error .= "Ошибка обновления компании($companyID). ";
            }


            //Получение информации от налоговой по ИНН компании
            $START_TIME_FNS = microtime();
            $arCompanyInfo = $this->GetInfo($arParams['INN']);
            $TIME_FNS = (microtime() - $START_TIME_FNS);
            echo "\nВремя обработки запроса из ФНС, сек: $TIME_FNS \n";

            if(array_key_exists(0, $arCompanyInfo) && empty(!$arCompanyInfo)) {
                $info = Array(
                    'fields' => Array(
                        'RQ_INN' => $arParams['INN'],
                        'RQ_KPP' => (!empty($arParams['KPP'])) ? $arParams['KPP'] : "",
                        //'RQ_KPP' => (!empty($arCompanyInfo['RQ_KPP'])) ? $arCompanyInfo['RQ_KPP'] : $arParams['KPP'],
                        'RQ_OKVED' => $arParams['OKVD'],
                        'UF_CRM_1499413026' => $arParams['FACT_ADDRESS'], //фактический адрес
                        'UF_CRM_1499413051' => $arParams['CORR_ADDRESS'], //Корреспондентский адрес
                        'SORT' => 500,
                        'ENTITY_TYPE_ID' => 4,
                        'ENTITY_ID' => $companyID,
                    )
                );

                $ogrn = (!empty($arCompanyInfo['RQ_OGRNIP'])) ? $arCompanyInfo['RQ_OGRNIP'] : $arParams['OGRN'];
                $ogrn = trim($ogrn);
                switch (strlen($ogrn)){
                    case 15: //ИП
                        $info['fields']['NAME']                     = 'ИП';
                        $info['fields']['RQ_OGRNIP']                = $ogrn;
                        $info['fields']['PRESET_ID']                = 2;
                        $info['fields']['RQ_LAST_NAME']             = $arCompanyInfo[0]['fields']['RQ_LAST_NAME'];
                        $info['fields']['RQ_FIRST_NAME']            = $arCompanyInfo[0]['fields']['RQ_FIRST_NAME'];
                        $info['fields']['RQ_SECOND_NAME']           = $arCompanyInfo[0]['fields']['RQ_SECOND_NAME'];
                        break;
                    default: //Организация
                        $info['fields']['NAME']                     = 'Организация';
                        $info['fields']['RQ_COMPANY_NAME']          = (!empty($arCompanyInfo[0]['fields']['RQ_COMPANY_NAME'])) ? $arCompanyInfo[0]['fields']['RQ_COMPANY_NAME'] : $arParams['ORG_NAME'];
                        $info['fields']['RQ_COMPANY_FULL_NAME']     = (!empty($arCompanyInfo[0]['fields']['RQ_COMPANY_FULL_NAME'])) ? $arCompanyInfo[0]['fields']['RQ_COMPANY_FULL_NAME'] : $arParams['TITLE'];
                        $info['fields']['RQ_OGRN']                  = $ogrn;
                        $info['fields']['PRESET_ID']                = 1;
                        break;
                }

                if (empty($info['fields']['RQ_COMPANY_NAME'])) {
                    $info['fields']['RQ_COMPANY_NAME'] = $info['fields']['RQ_COMPANY_FULL_NAME'];
                }

                if ($resGk = gk_UpdateRQ($RQ_ID, $info)) {
                    $this->status .= "Успешно обновлены реквизиты для компании $companyID . ";
                    addRelationship($arParams['INN'], $companyID);

                } else {
                    $this->error .= "Ошибка обновления реквизитов для компании $companyID . ";
                }
            }

            //Add history in Lead
            if($isLead) {

                $idEventHistory = $CCrmEvent->Add(
                    array(
                        'ENTITY_TYPE' => 'LEAD',
                        'ENTITY_ID' => $arSelectedLead['ID'],
                        'EVENT_ID' => 'INFO',
                        'EVENT_TEXT_1' => 'Зарегистрирована полноценная учетная запись',
                    )
                );

                if ($idEventHistory) {
                    $this->status .= "Успешно создана история для Лида. ";
                } else {
                    $this->error .= "Ошибка создания история для Лида. ";
                }
            }

            //Add history in Company
            $idEventHistory = $CCrmEvent->Add(
                array(
                    'ENTITY_TYPE' => 'COMPANY',
                    'ENTITY_ID' => $companyID,
                    'EVENT_ID' => 'INFO',
                    'EVENT_TEXT_1' => 'Зарегистрирована полноценная учетная запись',
                )
            );

            if ($idEventHistory) {
                $this->status .= "Успешно создана история для Компании. ";
            } else {
                $this->error .= "Ошибка создания история для Компании. ";
            }

            // Требование: если фио нет, либо указано noname, то контакт не создается
            $idSelectedContact = false;

            //Поиск контакта по ФИО пришедшего из сообщения и EMAIL
            if(!empty($arParams['SINGER']) && !empty($arParams['EMAIL'])){
                //Поиск дубликата
                $resMultiDublicateContact = CCrmFieldMulti::GetList(
                    array('ID' => 'asc'), array('ENTITY_ID' => 'CONTACT', 'TYPE_ID' => 'EMAIL', 'VALUE' => $arParams['EMAIL'])
                );
                while ($arMultiDublicateContact = $resMultiDublicateContact->Fetch()) {

                    //Проверяем на совпадение по ФИО
                    $arFilter = array(
                        "ID"            => $arMultiDublicateContact['ELEMENT_ID'],
                        "LAST_NAME"     => $arParams['LAST_NAME'],
                        "NAME"          => $arParams['NAME'],
                        "SECOND_NAME"   => $arParams['SECOND_NAME'],
                    );
                    $arResContact = $oContact->GetList(array(), $arFilter);
                    if(intval($arResContact->SelectedRowsCount()) > 0) {
                        $isContact = true;
                        $idSelectedContact = $arMultiDublicateContact['ELEMENT_ID'];
                        break;
                    }
                }
            }

            //Поиск контакта по ФИО и Email из лида
            if( !empty($arSelectedLead['EMAIL']) &&
                empty($isContact) &&
                ( !empty($arSelectedLead['LAST_NAME']) || !empty($arSelectedLead['NAME']) || !empty($arSelectedLead['SECOND_NAME'])))
            {
                //Поиск дубликата
                $resMultiDublicateContact = CCrmFieldMulti::GetList(
                    array('ID' => 'asc'), array('ENTITY_ID' => 'CONTACT', 'TYPE_ID' => 'EMAIL', 'VALUE' => $arSelectedLead['EMAIL'])
                );
                while ($arMultiDublicateContact = $resMultiDublicateContact->Fetch()) {

                    //Проверяем на совпадение по ФИО
                    $arFilter = array(
                        "ID"            => $arMultiDublicateContact['ELEMENT_ID'],
                        "LAST_NAME"     => $arSelectedLead['LAST_NAME'],
                        "NAME"          => $arSelectedLead['NAME'],
                        "SECOND_NAME"   => $arSelectedLead['SECOND_NAME'],
                    );
                    $arResContact = $oContact->GetList(array(), $arFilter);

                    if(intval($arResContact->SelectedRowsCount()) > 0) {
                        $isContact = true;
                        $idSelectedContact = $arMultiDublicateContact['ELEMENT_ID'];
                        break;
                    }
                }
            }

            $isnewContact = false;
            //Если контакта нет, добавляем его
            if(empty($isContact)){
                $name = "";
                $mobilephone = "";
                $email = "";
                //Создаем контакт на основании данных из письма
                if(!empty($arParams['SINGER']) && !empty($arParams['EMAIL'])){

                    $name = $arParams['NAME'] . " " . $arParams['LAST_NAME'];
                    $mobilephone = (empty($arParams['PHONE'])) ? "" : $arParams['PHONE'];
                    $email = (empty($arParams['EMAIL'])) ? "" : $arParams['EMAIL'];

                    $arContact = Array(
                        'NAME'              => $arParams['NAME'],
                        'SECOND_NAME'       => $arParams['SECOND_NAME'],
                        'LAST_NAME'         => $arParams['LAST_NAME'],
                        'ASSIGNED_BY_ID'    => 372,
                        'COMPANY_ID'        => $companyID,
                        "FM" => Array(
                            "EMAIL" => Array(
                                "n0" => Array(
                                    "VALUE"         => (empty($arParams['EMAIL']))? "" : $arParams['EMAIL'],
                                    "VALUE_TYPE"    => "WORK",
                                ),
                            ),
                            "PHONE" => Array(
                                "n0" => Array(
                                    //"VALUE"         => (empty($arParams['PHONE']))? "" : $arParams['PHONE'], //Временно отключено, пока не будет приходить телефон в письме
                                    "VALUE"         => (empty($arSelectedLead['PHONE']))? "" : $arSelectedLead['PHONE'], //Временно добавлено проставление телефона из лида, пока не будет приходить телефон в письме
                                    "VALUE_TYPE"    => "WORK",
                                ),
                            ),
                        ),
                    );
                    if($idCreateContact = $oContact->Add($arContact)){
                        $this->status .= "Создан новый контакт на основании данных из письма: $idCreateContact . ";
                        $idSelectedContact = $idCreateContact;
                        $isContact = true; $isnewContact = true;
                    } else {
                        $this->error .= "Ошибка создания нового контакта. ";
                    }
                }

                //Создаем контакт на основании данных из лида
                if( !empty($arSelectedLead['EMAIL']) &&
                    empty($isContact) &&
                    ( !empty($arSelectedLead['LAST_NAME']) || !empty($arSelectedLead['NAME']) || !empty($arSelectedLead['SECOND_NAME'])))
                {
                    $name = $arSelectedLead['NAME'] . " " . $arSelectedLead['LAST_NAME'];
                    $mobilephone = (empty($arSelectedLead['PHONE'])) ? "" : $arSelectedLead['PHONE'];
                    $email = (empty($arSelectedLead['EMAIL'])) ? "" : $arSelectedLead['EMAIL'];

                    $arContact = Array(
                        'NAME'              => $arSelectedLead['NAME'],
                        'SECOND_NAME'       => $arSelectedLead['SECOND_NAME'],
                        'LAST_NAME'         => $arSelectedLead['LAST_NAME'],
                        'ASSIGNED_BY_ID'    => 372,
                        'COMPANY_ID'        => $companyID,
                        "FM" => Array(
                            "EMAIL" => Array(
                                "n0" => Array(
                                    "VALUE"         => (empty($arSelectedLead['EMAIL']))? "" : $arSelectedLead['EMAIL'],
                                    "VALUE_TYPE"    => "WORK",
                                ),
                            ),
                            "PHONE" => Array(
                                "n0" => Array(
                                    "VALUE"         => (empty($arSelectedLead['PHONE']))? "" : $arSelectedLead['PHONE'],
                                    "VALUE_TYPE"    => "WORK",
                                ),
                            ),
                        ),
                    );
                    if($idCreateContact = $oContact->Add($arContact)){
                        $this->status .= "Создан новый контакт на основании данных из лида: $idCreateContact . ";
                        $idSelectedContact = $idCreateContact;
                        $isContact = true;
                        $isnewContact = true;
                    } else {
                        $this->error .= "Ошибка создания нового контакта. ";
                    }
                }
            }

            //Отправка контакта в BPM
            if ($isnewContact) {
                $guidperson = strtolower(RestBpmBitrix::generate_guid());
                $entity_id = "CRM_CONTACT";
                $uf_guid = "UF_BPMCONTACTID";
                SetUserField($entity_id, $idSelectedContact, $uf_guid, $guidperson);
                $guidcompany = GetUserField("CRM_COMPANY", $companyID, "UF_CRM_COMPANY_GUID");
                ContactBpmBitrix::contactInsertToBMP($idSelectedContact, $guidperson, $guidcompany, $name, $mobilephone, $email);
            }

            //Поиск дубликата сделки
            $arSelectedDeal = $this->findOptimalDealKktByCompanyID($companyID);

            //Обновляем сделку
            if(!empty($arSelectedDeal)) {
                //Получение текущего количеста ККТ
                $countKKT = 0;
                $resDealCountKKT = CCrmDeal::GetList(array('ID'=>'desc'),array('ID' => $arSelectedDeal['ID']), array("ID", "UF_KKT"));
                if ($arDealKKT = $resDealCountKKT->Fetch()) {
                    $countKKT = (int)$arDealKKT['UF_KKT'];
                    $countKKT = (empty($countKKT)) ? 0 : $countKKT;
                }

                $idDeal = $arSelectedDeal['ID'];
                $arFieldsUpdateDeal = Array(
                    'STAGE_ID'  => "C1:NEGOTIATION", // Стадия сделки: Подписание договора
                    'UF_KKT'    => $countKKT,
                    'TYPE_ID'   => 2, //Тип: Основная услуга ОФД
                );
                if(!empty($isContact)){
                    $arFieldsUpdateDeal['CONTACT_ID'] = $idSelectedContact;
                }
                $idUpdateDeal = $oDeal->Update($arSelectedDeal['ID'], $arFieldsUpdateDeal);
                if($idUpdateDeal) {
                    $isDeal = true;
                    $this->status .= "Сделка успешно обновлена: $arSelectedDeal[ID] . ";
                } else {
                    $this->error .= "Ошибка при обновлении сделки: $arSelectedDeal[ID] . ";
                }
            }
            else //Создаем сделку
            {
                $arFieldsDeal = Array(
                    "TITLE"                 => "Основная услуга ОФД",
                    "STAGE_ID"              => "C1:NEGOTIATION", //Стадия сделки: Подписание договора
                    "ASSIGNED_BY_ID"        => 372,
                    "CATEGORY_ID"           => 1, //Направление клиент
                    'TYPE_ID'               => 2, //Тип: Основная услуга ОФД
                    'COMPANY_ID'            => $companyID,
                    'UF_KKT'                => 0, //кол-во ККТ
                );
                if(!empty($isContact)){
                    $arFieldsUpdateDeal['CONTACT_ID'] = $idSelectedContact;
                }
                $idCreateDeal = $oDeal->Add($arFieldsDeal);
                if($idCreateDeal) {
                    $isDeal = true;
                    $idDeal = $idCreateDeal;
                    $this->status .= "Сделка успешно создана: $idCreateDeal . ";
                } else {
                    $this->error .= "Ошибка при создании сделки. ";
                }
            }

            //Создание активности
            if(!empty($isDeal)) {
                $idEventHistory = $CCrmEvent->Add(
                    array(
                        'ENTITY_TYPE'   => 'DEAL',
                        'ENTITY_ID'     => $idDeal,
                        'EVENT_ID'      => 'INFO',
                        'EVENT_TEXT_1'  => 'Зарегистрирована полноценная учетная запись',
                    )
                );

                if ($idEventHistory) {
                    $this->status .= "Успешно создана история для Сделки. ";
                } else {
                    $this->error .= "Ошибка создания история для Сделки. ";
                }
            }

        } else {

            $arFields = Array(
                "TITLE"                 => $arParams['TITLE'],
                "UF_CRM_INN"            => $arParams['INN'],
                "UF_CRM_KPP"            => $arParams['KPP'],
                "UF_CRM_COMPANY_GUID"   => $guid,
                "UF_CRM_OGRN_API"       => $arParams['OGRN'],
                "UF_STATUS"             => '264',
                "COMPANY_TYPE"          => $arParams['TENANT'],
                "UF_FULL_NAME"          => $arParams['TITLE'],
                "ASSIGNED_BY_ID"        => 372,
                "COMPANY_TITLE"         => $arParams['ORG_NAME'],
                "UF_ID_PROD"            => $arParams["ID_PROD"],
            );

            if(!empty($arParams['EMAIL']) || !empty($arSelectedLead['EMAIL'])){
                $arFields['FM']['EMAIL'] =  Array(
                    "n0" => Array(
                        "VALUE"         => (!empty($arParams['EMAIL'])) ? $arParams['EMAIL'] : $arSelectedLead['EMAIL'],
                        "VALUE_TYPE"    => "WORK",
                    ),
                );
            }

            if(!empty($arParams['PHONE'] || !empty($arSelectedLead['PHONE']))){
                $arFields['FM']['PHONE'] =  Array(
                    "n0" => Array(
                        "VALUE"         => (!empty($arParams['PHONE'])) ? $arParams['PHONE'] : $arSelectedLead['PHONE'],
                        "VALUE_TYPE"    => "WORK",
                    ),
                );
            }
            $arFields['UF_PAYMENT'] = 269; //GK из биллинга

            if($companyID = $oCompany->Add($arFields)){
                $this->status .= "Создали компанию: $companyID . ";

            } else {
                $this->error .= "Не удалось создать компанию. ";
                return true; //Добавить обработку ошибок
            }

            //Получение информации от налоговой по ИНН компании
            $START_TIME_FNS = microtime(); 
            $arCompanyInfo = $this->GetInfo($arParams['INN']);
            $TIME_FNS = (microtime() - $START_TIME_FNS);
            echo "\nВремя обработки запроса из ФНС, сек: $TIME_FNS \n";

            if(array_key_exists(0, $arCompanyInfo) && empty(!$arCompanyInfo)){
                $info = Array(
                    'fields' => Array(
                        'RQ_INN'                    => $arParams['INN'],
                        'RQ_KPP'                    => (!empty($arParams['KPP'])) ? $arParams['KPP'] : "",
                        //'RQ_KPP'                    => (!empty($arCompanyInfo[0]['fields']['RQ_KPP'])) ? $arCompanyInfo[0]['fields']['RQ_KPP'] : $arParams['KPP'],
                        'RQ_OKVED'                  => $arParams['OKVD'],
                        'UF_CRM_1499413026'         => $arParams['FACT_ADDRESS'], //фактический адрес
                        'UF_CRM_1499413051'         => $arParams['CORR_ADDRESS'], //Корреспондентский адрес
                        'SORT'                      => 500,
                        'ENTITY_TYPE_ID'            => 4,
                        'ENTITY_ID'                 => $companyID,
                    )
                );

                $ogrn = (!empty($arCompanyInfo[0]['fields']['RQ_OGRNIP'])) ? $arCompanyInfo[0]['fields']['RQ_OGRNIP'] : $arParams['OGRN'];
                $ogrn = trim($ogrn);
                switch (strlen($ogrn)){
                    case 15: //ИП
                        $info['fields']['NAME']                     = 'ИП';
                        $info['fields']['RQ_OGRNIP']                = $ogrn;
                        $info['fields']['PRESET_ID']                = 2;
                        $info['fields']['RQ_LAST_NAME']             = $arCompanyInfo[0]['fields']['RQ_LAST_NAME'];
                        $info['fields']['RQ_FIRST_NAME']            = $arCompanyInfo[0]['fields']['RQ_FIRST_NAME'];
                        $info['fields']['RQ_SECOND_NAME']           = $arCompanyInfo[0]['fields']['RQ_SECOND_NAME'];
                        break;
                    default: //Организация
                        $info['fields']['NAME']                     = 'Организация';
                        $info['fields']['RQ_COMPANY_NAME']          = (!empty($arCompanyInfo[0]['fields']['RQ_COMPANY_NAME'])) ? $arCompanyInfo[0]['fields']['RQ_COMPANY_NAME'] : $arParams['ORG_NAME'];
                        $info['fields']['RQ_COMPANY_FULL_NAME']     = (!empty($arCompanyInfo[0]['fields']['RQ_COMPANY_FULL_NAME'])) ? $arCompanyInfo[0]['fields']['RQ_COMPANY_FULL_NAME'] : $arParams['TITLE'];
                        $info['fields']['RQ_OGRN']                  = $ogrn;
                        $info['fields']['PRESET_ID']                = 1;
                        break;
                }

                if(empty($info['fields']['RQ_COMPANY_NAME'])){
                    $info['fields']['RQ_COMPANY_NAME'] = $info['fields']['RQ_COMPANY_FULL_NAME'];
                }

                if($resGk = gk_AddRQ($info)){
                    $this->status .= "Успешно добавлены реквизиты для компании $companyID . ";
                    addRelationship($arParams['INN'], $companyID);

                } else {
                    $this->error .= "Ошибка добавления реквизитов для компании $companyID . ";
                }

            }

            //Добавляем реквизиты для компании, если нет информации от Налоговой
            if(empty($arCompanyInfo)){

                //TODO добавить проверку на ИП/Организация. На основании полученной инфы создать нужный реквизит

                //Добавляем реквизиты
                $info = Array(
                    'fields' => Array(
                        'NAME'              => "ИП",
                        'RQ_INN'            => $arParams['INN'],
                        'RQ_KPP'            => (!empty($arParams['KPP'])) ? $arParams['KPP'] : "",
                        'SORT'              => 500,
                        'ENTITY_TYPE_ID'    => 4,
                        'ENTITY_ID'         => $companyID,
                        'PRESET_ID'         => 2,
                        'RQ_OKVED'          => $arParams['OKVD'],
                        'RQ_OGRNIP'         => $arParams['OGRN'],
                        'UF_CRM_1499413026' => $arParams['FACT_ADDRESS'], //фактический адрес
                        'UF_CRM_1499413051' => $arParams['CORR_ADDRESS'], //Корреспондентский адрес
                        'RQ_LAST_NAME'      => $arParams['LAST_NAME'],
                        'RQ_FIRST_NAME'     => $arParams['NAME'],
                        'RQ_SECOND_NAME'    => $arParams['SECOND_NAME']
                    )
                );

                if($resGk = gk_AddRQ($info)){
                    $this->status .= "Успешно добавлены реквизиты для компании $companyID. ";
                    addRelationship($arParams['INN'], $companyID);
                } else {
                    $this->error .= "Ошибка добавления реквизитов для компании $companyID. ";
                }

            }

            //Add history in Lead
            if($isLead) {
                $idEventHistory = $CCrmEvent->Add(
                    array(
                        'ENTITY_TYPE' => 'LEAD',
                        'ENTITY_ID' => $arSelectedLead['ID'],
                        'EVENT_ID' => 'INFO',
                        'EVENT_TEXT_1' => 'Зарегистрирована полноценная учетная запись',
                    )
                );

                if ($idEventHistory) {
                    $this->status .= "Успешно создана история для Лида. ";
                } else {
                    $this->error .= "Ошибка создания история для Лида. ";
                }
            }

            //Add history in Company
            $idEventHistory = $CCrmEvent->Add(
                array(
                    'ENTITY_TYPE' => 'COMPANY',
                    'ENTITY_ID' => $companyID,
                    'EVENT_ID' => 'INFO',
                    'EVENT_TEXT_1' => 'Зарегистрирована полноценная учетная запись',
                )
            );

            if ($idEventHistory) {
                $this->status .= "Успешно создана история для Компании. ";
            } else {
                $this->error .= "Ошибка создания история для Компании. ";
            }

            // Требование: если фио нет, либо указано noname, то контакт не создается
            $idSelectedContact = false;

            //Поиск контакта по ФИО и Email пришедшего из письма
            if(!empty($arParams['SINGER']) && !empty($arParams['EMAIL'])){
                //Поиск дубликата
                $resMultiDublicateContact = CCrmFieldMulti::GetList(
                    array('ID' => 'asc'), array('ENTITY_ID' => 'CONTACT', 'TYPE_ID' => 'EMAIL', 'VALUE' => $arParams['EMAIL'])
                );
                while ($arMultiDublicateContact = $resMultiDublicateContact->Fetch()) {

                    //Проверяем на совпадение по ФИО
                    $arFilter = array(
                        "ID"            => $arMultiDublicateContact['ELEMENT_ID'],
                        "LAST_NAME"     => $arParams['LAST_NAME'],
                        "NAME"          => $arParams['NAME'],
                        "SECOND_NAME"   => $arParams['SECOND_NAME'],
                    );
                    $arResContact = $oContact->GetList(array(), $arFilter);
                    if(intval($arResContact->SelectedRowsCount()) > 0) {
                        $isContact = true;
                        $idSelectedContact = $arMultiDublicateContact['ELEMENT_ID'];
                        break;
                    }
                }
            }

            //Поиск контакта по ФИО и Email из лида
            if( !empty($arSelectedLead['EMAIL']) &&
                empty($isContact) &&
                ( !empty($arSelectedLead['LAST_NAME']) || !empty($arSelectedLead['NAME']) || !empty($arSelectedLead['SECOND_NAME'])))
            {
                //Поиск дубликата
                $resMultiDublicateContact = CCrmFieldMulti::GetList(
                    array('ID' => 'asc'), array('ENTITY_ID' => 'CONTACT', 'TYPE_ID' => 'EMAIL', 'VALUE' => $arSelectedLead['EMAIL'])
                );
                while ($arMultiDublicateContact = $resMultiDublicateContact->Fetch()) {

                    //Проверяем на совпадение по ФИО
                    $arFilter = array(
                        "ID"            => $arMultiDublicateContact['ELEMENT_ID'],
                        "LAST_NAME"     => $arSelectedLead['LAST_NAME'],
                        "NAME"          => $arSelectedLead['NAME'],
                        "SECOND_NAME"   => $arSelectedLead['SECOND_NAME'],
                    );
                    $arResContact = $oContact->GetList(array(), $arFilter);

                    if(intval($arResContact->SelectedRowsCount()) > 0) {
                        $isContact = true;
                        $idSelectedContact = $arMultiDublicateContact['ELEMENT_ID'];
                        break;
                    }
                }
            }

            $isnewContact = false;
            //Если контакта нет, добавляем его
            if(empty($isContact)){
                $name = "";
                $mobilephone = "";
                $email = "";
                //Создаем контакт на основании данных из письма
                if(!empty($arParams['SINGER']) && !empty($arParams['EMAIL'])){

                    $name = $arParams['NAME'] . " " . $arParams['LAST_NAME'];
                    $mobilephone = (empty($arParams['PHONE'])) ? "" : $arParams['PHONE'];
                    $email = (empty($arParams['EMAIL'])) ? "" : $arParams['EMAIL'];

                    $arContact = Array(
                        'NAME'              => $arParams['NAME'],
                        'SECOND_NAME'       => $arParams['SECOND_NAME'],
                        'LAST_NAME'         => $arParams['LAST_NAME'],
                        'ASSIGNED_BY_ID'    => 372,
                        'COMPANY_ID'        => $companyID,
                        "FM" => Array(
                            "EMAIL" => Array(
                                "n0" => Array(
                                    "VALUE"         => (empty($arParams['EMAIL']))? "" : $arParams['EMAIL'],
                                    "VALUE_TYPE"    => "WORK",
                                ),
                            ),
                            "PHONE" => Array(
                                "n0" => Array(
                                    //"VALUE"         => (empty($arParams['PHONE']))? "" : $arParams['PHONE'], //Временно отключено, пока не будет приходить телефон в письме
                                    "VALUE"         => (empty($arSelectedLead['PHONE']))? "" : $arSelectedLead['PHONE'], //Временно добавлено проставление телефона из лида, пока не будет приходить телефон в письме
                                    "VALUE_TYPE"    => "WORK",
                                ),
                            ),
                        ),
                    );
                    if($idCreateContact = $oContact->Add($arContact)) {
                        $this->status .= "Создан новый контакт на основании данных из письма: $idCreateContact . ";
                        $idSelectedContact = $idCreateContact;
                        $isContact = true;
                        $isnewContact= true;
                    } else {
                        $this->error .= "Ошибка создания нового контакта. ";
                    }
                }

                //Создаем контакт на основании данных из лида
                if( !empty($arSelectedLead['EMAIL']) &&
                    empty($isContact) &&
                    ( !empty($arSelectedLead['LAST_NAME']) || !empty($arSelectedLead['NAME']) || !empty($arSelectedLead['SECOND_NAME'])))
                {
                    $name = $arSelectedLead['NAME'] . " " . $arSelectedLead['LAST_NAME'];
                    $mobilephone = (empty($arSelectedLead['PHONE'])) ? "" : $arSelectedLead['PHONE'];
                    $email = (empty($arSelectedLead['EMAIL'])) ? "" : $arSelectedLead['EMAIL'];


                    $arContact = Array(
                        'NAME'              => $arSelectedLead['NAME'],
                        'SECOND_NAME'       => $arSelectedLead['SECOND_NAME'],
                        'LAST_NAME'         => $arSelectedLead['LAST_NAME'],
                        'ASSIGNED_BY_ID'    => 372,
                        'COMPANY_ID'        => $companyID,
                        "FM" => Array(
                            "EMAIL" => Array(
                                "n0" => Array(
                                    "VALUE"         => (empty($arSelectedLead['EMAIL']))? "" : $arSelectedLead['EMAIL'],
                                    "VALUE_TYPE"    => "WORK",
                                ),
                            ),
                            "PHONE" => Array(
                                "n0" => Array(
                                    "VALUE"         => (empty($arSelectedLead['PHONE']))? "" : $arSelectedLead['PHONE'],
                                    "VALUE_TYPE"    => "WORK",
                                ),
                            ),
                        ),
                    );
                    if($idCreateContact = $oContact->Add($arContact)){
                        $this->status .= "Создан новый контакт на основании данных из лида: $idCreateContact . ";
                        $idSelectedContact = $idCreateContact;
                        $isContact = true;
                        $isnewContact= true;
                    } else {
                        $this->error .= "Ошибка создания нового контакта. ";
                    }
                }
            }
            if ($isnewContact) {
                $guidperson = strtolower(RestBpmBitrix::generate_guid());
                $entity_id = "CRM_CONTACT";
                $uf_guid = "UF_BPMCONTACTID";
                SetUserField($entity_id, $idSelectedContact, $uf_guid, $guidperson);
                $guidcompany = GetUserField("CRM_COMPANY", $companyID, "UF_CRM_COMPANY_GUID");
                ContactBpmBitrix::contactInsertToBMP($idSelectedContact, $guidperson, $guidcompany, $name, $mobilephone, $email);
            }

            //Поиск дубликата сделки
            $arFilter = array(
                'CATEGORY_ID'   => '1', //Направление сделки продажа клиент
                'COMPANY_ID'    => $companyID,
            );

            $arSelectedDeal = $this->findDublicateDeal($arFilter);

            //Обновляем сделку
            if(!empty($arSelectedDeal)) {
                //Получение текущего количеста ККТ
                $countKKT = 0;
                $resDealCountKKT = CCrmDeal::GetList(array('ID'=>'desc'),array('ID' => $arSelectedDeal['ID']), array("ID", "UF_KKT"));
                if ($arDealKKT = $resDealCountKKT->Fetch()) {
                    $countKKT = (int)$arDealKKT['UF_KKT'];
                    $countKKT = (empty($countKKT)) ? 0 : $countKKT;
                }

                $idDeal = $arSelectedDeal['ID'];
                $arFieldsUpdateDeal = Array(
                    'STAGE_ID'  => "C1:NEGOTIATION", // Стадия сделки: Подписание договора
                    'UF_KKT'    => $countKKT
                );
                if(!empty($isContact)){
                    $arFieldsUpdateDeal['CONTACT_ID'] = $idSelectedContact;
                }

                $idUpdateDeal = $oDeal->Update($arSelectedDeal['ID'], $arFieldsUpdateDeal);
                if($idUpdateDeal) {
                    $isDeal = true;
                    $this->status .= "Сделка успешно обновлена: $arSelectedDeal[ID] . ";
                } else {
                    $this->error .= "Ошибка при обновлении сделки. ";
                }
            }
            else //Создаем сделку
            {
                $arFieldsDeal = Array(
                    "TITLE"                 => "Основная услуга ОФД",
                    "STAGE_ID"              => "C1:NEGOTIATION",
                    "ASSIGNED_BY_ID"        => 372,
                    "CATEGORY_ID"           => 1,
                    'TYPE_ID'               => 2, //Тип: Основная услуга ОФД
                    'COMPANY_ID'            => $companyID,
                    'UF_KKT'                => 0
                );
                if(!empty($isContact)){
                    $arFieldsUpdateDeal['CONTACT_ID'] = $idSelectedContact;
                }

                $idCreateDeal = $oDeal->Add($arFieldsDeal);
                if($idCreateDeal) {
                    $isDeal = true;
                    $idDeal = $idCreateDeal;
                    $this->status .= "Сделка успешно создана: $idCreateDeal . ";
                } else {
                    $this->error .= "Ошибка при создании сделки . ";
                }
            }

            //Создание активности
            if(!empty($isDeal)) {
                $idEventHistory = $CCrmEvent->Add(
                    array(
                        'ENTITY_TYPE'   => 'DEAL',
                        'ENTITY_ID'     => $idDeal,
                        'EVENT_ID'      => 'INFO',
                        'EVENT_TEXT_1'  => 'Зарегистрирована полноценная учетная запись',
                    )
                );

                if ($idEventHistory) {
                    $this->status .= "Успешно создана история для Сделки. ";
                } else {
                    $this->error .= "Ошибка создания история для Сделки";
                }
            }
            return true;
        }
    }

    //Функция обработки сообщения с типом Temporary Account
    function temporaryAccount($onemsg) {
        $CCRMLead = new CCrmLead();

        $FIO 		= $this->parseParamsString("FIO", $onemsg);
        $name 		= $this->getName($FIO);
        $lastName 	= $this->getLastName($FIO);
        $secondName = $this->getSecondName($FIO);

        $email 		= $this->parseParamsString("Email", $onemsg);
        $phone 		= $this->parseParamsString("Phone", $onemsg);
        $phone  	= str_replace(array("+", " "), "", $phone);
        $inn 		= $this->parseParamsString("ИНН", $onemsg);

        $idXML = 'XML_STATE_LEAD_PAUSE';
        $idElementList = getIdElementListState ($idXML);

        //Проверка на валидность ИНН
        switch (strlen($inn)) {
            case 10:
                break;
            case 12:
                break;
            default:
                $this->error = "Не валидный ИНН: " . $inn;
                return $inn;
                break;
        }

        //проверка по E-mail: существует ли лид с таким E-mail
        $leadId = $this->leadDublicateCheck('регистрация на сайте www.1ofd.ru', $email);

        if($leadId === false) {
            //создаем лид
            $arFields = Array(
                "TITLE" 			=> "регистрация на сайте www.1ofd.ru",
                "NAME" 				=> $name,
                "LAST_NAME" 		=> $lastName,
                "SECOND_NAME" 		=> $secondName,
                "UF_TYPE" 			=> "3414",
                "UF_CHANNEL"        => "5274",
                "SOURCE_ID" 		=> "24",
                "UF_AUTO"           => 'Y',
                "STATUS_ID" 		=> "ASSIGNED", //Заполнена полная форма регистрации
                "ASSIGNED_BY_ID" 	=> 372,
                "UF_CRM_1499414186" => $inn,
                "UF_STATE_LEAD"     => $idElementList,
                "FM" => Array(
                    "EMAIL" => Array(
                        "n0" => Array(
                            "VALUE" 		=> $email,
                            "VALUE_TYPE" 	=> "WORK",
                        ),
                    ),
                    "PHONE" => Array(
                        "n0" => Array(
                            "VALUE" 		=> $phone,
                            "VALUE_TYPE" 	=> "WORK",
                        ),
                    ),
                ),
            );
            if($LEAD_ID = $CCRMLead->Add($arFields)){
                $this->status = "Создан лид(Temporary Account): регистрация на сайте www.1ofd.ru. ID Lead: " . $LEAD_ID;

                $CCrmEvent = new CCrmEvent();
                $idEventHistory = $CCrmEvent->Add(
                    array(
                        'ENTITY_TYPE'=> 'LEAD',
                        'ENTITY_ID' => $LEAD_ID,
                        'EVENT_ID' => 'INFO',
                        'EVENT_TEXT_1' => 'Зарегистрированна временная учетная запись',
                    )
                );

                if ($idEventHistory) {
                    $this->status .= " Активность успешно создана.";
                }
            } else {
                $this->error ="Error: ".$CCRMLead->LAST_ERROR;
            }
        } else {
            //Обновляем лид
            $arFields = array(
                "UF_CRM_1499414186" => $inn,
                "UF_CHANNEL"        => "5274",
                "SOURCE_ID" 		=> "24",
                "STATUS_ID" 		=> "ASSIGNED" //Заполнена полная форма регистрации
            );

            if($CCRMLead->Update($leadId, $arFields)) {
                $this->status = "Добавлены реквизиты из TEMPRORARY_ACCOUNT ID Lead: " . $leadId . ".";

                $CCrmEvent = new CCrmEvent();
                $idEventHistory = $CCrmEvent->Add(
                    array(
                        'ENTITY_TYPE'=> 'LEAD',
                        'ENTITY_ID' => $leadId,
                        'EVENT_ID' => 'INFO',
                        'EVENT_TEXT_1' => 'Зарегистрированна временная учетная запись',
                    )
                );

                if ($idEventHistory) {
                    $this->status .= " Активность успешно создана.";
                }
            } else {
                $this->error = "Error: ".$CCRMLead->LAST_ERROR;
            }
        }
    }

    //Функция обработки сообщения с типом Event Type NewLead
    function eventTypeNewLead($onemsg) {
        $logger = Logger::getLogger("LeadAdd", "Lead/eventTypeNewLead.log");
        $logger->log('$onemsg = ' . $onemsg);
        $FIO 		= $this->parseParamsString("FIO", $onemsg);
        $name 		= $this->getName($FIO);
        $lastName 	= $this->getLastName($FIO);
        $secondName = $this->getSecondName($FIO);
        CModule::IncludeModule('crm');

        // START. HotFix email
        $email = "";
        preg_match("/Email: (.*?)UTM/i", $onemsg , $out);
        if(array_key_exists(1,$out)) {
            $email = str_replace(array("\r\n", "\r", "\n"), '',  strip_tags($out[1]));
            $email = trim($email);
        }
        // END. HotFix email
        $logger->log('$email = ' . $email);

        //$email 		= $this->parseParamsString("Email", $onemsg);
        $phone 		= $this->parseParamsString("Telefon", $onemsg);
        $phone      = preg_replace("/[^0-9]/", '', $phone);
        $utm 		= $this->parseParamsString("UTM", $onemsg);

        $dubLeads   = $this->leadDublicateCheck('регистрация на сайте www.1ofd.ru', $email);

        $emailFmId  = $this->getFmId($dubLeads, 'EMAIL');
        $phoneFmId  = $this->getFmId($dubLeads, 'PHONE');

        $idXML = 'XML_STATE_LEAD_PAUSE';
        $idElementList = getIdElementListState ($idXML);

        $logger->log('$dubLeads = ' . $dubLeads);
        if (!$dubLeads) {
            $oLead = new CCrmLead;

            $arFields = Array(
                "TITLE" 			=> "регистрация на сайте www.1ofd.ru",
                "NAME" 				=> $name,
                "LAST_NAME" 		=> $lastName,
                "SECOND_NAME" 		=> $secondName,
                "UF_TYPE" 			=> "3414", //Тип лида - клиент
                "UF_CHANNEL"        => "5274", //Канал - сайт
                "UF_STATE_LEAD"     => $idElementList,
                "UF_NEED"           => "3333", //тип потребности - Клиенты
                "SOURCE_ID" 		=> "24", //Форма регистрации на сайте 1-ofd
                "STATUS_ID" 		=> "NEW", //Статус - Распределение
                "UF_AUTO"           => 'Y',
                "ASSIGNED_BY_ID" 	=> 372,
                "FM" => Array(
                    "EMAIL" => Array(
                        "n0" => Array(
                            "VALUE" 		=> $email,
                            "VALUE_TYPE" 	=> "WORK",
                        ),
                    ),
                    "PHONE" => Array(
                        "n0" => Array(
                            "VALUE" 		=> $phone,
                            "VALUE_TYPE" 	=> "WORK",
                        ),
                    ),
                ),
            );
            if($LEAD_ID = $oLead->Add($arFields)){
                $this->status = "Создан лид:регистрация на сайте www.1ofd.ru. ID Lead: " . $LEAD_ID;
            } else {
                $this->error ="Error: ".$oLead->LAST_ERROR;
            }
        } else {
            //Обновляем лид
            $oLead = new CCrmLead;
            $arFields = Array(
                "NAME" 				=> $name,
                "LAST_NAME" 		=> $lastName,
                "SECOND_NAME" 		=> $secondName,
                "UF_CHANNEL"        => "5274",
                "UF_NEED"           => "3333",
                "SOURCE_ID" 		=> "24",
                "FM" => Array(
                    "EMAIL" => Array(
                        "$emailFmId" => Array(
                            "VALUE" 		=> $email,
                            "VALUE_TYPE" 	=> "WORK",
                        ),
                    ),
                    "PHONE" => Array(
                        "$phoneFmId" => Array(
                            "VALUE" 		=> $phone,
                            "VALUE_TYPE" 	=> "WORK",
                        ),
                    ),
                ),
            );

            if($oLead->Update($dubLeads, $arFields)) {
                $this->status = "Лид обновлён:регистрация на сайте www.1ofd.ru. ID Lead:" . $dubLeads . ".";
            } else {
                $this->error = "Error: ".$oLead->LAST_ERROR;
            }
        }
    }

    //Функция обработки сообщения с типом Event Type NewLeadSer
    function eventTypeNewLeadSer($onemsg) {

        $FIO 		   = $this->parseParamsString("ФИО", $onemsg);
        $name 		   = $this->getName($FIO);
        $lastName 	   = $this->getLastName($FIO);
        $secondName    = $this->getSecondName($FIO);

        $email 		   = $this->parseParamsString("E-mail", $onemsg);
        $phone 		   = $this->parseParamsString("Телефон", $onemsg);
        $phone         = preg_replace("/[^0-9]/", '', $phone);

        $countSer	   = $this->parseParamsString("Планируемое кол-во Сертификатов", $onemsg);
        $utm 		   = $this->parseParamsString("UTM", $onemsg);

        $idElementList = getIdElementListState ('XML_STATE_LEAD_ACTIVITY');

        CModule::IncludeModule('crm');
        $oLead    = new CCrmLead;
        $arFields = Array(
            "TITLE" 				=> "Запрос сертификатов на сайте www.1ofd.ru",
            "NAME" 					=> $name,
            "LAST_NAME" 			=> $lastName,
            "SECOND_NAME" 			=> $secondName,
            "STATUS_ID"             => "4", //Новый
            "SOURCE_DESCRIPTION" 	=> "Планируемое кол-во Сертификатов: " . $countSer,
            "UF_TYPE" 				=> "3414", //Тип лида - клиент
            "UF_CHANNEL"            => "5274", //Канал - сайт
            "UF_NEED"               => "3412", //тип потребности - Сертификат с КА
            "SOURCE_ID" 			=> "23", //Источник  Форма запроса сертификата на сайте 1-ofd
            "ASSIGNED_BY_ID" 		=> 372,
            "UF_STATE_LEAD"         => $idElementList,
            "FM" => Array(
                "EMAIL" => Array(
                    "n0" => Array(
                        "VALUE" 		=> $email,
                        "VALUE_TYPE" 	=> "WORK",
                    ),
                ),
                "PHONE" => Array(
                    "n0" => Array(
                        "VALUE" 		=> $phone,
                        "VALUE_TYPE" 	=> "WORK",
                    ),
                ),
            ),
        );

        //Добавляем значение для пользовательского свойства "Количество сертификатов"
        $arUserProp = getUserPropArray("XML_ID", "LEAD_SERT_COUNT");
        $arFields[$arUserProp["FIELD_NAME"]] = $countSer;

        if($LEAD_ID = $oLead->Add($arFields)){
            $this->status = "Создан лид: Запрос сертификатов на сайте www.1ofd.ru. ID Lead: " . $LEAD_ID;
        } else {
            $this->error ="Error: " . $oLead->LAST_ERROR;
        }

        return true;
    }

    //Функция обработки сообщения с типом NewLeadFN
    function newLeadFN($onemsg) {

        $FIO 		= $this->parseParamsString("Как к вам обращаться\?", $onemsg);
        $name 		= $this->getName($FIO);
        $lastName 	= $this->getLastName($FIO);
        $secondName = $this->getSecondName($FIO);

        $company    = $this->parseParamsString("Компания", $onemsg);
        $email 		= $this->parseParamsString("E-mail", $onemsg);
        $phone 		= $this->parseParamsString("Телефон", $onemsg);
        $phone      = preg_replace("/[^0-9]/", '', $phone);

        $countFN	= $this->parseParamsString("Планируемое кол-во комплектов", $onemsg);
        $utm 		= $this->parseParamsString("UTM", $onemsg);

        $idElementList = getIdElementListState ('XML_STATE_LEAD_ACTIVITY');

        CModule::IncludeModule('crm');
        $oLead = new CCrmLead;

        $arFields = Array(
            "TITLE"                 => "Запрос ФН на сайте www.1-ofd.ru",
            "COMPANY_TITLE"         => $company,
            "NAME"                  => $name ,
            "LAST_NAME"             => $lastName,
            "SECOND_NAME"           => $secondName,
            "STATUS_ID"             => "4", //Новый
            "SOURCE_DESCRIPTION"    => "Планируемое кол-во комплектов: " . $countFN,
            "UF_TYPE"               => "3414", //Тип лида - клиент
            "UF_CHANNEL"            => "5274", //Канал - сайт
            "UF_NEED"               => "3411", // тип потребности Продажа ФН
            "SOURCE_ID"             => "22", //Источник Форма запроса ФН на сайте 1-ofd
            "ASSIGNED_BY_ID"        => 372,
            "UF_STATE_LEAD"         => $idElementList,
            "FM" => Array(
                "EMAIL" => Array(
                    "n0" => Array(
                        "VALUE"         => $email,
                        "VALUE_TYPE"    => "WORK",
                    ),
                ),
                "PHONE" => Array(
                    "n0" => Array(
                        "VALUE"         => $phone,
                        "VALUE_TYPE"    => "WORK",
                    ),
                ),
            ),
        );

        if($LEAD_ID = $oLead->Add($arFields)){
            $this->status = "Создан лид:Запрос ФН на сайте www.1-ofd.ru ID (" . $LEAD_ID.")";
        } else {
            $this->error ="Error: " . $oLead->LAST_ERROR;
        }

        return true;
    }

    //Функция обработки сообщения с типом NewLeadBD
    function newLeadBD($onemsg) {

        $FIO 		= $this->parseParamsString("FIO", $onemsg);
        $name 		= $this->getName($FIO);
        $lastName 	= $this->getLastName($FIO);
        $secondName = $this->getSecondName($FIO);

        $email 		= $this->parseParamsString("Email", $onemsg);
        $phone 		= $this->parseParamsString("Telefon", $onemsg);
        $phone      = preg_replace("/[^0-9]/", '', $phone);

        $idElementList = getIdElementListState ('XML_STATE_LEAD_ACTIVITY');

        CModule::IncludeModule('crm');
        $oLead = new CCrmLead;

        $arFields = Array(
            "TITLE"                 => "Форма запроса на анализ (Биг дата)",
            "NAME"                  => $name ,
            "LAST_NAME"             => $lastName,
            "SECOND_NAME"           => $secondName,
            "STATUS_ID"             => "4", //Новый
            "UF_TYPE"               => "3414", //Тип лида - клиент
            "UF_CHANNEL"            => "5274", //Канал - сайт
            "UF_NEED"               => "3408", //Тип потребности - Не распределено
            "SOURCE_ID"             => "44", //Источник -  Форма запроса на анализ (Биг дата)
            "UF_AUTO"               => 'Y',
            "ASSIGNED_BY_ID"        => 372,
            "UF_STATE_LEAD"         => $idElementList,
            "FM" => Array(
                "EMAIL" => Array(
                    "n0" => Array(
                        "VALUE"         => $email,
                        "VALUE_TYPE"    => "WORK",
                    ),
                ),
                "PHONE" => Array(
                    "n0" => Array(
                        "VALUE"         => $phone,
                        "VALUE_TYPE"    => "WORK",
                    ),
                ),
            ),
        );

        if($LEAD_ID = $oLead->Add($arFields)){
            $this->status = "Создан лид: Форма запроса на анализ (Биг дата). ID:(" . $LEAD_ID.")";
        } else {
            $this->error ="Error: " . $oLead->LAST_ERROR;
        }

        return true;
    }

    // Функция обработки OrderKKT
    function orderKKT($onemsg){
        CModule::IncludeModule('crm');

        $CCrmEvent  = new CCrmEvent();
        $oLead      = new CCrmLead;

        $FIO 		= $this->parseParamsString("FIO", $onemsg);
        $name 		= $this->getName($FIO);
        $lastName 	= $this->getLastName($FIO);
        $secondName = $this->getSecondName($FIO);

        $modelKKT   = $this->parseParamsString("ModelKKT", $onemsg);
        $email 		= $this->parseParamsString("Email", $onemsg);
        $phone 		= $this->parseParamsString("Telefon", $onemsg);
        $phone      = preg_replace("/[^0-9]/", '', $phone);

        $idElementList = getIdElementListState ('XML_STATE_LEAD_ACTIVITY');

        $arFields = Array(
            "TITLE"                 => "Запрос на покупку ККТ",
            "NAME" 				    => (!empty($name)) ? $name : "",
            "LAST_NAME" 		    => (!empty($lastName)) ? $lastName : "",
            "SECOND_NAME" 		    => (!empty($secondName)) ? $secondName : "",
            "STATUS_ID"             => "4", //Новый
            "UF_TYPE"               => "3414", //Тип лида - клиент
            "UF_CHANNEL"            => "5274", //Канал - сайт
            "SOURCE_ID"             => "37", //Источник - Форма покупки ККТ на сайте 1-ofd
            "UF_AUTO"               => 'Y',
            "ASSIGNED_BY_ID"        => 372,
            "UF_STATE_LEAD"         => $idElementList,
            "COMMENTS"              => 'Модель ККТ «' . $modelKKT . '»',
            "FM" => Array(
                "EMAIL" => Array(
                    "n0" => Array(
                        "VALUE"         => (!empty($email)) ? $email : "",
                        "VALUE_TYPE"    => "WORK",
                    ),
                ),
                "PHONE" => Array(
                    "n0" => Array(
                        "VALUE"         => (!empty($phone)) ? $phone : "",
                        "VALUE_TYPE"    => "WORK",
                    ),
                ),
            ),
        );

        if($LEAD_ID = $oLead->Add($arFields)){
            $this->status = "Создан лид: запрос на покупку ККТ id (" . $LEAD_ID."). ";

            $idEventHistory = $CCrmEvent->Add(
                array(
                    'ENTITY_TYPE'  => 'LEAD',
                    'ENTITY_ID'    => $LEAD_ID,
                    'EVENT_ID'     => 'INFO',
                    'EVENT_TEXT_1' => "Получен запрос на покупку ККТ. Модель ККТ: «" . $modelKKT . "»",
                )
            );

            if ($idEventHistory) {
                $this->status .= " Активность успешно создана. ";
            }  else {
                $this->error .= " Ошибка создания активности. ";
            }

        } else {
            $this->error ="Error: " . $oLead->LAST_ERROR;
        }

        return true;

    }

    // Функция обработки OrderEDO
    function orderEDO($onemsg)
    {

        CModule::IncludeModule('crm');

        $CCrmEvent = new CCrmEvent();
        $oLead     = new CCrmLead;

        $parsedParams = [];

        foreach (["Email","NameCompany","documentID","INN","KPP","OperatorEDO","Telefon"] as $param){
            $parsedParams[$param] = $this->parseParamsString($param, $onemsg);
        }

        $parsedParams["Telefon"] = preg_replace("/[^0-9]/", '', $parsedParams["Telefon"]);

        $idElementList = getIdElementListState('XML_STATE_LEAD_ACTIVITY');

        $arFields = Array(
            "TITLE"          => "Запрос на подключение к ЭДО",
            "STATUS_ID"      => "4", //Новый
            "UF_TYPE"        => "3414", //Тип лида - клиент
            "UF_CHANNEL"     => "5274", //Канал - сайт
            "SOURCE_ID"      => "45", //Источник - Форма подключения к ЭДО 45
            "UF_AUTO"        => 'Y',
            "ASSIGNED_BY_ID" => 372,
            "UF_STATE_LEAD"  => $idElementList,
			"UF_CRM_1499414186" => $parsedParams["INN"],
            "UF_KPP"            => (empty($parsedParams["KPP"])) ? '' : $parsedParams["KPP"],
            "COMMENTS"       => "",
            "FM"             => Array(
                "EMAIL" => Array(
                    "n0" => Array(
                        "VALUE"      => ( ! empty($parsedParams["Email"])) ? $parsedParams["Email"] : "",
                        "VALUE_TYPE" => "WORK",
                    ),
                ),
                "PHONE" => Array(
                    "n0" => Array(
                        "VALUE"      => ( ! empty($parsedParams["Telefon"])) ? $parsedParams["Telefon"] : "",
                        "VALUE_TYPE" => "WORK",
                    ),
                ),
            ),
        );

        if ($LEAD_ID = $oLead->Add($arFields)) {
            $this->status = "Создан лид: Запрос на подключение к ЭДО id (" . $LEAD_ID . "). ";

            $idEventHistory = $CCrmEvent->Add(
                array(
                    'ENTITY_TYPE'  => 'LEAD',
                    'ENTITY_ID'    => $LEAD_ID,
                    'EVENT_ID'     => 'INFO',
                    'EVENT_TEXT_1' => "Получен запрос на подключение к ЭДО.",
                )
            );

            if ($idEventHistory) {
                $this->status .= " Активность успешно создана. ";
            } else {
                $this->error .= " Ошибка создания активности. ";
            }

            $params  = array(
                "inn"          => $parsedParams["INN"],
                "kpp"          => $parsedParams["KPP"],
                "company_name" => $parsedParams["NameCompany"],
                "operator_edo" => $parsedParams["OperatorEDO"],
                "document_id"  => $parsedParams["documentID"],
            );
            $pdfPath = $this->generatePDFfromTemplate($params);
            $docPath = $this->generateDOCfromTemplate($params);

            $arFilePDF = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"] . $pdfPath);
            $arFileDOC = CFile::MakeFileArray($docPath);

            $arFields  = array(
                "ACTIVE"          => "Y",
                "IBLOCK_ID"       => self::ORDERS_EDO_IBLOCK_ID,
                "NAME"            => $parsedParams["NameCompany"],
                "PROPERTY_VALUES" => array(
                    "INN"          => $parsedParams["INN"],
                    "KPP"          => $parsedParams["KPP"],
                    "EDO_OPERATOR" => $parsedParams["OperatorEDO"],
                    "EDO_ID"       => $parsedParams["documentID"],
                    "PHONE"        => $parsedParams["Telefon"],
                    "E_MAIL"       => $parsedParams["Email"],
                    "LID"          => $LEAD_ID,
                    "PDF_DOC"      => array($arFilePDF, $arFileDOC),
                )
            );
            $oElement  = new CIBlockElement();
            $oElement->Add($arFields, false, false, true);

        } else {
            $this->error = "Error: " . $oLead->LAST_ERROR;
        }

        return true;

    }

    //Функция генерации pdf
    function generatePDFfromTemplate($arVariables = [])
    {
        require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

        $arProperties = array(
            "DocumentSection"            => self::PDF_SECTION_DISK_ID,
            "DocDesignerDocumentSection" => self::PDF_SECTION_DISK_ID
        );

        $arProperties = array_merge($arProperties, $arVariables);

        $CDocDes  = new CDocDesignerProcessing();
        $arResult = $CDocDes->PrepareTable(array(LEAD_EDO, intval(self::PDF_TEMPLATE_ID)), $arProperties);

        $arResult['fname']    = "б/н";
        $arResult['fontSize'] = 8;
        $arResult['mime']     = 'PDF';
        $arResult['params']['tm'] = 20;
        $arResult['params']['bm'] = 20;

        $DocId   = date("Ymd_His") . rand(1, 1000); //Название файла
        $filePDF = $CDocDes->CreateFile($arResult, LEAD_EDO, $DocId);

        return $filePDF;
    }

    function generateDOCfromTemplate($arVariables = [])
    {
        require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

        $arProperties = array(
            "DocumentSection"            => PDF_SECTION_DISK_ID,
            "DocDesignerDocumentSection" => PDF_SECTION_DISK_ID,
            'Permission' => Array(),
            'TargetUser' => 'user_372',
            'ModifiedDocumentField' =>'' ,
            'DOCXNumerator' => 0,
            'DocumentSection' => PDF_SECTION_DISK_ID,
            'DocDesignerDogTemplateID' => self::DOCX_TEMPLATE_ID,
            '_DocDesignerTemplateFromList' => 1
        );

        foreach($arVariables as $vname => $vVal){
            $arProperties[$vname] = $vVal;
        }

        $CContract = new CDocDesignerContracts();
        $CDocDes = new CDocDesignerProcessing();
        $Word = new PHPWord();

        $path = $CContract->GetDocTemplate($arProperties['DocDesignerDogTemplateID'], $arProperties['_DocDesignerTemplateFromList']);
        $document = $Word->loadTemplate($_SERVER["DOCUMENT_ROOT"] . $path);

        foreach($arProperties as $NAME => $VALUE){
            $document->setValue($NAME, $CDocDes->win2uni($VALUE));
        }

        $DocId   = date("Ymd_His") . rand(1, 1000); //Название файла
        $fname = $_SERVER["DOCUMENT_ROOT"] . '/upload/htmls.docdesigner/LEAD_EDO/' . $DocId . ".docx";

        $dom = new DOMDocument();
        $xml = $document->getXML();
        $dom->loadXML($xml);

        $xml = $dom->saveXML();
        $document->setXML($xml);
        $document->save($fname);

        $Footer = new PHPWord();
        $document = $Footer->loadFooter($fname);
        $document->setValue('DOGOVOR_NUMBER', "");
        $document->setValue('DOGOVOR_DATE', "13 Июля 2018");
        $document->save($fname);

        $Header = new PHPWord();
        $document = $Footer->loadHeader($fname);
        $document->setValue('DOGOVOR_NUMBER', "");
        $document->setValue('DOGOVOR_DATE', "13 Июля 2018");
        $document->save($fname);

        if(file_exists($fname)){
            return $fname;
        } else {
            return false;
        }
    }


    // Функция обработки dopserv
    function dopServ($onemsg){
        CModule::IncludeModule('crm');

        $CCrmEvent  = new CCrmEvent();
        $oLead      = new CCrmLead;

        $FIO 		= $this->parseParamsString("FIO", $onemsg);
        $name 		= $this->getName($FIO);
        $lastName 	= $this->getLastName($FIO);
        $secondName = $this->getSecondName($FIO);

        $modelKKT   = $this->parseParamsString("Model", $onemsg);
        $email 		= $this->parseParamsString("Email", $onemsg);
        $phone 		= $this->parseParamsString("Telefon", $onemsg);
        $phone      = preg_replace("/[^0-9]/", '', $phone);

        $idElementList = getIdElementListState ('XML_STATE_LEAD_ACTIVITY');

        $arFields = Array(
            "TITLE"                 => "Запрос на доп. услуги",
            "NAME" 				    => (!empty($name)) ? $name : "",
            "LAST_NAME" 		    => (!empty($lastName)) ? $lastName : "",
            "SECOND_NAME" 		    => (!empty($secondName)) ? $secondName : "",
            "STATUS_ID"             => "4", //Новый
            "UF_TYPE"               => "3414",//Тип лида - клиент
            "UF_CHANNEL"            => "5274",//Канал - сайт
            "SOURCE_ID"             => "38",//Источник - Форма запроса заказа доп. услуг на сайте 1-ofd
            "UF_AUTO"               => 'Y',
            "ASSIGNED_BY_ID"        => 372,
            "UF_STATE_LEAD"         => $idElementList,
            "COMMENTS"              => 'Модель ККТ «' . $modelKKT . '»',
            "FM" => Array(
                "EMAIL" => Array(
                    "n0" => Array(
                        "VALUE"         => (!empty($email)) ? $email : "",
                        "VALUE_TYPE"    => "WORK",
                    ),
                ),
                "PHONE" => Array(
                    "n0" => Array(
                        "VALUE"         => (!empty($phone)) ? $phone : "",
                        "VALUE_TYPE"    => "WORK",
                    ),
                ),
            ),
        );

        if($LEAD_ID = $oLead->Add($arFields)){
            $this->status = "Создан лид: запрос на доп. услуги id (" . $LEAD_ID."). ";

            $idEventHistory = $CCrmEvent->Add(
                array(
                    'ENTITY_TYPE'  => 'LEAD',
                    'ENTITY_ID'    => $LEAD_ID,
                    'EVENT_ID'     => 'INFO',
                    'EVENT_TEXT_1' => "Получен запрос на доп. услуги. Модель ККТ: «" . $modelKKT . "»",
                )
            );

            if ($idEventHistory) {
                $this->status .= " Активность успешно создана. ";
            }  else {
                $this->error .= " Ошибка создания активности. ";
            }

        } else {
            $this->error ="Error: " . $oLead->LAST_ERROR;
        }

        return true;

    }

    //Функция обработки RentKKT
    function rentKKT($onemsg){
        CModule::IncludeModule('crm');

        $CCrmEvent  = new CCrmEvent();
        $oLead      = new CCrmLead;

        $FIO 		= $this->parseParamsString("FIO", $onemsg);
        $name 		= $this->getName($FIO);
        $lastName 	= $this->getLastName($FIO);
        $secondName = $this->getSecondName($FIO);

        $modelKKT   = $this->parseParamsString("ModelKKT", $onemsg);
        $email 		= $this->parseParamsString("Email", $onemsg);
        $phone 		= $this->parseParamsString("Telefon", $onemsg);
        $phone      = preg_replace("/[^0-9]/", '', $phone);

        $idElementList = getIdElementListState ('XML_STATE_LEAD_ACTIVITY');

        $arFields = Array(
            "TITLE"                 => "Запрос на аренду ККТ",
            "NAME" 				    => (!empty($name)) ? $name : "",
            "LAST_NAME" 		    => (!empty($lastName)) ? $lastName : "",
            "SECOND_NAME" 		    => (!empty($secondName)) ? $secondName : "",
            "STATUS_ID"             => "4", //Новый
            "UF_TYPE"               => "3414",//Тип лида - клиент
            "UF_CHANNEL"            => "5274",//Канал - сайт
            "SOURCE_ID"             => "41",//Источник - Форма аренды ККТ на сайте 1-ofd
            "UF_AUTO"               => 'Y',
            "ASSIGNED_BY_ID"        => 372,
            "UF_STATE_LEAD"         => $idElementList,
            "COMMENTS"              => 'Модель ККТ «' . $modelKKT . '»',
            "FM" => Array(
                "EMAIL" => Array(
                    "n0" => Array(
                        "VALUE"         => (!empty($email)) ? $email : "",
                        "VALUE_TYPE"    => "WORK",
                    ),
                ),
                "PHONE" => Array(
                    "n0" => Array(
                        "VALUE"         => (!empty($phone)) ? $phone : "",
                        "VALUE_TYPE"    => "WORK",
                    ),
                ),
            ),
        );

        if($LEAD_ID = $oLead->Add($arFields)){
            $this->status = "Создан лид: запрос на аренду ККТ id (" . $LEAD_ID."). ";

            $idEventHistory = $CCrmEvent->Add(
                array(
                    'ENTITY_TYPE'  => 'LEAD',
                    'ENTITY_ID'    => $LEAD_ID,
                    'EVENT_ID'     => 'INFO',
                    'EVENT_TEXT_1' => "Получен запрос на аренду ККТ. Модель ККТ: «" . $modelKKT . "»",
                )
            );

            if ($idEventHistory) {
                $this->status .= " Активность успешно создана. ";
            }  else {
                $this->error .= " Ошибка создания активности. ";
            }

        } else {
            $this->error ="Error: " . $oLead->LAST_ERROR;
        }

        return true;

    }

    //Функция обработки RequestClosingDoc
    function RequestClosingDoc($onemsg) {
        global $APPLICATION;
        /* Параметы в письме POSTFIX
        Event Type: "RequestClosingDoc"
        INN: 11111111111
        KPP: КПП организации
        NameCompany: Полное наименование организации
        TypeDoc: Акт сверки
        PeriodFrom: 22.07.2018
        PeriodTo: 22.08.2018
        LinkMonitoring: http://10.1.102.10/taxpayer/view/65456
        FIO: Петров Иван Иванович
        Telefon: 8999-343-21-21
        Email: 123@stamp.ru
        MethodSend:  //Пока отсутствует
        */
        $arExpectedParams = array(
            "INN",
            "KPP",
            "NameCompany",
            "FIO",
            "Telefon",
            "Email",
            "TypeDoc",
            "PeriodFrom",
            "PeriodTo",
            "MethodSend",
            "LinkMonitoring",
        );
        $title = "Запрос ЗД ИНН: ";
        $taskDesc = '';
        foreach($arExpectedParams as $expParam) {
            $expParamVal = $this->parseParamsString($expParam, $onemsg);
            if (empty($expParamVal)) {
                continue;
            }
            switch ($expParam) {
                case 'INN':
                    $title .= $expParamVal;
                    $taskDesc .= "ИНН: $expParamVal<br />";
                    break;
                case 'KPP':
                    $title .= " КПП: " . $expParamVal;
                    $taskDesc .= "КПП: $expParamVal<br />";
                    break;
                case 'NameCompany':
                    $title .= " $expParamVal";
                    $taskDesc .= "Наименование организации: $expParamVal<br />";
                    break;
                case 'FIO':
                    $taskDesc .= "Ф.И.О.: $expParamVal<br />";
                    break;
                case 'Telefon':
                    $taskDesc .= "Телефон: $expParamVal<br />";
                    break;
                case 'Email':
                    $taskDesc .= "E-mail: $expParamVal<br />";
                    break;
                case 'TypeDoc':
                    $taskDesc .= "Тип документов: $expParamVal<br />";
                    break;
                case 'PeriodFrom':
                    $taskDesc .= "Период: с $expParamVal ";
                    break;
                case 'PeriodTo':
                    $taskDesc .= "по  $expParamVal<br />";
                    break;
                case 'MethodSend':
                    $taskDesc .= "Способ отправки: $expParamVal<br />";
                    break;
                case 'LinkMonitoring':
                    $taskDesc .= "Ссылка на мониторинг на данную компанию: <a href='$expParamVal' target='_blank'>$expParamVal</a><br />";
                    break;
            }
        }

        if(CModule::IncludeModule("socialnetwork")){
            $arDepartments = array(self::AUDITORS_DEPARTMENT_ID_FROM_TASK);
            $departmentEmployees = CIntranetUtils::GetDepartmentEmployees($arDepartments, $bRecursive = true, $bSkipSelf = false, $onlyActive = 'Y');
            while($arEmployees = $departmentEmployees->GetNext()) {
                if ($arEmployees['ID'] != self::DEF_USER_ID_FROM_TASK_RESPONSIBLE){
                    $arAccomplices[] = $arEmployees['ID'];
                }
            }
        }

        $arTaskFields = array(
            "TITLE" => $title,
            "DESCRIPTION" => $taskDesc,
            "RESPONSIBLE_ID" => self::DEF_USER_ID_FROM_TASK_RESPONSIBLE,
            "ACCOMPLICES" => $arAccomplices,
            "UF_AUTO_222702896772" => 1,
            "UF_TASK_EDITABLE" => 1,
        );
        //$logger = Logger::getLogger('RequestClosingDoc','RequestClosingDoc.log');
        //$logger->log("arTaskFields[] = ".print_r($arTaskFields, true));
        CModule::includeModule("tasks");
        $obTask = new CTasks;
        if($ID = $obTask->Add($arTaskFields)) {
            $this->status = "Добавлена задача: $ID";
        } else {
            if($e = $APPLICATION->GetException())
                $this->error ="Error: " . $e->GetString();
        }
        return true;
    }

    //Функция поиска дубликатов лидов по TITLE и EMAIL
    function leadDublicateCheck($title, $email) {
        $ResMulti = CCrmFieldMulti::GetList(
            array('ID' => 'desc'), array('ENTITY_ID' => 'LEAD', 'TYPE_ID' => 'EMAIL', 'VALUE' => $email)
        );
        if(intval($ResMulti->SelectedRowsCount()) <= 0 ) {
            return false;
        }
        while ($arMulti = $ResMulti->Fetch()) {
            $res = CCrmLead::GetList(array(), array("ID" => $arMulti['ELEMENT_ID'], "TITLE" => $title));
            if(intval($res->SelectedRowsCount()) > 0 ) {
                $this->status = "Найден существующий лид, ID лида: " . $arMulti['ELEMENT_ID'];
                return $arMulti['ELEMENT_ID'];
            }
        }
        return false;
    }

    function getIblockIDByCode($type, $code) {
        if (CModule::IncludeModule('iblock')) {
            $resc = CIBlock::GetList(Array(), Array('TYPE' => $type, 'SITE_ID' => 's1', '=CODE' => $code), false);
            if ($arrc = $resc->Fetch())
                return $arrc['ID'];
            else
                return false;
        }
    }

    function addCRMActivity($subject, $description, $owner_id, $owner_type, $sonet_event_register) {
        CModule::IncludeModule('crm');
        $activityFields = array(
            'TYPE_ID' => \CCrmActivityType::Email,
            'SUBJECT' => $subject,
            'COMPLETED' => 'N',
            'PRIORITY' => \CCrmActivityPriority::Medium,
            'DESCRIPTION' => $description,
            'DESCRIPTION_TYPE' => \CCrmContentType::PlainText,
            'LOCATION' => '',
            'DIRECTION' => \CCrmActivityDirection::Outgoing,
            'NOTIFY_TYPE' => \CCrmActivityNotifyType::None,
            'SETTINGS' => array(),
            'STORAGE_TYPE_ID' => \CCrmActivity::GetDefaultStorageTypeID(),
            'START_TIME' => $this->getDateTime(),
            'END_TIME' => $this->getDateTime(),
            'OWNER_ID' => $owner_id,
            'OWNER_TYPE_ID' => $owner_type,
            'RESPONSIBLE_ID' => '',
            'BINDINGS' => '',
        );

        if (!($callId = \CCrmActivity::Add($activityFields, false, true, array('REGISTER_SONET_EVENT' => $sonet_event_register)))) {
            $this->throwError('Ошибка при создании Активити CRM: ' . CCrmActivity::GetLastErrorMessage(), false);
        }
    }

    //Парсинг параметров строки
    function parseParamsString($nameParametr, $strSearch){
        //Обработка исключительной ситуации для параметра ID
        if($nameParametr == "ID") {
            preg_match_all("/" . $nameParametr . ": (.*?)\n/i", $strSearch , $out);
            $last_key = key(array_slice($out, -1, 1, TRUE));

            if(array_key_exists(1,$out[$last_key])) {
                $last_key_two = key(array_slice($out[$last_key], -1, 1, TRUE));
                $name = str_replace(array("\r\n", "\r", "\n"), '',  strip_tags($out[$last_key][$last_key_two]));
                $name = trim($name);
                return $name;
            } else {
                //Исключительная ситуация для нестандартного формата письма FullAccaunt
                preg_match_all("/" . $nameParametr . ": (.*?)$/i", $strSearch , $out);
                $last_key = key(array_slice($out, -1, 1, TRUE));

                if(array_key_exists(0,$out[$last_key])) {
                    $last_key_two = key(array_slice($out[$last_key], -1, 1, TRUE));
                    $name = str_replace(array("\r\n", "\r", "\n"), '',  strip_tags($out[$last_key][$last_key_two]));
                    $name = trim($name);
                    return $name;
                }
                return false;
            }
        }

        if($nameParametr == "Accaunt") {
            preg_match("/" . $nameParametr . ": (.*?)\n/s", $strSearch , $out);
            if(array_key_exists(1,$out)) {
                $name = str_replace(array("\r\n", "\r", "\n"), '',  strip_tags($out[1]));
                $name = trim($name);
                return $name;
            } else {
                return false;
            }
        }

        preg_match("/" . $nameParametr . ": (.*?)\n/i", $strSearch , $out);


        if(array_key_exists(1,$out)) {
            $name = str_replace(array("\r\n", "\r", "\n"), '',  strip_tags($out[1]));
            $name = trim($name);
            return $name;
        } else {
            return false;
        }
    }

    function getName($FIO){
        $arr = explode(' ', $FIO);
        if(array_key_exists(1, $arr)) {
            return $arr[1];
        }
        return "noname";
    }

    function getLastName($FIO){
        $arr = explode(' ', $FIO);
        if(array_key_exists(0, $arr)) {
            $name = trim($arr[0]);
            $name = (empty($name))? "noname" : $name;
            return $name;
        }
        return "noname";
    }

    function getSecondName($FIO){
        $arr = explode(' ', $FIO);
        if(array_key_exists(2, $arr)) {
            return $arr[2];
        }
        return "noname";
    }

    //Получение текущей даты и времени
    function getDateTime() {
        return ConvertTimeStamp(false, "FULL", false, false);
    }

    //Обработка ошибок. Если $die задано true - выполнение прерывается
    function throwError($msg, $die) {
        $this->errors[] = $msg;
        if ($die)
            die(print_r($this->errors, true));
    }

    function GetInfo($inn)
    {
        $propertyTypeID = 'ITIN';
        $propertyValue = $inn;
        $countryID = 1;

        $result = \Bitrix\Crm\Integration\ClientResolver::resolve(
            $propertyTypeID,
            $propertyValue,
            $countryID
        );
        if (count($result) == 0)
        {
            $result = $this->GetInfoOGRNOnline($inn);
        }
        return $result;
    }

    function GetInfoOGRNOnline($inn)
    {
        $dataJSON = file_get_contents('https://xn--c1aubj.xn--80asehdb/%D0%B8%D0%BD%D1%82%D0%B5%D0%B3%D1%80%D0%B0%D1%86%D0%B8%D1%8F/%D0%BA%D0%BE%D0%BC%D0%BF%D0%B0%D0%BD%D0%B8%D0%B8/?%D0%B8%D0%BD%D0%BD='.$inn);
        $data = json_decode($dataJSON, true);

        if (strlen($inn) == 10 && count($data) > 0)
        {
            $arFields = array(
                'RQ_COMPANY_NAME' => 'shortName',
                'RQ_COMPANY_FULL_NAME' => 'name',
                'RQ_INN' => 'inn',
                'RQ_KPP' => 'kpp',
                'RQ_OGRN' => 'ogrn');
            $result = array();
            foreach ($data as $k => $v) {
                $rq = array();
                foreach ($arFields as $kf => $vf) {
                    $rq[$kf] = $v[$vf];
                }
                $result[] = array('fields' => $rq);

            }
            return $result;

        }
        else
        {
            $data = array();
        }

        return $data;
    }

    function getFmId($elementID, $typeID){
        $dbResFM = CCrmFieldMulti::GetList(
            array('ID' => 'asc'),
            array('ENTITY_ID' => 'LEAD', 'TYPE_ID' => $typeID, 'ELEMENT_ID' => $elementID)
        );

        if($arFM = $dbResFM->Fetch())
        {
            return $arFM['ID'];
        }

        return "n0";
    }

    function findDublicateDeal($arFilter){
        $resDublicateDeal = CCrmDeal::GetListEx(array('ID'=>'ASC'),$arFilter);
        if ($arDublicateDeal = $resDublicateDeal->Fetch()) {
            return $arDublicateDeal;
        }else{
            return false;
        }

    }

    function findOptimalDealKktByCompanyID($companyID){
        $arFilter = array(
            'CATEGORY_ID'   => '1', //Продажа клиент
            'COMPANY_ID'    => $companyID,
        );

        $resDublicateDeal = CCrmDeal::GetList(array('ID'=>'DESC'), $arFilter);
        if($resDublicateDeal->SelectedRowsCount() <= 0){
            return false;
        }

        $n = 0;
        $arResult = [];
        $arAllDeals = [];
        $arDogovorID = [];
        $arDogovorAndDeal = [];
        while ($arDeal = $resDublicateDeal->Fetch()) {
            $arAllDeals[$arDeal['ID']] = $arDeal;
            if($n == 0){
                $arResult = $arDeal;
            }

            foreach($arDeal['UF_DOGOVOR'] as $dogovorId){
                $arDogovorID[] = $dogovorId;
                $arDogovorAndDeal[$dogovorId] = $arDeal['ID'];
            }

            $n++;
        }

        //Если нет договоров, возвращаем самую новую сделку
        if(empty($arDogovorID)){
            return $arResult;
        }

        //Если договор один, возвращаем сделку
        if(count($arDogovorID) === 1){
            $idDeal = $arDogovorAndDeal[$arDogovorID[0]];
            return $arAllDeals[$idDeal];
        }

        //Если есть 2 и более договора, то вычисляем самый новый по дате.
        if(!empty($arDogovorID)){
            $IBLOCK_ID = getIblockIDByCode('registry_contracts');

            $arOrder = array(
                "PROPERTY_DATA_DOGOVORA" => "DESC",
                "ID" => "DESC",
            );

            $arFilter = array(
                "IBLOCK_ID" => $IBLOCK_ID,
                "ID" => $arDogovorID
            );

            $arSelect = array("ID", "PROPERTY_DATA_DOGOVORA");

            $res = CIBlockElement::GetList($arOrder, $arFilter, false, false,$arSelect);
            if($arrDogovor = $res->Fetch()){
                $idDeal = $arDogovorAndDeal[$arrDogovor['ID']];
                return $arAllDeals[$idDeal];
            }
        }

        return $arResult;
    }

    function findDublicateKKT($kkmRegId){
        if($kkmRegId){
            $IBLOCK_ID = getIblockIDByCode('registry_kkt');
            $arSelect = Array("ID", "IBLOCK_ID","PROPERTY_RNM_KKT", "PROPERTY_COST_TARIF", "PROPERTY_DEAL","PROPERTY_NUM_KKT","PROPERTY_COMPANY");
            $arFilter = array("IBLOCK_ID" => $IBLOCK_ID,"PROPERTY_RNM_KKT" =>$kkmRegId);
            $res = CIBlockElement::GetList(Array(), $arFilter, false, false,$arSelect);
            if($arKKT = $res->Fetch()){
                return $arKKT;
            }
            return false;
        }else{
            return false;
        }
    }

    function dateCut($date){
        if(!empty($date)) {
            $date = new DateTime($date);
            $date = $date->format('d.m.Y');
            return $date;
        }else{
            return false;
        }
    }
}