<?
file_put_contents('/tmp/postfix.log', date('d.m.Y H:i:s') . " ---  Пришло новое письмо!\n", FILE_APPEND);

//START. Подсчет времени на парсинг 1-го письма
$startTime = microtime();

// Соединяемся, выбираем базу данных
/*$link = mysql_connect('localhost', 'root', 'uSc5c5MspwUy')
    or die('Не удалось соединиться: ' . mysql_error());
mysql_select_db('sitemanager0') or die('Не удалось выбрать базу данных');*/

$mysqli = new mysqli('localhost', 'root', 'uSc5c5MspwUy', 'sitemanager0');
if ($mysqli->connect_errno) {
    file_put_contents('/tmp/postfix.log', "Ошибка подключлючения к БД. ".$mysqli->connect_error." \n", FILE_APPEND);
    exit();
}

@set_time_limit(0);
@ignore_user_abort(true);

//текст сообщения считываем из STDIN
$msg = file_get_contents("php://stdin");
$msgOriginal = $msg;

//отправитель письма
$sender = getenv('SENDER');

//получатель письма
$recipient = getenv('RECIPIENT');

//парсинг сообщения
list($header, $body) = explode("\n\n", $msg, 2);

//выделим строки с Subject: и From:
$subject = '';
$from = '';
$headerArr = explode("\n", $header);
foreach ($headerArr as $str) {
  if (strpos($str, 'Subject:') === 0) {
    $subject = $str;
  }
}

$msg     = getDecodeMsg($msg);
$msg     = strip_tags($msg);
$subject = getSubjectDecodeMsg($subject);
$from    = getFrom($msg);

$eventType = parseParamsString("Event Type", $msg);
$eventType = str_replace(array("\"", "'"), "", $eventType);
$eventType = trim($eventType);

$data = array(
  "UF_DATE" => date('Y-m-d H:i:s'),
  "UF_EVENT_TYPE" => $eventType,
  "UF_TEXT_ORIGINAL" => $msgOriginal,
  "UF_TEXTMAIL" => $msg,
  "UF_SUBJECT" => $subject,
  "UF_EMAIL" => $from,
  );

$res = mysql_insert('messages_postfix', $data );

if($res)
{
	file_put_contents('/tmp/postfix.log', date('d.m.Y H:i:s') . " ---  В справочник добавлена запись\n", FILE_APPEND);
}  
else {
	file_put_contents('/tmp/postfix.log', date('d.m.Y H:i:s') . " ---  Ошибка добавления записи!\n".$mysqli->error, FILE_APPEND);
}


// Освобождаем память от результата
mysqli_free_result($res);

// Закрываем соединение
mysqli_close($link);

//END Подсчет времени на парсинг 1-го письма
$time = (microtime() - $startTime);
file_put_contents('/tmp/postfix.log', date('d.m.Y H:i:s') . " ---  Итоговое время, секунд: " . $time . "\n", FILE_APPEND);

function mysql_insert($table, $inserts ) {
    global $mysqli;
    $values = array_map(array($mysqli, 'real_escape_string'), array_values($inserts));
    $keys = array_keys($inserts);

    return $mysqli->query( 'INSERT INTO `'.$table.'` (`'.implode('`,`', $keys).'`) VALUES (\''.implode('\',\'', $values).'\')');
}

//Парсинг параметров строки
function parseParamsString($nameParametr, $strSearch){
	preg_match("/" . $nameParametr . ": (.*?)\n/i", $strSearch , $out);

	if(array_key_exists(1,$out)) {
		$name = str_replace(array("\r\n", "\r", "\n"), '',  strip_tags($out[1]));
		return $name;
	} else {
		return "";
	}
}

//Decoder
function getDecodeMsg($msg){
	if (substr_count($msg, 'Content-Transfer-Encoding: quoted-printable')) {
		$cutMsg = explode("Content-Transfer-Encoding: quoted-printable", $msg);
		if(array_key_exists(1, $cutMsg)) {
			$msg = $cutMsg[0] . "Content-Transfer-Encoding: quoted-printable" . quoted_printable_decode($cutMsg[1]);
		} else {
			$msg = quoted_printable_decode($msg);
		}
		return $msg;
	}
	elseif (substr_count($msg, 'Content-Transfer-Encoding: base64')) 
	{
		$headers = "";
		$msgPreg = str_replace(array("\r\n", "\n\r", "\n", "\r"), "", $msg);			
		preg_match("/base64(.*?)\=------\=/i", $msgPreg , $out);
		if(array_key_exists(1,$out)) {
			$msgBase64 = str_replace(array("\r\n", "\r", "\n"), '',  strip_tags($out[1]));	
			$msgBase64 = base64_decode($msgBase64);
			//Склеиваем Header
			$msg = explode("base64", $msg);
			$msg = $msg[0] . "base64 " . $msgBase64;			
			return $msg;
		} else {
			return false;
		}	
	} 
	else
	{
		return $msg;
	}
}

function getSubjectDecodeMsg($subject){
    preg_match_all("/=\?utf-8\?B\?(.*?)\?=/i", $subject, $out);
    if(array_key_exists(1,$out) && !empty($out[1])) {
        $subject = implode(' ', $out[1]);
        return base64_decode($subject);
    } else {
        return $subject;
    }
}

function getFrom($msg){
    preg_match("/ (.*?) /i", $msg, $out);
    if(array_key_exists(1,$out)) {
        $from = $out[1];
        return $from;
    }else{
        return false;
    }
}
