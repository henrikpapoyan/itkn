<?
$_SERVER["DOCUMENT_ROOT"] = str_replace("/local/php_interface/postfix", "", $_SERVER['PWD']);

/** Command Line Arguments */
// Тут показаны две формы записи аргументов: короткая и полная
// При этом, если после параметра стоит два двоеточия, значит параметр необязательный,
// а если одно двоеточие - значит обязательный.
// все параметры которые не указаны в конфигурации будут проигнорированы
$params = array(
    ''      => 'help',
    'i:'    => 'id:',
    'e:'    => 'eventType:',
    'u:'    => 'update:',
);

// Default values
$idHlb      = false;
$eventType  = false;
$isUpdate   = true;
$errors     = array();

$arEventType = array(
    "1" => "Event Type NewLead",
    "2" => "Event Type NewLeadSer",
    "3" => "Event Type NewLeadFN",
    "4" => "Temporary_account",
    "5" => "Full_account",
);

$options = getopt( implode('', array_keys($params)), $params );

if (isset($options['id']) || isset($options['i']))
{
    $idHlb = isset( $options['id'] ) ? $options['id'] : $options['i'];
}

if (isset($options['eventType']) || isset($options['e']))
{
    $eventType = isset( $options['eventType'] ) ? $options['eventType'] : $options['e'];
    $eventType = $arEventType[$eventType];
}

if (isset($options['update']) || isset($options['u']))
{
    $isUpdate = isset( $options['update'] ) ? $options['update'] : $options['u'];
}

if ( isset($options['help']) || count($errors) )
{
    $help = "
usage: php testRequestEvent.php [--help] [-i|--id=999] [-e|--event=1] [-u|--update=false]

Options:
            --help      Show this message
        -i  --id        ID element HighloadBlock (default: false)
        -e  --event     Event type (default: false)
        -u  --update    Update (default: true)
Example:
        php testRequestEvent.php --id=root --event=secret --update=false
";
    if ( $errors )
    {
        $help .= 'Errors:' . PHP_EOL . implode("\n", $errors) . PHP_EOL;
    }
    die($help);
}

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('CHK_EVENT', true);

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
require($_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/Classes/postfix/AgentMessagePostfix.class.php");

\Bitrix\Main\Loader::includeModule('highloadblock');

@set_time_limit(0);
@ignore_user_abort(true);
//авторизация файла по ID пользователя
global $USER;
$USER->Authorize(372);

$logger = Logger::getLogger("AgentMessagePostfix", "AgentMessagePostfix/result.log");
$logger->log('Run script AgentMessagePostfix');

$AM = new AgentMessagePostfix();

//Получаем письмо из HigloadBlock
use Bitrix\Highloadblock as HL;

$HL_Infoblock_ID = 3; //ID предварительно созданного HL ИБ

$hlblock = HL\HighloadBlockTable::getById($HL_Infoblock_ID)->fetch();
$entity = HL\HighloadBlockTable::compileEntity($hlblock);
$entity_data_class = $entity->getDataClass();

$arFilter = array();

$arFilter['UF_PROCESSED'] = false; //Берем неотработанные письма
if($idHlb){
    $arFilter['ID'] = $idHlb;
}
if($eventType){
    $arFilter['UF_EVENT_TYPE'] = $eventType;
}

$rsData = $entity_data_class::getList(array(
    'select' => array('*'),
    'filter' => $arFilter
));

while($el = $rsData->fetch()){
    print_r("ID Element: " . $el['ID'] . "\n");
    print_r("Event Type: " . $el['UF_EVENT_TYPE'] . "\n");
    $msg = $el['UF_TEXTMAIL'];

    if($AM->parseOne($msg) === false){
        continue;
    }

    echo $AM->status;
    echo $AM->error;
    echo "\n\n";

    //Update off
    if($isUpdate === 'false'){
        continue;
    }

    //Работа завершена с ошибками
    if($AM->error != ""){
        $data = array(
            "UF_PROCESSED" => "1",
            "UF_ERROR" => "1",
            "UF_ERROR_TEXT" => $AM->error,
        );
    }

    //Работа завершена успешно
    if($AM->error == "" && $AM->status != ""){
        $data = array(
            "UF_PROCESSED" => "1",
            "UF_RESULT" => $AM->status
        );
    }
    $result = $entity_data_class::update($el['ID'], $data);

    //Очистить статус
    $AM->status = "";
    $AM->error 	= "";
}

$USER->Logout(); //делогируем пользователя
$logger->log('End script AgentMessagePostfix');
