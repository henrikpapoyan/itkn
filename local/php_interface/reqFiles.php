<?
    $arRequiredFilesPaths = [
        "/local/php_interface/ofd.bitrix24/constans/bpm_bitrix24.php",
        "/local/php_interface/ofd.bitrix24/constans/bpm_access.php",
        "/local/php_interface/ofd.bitrix24/functions/function.php",
        "/local/php_interface/ofd.bitrix24/events/handler.php",
        "/local/php_interface/ofd.bitrix24/debug/class.krumo.php",
        "/local/php_interface/Classes/crm/common/controlSalesCompanyByLead.php",
    ];

    foreach ($arRequiredFilesPaths as $requiredFilePath) {
        if (file_exists($_SERVER["DOCUMENT_ROOT"] . $requiredFilePath)) {
            require_once($_SERVER["DOCUMENT_ROOT"] . $requiredFilePath);
        }
    }