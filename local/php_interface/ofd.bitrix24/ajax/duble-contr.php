<?
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule('crm');
//unset($_SESSION["CONTRAGENTS_MUSR"]);
$url = "https://" . $_SERVER["SERVER_NAME"] . "/local/php_interface/ofd.bitrix24/ajax/duble-merg.php";
//предварительно пустые контрагенты
$empty_contr_proverka = \Bitrix\Main\Application::getDocumentRoot() . OFD_BITRIX_TMP."dubles/empty_contr_proverka.txt";
//предварительно нормальные контрагенты
$company_inn_kpp = \Bitrix\Main\Application::getDocumentRoot() . OFD_BITRIX_TMP."dubles/company_inn_kpp.txt";
// ИП
$ipe_inn = \Bitrix\Main\Application::getDocumentRoot() . OFD_BITRIX_TMP."dubles/ipe_inn.txt";
//контрагенты более 1 реквизита + IDPROD
$oter = \Bitrix\Main\Application::getDocumentRoot() . OFD_BITRIX_TMP."dubles/oter.txt";
//компании с ИНН и без КПП
$company_empty_kpp = \Bitrix\Main\Application::getDocumentRoot() . OFD_BITRIX_TMP."dubles/company_empty_kpp.txt";
//мусорные контрагенты
$musor_contragent = \Bitrix\Main\Application::getDocumentRoot() . OFD_BITRIX_TMP."dubles/musor_contragent.txt";
?>
<?
if (isset($_POST["STOPINTERVAL"])){
    unset($_POST["MUSR"]);
    unset($_SESSION["SBOR_STOP"]);
    unset($_SESSION["STOP_DELETE"]);
    unset($_POST["DELETE"]);
    LocalRedirect($url, true);
}

if (isset($_POST["REFR_MASS"])) {
    unset($_SESSION["SBOR_STOP"]);
    unset($_SESSION["STOP_DELETE"]);
    session_start();
    $_SESSION["SBOR_STOP"] = "0";
    $_SESSION["STOP_DELETE"] = "0";
    $resBitrix = contragents();
    $col = count($resBitrix[0])+count($resBitrix[1]);
    echo "Всего контрагентов: ".$col."<br>";
    echo "0.) Группа 0 - предварительно пустые: ".count($resBitrix[0])."<br>";
    echo "1.) Предварительно нормальные: ".count($resBitrix[1])."<br>";
    echo "2.) Группа 1 - компаний: ".count($resBitrix[2])."<br>";
    echo "3.) Группа 2 - ИП: ".count($resBitrix[3])."<br>";
    echo "4.) Группа 3 - контрагенты с более чем 1 реквизитом + IDPROD: ".count($resBitrix[4])."<br>";
    echo "5.) Группа 4 - компаний с ИНН и без КПП: ".count($resBitrix[8])."<br>";
    echo "6.) Дубли компаний: ".count($resBitrix[5])."<br>";
    echo "7.) Дубли ИП: ".count($resBitrix[6])."<br>";
    echo "8.) Дубли в 3 группе: ".count($resBitrix[7])."<br>";
    echo "9.) Дубли в 4 группе: ".count($resBitrix[9])."<br>";
    if(file_exists($empty_contr_proverka) and empty($_SESSION["CONTRAGENTS_MUSR"])){
        $arr_empty_contr_proverka = readArrayInFileA('empty_contr_proverka',  "dubles");
        echo "Проверка: ".count($arr_empty_contr_proverka)."<br>";
        session_start();
        $_SESSION["CONTRAGENTS_MUSR"] = $arr_empty_contr_proverka;

    }//LocalRedirect($url, true);
}

if(isset($_POST["MUSR"]) and isset($_SESSION["SBOR_STOP"])){

    if(!empty($_SESSION["CONTRAGENTS_MUSR"])){
        echo $_SESSION["SBOR_STOP"].".) ";
        $_SESSION["SBOR_STOP"]++;
        $elem = array_kshift($_SESSION["CONTRAGENTS_MUSR"]);
        $el = current($elem);
        $id = key($elem);
        $rez = dopInfoContragent(1,$id);
        if($rez){
            echo "Контрагент: ".$el." имеет связи";
        }else{
            $rez = GetUserField('CRM_COMPANY', $id, 'UF_CRM_COMPANY_GUID');
            writeArrayInFile($rez, 'musor_contragent',"dubles");
            echo "<span style='color:red'>Мусорный контрагент: </span>".$el;
        }
        //pre($elem);
        if(empty($id)){
            exit('Конец обработки!');?>
            <script>
                clearTimeout(timerId);
            </script>
        <?}
    }else{
        echo "Файла нет";
    }

}
// удаление контрагента
if(isset($_POST["DELETE"]) and isset($_SESSION["STOP_DELETE"])){
    if(file_exists($musor_contragent) and empty($_SESSION["CONTRAGENTS_DELETE"])){
        $arr_delete =  file($musor_contragent);
        session_start();
        $_SESSION["CONTRAGENTS_DELETE"] = $arr_delete;
    }else{
        echo $_SESSION["STOP_DELETE"].".) ";
        $elemdel = array_kshift($_SESSION["CONTRAGENTS_DELETE"]);
        $eldel = current($elemdel);
        $guid = trim($eldel);
        if($_SESSION["STOP_DELETE"] == "0"){
            $rez = CrmBitrixBpmUpdate::deleteContragent($guid,1);
        }else{
            $rez = CrmBitrixBpmUpdate::deleteContragent($guid);
        }


        $_SESSION["STOP_DELETE"]++;
        pre($rez);
        if(empty($eldel)){
            exit('Конец обработки!');
        }
    }

}