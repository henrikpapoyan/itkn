<?
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule('crm');
$url = "http://" . $_SERVER["SERVER_NAME"] . "/local/php_interface/ofd.bitrix24/ajax/dublemergecontact.php";
?>


<?
if (isset($_POST["REFR_MASS"])) {
    //общее кол-во контактов
    unset($_SESSION["RQ_CONTACT_D"]);

    //общее кол-во контактов
    unset($_SESSION["CONTACT_COUNT"]);


    //контактов два дубля
    unset($_SESSION["CONTACT_ID"]);


    //контрагенты больше двух дублей
    unset($_SESSION["CONTACT_ID_ARRAY"]);
    unset($_SESSION["DEL_MASS"]);
    unset($_SESSION["START_ID"]);
    unset($_SESSION["FIS_CONTR"]);

    $logfile = $_SERVER["DOCUMENT_ROOT"] . "/log/ofd.bitrix24/dubleContact/" . $_SESSION["NAME_LOG"];
    //unlink($logfile);
    session_start();
    //уникальное название лога
    $_SESSION["NAME_LOG"] = "mergcontact_" . $_SERVER["REQUEST_TIME"] . ".log";
    //кол-во запросов
    $_SESSION["COUNT_CHANGED"]="0";
    $_SESSION["COUNT"] = "0";
    //кол-во запросов
    $_SESSION["COUNT_ARRAY"] = "0";
    $_SESSION["FIND_DUBLE_CONTINUE"]="0";
    $_SESSION["CURRENT_ELEMENT_CONTACT"]=0;
    $_SESSION["NOTINPROCESS"]="0";
    //pre($_POST["REFR_MASS"]);
    LocalRedirect($url, true);
}

if ($_POST["STOP_MASS"]) {
    unset($_SESSION["DEL_MASS"]);
    LocalRedirect($url, true);
}

if (empty($_SESSION["RQ_CONTACT_D"])) {
    $select = array(
        "ID",
        "FULL_NAME"/*
        "UF_BPMCONTACTID"*/
    );
    $res = CCrmContact::GetList(array(), array('CHECK_PERMISSIONS' => 'N'),$select);
    $countcontact=0;$arraycontact=array();
    /*while ($fields = $res->Fetch()) {
         $countcontact++;
         $arraycontact[]=$fields;
     }*/
    while ($fields = $res->Fetch()) {
        $countcontact++;
        $arraycontact[]=$fields;
        $ar[$fields["ID"]] = array($fields["FULL_NAME"]);
    }


    session_start();
    //общее кол-во контрагентов
    $_SESSION["RQ_CONTACT_D"] = $ar;
    $_SESSION["CONTACT_COUNT"]=$countcontact;
    $_SESSION["CURRENT_ELEMENT_CONTACT"] = 0;
    $_SESSION["FIND_DUBLE_CONTINUE"] = "0";
    $_SESSION["NOTINPROCESS"]="0";
    //print_r($ar);
    $_SESSION["DEL_MASS"] = 1;
    LocalRedirect($url, true);

} else {
    /* session_start();
     if (($_SESSION["FIND_DUBLE_CONTINUE"]=="0")) {?>
         <script>
             $("#debug").css("display","none");
             $("#product_add_result").css("display","block");
             $("#del").css("display","block");

         </script>

         <?

         $logger = Logger::getLogger('logmain', 'ofd.bitrix24/logmain.txt');

         $_SESSION["NOTINPROCESS"]="1";
         $count = 2000;
         $begin = $_SESSION["CURRENT_ELEMENT_CONTACT"];
         $endelement = $_SESSION["CURRENT_ELEMENT_CONTACT"] + $count;

         $logger->log($begin);
         $logger->log($endelement);
         if ($endelement > $_SESSION["CONTACT_COUNT"]) {
             $endelement = $_SESSION["CONTACT_COUNT"];
             $_SESSION["FIND_DUBLE_CONTINUE"] = "1";
         }


         $newidmass = array();
         $newidmass = $_SESSION["CONTACT_ID"];
         $logger->log("newidmass");
         //$logger->log(array($newidmass));
         for ($i = $begin; $i < $endelement; $i++) {

             $arrayelement = ContactBitrixBpmMerge::FindDubleContact($_SESSION["RQ_CONTACT_D"][$i]['ID']);
             if (!empty($arrayelement)) {
                 //сохранять id породитель
                 $newidmass[] = array('FROM' => $_SESSION["RQ_CONTACT_D"][$i]['ID'], 'TO' => $arrayelement);
             }
         }
         $logger->log("newidmass_new");
         //$logger->log(array($newidmass));
         unset($_SESSION["CONTACT_ID"]);
         $_SESSION["CONTACT_ID"] = $newidmass;
         $_SESSION["CURRENT_ELEMENT_CONTACT"] = $endelement;
         //$_SESSION["FIND_DUBLE_CONTINUE"] = "1";
         $_SESSION["NOTINPROCESS"]="0";
         echo "шаг завершен";
         // $_SESSION["CONTACT_ID_ARRAY"] = $newidmassarray;
         LocalRedirect($url, true);
     }*/

    $res_arr = array_count_values(array_map("serialize", $_SESSION["RQ_CONTACT_D"]));
    $count_values_arr_res = array_filter($res_arr, function ($elem) {
        return ($elem > 1);
    });


    $del_arr = array_count_values(array_map("serialize", $_SESSION["RQ_CONTACT_D"]));
    $del_res = array_filter($del_arr, function ($elem) {
        return ($elem > 1);
    });

    $result_array = array();
    $result_lid = array();
    $unser_arr1 = array();

    $count2 = 1;
    if (empty($_SESSION["CONTACT_ID"]) or empty($_SESSION["CONTACT_ID_ARRAY"])) {

        foreach ($del_res as $key => $value) {
            $unser = unserialize($key);
            $unser_arr["fio"] = $unser[0];

            $logger = Logger::getLogger($_SESSION["NAME_LOG"], 'ofd.bitrix24/dubleContact/' . $_SESSION["NAME_LOG"].".txt");
            //$logger->log(array($unser_arr["fio"]));


            $id = ContactBitrixBpmMerge::FindIDUsingFIO($unser_arr["fio"]);
            //$logger->log(array($id));

            $unser_arr['id_name'] = $id;
            array_push($result_array, $unser_arr);
            foreach ($id as $key => $val) { $col[] = $val; }

        }

        $logger = Logger::getLogger("olegtest.txt", 'ofd.bitrix24/dubleContact/olegtest.txt');
        //$logger->log(array($result_array));
        foreach ($result_array as $kv => $vk) {
            // два дубля
            if (count($vk['id_name']) == 2) {
                    $massIdconntr[] = $vk['id_name'];
            }
            //больше двух дубликатов
            if (count($vk['id_name']) >= 3) {
                 $massIdconntrArray[] = $vk['id_name'];
            }
        }
       // $logger->log(array($massIdconntr));
       // $logger->log(array($massIdconntrArray));


        session_start();
        //два дубля
        $_SESSION["CONTACT_ID"] = $massIdconntr;
        //больше двух дублей
        $_SESSION["CONTACT_ID_ARRAY"] = $massIdconntrArray;

    }

}

if (isset($_POST["DEL_MASS"]) and !empty($_SESSION["CONTACT_ID"])) { ?>
    <script>
        $("#del").css("display", "none");
        $("#new").css("display", "block");
        $("#product_add_result_two").css("display", "block");

    </script>
    <?

    //$idcon = "";
   $idcon = $_SESSION["CONTACT_ID"][$_SESSION["COUNT"]];

    if (!empty($idcon)) {

        pre($idcon);
        $logger = Logger::getLogger($_SESSION["NAME_LOG"], 'ofd.bitrix24/dubleContact/elementtoanalize.txt');
        $logger->log(array($idcon));

        if ((ContactBitrixBpmMerge::CheckGuideQuality(trim($idcon[0]), trim($idcon[1])))
   || (ContactBitrixBpmMerge::CheckFioEmailQuality(trim($idcon[0]), trim($idcon[1])))
   || (ContactBitrixBpmMerge::CheckFioTelQuality(trim($idcon[0]), trim($idcon[1])))
   || (ContactBitrixBpmMerge::CheckFioEmailTelQuality(trim($idcon[0]), trim($idcon[1])))) {


            $logger = Logger::getLogger($_SESSION["NAME_LOG"], 'ofd.bitrix24/dubleContact/mainfile.txt');
            $logger->log("Первый элемент=".$idcon[0]."///Второй элемент=".$idcon[1]);

            if (trim($idcon[0]) > trim($idcon[1])) {
                ContactBitrixBpmMerge::MergeBitrixContacts(trim($idcon[1]), trim($idcon[0]));
            } else {
                ContactBitrixBpmMerge::MergeBitrixContacts(trim($idcon[0]), trim($idcon[1]));
            }
            $_SESSION["COUNT_CHANGED"]++;
            $n = $_SESSION["COUNT_CHANGED"]++;;

            echo "Исправлено контактов: " . $n;

            $logger = Logger::getLogger($_SESSION["NAME_LOG"], 'ofd.bitrix24/dubleContact/' . $_SESSION["NAME_LOG"]);
            $logger->log($idcon);
        }

        $_SESSION["COUNT"]++;
    } elseif (empty($idcon) and !empty($_SESSION["CONTACT_ID_ARRAY"])) {

        if (!empty($_SESSION["CONTACT_ID_ARRAY"])) {
            foreach ($_SESSION["CONTACT_ID_ARRAY"] as $rt => $rts) {


                pre($rts);

             /*  for ($i = 0; $i <= count($rts); $i++) {
                    for ($j = $i+1; $j <= count($rts); $j++) {
                        pre($rts[$i],$rts[$j]);
                        if ((ContactBitrixBpmMerge::CheckGuideQuality(trim($rts[$i]), trim($rts[$j])))
                            || (ContactBitrixBpmMerge::CheckFioEmailQuality(trim($rts[$i]), trim($rts[$j])))
                            || (ContactBitrixBpmMerge::CheckFioTelQuality(trim($rts[$i]), trim($rts[$j])))
                            || (ContactBitrixBpmMerge::CheckFioEmailTelQuality(trim($rts[$i]), trim($rts[$j])))) {


                            $logger = Logger::getLogger($_SESSION["NAME_LOG"], 'ofd.bitrix24/dubleContact/mainfile.txt');
                            $logger->log("Первый элемент=".$rts[$i]."///Второй элемент=".$rts[$j]);
                            ContactBitrixBpmMerge::MergeBitrixContacts(trim($rts[$i]), trim($rts[$j]));

                        }

                    }
                }continue;*/

                /*
                $logger = Logger::getLogger("11.txt", 'ofd.bitrix24/dubleContact/11.txt');
                $logger->log(array($rt));
                $logger->log(array($rts));


                continue;



                unset($result);
                if (empty($_SESSION["CONTACT_ID_ARRAY"][$rt])) {
                    unset($_SESSION["CONTACT_ID_ARRAY"][$rt]);
                    break;
                }
                $logger = Logger::getLogger($_SESSION["NAME_LOG"], 'ofd.bitrix24/dubleContact/11.txt');
                 $logger->log(array($rts));
                $u1 = array_shift($rts);
                $u2 = array_shift($rts);
                if (strlen($u1)>1) {
                    $mainelement = $u1;
                }




                if ((ContactBitrixBpmMerge::CheckGuideQuality(trim($mainelement), trim($u2))) || (ContactBitrixBpmMerge::CheckFioEmailQuality(trim($mainelement), trim($u2))) || (ContactBitrixBpmMerge::CheckFioTelQuality(trim($mainelement), trim($u2))) || (ContactBitrixBpmMerge::CheckFioEmailTelQuality(trim($mainelement), trim($u2)))) {
                    ContactBitrixBpmMerge::MergeBitrixContacts(trim($mainelement), trim($u2));
                   // $logger->log("Мерж!");
                    //$logger->log(array($mainelement, $u2));
                }

                if (count($rts) >= 1) {
                    array_unshift($rts, $result);
                }

                $_SESSION["CONTACT_ID_ARRAY"][$rt] = $rts;
                if (empty($_SESSION["CONTACT_ID_ARRAY"][$rt])) {
                    unset($_SESSION["CONTACT_ID_ARRAY"][$rt]);
                    break;
                }

                unset($result);
                break;*/
            }

            $n = $_SESSION["COUNT_ARRAY"] + 1;
            echo "Исправлено контактов: " . $n;
            $_SESSION["COUNT_ARRAY"]++;

            //pre($_SESSION["COMPANY_ID_ARRAY"]);
        } else {
            unset($_SESSION["CONTACT_ID_ARRAY"]);
            unset($_SESSION["CONTACT_ID"]);
        }
       // echo "Test1";

    } elseif (empty($_SESSION["CONTACT_ID_ARRAY"]) and empty($_SESSION["CONTACT_ID"])) {
        ?>
        <script>
            clearTimeout(timerId);
            $("#debug").css("display", "block");
            $("#new").css("display", "none");
        </script>
        <?
        echo "<h2 id='exit' style='color: green'>Контрагенты  с дублями по ИНН и КПП исправлены!</h2>";
        unset($_SESSION["COUNT"]);
        unset($_SESSION["CONTACT_ID_ARRAY"]);
        unset($_SESSION["CONTACT_ID"]);
        unset($_SESSION["START_ID"]);
    }
}
elseif (empty($_SESSION["CONTACT_ID_ARRAY"]) and empty($_SESSION["CONTACT_ID"]) and !empty($_POST["REFR_MASS"])) {
    ?>
    <script>
        clearTimeout(timerId);
        $("#debug").css("display", "block");
        $("#new").css("display", "none");
    </script>
    <?
    echo "<h2 id='exit' style='color: green'>Контрагенты  с дублями по ИНН и КПП исправлены!</h2>";
    unset($_SESSION["COUNT"]);
    unset($_SESSION["CONTACT_ID_ARRAY"]);
    unset($_SESSION["CONTACT_ID"]);
    unset($_SESSION["START_ID"]);
}
if (empty($_POST["DEL_MASS"]) and empty($_SESSION["FIS_CONTR"]) or !empty($_POST["REFR_MASS"]) and empty($_SESSION["FIS_CONTR"])) {

    foreach ($result_array as $d => $df) {
        foreach ($df['id_name'] as $e => $ef) {
            $sult_array[$e] = $ef;
        }
    }
    if (is_array($unser_arr1['id_name']) and is_array($sult_array) or is_array($unser_arr1['id_name'])) {
        if (is_array($sult_array)) {
            $DubleScv = ($unser_arr1['id_name'] + $sult_array);
        } else {
            $DubleScv = $unser_arr1['id_name'];
        }

        $kol1 = count($unser_arr1['id_name']);
        $kol2 = count($col);
        $kol3 = count($DubleScv);
        echo "<h4 style='color: green'>Всего контактов с системе: " . $_SESSION["CONTACT_COUNT"] . "</h4>";
        if(!empty($kol1)){
           // echo "<q>С контрагентами без ИНН и КПП ничего не делаем!</q>";
           // echo "<pre style='color: green'>Кол-во контрагентов без ИНН и КПП: <b>" . $kol1 . "</b></pre>";
            krumo($unser_arr1['id_name']);
        }
        if(!empty($kol2)){
            echo "<pre style='color: green'>Кол-во контактов с совпадающими ФИО: <b>" . $kol2 . "</b></pre>";
            if (is_array($sult_array)) {
                echo "<h4>Подробная информация по дублированным контрагентам по ИНН/КПП или по ИНН.</h4>";
                krumo($sult_array);
            }
        }else{?>
            <?="<h1 style='color: #0a7ddd'>Kонтрагентов c дублями по ИНН/КПП или по ИНН нет!</h1>";?>

            <script>
                //clearTimeout(timerId);
                $("#debug").css("display", "block");
                $("#new").css("display", "none");
                $("#del").css("display", "none");
            </script>
        <?}

        if(!empty($DubleScv)){
            $time = time();
            $file = $_SERVER["DOCUMENT_ROOT"] . "/log/ofd.bitrix24/dubleContact/dublContragents_" . $time . ".csv";
            $file_url = "http://" . $_SERVER["SERVER_NAME"] . "/log/ofd.bitrix24/dubleContact/dublContragents_" . $time . ".csv";
            $date = array("ID", "DATE_CREATE", "TITLE", "ASSIGNED_BY_NAME", "ASSIGNED_BY_LAST_NAME", "ASSIGNED_BY_SECOND_NAME");
            $arrHeaderCSV = array("ID", "Дата создания", "Название", "Инн", "Кпп", "Ответственный");
            $count = 1;
            foreach ($DubleScv as $du => $dug) {
                if ($count == 1) {
                    $csv = CsvGenerite($du, $file, $date, $arrHeaderCSV);
                } else {
                    $csv = CsvGenerite($du, $file, $date);
                }
                $count++;
            } ?>
            <div id="myDiv">
                <div id="csv" class="button-duble" style="width: 35%;float: right;">
                    <button class="button dsgnmoo" onclick="location.href = '<?= $file_url ?>'">2.	&darr; CSV контактов
                    </button>
                </div>
            </div>
            <?
        }

    } else {
        $kol1 = count($unser_arr1['id_name']);
        $kol2 = count($col);
        echo "<h4 style='color: green'>Всего контактов с системе: " . $_SESSION["CONTACT_COUNT"] . "</h4>";
        if ($kol1 + $kol2 > 0) {
            //echo "<pre style='color: green'>Кол-во контрагентов без ИНН и КПП: <b>" . $kol1 . "</b></pre>";
           // krumo($unser_arr1['id_name']);

           // echo "<pre style='color: green'>Кол-во контрагентов c дублями по ИНН/КПП или по ИНН: <b>" . $kol2 . "</b></pre>";
        } else {
            //echo "<h1 style='color: #0a7ddd'>Kонтрагентов c дублями по ИНН/КПП или по ИНН нет!</h1>";
        }

    } ?>

    <script>
        $("div#myDiv2 div#csv").remove();
        if ($("div#myDiv2").is("#csv")) {
            var lent = false;
        }
        if (lent != false) {
            $('#myDiv2').append($('#myDiv>div'));
        }

        $("#product_add_result").css("display", "block");
        clearTimeout(timerId);
    </script>
    <?
}