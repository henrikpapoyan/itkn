<?
use Bitrix\Main\Loader,
    Bitrix\Crm\Integration\ClientResolver,
    Bitrix\Highloadblock as HL;
Loader::includeModule("iblock");
Loader::includeModule("crm");
Loader::includeModule("highloadblock");

require_once($_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/Classes/postfix/AgentMessagePostfix.class.php");

function readOneStrok($name){
    $path = \Bitrix\Main\Application::getDocumentRoot() . OFD_BITRIX_TMP;
    $handlePos = fopen($path . $name.".txt", 'r+');//открываем файл с последним положением
    if($handlePos){
        if (($buffer = fgets($handlePos, 10)) !== false) {
            $startPos=(int)$buffer;
            if($startPos<0) $startPos=0;

            $handle = fopen($path . $name.".txt", 'rb');
            if($handle){
                $buffer='';
                $newPos=$startPos;
                fseek($handle, $startPos, SEEK_SET);//указатель в положение
                if (($buffer = fgets($handle, 5000)) !== false) {//попытка чтения строки
                    $buffer;//прочитали строку
                    $newPos=ftell($handle);//оказались в позиции
                }
                fclose($handle);
                //rewind($handle);
                if($newPos!=$startPos){
                    rewind($handlePos);
                    fwrite($handlePos, sprintf('%09d', $newPos));//записываем позицию, на которой остановились
                }
            }
            fclose($handlePos);
            return $buffer;
        }
        return "Не нашли файл!";
    }
}


function GetGUIDCompany ($idcompany)
{
    global $DB;
    $strSql = "SELECT
				b_uts_crm_company.UF_CRM_COMPANY_GUID	
			  FROM b_uts_crm_company 
			  where b_uts_crm_company.VALUE_ID = '$idcompany'";
    $resCompany = $DB->Query($strSql, false, false);
    if($arCompany = $resCompany->Fetch()) {
        return $arCompany['UF_CRM_COMPANY_GUID'];
    }

}

function GuidID ($guid, $keyInn = null){
    $guid = strtoupper($guid);

    $connection = Bitrix\Main\Application::getConnection();
    $sql = 'SELECT VALUE_ID,UF_CRM_COMPANY_GUID FROM b_uts_crm_company WHERE UF_CRM_COMPANY_GUID = \''.$guid.'\'';
    $recordset = $connection->query($sql);
    while ($record = $recordset->fetch())
    {
        $id[] =  $record['VALUE_ID'];
    }
    if(count($id) == 1){
        $id = $id[0];
        if($keyInn){
            $verify = RestBpmBitrix::verify(null,$id);
            return $verify;
        }

        return $id;
    }else{

        return false;
    }
}
/**
 * Запись массива в файл
 */
function writeArrayInFileA($array, $name, $directories = null){
    if(empty($directories)){
        $path = \Bitrix\Main\Application::getDocumentRoot() . OFD_BITRIX_TMP;
    }else{
        $path = \Bitrix\Main\Application::getDocumentRoot() . OFD_BITRIX_TMP.$directories."/";
    }
    $serArray = serialize($array); // преобразовываем массив в строку
    $file = fopen ($path . $name . ".txt","w+"); // открываем файл, если надо то создаем
    fputs($file, $serArray); // записываем в него строку
    fclose($file); // закрываем файл
}

/**
 * Чтение массива из файл
 */
function readArrayInFileA($fileName, $directories = null){
    if(empty($directories)){
        $path = \Bitrix\Main\Application::getDocumentRoot() . OFD_BITRIX_TMP;
    }else{
        $path = \Bitrix\Main\Application::getDocumentRoot() . OFD_BITRIX_TMP.$directories."/";
    }
    $file = fopen($path.$fileName.'.txt', 'r'); // открываем файл
    $str = "";
    // считываем все из файла
    while (($buffer = fgets($file, 128)) !== false) {
        $str .= $buffer;
    }
    $array = unserialize($str); // преобразовываем строку в массив
    return $array;
}
function readArrayInFile($name, $full = null, $directories = null)
{
    if(empty($directories)){
        $path = \Bitrix\Main\Application::getDocumentRoot() . OFD_BITRIX_TMP;
    }else{
        $path = \Bitrix\Main\Application::getDocumentRoot() . OFD_BITRIX_TMP.$directories."/";
    }
    $file = fopen($path . $name . ".txt", 'r');
    if ($full) {
        while (!feof($file)) {
            $str = fgets($file);
            $array[] = unserialize($str);
        }
        fclose($file);
        $array = array_diff($array, array(''));
    } else {
        $str = "";
        while (($buffer = fgets($file, 128)) !== false) {
            $str .= $buffer;
        }
        $array = unserialize($str);
    }

    return $array;
}

function writeArrayInFile($array, $name, $directories = null)
{
    if(empty($directories)){
        $path = \Bitrix\Main\Application::getDocumentRoot() . OFD_BITRIX_TMP;
    }else{
        $path = \Bitrix\Main\Application::getDocumentRoot() . OFD_BITRIX_TMP.$directories."/";
    }

    if (is_array($array)) {
        $serArray = serialize($array);
    } else {
        $serArray = $array;
    }

    $file = fopen($path . $name . ".txt", "a+");
    fputs($file, "$serArray\n");
    fclose($file);
}


function postDataContact ($idContact, $path){
    $url = "https://".$_SERVER["SERVER_NAME"]."/crm/company/show/".$path;
    //$ServerIP = $_SERVER['SERVER_NAME'];
    $ServerIP = "bitrix.energocomm.ru";
    $ServerPort = 443;
    $strRequest = "";

    $data="idContragent=$idContact";
    //$fp = fsockopen($ServerIP, 443, $errno, $errstr, 10);
    $FP = fsockopen("ssl://".$ServerIP, $ServerPort, $errno, $errstr, 20);

    $strRequest .= "POST ".$path." HTTP/1.0\r\n";

    $strRequest .= "User-Agent: BitrixSMUpdater\r\n";
    $strRequest .= "Accept: */*\r\n";
    $strRequest .= "Host: ".$ServerIP."\r\n";
    $strRequest .= "Accept-Language: en\r\n";
    $strRequest .= "Content-type: application/x-www-form-urlencoded\r\n";
    $strRequest .= "Content-length: ".strlen($data)."\r\n\r\n";
    $strRequest .= "$data";
    $strRequest .= "\r\n";

    //fputs($fp, $out);
    fputs($FP, $strRequest);
    while($gets=fgets($FP,2048))
    {
        //pre($gets);
    }
    fclose($FP);
    return array($ServerIP,$FP, $strRequest);
}



function postData ($idContragent, $path){
    $url = "https://".$_SERVER["SERVER_NAME"]."/crm/company/show/".$path;
    //$ServerIP = $_SERVER['SERVER_NAME'];
    $ServerIP = "bitrix.energocomm.ru";
    $ServerPort = 443;
    $strRequest = "";

    $data="idContragent=$idContragent";
    //$fp = fsockopen($ServerIP, 443, $errno, $errstr, 10);
    $FP = fsockopen("ssl://".$ServerIP, $ServerPort, $errno, $errstr, 20);

    $strRequest .= "POST ".$path." HTTP/1.0\r\n";

    $strRequest .= "User-Agent: BitrixSMUpdater\r\n";
    $strRequest .= "Accept: */*\r\n";
    $strRequest .= "Host: ".$ServerIP."\r\n";
    $strRequest .= "Accept-Language: en\r\n";
    $strRequest .= "Content-type: application/x-www-form-urlencoded\r\n";
    $strRequest .= "Content-length: ".strlen($data)."\r\n\r\n";
    $strRequest .= "$data";
    $strRequest .= "\r\n";

    //fputs($fp, $out);
    fputs($FP, $strRequest);
    while($gets=fgets($FP,2048))
    {
        //pre($gets);
    }
    fclose($FP);
    return array($ServerIP,$FP, $strRequest);
}


function pre($pre){
    echo '<hr><pre>';
    var_export($pre);
    echo '</pre>';
}
function ownerIdGuid ($owner){
    switch ($owner) {
        case "365":
            $manager = BPM_MANAGER_VLASENKO;
            break;
        case "396":
            $manager = BPM_MANAGER_BUNTOVA;
            break;
        case "410":
            $manager = BPM_MANAGER_GRIBANOVA;
            break;
        case "398":
            $manager = BPM_MANAGER_KOPEYKINA;
            break;
        case "285":
            $manager = BPM_MANAGER_LUTAY;
            break;
        case "399":
            $manager = BPM_MANAGER_SERGEENKO;
            break;
        case "346":
            $manager = BPM_MANAGER_GDANOVA;
            break;
        case "302":
            $manager = BPM_MANAGER_ZAMARAEVA;
            break;
        case "426":
            $manager = BPM_MANAGER_KOLOMYCEV;
            break;
        case "275":
            $manager = BPM_MANAGER_SHEMYKIN;
            break;
        case "402":
            $manager = BPM_MANAGER_GAVRILOV;
            break;
        case "300":
            $manager = BPM_MANAGER_GUREV;
            break;
        case "357":
            $manager = BPM_MANAGER_DEREVYNSKIY;
            break;
        case "403":
            $manager = BPM_MANAGER_KRYLOV;
            break;
        case "347":
            $manager = BPM_MANAGER_MAKSIMOV;
            break;
        case "342":
            $manager = BPM_MANAGER_MALCEV;
            break;
        case "259":
            $manager = BPM_MANAGER_MASLENNIKOVA;
            break;
        case "400":
            $manager = BPM_MANAGER_POPOV;
            break;
        case "161":
            $manager = BPM_MANAGER_FEOKTISOVA;
            break;
        case "301":
            $manager = BPM_MANAGER_SMOLYKOV;
            break;
        case "311":
            $manager = BPM_MANAGER_STEBLIN;
            break;
        case "401":
            $manager = BPM_MANAGER_TABAKOV;
            break;
        case "299":
            $manager = BPM_MANAGER_COKOLENKO;
            break;
        case "388":
            $manager = BPM_MANAGER_SHKURENKOV;
            break;
        case "406":
            $manager = BPM_MANAGER_YKOVLEVA;
            break;
        default:
            $manager = BPM_MANAGER_BUNTOVA;
    }
    return ($manager);
}

/**
 * Обновление
 * @param $entity_id - имя объекта
 * @param $value_id - идентификатор элемента ( ID контрагента)
 * @param $uf_id - имя пользовательского свойства (UF_CRM_COMPANY_GUID)
 * @param $uf_value - запись значения
 * @return mixed
 */

function SetUserField($entity_id, $value_id, $uf_id, $uf_value)
{
    return $GLOBALS["USER_FIELD_MANAGER"]->Update(
        $entity_id,
        $value_id,
        Array($uf_id => $uf_value)
    );
}

/**
 * Чтение
 * @param $entity_id - имя объекта CRM_COMPANY
 * @param $value_id - идентификатор элемента ( ID контрагента)
 * @param $uf_id - имя пользовательского свойства (UF_CRM_COMPANY_GUID)
 * @return mixed
 */
function GetUserField($entity_id, $value_id, $uf_id)
{ //считывание значения
    $arUF = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields($entity_id, $value_id);
    return $arUF[$uf_id]["VALUE"];
}

/**
 * Форматирование даты в формат Atom или ISO-8601, получение текущей даты в формате Atom или ISO-8601
 * @param null $format
 * @return string 2017-07-18T12:09:26.000
 */
function dataTimeformat($format = null, $convert = null, $newdat = null)
{
    if ($format) {
        $date = new DateTime($format, new DateTimeZone("Europe/Moscow"));
        return ($date->format('Y-m-d'));
    } elseif ($convert) {
        $date = new DateTime($convert, new DateTimeZone("Europe/Moscow"));
        return ($date->format('Y-m-d H:i:s'));
    } elseif ($newdat) {
        $date = new DateTime("", new DateTimeZone("Europe/Moscow"));
        return ($date->format('Y-m-d H:i:s'));
    } else {
        $date = new DateTime("", new DateTimeZone("Europe/Moscow"));
        return ($date->format('Y-m-d\TH:i:s'));
    }
}

function convertTime($convert)
{
    if ($convert) {
        $date = new DateTime("$convert", new DateTimeZone("Europe/Moscow"));
        return ($date->format('Y-m-d\TH:i:s'));
    }
}

function returnIdCodeIblock($code)
{
    $res = CIBlock::GetList(
        Array(),
        Array(
            "CODE" => $code,
            "CHECK_PERMISSIONS" => "N"
        )
    );
    while ($ar_res = $res->Fetch()) {
        return ($ar_res['ID']);
    }
}

function metkaColTime($id, $uf_metka, $new = 0)
{
    $req = new \Bitrix\Crm\EntityRequisite();
    $rser = $req->getList(array(
        "filter" => array(
            "ENTITY_ID" => $id,
            "ENTITY_TYPE_ID" => CCrmOwnerType::Company,
            "PRESET_ID" => array(1, 2)
        )
    ));
    $rows = $rser->fetchAll();
    foreach ($rows as $contragent) {
        $entity_id = "CRM_COMPANY";

        $metka_time = GetUserField($entity_id, $id, $uf_metka);
        if (empty($metka_time) || $new > 0) {
            $entity_id = "CRM_COMPANY";
            $adress = Bitrix\Crm\EntityRequisite::getAddresses($contragent["ID"]);

            $bank = new \Bitrix\Crm\EntityBankDetail();
            $dbRes = $bank->getList(array(
                'filter' => array('ENTITY_ID' => $contragent["ID"])
            ));
            $rowsd = $dbRes->fetchAll();

            foreach ($rowsd as $k => $vale) {
                switch ($k) {
                    default:
                        $verifity["bank"][$k] = $vale;
                }

            }
            $dbResMultiFields = CCrmFieldMulti::GetList(array(), array('ENTITY_ID' => 'COMPANY', 'ELEMENT_ID' => $id));
            while ($arMultiFields = $dbResMultiFields->Fetch()) {
                $comunicetions[] = $arMultiFields;
            }

            $col_adress = count($adress);
            $col_svyz = count($comunicetions);
            $col_bank = count($rowsd);
            $eventDate = ConvertTimeStamp(time() + CTimeZone::GetOffset(), 'FULL', SITE_ID);
            $res["b"] = $col_bank;
            $res["a"] = $col_adress;
            $res["s"] = $col_svyz;
            $res["t"] = $eventDate;
            if ($uf_metka == CONTRAGENT_METKA_ADD) {
                $eventDate = ConvertTimeStamp(time() - 1 * 60 * 60 + CTimeZone::GetOffset(), 'FULL', SITE_ID);
            }
            SetUserField($entity_id, $id, $uf_metka, $eventDate . "/" . count($rowsd) . "/" . $col_adress . "/" . $col_svyz);
        }

    }
    //pre($rowsd);
    //exit;
    return ($res);
}

function checkformatPhone($phone = '')
{
    if (strlen($phone) != 12) {
        return false;
    } else if (substr($phone, 0, 1) != « + ») {
        return false;
    } else if (!preg_match("/[0-9+]/", $phone)) {
        return false;
    }
    return true;
}

function convertPhone($phone = '')
{
    $phone = str_replace(" ", "", $phone);

    if (empty($phone)) {
        return '';
    }


    $phone = trim($phone);
    if ($phone[0] == '+') {
        $phone = substr($phone, 1, strlen($phone) - 1);
        $plus = "0";
    }
    $phone = preg_replace("/[^0-9]/", "", $phone);
    if (substr($phone, 0, 1) == "8") {
        $phone = substr($phone, 1, strlen($phone) - 1);
        $plus = "1";
    }
    if ($plus == "0") {
        $phone = "+" . $phone;
    } else if ($plus == "1") {
        $phone = "+7" . $phone;
    }
    return $phone;
}

function lidDubleContragent($id){
    CModule::IncludeModule('crm');
    CCrmCompany::Delete($id);
}

function CsvGenerite($companyID, $file, $date = null, $arrHeaderCSV = null){
    CModule::IncludeModule('crm');
    require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/csv_data.php");
    $csvFile = new CCSVData();
    $csvFile = new CCSVData('R', true);
    $delimiter = ",";
    $csvFile->SetDelimiter($delimiter);
    if(!empty($arrHeaderCSV)){
        foreach ($arrHeaderCSV as $key){
            $HeaderCSV[] = iconv("UTF-8", "CP1251", $key);
        }

        $csvFile->SaveFile($file, $HeaderCSV);
    }
    $resCompany = CCrmCompany::GetListEx(array("ID" => "ASC"), array("ID" => $companyID),false,
        false, $date);
    if ($fieldsCompany = $resCompany->Fetch()) {
        $array[0] = iconv("UTF-8", "CP1251", $fieldsCompany['ID']);
        $array[2] = iconv("UTF-8", "CP1251", $fieldsCompany["TITLE"]);
        $array[1] = iconv("UTF-8", "CP1251", $fieldsCompany['DATE_CREATE']);
        $array[5] = iconv("UTF-8", "CP1251", $fieldsCompany['ASSIGNED_BY_NAME']." ".$fieldsCompany['ASSIGNED_BY_LAST_NAME']." ".$fieldsCompany['ASSIGNED_BY_SECOND_NAME']);
        $array[6] = iconv("UTF-8", "CP1251", $fieldsCompany['UF_ID_PROD']);
    }
    $req = new \Bitrix\Crm\EntityRequisite();
    $rser = $req->getList(array(
        "filter" => array(
            "ENTITY_ID" => $companyID,
            "ENTITY_TYPE_ID" => CCrmOwnerType::Company,
            "PRESET_ID" => array(1,2)
        )
    ));
    $rows = $rser->fetchAll();
    foreach ($rows as $company) {
        if(empty($array[2])){
            $array[2] = iconv("UTF-8", "CP1251", $company["RQ_COMPANY_NAME"]);
        }

        $array[3] = $company["RQ_INN"];
        $array[4] = $company["RQ_KPP"];
    }
    $csvFile->SaveFile($file, $array);
    return $file;
}
/**
 * Получение даты создания контрагента
 * @param $id
 * @return mixed
 */
function datCompany($id){
    $oCompany = new CCrmCompany;
    $resCompany = $oCompany->GetListEx(array("ID" => "ASC"), array("ID" => $id), array("DATE_CREATE"));
    if ($fieldsCompany = $resCompany->Fetch()) {
        $data = $fieldsCompany['DATE_CREATE'];
    }
    return $data;
}
/*
 * Поиск тикета по гуиду из BPM
 */
function tiketVerifityGuid ($guid){
    if(CModule::IncludeModule('iblock')) {

        $iblock = array(77, 78, 89, 90);
        $result = array();

        foreach ($iblock as $key => $ib) {

            $arSelect = Array("ID", "PROPERTY_ID_ZADACHI_PO_OBRASHCHENIYU");
            $arFilter = Array("IBLOCK_ID" => $ib, "PROPERTY_ID_ZADACHI_PO_OBRASHCHENIYU" => $guid);
            $res = CIBlockElement::GetList(array(), $arFilter, false, array(), $arSelect);
            while ($ob = $res->GetNextElement()) {
                $arFields = $ob->GetFields();
                $result = $arFields["ID"];
                pre($arFields);
            }
            if (empty($result)) {
                continue;
            } else {
                return $result;
            }

        }

        return false;
    }
}
/*
 * Уменьшает и выбирает первый элемент ассоциативного массива
 */
function array_kshift(&$arr)
{
    list($k) = array_keys($arr);
    $r  = array($k=>$arr[$k]);
    unset($arr[$k]);
    return $r;
}
/**
 * проверка на пустые гуиды контрагентов
 * если указан параметр $inn то выбирает с не пустым инн
 * @param null $id
 * @param null $inn
 * @return array
 */
function emptyGuidContr($id = null, $inn = null){
    if(empty($id)){
        $resBitrix = Reports::getAllCompanyBitrix();
        if(empty($inn)){
            foreach ($resBitrix as $key => $val){
                //Компании в Bitrix с пустым GUID_ID.
                if(empty($val['GUID'])){
                    $val = array_change_key_case($val);
                    $arResult[] = $val;
                }
            }

            return $arResult;
        }else{
            foreach ($resBitrix as $key => $val){
                //Компании в Bitrix с пустым GUID_ID.
                if(empty($val['GUID']) and !empty($val['INN'])){
                    $val = array_change_key_case($val);
                    $arResult[] = $val;
                }
            }

            return $arResult;
        }

    }else{
        $filter = array('ID' => $id, '!UF_LINK' => "",'=UF_CRM_COMPANY_GUID' => "");
        $dbContragentGet = CrmBitrixBpm::GetListEx(
            array(),
            $filter,
            false,
            false,
            array("TITLE","UF_CRM_COMPANY_GUID", "UF_LINK")
        );
        while ($dbContragent = $dbContragentGet->GetNext()) {
            $result[$dbContragent["ID"]][] = $dbContragent["TITLE"];
            $result[$dbContragent["ID"]][] = $dbContragent["UF_CRM_COMPANY_GUID"];
            $result[$dbContragent["ID"]][] = $dbContragent["UF_LINK"];
        }
        return $result;
    }

}

function idForInnKpp($inn = null, $kpp = null, $id =null){
    $requisite = new \Bitrix\Crm\EntityRequisite();
    $select = array('ENTITY_ID','ID','RQ_INN','RQ_KPP');
    if(empty($id)) {
        if (!empty($kpp)) {
            $res = $requisite->getList(
                array(
                    'order' => array('SORT' => 'ASC', 'ID' => 'ASC'),
                    'select' => $select,
                    "filter" => array(
                        "RQ_INN" => $inn,
                        "RQ_KPP" => $kpp,
                        "ENTITY_TYPE_ID" => CCrmOwnerType::Company,
                        "PRESET_ID" => array(1, 2)
                    )
                )
            );
        } elseif(empty($kpp)) {
            if(strlen($inn) == 12 or strlen($inn) == 15){
                $res = $requisite->getList(
                    array(
                        'order' => array('SORT' => 'ASC', 'ID' => 'ASC'),
                        'select' => $select,
                        "filter" => array(
                            "RQ_INN" => $inn,
                            "RQ_KPP" => '',
                            "ENTITY_TYPE_ID" => CCrmOwnerType::Company,
                            "PRESET_ID" => array(1, 2)
                        )
                    )
                );
            }else{
                return false;
            }

        }else{
            return false;
        }
    }else{
        $res = $requisite->getList(
            array(
                'order' => array('SORT' => 'ASC', 'ID' => 'ASC'),
                'select' => $select,
                "filter" => array(
                    "ENTITY_ID" => $id,
                    "ENTITY_TYPE_ID" => CCrmOwnerType::Company,
                    "PRESET_ID" => array(1, 2)
                )
            )
        );
    }


    $row = $res->fetchAll();
    foreach ($row as $item) {
        $arResult[] = $item;
    }

    if($arResult){
        if(count($arResult) == 1){
            return $arResult[0]["ENTITY_ID"];
        }
        $rez = [];
        if(count($arResult) > 1) {
            foreach ($arResult as  $v) {
                $rez[] = $v["ENTITY_ID"];
            }

            return ($rez);
        }
    }else{
        return false;
    }

}
function dopInfoContragent($all = null,$id,$rekvisit = null,$deals= null,$dogPlat=null,$inv = null){
    // проверка реквизитов 1
    if($rekvisit or $all){
        $select = array(
            "RQ_INN",
            "RQ_KPP",
            "ID",
            "ENTITY_ID"
        );
        $requisite = new \Bitrix\Crm\EntityRequisite();
        $res = $requisite->getList(
            array(
                'order' => array('SORT' => 'ASC', 'ID' => 'ASC'),
                'select' => $select,
                "filter" => array(
                    "ENTITY_ID" => $id,
                    "ENTITY_TYPE_ID" => CCrmOwnerType::Company,
                    "PRESET_ID" => array(1, 2)
                )
            )
        );
        $row = $res->fetchAll();
        foreach ($row as $key => $valu) {
            $result["requisite_".$valu['ID']] = $valu['ENTITY_ID'];
        }
        if(!empty($result)){
            return $result;
        }
    }
    //проверка сделок 2
    if($deals or $all){
        $deal = new CCrmDeal;
        $resDeal = $deal->GetList(array(), array("=COMPANY_ID" => $id), array("ID","COMPANY_ID"));
        if ($fieldsDeal = $resDeal->Fetch()) {
            $result["deal_".$fieldsDeal['ID']] = $fieldsDeal['COMPANY_ID'];
        }
        if(!empty($result)){
            return $result;
        }
    }
    // проверка договоров и реестра платежей 3
    if($dogPlat or $all){
        $nameProp = array(
            "40" => "KONTRAGENT",
            "86" => "COMPANY"
        );
        $result = [];
        $arElSelect = Array("ID","PROPERTY_*");

        foreach ($nameProp as $key=>$val){
            if($key == "40"){
                $name = "dogovor";
            }
            if($key == "86"){
                $name = "plateg";
            }
            $arElFilter = Array("IBLOCK_ID" => $key, "=PROPERTY_".$val => $id);
            $dbElem = CIBlockElement::GetList(Array(), $arElFilter, false, false, $arElSelect);

            while ($ob = $dbElem->GetNextElement()) {
                $arFieldsOld = $ob->GetFields();
                $arProps = $ob->GetProperties();
                $result[$name."_".$arFieldsOld["ID"]] = $arProps[$val]['VALUE'];
            }
        }
        if(!empty($result)){
            return $result;
        }
    }
    // проверка счетов 4
    if ($inv or $all){
        $invois = new CCrmInvoice;
        $resInvois = $invois->GetList(array(), array("=UF_COMPANY_ID" => $id),false,false, array("ID","UF_COMPANY_ID"));
        if ($fieldsInvois = $resInvois->Fetch()) {
            $result["invois_".$fieldsInvois['ID']] = $fieldsInvois["UF_COMPANY_ID"];
        }
        if(!empty($result)){
            return $result;
        }
    }

    if(empty($result)){
        return false;
    }else{
        return $result;
    }

}
function idGuidConnragent($id){
    $filter = array('=ID' => $id);
    $dbContragentGet = CrmBitrixBpm::GetListEx(
        array(),
        $filter,
        false,
        false,
        array("UF_CRM_COMPANY_GUID", "ID")
    );
    while ($dbContragent = $dbContragentGet->GetNext()) {
        $result = $dbContragent["UF_CRM_COMPANY_GUID"];
    }
    if($result){
        return $result;
    }else{
        return false;
    }
}
function contragents($smotr = null)
{
    $arResult = [];
    $connection = Bitrix\Main\Application::getConnection();
    if($smotr == 3){
        $musContragent = \Bitrix\Main\Application::getDocumentRoot() . OFD_BITRIX_TMP."dubles/EmptyContragentInnKpp.txt";
        $delContragent = \Bitrix\Main\Application::getDocumentRoot() . OFD_BITRIX_TMP."dubles/DeleteContragent.txt";
        if(file_exists($musContragent)){
            unlink($musContragent);
        }
        if(file_exists($delContragent)){
            unlink($delContragent);
        }
		$fyes = 1;
    }

    $strSql = "SELECT
				b_crm_company.ID,
				b_crm_company.TITLE,
				b_crm_company.COMPANY_TYPE,
                b_crm_requisite.RQ_INN, 
                b_crm_requisite.RQ_KPP, 
                b_crm_requisite.ENTITY_ID,
                b_uts_crm_company.UF_ID_PROD,
                b_uts_crm_company.UF_CRM_COMPANY_GUID
			  FROM b_crm_company 
			  LEFT JOIN b_crm_requisite ON b_crm_company.ID = b_crm_requisite.ENTITY_ID
			  LEFT JOIN b_uts_crm_company ON b_crm_company.ID = b_uts_crm_company.VALUE_ID
				ORDER BY b_crm_company.ID ASC";

    $resCompany = $connection->query($strSql);
    while ($arCompany = $resCompany->Fetch()) {
        $countRequisite = 0;

        if(!empty($arResult[$arCompany['ID']]['COUNT_REQUISITE'])){
            $countRequisite = $arResult[$arCompany['ID']]['COUNT_REQUISITE'] + 1;
        } elseif(!empty($arCompany['ENTITY_ID'])) {
            $countRequisite = 1;
        }
        $arResult[$arCompany['ID']] = array(
            "INN" => $arCompany['RQ_INN'],
            "KPP" => $arCompany['RQ_KPP'],
            "IDPROD" => $arCompany['UF_ID_PROD'],
            "COMPANY_TYPE" => $arCompany['COMPANY_TYPE'],
            "GUID" => strtolower($arCompany['UF_CRM_COMPANY_GUID']), //Регистр
            "NAME" => $arCompany['TITLE'],
            "COUNT_REQUISITE"   => $countRequisite,
        );
        if(empty($arCompany['RQ_INN']) and empty($arCompany['RQ_KPP']) and $countRequisite < 2){
            $emptyInnKpp[$arCompany['ID']] = array($arCompany['UF_CRM_COMPANY_GUID']);
			if($fyes){
				writeArrayInFile(strtolower($arCompany['UF_CRM_COMPANY_GUID']), "EmptyContragentInnKpp","dubles");
				writeArrayInFile(strtolower($arCompany['ID']), "DeleteContragent","dubles");
			}
        }

    }

    foreach ($arResult as $key=>$val){
        if(empty($val["INN"]) and empty($val["KPP"])){
            //Контрагенты для проверки связанных сущностей
            $mus1[$key] = $val["NAME"];
            continue;
        }else{
            $gud2[$key] = array($val["INN"],$val["KPP"],$val["IDPROD"],$val["COMPANY_TYPE"],$val["NAME"]);
        }
    }

    foreach ($gud2 as $g=>$gu){
        if(strlen($gu[0]) == 10 and empty($gu[1])){
            //Компании с пустым КПП
            $emptyCompany9[$g] = array($gu[0]);

        }else{
            //Нормальные компании
            if(strlen($gu["0"]) == 10 and !empty($gu[1])){
                $Company3[$g] = array($gu[0],$gu[1]);
                continue;
            }
            if(strlen($gu["0"]) == 12 and empty($gu[1])){
                $IP4[$g] = array($gu[0]);
                continue;
            }else{
                $Oter5[$g] = array($gu);
            }

        }
    }

    writeArrayInFileA($mus1, "empty_contr_proverka","dubles");
    writeArrayInFileA($emptyCompany9, "company_empty_kpp","dubles");
    writeArrayInFileA($Company3, "company_inn_kpp","dubles");
    writeArrayInFileA($IP4, "ipe_inn","dubles");
    writeArrayInFileA($Oter5, "oter","dubles");

    $massiv["COMPANY"] = $Company3;
    $comp_arr = array_count_values(array_map("serialize", $massiv["COMPANY"]));
    $comp_res = array_filter($comp_arr, function ($elem) {
        return ($elem > 1);
    });
    $result_array_company6 = [];
    foreach ($comp_res as $ky => $value) {
        $unser = unserialize($ky);
        $unser_arr["inn"] = $unser[0];
        $unser_arr["kpp"] = $unser[1];
        $id = idForInnKpp($unser_arr["inn"], $unser_arr["kpp"]);
        $unser_arr['id_name'] = $id;
        array_push($result_array_company6, $unser_arr);//добавляем в результирующий массив
    }

    $massiv["IP"] = $IP4;
    $ip_arr = array_count_values(array_map("serialize", $massiv["IP"]));
    $ip_res = array_filter($ip_arr, function ($elemip) {
        return ($elemip > 1);
    });
    $result_array_ip7 = [];
    foreach ($ip_res as $kyp => $valup) {
        $ipser = unserialize($kyp);
        $ipser_arr["inn"] = $ipser[0];
        $idip = idForInnKpp($ipser_arr["inn"]);
        $ipser_arr['id_name'] = $idip;
        array_push($result_array_ip7, $ipser_arr);//добавляем в результирующий массив
    }

    $massiv["emptyCompany"] = $emptyCompany9;
    $emptyCompany_arr = array_count_values(array_map("serialize", $massiv["emptyCompany"]));
    $emptyCompany_res = array_filter($emptyCompany_arr, function ($ele) {
        return ($ele > 1);
    });
    $result_array_emptyCompany10 = [];
    foreach ($emptyCompany_res as $ker => $v) {
        $emptyCompanySer = unserialize($ker);
        $emptyCompany_arr["inn"] = $emptyCompanySer[1];
        $emptyCompany_arr["type"] = $emptyCompanySer[0];
        $eCompany = idForInnKpp($emptyCompany_arr["inn"]);
        $eCompany_arr['id_name'] = $eCompany;
        array_push($result_array_emptyCompany10, $eCompany_arr);//добавляем в результирующий массив
    }



    $resBitrix = array($mus1,$gud2,$Company3, $IP4, $Oter5,$emptyCompany9, $result_array_company6,$result_array_ip7,$result_array_emptyCompany10,$emptyInnKpp);

    $col = count($resBitrix[0])+count($resBitrix[1]);
    echo "Всего контрагентов: ".$col."<br>";
    echo "0.) Группа 0 - предварительно пустые: ".count($resBitrix[0])."<br>";
    echo "1.) Предварительно нормальных: ".count($resBitrix[1])."<br>";
    echo "2.) Группа 1 - компаний: ".count($resBitrix[2])."<br>";
    echo "3.) Группа 2 - ИП: ".count($resBitrix[3])."<br>";
    echo "4.) Группа 3 - непонятные контрагенты (не верный ИНН или Физ.лица): ".count($resBitrix[4])."<br>";
    echo "5.) Группа 4 - компаний с ИНН и без КПП: ".count($resBitrix[5])."<br>";
    echo "6.) Дубли компаний: ".count($resBitrix[6])."<br>";
    echo "7.) Дубли ИП: ".count($resBitrix[7])."<br>";
    echo "8.) Дубли в 4 группе: ".count($resBitrix[8])."<br>";
    echo "9.) Контрагенты без ИНН и КПП: ".count($resBitrix[9])."<br>";
    if($smotr == 1){
        return ($resBitrix);
    }
}
//Парсинг параметров строки
function parseParamsString($nameParametr, $strSearch){
    //Обработка исключительной ситуации для параметра ID
    if($nameParametr == "ID") {
        preg_match_all("/" . $nameParametr . ": (.*?)\n/i", $strSearch , $out);
        $last_key = key(array_slice($out, -1, 1, TRUE));

        if(array_key_exists(1,$out[$last_key])) {
            $last_key_two = key(array_slice($out[$last_key], -1, 1, TRUE));
            $name = str_replace(array("\r\n", "\r", "\n"), '',  strip_tags($out[$last_key][$last_key_two]));
            $name = trim($name);
            return $name;
        } else {
            //Исключительная ситуация для нестандартного формата письма FullAccaunt
            preg_match_all("/" . $nameParametr . ": (.*?)$/i", $strSearch , $out);
            $last_key = key(array_slice($out, -1, 1, TRUE));

            if(array_key_exists(0,$out[$last_key])) {
                $last_key_two = key(array_slice($out[$last_key], -1, 1, TRUE));
                $name = str_replace(array("\r\n", "\r", "\n"), '',  strip_tags($out[$last_key][$last_key_two]));
                $name = trim($name);
                return $name;
            }
            return false;
        }
    }

    if($nameParametr == "Accaunt") {
        preg_match("/" . $nameParametr . ": (.*?)\n/s", $strSearch , $out);
        //pre($out);
        if(array_key_exists(1,$out)) {
            $name = str_replace(array("\r\n", "\r", "\n"), '',  strip_tags($out[1]));
            $name = trim($name);
            return $name;
        } else {
            return false;
        }
    }

    preg_match("/" . $nameParametr . ": (.*?)\n/i", $strSearch , $out);


    if(array_key_exists(1,$out)) {
        $name = str_replace(array("\r\n", "\r", "\n"), '',  strip_tags($out[1]));
        $name = trim($name);
        return $name;
    } else {
        return false;
    }
}
/*function InsertToBPMNewContact()
{
    $logger = Logger::getLogger('InsertToBPMNewContact','ofd.bitrix24/InsertToBPMNewContact.txt');
    $connection = Bitrix\Main\Application::getConnection();
    $sql = 'SELECT IDBITRIX FROM connectionbpmbitrix WHERE PersonID = \''."0".'\' ';
    $recordset = $connection->query($sql,20);
    while ($record = $recordset->fetch())
    {
       // $arResult[]=$record['IDBITRIX'];
        $idperson = $record['IDBITRIX'];
        $guidperson = GetUserField("CRM_CONTACT", $record['IDBITRIX'], "UF_BPMCONTACTID");
        $inncompany = GetUserField("CRM_CONTACT", $record['IDBITRIX'], "UF_BPMCONTACTID");

        $name = "";
        $mobilephone = "";
        $email = "";

        $result = CCrmContact::GetListEx(array(), array('=ID' => $record['IDBITRIX'], 'CHECK_PERMISSIONS' => 'N'), false, false, array('*'));
        $fields = is_object($result) ? $result->Fetch() : null;
        if (is_array($fields)) {
            $name = $fields['FULL_NAME'];
        }

        $res = CCrmFieldMulti::GetList(array('ID' => 'ASC'), array('ELEMENT_ID' => $record['IDBITRIX'], 'TYPE_ID' => "PHONE"));
        if ($arRes = $res->Fetch()) {
            $mobilephone = $arRes['VALUE'];
        }

        $res = CCrmFieldMulti::GetList(array('ID' => 'ASC'), array('ELEMENT_ID' => $record['IDBITRIX'], 'TYPE_ID' => "EMAIL"));
        if ($arRes = $res->Fetch()) {
            $email = $arRes['VALUE'];
        }
        $logger->log(array("ID=".$idperson."//INN=".$inncompany."//NAME=".$name."//MOBILEPHONE=".$mobilephone."//EMAIl=".$email));
        ContactBpmBitrix::contactInsertToBMP($idperson,$guidperson,$inncompany,$name,$mobilephone,$email);
    }



    return "InsertToBPMNewContact();";
}



function CronAgentZadacha3()
{
    $intcountelementstep = 1000;
    $logger = Logger::getLogger('forsynchronization_3zadacha', 'ofd.bitrix24/forsynchronization_3zadacha.txt');
    $logger->log("Запуск==================================");
    $connection = \Bitrix\Main\Application::getConnection();
    $sql = 'SELECT GUIDBPM_COMPANY FROM synchbpmbitrixcompanybpm WHERE STATYS_ANSWER_BPM_COMPANY = \'\'';
    $logger->log("Шаг равен=" . $intcountelementstep);
    $recordset = $connection->query($sql, $intcountelementstep);

    while ($record = $recordset->fetch()) {
        $logger->log($record['GUIDBPM_COMPANY']);
        $part1value = "";
        CModule::IncludeModule('crm');
        $res = CCrmCompany::GetList(array(), array('=UF_CRM_COMPANY_GUID' => $record['GUIDBPM_COMPANY'],"CHECK_PERMISSIONS" => "N"));
        if ($arRes = $res->Fetch()) {
            $part1value = $arRes['ID'];
            $logger->log("Нашли компанию=" . $arRes['ID']);
            $connection = Bitrix\Main\Application::getConnection();
            $sql = 'UPDATE synchbpmbitrixcompanybpm SET STATYS_ANSWER_BPM_COMPANY = \'' . " CANCEL" . '\' WHERE GUIDBPM_COMPANY=\'' . $record['GUIDBPM_COMPANY'] . '\' ';
            $connection->queryExecute($sql);
        }
        if (empty($part1value)) {
            $connection = Bitrix\Main\Application::getConnection();
            $sql = 'UPDATE synchbpmbitrixcompanybpm SET STATYS_ANSWER_BPM_COMPANY = \'' . "TOINSERT" . '\' WHERE GUIDBPM_COMPANY=\'' . $record['GUIDBPM_COMPANY'] . '\' ';
            $connection->queryExecute($sql);
        }
    }
    $logger->log("Конец==================================");
    return "CronAgentZadacha3();";
}


function CronAgentZadacha4()
{ CModule::IncludeModule('crm');
    $intcountelementstep = 100;
    QueryBpm::login();
    pre($_SESSION["BPM_COOKIES"]);
    $logger = Logger::getLogger('forsynchronization_4zadacha', 'ofd.bitrix24/forsynchronization_4zadacha.txt');
    $logger->log("Запуск==================================");
    $connection = \Bitrix\Main\Application::getConnection();
    $sql = 'SELECT GUIDBPM_COMPANY FROM synchbpmbitrixcompanybpm WHERE STATYS_ANSWER_BPM_COMPANY = \'TOSELECT\'';
    $logger->log("Шаг равен=" . $intcountelementstep);
    $recordset = $connection->query($sql, $intcountelementstep);
    while ($record = $recordset->fetch()) {
        $logger->log($record['GUIDBPM_COMPANY']);
        ContactBpmBitrix::SelectNAMEINNKPPFromBPMusingGUID($record['GUIDBPM_COMPANY']);
    }
    $logger->log("Конец==================================");
    return "CronAgentZadacha4();";
}


function CronAgentZadacha5()
{ CModule::IncludeModule('crm');
    $intcountelementstep = 1000;
    $logger = Logger::getLogger('forsynchronization_5zadacha', 'ofd.bitrix24/forsynchronization_5zadacha.txt');
    $logger->log("Запуск==================================");
    $connection = \Bitrix\Main\Application::getConnection();
    $sql = 'SELECT GUIDBPM_COMPANY,INN_BPM_COMPANY,KPP_BPM_COMPANY,NAME_BPM_COMPANY FROM synchbpmbitrixcompanybpm WHERE STATYS_ANSWER_BPM_COMPANY = \'TOINSERT\'';
    $logger->log("Шаг равен=" . $intcountelementstep);
    $recordset = $connection->query($sql, $intcountelementstep);

    while ($record = $recordset->fetch()) {
        $logger->log($record['GUIDBPM_COMPANY']);
        global $DB;
        $arFields['~DATE_CREATE'] = $DB->CurrentTimeFunction();
        $arFields['~DATE_MODIFY'] = $DB->CurrentTimeFunction();
        $iUserId = "372";

        if (isset($arFields['ID']))
            unset($arFields['ID']);
        if (isset($arFields['DATE_CREATE']))
            unset($arFields['DATE_CREATE']);
        $arFields['~DATE_CREATE'] = $DB->CurrentTimeFunction();
        $arFields['~DATE_MODIFY'] = $DB->CurrentTimeFunction();
        if (!isset($arFields['CREATED_BY_ID']) || intval($arFields['CREATED_BY_ID']) <= 0)
            $arFields['CREATED_BY_ID'] = $iUserId;
        if (!isset($arFields['MODIFY_BY_ID']) || intval($arFields['MODIFY_BY_ID']) <= 0)
            $arFields['MODIFY_BY_ID'] = $iUserId;
        if (!isset($arFields['ASSIGNED_BY_ID']) || intval($arFields['ASSIGNED_BY_ID']) <= 0)
            $arFields['ASSIGNED_BY_ID'] = $iUserId;

        $arFields['TITLE'] = $record['NAME_BPM_COMPANY'];
        $arFields['COMPANY_TYPE'] = "CUSTOMER";
        $arFields['UF_FULL_NAME'] = $record['NAME_BPM_COMPANY'];

        $ID = intval($DB->Add('b_crm_company', $arFields, array()));
		$result = $arFields['ID'];// = $ID
        if (!empty($result)) {
            SetUserField("CRM_COMPANY", $result, "UF_CRM_INN", $record['INN_BPM_COMPANY']);
            SetUserField("CRM_COMPANY", $result, "UF_CRM_KPP", $record['KPP_BPM_COMPANY']);
            SetUserField("CRM_COMPANY", $result, "UF_CRM_COMPANY_GUID", $record['GUIDBPM_COMPANY']);
            $connection = Bitrix\Main\Application::getConnection();
            $sql = 'UPDATE synchbpmbitrixcompanybpm SET STATYS_ANSWER_BPM_COMPANY = \'' . "INSERTDONE" . '\' WHERE GUIDBPM_COMPANY=\'' . $record['GUIDBPM_COMPANY'] . '\' ';
            $connection->queryExecute($sql);
        }
    }
    $logger->log("Конец==================================");
    return "CronAgentZadacha5();";
}

function CronAgentZadacha6()
{ CModule::IncludeModule('crm');
    $logger = Logger::getLogger('forsynchronization_6zadacha', 'ofd.bitrix24/forsynchronization_6zadacha.txt');
    $logger->log("Запуск==================================");
    $res = CCrmCompany::GetList(array(), array('=UF_CRM_COMPANY_GUID' => "","CHECK_PERMISSIONS" => "N"));
    while ($arRes = $res->Fetch()) {
        $guidforbpm = RestBpmBitrix::generate_guid();
        $logger->log("guidforbpm=" . $guidforbpm);
        $idbitrixcompany = $arRes['ID'];
        $logger->log("idbitrixcompany=" . $idbitrixcompany);

        //$idbitrixcompany=$arRes['ID'];
        // $logger->log("idbitrixcompany=".$idbitrixcompany);
        $entity_id = "CRM_COMPANY";
        $uf_guid = "UF_CRM_INN";
        $inncompany = GetUserField($entity_id, $idbitrixcompany, $uf_guid);

        $statys = "INSERTTOBPM";
        $logger->log("statys=" . $statys);
        $connection = Bitrix\Main\Application::getConnection();
        $sql = 'INSERT INTO synchbitrixtobpmcompany (ID_BITRIX,GUID_BPM_COMPANY,INN_BPM_COMPANY,STATYS_ANSWER_BPM_COMPANY) VALUES (\'' . $idbitrixcompany . '\',\'' . $guidforbpm . '\',\'' . $inncompany . '\',\'' . $statys . '\');';
        $connection->queryExecute($sql);
    }
    $logger->log("Конец==================================");
    return "CronAgentZadacha6();";
}

function CronAgentZadacha7()
{ CModule::IncludeModule('crm');
    $logger = Logger::getLogger('forsynchronization_7zadacha', 'ofd.bitrix24/forsynchronization_7zadacha.txt');
    $intcountelementstep = 500;
    QueryBpm::login();
    pre($_SESSION["BPM_COOKIES"]);
    $logger->log("Запуск==================================");
    $connection = \Bitrix\Main\Application::getConnection();
//sql без инн
    $sql = 'SELECT ID_BITRIX,GUID_BPM_COMPANY,INN_BPM_COMPANY FROM synchbitrixtobpmcompany WHERE STATYS_ANSWER_BPM_COMPANY = \'INSERTTOBPM\'';
//sql c инн
 //$sql = 'SELECT ID_BITRIX,GUID_BPM_COMPANY,INN_BPM_COMPANY FROM synchbitrixtobpmcompany WHERE STATYS_ANSWER_BPM_COMPANY = \'INSERTTOBPM\' AND INN_BPM_COMPANY <> \'\'';
    $logger->log("Шаг равен=" . $intcountelementstep);
    $recordset = $connection->query($sql, $intcountelementstep);
    while ($record = $recordset->fetch()) {
        $verify = array();
        $verify["id"] = $record['ID_BITRIX'];
        $logger->log("id=" . $verify["id"]);
        $verify["inn"] = $record['INN_BPM_COMPANY'];
        $logger->log("inn=" . $verify["inn"]);
        $verify["guid"] = $record['GUID_BPM_COMPANY'];
        $logger->log("guid=" . $verify["guid"]);
        $verify['fulname'] = "";

		//SetUserField("CRM_COMPANY", $record['ID_BITRIX'], "UF_CRM_COMPANY_GUID", $record['GUID_BPM_COMPANY']);


        $res = CCrmCompany::GetList(array(), array("ID"=>$record['ID_BITRIX'],"CHECK_PERMISSIONS" => "N"));
        if ($arRes = $res->Fetch()) { $verify['fulname']= $arRes['TITLE'];}

        $logger->log("fulname".$verify["fulname"]);

        $que = CrmBitrixBpm::contragenAdd($verify);
        $status = $que["status"];
        $logger->log(array($que));
        $logger->log("Статус " . $status);
        if ($status == 200) {
            $connection = Bitrix\Main\Application::getConnection();
            $sql = 'UPDATE synchbitrixtobpmcompany SET STATYS_ANSWER_BPM_COMPANY = \'' . "INSERTBPMDONEOK" . '\' WHERE GUID_BPM_COMPANY=\'' . $record['GUID_BPM_COMPANY'] . '\' ';
            $connection->queryExecute($sql);
        }
        if ($status == 500) {
            $connection = Bitrix\Main\Application::getConnection();
            $sql = 'UPDATE synchbitrixtobpmcompany SET STATYS_ANSWER_BPM_COMPANY = \'' . "INSERTBPMDONEOK__" . '\' WHERE GUID_BPM_COMPANY=\'' . $record['GUID_BPM_COMPANY'] . '\' ';
            $connection->queryExecute($sql);
        }


    }
    $logger->log("Конец==================================");

    return "CronAgentZadacha7();";
}

function CronAgentZadacha10()
{ CModule::IncludeModule('crm');
//Задача10 По GUID BPM контакта проверяем наличие на стороне BITRIX контакта
    $intcountelementstep = 100;
    $logger = Logger::getLogger('forsynchronization_10zadacha', 'ofd.bitrix24/forsynchronization_10zadacha.txt');
    $logger->log("Запуск==================================");
    $connection = \Bitrix\Main\Application::getConnection();
    $sql = 'SELECT GUIDBPM_CONTACT FROM synchbpmbitrix WHERE STATYS_ANSWER_BPM_CONTACT = \'\'';
    $logger->log("Шаг равен=" . $intcountelementstep);
    $recordset = $connection->query($sql, $intcountelementstep);
    while ($record = $recordset->fetch()) {
        $logger->log($record['GUIDBPM_CONTACT']);
        $part1value = "";
        $res = CCrmContact::GetList(array(), array('=UF_BPMCONTACTID' => $record['GUIDBPM_CONTACT'],"CHECK_PERMISSIONS" => "N"));
        if ($arRes = $res->Fetch()) {
            $part1value = $arRes['ID'];
            $logger->log("Нашли контакт=" . $arRes['ID']);
            $connection = Bitrix\Main\Application::getConnection();
            $sql = 'UPDATE synchbpmbitrix SET STATYS_ANSWER_BPM_CONTACT = \'' . " CANCEL" . '\' WHERE GUIDBPM_CONTACT=\'' . $record['GUIDBPM_CONTACT'] . '\' ';
            $connection->queryExecute($sql);
        }
        if (empty($part1value)) {
            $connection = Bitrix\Main\Application::getConnection();
            $sql = 'UPDATE synchbpmbitrix SET STATYS_ANSWER_BPM_CONTACT = \'' . "TOSELECT" . '\' WHERE GUIDBPM_CONTACT=\'' . $record['GUIDBPM_CONTACT'] . '\' ';
            $connection->queryExecute($sql);
        }
    }
    $logger->log("Конец==================================");
    return "CronAgentZadacha10();";
}


function CronAgentZadacha11()
{ CModule::IncludeModule('crm');
//Задача11 скачивание данных с BPM в Bitrix
    $logger = Logger::getLogger('forsynchronization_11zadacha', 'ofd.bitrix24/forsynchronization_11zadacha.txt');
    QueryBpm::login();
    pre($_SESSION["BPM_COOKIES"]);
    $logger->log("Запуск==================================");
    $connection = \Bitrix\Main\Application::getConnection();
    $sql = 'SELECT GUIDBPM_CONTACT FROM synchbpmbitrix WHERE STATYS_ANSWER_BPM_CONTACT = \'' . "TOSELECT" . '\'';
    $recordset = $connection->query($sql, 100);
    while ($record = $recordset->fetch()) {
        ContactBpmBitrix::SelectDatafromBPMusingGUID($record['GUIDBPM_CONTACT']);
    }
    $logger->log("Конец==================================");
    return "CronAgentZadacha11();";
}


function CronAgentZadacha12()
{ CModule::IncludeModule('crm');
//Задача 12 GUID контрагента-> ID Bitirix контрагента
    $logger = Logger::getLogger('forsynchronization_12zadacha', 'ofd.bitrix24/forsynchronization_12zadacha.txt');
    $logger->log(array("Запуск"));
    $connection = \Bitrix\Main\Application::getConnection();
    $sql = 'SELECT GUIDBPM_CONTACT,GUID_BPM_COMPANY FROM synchbpmbitrix WHERE STATYS_ANSWER_BPM_CONTACT = \'' . "TOINSERTWITHOUTGUID" . '\'';
    $recordset = $connection->query($sql, 100);
    while ($record = $recordset->fetch()) {
        $part1value = "";
        $logger->log("`````````````````````````````````````````````````````````````````````````````````````````````````");
        $logger->log("GUIDBPMCOMPANY= " . $record['GUID_BPM_COMPANY']);
        if (!empty($record['GUID_BPM_COMPANY'])) {
            $res = CCrmCompany::GetList(array(), array('=UF_CRM_COMPANY_GUID' => $record['GUID_BPM_COMPANY'],"CHECK_PERMISSIONS" => "N"));
            if ($arRes = $res->Fetch()) {
                $part1value = $arRes['ID'];
                $logger->log("1_BitrixCompanyID= " . $part1value);
            }

        }
        if (empty($part1value)) {
            $connection = Bitrix\Main\Application::getConnection();
            $sql = 'UPDATE synchbpmbitrix SET ID_BITRIX_COMPANY = \'' . "" . '\' WHERE GUIDBPM_CONTACT=\'' . $record['GUIDBPM_CONTACT'] . '\' ';
            $connection->queryExecute($sql);
            $logger->log("BPM Company not found in Bitrix guid=" . $record['GUID_BPM_COMPANY']);

            $connection = Bitrix\Main\Application::getConnection();
            $sql = 'UPDATE synchbpmbitrix SET STATYS_ANSWER_BPM_CONTACT = \'' . "TOINSERT__" . '\' WHERE GUIDBPM_CONTACT=\'' . $record['GUIDBPM_CONTACT'] . '\' ';
            $connection->queryExecute($sql);

        } else {
            $connection = Bitrix\Main\Application::getConnection();
            $sql = 'UPDATE synchbpmbitrix SET ID_BITRIX_COMPANY = \'' . $part1value . '\' WHERE GUIDBPM_CONTACT=\'' . $record['GUIDBPM_CONTACT'] . '\' ';
            $connection->queryExecute($sql);

            $connection = Bitrix\Main\Application::getConnection();
            $sql = 'UPDATE synchbpmbitrix SET STATYS_ANSWER_BPM_CONTACT = \'' . "TOINSERT" . '\' WHERE GUIDBPM_CONTACT=\'' . $record['GUIDBPM_CONTACT'] . '\' ';
            $connection->queryExecute($sql);
        }
        $logger->log("`````````````````````````````````````````````````````````````````````````````````````````````````");

    }
    return "CronAgentZadacha12();";
}


function CronAgentZadacha13()
{ CModule::IncludeModule('crm');
//Задача 13. Добавляем все контакты BPM в Bitrix
    function multiexplode($delimiters, $string)
    {

        $ready = str_replace($delimiters, $delimiters[0], $string);
        $launch = explode($delimiters[0], $ready);
        return $launch;
    }

    $logger = Logger::getLogger('forsynchronization_13zadacha', 'ofd.bitrix24/forsynchronization_13zadacha.txt');
    $logger->log(array("Запуск"));
    $connection = \Bitrix\Main\Application::getConnection();
    $sql = 'SELECT GUIDBPM_CONTACT,FIO_BPM_CONTACT,PHONE_BPM_CONTACT,EMAIL_BPM_CONTACT,GUID_BPM_COMPANY,INN_BPM_COMPANY,ID_BITRIX_COMPANY FROM synchbpmbitrix WHERE STATYS_ANSWER_BPM_CONTACT = \'TOINSERT\'';
    $logger->log($sql);
//echo $sql;
    $recordset = $connection->query($sql, 100);
    while ($record = $recordset->fetch()) {
        $logger->log("begin-------------------------------------------------------------------");

        $logger->log($record['GUIDBPM_CONTACT']);
        $arParams = array();
        $namerecord = "";
        $guidcontactrecord = "";
        $phonerecord = "";
        $emailcontactrecord = "";
        $guidcompanyrecord = "";
        $inncompanyrecord = "";
        $idbitrixcompanyrecord = "";
        $namerecord = $record['FIO_BPM_CONTACT'];//
        $guidcontactrecord = $record['GUIDBPM_CONTACT'];
        $phonerecord = $record['PHONE_BPM_CONTACT'];//
        $emailcontactrecord = $record['EMAIL_BPM_CONTACT'];//
        $guidcompanyrecord = $record['GUID_BPM_COMPANY'];
        $inncompanyrecord = $record['INN_BPM_COMPANY'];//
        $idbitrixcompanyrecord = $record['ID_BITRIX_COMPANY'];//
        $new_contact_id = "";
        $logger->log($record['PHONE_BPM_CONTACT']);

        $ct = new CCrmContact(false);
        $arParams['HAS_EMAIL'] = 'N';
        $arParams['HAS_PHONE'] = 'N';
        $doptel = "";
        $dopemail = "";

        if (!empty($phonerecord)) {
            $phonerecord = str_replace(" ", "", $phonerecord);
            $pieces = multiexplode(array(",", ";", "/", "."), $phonerecord);
            $result = count($pieces);
            if ($result == 1) {
                $arParams['FM']['PHONE'] = array(
                    'n0' => array(
                        'VALUE_TYPE' => 'MOBILE',
                        'VALUE' => $phonerecord,
                    )
                );
                $arParams['HAS_PHONE'] = 'Y';
            } else {
                $doptel = $pieces[1];
                $arParams['FM']['PHONE'] = array(
                    'n0' => array(
                        'VALUE_TYPE' => 'MOBILE',
                        'VALUE' => $pieces[0],
                    )
                );
                $arParams['HAS_PHONE'] = 'Y';
                $logger->log("ОШИБКА TEL = ", $phonerecord);
            }
        }
        if (!empty($emailcontactrecord)) {
			$emailcontactrecord = str_replace(" ", "", $emailcontactrecord);
            $pieces = multiexplode(array(",", ";", "/"), $emailcontactrecord);
            $result = count($pieces);
            if ($result == 1) {
                $arParams['FM']['EMAIL'] = array(
                    'n0' => array(
                        'VALUE' => $emailcontactrecord,
                    )
                );
                $arParams['HAS_EMAIL'] = 'Y';
            } else {
                $dopemail = $pieces[1];
                $arParams['FM']['EMAIL'] = array(
                    'n0' => array(
                        'VALUE' => $pieces[0],
                    )
                );
                $arParams['HAS_EMAIL'] = 'Y';
                $logger->log("ОШИБКА EMAIL = ", $emailcontactrecord);
            }
        }

        $arParams['FULL_NAME'] = $namerecord;
        $arParams['LAST_NAME'] = $namerecord;
        //$arParams['HAS_EMAIL']='N';
        $arParams['TYPE_ID'] = 'CLIENTBPM';
        $arParams['OPENED'] = 'Y';
        if (!empty($idbitrixcompanyrecord)) {
            $arParams['COMPANY_ID'] = $idbitrixcompanyrecord;
        }
        $logger->log("arParams");
        $logger->log(array($arParams));
        $new_contact_id = $ct->Add($arParams, true, array('DISABLE_USER_FIELD_CHECK' => true));


        if ($new_contact_id) {

            if ($doptel != "") {
                $arParams = array();
                $arParams['FM']['PHONE'] = array(
                    'n0' => array(
                        'VALUE_TYPE' => 'MOBILE',
                        'VALUE' => $doptel,
                    )
                );
                $ct->Update($new_contact_id, $arParams);
            }
            if ($dopemail != "") {
                $arParams = array();
                $arParams['FM']['PHONE'] = array(
                    'n0' => array(
                        'VALUE_TYPE' => 'MOBILE',
                        'VALUE' => $dopemail,
                    )
                );
                $ct->Update($new_contact_id, $arParams);
            }

            $entity_id = "CRM_CONTACT";
            if (!empty($guidcontactrecord)) {
                $uf_guid = "UF_BPMCONTACTID";
                SetUserField($entity_id, $new_contact_id, $uf_guid, $guidcontactrecord);
                $logger->log(array("contactId  " . $guidcontactrecord));
            }

            if (!empty($inncompanyrecord)) {
                $uf_guid = "UF_BPMINN";
                SetUserField($entity_id, $new_contact_id, $uf_guid, $inncompanyrecord);
                $logger->log(array("inn  " . $inncompanyrecord));
            }


            if (!empty($guidcompanyrecord)) {
                $uf_guid = "UF_BPMACCOUNTID";
                SetUserField($entity_id, $new_contact_id, $uf_guid, $guidcompanyrecord);
                $logger->log(array("accountId  " . $guidcompanyrecord));
            }
            $sql = 'UPDATE synchbpmbitrix SET ID_BITRIX_CONTACT = \'' . $new_contact_id . '\' WHERE GUIDBPM_CONTACT=\'' . $record['GUIDBPM_CONTACT'] . '\' ';
            $logger->log($sql);
            $connection->queryExecute($sql);

            $sql = 'UPDATE synchbpmbitrix SET STATYS_ANSWER_BPM_CONTACT = \'' . "TOINSERTDONE" . '\' WHERE GUIDBPM_CONTACT=\'' . $record['GUIDBPM_CONTACT'] . '\' ';
            $logger->log($sql);
            $connection->queryExecute($sql);

        } else {
            $logger->log($ct->LAST_ERROR);
        }
        $logger->log("end----------------------------------------------------------------------------");
    }
    return "CronAgentZadacha13();";
}


function CronAgentZadacha14()
{ CModule::IncludeModule('crm');
//Задача 14.  Поиск контакта без GUID BPM и сохранение их в служебную базу synchbpmbitrixcompanybpm с меткой INSERTTOBPM
    $logger = Logger::getLogger('forsynchronization_14zadacha', 'ofd.bitrix24/forsynchronization_14zadacha.txt');
    $logger->log("Запуск==================================");

    $res = CCrmContact::GetList(array(), array('=UF_BPMCONTACTID' => "","CHECK_PERMISSIONS" => "N"));
    while ($arRes = $res->Fetch()) {
        $guidperson = RestBpmBitrix::generate_guid();
        $logger->log("guidforbpm=" . $guidperson);
        $idbitrixcontact = $arRes['ID'];
        $logger->log("idbitrixcompany=" . $idbitrixcontact);
        $name = $arRes['FULL_NAME'];
        $logger->log("FULL_NAME=" . $name);
        $connection = Bitrix\Main\Application::getConnection();
        $sql = 'INSERT INTO synchbpmbitrix (ID_BITRIX_CONTACT,GUIDBPM_CONTACT,FIO_BPM_CONTACT,STATYS_ANSWER_BPM_CONTACT) VALUES (\'' . $idbitrixcontact . '\',\'' . $guidperson . '\',\'' . $name . '\',\'' . "INSERTTOBPMSTEP2" . '\');';
        $logger->log("SQL=" . $sql);
        $connection->queryExecute($sql);
    }
    $logger->log("Конец==================================");
    return "CronAgentZadacha14();";
}


function CronAgentZadacha14_1()
{
    CModule::IncludeModule('crm');
//Задача 14/1.  Получение данных контактов без GUID
    $logger = Logger::getLogger('forsynchronization_14_1zadacha', 'ofd.bitrix24/forsynchronization_14_1zadacha.txt');
    $logger->log("Запуск==================================");
    $connection = \Bitrix\Main\Application::getConnection();
    $sql = 'SELECT ID_BITRIX_CONTACT,GUIDBPM_CONTACT FROM synchbpmbitrix WHERE STATYS_ANSWER_BPM_CONTACT = \'INSERTTOBPMSTEP2\'';
    $logger->log($sql);
    $recordset = $connection->query($sql, 250);

    while ($record = $recordset->fetch()) {
        $logger->log("begin-------------------------------------------------------------------");
        $idbitrixcontact = $record['ID_BITRIX_CONTACT'];
        $guidcompany = "";
        $company_id = "";
        $res = CCrmContact::GetList(array(), array('ID' => $idbitrixcontact,"CHECK_PERMISSIONS" => "N"));
        while ($arRes = $res->Fetch()) {
            $company_id = $arRes['COMPANY_ID'];
        }
        if ($company_id != "") {
            $entity_id = "CRM_COMPANY";
            $uf_guid = "UF_CRM_COMPANY_GUID";
            $guidcompany = GetUserField($entity_id, $company_id, $uf_guid);
        }

        $mobilephone = "";
        $res = CCrmFieldMulti::GetList(array(), array('TYPE_ID' => "PHONE", "ELEMENT_ID" => $idbitrixcontact,"CHECK_PERMISSIONS" => "N"));
        if ($arRes = $res->Fetch()) {
            $mobilephone = $arRes['VALUE'];
        }

        $email = "";
        $res = CCrmFieldMulti::GetList(array(), array('TYPE_ID' => "EMAIL", "ELEMENT_ID" => $idbitrixcontact,"CHECK_PERMISSIONS" => "N"));
        if ($arRes = $res->Fetch()) {
            $email = $arRes['VALUE'];
        }

        $connection = Bitrix\Main\Application::getConnection();
        $sql = 'UPDATE synchbpmbitrix SET GUID_BPM_COMPANY = \'' . $guidcompany . '\' WHERE ID_BITRIX_CONTACT=\'' . $idbitrixcontact . '\' ';
        $connection->queryExecute($sql);

        $connection = Bitrix\Main\Application::getConnection();
        $sql = 'UPDATE synchbpmbitrix SET PHONE_BPM_CONTACT = \'' . $mobilephone . '\' WHERE ID_BITRIX_CONTACT=\'' . $idbitrixcontact . '\' ';
        $connection->queryExecute($sql);

        $connection = Bitrix\Main\Application::getConnection();
        $sql = 'UPDATE synchbpmbitrix SET EMAIL_BPM_CONTACT = \'' . $email . '\' WHERE ID_BITRIX_CONTACT=\'' . $idbitrixcontact . '\' ';
        $connection->queryExecute($sql);

        $connection = Bitrix\Main\Application::getConnection();
        $sql = 'UPDATE synchbpmbitrix SET STATYS_ANSWER_BPM_CONTACT = \'' . "TOINSERTBPM" . '\' WHERE ID_BITRIX_CONTACT=\'' . $idbitrixcontact . '\' ';
        $connection->queryExecute($sql);
    }
    $logger->log("Конец==================================");
    return "CronAgentZadacha14_1();";
}

function CronAgentZadacha15()
{ CModule::IncludeModule('crm');
//Задача 15.   Запускаем добавление контактов в сторону BPM
    $logger = Logger::getLogger('forsynchronization_15zadacha', 'ofd.bitrix24/forsynchronization_15zadacha.txt');
    $logger->log("Запуск==================================");
    QueryBpm::login();
    $connection = \Bitrix\Main\Application::getConnection();
    $sql = 'SELECT ID_BITRIX_CONTACT,GUIDBPM_CONTACT,GUID_BPM_COMPANY,FIO_BPM_CONTACT,EMAIL_BPM_CONTACT,PHONE_BPM_CONTACT FROM synchbpmbitrix WHERE STATYS_ANSWER_BPM_CONTACT = \'TOINSERTBPM__\'';
    $logger->log($sql);
    $recordset = $connection->query($sql, 250);

    while ($record = $recordset->fetch()) {


        $id = $record['ID_BITRIX_CONTACT'];
        $guidperson = $record['GUIDBPM_CONTACT'];
        $guidcompany = $record['GUID_BPM_COMPANY'];
        $name = $record['FIO_BPM_CONTACT'];
        $email = $record['EMAIL_BPM_CONTACT'];
        $mobilephone = $record['PHONE_BPM_CONTACT'];

        $entity_id = "CRM_CONTACT";
        $uf_guid = "UF_BPMCONTACTID";
        SetUserField($entity_id, $id, $uf_guid, $guidperson);

        $statusanswer = ContactBpmBitrix::contactInsertToBMP($id, $guidperson, $guidcompany, $name, $mobilephone, $email);
 $logger->log($statusanswer);
 $logger->log($guidperson);
        if ($statusanswer == "200") {
            $connection = Bitrix\Main\Application::getConnection();
            $sql = 'UPDATE synchbpmbitrix SET STATYS_ANSWER_BPM_CONTACT = \'' . "INSERTBPMDONE" . '\' WHERE ID_BITRIX_CONTACT=\'' . $id . '\' ';
            $connection->queryExecute($sql);
        }
if ($statusanswer == "500") {
            $connection = Bitrix\Main\Application::getConnection();
            $sql = 'UPDATE synchbpmbitrix SET STATYS_ANSWER_BPM_CONTACT = \'' . "TOINSERTBPM__1" . '\' WHERE ID_BITRIX_CONTACT=\'' . $id . '\' ';
            $connection->queryExecute($sql);
        }


    }
    $logger->log("Конец==================================");
    return "CronAgentZadacha15();";
}*/