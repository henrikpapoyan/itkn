<?
use Bitrix\Main\Loader;
use Bitrix\Main\Type\DateTime;
Loader::includeModule("iblock");

class TiketBitrix
{

    public static $arrProp = Array(
        "VALUE" => ""
    );
    public static function idPropertyGuid($iblock, $code, $guid){
        $db_enum_list = CIBlockProperty::GetPropertyEnum($code, Array(), Array("IBLOCK_ID"=>$iblock, "EXTERNAL_ID"=>$guid));
        if($ar_enum_list = $db_enum_list->GetNext())
        {
            $result = $ar_enum_list["ID"];
        }
        return ($result);
    }

    public static function add($guidTiket)
    {
        $arUserSatisfaction = array(
            BPM_MANAGER_VLASENKO => 365,
            BPM_MANAGER_BUNTOVA => 396,
            BPM_MANAGER_GRIBANOVA => 410,
            BPM_MANAGER_KOPEYKINA => 398,
            BPM_MANAGER_LUTAY => 285,
            BPM_MANAGER_SERGEENKO => 399,
            BPM_MANAGER_GDANOVA => 346,
            BPM_MANAGER_ZAMARAEVA => 302,
            BPM_MANAGER_KOLOMYCEV => 426,
            BPM_MANAGER_SHEMYKIN => 275,
            BPM_MANAGER_GAVRILOV => 402,
            BPM_MANAGER_GUREV => 300,
            BPM_MANAGER_DEREVYNSKIY => 357,
            BPM_MANAGER_KRYLOV => 403,
            BPM_MANAGER_MAKSIMOV => 347,
            BPM_MANAGER_MALCEV => 342,
            BPM_MANAGER_MASLENNIKOVA => 259,
            BPM_MANAGER_POPOV => 400,
            BPM_MANAGER_FEOKTISOVA => 161,
            BPM_MANAGER_SMOLYKOV => 301,
            BPM_MANAGER_STEBLIN => 311,
            BPM_MANAGER_TABAKOV => 401,
            BPM_MANAGER_COKOLENKO => 299,
            BPM_MANAGER_YKOVLEVA => 406,
        );

        $Iblock = $guidTiket["iblocId"];
        $guidStatus = strtoupper($guidTiket["Status"]);
	    $guidGrup = strtoupper($guidTiket["Group"]);
        $datTime = strtotime($guidTiket["RegisteredOn"]);
        $prop = array();

        $prop['Origin'] = $guidTiket["Origin"];
        $prop["idOldTiket"] = $guidTiket["idOldTiket"];

        //В BPM поставили ответственного Иван Пупкин, если в Битриксе его нет - то ставим пустого ответственного в Битриксе -  если это обращение, для лида Бунтова
        if(!empty($guidTiket["Owner"])){
            if(isset($arUserSatisfaction[$guidTiket["Owner"]])) {
                $prop['Owner_display'] = $arUserSatisfaction[$guidTiket["Owner"]];
            } else {
                $prop['Owner_display'] = '';
            }
            $prop['Owner'] = $guidTiket["Owner"];
        }

        $prop['DATA_I_VREMYA'] = date("d.m.Y H:i:s", $datTime);
        $prop['ID_ZADACHI_PO_OBRASHCHENIYU'] = $guidTiket["Id"];
        $prop['NUM_OBRASHCHENIYA_V_SERVICEDESK'] = $guidTiket["Number"];
        $prop["ISTORIYA_RABOTY_S_OBRASHCHENIEM"] = array(date("m.d.y H:i:s").": ".$guidTiket["Symptoms"]);

        $prop['Priority'] = $guidTiket["Priority"];
        $prop['Priority_display'] = $guidTiket["Priority_display"];
        $prop['ConfItem'] = $guidTiket["ConfItem"];
        $prop['UsrConfLocation'] = $guidTiket["UsrConfLocation"];
        $prop['Contact'] = $guidTiket["Contact"];
        $prop['Contact_display'] = $guidTiket["Contact_display"];
        $prop['Account'] = $guidTiket["Account"];
        $prop['Account_display'] = $guidTiket["Account_display"];
        $prop['UsrCaseType'] = $guidTiket["UsrCaseType"];
        $prop['UsrCaseType_display'] = $guidTiket["UsrCaseType_display"];

        $prop['UsrPhoneNumber'] = $guidTiket["UsrPhoneNumber"];
        $prop['UsrCaseType_display'] = $guidTiket["UsrCaseType_display"];
        $prop['Category'] = $guidTiket["Category"];
        $prop['Category_display'] = $guidTiket["Category_display"];
        $prop['UsrINN'] = $guidTiket["UsrINN"];


        $prop['REASON_CLOSURE'] =  array("EXTERNAL_ID" => self::idPropertyGuid($Iblock, "REASON_CLOSURE", $guidTiket["ClosureCode"]));
        $prop['GRUPPY_OTVETSTVENNYKH'] =  array("EXTERNAL_ID" => self::idPropertyGuid($Iblock, "GRUPPY_OTVETSTVENNYKH", $guidGrup));
        $prop['STATUS_OBRASHCHENIYA'] =  array("EXTERNAL_ID" => self::idPropertyGuid($Iblock, "STATUS_OBRASHCHENIYA", $guidStatus));
        $prop['KATEGORIYA'] =  array("EXTERNAL_ID" => self::idPropertyGuid($Iblock, "KATEGORIYA", $guidTiket["KATEGORIYA"]));
        $prop['TEKST_SOOBSHCHENIYA'] = $guidTiket["Solution"];

        $arFields = Array(
            "DATE_CREATE" => date("d.m.Y H:i:s"),
            "IBLOCK_ID" => $Iblock,
            "PROPERTY_VALUES" => $prop,
            "NAME" => $guidTiket["Subject"],
            "ACTIVE" => "Y"
        );
        $logger = Logger::getLogger('creatTiket','ofd.bitrix24/creatTiket.log');
        $logger->log('$guidTiket[] = '.print_r($guidTiket, true));
        $logger->log('$arFields[] = '.print_r($arFields, true));
        $el = new CIBlockElement;
        if($ELEMENT_ID = $el->Add($arFields))
            $result = "Y";
        else
            $result = $el->LAST_ERROR;
        return $result;
    }

    public static function histories ($BL_ID, $ELEMENT_ID, $newText){
        $objDateTime = new DateTime();
        if(CModule::IncludeModule("iblock"))
        {
            $VALUES[] = array(
                "VALUE" => array(
                    "TEXT"=>$objDateTime->toString().": ".$newText,
                    //"TEXT"=>date("m.d.y H:i:s").": ".$newText,
                    "TYPE"=>"TEXT"
                ));
            $res = CIBlockElement::GetProperty($BL_ID, $ELEMENT_ID, Array("sort"=>"asc"), array("CODE" => "ISTORIYA_RABOTY_S_OBRASHCHENIEM"));
            while ($ob = $res->GetNext())
            {
                $text = $ob['VALUE'];
                $VALUES[] = array(
                    "VALUE" => $text);
            }

            $result = CIBlockElement::SetPropertyValueCode($ELEMENT_ID, "ISTORIYA_RABOTY_S_OBRASHCHENIEM", $VALUES);
            return ($result);
        }
    }

    public static function update($guidTiket){
        $arUserSatisfaction = array(
            BPM_MANAGER_VLASENKO => 365,
            BPM_MANAGER_BUNTOVA => 396,
            BPM_MANAGER_GRIBANOVA => 410,
            BPM_MANAGER_KOPEYKINA => 398,
            BPM_MANAGER_LUTAY => 285,
            BPM_MANAGER_SERGEENKO => 399,
            BPM_MANAGER_GDANOVA => 346,
            BPM_MANAGER_ZAMARAEVA => 302,
            BPM_MANAGER_KOLOMYCEV => 426,
            BPM_MANAGER_SHEMYKIN => 275,
            BPM_MANAGER_GAVRILOV => 402,
            BPM_MANAGER_GUREV => 300,
            BPM_MANAGER_DEREVYNSKIY => 357,
            BPM_MANAGER_KRYLOV => 403,
            BPM_MANAGER_MAKSIMOV => 347,
            BPM_MANAGER_MALCEV => 342,
            BPM_MANAGER_MASLENNIKOVA => 259,
            BPM_MANAGER_POPOV => 400,
            BPM_MANAGER_FEOKTISOVA => 161,
            BPM_MANAGER_SMOLYKOV => 301,
            BPM_MANAGER_STEBLIN => 311,
            BPM_MANAGER_TABAKOV => 401,
            BPM_MANAGER_COKOLENKO => 299,
            BPM_MANAGER_YKOVLEVA => 406,
        );

        $logger = Logger::getLogger('TiketBitrix_update','ofd.bitrix24/TiketBitrix_update.log');
        $logger->log($guidTiket);
        $el = new CIBlockElement;

        /** Get all Properties by ElementID*/
        $PRODUCT_ID = $guidTiket["idElem"];
        $Iblock     = $guidTiket["iblocId"];

        //Get current value Properties
        $arSelect = array(
            "NAME",
            "PROPERTY_KOMPANIYA_KLIENTA",
            "PROPERTY_KONTAKT_KLIENTA",
            "PROPERTY_LID_KLIENTA",
            "PROPERTY_E_MAIL",
            "PROPERTY_UsrPhoneNumber",
            "PROPERTY_Owner_display",
            "PROPERTY_owner",
            "PROPERTY_SKRYTOE_OBRASHCHENIE"
        );
        $res = $el->GetList(array(), array("IBLOCK_ID" => $Iblock, "ID" => $PRODUCT_ID), false, false, $arSelect);
        $arElement = $res->Fetch();
        $logger->log("arElement[] = ".print_r($arElement, true));

        $guidStatus = strtoupper($guidTiket["Status"]);
        $guidOwner = strtoupper($guidTiket["Group"]);
        $datTime = strtotime($guidTiket["RegisteredOn"]);
        $prop = array();
        $Origin = strtoupper($guidTiket["Origin"]);
        $prop['Origin'] = $Origin;
        if($guidTiket["reopen"] == null){
            $prop["idOldTiket"] = "";
        }else{
            $prop["idOldTiket"] = "reopen";
        }

        if(empty($guidTiket["Owner"])){
            $prop['Owner_display'] = '';
            $prop['Owner'] = '';
        } else {
            if (isset($arUserSatisfaction[$guidTiket["Owner"]]))
            {
                $prop['Owner_display'] = $arUserSatisfaction[$guidTiket["Owner"]];
            } else
            {
                $prop['Owner_display'] = '';
            }
            $prop['Owner'] = $guidTiket["Owner"];
        }

        $prop['DATA_I_VREMYA'] = date("d.m.Y H:i:s", $datTime);
        $prop['ID_ZADACHI_PO_OBRASHCHENIYU'] = $guidTiket["Id"];
        $prop['NUM_OBRASHCHENIYA_V_SERVICEDESK'] = $guidTiket["Number"];

        $prop['Priority'] = $guidTiket["Priority"];
        $prop['Priority_display'] = $guidTiket["Priority_display"];
        $prop['ConfItem'] = $guidTiket["ConfItem"];
        $prop['UsrConfLocation'] = $guidTiket["UsrConfLocation"];
        $prop['Contact'] = $guidTiket["Contact"];
        $prop['Contact_display'] = $guidTiket["Contact_display"];
        $prop['Account'] = $guidTiket["Account"];
        $prop['Account_display'] = $guidTiket["Account_display"];
        $prop['UsrCaseType'] = $guidTiket["UsrCaseType"];
        $prop['UsrCaseType_display'] = $guidTiket["UsrCaseType_display"];

        $prop['UsrPhoneNumber'] = $guidTiket["UsrPhoneNumber"];
        $prop['Category'] = $guidTiket["Category"];
        $prop['Category_display'] = $guidTiket["Category_display"];
        $prop['UsrINN'] = $guidTiket["UsrINN"];

        $prop['REASON_CLOSURE'] =  array("EXTERNAL_ID" => self::idPropertyGuid($Iblock, "REASON_CLOSURE", $guidTiket["ClosureCode"]));
        $prop['GRUPPY_OTVETSTVENNYKH'] =  array("EXTERNAL_ID" => self::idPropertyGuid($Iblock, "GRUPPY_OTVETSTVENNYKH", $guidOwner));
        $prop['STATUS_OBRASHCHENIYA'] =  array("EXTERNAL_ID" => self::idPropertyGuid($Iblock, "STATUS_OBRASHCHENIYA", $guidStatus));
        $prop['KATEGORIYA'] =  array("EXTERNAL_ID" => self::idPropertyGuid($Iblock, "KATEGORIYA", $guidTiket["KATEGORIYA"]));
        $prop['TEKST_SOOBSHCHENIYA'] = $guidTiket["Solution"];

        $prop["KOMPANIYA_KLIENTA"] = (!empty($arElement['PROPERTY_KOMPANIYA_KLIENTA_VALUE'])) ? $arElement['PROPERTY_KOMPANIYA_KLIENTA_VALUE'] : "";
        $prop["KONTAKT_KLIENTA"] = (!empty($arElement['PROPERTY_KONTAKT_KLIENTA_VALUE'])) ? $arElement['PROPERTY_KONTAKT_KLIENTA_VALUE'] : "";
        $prop["LID_KLIENTA"] = (!empty($arElement['PROPERTY_LID_KLIENTA_VALUE'])) ? $arElement['PROPERTY_LID_KLIENTA_VALUE'] : "";
        $prop["E_MAIL"] = (!empty($arElement['PROPERTY_E_MAIL_VALUE'])) ? $arElement['PROPERTY_E_MAIL_VALUE'] : "";
        $prop["SKRYTOE_OBRASHCHENIE"] = (!empty($arElement['PROPERTY_SKRYTOE_OBRASHCHENIE_ENUM_ID'])) ? $arElement['PROPERTY_SKRYTOE_OBRASHCHENIE_ENUM_ID'] : "";
        if(empty($prop['UsrPhoneNumber'])) {
            $prop["UsrPhoneNumber"] = (!empty($arElement['PROPERTY_USRPHONENUMBER_VALUE'])) ? $arElement['PROPERTY_USRPHONENUMBER_VALUE'] : "";
        }

        $newText = trim($guidTiket["Symptoms"]);

        $arFields = Array(
            "DATE_CREATE" => date("d.m.Y H:i:s"),
            "IBLOCK_ID" => $Iblock,
            "PROPERTY_VALUES" => $prop,
            "NAME" => $guidTiket["Subject"],
            "ACTIVE" => "Y",
            "UPDATE_FROM_BPM" => "Y"
        );

        $logger->log('$arFields[] = '.print_r($arFields, true));

        if($ELEMENT_ID = $el->Update($PRODUCT_ID, $arFields)){
            $result = "U";
        } else{
            $result = $el->LAST_ERROR;
        }
        self::histories($Iblock, $PRODUCT_ID, $newText);
        return $result;
    }
}