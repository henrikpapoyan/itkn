<?php
use Bitrix\Main\Loader;
Loader::includeModule("crm");

class LeadBpmBitrix
{
    public static function leadAdd($arParams = array())
    {
        $oContact           = new CCrmContact;
        $oLead              = new CCrmLead;
        $arFieldsLead       = [];
        $arResult           = [];
        $arResult['METHOD'] = "insert"; //default


        /** Check data for validity */

        switch (strlen($arParams['inn'])) {
            case 0:
                break;
            case 10: //Организация
                if(empty($arParams['kpp'])) {
                    $arResult['STATUS'] = 101;
                    $arResult['MESSAGE'] = "The organization (INN: " . $arParams['inn'] . ") not have KPP.";
                    return $arResult;
                }
                break;
            case 12: //ИП
                break;
            default:
                $arResult['STATUS'] = 101;
                $arResult['MESSAGE'] = "Not valid INN: " . $arParams['inn'];
                return $arResult;
                break;
        }

        //проверка на наличие контрагента в битрикс24
        if(!empty($arParams['inn']) and strlen($arParams['inn']) == 12){
            $contrArray = RestBpmBitrix::verify($arParams['inn']);
        }elseif (!empty($arParams['inn']) and !empty($arParams['kpp'])){
            $contrArray = RestBpmBitrix::verify($arParams['inn'], null,null,$arParams['kpp']);
        }
        //проверка на наличие данных контакта или контрагента
        if(empty($arParams['contactId']) and empty($contrArray["guid"])){
            $arResult['STATUS'] = 101;
            $arResult['MESSAGE'] = "Missing parameter [contactId]";
            return $arResult;
        }


        /** Search contact by contactId BPM */

        $arFilter = array(
            "UF_BPMCONTACTID" => $arParams['contactId'],
        );

        $resContact = $oContact->GetList(array(), $arFilter);
        if($arContact = $resContact->Fetch()){
            $arFieldsLead['NAME']           = $arContact["NAME"];
            $arFieldsLead['SECOND_NAME']    = $arContact["SECOND_NAME"];
            $arFieldsLead['LAST_NAME']  = $arContact["LAST_NAME"];
            $arFieldsLead['EMAIL'] = "";
            $arFieldsLead['PHONE'] = "";

            //Get Email/Phone contact
            $resMultiContact = CCrmFieldMulti::GetList(array(), array('ENTITY_ID' => 'CONTACT', 'ELEMENT_ID' => $arContact['ID']));
            while($arMultiContact = $resMultiContact->Fetch()){
                if($arMultiContact['TYPE_ID'] == 'EMAIL'){
                    $arFieldsLead['EMAIL'] = $arMultiContact['VALUE'];
                }
                if($arMultiContact['TYPE_ID'] == 'PHONE'){
                    $arFieldsLead['PHONE'] = $arMultiContact['VALUE'];
                }
            }
        } elseif(empty($contrArray["guid"])) {
            $arResult['STATUS'] = 101;
            $arResult['MESSAGE'] = "Contact by contactIdBpm not find";
            return $arResult;
        }

        // if not email and phone, then Lead not created.
        if( empty($arFieldsLead['EMAIL']) && empty($arFieldsLead['PHONE']) ) {
            $arResult['STATUS'] = 101;
            $arResult['MESSAGE'] = "Missing contact details: email, phone";
            return $arResult;
        }

        /**  Selected TYPE Lead and Group */
        $group = null;
        if($arParams['leadType'] == "Запрос на подключение - Клиент") {
            $ufType     = BITRIX_LEAD_UF_TYPE_CLIENT;
            $group      = IBLOK_OTDEL_SALE;
            $ufChannel  = BITRIX_LEAD_UF_CHANNEL_CONTACT_CENTER;
        } elseif ($arParams['leadType'] == "Запрос на сотрудничество - Агент") {
            $ufType     = BITRIX_LEAD_UF_TYPE_PARTNER;
            $group      = IBLOK_OTDEL_SALE_PARTNER;
            $ufChannel  = BITRIX_LEAD_UF_CHANNEL_CONTACT_CENTER;
        } elseif ($arParams['leadType'] == "Маркетинг") {
            $ufType     = BITRIX_LEAD_UF_TYPE_NOT_SELECTED;
            $group      = IBLOK_OTDEL_MARKET;
            $ufChannel  = BITRIX_LEAD_UF_CHANNEL_MARKETING;
            $sourceID   = BITRIX_LEAD_SOURCE_ID_NOT_SELECTED;
        } else {
            $ufType     = BITRIX_LEAD_UF_TYPE_NOT_SELECTED;
            $group      = IBLOK_TWO_LINE;
            $ufChannel  = BITRIX_LEAD_UF_CHANNEL_CONTACT_CENTER;
        }

        //Check leadType for valid
        if(empty($ufType)) {
            $arResult['STATUS'] = 101;
            $arResult['MESSAGE'] = "leadType not valid";
            return $arResult;
        }

        //Add Tiket
        if(!empty($arParams["caseId"]) and !empty($contrArray["guid"])) {
            $idTiket = strtoupper($arParams["caseId"]);
            TiketBpmBitrix::getTiket($idTiket, $group);
            $arResult['STATUS'] = 100;
            $arResult['MESSAGE'] = "Insert successful";
            return $arResult;
        }


        /** Search existing Lead by INN, KPP and FIO */

        $resLead = $oLead->GetList(array('ID' => 'asc'), array(
            'UF_CRM_1499414186' => $arParams['inn'],
            'UF_KPP'            => $arParams['kpp'],
            'NAME'              => $arFieldsLead['NAME'],
            'SECOND_NAME'       => $arFieldsLead['SECOND_NAME'],
            'LAST_NAME'         => $arFieldsLead['LAST_NAME'],
        ));
        while ($arLead = $resLead->Fetch()) {

            //Если телефон и Email совпадают. Значит дубликат
            $leadEmail = "";
            $leadPhone = "";

            $resMultiLead = CCrmFieldMulti::GetList(array(), array('ENTITY_ID' => 'LEAD', 'ELEMENT_ID' => $arLead['ID']));
            while ($arMultiLead = $resMultiLead->Fetch()) {
                if($arMultiLead['TYPE_ID'] == 'EMAIL'){
                    $leadEmail = $arMultiLead['VALUE'];
                }
                if($arMultiLead['TYPE_ID'] == 'PHONE'){
                    $leadPhone = $arMultiLead['VALUE'];
                }
            }

            //Isset dublicate Lead
            if( $arFieldsLead['EMAIL'] == $leadEmail && $arFieldsLead['PHONE'] == $leadPhone ) {
                $arResult['STATUS'] = 100;
                $arResult['MESSAGE'] = "Lead already exists in Bitrix, ID Lead: " . $arLead['ID'];

                //Add Tiket
                if(!empty($arParams["caseId"])) {
                    $idTiket = strtoupper($arParams["caseId"]);
                    TiketBpmBitrix::getTiket($idTiket, $group);
                }

                return $arResult;
            }
        }


        /** Add LEAD */

        //State lead: activity
        $idElementList = getIdElementListState ('XML_STATE_LEAD_ACTIVITY');

        //Selected SOURCE Lead
        if($arParams['leadSource'] == "Email" && empty($sourceID)){
            $sourceID = BITRIX_LEAD_SOURCE_ID_EMAIL;
        }
        if($arParams['leadSource'] == "Звонок" && empty($sourceID)){
            $sourceID = BITRIX_LEAD_SOURCE_ID_CALL;
        }

        if(empty($sourceID)) {
            $arResult['STATUS'] = 101;
            $arResult['MESSAGE'] = "leadSource not valid";
            return $arResult;
        }

        //Add Lead
        $arFields = Array(
            "TITLE" 			=> "Обращение в КЦ",
            "NAME" 				=> $arFieldsLead['NAME'],
            "LAST_NAME" 		=> $arFieldsLead['LAST_NAME'],
            "SECOND_NAME" 		=> $arFieldsLead['SECOND_NAME'],
            "UF_TYPE" 			=> $ufType,
            "UF_CHANNEL"        => $ufChannel,
            "SOURCE_ID" 		=> $sourceID,
            "STATUS_ID" 		=> BITRIX_LEAD_STATUS_ID_NEW,
            "ASSIGNED_BY_ID" 	=> BITRIX_USER_ID_DEFAULT,
            "UF_CRM_1499414186" => $arParams['inn'],
            "UF_KPP"            => $arParams['kpp'],
            "UF_STATE_LEAD"     => $idElementList,
            "FM" => Array(
                "EMAIL" => Array(
                    "n0" => Array(
                        "VALUE" 		=> (empty($arFieldsLead['EMAIL']))? "" : $arFieldsLead['EMAIL'],
                        "VALUE_TYPE" 	=> "WORK",
                    ),
                ),
                "PHONE" => Array(
                    "n0" => Array(
                        "VALUE" 		=> (empty($arFieldsLead['PHONE']))? "" : $arFieldsLead['PHONE'],
                        "VALUE_TYPE" 	=> "WORK",
                    ),
                ),
            ),
        );

        if($LEAD_ID = $oLead->Add($arFields)){
            $arResult['STATUS'] = 100;
            $arResult['MESSAGE'] = "Insert successful";

            //Add Tiket
            if(!empty($arParams["caseId"])) {
                $idTiket = strtoupper($arParams["caseId"]);
                TiketBpmBitrix::getTiket($idTiket, $group);
            }
        } else {
            $arResult['STATUS'] = 104;
            $arResult['MESSAGE'] = "Error add contact: $oLead->LAST_ERROR";
        }

        return $arResult;
    }
}