<?

use Bitrix\Main\Loader;

Loader::includeModule("iblock");
Loader::includeModule("crm");

class CIblockEvent
{
    static protected  $ibIDArray = array(77, 78, 89, 90);

    static protected function access($idIblock)
    {
        if (in_array($idIblock, self::$ibIDArray)){
            return true;
        }else{
            return false;
        }
    }

    public function propElementUpdate(&$arFields)
    {
        if (self::access($arFields['IBLOCK_ID']) && isset($arFields["PROPERTY_VALUES"])) {
            $logger = Logger::getLogger('propElementUpdate', 'ofd.bitrix24/propElementUpdate.log');
            $logger->log("propElementUpdate. START");
            $logger->log(array($arFields));
            $elementId = $arFields["ID"];
            // получаем ID свойств элемента
            $arPropertiesId = array_keys($arFields["PROPERTY_VALUES"]);

            $arElSelect = Array("ID", "NAME", "IBLOCK_ID", "ACTIVE", "PROPERTY_*");

            $arElFilter = Array("IBLOCK_ID" => $arFields["IBLOCK_ID"], "ID" => $elementId);
            $dbElem = CIBlockElement::GetList(Array(), $arElFilter, false, false, $arElSelect);
            while ($ob = $dbElem->GetNextElement()) {
                $arFieldsOld = $ob->GetFields();
                $arProps = $ob->GetProperties();
                $clonTiket["Subject"] = $arFieldsOld["NAME"];
                $clonTiket["UsrINN"] = $arProps["UsrINN"]["VALUE"];
                $clonTiket["KOMPANIYA_KLIENTA"] = $arProps["KOMPANIYA_KLIENTA"]["ID"];
                $clonTiket["LID_KLIENTA"] = $arProps["LID_KLIENTA"]["ID"];
                $clonTiket["E_MAIL"] = trim($arProps["E_MAIL"]["VALUE"]);
                $clonTiket["UsrPhoneNumber"] = trim($arProps["UsrPhoneNumber"]["VALUE"]);
                $clonTiket["Contact_display"] = trim($arProps["Contact_display"]["VALUE"]);
                $clonTiket["Contact"] = trim($arProps["Contact"]["VALUE"]);
                $clonTiket["Priority"] = trim($arProps["Priority"]["VALUE"]);
                $clonTiket["Priority_display"] = trim($arProps["Priority_display"]["VALUE"]);
                $clonTiket["Category"] = trim($arProps["Category"]["VALUE"]);
                $clonTiket["Category_display"] = trim($arProps["Category_display"]["VALUE"]);
                $clonTiket["UsrCaseType"] = trim($arProps["UsrCaseType"]["VALUE"]);
                $clonTiket["UsrCaseType_display"] = trim($arProps["UsrCaseType_display"]["VALUE"]);
                $clonTiket["Contact"] = trim($arProps["Category"]["VALUE"]);
                $clonTiket["Account"] = trim($arProps["Account"]["VALUE"]);
                $clonTiket["Account_display"] = trim($arProps["Account_display"]["VALUE"]);

                $clonTiket["ConfItem"] = trim($arProps["ConfItem"]["VALUE"]);
                $clonTiket["UsrConfLocation"] = trim($arProps["UsrConfLocation"]["VALUE"]);
                $clonTiket["RegisteredOn"] = trim($arProps["DATA_I_VREMYA"]["VALUE"]);
                $clonTiket["Number"] = trim($arProps["NUM_OBRASHCHENIYA_V_SERVICEDESK"]["VALUE"]);
                $clonTiket["Id"] = trim($arProps["ID_ZADACHI_PO_OBRASHCHENIYU"]["VALUE"]);
                $clonTiket["Origin"] = trim($arProps["Origin"]["VALUE"]);
                $clonTiket["idOldTiket"] = trim($arFieldsOld["ID"]);
                $clonTiket["SKRYTOE_OBRASHCHENIE"] = $arProps["SKRYTOE_OBRASHCHENIE"]["VALUE_ENUM_ID"];
            }


            $dbProperties = CIBlockProperty::GetList(
                Array("sort" => "asc", "name" => "asc"),
                Array("IBLOCK_ID" => $arFields["IBLOCK_ID"], "=ID" => $arPropertiesId)
            );
            while ($arProperties = $dbProperties->GetNext()) {
                // проверяем свойство на соответствие CODE
                if ($arProperties["CODE"] == "ISTORIYA_RABOTY_S_OBRASHCHENIEM") {
                    $propValue = $arFields["PROPERTY_VALUES"][$arProperties["ID"]];
                    foreach ($propValue as $propValueItem) {
                        $istoriyaObrasc[] = $propValueItem["VALUE"]["TEXT"];
                    }
                    $istoriyaObrasc = implode(",", $istoriyaObrasc);
                    $clonTiket["Symptoms"] = $istoriyaObrasc;
                }
                if ($arProperties["CODE"] == "KATEGORIYA") {
                    $arPropValue = $arFields["PROPERTY_VALUES"][$arProperties["ID"]];
                    $firstElem = array_shift($arPropValue);
                    $propValue = $firstElem["VALUE"];
                    $arEnumList = CIBlockPropertyEnum::GetByID($propValue);
                    $categoryObraschXmlId = $arEnumList["XML_ID"];
                    $clonTiket["KATEGORIYA"] = $categoryObraschXmlId;
                }
                if ($arProperties["CODE"] == "STATUS_OBRASHCHENIYA") {
                    $arPropValue = $arFields["PROPERTY_VALUES"][$arProperties["ID"]];
                    $firstElem = array_shift($arPropValue);
                    $propValue = $firstElem["VALUE"];
                    $arEnumListSt = CIBlockPropertyEnum::GetByID($propValue);
                    $statusObraschXmlId = $arEnumListSt["XML_ID"];
                    $clonTiket["Status"] = $statusObraschXmlId;
                    $idPropSt = $arEnumListSt["PROPERTY_ID"];
                }
                if ($arProperties["CODE"] == "TEKST_SOOBSHCHENIYA") {
                    $propValue = end($arFields["PROPERTY_VALUES"][$arProperties["ID"]]);
                    $textSoobsc = HTMLToTxt($propValue["VALUE"]["TEXT"], "", array(), false);
                }
                if ($arProperties["CODE"] == "REASON_CLOSURE") {
                    $arPropValue = $arFields["PROPERTY_VALUES"][$arProperties["ID"]];
                    $firstElem = array_shift($arPropValue);
                    $propValue = $firstElem["VALUE"];
                    $arEnumList = CIBlockPropertyEnum::GetByID($propValue);
                    $reasonClosureXmlId = $arEnumList["XML_ID"];
                    $clonTiket["reason_closure"] = $reasonClosureXmlId;
                }
                if ($arProperties["CODE"] == "GRUPPY_OTVETSTVENNYKH") {
                    $arPropValue = $arFields["PROPERTY_VALUES"][$arProperties["ID"]];
                    $firstElem = array_shift($arPropValue);
                    $propValue = $firstElem["VALUE"];
                    $arEnumListOtv = CIBlockPropertyEnum::GetByID($propValue);
                    $gruppiOtvetstXmlId = $arEnumListOtv["XML_ID"];
                    $idProp = $arEnumListOtv["PROPERTY_ID"];
                }
            }


            $res = CIBlockElement::GetProperty($arFields["IBLOCK_ID"], $elementId, "sort", "asc", array("CODE" => "GRUPPY_OTVETSTVENNYKH"));
            while ($ob = $res->GetNext()) {
                $gruppiOtvetstXmlIdOld = $ob['VALUE_XML_ID'];
            }

            $resd = CIBlockElement::GetProperty($arFields["IBLOCK_ID"], $elementId, "sort", "asc", array("CODE" => "STATUS_OBRASHCHENIYA"));
            while ($obs = $resd->GetNext()) {
                $statusXmlIdOld = $obs['VALUE_XML_ID'];
            }
            if ($statusXmlIdOld == ID_STATUS_CLOSE and !empty($textSoobsc)) {
                session_name('STATUS_CLOSE');
                session_start();
                $_SESSION["STATUS_CLOSE"][$propValueSt]["propValueSt"] = $propValueSt;
                $_SESSION["STATUS_CLOSE"][$idPropSt]["idPropSt"] = $idPropSt;
                //pre($_SESSION["GROUP_EXPLUATEION"]);
                //exit;

            }
            if ($statusXmlIdOld != ID_STATUS_CLOSE) {
                if ($_SESSION["STATUS_CLOSE"][$propValueSt] or $_SESSION["STATUS_CLOSE"][$idPropSt]) {
                    unset($_SESSION["STATUS_CLOSE"][$propValueSt]);
                    unset($_SESSION["STATUS_CLOSE"][$idPropSt]);
                }
            }
            if ($gruppiOtvetstXmlId == ID_GROUP_EXPLUATEION) {
                session_name('GROUP_EXPLUATEION');
                session_start();
                $_SESSION["GROUP_EXPLUATEION"][$propValueId]["propValueId"] = $propValueId;
                $_SESSION["GROUP_EXPLUATEION"][$idProp]["idProp"] = $idProp;
                //pre($_SESSION["GROUP_EXPLUATEION"]);
                //exit;

            }

            if (!empty($gruppiOtvetstXmlId) and $gruppiOtvetstXmlId != $gruppiOtvetstXmlIdOld and $gruppiOtvetstXmlId != ID_GROUP_EXPLUATEION) {
                $clonTiket["Group"] = $gruppiOtvetstXmlId;
                if ($_SESSION["GROUP_EXPLUATEION"][$idProp] or $_SESSION["GROUP_EXPLUATEION"][$propValueId]) {
                    unset($_SESSION["GROUP_EXPLUATEION"][$propValueId]);
                    unset($_SESSION["GROUP_EXPLUATEION"][$idProp]);
                }

                switch ($gruppiOtvetstXmlId) {
                    case (ID_GROUP_SUPPORT):
                        (int)$clonTiket["iblocId"] = returnIdCodeIblock(IBLOK_TWO_LINE);
                        break;
                    case (ID_GROUP_SALE):
                        (int)$clonTiket["iblocId"] = returnIdCodeIblock(IBLOK_OTDEL_SALE);
                        break;
                    case (ID_GROUP_SALE_PARTNER):
                        (int)$clonTiket["iblocId"] = returnIdCodeIblock(IBLOK_OTDEL_SALE_PARTNER);
                        break;
                    case (ID_GROUP_MARKETING):
                        (int)$clonTiket["iblocId"] = returnIdCodeIblock(IBLOK_OTDEL_MARKET);
                        break;
                    default:
                        (int)$clonTiket["iblocId"] = returnIdCodeIblock(IBLOK_TWO_LINE);
                }

                //пересоздание и удаление тикета
                if ($clonTiket["iblocId"] != $arFields["IBLOCK_ID"]) {
                    $logger = Logger::getLogger('LeadAddCiblockEvent', 'ofd.bitrix24/TiketAddCiblockEvent.log');
                    $logger->log("propElementUpdate. Пересоздание и удаление тикета");
                    $rezClon = TiketBitrix::add($clonTiket);
                    $logger->log("propElementUpdate. СlonTiket:");
                    $logger->log($clonTiket);
                    if ($rezClon == "Y") {
                        CIBlockElement::Delete($elementId);
                        $logger->log("Удаление тикета: $elementId");
                    }
                }

            }
            //pre($arFields["RIGHTS"]);
            //exit();
            if ($statusObraschXmlId == ID_STATUS_RE_OPEN) {
                global $APPLICATION;
                $APPLICATION->ThrowException("Данный статус обращения устанавливается в bpm'online!");
                return false;
            }
            if ($statusObraschXmlId == ID_STATUS_CLOSE and empty($textSoobsc)) {
                global $APPLICATION;
                $APPLICATION->ThrowException("При выборе статуса: задача закрыта, нужно указать решение при закрытии обращения!");
                return false;
            }
            if ($statusObraschXmlId == ID_STATUS_CANCELLED and empty($reasonClosureXmlId)) {
                global $APPLICATION;
                $APPLICATION->ThrowException('При выборе статуса: отменено, нужно указать причину отмены!');
                return false;
            }

        }
    }

    public function iblockElementUpdate(&$arFields)
    {
        $logger = Logger::getLogger('UpdateCIblockEvent', 'ofd.bitrix24/UpdateCIblockEvent.log');
        if (self::access($arFields['IBLOCK_ID'])) {
            $logger->log("iblockElementUpdate. START");
            $logger->log("iblockElementUpdate. arFields[] = ".print_r($arFields, true));
            // получаем ID свойств элемента
            $arPropertiesId = array_keys($arFields["PROPERTY_VALUES"]);

            // получаем CODE инфоблока и сравниваем с кодами инфоблоков обращений
            $dbIBlock = CIBlock::GetByID($arFields["IBLOCK_ID"]);
            if ($arIBlock = $dbIBlock->GetNext()) {
                $iblockCode = $arIBlock["CODE"];
                $logger->log('iblockElementUpdate. $iblockCode = '.$iblockCode);
                if ($iblockCode == IBLOK_TWO_LINE or $iblockCode == IBLOK_OTDEL_MARKET or $iblockCode == IBLOK_OTDEL_SALE or $iblockCode == IBLOK_OTDEL_SALE_PARTNER) {
                    // по ID элемента выбираем все поля элемента и сравниваем с теми, что были отправлены. Если есть изменения, то отправляем запрос в bpm
                    $elementId = $arFields["ID"];
                    $triggerUpdate = false;


                    //Обновление лида связанного с обращением

                    //Проверяем, не вызвано ли обновление обращения из-за обновления лида,
                    // тогда нет необходимости обновлять данные лида
                    if(!isset($arFields["UPD_FROM_LEAD"])) {
                        $logger->log('iblockElementUpdate. $arFields["UPD_FROM_LEAD"] НЕ ПЕРЕДАНО');
                    } else {
                        $logger->log('iblockElementUpdate. $arFields["UPD_FROM_LEAD"] ПЕРЕДАНО и = '.$arFields["UPD_FROM_LEAD"]);
                    }
                    if(!isset($arFields["UPD_FROM_LEAD"])){
                        //Получаем данные обращения
                        $loggerFromLeadUpdate = Logger::getLogger('LeadUpdateByUpdateObrachenie');
                        $el = new CIBlockElement();
                        $rsElemObracheniya = $el->GetList(
                            array("ID" => "ASC"),
                            array("ID" => $elementId),
                            false,
                            false,
                            array(
                                "ID",
                                "IBLOCK_ID",
                            )
                        );
                        if($ob = $rsElemObracheniya->GetNextElement())
                        {
                            $arElemObracheniya = $ob->GetFields();
                            $arElemProps = $ob->GetProperties();
                            $loggerFromLeadUpdate->log('iblockElementUpdate. $arElemObracheniya[] = '.print_r($arElemObracheniya, true));

                            //Получаем данные лида
                            $CCrmLead = new CCrmLead();
                            $lidID = $arElemProps["LID_KLIENTA"]["VALUE"];
                            $loggerFromLeadUpdate->log('iblockElementUpdate. $lidID = '.$lidID);
                            $rsLead = $CCrmLead->GetList(
                                array("ID" => "ASC"),
                                array("ID" => $lidID),
                                array("ID", "ASSIGNED_BY_ID", "STATUS_ID", "UF_OBRACHENIYE_ID", "UF_CLOSURE_TEXT", "UF_RESULT")
                            );
                            if($arLead = $rsLead->Fetch())
                            {
                                $loggerFromLeadUpdate->log('iblockElementUpdate. $arLead[] = '.print_r($arLead, true));
                                $arUpdLeadFields = array();

                                //Сверяем ответственных по лиду и обращению
                                if($arLead["ASSIGNED_BY_ID"] != $arElemProps["Owner_display"]["VALUE"] && !empty($arElemProps["Owner_display"]["VALUE"]))
                                {
                                    $arUpdLeadFields["ASSIGNED_BY_ID"] = $arElemProps["Owner_display"]["VALUE"];
                                }

                                //Сверяем статус лида и обращения

                                //Массив с соответсвием статусов ЛИДА и ОБРАЩЕНИЯ
                                $arConformityStatusLidObracheniye = array(
                                    "4" =>          "AE5F2F10-F46B-1410-FD9A-0050BA5D6C38", //Новый -> Новое обращение
                                    "5" =>          "7E9F1204-F46B-1410-FB9A-0050BA5D6C38", //В работе -> В работе
                                    "6" =>          "3E7F420C-F46B-1410-FC9A-0050BA5D6C38", //Консультация оказана -> Задача закрыта
                                    "CONVERTED" =>  "3E7F420C-F46B-1410-FC9A-0050BA5D6C38", //Создать на основании... -> Задача закрыта
                                    "JUNK" =>       "6E5F4218-F46B-1410-FE9A-0050BA5D6C38", //Не успешно -> Отменено
                                );

                                //Получаем массив с описанием текущего выбранного элемента свойства "Статус обращения" типа список для ОБРАЩЕНИЯ
                                $loggerFromLeadUpdate->log('iblockElementUpdate. $arElemProps["STATUS_OBRASHCHENIYA"] = '.print_r($arElemProps["STATUS_OBRASHCHENIYA"], true));
                                $arStatusObracheniyaXmlID = $arElemProps["STATUS_OBRASHCHENIYA"]["VALUE_XML_ID"];
                                $loggerFromLeadUpdate->log('iblockElementUpdate. $arStatusObracheniyaXmlID = '.$arStatusObracheniyaXmlID);

                                //Получаем соответствующий STATUS_ID для лида по выбранному элементу свойства "Статус обращения" из ОБРАЩЕНИЯ
                                $key = array_search($arStatusObracheniyaXmlID, $arConformityStatusLidObracheniye);
								if($key === false){
                                    $key = 5;
                                }
                                $loggerFromLeadUpdate->log('iblockElementUpdate. $key = '.$key);
                                if($arLead["STATUS_ID"] != $key)
                                {
                                    $arUpdLeadFields["STATUS_ID"] = $key;
                                    if($arUpdLeadFields["STATUS_ID"] != "JUNK"){
                                        $arUpdLeadFields["UF_CLOSURE_TEXT"] = '';
                                    }
                                }

                                if(!empty($arUpdLeadFields["STATUS_ID"]) && $arLead["STATUS_ID"] != "JUNK"){
                                    $arUpdLeadFields["UF_CLOSURE_TEXT"] = '';
                                }

                                //Перезаписываем лид
                                if(!empty($arUpdLeadFields))
                                {
                                    $arUpdLeadFields["UPDATE_OBRACHENIYA"] = "Y";
                                    $loggerFromLeadUpdate->log('iblockElementUpdate. $arUpdLeadFields[] = '.print_r($arUpdLeadFields, true));
                                    if($CCrmLead->Update($arLead["ID"], $arUpdLeadFields))
                                    {
                                        $loggerFromLeadUpdate->log('iblockElementUpdate. Данные по лиду с ID = '.$arLead["ID"].' успешно обновлен!');
                                    }
                                    else
                                    {
                                        $loggerFromLeadUpdate->log('iblockElementUpdate. Ошибка обновления лида с ID = '.$arLead["ID"].'. ERROR->'.$CCrmLead->LAST_ERROR);
                                    }
                                }

                            }
                        }
                    }

                    $arElSelect = Array("ID", "IBLOCK_ID", "NAME");
                    if (!empty($arPropertiesId)) {
                        foreach ($arPropertiesId as $propertyId) {
                            $arElSelect[] = "PROPERTY_" . $propertyId;
                        }
                    }
                    $arElFilter = Array("IBLOCK_ID" => $arFields["IBLOCK_ID"], "ID" => $elementId);
                    $dbElem = CIBlockElement::GetList(Array(), $arElFilter, false, false, $arElSelect);
                    while ($arElem = $dbElem->GetNext()) {
                        // сравниваем имена
                        if ($arElem["NAME"] != $arFields["NAME"]) $triggerUpdate = true;

                        // сравниваем свойства
                        if (!empty($arFields["PROPERTY_VALUES"])) {
                            foreach ($arFields["PROPERTY_VALUES"] as $keyProperty => $property) {
                                $propertyValue = end($property);
                                $propertyValue = $propertyValue["VALUE"];

                                // для несписочных свойств
                                if (!isset($arElem["PROPERTY_" . $keyProperty . "_ENUM_ID"])) {
                                    // если поле не типа техt/html
                                    if (!is_array($propertyValue)) {
                                        if ($arElem["PROPERTY_" . $keyProperty . "_VALUE"] != $propertyValue) $triggerUpdate = true;
                                    } else {
                                        // для текстовых
                                        if ($propertyValue["TEXT"]) {
                                            if ($arElem["PROPERTY_" . $keyProperty . "_VALUE"]["TEXT"] != $propertyValue["TEXT"]) $triggerUpdate = true;
                                        }
                                    }
                                } // для списочного свойства
                                else {
                                    if ($arElem["PROPERTY_" . $keyProperty . "_ENUM_ID"] != $propertyValue) $triggerUpdate = true;
                                }
                            }
                        }
                    }

                    // отсылка данных в bpm
                    //Если обновление вызвано со стороны БПМ, то не отправляем данные в БПМ
                    if($arFields["UPDATE_FROM_BPM"] == "Y")
                        $triggerUpdate = false;

                    $logger->log("iblockElementUpdate. triggerUpdate = ".$triggerUpdate);
                    if ($triggerUpdate) {
                        $arUserSatisfaction = array(
                            365 => BPM_MANAGER_VLASENKO,
                            396 => BPM_MANAGER_BUNTOVA,
                            410 => BPM_MANAGER_GRIBANOVA,
                            398 => BPM_MANAGER_KOPEYKINA,
                            285 => BPM_MANAGER_LUTAY,
                            399 => BPM_MANAGER_SERGEENKO,
                            346 => BPM_MANAGER_GDANOVA,
                            302 => BPM_MANAGER_ZAMARAEVA,
                            426 => BPM_MANAGER_KOLOMYCEV,
                            275 => BPM_MANAGER_SHEMYKIN,
                            402 => BPM_MANAGER_GAVRILOV,
                            300 => BPM_MANAGER_GUREV,
                            357 => BPM_MANAGER_DEREVYNSKIY,
                            403 => BPM_MANAGER_KRYLOV,
                            347 => BPM_MANAGER_MAKSIMOV,
                            342 => BPM_MANAGER_MALCEV,
                            259 => BPM_MANAGER_MASLENNIKOVA,
                            400 => BPM_MANAGER_POPOV,
                            161 => BPM_MANAGER_FEOKTISOVA,
                            301 => BPM_MANAGER_SMOLYKOV,
                            311 => BPM_MANAGER_STEBLIN,
                            401 => BPM_MANAGER_TABAKOV,
                            299 => BPM_MANAGER_COKOLENKO,
                            406 => BPM_MANAGER_YKOVLEVA,
                        );

                        switch ($iblockCode) {
                            case (IBLOK_TWO_LINE):
                                $grup_soc = GRUP_SOC_SUPPORT;
                                break;
                            case (IBLOK_OTDEL_MARKET):
                                $grup_soc = GRUP_SOC_MARKETING;
                                break;
                            case (IBLOK_OTDEL_SALE):
                                $grup_soc = GRUP_SOC_SALE;
                                break;
                            case (IBLOK_OTDEL_SALE_PARTNER):
                                $grup_soc = GRUP_SOC_SALE_PARTNER;
                                break;
                        }
                        $elementId = $arFields["ID"];
                        $IBLOCK_ID = $arFields["IBLOCK_ID"];

                        $arElSelect = Array(
                            "ID",
                            "NAME",
                            "IBLOCK_ID",
                            "PROPERTY_UsrINN",
                            "PROPERTY_idOldTiket",
                            "PROPERTY_ID_ZADACHI_PO_OBRASHCHENIYU",
                            "PROPERTY_TEKST_SOOBSHCHENIYA",
                            "PROPERTY_ISTORIYA_RABOTY_S_OBRASHCHENIEM",
                            "PROPERTY_KOMPANIYA_KLIENTA",
                            "PROPERTY_LID_KLIENTA",
                            "PROPERTY_E_MAIL",
                            "PROPERTY_UsrPhoneNumber"
                        );

                        $arElFilter = Array("IBLOCK_ID" => $IBLOCK_ID, "ID" => $elementId);
                        $dbElem = CIBlockElement::GetList(Array(), $arElFilter, false, false, $arElSelect);

                        while ($ob = $dbElem->GetNextElement()) {
                            $arFieldsOld = $ob->GetFields();
                            $arProps = $ob->GetProperties();
                            $historys = $arProps["ISTORIYA_RABOTY_S_OBRASHCHENIEM"]["VALUE"];
                            $reshenie = htmlspecialcharsBack($arProps["TEKST_SOOBSHCHENIYA"]["VALUE"]["TEXT"]);
                            $guid = trim($arProps["ID_ZADACHI_PO_OBRASHCHENIYU"]["VALUE"]);
                            $idOldTiket = $arFieldsOld["ID"];
                            $owner_display = $arProps["Owner_display"]["VALUE"];
                            $owner = $arProps["Owner"]["VALUE"];
                        }
                        $href = "https://" . $_SERVER["HTTP_HOST"] . "/workgroups/group/" . $grup_soc . "/lists/" . $IBLOCK_ID . "/element/0/" . $elementId . "/";
//pre($arProps);
                        //exit;
                        //CIBlockElement::GetPropertyValues();
                        if (is_array($historys) and count($historys) > 1) {
                            foreach ($historys as $val => $hist) {
                                $his[] = $hist["TEXT"];
                            }
                            $history = implode(",", $his);
                        } else {
                            $history = $historys[0]["TEXT"];
                        }

                        //Получаем текущие данные тикета из BPM
                        $arCurTiketInfo = TiketBpmBitrix::getTiket($guid, null, true);

                        //формирование данных на первичное обновление данных тикета на стороне bpm
                        $link = array();
                        $res = CIBlockElement::GetProperty($IBLOCK_ID, $elementId, "sort", "asc", array("CODE" => "STATUS_OBRASHCHENIYA"));
                        while ($ob = $res->GetNext()) {
                            $link["status"] = $ob['VALUE_XML_ID'];
                        }
                        $res = CIBlockElement::GetProperty($IBLOCK_ID, $elementId, "sort", "asc", array("CODE" => "GRUPPY_OTVETSTVENNYKH"));
                        while ($ob = $res->GetNext()) {
                            $link["grup_owner"] = $ob['VALUE_XML_ID'];
                        }
                        $res = CIBlockElement::GetProperty($IBLOCK_ID, $elementId, "sort", "asc", array("CODE" => "REASON_CLOSURE"));
                        while ($ob = $res->GetNext()) {
                            $link["reason_closure"] = $ob['VALUE_XML_ID'];
                        }

                        $link["idOldTiket"] = $idOldTiket;
                        $link["href"] = $href;
                        $link["id"] = $guid;
                        $link["history"] = $history;
                        if ($link["grup_owner"] == ID_GROUP_EXPLUATEION) {
                            $link["owner"] = null;
                        } else {
                            //В битриксе поставили ответственного Иван Пупкин, если в БПМ его нет - то ставим  Катя Бунтова ответственного в БПМ.
                            //Сверяем ответственных в БПМ и Битрикс
                            if($arCurTiketInfo["Owner"] != $arUserSatisfaction[$owner_display]){
                                if(!empty($owner_display)){
                                    if(isset($arUserSatisfaction[$owner_display])){
                                        $link["owner"] = $arUserSatisfaction[$owner_display];
                                    } else {
                                        $link["owner"] = BPM_MANAGER_BUNTOVA;
                                        $rsUser = CUser::GetByID($owner_display);
                                        $arUser = $rsUser->Fetch();
                                        $link["history"] .= ', Ответственный по обращению в Битрикс: '.$arUser["NAME"].' '.$arUser["LAST_NAME"];
                                    }
                                    //Исключение, на случай, если в BPM назначаен пользователь BPM_MANAGER_BUNTOVA,
                                    //который является пользователем по умолчанию, если совпадений не найдено
                                    if($arCurTiketInfo["Owner"] == BPM_MANAGER_BUNTOVA){
                                        unset($link["owner"]);
                                    }
                                } else {
                                    $link["owner"] = null;
                                }
                            }
                        }

                        $link["reshenie"] = $reshenie;
                        //pre($link);
                        //exit;

                        $logger = Logger::getLogger('LeadAddCiblockEvent', 'ofd.bitrix24/TiketAddCiblockEvent.log');
                        $logger->log("iblockElementUpdate. tiketUpdate. Link: ");
                        $logger->log($link);

                        $tiketUpdate = CrmBitrixBpmUpdate::tiketUpdate($link);
                        $tikUp["tiketUpdate"] = $tiketUpdate;
                        CIBlockElement::SetPropertyValueCode($elementId, "idOldTiket", $elementId);
                        $tikUp["link"] = $link;
                        $logger = Logger::getLogger('ProblemUpdateTiket', 'ofd.bitrix24/ProblemUpdateTiket.log');
                        $logger->log($tikUp);
                    }
                }
            }
        }
    }

    public function iblockElementAdd(&$arFields)
    {

        if (self::access($arFields['IBLOCK_ID'])) {
            // получаем CODE инфоблока и сравниваем с кодами инфоблоков обращений
            $loggerAddElem = Logger::getLogger('iblockElementAdd', 'ofd.bitrix24/iblockElementAdd.log');
            $dbIBlock = CIBlock::GetByID($arFields["IBLOCK_ID"]);
            if ($arIBlock = $dbIBlock->GetNext()) {
                $iblockCode = $arIBlock["CODE"];
                if ($iblockCode == IBLOK_TWO_LINE or $iblockCode == IBLOK_OTDEL_MARKET or $iblockCode == IBLOK_OTDEL_SALE or $iblockCode == IBLOK_OTDEL_SALE_PARTNER) {
                    switch ($iblockCode) {
                        case (IBLOK_TWO_LINE):
                            $grup_soc = GRUP_SOC_SUPPORT;
                            break;
                        case (IBLOK_OTDEL_MARKET):
                            $grup_soc = GRUP_SOC_MARKETING;
                            break;
                        case (IBLOK_OTDEL_SALE):
                            $grup_soc = GRUP_SOC_SALE;
                            break;
                        case (IBLOK_OTDEL_SALE_PARTNER):
                            $grup_soc = GRUP_SOC_SALE_PARTNER;
                            break;
                    }
                    $arUserSatisfaction = array(
                        365 => BPM_MANAGER_VLASENKO,
                        396 => BPM_MANAGER_BUNTOVA,
                        410 => BPM_MANAGER_GRIBANOVA,
                        398 => BPM_MANAGER_KOPEYKINA,
                        285 => BPM_MANAGER_LUTAY,
                        399 => BPM_MANAGER_SERGEENKO,
                        346 => BPM_MANAGER_GDANOVA,
                        302 => BPM_MANAGER_ZAMARAEVA,
                        426 => BPM_MANAGER_KOLOMYCEV,
                        275 => BPM_MANAGER_SHEMYKIN,
                        402 => BPM_MANAGER_GAVRILOV,
                        300 => BPM_MANAGER_GUREV,
                        357 => BPM_MANAGER_DEREVYNSKIY,
                        403 => BPM_MANAGER_KRYLOV,
                        347 => BPM_MANAGER_MAKSIMOV,
                        342 => BPM_MANAGER_MALCEV,
                        259 => BPM_MANAGER_MASLENNIKOVA,
                        400 => BPM_MANAGER_POPOV,
                        161 => BPM_MANAGER_FEOKTISOVA,
                        301 => BPM_MANAGER_SMOLYKOV,
                        311 => BPM_MANAGER_STEBLIN,
                        401 => BPM_MANAGER_TABAKOV,
                        299 => BPM_MANAGER_COKOLENKO,
                        406 => BPM_MANAGER_YKOVLEVA,
                    );
                    $elementId = $arFields["ID"];
                    $IBLOCK_ID = returnIdCodeIblock($iblockCode);
                    $arElSelect = Array("ID", "NAME", "IBLOCK_ID", "PROPERTY_UsrINN", "PROPERTY_idOldTiket", "PROPERTY_Origin", "PROPERTY_UsrCaseType", "PROPERTY_ID_ZADACHI_PO_OBRASHCHENIYU", "PROPERTY_TEKST_SOOBSHCHENIYA", "PROPERTY_ISTORIYA_RABOTY_S_OBRASHCHENIEM", "PROPERTY_KOMPANIYA_KLIENTA", "PROPERTY_LID_KLIENTA", "PROPERTY_E_MAIL", "PROPERTY_UsrPhoneNumber");

                    $arElFilter = Array("IBLOCK_ID" => $IBLOCK_ID, "ID" => $elementId);
                    $dbElem = CIBlockElement::GetList(Array(), $arElFilter, false, false, $arElSelect);

                    while ($ob = $dbElem->GetNextElement()) {
                        $arFieldsOld = $ob->GetFields();
                        $arProps = $ob->GetProperties();
                        $titleLid = $arFieldsOld["NAME"];
                        $idOldTiket = $arProps["idOldTiket"]["VALUE"];
                        $inn = $arProps["UsrINN"]["VALUE"];
                        $contragentGuid = $arProps["Account"]["VALUE"];
                        $idcompany = $arProps["KOMPANIYA_KLIENTA"]["ID"];
                        $idLid = $arProps["LID_KLIENTA"]["ID"];
                        $guidContact = $arProps["Contact"]["VALUE"];
                        $email = trim($arProps["E_MAIL"]["VALUE"]);
                        $phone = trim($arProps["UsrPhoneNumber"]["VALUE"]);
                        $contactName = trim($arProps["Contact_display"]["VALUE"]);
                        $historys = $arProps["ISTORIYA_RABOTY_S_OBRASHCHENIEM"]["VALUE"];
                        $reshenie = htmlspecialcharsBack($arProps["TEKST_SOOBSHCHENIYA"]["VALUE"]["TEXT"]);
                        $guid = $arProps["ID_ZADACHI_PO_OBRASHCHENIYU"]["VALUE"];
                        $Origin = $arProps["Origin"]["VALUE"];
                        $owner_display = $arProps["Owner_display"]["VALUE"];
                        $owner = $arProps["Owner"]["VALUE"];
                    }
                    $href = "https://" . $_SERVER["HTTP_HOST"] . "/workgroups/group/" . $grup_soc . "/lists/" . $IBLOCK_ID . "/element/0/" . $elementId . "/";
                    $nameLink = "<a href='$href' >Ссылка на обращение в Битрикс24</a>";


                    //CIBlockElement::GetPropertyValues();
                    if (is_array($historys) and count($historys) > 1) {
                        foreach ($historys as $val => $hist) {
                            $his[] = $hist["TEXT"];
                        }
                        $history = implode(",", $his);
                    } else {
                        $history = $historys[0]["TEXT"];
                    }

                    //формирование данных на первичное обновление данных тикета на стороне bpm
                    $link = array();
                    $res = CIBlockElement::GetProperty($IBLOCK_ID, $elementId, "sort", "asc", array("CODE" => "STATUS_OBRASHCHENIYA"));
                    while ($ob = $res->GetNext()) {
                        $link["status"] = $ob['VALUE_XML_ID'];
                    }
                    $res = CIBlockElement::GetProperty($IBLOCK_ID, $elementId, "sort", "asc", array("CODE" => "GRUPPY_OTVETSTVENNYKH"));
                    while ($ob = $res->GetNext()) {
                        $link["grup_owner"] = $ob['VALUE_XML_ID'];
                    }
                    $res = CIBlockElement::GetProperty($IBLOCK_ID, $elementId, "sort", "asc", array("CODE" => "REASON_CLOSURE"));
                    while ($ob = $res->GetNext()) {
                        $link["reason_closure"] = $ob['VALUE_XML_ID'];
                    }
                    $link["idOldTiket"] = $idOldTiket;
                    $link["href"] = $href;
                    $link["id"] = $guid;
                    $link["history"] = $history;

                    //В битриксе поставили ответственного Иван Пупкин, если в БПМ его нет - то ставим  Катя Бунтова ответственного в БПМ.
                    if(!empty($owner_display)) {
                        if(isset($arUserSatisfaction[$owner_display])){
                            $link["owner"] = $arUserSatisfaction[$owner_display];
                        } else {
                            $link["owner"] = BPM_MANAGER_BUNTOVA;
                            $rsUser = CUser::GetByID($owner_display);
                            $arUser = $rsUser->Fetch();
                            $link["history"] .= ', Ответственный по обращению в Битрикс: '.$arUser["NAME"].' '.$arUser["LAST_NAME"];
                        }
                    } else {
                        $link["owner"] = null;
                    }
                    $link["reshenie"] = $reshenie;
                    $logger = Logger::getLogger('LeadAddContr1', 'ofd.bitrix24/LeadAddContr1.log');
                    $logger->log(array($guid, $arFields));
                    //проверка на наличие привязки к контрагенту
                    if (!empty($contragentGuid)) {
                        if($idContr = RestBpmBitrix::verify(null, null, $contragentGuid)){
                            $contr_id = $idContr["id"];
                            CIBlockElement::SetPropertyValues($elementId, $IBLOCK_ID, $contr_id, $idcompany);
                            $logger = Logger::getLogger('AddContrTiket', 'ofd.bitrix24/AddContrTiket.log');
                            $logger->log($arFields);
                        }
                    } else {
                        $oLead = new CCrmLead;
                        $propIDHiddenElem = self::getIdPropertyListByText($IBLOCK_ID, "SKRYTOE_OBRASHCHENIE", "Да");
                        $loggerAddElem->log("CIBlockElement::SetPropertyValuesEx: $elementId, $IBLOCK_ID, $propIDHiddenElem");
                        CIBlockElement::SetPropertyValuesEx($elementId, $IBLOCK_ID, array("SKRYTOE_OBRASHCHENIE" => $propIDHiddenElem));
                        $logger = Logger::getLogger('LeadAddCiblockEvent', 'ofd.bitrix24/TiketAddCiblockEvent.log');

                        /** Search contact by contactId BPM */

                        $arFilter = array(
                            "UF_BPMCONTACTID" => $guidContact,
                        );

                        $resContact = CCrmContact::GetList(array(), $arFilter);
                        if ($arContact = $resContact->Fetch()) {
                            $arFieldsLead['NAME'] = $arContact["NAME"];
                            $arFieldsLead['SECOND_NAME'] = $arContact["SECOND_NAME"];
                            $arFieldsLead['LAST_NAME'] = $arContact["LAST_NAME"];
                            $arFieldsLead['EMAIL'] = "";
                            $arFieldsLead['PHONE'] = "";

                            //Get Email/Phone contact
                            $resMultiContact = CCrmFieldMulti::GetList(array(), array('ENTITY_ID' => 'CONTACT', 'ELEMENT_ID' => $arContact['ID']));
                            while ($arMultiContact = $resMultiContact->Fetch()) {
                                if ($arMultiContact['TYPE_ID'] == 'EMAIL') {
                                    $email = $arMultiContact['VALUE'];
                                }
                                if ($arMultiContact['TYPE_ID'] == 'PHONE') {
                                    $phone = $arMultiContact['VALUE'];
                                }
                            }
                        }

                        
                        $lead_id = "";

                        $logger->log("Значение переменной email: [$email]");
                        $logger->log("Значение переменной phone: [$phone]");

                        if (!empty($email)) {
                            $dbResMultiFields = CCrmFieldMulti::GetList(
                                array('ID' => 'asc'),
                                array('ENTITY_ID' => 'LEAD', 'TYPE_ID' => 'EMAIL', 'VALUE' => $email)
                            );
                            while ($arMultiFields = $dbResMultiFields->Fetch()) {
                                $lead_id = $arMultiFields["ELEMENT_ID"];
                            }
							
                            CIBlockElement::SetPropertyValues($elementId, $IBLOCK_ID, $email, "E_MAIL");
                            CIBlockElement::SetPropertyValues($elementId, $IBLOCK_ID, $phone, "UsrPhoneNumber");
                        } elseif (!empty($phone)) {
                            $dbResMultiFields = CCrmFieldMulti::GetList(
                                array('ID' => 'asc'),
                                array('ENTITY_ID' => 'LEAD', 'TYPE_ID' => 'PHONE', 'VALUE' => "'" . $phone . "'")
                            );
                            while ($arMultiFields = $dbResMultiFields->Fetch()) {
                                $lead_id = $arMultiFields["ELEMENT_ID"];
                            }

                            CIBlockElement::SetPropertyValues($elementId, $IBLOCK_ID, $phone, "UsrPhoneNumber");
                        }
                        $logger->log("Значение переменной lead_id: [$lead_id]");

                        //Если нашли ЛИД, то проверяем, были ли он создан при вызове метода LeadBpmBitrix::leadAdd
                        //т.к. в данном методе сначала создается лид, а потом обращение
                        //и необходимо обновить данные в лиде и обращение
                        if (!empty($lead_id)) {
                            $rsLead = $oLead->GetList(
                                Array('DATE_CREATE' => 'DESC'),
                                array("ID" => $lead_id),
                                array(),
                                false
                            );
                            if($arSearchedLead = $rsLead->Fetch()){
                                if($arSearchedLead["TITLE"] == "Обращение в КЦ" && empty($arSearchedLead["COMMENTS"])) {
                                    $updLeadFields = array(
                                        'COMMENTS' => $href,
                                        'UF_OBRACHENIYE_ID' => $elementId,
                                        'UPDATE_OBRACHENIYA' => 'Y'
                                    );
                                    $CCrmLead->Update(
                                        $lead_id,
                                        $updLeadFields
                                    );
                                    CIBlockElement::SetPropertyValues($elementId, $IBLOCK_ID, $lead_id, "LID_KLIENTA");
                                } else {
                                    $lead_id = '';
                                }
                            }
                        }

                        $logger->log("Значение переменной lead_id: [$lead_id]");

                        if (empty($lead_id)) {
                            switch ($Origin) {
                                case (strtolower(BPM_ORIGIN_CALL)):
                                    $surs = ID_ORIGIN_CALL;
                                    break;
                                case (strtolower(BPM_ORIGIN_EMAIL)):
                                    $surs = ID_ORIGIN_EMAIL;
                                    break;
                                default:
                                    $surs = ID_ORIGIN_CALL;
                            }
                            if (empty($email)) {
                                $arr = preg_split('/\(|\)(, *)?/', $contactName, -1, PREG_SPLIT_NO_EMPTY);
                                foreach ($arr as $key => $val) {
                                    $output = filter_var($val, FILTER_VALIDATE_EMAIL);
                                    if ($output) {
                                        $email = $output;
                                    }
                                }
                            }

                            $assignedFromLid = BITRIX24_MANAGER_BUNTOVA;
                            if(!empty($owner_display)) {
                                $assignedFromLid = $owner_display;
                            }

                            $arFieldsLeadAdd = Array(
                                "TITLE" => "Обращения на 2-ую линию",
                                "COMPANY_TITLE" => "",
                                "NAME" => $contactName,
                                "LAST_NAME" => "",
                                "SECOND_NAME" => "",
                                "POST" => "",
                                "ADDRESS" => "",
                                "COMMENTS" => $nameLink,
                                "SOURCE_DESCRIPTION" => "Источник servicedesk",
                                "STATUS_DESCRIPTION" => "",
                                "OPPORTUNITY" => "",
                                "CURRENCY_ID" => "",
                                "PRODUCT_ID" => "",
                                "SOURCE_ID" => $surs,
                                "STATUS_ID" => "4",
                                "ASSIGNED_BY_ID" => $assignedFromLid,
                                "FM" => Array(
                                    "EMAIL" => Array(
                                        "n1" => Array(
                                            "VALUE" => $email,
                                            "VALUE_TYPE" => "WORK",
                                        )
                                    ),
                                    "PHONE" => Array(
                                        "n1" => Array(
                                            "VALUE" => $phone,
                                            "VALUE_TYPE" => "WORK",
                                        )
                                    ),
                                ),
                                "UF_CHANNEL" => ID_CHANNEL,
                                "UF_OBRACHENIYE_ID" => $elementId,
                            );

                            //Устанавливаем состояние лида в [ACTIVITY]
                            $arFields["UF_STATE_LEAD"] = getIdElementListState('XML_STATE_LEAD_ACTIVITY');

                            $oLead = new CCrmLead;
                            $loggerAddElem->log('$arFieldsLeadAdd[] = '.print_r($arFieldsLeadAdd, true));
                            $LidID = $oLead->Add($arFieldsLeadAdd);
                            if ($LidID) {
                                $loggerAddElem->log("CIBlockElement::SetPropertyValues: $elementId, $IBLOCK_ID, $LidID, $idLid");
                                CIBlockElement::SetPropertyValues($elementId, $IBLOCK_ID, $LidID, $idLid);
                                $logger->log("iblockElementAdd: Создали лида [$LidID]");
                                $logger->log($arFieldsLeadAdd);
                            }
                        }
                    }
                    $loggerAddElem->log('$link[] = '.print_r($link, true));
                    $tiketUpdate = CrmBitrixBpmUpdate::tiketUpdate($link);
                    $tikUp["tiketUpdate"] = $tiketUpdate;
                    CIBlockElement::SetPropertyValueCode($elementId, "idOldTiket", $elementId);
                    $tikUp["link"] = $link;
                    if (!empty($link["idOldTiket"])) {
                        $logger = Logger::getLogger('eventOldTiketCreat', 'ofd.bitrix24/eventOldTiketCreat.log');
                        $logger->log($tikUp);
                    } else {
                        $logger = Logger::getLogger('eventNewTiketCreat', 'ofd.bitrix24/eventNewTiketCreat.log');
                        $logger->log($tikUp);
                    }

                }
            }
        }
    }

    public static function getIdPropertyListByText($iblock, $code, $textVal){
        $db_enum_list = CIBlockProperty::GetPropertyEnum($code, Array(), Array("IBLOCK_ID"=>$iblock, "VALUE"=>$textVal));
        if($ar_enum_list = $db_enum_list->GetNext())
        {
            $result = $ar_enum_list["ID"];
        }
        return ($result);
    }
}


