<?

class Industry
{
    public static function ChangeOkvedByTimeAgent()
    {
        $steptime = 15;//время в минутах.
        Industry::ChangeOkvedByTime($steptime);

        return "Industry::ChangeOkvedByTimeAgent();";
    }

    public static function ChangeOkvedByTime($steptime)
    {
        $arrayelements = [];
        global $DB;
        //$strSql = "select ENTITY_ID, RQ_OKVED  FROM b_crm_requisite where DATE_MODIFY > DATE_SUB(CURRENT_TIMESTAMP(), INTERVAL $steptime MINUTE) and RQ_OKVED IS NOT NULL";
        $strSql = "select ENTITY_ID, RQ_OKVED  FROM b_crm_requisite where DATE_MODIFY > DATE_SUB(CURRENT_TIMESTAMP(), INTERVAL $steptime MINUTE)";

        $resCompany = $DB->Query($strSql, false, false);
        while ($arCompany = $resCompany->Fetch()) {
            if (!empty($arCompany['RQ_OKVED'])) {
                //TODO переписать на запрос из бд
                $arCompany['INDUSTRY'] = GetUserField("CRM_COMPANY", $arCompany['ENTITY_ID'], "UF_OKVED");
                $namefromokved = Industry::GetNameOkvedUsingId($arCompany['RQ_OKVED']);
                if ($namefromokved != $arCompany['INDUSTRY']) {
                    SetUserField("CRM_COMPANY", $arCompany['ENTITY_ID'], "UF_OKVED", $namefromokved);
                    $arCompany['INDUSTRY'] = $namefromokved;
                    $arrayelements[] = $arCompany;
                }
            } else {
                SetUserField("CRM_COMPANY", $arCompany['ENTITY_ID'], "UF_OKVED", "");
            }
        }

        $strSql = "select ENTITY_ID, RQ_OKVED  FROM b_crm_requisite where DATE_CREATE > DATE_SUB(CURRENT_TIMESTAMP(), INTERVAL $steptime MINUTE) and RQ_OKVED IS NOT NULL";
        $resCompany = $DB->Query($strSql, false, false);
        while ($arCompany = $resCompany->Fetch()) {
            if (!empty($arCompany['RQ_OKVED'])) {
                //TODO переписать на запрос из бд
                $arCompany['INDUSTRY'] = GetUserField("CRM_COMPANY", $arCompany['ENTITY_ID'], "UF_OKVED");
                $namefromokved = Industry::GetNameOkvedUsingId($arCompany['RQ_OKVED']);
                if ($namefromokved != $arCompany['INDUSTRY']) {
                    SetUserField("CRM_COMPANY", $arCompany['ENTITY_ID'], "UF_OKVED", $namefromokved);
                    $arCompany['INDUSTRY'] = $namefromokved;
                    $arrayelements[] = $arCompany;
                }
            }
        }

        return $arrayelements;
    }

    public static function ChangeAllOkved()
    {
        $arrayelements = [];
        global $DB;
        $strSql = "select ENTITY_ID, RQ_OKVED  FROM b_crm_requisite where RQ_OKVED IS NOT NULL";
        $resCompany = $DB->Query($strSql, false, false);
        while ($arCompany = $resCompany->Fetch()) {
            if (!empty($arCompany['RQ_OKVED'])) {
                //TODO переписать на запрос из бд
                $arCompany['INDUSTRY'] = GetUserField("CRM_COMPANY", $arCompany['ENTITY_ID'], "UF_OKVED");
                $namefromokved = Industry::GetNameOkvedUsingId($arCompany['RQ_OKVED']);
                if ($namefromokved != $arCompany['INDUSTRY']) {
                    SetUserField("CRM_COMPANY", $arCompany['ENTITY_ID'], "UF_OKVED", $namefromokved);
                    $arCompany['INDUSTRY'] = $namefromokved;
                    $arrayelements[] = $arCompany;
                }
            }
        }

        return $arrayelements;
    }

    public static function GetNameOkvedUsingId($idokved)
    {
        $stringname = "";
        $IBLOCK_ID_OKVED = getIblockIDByCode('okved');
        $arSelect = Array("NAME");
        $arFilter = array("IBLOCK_ID" => $IBLOCK_ID_OKVED, "XML_ID" => $idokved);
        $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
        if ($arOKVED = $res->Fetch()) {
            $stringname = $arOKVED['NAME'];
        }

        return $stringname;
    }
}