<?

//Loader::includeModule("iblock");
//Loader::includeModule("crm");

class SlaBpmBitrix
{


    public static function SelectGuidServDogovor($guidcontragent)
    {
        $tiket = array (
            'RootSchemaName' => 'ServiceObject',
            'QueryType' => 0,
            'Columns' =>
                array (
                    'Items' =>
                        array (
                            'ServicePact' =>
                                array (
                                    'Expression' =>
                                        array (
                                            'ColumnPath' => 'ServicePact',
                                        ),
                                ),
                        ),
                ),

            'Filters' =>
                array (
                    'FilterType' => 6,
                    'ComparisonType' => 0,
                    'Items' =>
                        array (
                            'FilterAccount' =>
                                array (
                                    'FilterType' => 1,
                                    'ComparisonType' => 3,
                                    'LeftExpression' =>
                                        array (
                                            'ExpressionType' => 0,
                                            'ColumnPath' => 'Account.Id',
                                        ),
                                    'RightExpression' =>
                                        array (
                                            'ExpressionType' => 2,
                                            'Parameter' =>
                                                array (
                                                    'DataValueType' => 0,
                                                    'Value' => $guidcontragent,
                                                ),
                                        ),
                                ),


                        ),
                ),
        );

        $arrTeket = QueryBpm::jsonDataBpm($tiket, BPM_URL_SELECT);

        if($arrTeket["status"] == 403)
        {
            QueryBpm::login();
            $arrTeket = QueryBpm::jsonDataBpm($tiket, BPM_URL_SELECT);
        }

        if(empty($arrTeket)){return "-1";}
        else{return $arrTeket['success']['rows'];}


    }




    public static function SelectSlarServDogovor($arrayanswer)
    {
        $elementguid="";
        $elemid=1;
        $resultfilter=array();
// генерация итогового фильтра содержащий неопределенное количество входных параметров
        foreach($arrayanswer as $elemanswer)
        {
            $elementguid=$elemanswer['ServicePact']['value'];
            $resultfilter["FilterServicePactId$elemid"]=array (
            'FilterType' => 1,
            'ComparisonType' => 3,
            'LeftExpression' =>
                array (
                    'ExpressionType' => 0,
                    'ColumnPath' => 'ServicePact.Id',
                ),
            'RightExpression' =>
                array (
                    'ExpressionType' => 2,
                    'Parameter' =>
                        array (
                            'DataValueType' => 0,
                            'Value' => "$elementguid",
                        ),
                ),);$elemid++;
        }

        $tiket = array (
            'RootSchemaName' => 'ServiceInServicePact',
            'QueryType' => 0,
            'Columns' =>
                array (
                    'Items' =>
                        array (
                            'ServicePact' =>
                                array (
                                    'Expression' =>
                                        array (
                                            'ColumnPath' => 'ServicePact',
                                        ),
                                ),
                            'ServiceItem' =>
                                array (
                                    'Expression' =>
                                        array (
                                            'ColumnPath' => 'ServiceItem',
                                        ),
                                ),
                            'ReactionTimeValue' =>
                                array (
                                    'Expression' =>
                                        array (
                                            'ColumnPath' => 'ReactionTimeValue',
                                        ),
                                ),
                            'ReactionTimeUnit' =>
                                array (
                                    'Expression' =>
                                        array (
                                            'ColumnPath' => 'ReactionTimeUnit',
                                        ),
                                ),
                            'SolutionTimeValue' =>
                                array (
                                    'Expression' =>
                                        array (
                                            'ColumnPath' => 'SolutionTimeValue',
                                        ),
                                ),

                            'SolutionTimeUnit' =>
                                array (
                                    'Expression' =>
                                        array (
                                            'ColumnPath' => 'SolutionTimeUnit',
                                        ),
                                ),
                       ),
                ),

            'Filters' =>
                array (
                    'FilterType' => 6,
                    'ComparisonType' => 0,
                    'LogicalOperation' => 'OR',
                    'Items' => $resultfilter,
                ),
        );
        $arrTeket = QueryBpm::jsonDataBpm($tiket, BPM_URL_SELECT);
        if($arrTeket["status"] == 403)
        {
            QueryBpm::login();
            $arrTeket = QueryBpm::jsonDataBpm($tiket, BPM_URL_SELECT);
        }

        if(empty($arrTeket)){return "-1";}
        else{return $arrTeket['success']['rows'];}
    }

    public static function DisplaySlaMatrix($idcontr)
    {
        if (!empty($idcontr)) {
            $resultguidcont = GetUserField("CRM_COMPANY", $idcontr, "UF_CRM_COMPANY_GUID");

            $resultarray = SlaBpmBitrix::SelectGuidServDogovor($resultguidcont);
            $resultanswer = SlaBpmBitrix::SelectSlarServDogovor($resultarray);
            echo "<br><br><table class='reports-list-table' id='report-result-table' >";


            if(!empty($resultanswer)){
                echo "<tr>";
                echo "<td class='reports-total-column'>";
                echo "Сервисный договор";
                echo "</td>";
                echo "<td class='reports-total-column'>";
                echo "Название сервиса";
                echo "</td>";
                echo "<td colspan='2' class='reports-total-column'>";
                echo "Время реакции";
                echo "</td>";
                echo "<td colspan='2' class='reports-total-column'>";
                echo "Время решения";
                echo "</td>";
                echo "</tr>";
            }

            foreach ($resultanswer as $elemanswer) {
                echo "<tr>";
                echo "<td>";
                echo $elemanswer['ServicePact']['displayValue'];
                echo "</td>";
                echo "<td>";
                echo $elemanswer['ServiceItem']['displayValue'];
                echo "</td>";
                echo "<td>";
                echo $elemanswer['ReactionTimeValue'];
                echo "</td>";
                echo "<td>";
                echo $elemanswer['ReactionTimeUnit']['displayValue'];
                echo "</td>";
                echo "<td>";
                echo $elemanswer['SolutionTimeValue'];
                echo "</td>";
                echo "<td>";
                echo $elemanswer['SolutionTimeUnit']['displayValue'];
                echo "</td>";
                echo "</tr>";
            }
            echo "</table>";
        } else {
            return "-1";
        }


    }



}