<?
use Bitrix\Main\Loader;
use \Bitrix\Main\Application;
use \Bitrix\Crm;

class CrmEvent
{

    public static function deleteContragent(&$arFields){
        unset($_SESSION["DELETE_CONTR_LINE"][$arFields["ID"]]);
        $dbContragentGet = CrmBitrixBpm::GetListEx(
            array(),
            array('ID' => $arFields["ID"]),
            false,
            false,
            array("TITLE", "UF_CRM_COMPANY_GUID", "UF_CRM_INN", "UF_CRM_1486035239", "UF_CRM_1486033688", "UF_CRM_1486035185", "COMPANY_TYPE", "INDUSTRY", "ASSIGNED_BY_ID", "UF_CRM_COMPANY_GUID")
        );
        while ($dbContragent = $dbContragentGet->GetNext()) {

            $verifity["guidd"] = $dbContragent["UF_CRM_COMPANY_GUID"];
            $verifity["inn"] = $dbContragent["UF_CRM_INN"];
            if (!empty($verifity["dataOfd"])) {
                $dtim = null;
            } else {
                $dt = dataTimeformat("Y");
                $dtim = "\"$dt\"";
            }
        }
        $req = new \Bitrix\Crm\EntityRequisite();
        $rser = $req->getList(array(
            "filter" => array(
                "ENTITY_ID" => $arFields["ID"],
                "ENTITY_TYPE_ID" => CCrmOwnerType::Company,
                "PRESET_ID" => array(1, 2)
            )
        ));
        $rows = $rser->fetchAll();

        foreach ($rows as $company) {
            $params = array(
                'filter' => array(
                    'ENTITY_ID' => $company['ID'],
                    'ENTITY_TYPE_ID' => CCrmOwnerType::Requisite)
            );
            $bank = new \Bitrix\Crm\EntityBankDetail();
            $dbRes = $bank->getList(array(
                'filter' => array('ENTITY_ID' => $company['ID'])
            ));
            $rowsd = $dbRes->fetchAll();
            foreach ($rowsd as $k => $vale) {
                switch ($k) {
                    default:
                        $verifity["bank"][$vale["RQ_ACC_NUM"]] = $vale["RQ_ACC_NUM"];
                }

            }
            $adress = Bitrix\Crm\EntityRequisite::getAddresses($company['ID']);
            $dbResMultiFields = CCrmFieldMulti::GetList(array(), array('ENTITY_ID' => 'COMPANY', 'ELEMENT_ID' => $arFields["ID"]));
            while ($arMultiFields = $dbResMultiFields->Fetch()) {
                $comunicetions[] = $arMultiFields;

            }
            foreach ($comunicetions as $com => $value) {
                if ($value["TYPE_ID"] == "PHONE") {
                    switch ($value["VALUE_TYPE"]) {
                        case "WORK":
                            $value["COMMUN_TYP"] = BPM_COMMUNICATION_WORK_PHONE;
                            break;
                        case "MOBILE":
                            $value["COMMUN_TYP"] = BPM_COMMUNICATION_MOBFHONE;
                            break;
                        case "FAX":
                            $value["COMMUN_TYP"] = BPM_COMMUNICATION_FAX;
                            break;
                        case "OTHER":
                            $value["COMMUN_TYP"] = BPM_COMMUNICATION_OTHER_PHONE;
                            break;
                    }
                }
                if ($value["TYPE_ID"] == "EMAIL") {
                    switch ($value["VALUE_TYPE"]) {
                        case "WORK":
                            $value["COMMUN_TYP"] = BPM_COMMUNICATION_EMAIL;
                            break;
                        case "OTHER":
                            $value["COMMUN_TYP"] = BPM_COMMUNICATION_EMAIL;
                            break;
                    }
                }
                if ($value["TYPE_ID"] == "IM") {
                    switch ($value["VALUE_TYPE"]) {
                        case "SKYPE":
                            $value["COMMUN_TYP"] = BPM_COMMUNICATION_SKYPE;
                            break;
                    }
                }
                if ($value["TYPE_ID"] == "WEB") {
                    switch ($value["VALUE_TYPE"]) {
                        case "WORK":
                            $value["COMMUN_TYP"] = BPM_COMMUNICATION_URL;
                            break;
                        case "TWITTER":
                            $value["COMMUN_TYP"] = BPM_COMMUNICATION_TWITTER;
                            break;
                        case "FACEBOOK":
                            $value["COMMUN_TYP"] = BPM_COMMUNICATION_FACEBOOK;
                            break;
                        default:
                            $value["COMMUN_TYP"] = BPM_COMMUNICATION_URL;
                    }
                }
                $comunic[] = $value;
            }
            foreach ($adress as $key => $val) {
                switch ($key) {
                    case 6:
                        $verifity["adress"][$key]["typAdress"] = BPM_ADRESS_UR;
                        $verifity["adress"][$key]["adressValu"] = $val["ADDRESS_1"] . " ";
                        break;
                    case 1:
                        $verifity["adress"][$key]["typAdress"] = BPM_ADRESS_REAL;
                        $verifity["adress"][$key]["adressValu"] = $val["ADDRESS_1"] . " ";
                        break;
                    default:
                        $verifity["adress"][$key]["typAdress"] = BPM_ADRESS_OTHER;
                        $verifity["adress"][$key]["adressValu"] = $val["ADDRESS_1"] . " ";
                }
            }
        }
        foreach ($arFields["FM"]["PHONE"] as $key => $val) {

            $deleteComunic[$key] = $val['VALUE'];

        }
        foreach ($arFields["FM"]["EMAIL"] as $key => $val) {

            $deleteComunic[$key] = $val['VALUE'];

        }
        foreach ($arFields["FM"]["IM"] as $key => $val) {

            $deleteComunic[$key] = $val['VALUE'];

        }
        foreach ($arFields["FM"]["WEB"] as $key => $val) {

            $deleteComunic[$key] = $val['VALUE'];

        }
        $result["ID"] = $arFields["ID"];
        $result["guid"] = $arFields["UF_CRM_COMPANY_GUID"];
        $result["comunicetions"] = array_diff_assoc($comunic, $deleteComunic);
        $result["comunicetions_ar"] = $comunic;
        $result["comunicetions_del"] = $deleteComunic;
        $result["adress"] = $verifity["adress"];
        $result["bank"] = $verifity["bank"];
        $result["inn"] = $arFields;

        session_start();
        $_SESSION["DELETE_CONTR_LINE"][$arFields["ID"]] = $result;
    }

    public static function addContactEventBefore(&$arFields)
    {
		/*$err=false;
        $errormessage="";
        $logger = Logger::getLogger('addContactEventBefore', 'ofd.bitrix24/addContactEventBefore.txt');
        $logger->log(array($arFields));
        $fullname = $arFields['FULL_NAME'];
        $arrayphone = array();
        $arrayemail = array();


        if (is_array($arFields['FM']['PHONE'])) {
            foreach ($arFields['FM']['PHONE'] as $value) {
                $arrayphone[] = $value['VALUE'];
            }
        } else {
            $arrayphone[] = $arFields['FM']['PHONE']['n1']['VALUE'];
        }


        if (is_array($arFields['FM']['EMAIL'])) {
            foreach ($arFields['FM']['EMAIL'] as $value) {
                $arrayemail[] = $value['VALUE'];
            }
        } else {
            $arrayemail[] = $arFields['FM']['EMAIL']['n1']['VALUE'];
        }
        //echo "<div class='olegnizamov' style='display:none'> ";
        $resultanswer=ContactBitrixBpmMerge::FindDUBLESUsingFIOTELEMAIL($fullname,$arrayphone,$arrayemail);
        //echo "</div>";
        $errormessage="";
        if (!empty($resultanswer))
        {
            $resulttext="";
            foreach($resultanswer as $resultanswervalue)
            {
                //$resulttext.= $resultanswervalue." ";
            }

            $err=true;
            $errormessage.="Данный контакт является дубликатом!<br>";
        }

        if($err)
        {
            $logger->log(array("errormessage  ".$errormessage));
            $arFields['RESULT_MESSAGE']=$errormessage;
            $arFields['EXIST_CONTACT'] = $resultanswer; //GK

            global $APPLICATION;
            $APPLICATION->ThrowException($errormessage); return false;
}*/

    }

    public static function updateContragent(&$arFields)
    {
        //выбранное значение NDA
        if(!empty($arFields['UF_NDA'])){
            switch ($arFields['UF_NDA']) {
                case 267:
                    $nda = 0;
                    break;
                case 268:
                    $nda = 1;
                    break;
            }
            $verifity = RestBpmBitrix::verify(null,$arFields["ID"]);
            $ndaMass = array($nda,$verifity['guid']);
            CrmBitrixBpmUpdate::ndaUpdate($ndaMass);
        }
        //pre($ndaMass);

        //exit;
        metkaColTime($arFields["ID"], CONTRAGENT_METKA_UPDATE, 1);

    }

    public static function addContactEvent(&$arFields)
    {
        if ($arFields['TYPE_ID'] != "CLIENTBPM") {
            $logger = Logger::getLogger('addContactEvent', 'ofd.bitrix24/addContactEvent.txt');
            $logger->log(array($arFields));
            $guidperson = strtolower(RestBpmBitrix::generate_guid());
            $logger->log(array("guidperson=" . $guidperson));
            if (isset($arFields['ID'])) {
                // $logger->log(array("вставка в бд"));
                $idfordb = $arFields['ID'];
                //$notinsert = "0";
                // $connection = \Bitrix\Main\Application::getConnection();
                //$sql = 'INSERT INTO connectionbpmbitrix (PersonID,IDBITRIX, GUIDBPM) VALUES '."('$notinsert','$idfordb','$guidperson')";
                // $connection->queryExecute($sql);
                $entity_id = "CRM_CONTACT";
                $uf_guid = "UF_BPMCONTACTID";
                SetUserField($entity_id, $idfordb, $uf_guid, $guidperson);
            }


            if (is_array($arFields['COMPANY_BINDINGS'])) {
                foreach ($arFields['COMPANY_BINDINGS'] as $value) {
                    $company_id = $value['COMPANY_ID'];
                    break;
                }
            }

            $logger->log(array("company_id  " . $company_id));


            /*$inncompany="";
            $dbRes = CCrmCompany::GetList(array('TITLE' => 'ASC'), array('ID' => $company_id));
            if ($arRes = $dbRes->Fetch())
            {
                $inncompany=$arRes['UF_CRM_INN'];
            }
            $logger->log(array("inncompany  ".$inncompany));*/

            $guidcompany = GetUserField("CRM_COMPANY", $company_id, "UF_CRM_COMPANY_GUID");


            $id = "";
            $name = "";
            $mobilephone = "";
            $email = "";
            if (isset($arFields['ID'])) {
                $id = $arFields['ID'];

            }

            if (isset($arFields['FULL_NAME'])) {
                $name = $arFields['FULL_NAME'];
            }

            if ($arFields['HAS_EMAIL']) {
                if (isset($arFields['FM']['EMAIL']['n1']['VALUE'])) {
                    $email = $arFields['FM']['EMAIL']['n1']['VALUE'];
                }
            }

            if ($arFields['HAS_PHONE']) {
                if (isset($arFields['FM']['PHONE']['n1']['VALUE'])) {
                    $mobilephone = $arFields['FM']['PHONE']['n1']['VALUE'];
                }
            }
            $logger->log(array("ID=" . $id . "//GUIDCOMPANY=" . $guidcompany . "//NAME=" . $name . "//MOBILEPHONE=" . $mobilephone . "//EMAIl=" . $email));
            // ContactBpmBitrix::contactInsertToBMP($id,$guidperson,$inncompany,$name,$mobilephone,$email);

            session_start();
            $_SESSION["NOTSENDELEMENT"]["INSERT"][$id]['ID'] = $id;
            $_SESSION["NOTSENDELEMENT"]["INSERT"][$id]['GUID'] = $guidperson;
            $_SESSION["NOTSENDELEMENT"]["INSERT"][$id]['GUIDCOMPANY'] = $guidcompany;
            $_SESSION["NOTSENDELEMENT"]["INSERT"][$id]['NAME'] = $name;
            $_SESSION["NOTSENDELEMENT"]["INSERT"][$id]['MOBILEPHONE'] = $mobilephone;
            $_SESSION["NOTSENDELEMENT"]["INSERT"][$id]['EMAIl'] = $email;

        }

    }

    public static function updateContactEvent(&$arFields)
    {
        //usleep(300000);
        $logger = Logger::getLogger('updateContactEvent', 'ofd.bitrix24/updateContactEvent.txt');
        $logger->log(array($arFields));

        $result = CCrmContact::GetListEx(array(), array('=ID' => $arFields['ID'], 'CHECK_PERMISSIONS' => 'N'), false, false, array('*', 'UF_*'));
        $fields = is_object($result) ? $result->Fetch() : null;

        if (isset($fields['UF_BPMCONTACTID']) && ($arFields['TYPE_ID'] != "CLIENTBPM")) {
            $name = "";
            $mobilephone = "";
            $email = "";
            $id = "";
            $guidcontact = $fields['UF_BPMCONTACTID'];
            if (isset($arFields['ID'])) {
                $id = $arFields['ID'];
            }
            if (isset($arFields['FULL_NAME'])) {
                $name = $arFields['FULL_NAME'];
            }

            if (is_array($arFields['FM']['EMAIL'])) {
                foreach ($arFields['FM']['EMAIL'] as $value) {
                    $email = $value['VALUE'];
                    break;
                }
            }

            if (is_array($arFields['FM']['PHONE'])) {
                foreach ($arFields['FM']['PHONE'] as $value) {
                    $mobilephone = $value['VALUE'];
                    break;
                }
            }
            $logger->log(array("ID=" . $id . "guidcontact=" . $guidcontact . "//NAME=" . $name . "//MOBILEPHONE=" . $mobilephone . "//EMAIl=" . $email));
            //ContactBpmBitrix::contactUpdateToBMP($id,$guidcontact,$name,$mobilephone,$email);
            session_start();
            $_SESSION["NOTSENDELEMENT"]["UPDATE"][$id]['ID'] = $id;
            $_SESSION["NOTSENDELEMENT"]["UPDATE"][$id]['GUID'] = $guidcontact;
            $_SESSION["NOTSENDELEMENT"]["UPDATE"][$id]['NAME'] = $name;
            $_SESSION["NOTSENDELEMENT"]["UPDATE"][$id]['MOBILEPHONE'] = $mobilephone;
            $_SESSION["NOTSENDELEMENT"]["UPDATE"][$id]['EMAIl'] = $email;

        }

    }

    public static function addContragent(&$arFields){
        if(empty($arFields["UF_CRM_COMPANY_GUID"])){
            $arFields["UF_CRM_COMPANY_GUID"] = RestBpmBitrix::generate_guid();
			SetUserField('CRM_COMPANY', $arFields["ID"], 'UF_CRM_COMPANY_GUID', $arFields["UF_CRM_COMPANY_GUID"]);
        }
        if($arFields["COMPANY_TYPE"] == 'PARTNER' || $arFields["COMPANY_TYPE"] == '2')
        {
            if(empty($arFields["UF_RATE"])) //GK
            {
                $arFields["UF_RATE"] = 109; //30%;
                SetUserField('CRM_COMPANY', $arFields["ID"], 'UF_RATE', $arFields["UF_RATE"]);
            }
        }
        if(empty($arFields["UF_STATUS"])) //GK
        {
            $arFields["UF_STATUS"] = 265;//Потенциальный //264;//Действующий
            SetUserField('CRM_COMPANY', $arFields["ID"], 'UF_STATUS', $arFields["UF_STATUS"]);
        }
        $resID = array(
            "id" => $arFields["ID"],
            "fulname"=> $arFields["TITLE"],
            "inn"=> $arFields["UF_CRM_INN"],
            "kpp"=> $arFields["UF_CRM_KPP"],
            "guid"=>$arFields["UF_CRM_COMPANY_GUID"],
            "okpo"=> '',
            "oktmo"=>'',
            "ogrnip"=>$arFields["UF_CRM_OGRN_API"]
        );

        writeArrayInFile($resID, "BPM_".$arFields["ID"]);
        if(empty($_SESSION["ADD_CONTR_BPM"][$arFields["ID"]]) and strlen($arFields["UF_CRM_INN"]) != 15){
            $path = "/crm/company/index.php";            
            $rez = postData($arFields["ID"], $path);
        }
        if(strlen($arFields["UF_CRM_INN"]) != 15){
            session_start();
            $_SESSION["ADD_CONTR_BPM"][$arFields["ID"]] = $arFields["ID"];
        }


        $logger = Logger::getLogger('ContragentEvenT', 'ofd.bitrix24/contragent/ContragentEvenT.txt');
        $logger->log($resID);
    }

    public static function addContragentBefor(&$arFields){

        if(empty($arFields["UF_CRM_INN"])){
            $logger = Logger::getLogger('ContragentB','ofd.bitrix24/contragent/ContragentB.log');
            $logger->log($arFields);
            return false;
        }
        $inn = $arFields["UF_CRM_INN"];
        $kpp = $arFields["UF_CRM_KPP"];
        /** Поиск компании по реквизитам ИНН и КПП */

        //Проверка на создание компании из функционала очистки реквизитов: Reports::mergeRequisite();
        if(!empty($arFields['FLAG_MERGE_REQUISITE'])){
            unset($arFields['FLAG_MERGE_REQUISITE']);
            return true;
        };

        if (empty($kpp)) {
            $res = idForInnKpp($inn);
            if(!empty($res)){
                $arFields['RESULT_MESSAGE'] = 'Ошибка контрагент с ИНН '.$arFields['INN']." уже существует"; //GK
                $arFields['EXIST_COMPANY'] = $res; //GK
                return false;
            }
        }else{
            $res = idForInnKpp($inn, $kpp);
            if(!empty($res)){
                $arFields['RESULT_MESSAGE'] = 'Ошибка контрагент с ИНН '.$arFields['INN']." и КПП ". $arFields['KPP'].
                    " уже существует"; //GK
                $arFields['EXIST_COMPANY'] = $res; //GK
                return false;
            }
        }
        if (strlen($inn) == 10 and empty($kpp)){
            //return false;
        }

    }
}