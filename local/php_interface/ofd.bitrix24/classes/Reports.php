<?php
use Bitrix\Main\CUserTypeEntity,
    Bitrix\Main\Loader,
    Bitrix\Highloadblock\HighloadBlockTable as HLBT,
    \Bitrix\Main\Application;

Loader::includeModule("highloadblock");
Loader::includeModule("iblock");
Loader::includeModule("crm");
Loader::includeModule("sale");

class Reports
{
    const ID_COMPANY_ESK = 55060; //ЭСК
    const ID_COMPANY_IS = 6426; //ИНТЕГРАЦИОННЫЕ СИСТЕМЫ
    const ID_COMPANY_RJe = 146137; //РУСЭКСПЕРТИЗА

    const ID_TEMPlATE_INVOICE_ESK = 1; //ЭСК
    const ID_TEMPlATE_INVOICE_IS = 9; //ИНТЕГРАЦИОННЫЕ СИСТЕМЫ
    const ID_TEMPlATE_INVOICE_RJe = 10; //РУСЭКСПЕРТИЗА

    const COMPANY_TYPES = array("esk"/*, "is", "rus"*/);

    private static $listEntity = array(
        "Case" => "Tiket",
        "Contact" => "Contact",
        "Account" => "Company",
        "ConfItem" => "KKT",
        "ConfItemUser" => "KKT",
    );

    //Логирование информации по запросам BPM - Bitrix
    public static function addReportQueryBpm($arFields)
    {
        $HlBlockId = 4;
        $entity_data_class = self::GetEntityDataClass($HlBlockId);

        if (empty($entity_data_class)) {
            return false;
        }

        $result = $entity_data_class::add($arFields);

        if ($result->isSuccess()) {
            return 'ДОБАВЛЕН ' . $result->getId();
        } else {
            return 'ОШИБКА ' . implode(', ', $result->getErrors());
        }
    }

    //Подготовка данных для логирование информации по запросам BPM - Bitrix
    public static function prepareDataToReportQueryBPM($url, $statusQuery, $sendJson = "", $getJson = "", $bitrixResult = "", $ufEntity = "")
    {
        $arFields = array(
            "UF_DATE" => date('d.m.Y H:i:s'),
            "UF_TYPE" => "",
            "UF_ENTITY" => $ufEntity,
            "UF_STATUS_BPM" => $statusQuery,
            "UF_RESULT_BITRIX" => $bitrixResult,
            "UF_SEND_JSON" => $sendJson,
            "UF_GET_JSON" => $getJson,
        );

        //Определение типа запроса
        $res = explode("/", $url);
        $type = $res[count($res) - 1];
        $arFields['UF_TYPE'] = str_replace("Query", "", $type);

        //Определение сущности запроса
        $listEntity = self::$listEntity;
        if (!empty($sendJson)) {
            $data = json_decode($sendJson, true);
            if (!empty($data['RootSchemaName']) && !empty($listEntity[$data['RootSchemaName']])) {
                $arFields['UF_ENTITY'] = $listEntity[$data['RootSchemaName']];
            } elseif (!empty($data['items'][0]['RootSchemaName']) && !empty($listEntity[$data['items'][0]['RootSchemaName']])) {
                $arFields['UF_ENTITY'] = $listEntity[$data['items'][0]['RootSchemaName']];
            }
        }

        return $arFields;
    }

    //Получение всех компаний в Битриксе, включая ИНН/КПП. Доп. фильтр - период.
    public static function getAllCompanyBitrix()
    {
        global $DB;
        $arResult = [];

        $strSql = "SELECT
				b_crm_company.ID,
				b_crm_company.TITLE,
				b_crm_requisite.RQ_INN,
				b_crm_requisite.RQ_KPP,
				b_crm_requisite.ENTITY_ID,
				b_uts_crm_company.UF_CRM_COMPANY_GUID				
			  FROM b_crm_company 
			  LEFT JOIN b_crm_requisite ON b_crm_company.ID = b_crm_requisite.ENTITY_ID
			  LEFT JOIN b_uts_crm_company ON b_crm_company.ID = b_uts_crm_company.VALUE_ID	
				ORDER BY b_crm_company.ID ASC";

        $resCompany = $DB->Query($strSql, false, $err_mess . __LINE__);
        //Решить вопрос, когда у компании двое реквизитов: заполненный и пустой
        while ($arCompany = $resCompany->Fetch()) {
            $countRequisite = 0;

            if (!empty($arResult[$arCompany['ID']]['COUNT_REQUISITE'])) {
                $countRequisite = $arResult[$arCompany['ID']]['COUNT_REQUISITE'] + 1;
                $arResult[$arCompany['ID']]['COUNT_REQUISITE'] = $countRequisite;
                continue;
            } elseif (!empty($arCompany['ENTITY_ID'])) {
                $countRequisite = 1;
            }

            $arResult[$arCompany['ID']] = array(
                "ID" => $arCompany['ID'],
                "GUID" => strtolower($arCompany['UF_CRM_COMPANY_GUID']), //Регистр
                "INN" => $arCompany['RQ_INN'],
                "KPP" => $arCompany['RQ_KPP'],
                "NAME" => $arCompany['TITLE'],
                "COUNT_REQUISITE" => $countRequisite,
            );
        }

        return $arResult;
    }

    //Получение всех компаний в Битриксе, включая ИНН/КПП, Тенант и ответственного. Доп. фильтр - период.
    public static function getAllCompanyBitrixWithTenant()
    {
        global $DB;
        $arResult = [];

        $strSql = "SELECT
				b_crm_company.ID,
				b_crm_company.TITLE,
				b_crm_company.ASSIGNED_BY_ID,
				b_crm_requisite.RQ_INN,
				b_crm_requisite.RQ_KPP,
				b_crm_requisite.ENTITY_ID,
				b_uts_crm_company.UF_CRM_COMPANY_GUID,				
				b_uts_crm_company.UF_ID_PROD,				
				b_uts_crm_company.UF_TENANT,
				b_user.LAST_NAME	
			  FROM b_crm_company 
			  LEFT JOIN b_crm_requisite ON b_crm_company.ID = b_crm_requisite.ENTITY_ID
			  LEFT JOIN b_user ON b_crm_company.ASSIGNED_BY_ID = b_user.ID
			  LEFT JOIN b_uts_crm_company ON b_crm_company.ID = b_uts_crm_company.VALUE_ID	
				ORDER BY b_crm_company.ID ASC";

        $resCompany = $DB->Query($strSql, false, $err_mess . __LINE__);
        //Решить вопрос, когда у компании двое реквизитов: заполненный и пустой

        $numberBitrixCompanies = $resCompany->SelectedRowsCount();

        while ($arCompany = $resCompany->Fetch()) {

            if (!empty($arCompany['UF_ID_PROD'])) {
                $arResult[$arCompany['UF_ID_PROD']] = array(
                    "ID" => $arCompany['ID'],
                    "GUID" => strtolower($arCompany['UF_CRM_COMPANY_GUID']), //Регистр
                    "ID_PROD" => $arCompany['UF_ID_PROD'],
                    "INN" => $arCompany['RQ_INN'],
                    "KPP" => $arCompany['RQ_KPP'],
                    "NAME" => $arCompany['TITLE'],
                    "ASSIGNED_BY_ID" => $arCompany['ASSIGNED_BY_ID'],
                    "UF_TENANT" => $arCompany['UF_TENANT'],
                    "LAST_NAME" => $arCompany['LAST_NAME'],
                );
            } else{
                $arResult[$arCompany['RQ_INN'].'_'.$arCompany['RQ_KPP'].'_'.$arCompany['UF_TENANT']] = array(
                    "ID"              => $arCompany['ID'],
                    "GUID"            => strtolower($arCompany['UF_CRM_COMPANY_GUID']), //Регистр
                    "ID_PROD"         => $arCompany['UF_ID_PROD'],
                    "INN"             => $arCompany['RQ_INN'],
                    "KPP"             => $arCompany['RQ_KPP'],
                    "NAME"            => $arCompany['TITLE'],
                    "ASSIGNED_BY_ID"  => $arCompany['ASSIGNED_BY_ID'],
                    "UF_TENANT"       => $arCompany['UF_TENANT'],
                    "LAST_NAME"       => $arCompany['LAST_NAME'],
                );
            }
        }

        $arResult["COUNT_ALL_COMPANIES"] = $numberBitrixCompanies;

        return $arResult;
    }

    //Получение всех компаний в Битриксе, включая ИНН/КПП, Тенант и ответственного.
    public static function getAllCompaniesFromProd()
    {
        $arOrder = array("SORT" => "ASC");
        $arFilter = array("IBLOCK_CODE" => "import_company");
        $arSelectFields = array(
            "ID",
            "ACTIVE",
            "NAME",
            "PROPERTY_INN",
            "PROPERTY_KPP",
            "PROPERTY_ASSIGNED_LAST_NAME",
            "PROPERTY_ID_PROD",
            "PROPERTY_COMPANY_TYPE"
        );
        $rsElements = CIBlockElement::GetList(
            $arOrder,
            $arFilter,
            false,
            false,
            $arSelectFields
        );

        $arResult = [];
        while ($arElement = $rsElements->GetNext()) {
            $arResult[$arElement["PROPERTY_ID_PROD_VALUE"]] = $arElement;
        }

        return $arResult;
    }

    //Сравнение компаний из ПРОд и Битрикса. Возвращает массив с разбиением покатегориям
    public static function compareArrayCompanyProdAndBitrix($arrayBitrix, $arrayProd)
    {
        $el = new CIBlockElement();
        $arResultByCategory = array(
            "COUNT_COMPANY_BITRIX" => $arrayBitrix["COUNT_ALL_COMPANIES"],
            //Общее кол-во компаний в Bitrix (статистика)
            "COUNT_COMPANY_PROD" => count($arrayProd),
            //Общее кол-во компаний в BPM (статистика)
            "COUNT_FULL_COPY_BITRIX_PROD" => 0,
            //Одинаковых контрагентов в BPM - Bitrix (статистика)
            "PROD_BITRIX_DIFF_INN_OR_KPP" => [
                [
                    "ID_PROD",
                    "INN_PROD",
                    "KPP_PROD",
                    "ID_COMPANY",
                    "INN",
                    "KPP"
                ]
            ],
            //Компании, которые отличаются по ИНН/КПП
            "PROD_BITRIX_DIFF_TENANT_OR_ASSIGNED" => [
                [
                    "ID_PROD",
                    "ID_BITRIX",
                    "TENANT_PROD",
                    "TENANT_BITRIX",
                ]
            ],
            //Компании, которые обработаются при запуске быстрой обработки
            "FAST_UPDATE_ELEMENTS" => [
                [
                    "ID_ELEM",
                    "ID_PROD",
                    "INN",
                    "KPP",
                ]
            ],
            //Компании, которые отличаются по Тенант или Ответственному
        );

        foreach ($arrayProd as $key => $val) {

            $arTenantIDs = array(
                "CUSTOMER" => "286", //ОФД RU. Клиенты
                "3"        => "287", //ОФД RU. Клиенты, анонсировавшие расторжение
                "4"        => "288", //ОФД RU. Бывшие клиенты
                "6"        => "289", //Тензор
                "8"        => "290", //Без блокировки касс
                "PARTNER"  => "291", //Агент/Партнер
                "9"        => "INVALID", //Временные пользователи
                "13"       => "292", //1c
                "2"        => "293", //Клиент + Агент
                "10"       => "294", //Корпорации
                "16"       => "295", //Атол-онлайн
                "17"       => "296",//Старрус
            );

            $duplicateKey = $val["PROPERTY_INN_VALUE"] . '_' . $val["PROPERTY_KPP_VALUE"] . '_' . $arTenantIDs[$val["PROPERTY_COMPANY_TYPE_VALUE"]];

            if (array_key_exists($duplicateKey, $arrayBitrix)) {
                $arResultByCategory["COUNT_FULL_COPY_BITRIX_PROD"]++;
                continue;
            }

            if ( ! empty($val["PROPERTY_INN_VALUE"]) && ! empty($arrayBitrix[$key]["INN"])) {
                $innCorrect = $val["PROPERTY_INN_VALUE"] === $arrayBitrix[$key]["INN"];
            } elseif (empty($arrayBitrix[$key]["INN"])) {
                $innCorrect = true;
            } else {
                $innCorrect = false;
            }

            if (!empty($val["PROPERTY_KPP_VALUE"]) && !empty($arrayBitrix[$key]["KPP"])) {
                $kppCorrect = $val["PROPERTY_KPP_VALUE"] === $arrayBitrix[$key]["KPP"];
            } elseif (empty($arrayBitrix[$key]["KPP"])) {
                $kppCorrect = true;
            } else {
                $kppCorrect = false;
            }

            $arBitrixTenants = array(
                "286" => "Клиент",
                "287" => "Клиенты, анонсировавшие расторжение",
                "288" => "Клиенты, с которыми расторгнут договор",
                "289" => "Тензор",
                "290" => "Без блокировки касс",
                "291" => "Агент",
                "INVALID" => "Неверный тенант",
                "292" => "1С",
                "293" => "Клиент+агент",
                "294" => "Корпорация",
                "295" => "Атол-онлайн",
                "296" => "Старрус",
                "297" => "Пустой",
            );

            if (!empty($val["PROPERTY_COMPANY_TYPE_VALUE"]) &&
                !empty($arrayBitrix[$key]["UF_TENANT"] &&
                    $arTenantIDs[$val["PROPERTY_COMPANY_TYPE_VALUE"]] !== "INVALID")) {
                $tenantCorrect = $arTenantIDs[$val["PROPERTY_COMPANY_TYPE_VALUE"]] === $arrayBitrix[$key]["UF_TENANT"];
            } elseif (empty($arrayBitrix[$key]["UF_TENANT"]) ||
                $arTenantIDs[$val["PROPERTY_COMPANY_TYPE_VALUE"]] === "INVALID") {
                $tenantCorrect = true;
            } else {
                $tenantCorrect = false;
            }

            if (!$tenantCorrect) {
                $arResultByCategory["PROD_BITRIX_DIFF_TENANT_OR_ASSIGNED"][] = [
                    $key,
                    $arrayBitrix[$key]["ID"],
                    $arBitrixTenants[$arTenantIDs[$val["PROPERTY_COMPANY_TYPE_VALUE"]]],
                    $arBitrixTenants[$arrayBitrix[$key]["UF_TENANT"]]
                ];
            }

            if (!$innCorrect || !$kppCorrect) {
                $arResultByCategory["PROD_BITRIX_DIFF_INN_OR_KPP"][] = [
                    $key,
                    $val["PROPERTY_INN_VALUE"],
                    $val["PROPERTY_KPP_VALUE"],
                    $arrayBitrix[$key]["ID"],
                    $arrayBitrix[$key]["INN"],
                    $arrayBitrix[$key]["KPP"]
                ];
            }

            if ( ! empty($arrayBitrix[$key]) && $innCorrect && $kppCorrect && $tenantCorrect) {
                $arResultByCategory["COUNT_FULL_COPY_BITRIX_PROD"]++;
            }
        }

        //Ищем элементы, которые будут обработаны при запуске быстрой обработки
        foreach($arrayProd as $arVal) {
            $arCompareProd[$arVal["ID"]] = $arVal["PROPERTY_ID_PROD_VALUE"].'_'.$arVal["PROPERTY_INN_VALUE"].'_'.$arVal["PROPERTY_KPP_VALUE"];
        }

        foreach($arrayBitrix as $arVal) {
            $arCompareBitrix[$arVal["ID"]] = $arVal["ID_PROD"].'_'.$arVal["INN"].'_'.$arVal["KPP"];
        }

        $arDifferentCompare = array_diff($arCompareProd, $arCompareBitrix);
        foreach($arDifferentCompare as $elemProdID => $val){
            $arVal = explode("_", $val);
            $arResultByCategory["FAST_UPDATE_ELEMENTS"][] = [
                $elemProdID,
                $arVal[0],
                $arVal[1],
                $arVal[2]
            ];
        };

        return $arResultByCategory;
    }

    public static function getAllContactBitrix()
    {

        $arBitrixContactBitrix = [];
        $res = CCrmContact::GetList(array(), array("CHECK_PERMISSIONS" => "N"));
        while ($arRes = $res->Fetch()) {
            $keyGUID = strtoupper($arRes['UF_BPMCONTACTID']);
            $guidcompany = "";

            if (!empty($arRes['COMPANY_ID'])) {
                $guidcompany = GetGUIDCompany($arRes['COMPANY_ID']);
            }

            if (!empty($arBitrixContactBitrix[$keyGUID])) {
                $arBitrixContactBitrix[$keyGUID]['BITRIX_ID'] = $arRes['ID'];
                $count = $arBitrixContactBitrix[$keyGUID]['COUNT_CONTACT'] + 1;
                $arBitrixContactBitrix[$keyGUID]['COUNT_CONTACT'] = $count;
                continue;
            }
            $arBitrixContactBitrix[$keyGUID] = array(
                "ID_BITRIX" => $arRes['ID'],
                "GUID_BITRIX" => $arRes['UF_BPMCONTACTID'],
                "CONT_GUID" => $guidcompany,
                "GUID_BPM" => "NOTFOUND",
                "COUNT_CONTACT" => 1
            );
        }
        return $arBitrixContactBitrix;
    }

    public static function getAllContactBpm()
    {

        $tiket = array(
            'RootSchemaName' => 'Contact',
            'QueryType' => 0,
            'Columns' =>
                array(
                    'Items' =>
                        array(
                            'Id' =>
                                array(
                                    'Expression' =>
                                        array(
                                            'ColumnPath' => 'Id',
                                        ),
                                ),
                            'Account' =>
                                array(
                                    'Expression' =>
                                        array(
                                            'ColumnPath' => 'Account',
                                        ),
                                ),
                        ),
                ),
        );


        $arrTeket = QueryBpm::jsonDataBpm($tiket, BPM_URL_SELECT);
        if ($arrTeket["status"] == 403) {
            $arrTeket = QueryBpm::jsonDataBpm($tiket, BPM_URL_SELECT);
        }
        $arBitrixContactBpm = [];
        foreach ($arrTeket['success']['rows'] as $k => $val) {
            $keyGUID = strtoupper($val['Id']);
            $arBitrixContactBpm[$keyGUID] = array(
                "CONT_GUID" => $val['Account']['value'],
                "GUID_BPM" => $val['Id'],
                "ID_BITRIX" => "NOTFOUND",
            );
        }
        return $arBitrixContactBpm;
    }

    //Получение всех компаний в BPM с ИНН/КПП
    public static function getAllCompanyBpm()
    {
        $jsonData = array(
            'RootSchemaName' => 'Account', //контрагенты
            'QueryType' => 0,
            'Columns' =>
                array(
                    'Items' =>
                        array(
                            'Id' =>
                                array(
                                    'Expression' =>
                                        array(
                                            'ColumnPath' => 'Id',
                                        ),
                                ),
                            'Usraidiagent' =>
                                array(
                                    'Expression' =>
                                        array(
                                            'ColumnPath' => 'Usraidiagent',
                                        ),
                                ),
                            'UsrKPP' =>
                                array(
                                    'Expression' =>
                                        array(
                                            'ColumnPath' => 'UsrKPP',
                                        ),
                                ),
                            'Name' =>
                                array(
                                    'Expression' =>
                                        array(
                                            'ColumnPath' => 'Name',
                                        ),
                                ),
                        ),
                ),
        );

        $arResult = [];
        $limitCountQuery = 6;
        $i = 0;
        while ($i < $limitCountQuery) {
            QueryBpm::login();
            $que = QueryBpm::jsonDataBpm($jsonData, BPM_URL_SELECT);
            if ($que["status"] == 200) {
                foreach ($que['success']['rows'] as $key => $val) {
                    $arResult[$val['Id']] = array(
                        "GUID" => $val['Id'], //Регистр
                        "INN" => (!empty(trim($val['Usraidiagent']))) ? trim($val['Usraidiagent']) : null,
                        "KPP" => (!empty(trim($val['UsrKPP']))) ? trim($val['UsrKPP']) : null,
                        "NAME" => (!empty(trim($val['Name']))) ? trim($val['Name']) : null,
                    );
                }
                break;
            }
            $i++;
        }

        return $arResult;
    }

    public static function compareArrayContactBpmAndBitrix($arrayBitrix, $arrayBpm)
    {
        $arResultByCategory = array(
            "COUNT_CONTACT_BITRIX" => count($arrayBitrix), //!!!
            "COUNT_CONTACT_BPM" => count($arrayBpm), //!!!
            "COUNT_FULL_COPY_BITRIX_BPM" => 0,
            "BITRIX_NOT_IN_BPM" => [], //!!!
            "BPM_NOT_IN_BITRIX" => [], //!!!
            "BITRIX_DUBLE_GUID" => [], //!!!
            "BITRIX_AND_BPM_NOT_SAME_ACCOUNT" => 0,
        );

        $dubleGuidByCompany = [];
        foreach ($arrayBitrix as $k => $val) {
            if ($val['COUNT_CONTACT'] > 1) {
                $dubleGuidByCompany[$k] = $k;

            }
        }
        $arResultByCategory['BITRIX_DUBLE_GUID'] = $dubleGuidByCompany;
        foreach ($arrayBitrix as $key => $val) {
            if (!empty($arrayBpm[$key])) {
                $arrayBpm[$key]['ID_BITRIX'] = $val['ID_BITRIX'];
                $arResultByCategory['COUNT_FULL_COPY_BITRIX_BPM']++;

            }
            if (empty($arrayBpm[$key])) {
                $arResultByCategory['BITRIX_NOT_IN_BPM'][] = $val;
            }
        }

        foreach ($arrayBpm as $key => $val) {
            if ($val['ID_BITRIX'] == 'NOTFOUND') {
                $arResultByCategory['BPM_NOT_IN_BITRIX'][] = $val;
            }
        }
        $differentcontr = [];
        foreach ($arrayBitrix as $key => $val) {
            if (empty($arrayBpm[$key])) {
                continue;
            }
            if ($arrayBpm[$key]['CONT_GUID'] != $arrayBitrix[$key]['CONT_GUID']) {
                $arResultByCategory['BITRIX_AND_BPM_NOT_SAME_ACCOUNT']++;
            }
        }

        return $arResultByCategory;
    }

    //Сравнение компаний из BPM и Битрикса. Возвращает массив с разбиением покатегориям
    public static function compareArrayCompanyBpmAndBitrix($arrayBitrix, $arrayBpm)
    {
        $arResultByCategory = array(
            "COUNT_COMPANY_BITRIX" => count($arrayBitrix), //Общее кол-во компаний в Bitrix (статистика)
            "COUNT_COMPANY_BPM" => count($arrayBpm), //Общее кол-во компаний в BPM (статистика)
            "COUNT_FULL_COPY_BITRIX_BPM" => 0, //Одинаковых контрагентов в BPM - Bitrix (статистика)
            "BPM_BITRIX_NOT_INN_KPP_BPM" => [], //Есть в BPM/Bitrix. Отсутствует ИНН/КПП в BPM. (update BPM)
            "BPM_BITRIX_NOT_INN_KPP_BITRIX" => [], //Есть в BPM/Bitrix. Отсутствует ИНН/КПП в Bitrix. (check dublicate, update Bitrix + add requisite)
            "BITRIX_NOT_IN_BPM" => [],  //Есть в Bitrix(+ИНН), но нет в BPM. (add BPM)
            "BITRIX_NOT_IN_BPM_NOT_INN" => [],  //Есть в Bitrix(нет ИНН), но нет в BPM. (статистика)
            "BPM_NOT_IN_BITRIX" => [], //Есть в BPM, но нет в Bitrix. Если у контрагента BPM есть ИНН/КПП - забираем его в Битрикс.
            "BITRIX_NOT_GUID_ID" => [], //Компании в Bitrix без GUID_ID. (выполнить поиск компании по ИНН/КПП в BPM, если есть - то обновить компания в Битриксе. Если нет  - создать в BPM)
            "BITRIX_DUBLE_GUID" => [], //Список GUID, по которым есть дубликаты компаний в Битриксе (статистика. Ничего не делаем, игнорируем их)
            "BITRIX_INN_10_NOT_KPP" => [], //Компаний с ИНН Организаций(10) и без КПП (статистика)
            "BITRIX_INN_NOT_VALID" => [], //Компании с неккоректной длиной ИНН, отличные длины 10 и 12 символов (статистика)
            "BITRIX_REQUISITE_MORE_ONE" => [], //Компании, которые имеют более 1-го реквизита (статистика, игнорируем их)
        );

        //Поиск компаний с одинаковым GUID_ID
        $tmp = [];
        $dubleGuidByCompany = [];
        foreach ($arrayBitrix as $val) {
            if (empty($val['GUID'])) {
                continue;
            }

            if (!empty($tmp[$val['GUID']])) {
                $dubleGuidByCompany[$val['GUID']] = $val['GUID'];
            } else {
                $tmp[$val['GUID']] = true;
            }
        }
        $arResultByCategory['BITRIX_DUBLE_GUID'] = $dubleGuidByCompany;

        //Анализ контрагентов в Bitrix
        foreach ($arrayBitrix as $key => $val) {

            //Получение компаний с ИНН Организаций(10) и без КПП
            if (strlen($val['INN']) == 10 && empty($val['KPP'])) {
                $arResultByCategory['BITRIX_INN_10_NOT_KPP'][] = $val;
            }

            //Компании с неккоректной длиной ИНН
            if (strlen($val['INN']) != 10 && strlen($val['INN']) != 12 && !empty($val['INN'])) {
                $arResultByCategory['BITRIX_INN_NOT_VALID'][] = $val;
            }

            //Игнорируем компании, которые имеют более 1-го реквизита
            if ($val['COUNT_REQUISITE'] > 1) {
                $arResultByCategory['BITRIX_REQUISITE_MORE_ONE'][] = $val;
                continue;
            }

            //Игнорируем обработку компаний, которые имею дубль по GUID в Битриксе
            if (!empty($dubleGuidByCompany[$val['GUID']])) {
                continue;
            }

            //Компании в Bitrix с пустым GUID_ID.
            if (empty($val['GUID'])) {
                $arResultByCategory['BITRIX_NOT_GUID_ID'][] = $val;
                continue;
            }

            //Есть компания в BPM и Bitrix. GUID_ID одинаковый.
            if (!empty($arrayBpm[$val['GUID']])) {

                //Полная копия контрагента в Bitrix/BPM
                if ($val['INN'] == $arrayBpm[$val['GUID']]['INN'] && $val['KPP'] == $arrayBpm[$val['GUID']]['KPP']) {
                    $arResultByCategory['COUNT_FULL_COPY_BITRIX_BPM']++;
                    continue;
                }

                //ИНН/КПП есть в битриксе. Обновляем компанию в BPM
                if (!empty($val['INN'])) {
                    //update company BPM
                    $arResultByCategory['BPM_BITRIX_NOT_INN_KPP_BPM'][] = $val;
                } else { //Отсутствует ИНН/КПП в Bitrix.
                    //Если есть ИНН/КПП в BPM. Обновляем контрагента в Битриксе, после проверки на дубликат.
                    if (!empty($arrayBpm[$val['GUID']]['INN'])) {
                        //check company dublicate in Bitrix by INN/KPP
                        //update company Bitrix + add requisite
                        //$resMerge = array_merge($val, array("INN_BPM" => $arrayBpm[$val['GUID']]['INN'], "KPP_BPM" => $arrayBpm[$val['GUID']]['KPP'], "NAME_BPM" => $arrayBpm[$val['GUID']]['NAME']));
                        $resMerge = array_merge($val, array('INN_BPM' => $arrayBpm[$val['GUID']]['INN'], 'KPP_BPM' => $arrayBpm[$val['GUID']]['KPP']));
                        $arResultByCategory['BPM_BITRIX_NOT_INN_KPP_BITRIX'][] = $resMerge;
                    }
                }
            }

            //Есть компания в Битриксе с GUID_ID. Но нету в BPM.
            if (empty($arrayBpm[$val['GUID']])) {
                //add company BPM
                if (empty($val['INN'])) {
                    $arResultByCategory['BITRIX_NOT_IN_BPM_NOT_INN'][] = $val;
                } else {
                    $arResultByCategory['BITRIX_NOT_IN_BPM'][] = $val;
                }
            }
        }

        //Анализ контрагентов в BPM
        /*foreach ($arrayBpm as $key => $val){
            //Пропускаем компании Битрикс с дублями по GUID, массив $dubleGuidByCompany

            //Есть в BPM, но нет в Bitrix. Если у контрагента BPM есть ИНН/КПП - забираем его в Битрикс.
            //BPM_NOT_IN_BITRIX
        }*/

        //Выполнить поиск неопознанных объектов в BPM:
        // - Компания без ИНН/КПП
        // - Компании нету в Битриксе по Guid ID

        return $arResultByCategory;
    }

    public static function addContacttoBitrix($arData)
    {

        $res = [];
        $logger = Logger::getLogger('addContactBitrix_AJAX', 'ofd.bitrix24/addContactBitrix_AJAX.txt');
        foreach ($arData as $key => $value) {
            $logger->log($value['GUID_BPM']);
            $arrayresult = ContactBpmBitrix::SelectDatafromBPMusingGUID($value['GUID_BPM']);
            $namerecord = $arrayresult['Name'];
            $guidcontactrecord = $value['GUID_BPM'];
            $phonerecord = $arrayresult['MobilePhone'];
            $emailcontactrecord = $arrayresult['Email'];
            $new_contact_id = "";
            $ct = new CCrmContact(false);
            $arParams['HAS_EMAIL'] = 'N';
            $arParams['HAS_PHONE'] = 'N';
            $doptel = "";
            $dopemail = "";

            if (!empty($phonerecord)) {
                $arParams['FM']['PHONE'] = array(
                    'n0' => array(
                        'VALUE_TYPE' => 'MOBILE',
                        'VALUE' => $phonerecord,
                    )
                );
                $arParams['HAS_PHONE'] = 'Y';
            }
            if (!empty($emailcontactrecord)) {
                $arParams['FM']['EMAIL'] = array(
                    'n0' => array(
                        'VALUE' => $emailcontactrecord,
                    )
                );
                $arParams['HAS_EMAIL'] = 'Y';
            }

            $arParams['FULL_NAME'] = $namerecord;
            $arParams['LAST_NAME'] = $namerecord;
            //$arParams['HAS_EMAIL']='N';
            $arParams['TYPE_ID'] = 'CLIENTBPM';
            $arParams['OPENED'] = 'Y';
            $new_contact_id = $ct->Add($arParams, true, array('DISABLE_USER_FIELD_CHECK' => true));
            if ($new_contact_id) {
                $entity_id = "CRM_CONTACT";
                if (!empty($guidcontactrecord)) {
                    $uf_guid = "UF_BPMCONTACTID";
                    SetUserField($entity_id, $new_contact_id, $uf_guid, $guidcontactrecord);
                }
            }
        }
        $res['status'] = "200";
        $res['Message'] = "DONE";
        return $res;
    }

    public static function addContacttoBpm($arData)
    {
        $res = [];
        $logger = Logger::getLogger('updateToBitrix', 'ofd.bitrix24/addContacttoBpm_AJAX.txt');
        foreach ($arData as $key => $value) {
            ContactBpmBitrix::contactInsertToBmpUsingIdContact($value['ID_BITRIX']);
            $logger->log($value['ID_BITRIX']);
        }
        $res['status'] = "200";
        $res['Message'] = "DONE";
        return $res;
    }

    public static function MergeContactBitrix($arData)
    {
        $res = [];
        $logger = Logger::getLogger('updateToBitrix', 'ofd.bitrix24/MergeContactBitrix_AJAX.txt');
        $logger->log(array($arData));
        return $res;
    }

    public static function addCompanyBpm($arData)
    {
        //Подготовка массива нужного формата
        $arResult = [];
        $maxCountElement = 100;
        $currentCount = 0;
        foreach ($arData as $company) {
            $arResult[] = array(
                "id" => $company['ID'],
                "inn" => $company['INN'],
                "kpp" => $company['KPP'],
                "guid" => $company['GUID'],
            );
            $currentCount++;
            if ($currentCount == $maxCountElement) {
                break;
            }
        }

        //Send request update copmany BPM
        $res = CrmBitrixBpm::contragenAddArray($arResult);

        return $res;
    }

    //Update copmany BPM
    public static function updateCompanyBpm($arData)
    {
        $arResult = [];

        //Подготовка массива нужного формата
        foreach ($arData as $company) {
            $arResult[] = array(
                "inn" => $company['INN'],
                "kpp" => $company['KPP'],
                "guid" => $company['GUID'],
            );
        }

        //Send request update copmany BPM
        $res = CrmBitrixBpmUpdate::contragenUpdateInnKppArray($arResult);

        return $res;
    }

    public static function updateCompanyBitrix($arData)
    {
        $arResult = [
            "COUNT_SUCCESS" => 0, //Кол-во успешно обновленных компаний/реквизитов
            "COUNT_COMPANY_ISSET_REQUISITE" => 0, //Кол-во компаний c существующими реквизитами
            "COUNT_COMPANY_MORE_ONE_REQUISITE" => 0, //Кол-во компаний c реквизитами более 1-го
            "COUNT_NOT_VALID_INN" => 0, //Кол-во компаний с невалидным ИНН
            "COUNT_NOT_VALID_KPP" => 0, //Кол-во компаний с невалидным КПП
        ];

        foreach ($arData as $company) {
            $isValid = true;
            //Валидация по ИНН/КПП
            switch (strlen($company['INN_BPM'])) {
                case 10:
                    if (empty($company['KPP_BPM'])) {
                        $arResult['COUNT_NOT_VALID_KPP']++;
                        $isValid = false;
                        break;
                    }
                    break;
                case 12:
                    break;
                default:
                    $arResult['COUNT_NOT_VALID_INN']++;
                    $isValid = false;
                    break;
            }

            if ($isValid === false) {
                continue;
            }


            //Поиск существующей компании по BPM-реквизитами ИНН/КПП.
            //Компания найдена. Завершаем работу.
            //Компания НЕ найдена. Добавление/обновление реквизитов у компании в Bitrix.
            $requisite = new \Bitrix\Crm\EntityRequisite();
            $fieldsInfo = $requisite->getFormFieldsInfo();
            $select = array_keys($fieldsInfo);

            $arFilter = array(
                'RQ_INN' => $company['INN_BPM'],
                'RQ_KPP' => $company['KPP_BPM'],
                'ENTITY_TYPE_ID' => 4
            );

            $res = $requisite->getList(
                array(
                    'order' => array('SORT' => 'ASC', 'ID' => 'ASC'),
                    'filter' => $arFilter,
                    'select' => $select
                )
            );

            $reqData = $res->fetchAll();

            switch (intval(count($reqData))) {
                case 0:
                    //Update requisite
                    if ($company['COUNT_REQUISITE'] == 1) {
                        $arFilter = array(
                            'ENTITY_ID' => $company['ID']
                        );
                        $res = $requisite->getList(
                            array(
                                'order' => array('SORT' => 'ASC', 'ID' => 'ASC'),
                                'filter' => $arFilter,
                                'select' => $select
                            )
                        );
                        $reqData = $res->fetch();
                        $requisite->update($reqData['ID'], array('RQ_INN' => $company['INN_BPM'], 'RQ_KPP' => $company['KPP_BPM']));
                        $arResult['COUNT_SUCCESS']++;
                    }

                    //Add requisite
                    if ($company['COUNT_REQUISITE'] == 0) {
                        $arFieldsRequisite = array(
                            'NAME' => (strlen($company['INN_BPM']) == 10) ? "Ораганизация" : "ИП",
                            'PRESET_ID' => (strlen($company['INN_BPM']) == 10) ? 1 : 2,
                            'RQ_INN' => $company['INN_BPM'],
                            'RQ_KPP' => $company['KPP_BPM'],
                            'SORT' => 500,
                            'ENTITY_TYPE_ID' => 4,
                            'ENTITY_ID' => $company['ID'],
                        );
                        $requisite->add($arFieldsRequisite);
                        $arResult['COUNT_SUCCESS']++;
                    }
                    break;

                case 1: //Найдена компания с данными реквизитами. Игнорируем
                    $arResult['COUNT_COMPANY_ISSET_REQUISITE']++;
                    break;

                default: // Найдено 2 и более компании с указанными реквизитами. Игнорируем
                    $arResult['COUNT_COMPANY_MORE_ONE_REQUISITE']++;
                    break;
            }
        }

        return $arResult;
    }

    public static function getAllRequisite()
    {
        //Реализовать прямой запрос к БД
    }

    public static function mergeRequisite($arData)
    {
        var_dump(count($arData));//del
        var_dump($arData[0]);//del

        $CCrmInvoice = new CCrmInvoice();
        $oCompany = new CCrmCompany;
        $bank = new \Bitrix\Crm\EntityBankDetail();

        $arResult = [];
        $arResult['InvoiceManualProcessing'] = [];
        $arAllCompanyRequisite = [];

        //Получение всех реквизитов по компаниям
        $n = 0;
        foreach ($arData as $itemComapany) {
            /*
            if ($itemComapany['ID'] != 5535) {//del
                continue;//del
            }//del
            */
            $resRequisite = self::getRequisiteByCompanyID($itemComapany['ID']);

            $arRequisite = [];
            foreach ($resRequisite as $valRequisite) {
                //Удаление пустых полей
                $valRequisite = array_diff($valRequisite, array(''));
                $arRequisite[$valRequisite['ID']] = $valRequisite;
            }

            $arAllCompanyRequisite[$itemComapany['ID']] = $arRequisite;
            $n++;
            if ($n == 200) {
                break;
            }
        }

        //Merge реквизитов
        foreach ($arAllCompanyRequisite as $key => $valArrRequisite) {
            //Check two and more requisite
            if (count($valArrRequisite) <= 1) {
                continue;
            }

            $companyID = $key;
            //var_dump("Обрабатываем компанию: $companyID");//del
            $arrMergeRequisite = [];
            $mainRequisiteID = null;
            foreach ($valArrRequisite as $key2 => $itemRequisite) {
                $requisiteID = $key2;
                //var_dump("Реквизит: $requisiteID");//del

                //Главный реквизит (самый старый)
                if (empty($arrMergeRequisite)) {
                    $arrMergeRequisite = $itemRequisite;
                    $mainRequisiteID = $itemRequisite['ID'];
                    continue;
                }

                //var_dump("///////////////////////////////////////////////////////////////////////////////////////////////");//del
                //var_dump("Неактуальный реквизит");//del
                //var_dump($itemRequisite);//del

                //Реквизит является дублем Главного реквизита.
                if ($arrMergeRequisite['RQ_INN'] == $itemRequisite['RQ_INN'] && $arrMergeRequisite['RQ_KPP'] == $itemRequisite['RQ_KPP']) {
                    var_dump("Реквизит является дублем Главного реквизита. MainCompany: $companyID"); //del

                    //Merge основной информации по реквизиту
                    $arrMergeRequisite = array_merge($arrMergeRequisite, $itemRequisite);
                    $arrMergeRequisite['ID'] = $mainRequisiteID;

                    //Merge банковских реквизитов
                    //Поиск всех банковских реквизитов у неактуального реквизита и перепривязка к Главному реквизиту.
                    $arSearchBankRequisite = self::getBankDetailByRequisiteID($requisiteID);
                    foreach ($arSearchBankRequisite as $valBank) {
                        $bank->update($valBank['ID'], array('ENTITY_ID' => $mainRequisiteID));
                    }

                    //Поиск счетов компании.
                    $res = $CCrmInvoice->GetList($arOrder = Array("ID" => "ASC"), $arFilter = Array("UF_COMPANY_ID" => $companyID));
                    while ($arrInvoice = $res->fetch()) {
                        $isInvoice = self::checkBindRequisiteWithInvoice($requisiteID, $arrInvoice['ID'], $companyID);
                        //Удаляемый реквизит привязан к счету. Добавить счет в массив ручной обработки
                        if ($isInvoice) {
                            $arResult['InvoiceManualProcessing'][] = $arrInvoice['ID'];
                        }
                    }

                    //Обновление ГЛАВНОГО реквизита, используя массив $arrMergeRequisite
                    if (!empty($mainRequisiteID) && !empty($arrMergeRequisite)) {
                        self::updateRequisite($mainRequisiteID, $arrMergeRequisite);
                    }

                    //Удаление не актуального реквизита
                    self::deleteRequisiteByID($requisiteID);

                } else { //Реквизит отличный от Главного реквизита.

                    //Поиск компании по ИНН/КПП (текущая компания игнорируется)
                    $arSearchCompanyRequisite = self::getRequisiteByInnKpp($itemRequisite['RQ_INN'], $itemRequisite['RQ_KPP']);
                    unset($arSearchCompanyRequisite[$companyID]);

                    //- Компания найдена. Получен ID реквизита найденной компании
                    //- - Мердже реквизитов: неактуальный реквизит мерджится с найденным реквизитом.
                    //- - Merge банковских реквизитов
                    //- - Поиск счета, привязанного к неактуальному реквизиту.
                    //- - Перепривязка счета к найденной компании/Реквизиту.
                    //- - Добавить счет в список ручной обработки: InvoiceManualProcessing
                    //- - Удалить реквизит.
                    if (!empty($arSearchCompanyRequisite)) {
                        var_dump("COMPANY_YES. НАЙДЕН РЕКВИЗИТ ПО ИНН/КПП У ДРУГОЙ КОМПАНИИ. MainCompany: $companyID.");//del

                        $arRequisiteOne = array_shift($arSearchCompanyRequisite); //Берем 1-й реквизит(самый старый, главный)
                        $arRequisiteOne = array_diff($arRequisiteOne, array(''));//Удаляем пустрые поля
                        $searchCompanyID = $arRequisiteOne['ENTITY_ID'];
                        $searchReqiusiteID = $arRequisiteOne['ID'];
                        var_dump("SearchCompanyID: $arRequisiteOne[ENTITY_ID]");

                        //Merge основной информации по реквизиту
                        $arrMergeRequisite = array_merge($arRequisiteOne, $itemRequisite);
                        $arrMergeRequisite['ID'] = $searchReqiusiteID;
                        $arrMergeRequisite['ENTITY_ID'] = $searchCompanyID;

                        //Merge банковских реквизитов
                        //Поиск всех банковских реквизитов у неактуального реквизита и перепривязка к реквизиту найденной компании.
                        $arSearchBankRequisite = self::getBankDetailByRequisiteID($requisiteID);
                        foreach ($arSearchBankRequisite as $valBank) {
                            $bank->update($valBank['ID'], array('ENTITY_ID' => $searchReqiusiteID));
                        }

                        //Поиск счетов, которые привязаны к неактуальному реквизиту. Перепривязка счета к другой компании
                        $res = $CCrmInvoice->GetList($arOrder = Array("ID" => "ASC"), $arFilter = Array("UF_COMPANY_ID" => $companyID));
                        while ($arrInvoice = $res->fetch()) {
                            $isInvoice = self::checkBindRequisiteWithInvoice($requisiteID, $arrInvoice['ID'], $companyID);
                            if ($isInvoice) {
                                $arResult['InvoiceManualProcessing'][] = $arrInvoice['ID'];
                                $CCrmInvoice->Update($arrInvoice['ID'], array("UF_COMPANY_ID" => $searchCompanyID));
                            }
                        }

                        //Обновление реквизита у найденной компании, используя массив $arrMergeRequisite
                        if (!empty($searchReqiusiteID) && !empty($arrMergeRequisite)) {
                            self::updateRequisite($searchReqiusiteID, $arrMergeRequisite);
                        }

                        //Удаление не актуального реквизита
                        self::deleteRequisiteByID($requisiteID);


                        //[ELSE]
                        //- Компания НЕ найдена.
                        //- - Создаем компанию на основаннии данных из неактуального реквизита
                        //- - Перепривязка реквизита к созданной компании
                        //- - Поиск счета, привязанного к неактуальному реквизиту.
                        //- - Перепривязка счета к созданной компании/Реквизиту.
                        //- - Добавить счет в список ручной обработки: InvoiceManualProcessing
                        //- - Реквизит НЕ удаляем, т.к. идет его перепривязка
                    } else {
                        var_dump("COMPANY_NOT. НЕ НАЙДЕН РЕКВИЗИТ У ДРУГОЙ КОМПАНИИ. Создаем новую компанию/реквизит. MainCompany: $companyID");//del

                        if (empty($itemRequisite['RQ_INN'])) {
                            continue;
                        }

                        $guid = RestBpmBitrix::generate_guid();

                        switch ($itemRequisite['PRESET_ID']) {
                            case 1: //Организация
                                $companyName = "Организация";
                                $ogrn = (!empty($itemRequisite['RQ_OGRN'])) ? $itemRequisite['RQ_OGRN'] : "";

                                if (!empty($itemRequisite['RQ_COMPANY_FULL_NAME'])) {
                                    $companyName = $itemRequisite['RQ_COMPANY_FULL_NAME'];
                                    break;
                                }
                                if (!empty($itemRequisite['RQ_COMPANY_NAME'])) {
                                    $companyName = $itemRequisite['RQ_COMPANY_NAME'];
                                    break;
                                }
                                if (!empty($itemRequisite['NAME'])) {
                                    $companyName = $itemRequisite['NAME'];
                                    break;
                                }
                                break;
                            case 2: //ИП
                                $companyName = "ИП";
                                $ogrn = (!empty($itemRequisite['RQ_OGRNIP'])) ? $itemRequisite['RQ_OGRNIP'] : "";

                                if (!empty($itemRequisite['RQ_LAST_NAME'])) {
                                    $companyName .= " " . $itemRequisite['RQ_LAST_NAME'];
                                }
                                if (!empty($itemRequisite['RQ_FIRST_NAME'])) {
                                    $companyName .= " " . $itemRequisite['RQ_FIRST_NAME'];
                                }
                                if (!empty($itemRequisite['RQ_SECOND_NAME'])) {
                                    $companyName .= " " . $itemRequisite['RQ_SECOND_NAME'];
                                }
                                break;
                            default: //Неопределенный тип реквизита
                                continue 2;
                                break;
                        }

                        //Подготовка информации для создания компании
                        $arFieldsCompany = array(
                            "TITLE" => $companyName,
                            "UF_FULL_NAME" => $companyName,
                            "COMPANY_TITLE" => $companyName,
                            "UF_CRM_INN" => $itemRequisite['RQ_INN'],
                            "UF_CRM_KPP" => $itemRequisite['RQ_KPP'],
                            "UF_CRM_COMPANY_GUID" => $guid,
                            "UF_CRM_OGRN_API" => $ogrn,
                            "ASSIGNED_BY_ID" => (!empty($itemRequisite['CREATED_BY_ID'])) ? $itemRequisite['CREATED_BY_ID'] : 372,
                            "COMPANY_TYPE" => "CUSTOMER", //Клиенты
                            "FLAG_MERGE_REQUISITE" => true,
                        );

                        //Создаем компанию
                        if ($newCompanyID = $oCompany->Add($arFieldsCompany)) {
                            var_dump("Создали компанию: $newCompanyID");

                            //Перепривязка реквизита
                            self::updateRequisite($itemRequisite['ID'], array("ENTITY_ID" => $newCompanyID));

                            //Поиск счетов, которые привязаны к неактуальному реквизиту. Перепривязка счета к другой компании
                            $res = $CCrmInvoice->GetList($arOrder = Array("ID" => "ASC"), $arFilter = Array("UF_COMPANY_ID" => $companyID));
                            while ($arrInvoice = $res->fetch()) {
                                $isInvoice = self::checkBindRequisiteWithInvoice($requisiteID, $arrInvoice['ID'], $companyID);
                                if ($isInvoice) {
                                    $arResult['InvoiceManualProcessing'][] = $arrInvoice['ID'];
                                    $CCrmInvoice->Update($arrInvoice['ID'], array("UF_COMPANY_ID" => $newCompanyID));
                                }
                            }
                        } else {
                            var_dump("ОШИБКА создания компании: " . $oCompany->LAST_ERROR);
                        }
                    }
                }
            }
        }

        var_dump("********** arResult **********");
        $arResult['InvoiceManualProcessing'] = array_unique($arResult['InvoiceManualProcessing']);
        array2csv($arResult['InvoiceManualProcessing'], [], "/log/reports/company/InvoiceManualProcessing.csv");
        var_dump($arResult);

        //Можно добавить статистику по выполненным операциям.

        return $arResult;
    }

    //Получение всех реквизитов компании по ID компании
    private function getRequisiteByCompanyID($companyID)
    {
        if (empty($companyID)) {
            return false;
        }

        $requisite = new \Bitrix\Crm\EntityRequisite();
        $fieldsInfo = $requisite->getFormFieldsInfo();
        $select = array_keys($fieldsInfo);

        $arFilter = array(
            'ENTITY_ID' => $companyID,
            'ENTITY_TYPE_ID' => 4
        );

        $res = $requisite->getList(
            array(
                'order' => array('SORT' => 'ASC', 'ID' => 'ASC'),
                'filter' => $arFilter,
                'select' => $select
            )
        );

        $arRequisite = $res->fetchAll();

        return $arRequisite;
    }

    //Обновление реквизита
    private function updateRequisite($idRequisite, $arFields)
    {
        if (empty($idRequisite)) {
            return false;
        }

        $requisite = new \Bitrix\Crm\EntityRequisite();
        $res = $requisite->update($idRequisite, $arFields);

        return $res;
    }

    //Удаление реквизита по ID
    private function deleteRequisiteByID($requisiteID)
    {
        if (empty($requisiteID)) {
            return false;
        }

        $requisite = new \Bitrix\Crm\EntityRequisite();
        $res = $requisite->delete($requisiteID);

        return $res;
    }

    //Проверка привязки счета к реквизиту компании.
    private function checkBindRequisiteWithInvoice($requisiteID, $invoiceID, $companyID)
    {
        if (empty($requisiteID) || empty($invoiceID) || empty($companyID)) {
            return false;
        }

        $requisiteEntityList = array(
            "0" => array(
                "ENTITY_TYPE_ID" => 5,
                "ENTITY_ID" => $invoiceID, //ID счета
            ),
            "1" => array(
                "ENTITY_TYPE_ID" => 4,
                "ENTITY_ID" => $companyID, //ID компании
            )
        );

        $requisite = new \Bitrix\Crm\EntityRequisite();
        $requisiteInfoLinked = $requisite->getDefaultRequisiteInfoLinked($requisiteEntityList);
        if (!empty($requisiteInfoLinked['REQUISITE_ID']) && $requisiteInfoLinked['REQUISITE_ID'] == $requisiteID) {
            return true;
        }

        return false;
    }

    //Поиск реквизитов по ИНН/КПП
    private function getRequisiteByInnKpp($inn, $kpp)
    {
        if (empty($inn)) {
            return [];
        }

        $arResult = [];

        $requisite = new \Bitrix\Crm\EntityRequisite();
        $fieldsInfo = $requisite->getFormFieldsInfo();
        $select = array_keys($fieldsInfo);

        $arFilter = array(
            'RQ_INN' => $inn,
            'RQ_KPP' => $kpp,
            'ENTITY_TYPE_ID' => 4
        );

        $res = $requisite->getList(
            array(
                'order' => array('SORT' => 'ASC', 'ID' => 'ASC'),
                'filter' => $arFilter,
                'select' => $select
            )
        );

        //Реквизиты найдены
        $row = $res->fetchAll();
        foreach ($row as $item) {
            $arResult[$item['ENTITY_ID']] = $item;
        }

        return $arResult;
    }

    //Получение банковских реквизитов по ID реквизита
    private function getBankDetailByRequisiteID($requisiteID)
    {
        if (empty($requisiteID)) {
            return [];
        }

        $bank = new \Bitrix\Crm\EntityBankDetail();
        $dbRes = $bank->getList(array(
            'filter' => array('ENTITY_ID' => $requisiteID)
        ));
        $rowsd = $dbRes->fetchAll();

        return $rowsd;
    }

    //функцию получения экземпляра класса HighLoad инфоблока
    private function GetEntityDataClass($HlBlockId)
    {
        if (empty($HlBlockId) || $HlBlockId < 1) {
            return false;
        }
        $hlblock = HLBT::getById($HlBlockId)->fetch();
        $entity = HLBT::compileEntity($hlblock);
        $entity_data_class = $entity->getDataClass();

        return $entity_data_class;
    }

    public static function prepareDataToSaveCsv($array, $headerKeyAutoGenerate = false, $path)
    {
        if (!is_array($array)) {
            return false;
        }

        //Generate header
        $arHeader = [];
        if ($headerKeyAutoGenerate) {
            reset($array);
            $first = current($array);

            if (is_array($first)) {
                foreach ($first as $key => $val) {
                    $arHeader[] = $key;
                }
            }
        }

        $res = array2csv($array, $arHeader, $path);

        return $res;
    }

    //Получение всех счетов в Битриксе.
    public static function getAllInvoiceBitrix()
    {
        global $DB;
        $arResult = [];
        $fieldNameUFsummFrom1c = GetUserFieldIDByXMLID("UF_INVOICE_SUMM_FROM_1C", "FIELD_NAME");
        /*Массив, где хранятся соответствие статусов из Битрикс24 и 1с*/
        $arConformityStatuses = array(
            "S" => "НеОплачен", // Отправлен клиенту
            "A" => "НеОплачен", // Ждет оплаты
            "P" => "НеОплачен", // Оплачен
            "D" => "НеОплачен", // Отклонён
            "Y" => "НеОплачен", // Удален
        );

        $strSql = "SELECT 
                b_sale_order.ID as 'ORDER_ID', 
                b_sale_order.STATUS_ID, 
                b_sale_order.ACCOUNT_NUMBER, 
                b_sale_order.PRICE as 'INVOICE_PRICE',
                b_sale_order.DATE_BILL, 
                b_sale_order.DATE_PAY_BEFORE, 
                b_sale_order.PAY_VOUCHER_DATE, 
                b_sale_order.PAY_VOUCHER_NUM, 
                b_sale_order.DATE_UPDATE, 
                b_sale_basket.ID, 
                b_sale_basket.ORDER_ID as 'ORDER_ID_LINK',
                b_sale_basket.PRODUCT_ID, 
                b_sale_basket.PRICE, 
                b_sale_basket.VAT_INCLUDED, 
                b_sale_basket.QUANTITY, 
                b_sale_basket.NAME, 
                b_sale_basket.DISCOUNT_PRICE, 
                b_sale_basket.VAT_RATE,
                b_uts_order.UF_MYCOMPANY_ID,
                b_uts_order.$fieldNameUFsummFrom1c
              FROM b_sale_order
			    LEFT JOIN b_sale_basket ON b_sale_order.ID = b_sale_basket.ORDER_ID
	            LEFT JOIN b_uts_order ON b_sale_order.ID = b_uts_order.VALUE_ID 
			  WHERE b_uts_order.UF_MYCOMPANY_ID = '".self::ID_COMPANY_ESK."'
              ORDER BY b_sale_order.ID DESC";

        $resInvoice = $DB->Query($strSql, false, $err_mess . __LINE__);
        $countInvoice = 0;
        $arPaySystemOrg = array(
            self::ID_COMPANY_ESK => 'esk', //ЭСК
            self::ID_COMPANY_IS => 'is', //ИНТЕГРАЦИОННЫЕ СИСТЕМЫ
            self::ID_COMPANY_RJe => 'rus', //РУСЭКСПЕРТИЗА
        );
        $arCountByOrg = array(
            "esk" => 0,
            "is" => 0,
            "rus" => 0,
        );
        $showIndex = 0;
        while ($arInvoice = $resInvoice->Fetch()) {
            $accountNumber = trim(preg_replace('| +|', ' ', $arInvoice['ACCOUNT_NUMBER']));
            //Проверяем, есть ли в Номере счета информация о годе в скобках. Например: 00БП-003222 (2018)
            if (!preg_match("/\([0-9]{4}\)/", $accountNumber)) {
                $billYear = date('Y', strtotime($arInvoice['DATE_BILL']));
                $accountNumber .= ' (' . $billYear . ')';
            }
            $orgType = $arPaySystemOrg[$arInvoice['UF_MYCOMPANY_ID']];
            if (!isset($arResult[$orgType][$accountNumber])) {
                $arResult[$orgType][$accountNumber] = array(
                    "ID" => $arInvoice['ORDER_ID'],
                    "STATUS_ID" => $arInvoice['STATUS_ID'],
                    "ACCOUNT_NUMBER" => $arInvoice['ACCOUNT_NUMBER'],
                    "PRICE" => number_format($arInvoice['INVOICE_PRICE'], 2, '.', ''),
                    "DATE_UPDATE" => $arInvoice['DATE_UPDATE'],
                    "PRICE_FROM_1C" => number_format($arInvoice[$fieldNameUFsummFrom1c], 2, '.', ''),
                    "DATE_BILL" => (strtotime($arInvoice['DATE_BILL']) > 0) ? date('d.m.Y', strtotime($arInvoice['DATE_BILL'])) : '',
                    "DATE_PAY_BEFORE" => (strtotime($arInvoice['DATE_PAY_BEFORE']) > 0) ? date('d.m.Y', strtotime($arInvoice['DATE_PAY_BEFORE'])) : '',
                    "DATE_PAY" => (strtotime($arInvoice['PAY_VOUCHER_DATE']) > 0) ? date('d.m.Y', strtotime($arInvoice['PAY_VOUCHER_DATE'])) : '',
                    "PAY_VOUCHER_NUM" => $arInvoice['PAY_VOUCHER_NUM'],
                    "PRODUCTS" => array()
                );
                $countInvoice++;
                $arCountByOrg[$orgType]++;
            }
            $vatRate = number_format($arInvoice['VAT_RATE'], 2, '.', '');
            $vatSumm = $arInvoice['PRICE'] * intval($arInvoice['QUANTITY']) / (1 + $vatRate) * $vatRate;
            $vatSumm = number_format($vatSumm, 2, '.', '');
            $arResult[$orgType][$accountNumber]["PRODUCTS"][] = array(
                "PRODUCT_ID" => $arInvoice['PRODUCT_ID'],
                "NAME" => $arInvoice['NAME'],
                "PRICE" => number_format($arInvoice['PRICE'], 2, '.', ''),
                "QUANTITY" => intval($arInvoice['QUANTITY']),
                "VAT_RATE" => $vatRate,
                "VAT_SUMM" => $vatSumm,
            );
            $showIndex++;
        }
        $arResult["TOTAL_INVOICE_COUNT"] = $countInvoice;
        $arResult["arCountByOrg"] = $arCountByOrg;

        return $arResult;
    }

    //Получение счетов из 1с
    public static function getAllInvoice1с()
    {
        $logger = Logger::getLogger('getAllInvoice1с');
        define("PATH_TO_FILES_1C", "/upload/1c_invoice/");
        $arResult = [];

        $row = 1;
        $arOrgTypes = array("esk"/*, "rus", "is"*/);
        $arFileTemp = array(
            "invoice" => "_schet",
            "products" => "_product");
        $arNeededStatus = array();
        $arNeededProducts = array();
        $arExtKeyAccNum = array(); //Хранится соответствие УникальныйИдентификатор => Номер  полей счета
        $totalInvoiceCount = 0;
        $currentDate = new DateTime("now");
        $status = true;
        $arError = array();
        foreach ($arOrgTypes as $orgTypeName) {
            foreach ($arFileTemp as $fileType => $fileTemp) {
                $filePath = $_SERVER["DOCUMENT_ROOT"] . PATH_TO_FILES_1C . $orgTypeName . $fileTemp . ".csv";
                if (file_exists($filePath)) {
                    $dateFile = new DateTime(date("d.m.Y H:i:s", filemtime($filePath)));
                    $diff = $currentDate->diff($dateFile);
                    $hours = $diff->h + ($diff->days * 24);
                    if($hours == 0){
                        if (($handle = fopen($filePath, "r")) !== FALSE) {
                            if ($fileType == "invoice") {
                                /*Описание массива:
                               *_invoice.csv
                                 * 0 => Номер;
                                 * 1 => Дата;
                                 * 2 => Контрагент.Папка;
                                 * 3 => Контрагент;
                                 * 4 => Контрагент.ИНН;
                                 * 5 => Контрагент.КПП;
                                 * 6 => СуммаДокумента;
                                 * 7 => Б_Идентификатор;     //ID счета в Битрикс24
                                 * 8 => Б_НомерВерсии;
                                 * 9 => Б_ДатаДокумента;    //Дата выставления счета в Битрикс24
                                 * 10 => Ответственный;
                                 * 11 => Статус;
                                 * 12 => ДополнительныйСтатус;
                                 * 13 => Организация;
                                 * 14 => УникальныйИдентификатор;
                                 * 15 => СрокОплаты;
                                 * 16 => ППВНомер;
                                 * 17 => ППВДата;
                                 *
                                 * Значения статусов
                                 * "N" => Черновик
                                 * "S" => Отправлен клиенту
                                 * "A" => Ждет оплаты
                                 * "P" => Оплачен
                                 * "D" => Отклонён
                                 * "Y" => Удален
                                 */
                                $countInvoice = 0;
                                while (($arInvoice = fgetcsv($handle, 10000, ";")) !== FALSE) {
                                    $accountNumber = trim(preg_replace('| +|', ' ', $arInvoice[0]));
                                    /*if($arInvoice[0] == 'БИ-549710  '){
                                        echo '$accountNumber = "'.$accountNumber.'"<br />';
                                    }*/
                                    $statusText = $arInvoice[11];
                                    if (!preg_match("/\([0-9]{4}\)/", $accountNumber)) {
                                        $billYear = date('Y', strtotime($arInvoice[1]));
                                        $accountNumber .= ' (' . $billYear . ')';
                                    }
                                    $arExtKeyAccNum[$arInvoice[14]] = $accountNumber;
                                    $price = str_replace(',', '.', $arInvoice[6]);
                                    if (empty($price)) {
                                        $price = 0;
                                    }
                                    $arResult[$orgTypeName][$accountNumber] = array(
                                        "ID" => $arInvoice[7],
                                        "STATUS_ID" => $statusText,
                                        "ACCOUNT_NUMBER" => $arInvoice[0],
                                        "PRICE" => number_format($price, 2, '.', ''),
                                        "DATE_BILL" => !empty($arInvoice[1]) ? date('d.m.Y', strtotime($arInvoice[1])) : '',
                                        "DATE_PAY_BEFORE" => !empty($arInvoice[15]) ? date('d.m.Y', strtotime($arInvoice[15])) : '',
                                        "DATE_PAY" => !empty($arInvoice[17]) ? date('d.m.Y', strtotime($arInvoice[17])) : '',
                                        "PAY_VOUCHER_NUM" => $arInvoice[16],
                                        "PRODUCTS" => array()
                                    );
                                    $countInvoice++;
                                    $totalInvoiceCount++;
                                }
                                $arResult[$orgTypeName]["INVOICE_COUNT"] = $countInvoice;
                            } elseif ($fileType == "products") {
                                /*
                               *_product.csv
                                 * 0 => УникальныйИдентификатор;
                                 * 1 => НомерСтроки;
                                 * 2 => Номенклатура.Наименование;
                                 * 3=> ;
                                 * 4 => Количество;
                                 * 5 => Цена;
                                 * 6 => Сумма;
                                 * 7 => ПроцентСкидки;
                                 * 8 => СуммаСкидки;
                                 * 9 => СтавкаНДС;
                                 * 10 => СуммаНДС;
                                 * 11 => Номенклатура.Б_Идентификатор;
                                 * 12 => Номер;
                                 * 13 => Дата;
                                 * 14 => Номенклатура.НаименованиеПолное;
                                 * 15 => Номенклатура.УникальныйИдентификатор;
                                 */
                                while (($arProducts = fgetcsv($handle, 10000, ";")) !== FALSE) {
                                    preg_match('/[a-z0-9]{8}\-[a-z0-9]{4}\-[a-z0-9]{4}\-[a-z0-9]{4}\-[a-z0-9]{12}/', $arProducts[0], $matches);
                                    $schetCode = $matches[0];
                                    $accountNumber = $arExtKeyAccNum[$schetCode];
                                    if (!empty($accountNumber)) {
                                        $vateRate = $arProducts[9];
                                        $vateRate = number_format($vateRate / 100, 2, '.', '');
                                        $vateSumm = str_replace(',', '.', $arProducts[10]);
                                        $vateSumm = number_format($vateSumm, 2, '.', '');
                                        $price = str_replace(',', '.', $arProducts[5]);
                                        $arResult[$orgTypeName][$accountNumber]["PRODUCTS"][] = array(
                                            "EXT_ID" => $arProducts[15],
                                            "PRODUCT_ID" => '',
                                            "NAME" => $arProducts[2],
                                            "PRICE" => number_format($price, 2, '.', ''),
                                            "QUANTITY" => intval($arProducts[4]),
                                            "VAT_RATE" => $vateRate,
                                            "VAT_SUMM" => $vateSumm,
                                        );
                                    }
                                }
                            }
                        } else {
                            $arResult["ERROR"] = "Ошибка открытия файла $filePath";
                        }
                    } else {
                        $status = false;
                        if(empty($arError)){
                            $arError[] = "Устаревшие данные по след. счетам из 1с:";
                        }
                        $arError[] = sizeOf($arError).". ".$orgTypeName.$fileTemp.".csv файл обновлен более $hours ч. назад";
                    }
                }
            }
        }
        if($status) {
            $arResult["TOTAL_INVOICE_COUNT"] = $totalInvoiceCount;
        } else {
            $arResult["status"] = $status;
            $arResult["error"] = $arError;
        }
        return $arResult;
    }

    //Сравнение счетов из Битрикса и 1с. Возвращает массив с разбиением по категориям
    public static function compareInvoiceFrom1cAndBitrix($arBitrixInvoices, $ar1cInvoices)
    {
        $logger = Logger::getLogger('compareInvoiceFrom1cAndBitrix');
        $arResultByCategory = array(
            "COUNT_INVOICE_BITRIX" => $arBitrixInvoices["TOTAL_INVOICE_COUNT"], //Общее количество счетов в Bitrix
            "COUNT_INVOICE_1C" => 0, //Общее количество счетов в 1C
            "COUNT_FULL_COPY" => 0, //Одинаковых счетов в Битриксе и 1С
            "COUNT_INVOICE_NOT_MATCH_MAIN_PARAM" => array(), //Cчета, которые различаются по значением полей. (номер счета совпадает)
            "COUNT_INVOICE_NOT_MATCH_PRODUCTS_PARAM" => array(), //Cчета, которые различаются по значениям товарных позиций (номер счета и основные параметры счета (пункт выше) совпадает).
            "INVOICE_ID_NOT_MATCH_PRODUCTS_PARAM" => array(), //ID счетов, которые различаются по значениям товарных позиций (номер счета и основные параметры счета (пункт выше) совпадает).
            "COUTN_BITRIX_INVOICE_NO_MATCH_1C" => array(), //Кол-во счетов, которые есть в Б24, но нету в 1С
            "COUTN_1C_INVOICE_NO_MATCH_BITRIX" => array(), //Кол-во счетов, которые есть в 1С, но нету в Б24
            "COUTN_BITRIX_INVOICE_NO_MATCH_1C_IN_STATUS_DRAFT" => array(), //Кол-во счетов, которые есть в Б24, но нету в 1С, в статусе ЧЕРНОВИК
            "COUTN_BITRIX_INVOICE_NO_MATCH_1C_IN_STATUS_DEL" => array(), //Кол-во счетов, которые есть в Б24, но нету в 1С, в статусе УДАЛЕН
            "COUTN_BITRIX_INVOICE_NO_MATCH_1C_IN_STATUS_CANCELED" => array(), //Кол-во счетов, которые есть в Б24, но нету в 1С, в статусе отменен

        );

        /*
         * Сравниваем массив со счетами Битрикс и 1с.
         * 1. сравниваем массивы $arBitrixInvoices и $ar1cInvoices по ключам (номер счета)
         * 2. По найденным ключам сверям остальные параметры счета (кроме товаров)
         * 3. Сравниваем товарные позиции счетов.
         *
         */

        foreach (self::COMPANY_TYPES as $orgName) {
            $arCompareBitrix = $arBitrixInvoices[$orgName];
            $arCompare1c = $ar1cInvoices[$orgName];
            $arResultByCategory["COUNT_INVOICE_1C"] = $arCompare1c["INVOICE_COUNT"];
            unset($arCompare1c["INVOICE_COUNT"]);
            //Ищем полные совпадения по ключам
            $arItemsFound = array_intersect_key($arCompareBitrix, $arCompare1c);
            $arInvoiceNumber = array_keys($arItemsFound);
            $statusConsillience = false;
            foreach ($arInvoiceNumber as $invoiceNum) {
                //сравниваем статусы счетов
                $tempStatusInvoiceBitrix = $arCompareBitrix[$invoiceNum]["STATUS_ID"];
                $tempStatusInvoice1c = $arCompare1c[$invoiceNum]["STATUS_ID"];
                if (self::checkingInvoiceStatusMatch($tempStatusInvoiceBitrix, $tempStatusInvoice1c)) {
                    //Формируем массивы для сравнения
                    //Массив с данными одного счета из Битрикс.
                    $arTempImvoiceBitrixMain = array(
                        "SUMM" => $arCompareBitrix[$invoiceNum]["PRICE_FROM_1C"],
                        "DATE_BILL" => $arCompareBitrix[$invoiceNum]["DATE_BILL"],
                        "DATE_PAY_BEFORE" => $arCompareBitrix[$invoiceNum]["DATE_PAY_BEFORE"],
                        "DATE_PAY" => $arCompareBitrix[$invoiceNum]["DATE_PAY"],
                    );
                    //Массив с данными о товарах одного счета из Битрикс
                    $arTempImvoiceBitrixProducts = array();
                    $arTempImvoiceBitrixProductsNames = array();
                    if (!empty($arCompareBitrix[$invoiceNum]["PRODUCTS"])) {
                        foreach ($arCompareBitrix[$invoiceNum]["PRODUCTS"] as $product) {
                            $arTempImvoiceBitrixProducts[] = array(
                                "NAME" => $product["NAME"],
                                "PRICE" => $product["PRICE"],
                                "QUANTITY" => $product["QUANTITY"],
                                "VAT_RATE" => $product["VAT_RATE"],
                                "VAT_SUMM" => $product["VAT_SUMM"],
                            );
                            $arTempImvoiceBitrixProductsNames[] = $product["NAME"];
                        };
                        asort($arTempImvoiceBitrixProductsNames);
                    }


                    //Массив с данными одного счета из 1с.
                    $arTempImvoice1cMain = array(
                        "SUMM" => $arCompare1c[$invoiceNum]["PRICE"],
                        "DATE_BILL" => $arCompare1c[$invoiceNum]["DATE_BILL"],
                        "DATE_PAY_BEFORE" => $arCompare1c[$invoiceNum]["DATE_PAY_BEFORE"],
                        "DATE_PAY" => $arCompare1c[$invoiceNum]["DATE_PAY"],
                    );
                    //Массив с данными о товарах одного счета из 1с
                    $arTempImvoice1cProducts = array();
                    $arTempImvoice1cProductsNames = array();
                    if (!empty($arCompare1c[$invoiceNum]["PRODUCTS"])) {
                        foreach ($arCompare1c[$invoiceNum]["PRODUCTS"] as $product) {
                            $arTempImvoice1cProducts[] = array(
                                "NAME" => $product["NAME"],
                                "PRICE" => $product["PRICE"],
                                "QUANTITY" => $product["QUANTITY"],
                                "VAT_RATE" => $product["VAT_RATE"],
                                "VAT_SUMM" => $product["VAT_SUMM"],
                            );
                            $arTempImvoice1cProductsNames[] = $product["NAME"];
                        };
                        asort($arTempImvoice1cProductsNames);
                    }
                    //Сверяем на разницу основных параметров счета из Битрикс и 1с
                    $arDifMain = array_diff($arTempImvoiceBitrixMain, $arTempImvoice1cMain);
                    if (empty($arDifMain)) {
                        //Сверяем на разницу по названиям товаров счета из Битрикс и 1с
                        $arDifProd = array_diff($arTempImvoiceBitrixProductsNames, $arTempImvoice1cProductsNames);
                        //Если совпало по названиям, смотрим по остальным параметрам
                        if (empty($arDifProd)) {
                            $arProdConsilience = true;
                            foreach ($arTempImvoiceBitrixProducts as $keyProdBitrix => $arProdBitrix) {
                                $arProdConsilienceTemp = false;
                                //Пробуем найти полные совпадения товара из Битрикс в одном из всех товаров 1с
                                foreach ($arTempImvoice1cProductsNames as $keyProd1c => $arProd1c) {
                                    $arDifProd2 = array_diff($arProdBitrix, $arProd1c);
                                    //Если разницы между массивами не найдено - значит нашлось полное совпадение.
                                    //Прерываем текущий цикл и переходим на след. товарную позицию
                                    if (empty($arDifProd2)) {
                                        $arProdConsilienceTemp = true;
                                        break;
                                    }
                                }
                                //Если по товару из Битрикс не найдено ни одного полного совпадения среди товаров Битрикс
                                if (!$arProdConsilienceTemp) {
                                    $arProdConsilience = false;
                                    break;
                                }
                            }
                            //Если $arProdConsilience === true, то это означает, что все товары из Битрикс полностью совпалис товарами из 1с
                            if ($arProdConsilience) {
                                //Если различий не найдено, то увеличиваем количесвто полных совпадений
                                $arResultByCategory["COUNT_FULL_COPY"]++;
                            } else {
                                foreach ($arTempImvoice1cProducts as $key => $arProd) {
                                    $orderID = intval($arCompareBitrix[$invoiceNum]["ID"]);
                                    if (!in_array($orderID, $arResultByCategory["INVOICE_ID_NOT_MATCH_PRODUCTS_PARAM"])) {
                                        $arResultByCategory["INVOICE_ID_NOT_MATCH_PRODUCTS_PARAM"][] = $orderID;
                                    }
                                    $arResultByCategory["COUNT_INVOICE_NOT_MATCH_PRODUCTS_PARAM"][] = array(
                                        'INVOICE_NUM' => $invoiceNum,
                                        'ORDER_ID' => $arCompareBitrix[$invoiceNum]["ID"],
                                        'DATE_UPDATE' => $arCompareBitrix[$invoiceNum]["DATE_UPDATE"],
                                        'XML_ID' => $arProd["EXT_ID"],
                                        'NAME' => $arProd["NAME"],
                                        'PRICE' => $arProd["PRICE"],
                                        'QUANTITY' => $arProd["QUANTITY"],
                                        'VAT_RATE' => $arProd["VAT_RATE"],
                                        'VAT_SUMM' => $arProd["VAT_SUMM"],
                                    );
                                }
                            }
                        } else { //Если не совпало по названиям, дальше можно не проверять
                            $logger->log('$invoiceNum = ' . $invoiceNum . ' | $arTempImvoiceBitrixProducts = ' . print_r($arTempImvoiceBitrixProducts, true) . '. $arTempImvoice1cProducts = ' . print_r($arTempImvoice1cProducts, true) . '.  $arDifProd = ' . print_r($arDifProd, true));
                            //Если есть различия, то увеличиваем количество счетов,
                            // которые не совпали по товарным позиция
                            foreach ($arTempImvoice1cProducts as $key => $arProd) {
                                $orderID = intval($arCompareBitrix[$invoiceNum]["ID"]);
                                if (!in_array($orderID, $arResultByCategory["INVOICE_ID_NOT_MATCH_PRODUCTS_PARAM"])) {
                                    $arResultByCategory["INVOICE_ID_NOT_MATCH_PRODUCTS_PARAM"][] = $orderID;
                                }
                                $arResultByCategory["COUNT_INVOICE_NOT_MATCH_PRODUCTS_PARAM"][] = array(
                                    'INVOICE_NUM' => $invoiceNum,
                                    'ORDER_ID' => $arCompareBitrix[$invoiceNum]["ID"],
                                    'DATE_UPDATE' => $arCompareBitrix[$invoiceNum]["DATE_UPDATE"],
                                    'XML_ID' => $arProd["EXT_ID"],
                                    'NAME' => $arProd["NAME"],
                                    'PRICE' => $arProd["PRICE"],
                                    'QUANTITY' => $arProd["QUANTITY"],
                                    'VAT_RATE' => $arProd["VAT_RATE"],
                                    'VAT_SUMM' => $arProd["VAT_SUMM"],
                                );
                            }
                        }
                    } else {
                        $logger->log('$invoiceNum = ' . $invoiceNum . ' | $arTempImvoiceBitrixMain = ' . print_r($arTempImvoiceBitrixMain, true) . '. $arTempImvoice1cMain = ' . print_r($arTempImvoice1cMain, true) . '.  $arDifMain = ' . print_r($arDifMain, true));
                        //Если есть различия по основным параметрам счета, то увеличиваем количество счетов,
                        // которые не совпали по параметрам счета (номер счета одинаковый)
                        $arResultByCategory["COUNT_INVOICE_NOT_MATCH_MAIN_PARAM"][] = array(
                            'INVOICE_NUM' => $invoiceNum,
                            'INVOICE_DATA_TYPE' => 'BITRIX',
                            'ID' => $arCompareBitrix[$invoiceNum]["ID"],
                            'STATUS_ID' => $arCompareBitrix[$invoiceNum]["STATUS_ID"],
                            'PRICE' => $arCompareBitrix[$invoiceNum]["PRICE_FROM_1C"],
                            'DATE_BILL' => $arCompareBitrix[$invoiceNum]["DATE_BILL"],
                            'DATE_PAY_BEFORE' => $arCompareBitrix[$invoiceNum]["DATE_PAY_BEFORE"],
                            'DATE_PAY' => $arCompareBitrix[$invoiceNum]["DATE_PAY"],
                            'PAY_VOUCHER_NUM' => $arCompareBitrix[$invoiceNum]["PAY_VOUCHER_NUM"],
                            'DATE_UPDATE' => $arCompareBitrix[$invoiceNum]["DATE_UPDATE"]
                        );

                        $arResultByCategory["COUNT_INVOICE_NOT_MATCH_MAIN_PARAM"][] = array(
                            'INVOICE_NUM' => $invoiceNum,
                            'INVOICE_DATA_TYPE' => 'FROM_1C',
                            'ID' => $arCompare1c[$invoiceNum]["ID"],
                            'STATUS_ID' => $arCompare1c[$invoiceNum]["STATUS_ID"],
                            'PRICE' => $arCompare1c[$invoiceNum]["PRICE"],
                            'DATE_BILL' => $arCompare1c[$invoiceNum]["DATE_BILL"],
                            'DATE_PAY_BEFORE' => $arCompare1c[$invoiceNum]["DATE_PAY_BEFORE"],
                            'DATE_PAY' => $arCompare1c[$invoiceNum]["DATE_PAY"],
                            'PAY_VOUCHER_NUM' => $arCompare1c[$invoiceNum]["PAY_VOUCHER_NUM"],
                            'DATE_UPDATE' => ''
                        );
                    }
                } else {
                    $logger->log('$invoiceNum = ' . $invoiceNum . ' | $tempStatusInvoiceBitrix = ' . $tempStatusInvoiceBitrix . ' != $tempStatusInvoice1c = ' . $tempStatusInvoice1c);
                    $arResultByCategory["COUNT_INVOICE_NOT_MATCH_MAIN_PARAM"][] = array(
                        'INVOICE_NUM' => $invoiceNum,
                        'INVOICE_DATA_TYPE' => 'BITRIX',
                        'ID' => $arCompareBitrix[$invoiceNum]["ID"],
                        'STATUS_ID' => $arCompareBitrix[$invoiceNum]["STATUS_ID"],
                        'PRICE' => $arCompareBitrix[$invoiceNum]["PRICE_FROM_1C"],
                        'DATE_BILL' => $arCompareBitrix[$invoiceNum]["DATE_BILL"],
                        'DATE_PAY_BEFORE' => $arCompareBitrix[$invoiceNum]["DATE_PAY_BEFORE"],
                        'DATE_PAY' => $arCompareBitrix[$invoiceNum]["DATE_PAY"],
                        'PAY_VOUCHER_NUM' => $arCompareBitrix[$invoiceNum]["PAY_VOUCHER_NUM"],
                        'DATE_UPDATE' => $arCompareBitrix[$invoiceNum]["DATE_UPDATE"]
                    );

                    $arResultByCategory["COUNT_INVOICE_NOT_MATCH_MAIN_PARAM"][] = array(
                        'INVOICE_NUM' => $invoiceNum,
                        'INVOICE_DATA_TYPE' => 'FROM_1C',
                        'ID' => $arCompare1c[$invoiceNum]["ID"],
                        'STATUS_ID' => $arCompare1c[$invoiceNum]["STATUS_ID"],
                        'PRICE' => $arCompare1c[$invoiceNum]["PRICE"],
                        'DATE_BILL' => $arCompare1c[$invoiceNum]["DATE_BILL"],
                        'DATE_PAY_BEFORE' => $arCompare1c[$invoiceNum]["DATE_PAY_BEFORE"],
                        'DATE_PAY' => $arCompare1c[$invoiceNum]["DATE_PAY"],
                        'PAY_VOUCHER_NUM' => $arCompare1c[$invoiceNum]["PAY_VOUCHER_NUM"],
                        'DATE_UPDATE' => ''
                    );
                }
                unset($arCompareBitrix[$invoiceNum]);
                unset($arCompare1c[$invoiceNum]);
            }
            /*
             * Формируем данные для файла CSV
             */
            foreach ($arCompareBitrix as $arInvoice) {
                if ($arInvoice["STATUS_ID"] == "D"){
                    $arResultByCategory["COUTN_BITRIX_INVOICE_NO_MATCH_1C_IN_STATUS_CANCELED"][] = array(
                        "ACCOUNT_NUMBER" => $arInvoice["ACCOUNT_NUMBER"],
                        "ID" => $arInvoice["ID"],
                        "STATUS_ID" => $arInvoice["STATUS_ID"],
                        "PRICE" => $arInvoice["PRICE"],
                        "PRICE_FROM_1C" => $arInvoice["PRICE_FROM_1C"],
                        "DATE_BILL" => $arInvoice["DATE_BILL"],
                        "DATE_PAY_BEFORE" => $arInvoice["DATE_PAY_BEFORE"],
                        "DATE_PAY" => $arInvoice["DATE_PAY"],
                        'DATE_UPDATE' => $arInvoice["DATE_UPDATE"],
                    );
                } elseif($arInvoice["STATUS_ID"] == "Y") {
                    $arResultByCategory["COUTN_BITRIX_INVOICE_NO_MATCH_1C_IN_STATUS_DEL"][] = array(
                        "ACCOUNT_NUMBER" => $arInvoice["ACCOUNT_NUMBER"],
                        "ID" => $arInvoice["ID"],
                        "STATUS_ID" => $arInvoice["STATUS_ID"],
                        "PRICE" => $arInvoice["PRICE"],
                        "PRICE_FROM_1C" => $arInvoice["PRICE_FROM_1C"],
                        "DATE_BILL" => $arInvoice["DATE_BILL"],
                        "DATE_PAY_BEFORE" => $arInvoice["DATE_PAY_BEFORE"],
                        "DATE_PAY" => $arInvoice["DATE_PAY"],
                        'DATE_UPDATE' => $arInvoice["DATE_UPDATE"],
                    );
                } elseif($arInvoice["STATUS_ID"] == "N") {
                    $arResultByCategory["COUTN_BITRIX_INVOICE_NO_MATCH_1C_IN_STATUS_DRAFT"][] = array(
                        "ACCOUNT_NUMBER" => $arInvoice["ACCOUNT_NUMBER"],
                        "ID" => $arInvoice["ID"],
                        "STATUS_ID" => $arInvoice["STATUS_ID"],
                        "PRICE" => $arInvoice["PRICE"],
                        "PRICE_FROM_1C" => $arInvoice["PRICE_FROM_1C"],
                        "DATE_BILL" => $arInvoice["DATE_BILL"],
                        "DATE_PAY_BEFORE" => $arInvoice["DATE_PAY_BEFORE"],
                        "DATE_PAY" => $arInvoice["DATE_PAY"],
                        'DATE_UPDATE' => $arInvoice["DATE_UPDATE"],
                    );
                } else {
                    $arResultByCategory["COUTN_BITRIX_INVOICE_NO_MATCH_1C"][] = array(
                        "ACCOUNT_NUMBER" => $arInvoice["ACCOUNT_NUMBER"],
                        "ID" => $arInvoice["ID"],
                        "STATUS_ID" => $arInvoice["STATUS_ID"],
                        "PRICE" => $arInvoice["PRICE"],
                        "PRICE_FROM_1C" => $arInvoice["PRICE_FROM_1C"],
                        "DATE_BILL" => $arInvoice["DATE_BILL"],
                        "DATE_PAY_BEFORE" => $arInvoice["DATE_PAY_BEFORE"],
                        "DATE_PAY" => $arInvoice["DATE_PAY"],
                        'DATE_UPDATE' => $arInvoice["DATE_UPDATE"],
                    );
                }
            }
            /*
             * Формируем данные для файла CSV
             */
            foreach ($arCompare1c as $arInvoice) {
                if (is_array($arInvoice)) {
                    $arResultByCategory["COUTN_1C_INVOICE_NO_MATCH_BITRIX"][] = array(
                        "ACCOUNT_NUMBER" => $arInvoice["ACCOUNT_NUMBER"],
                        "STATUS_ID" => $arInvoice["STATUS_ID"],
                        "PRICE" => $arInvoice["PRICE"],
                        "DATE_BILL" => $arInvoice["DATE_BILL"],
                        "DATE_PAY_BEFORE" => $arInvoice["DATE_PAY_BEFORE"],
                        "DATE_PAY" => $arInvoice["DATE_PAY"],
                    );
                }
            }
        }
        $arResultByCategory["COUNT_INVOICE_NOT_MATCH_PRODUCTS_PARAM_COUNT"] = sizeOf($arResultByCategory["COUNT_INVOICE_NOT_MATCH_PRODUCTS_PARAM"]);
        return $arResultByCategory;
    }

    //Функция проверяет на совпадение статусы счетов из Битрикс и 1с
    function checkingInvoiceStatusMatch($invoiceStatusBitrix, $invoiceStatus1c)
    {
        $statusConsillience = false;
        if ($invoiceStatusBitrix == "S" && ($invoiceStatus1c == "НеОплачен" || $invoiceStatus1c == "Не оплачен" || $invoiceStatus1c == "")) {
            $statusConsillience = true;
        }

        if ($invoiceStatusBitrix == "A" && ($invoiceStatus1c == "НеОплачен" || $invoiceStatus1c == "Не оплачен" || $invoiceStatus1c == "")) {
            $statusConsillience = true;
        }

        if ($invoiceStatusBitrix == "P" && ($invoiceStatus1c == "Оплачен" || $invoiceStatus1c == "ОплаченЧастично" || $invoiceStatus1c == "Оплачен частично")) {
            $statusConsillience = true;
        }

        if ($invoiceStatus1c == "Отменен" && ($invoiceStatusBitrix == "D" || $invoiceStatusBitrix == "Y")) {
            $statusConsillience = true;
        }

        return $statusConsillience;
    }

    /*
     * Функция возвращает соответствующий ID статуса для счета в Битрикс24 по текстовому значению из 1с
     * Значение текущего статуса счета в Битрикс24 важно, т.к. в случае если счет отменен,
     * срабатывают различные условия в зависимости от текущего статуса счета в Битрикс24
     */
    function ConvertStatus1cToBitrix($invoiceStatusBitrix, $invoiceStatus1c)
    {
        $statusFromBitrix = '';
        if ($invoiceStatus1c == "НеОплачен" || $invoiceStatus1c == "Не оплачен" || $invoiceStatus1c == "") {
            $statusFromBitrix = "A";
        }

        if ($invoiceStatus1c == "Оплачен" || $invoiceStatus1c == "ОплаченЧастично" || $invoiceStatus1c == "Оплачен частично") {
            $statusFromBitrix = "P";
        }

        if ($invoiceStatus1c == "Отменен") {
            if ($invoiceStatusBitrix == "D") {
                $statusFromBitrix = $invoiceStatusBitrix;
            } elseif ($invoiceStatusBitrix == "Y") {
                $statusFromBitrix = $invoiceStatusBitrix;
            } else {
                $statusFromBitrix = "D";
            }
        }

        return $statusFromBitrix;
    }

    function updateInvoicesBitrix($arInvoices, $action = '')
    {
        global $DB;
        $logger = Logger::getLogger('updateInvoicesBitrix');
        $arResult = array();
        $CCRmInvoice = new CCRmInvoice();
        $CSaleBasket = new CSaleBasket();
        $datetimeBorder = new DateTime(date("d.m.Y H") . ':00:00');
        if (!empty($arInvoices) && $action != '') {
            $arOptions = Array(
                "REGISTER_SONET_EVENT" => 1,
                "UPDATE_SEARCH" => 1
            );
            $totalElemCount = 0;
            $updateElemCount = 0;
            $errorCount = 0;
            $countBorder = 0;
            $arUpdate = array();
            switch ($action) {
                case "updateMainParam": //Обновление основных параметров счета
                    foreach ($arInvoices as $key => $arInvoice) {
                        $fieldNameUFsummFrom1c = GetUserFieldIDByXMLID("UF_INVOICE_SUMM_FROM_1C", "FIELD_NAME");
                        if ($arInvoice['INVOICE_DATA_TYPE'] == 'BITRIX') {
                            $invoiceNum = $arInvoice['INVOICE_NUM'];
                            $arResult[$invoiceNum]["BITRIX"] = $arInvoice;
                            $statusInvoiceBitrix = $arInvoice['STATUS_ID'];
                            $invoiceID = $arInvoice['ID'];
                            $dateUpd = $arInvoice["DATE_UPDATE"];
                            $bitrixPayVoucherDate = $arInvoice["DATE_PAY"];
                            $bitrixPayVoucherNum = $arInvoice["PAY_VOUCHER_NUM"];
                        }
                        if ($arInvoice['INVOICE_DATA_TYPE'] == 'FROM_1C' && $arInvoice['INVOICE_NUM'] == $invoiceNum) {
                            $statusInvoice1c = $arInvoice['STATUS_ID'];
                            $arResult[$invoiceNum]["FROM_1C"] = $arInvoice;
                            $curStatusID = self::ConvertStatus1cToBitrix($statusInvoiceBitrix, $statusInvoice1c);
                            $arUpdate[$invoiceID] = array(
                                'STATUS_ID' => (!empty($curStatusID)) ? $curStatusID : $statusInvoiceBitrix,
                                "DATE_BILL" => $arInvoice["DATE_BILL"],
                                "DATE_PAY_BEFORE" => $arInvoice["DATE_PAY_BEFORE"],
                                "$fieldNameUFsummFrom1c" => $arInvoice["PRICE"],
                                "DATE_UPDATE" => $dateUpd,
                            );
                            if($bitrixPayVoucherDate != $arInvoice["DATE_PAY"]){
                                $arUpdate[$invoiceID]["PAY_VOUCHER_DATE"] = (empty($arInvoice["DATE_PAY"])) ? '' : $DB->FormatDate($arInvoice["DATE_PAY"], "DD.MM.YYYY", "YYYY-MM-DD");
                            }
                            if($bitrixPayVoucherNum != $arInvoice["PAY_VOUCHER_NUM"]){
                                $arUpdate[$invoiceID]["PAY_VOUCHER_NUM"] = (empty($arInvoice["PAY_VOUCHER_NUM"])) ? '' : $arInvoice["PAY_VOUCHER_NUM"];
                            }
                            $totalElemCount++;
                        }
                    }
                    if (!empty($arUpdate)) {
                        foreach ($arUpdate as $invoiceID => $arUpd) {
                            $dateUpd = new DateTime($arUpd["DATE_UPDATE"]);
                            unset($arUpd["DATE_UPDATE"]);
                            if ($dateUpd < $datetimeBorder) {
                                if ($updateElemCount < 50) {
                                    if(isset($arUpd["PAY_VOUCHER_DATE"])) {
                                        $strSql = "UPDATE `b_sale_order` SET `PAY_VOUCHER_DATE` = '" . $arUpd["PAY_VOUCHER_DATE"] . "' WHERE `ID` = $invoiceID";
                                        $DB->Query($strSql, false, $err_mess . __LINE__);
                                        unset($arUpd["PAY_VOUCHER_DATE"]);
                                    }
                                    self::updateInvoicePaySystemID($invoiceID);
                                    if ($CCRmInvoice->Update($invoiceID, $arUpd, array('REGISTER_SONET_EVENT' => true, 'UPDATE_SEARCH' => true))) {
                                        $updateElemCount++;
                                        $logger->log('УСПЕШНО Обновление счета $invoiceID = ' . $invoiceID . ' | $arUpd = ' . print_r($arUpd, true));
                                    } else {
                                        $errorCount++;
                                        $arResult["error_mess"][$invoiceID] = $arUpd;
                                        $logger->log('ОШИБКА Обновление счета $invoiceID = ' . $invoiceID . ' | $arUpd = ' . print_r($arUpd, true) . ' ERROR: ' . $CCRmInvoice->LAST_ERROR);
                                    }
                                } else {
                                    break;
                                }
                            } else {
                                $countBorder++;
                                $arResult["DATE_BORDER"][$invoiceID] = array(
                                    'STATUS_ID' => (!empty($curStatusID)) ? $curStatusID : $statusInvoiceBitrix,
                                    "DATE_BILL" => $arInvoice["DATE_BILL"],
                                    "PAY_VOUCHER_DATE" => (empty($arInvoice["DATE_PAY"])) ? '01.01.1970' : $arInvoice["DATE_PAY"],
                                    "DATE_PAY_BEFORE" => $arInvoice["DATE_PAY_BEFORE"],
                                    "$fieldNameUFsummFrom1c" => $arInvoice["PRICE"],
                                );
                            }
                        }
                    }
                    $arResult["arUpdate"] = $arUpdate;
                    $logger->log("Успешно обновлено данных по счетам: $updateElemCount из $totalElemCount. Ошибок при обновление: $errorCount. Дата обновления больше начала часа: $countBorder");

                    $arResult["message"] = "Успешно обновлено данных по счетам: $updateElemCount из $totalElemCount. Ошибок при обновление: $errorCount. Дата обновления больше начала часа: $countBorder";
                    $arResult["updateElemCount"] = $updateElemCount;
                    $arResult["errorCount"] = $errorCount;
                    $arResult["countBorder"] = $countBorder;
                    $arResult['status'] = 200;
                    break;
                case "updateProds": //Обновление информации о товарных позициях для счета
                    $invoiceNum = '';
                    $invoiceID = 0;
                    foreach ($arInvoices as $key => $arProd) {
                        $logger->log('$arProd = ' . print_r($arProd, true));
                        $logger->log('$invoiceNum = ' . $invoiceNum . ' | $invoiceID = ' . $invoiceID);
                        if ($invoiceNum != $arProd['INVOICE_NUM'] && $invoiceID != $arProd['ORDER_ID']) {
                            $totalElemCount++;
                            if (!empty($arUpdate)) {
                                $logger->log('ДО $invoiceID = ' . $invoiceID . ' | $invoiceNum = ' . $invoiceNum . ' |  $arUpdate = ' . print_r($arUpdate, true));
                                $arFields["PRODUCT_ROWS"] = $arUpdate["PRODUCT_ROWS"];
                                $dateUpd = new DateTime($arFields["DATE_UPDATE"]);
                                if ($dateUpd < $datetimeBorder) {
                                    /*$dbBasketItems = $CSaleBasket->GetList(
                                        array(
                                            "NAME" => "ASC",
                                            "ID" => "ASC"
                                        ),
                                        array(
                                            "ORDER_ID" => $invoiceID
                                        ),
                                        false,
                                        false,
                                        array("ID", "PRODUCT_ID", "QUANTITY", "NAME", "PRICE", "VAT_INCLUDED", "VAT_RATE", "MODULE")
                                    );
                                    while ($arItem = $dbBasketItems->Fetch()) {
                                        if (empty($arItem["MODULE"])) {
                                            $cartElemID = $arItem["ID"];
                                            $strSql = "UPDATE `b_sale_basket` SET `MODULE` = 'catalog' WHERE `ID` = $cartElemID";
                                            $DB->Query($strSql, false, $err_mess . __LINE__);
                                        }
                                        if ($CSaleBasket->Delete($arItem["ID"])) {
                                            $logger->log('УСПЕШНО Удаление элемента  $arItem["ID"] = ' . $arItem["ID"]);
                                        } else {
                                            $logger->log('ОШИБКА Удаление элемента  $arItem["ID"] = ' . $arItem["ID"]);
                                        }
                                    }*/
                                    if ($resUpd = $CCRmInvoice->update($invoiceID, $arFields, array('REGISTER_SONET_EVENT' => true, 'UPDATE_SEARCH' => true))) {
                                        $logger->log('УСПЕШНО обновление счета $invoiceID = ' . $invoiceID);
                                        $updateElemCount++;
                                        if ($updateElemCount > 50) {
                                            $arUpdate = array();
                                            break;
                                        }
                                    } else {
                                        $logger->log('ОШИБКА обновления счета $invoiceID = ' . $invoiceID . ' | $arFields = ' . print_r($arFields, true) . ' | $resUpd = ' . $CCRmInvoice->LAST_ERROR);
                                        //Костыль по ошибке ""
                                        if ($CCRmInvoice->LAST_ERROR == '') {

                                        }
                                        $errorCount++;
                                    }
                                } else {
                                    $arResult["DATE_BORDER"][$invoiceID] = $arFields;
                                    $countBorder++;
                                }
                            }
                            $invoiceNum = $arProd['INVOICE_NUM'];
                            $invoiceID = $arProd['ORDER_ID'];
                            self::updateInvoicePaySystemID($invoiceID);
                            $arInvoice = $CCRmInvoice->GetByID($invoiceID);
                            $prodName = $arProd["NAME"];
                            $arUpdate = array();
                            $arFields = array(
                                "PERSON_TYPE_ID" => $arInvoice["PERSON_TYPE_ID"],
                                "PAY_SYSTEM_ID" => $arInvoice["PAY_SYSTEM_ID"],
                                "INVOICE_PROPERTIES" => self::getInvoiceProp($invoiceID, $arInvoice["PERSON_TYPE_ID"]),
                                "DATE_UPDATE" => $arInvoice["DATE_UPDATE"],
                            );
                            $arUpdate["PRODUCT_ROWS"][] = array(
                                "ID" => 0,
                                "PRODUCT_ID" => self::getProdIdByName($prodName),
                                "PRODUCT_NAME" => $prodName,
                                "QUANTITY" => $arProd["QUANTITY"],
                                "PRICE" => $arProd["PRICE"],
                                "VAT_INCLUDED" => ($arProd["VAT_RATE"] > 0) ? 'Y' : 'N',
                                "VAT_RATE" => $arProd["VAT_RATE"],
                                "DISCOUNT_PRICE" => 0,
                                "MEASURE_CODE" => '796',
                                "MEASURE_NAME" => 'шт',
                                "CUSTOMIZED" => 'Y',
                                "SORT" => (sizeOf($arUpdate) + 1) * 10,
                            );
                        } elseif ($arProd['INVOICE_NUM'] == $invoiceNum && $arProd['ORDER_ID'] == $invoiceID) {
                            $prodName = $arProd["NAME"];
                            $arUpdate["PRODUCT_ROWS"][] = array(
                                "ID" => 0,
                                "PRODUCT_ID" => self::getProdIdByName($prodName),
                                "PRODUCT_NAME" => $prodName,
                                "QUANTITY" => $arProd["QUANTITY"],
                                "PRICE" => $arProd["PRICE"],
                                "VAT_INCLUDED" => ($arProd["VAT_RATE"] > 0) ? 'Y' : 'N',
                                "VAT_RATE" => $arProd["VAT_RATE"],
                                "DISCOUNT_PRICE" => 0,
                                "MEASURE_CODE" => '796',
                                "MEASURE_NAME" => 'шт',
                                "CUSTOMIZED" => 'Y',
                                "SORT" => (sizeOf($arUpdate) + 1) * 10,
                            );
                        }
                    }
                    if (!empty($arUpdate)) {

                        $arFields["PRODUCT_ROWS"] = $arUpdate["PRODUCT_ROWS"];
                        $logger->log('Данные для обновления счета $invoiceID = ' . $invoiceID . ' | $arFields = ' . print_r($arFields, true));

                        $dateUpd = new DateTime($arFields["DATE_UPDATE"]);
                        if ($dateUpd < $datetimeBorder) {
                            /*$dbBasketItems = $CSaleBasket->GetList(
                                array(
                                    "NAME" => "ASC",
                                    "ID" => "ASC"
                                ),
                                array(
                                    "ORDER_ID" => $invoiceID
                                ),
                                false,
                                false,
                                array("ID", "PRODUCT_ID", "QUANTITY", "NAME", "PRICE", "VAT_INCLUDED", "VAT_RATE", "MODULE
                            ")
                            );
                            while ($arItem = $dbBasketItems->Fetch()) {
                                if (empty($arItem["MODULE"])) {
                                    $cartElemID = $arItem["ID"];
                                    $strSql = "UPDATE `b_sale_basket` SET `MODULE` = 'catalog' WHERE `ID` = $cartElemID";
                                    $DB->Query($strSql, false, $err_mess . __LINE__);
                                }
                                if ($CSaleBasket->Delete($arItem["ID"])) {
                                    $logger->log('УСПЕШНО Удаление элемента  $arItem["ID"] = ' . $arItem["ID"]);
                                } else {
                                    $logger->log('ОШИБКА Удаление элемента  $arItem["ID"] = ' . $arItem["ID"]);
                                }
                            }*/
                            if ($resUpd = $CCRmInvoice->update($invoiceID, $arFields, array('REGISTER_SONET_EVENT' => true, 'UPDATE_SEARCH' => true))) {
                                $logger->log('УСПЕШНО обновление счета $invoiceID = ' . $invoiceID);
                                $updateElemCount++;
                            } else {
                                $logger->log('ОШИБКА обновления счета $invoiceID = ' . $invoiceID . ' | $arFields = ' . print_r($arFields, true) . ' | $resUpd = ' . $CCRmInvoice->LAST_ERROR);
                                $errorCount++;
                            }
                        } else {
                            $arResult["DATE_BORDER"][$invoiceID] = $arFields;
                            $countBorder++;
                        }
                    }

                    $logger->log("Успешно обновлено товаров по счетам: $updateElemCount из $totalElemCount. Ошибок при обновление: $errorCount. Дата обновления больше начала часа: $countBorder");

                    $arResult["message"] = "Успешно обновлено товаров по счетам: $updateElemCount из $totalElemCount. Ошибок при обновление: $errorCount. Дата обновления больше начала часа: $countBorder";
                    $arResult["updateElemCount"] = $updateElemCount;
                    $arResult["errorCount"] = $errorCount;
                    $arResult["countBorder"] = $countBorder;
                    $arResult['status'] = 200;
                    break;
                case "updateDateModify": //Обновление основных параметров счета
                    $logger->log('$arInvoices = ' . print_r($arInvoices, true));
                    $totalElemCount = sizeOf($arInvoices);
                    if (!empty($arInvoices)) {
                        foreach ($arInvoices as $key => $arInvoice) {
                            $dateUpd = new DateTime($arInvoice["DATE_UPDATE"]);
                            if ($dateUpd < $datetimeBorder) {
                                $invoiceID = $arInvoice["ID"];
                                $arUpd = array(
                                    "DATE_BILL" => $arInvoice["DATE_BILL"],
                                );
                                if ($updateElemCount < 50) {
                                    self::updateInvoicePaySystemID($invoiceID);
                                    if ($CCRmInvoice->Update($invoiceID, $arUpd, array('REGISTER_SONET_EVENT' => true, 'UPDATE_SEARCH' => true))) {
                                        $updateElemCount++;
                                        $logger->log('УСПЕШНО обновление счета $invoiceID = ' . $invoiceID);
                                    } else {
                                        $errorCount++;
                                        $logger->log('ОШИБКА обновления счета $invoiceID = ' . $invoiceID . ' | $arUpd = ' . print_r($arUpd, true) . ' | $resUpd = ' . $CCRmInvoice->LAST_ERROR);
                                        $arResult["error_mess"][] = $arUpd;
                                    }
                                } else {
                                    break;
                                }
                            } else {
                                $countBorder++;
                            }
                        }
                    }
                    $logger->log("Успешно обновлено счетов: $updateElemCount из $totalElemCount. Ошибок при обновление: $errorCount. Дата обновления больше начала часа: $countBorder");

                    $arResult["message"] = "Успешно обновлено счетов: $updateElemCount из $totalElemCount. Ошибок при обновление: $errorCount. Дата обновления больше начала часа: $countBorder";
                    $arResult["updateElemCount"] = $updateElemCount;
                    $arResult["errorCount"] = $errorCount;
                    $arResult["countBorder"] = $countBorder;
                    $arResult['status'] = 200;
                    break;
                default:
                    $arResult = ["status" => false, "error" => "Для данного параметра [$action] обработка не реализована."];
                    exit;
                    break;
            }
            $logger->log('Обновление счетов с $action = ' . $action . ' | $arInvoices = ' . print_r($arInvoices, true) . ' | $arResult = ' . print_r($arResult, true));
            return $arResult;
        } else {
            return false;
        }
    }

    function getProdIdByName($prodName, $prodXML = '', $orgType = 'esk')
    {
        $el = new CIBlockElement();
        $filterBySec = array(150, 151, 139);
        $elemID = 0;
        $arResult = array();
        $rsProd = $el->GetList(
            array("ID" => "DESC"),
            array("IBLOCK_ID" => GetIBlockIDByCode("products_catalog"), "ACTIVE" => "Y", "SECTION_ID" => $filterBySec, "NAME" => $prodName),
            false,
            false,
            array("ID", "NAME", "SECTION_ID")
        );
        if ($arProd = $rsProd->Fetch()) {
            $elemID = $arProd["ID"];
        } else {
            $arLoadProductArray = Array(
                "IBLOCK_SECTION_ID" => 139,          // элемент лежит в корне раздела
                "IBLOCK_ID" => GetIBlockIDByCode("products_catalog"),
                "NAME" => $prodName,
                "XML_ID" => $prodXML,
                "ACTIVE" => "Y",            // активен
            );

            if ($PRODUCT_ID = $el->Add($arLoadProductArray))
                $elemID = $PRODUCT_ID;
        }
        return $elemID;
    }

    function getInvoiceProp($invoiceID = 0, $personTypeId = 1)
    {
        $CCRmInvoice = new CCRmInvoice();
        $arReqs = array();
        if (intval($invoiceID) > 0) {
            $dbReqs = $CCRmInvoice->GetProperties($invoiceID, $personTypeId);
            foreach ($dbReqs as $arReqTemp) {
                $arReqs[$arReqTemp["FIELDS"]["ID"]] = (!empty($arReqTemp["VALUE"])) ? $arReqTemp["VALUE"] : '';
            }
        }
        return $arReqs;
    }

    function updateInvoicePaySystemID($invoiceID = 0)
    {
        global $DB;
        $CCRmInvoice = new CCRmInvoice();
        $paySystemID = false;
        if ($invoiceID > 0) {
            $arInvoice = $CCRmInvoice->GetByID($invoiceID);
            $paySystemID = $arInvoice["PAY_SYSTEM_ID"];
            $shipperCompID = $arInvoice["UF_MYCOMPANY_ID"];
            $strSql = '';
            if ($paySystemID > 0 && $shipperCompID > 0) {

                if ($shipperCompID == self::ID_COMPANY_ESK && $paySystemID != self::ID_TEMPlATE_INVOICE_ESK) {
                    $paySystemID = self::ID_TEMPlATE_INVOICE_ESK;
                    $strSql = "UPDATE `b_sale_order` SET `PAY_SYSTEM_ID` = '" . self::ID_TEMPlATE_INVOICE_ESK . "' WHERE `ID` = $invoiceID";
                }

                if ($shipperCompID == self::ID_COMPANY_IS && $paySystemID != self::ID_TEMPlATE_INVOICE_IS) {
                    $paySystemID = self::ID_TEMPlATE_INVOICE_IS;
                    $strSql = "UPDATE `b_sale_order` SET `PAY_SYSTEM_ID` = '" . self::ID_TEMPlATE_INVOICE_IS . "' WHERE `ID` = $invoiceID";
                }

                if ($shipperCompID == self::ID_COMPANY_RJe && $paySystemID != self::ID_TEMPlATE_INVOICE_RJe) {
                    $paySystemID = self::ID_TEMPlATE_INVOICE_RJe;
                    $strSql = "UPDATE `b_sale_order` SET `PAY_SYSTEM_ID` = '" . self::ID_TEMPlATE_INVOICE_RJe . "' WHERE `ID` = $invoiceID";
                }

                if (!empty($strSql)) {
                    $DB->Query($strSql, false, $err_mess . __LINE__);
                }
            }
        }
        return $paySystemID;
    }
}