<?php

use Bitrix\Main\Loader;

Loader::includeModule("iblock");
Loader::includeModule("crm");
class ContactBitrixBpmMerge
{

    public static function FindDUBLESUsingFIOTELEMAIL($fullname,$phone,$email)
    {
         $arraydublicatIDusingName = array();
         $result = CCrmContact::GetListEx(array(), array('FULL_NAME' => $fullname, 'CHECK_PERMISSIONS' => 'N'), false, false, array('ID'));
         while ($fields = $result->Fetch()) {
                $arraydublicatIDusingName[] = $fields['ID'];
         }

        $arraydublicatIDusingMOBILE = array();
        foreach ($phone as $mobilephonearrayelement) {
            if (!empty($mobilephonearrayelement)) {
            $res = CCrmFieldMulti::GetList(array('ID' => 'ASC'), array("VALUE" => $mobilephonearrayelement, 'TYPE_ID' => "PHONE"));
            while ($arRes = $res->Fetch()) {
                $arraydublicatIDusingMOBILE[] = $arRes['ELEMENT_ID'];
            }
        }
        }

        $arraydublicatIDusingEMAIL = array();
        foreach ($email as $emailarrayelement) {
            if (!empty($emailarrayelement)) {
                $res = CCrmFieldMulti::GetList(array('ID' => 'ASC'), array("VALUE" => $emailarrayelement, 'TYPE_ID' => "EMAIL"));
                while ($arRes = $res->Fetch()) {
                    $arraydublicatIDusingEMAIL[] = $arRes['ELEMENT_ID'];
                }
            }
        }

        $resultIDFIOEMAILTEL = array_intersect(array_unique($arraydublicatIDusingEMAIL), array_unique($arraydublicatIDusingMOBILE),array_unique($arraydublicatIDusingName));
        //print_r($resultIDFIOEMAILTEL);

        //Схождение ФИО, ЕМАИЛ
        $resultIDFIOEMAIL = array_intersect(array_unique($arraydublicatIDusingEMAIL), array_unique($arraydublicatIDusingName));
        //print_r($resultIDFIOEMAIL);

        //Схождение ФИО, ТЕЛЕФОН
        $resultIDFIOTEL = array_intersect( array_unique($arraydublicatIDusingMOBILE),array_unique($arraydublicatIDusingName));
       //print_r($resultIDFIOTEL);

        $resultarraytoreturn=array_unique(array_merge($resultIDFIOEMAILTEL,$resultIDFIOEMAIL,$resultIDFIOTEL));
        //print_r($resultarraytoreturn);

        return $resultarraytoreturn;
    }


    public static function FindIDDUBLEUsingEmail($email)
    {
        if(!empty($email)) {
				$arraydublicatIDusingEMAIL = array();		
                $res = CCrmFieldMulti::GetList(array('ID' => 'ASC'), array("VALUE" => "'$email'", 'TYPE_ID' => "EMAIL"));
                while ($arRes = $res->Fetch()) {
                    $arraydublicatIDusingEMAIL[] = $arRes['ELEMENT_ID'];

                }
			if(empty($arraydublicatIDusingEMAIL)){return false;}
			else{return $arraydublicatIDusingEMAIL;}

        }
        else{return false;}
    }

    public static function FindIDDUBLEUsingPhone($phone)
    {
        if(!empty($phone)) {
			$arraydublicatIDusingMOBILE = array(); 
            $res = CCrmFieldMulti::GetList(array('ID' => 'ASC'), array("VALUE" => "'$phone'", 'TYPE_ID' => "PHONE"));
            while ($arRes = $res->Fetch()) {
				$arraydublicatIDusingMOBILE[] = $arRes['ELEMENT_ID'];//print_r($arRes);
            }
			if(empty($arraydublicatIDusingMOBILE)){return false;}
			else{return $arraydublicatIDusingMOBILE;}

        }
        else{return false;}
    }



    public static function FindIDUsingFIO($fullname)
    {
        if(!empty($fullname)) {
            $arraydublicatIDusingName = array();
            $result = CCrmContact::GetListEx(array(), array('FULL_NAME' => $fullname, 'CHECK_PERMISSIONS' => 'N'), false, false, array('ID'));
            while ($fields = $result->Fetch()) {
                $arraydublicatIDusingName[] = $fields['ID'];
            }
            return $arraydublicatIDusingName;
        }
        else{return;}
    }


    public static function FindDubleContact($idcontact)
    {
        if (isset($idcontact)) {


            $name = "";
            $secondname="";
            $lastname="";
            $mobilephonearray = array();
            $emailarray = array();
$fullname="";
            //получаем или устанавливаем GUID BPM контакт
            $entity_id = "CRM_CONTACT";
            $uf_guid = "UF_BPMCONTACTID";
            $guidperson = GetUserField($entity_id, $idcontact, $uf_guid);


            $result = CCrmContact::GetListEx(array(), array('=ID' => $idcontact, 'CHECK_PERMISSIONS' => 'N'), false, false, array('FULL_NAME'));
            $fields = is_object($result) ? $result->Fetch() : null;
            if (is_array($fields)) {

                //$name = $fields['NAME'];
                //$secondname = $fields['SECOND_NAME'];
                //$lastname = $fields['LAST_NAME'];
                $fullname= $fields['FULL_NAME'];
            }
            $arraydublicatIDusingName = array();
            $result = CCrmContact::GetListEx(array(), array('>ID'=>$idcontact,'FULL_NAME' => $fullname, 'CHECK_PERMISSIONS' => 'N'), false, false, array('ID'));
            while ($fields = $result->Fetch()) {
                $arraydublicatIDusingName[]=$fields['ID'];
            }
            if(empty($arraydublicatIDusingName)) {return;}



            $res = CCrmFieldMulti::GetList(array('ID' => 'ASC'), array('ELEMENT_ID' => $idcontact, 'TYPE_ID' => "PHONE"));
            while ($arRes = $res->Fetch()) {
                $mobilephonearray[] = $arRes['VALUE'];
            }


            $res = CCrmFieldMulti::GetList(array('ID' => 'ASC'), array('ELEMENT_ID' => $idcontact, 'TYPE_ID' => "EMAIL"));
            while ($arRes = $res->Fetch()) {
                $emailarray[] = $arRes['VALUE'];
            }

            //поиск дублей по данным параметрам.





            $arraydublicatIDusingGUID = array();
            if(!empty($guidperson)) {
                $result = CCrmContact::GetListEx(array(), array('>ID' => $idcontact, '=UF_BPMCONTACTID' => $guidperson, 'CHECK_PERMISSIONS' => 'N'), false, false, array('ID'));
                while ($fields = $result->Fetch()) {
                    $arraydublicatIDusingGUID[] = $fields['ID'];
                }
            }

            $arraydublicatIDusingMOBILE = array();
            //print_r($mobilephonearray);

            foreach ($mobilephonearray as $mobilephonearrayelement) {
                if (!empty($mobilephonearrayelement)) {
                    $res = CCrmFieldMulti::GetList(array('ID' => 'ASC'), array("VALUE" => $mobilephonearrayelement, 'TYPE_ID' => "PHONE"));
                    while ($arRes = $res->Fetch()) {
                        if ($arRes['ELEMENT_ID'] != $idcontact) {
                            $arraydublicatIDusingMOBILE[] = $arRes['ELEMENT_ID'];
                        }
                    }
                }
            }
            // print_r($emailarray);
            //echo "<br>";
            $arraydublicatIDusingEMAIL = array();
            foreach ($emailarray as $emailarrayelement) {
                if (!empty($emailarrayelement)) {
                    $res = CCrmFieldMulti::GetList(array('ID' => 'ASC'), array("VALUE" => $emailarrayelement, 'TYPE_ID' => "EMAIL"));
                    while ($arRes = $res->Fetch()) {
                        if ($arRes['ELEMENT_ID'] != $idcontact) {
                            $arraydublicatIDusingEMAIL[] = $arRes['ELEMENT_ID'];
                        }
                    }
                }
            }

            $resultIDGUID=$arraydublicatIDusingGUID;
            //Схождение ФИО, ЕМАИЛ, ТЕЛЕФОН
            $resultIDFIOEMAILTEL = array_intersect(array_unique($arraydublicatIDusingEMAIL), array_unique($arraydublicatIDusingMOBILE),array_unique($arraydublicatIDusingName));
            //Схождение ФИО, ЕМАИЛ
            $resultIDFIOEMAIL = array_intersect(array_unique($arraydublicatIDusingEMAIL), array_unique($arraydublicatIDusingName));
            //Схождение ФИО, ТЕЛЕФОН
            $resultIDFIOTEL = array_intersect( array_unique($arraydublicatIDusingMOBILE),array_unique($arraydublicatIDusingName));

            $resultarraytoreturn=array_unique(array_merge($resultIDGUID,$resultIDFIOEMAILTEL,$resultIDFIOEMAIL,$resultIDFIOTEL));
            return $resultarraytoreturn;
        }




        /*$res = CCrmContact::GetList(array(),  array('>ID' => $idcontact));
        while ($arRes = $res->Fetch())
        {
            if(ContactBitrixBpmMerge::CheckGuideQuality($idcontact, $arRes['ID']))
            {
                echo $arRes['ID']."<br>";
            }
            if(ContactBitrixBpmMerge::CheckFioEmailTelQuality($idcontact, $arRes['ID']))
            {
                echo $arRes['ID']."<br>";
            }
            if(ContactBitrixBpmMerge::CheckFioTelQuality($idcontact, $arRes['ID']))
            {
                echo $arRes['ID']."<br>";
            }
            if(ContactBitrixBpmMerge::CheckFioEmailQuality($idcontact, $arRes['ID']))
            {
                echo $arRes['ID']."<br>";
            }
        }*/


        //return $iddublecontact;
    }


    public static function CheckGuideQuality($idcontact1,$idcontact2)
    {
        $idcontact1_GUIDBMP = GetUserField("CRM_CONTACT", $idcontact1, "UF_BPMCONTACTID");
        $idcontact2_GUIDBMP = GetUserField("CRM_CONTACT", $idcontact2, "UF_BPMCONTACTID");
        if (($idcontact1_GUIDBMP == $idcontact2_GUIDBMP)&&!empty($idcontact1_GUIDBMP)&&!empty($idcontact2_GUIDBMP)) {
            return true;
        } else {
            return false;
        }
    }


    public static function CheckFioEmailQuality($idcontact1,$idcontact2)
    {
        $contact1fio="";$contact2fio=""; $arrayemail1=array();$arrayemail2=array();
        $flatequalemail = false;
        $flatequalfio = false;
        $result = CCrmContact::GetListEx(array(), array('=ID' => $idcontact1, 'CHECK_PERMISSIONS' => 'N'), false, false, array('*', 'UF_*'));
        $fields = is_object($result) ? $result->Fetch() : null;
        if (is_array($fields)) {
            $contact1fio=$fields['FULL_NAME'];
        }


        $result = CCrmContact::GetListEx(array(), array('=ID' => $idcontact2, 'CHECK_PERMISSIONS' => 'N'), false, false, array('*', 'UF_*'));
        $fields = is_object($result) ? $result->Fetch() : null;
        if (is_array($fields)) {
            $contact2fio=$fields['FULL_NAME'];
        }


        $arFilter = [
            'ELEMENT_ID' => $idcontact1,
            'TYPE_ID'    => 'EMAIL',
        ];
        $resPhones = \CCrmFieldMulti::GetListEx([],$arFilter,false,false,['VALUE']);
        while($arPhone = $resPhones->fetch()){ $arrayemail1[]=trim($arPhone['VALUE']);}

        $arFilter = [
            'ELEMENT_ID' => $idcontact2,
            'TYPE_ID'    => 'EMAIL',
        ];
        $resPhones = \CCrmFieldMulti::GetListEx([],$arFilter,false,false,['VALUE']);
        while($arPhone = $resPhones->fetch()){ $arrayemail2[]=trim($arPhone['VALUE']);}





        $resultdiffemail = array_diff($arrayemail1, $arrayemail2);
        if (count($arrayemail1) != count($resultdiffemail)) {
            $flatequalemail=true;
        }
        if ($contact1fio == $contact2fio) {
            $flatequalfio = true;
        }

        if(($flatequalemail)&&($flatequalfio))
        {
            return true;
        }
        else{return false;}

    }

    public static function CheckFioTelQuality($idcontact1,$idcontact2)
    {
        $contact1fio="";$contact2fio=""; $arraytel1=array();$arraytel2=array();
        $flatequalfio = false;
        $flatequaltel = false;
        $result = CCrmContact::GetListEx(array(), array('=ID' => $idcontact1, 'CHECK_PERMISSIONS' => 'N'), false, false, array('*', 'UF_*'));
        $fields = is_object($result) ? $result->Fetch() : null;
        if (is_array($fields)) {
            $contact1fio=$fields['FULL_NAME'];
        }


        $result = CCrmContact::GetListEx(array(), array('=ID' => $idcontact2, 'CHECK_PERMISSIONS' => 'N'), false, false, array('*', 'UF_*'));
        $fields = is_object($result) ? $result->Fetch() : null;
        if (is_array($fields)) {
            $contact2fio=$fields['FULL_NAME'];
        }

        $arFilter = [
            'ELEMENT_ID' => $idcontact1,
            'TYPE_ID'    => 'PHONE',
        ];
        $resPhones = \CCrmFieldMulti::GetListEx([],$arFilter,false,false,['VALUE']);
        while($arPhone = $resPhones->fetch()){ $arraytel1[]=trim($arPhone['VALUE']);}

        $arFilter = [
            'ELEMENT_ID' => $idcontact2,
            'TYPE_ID'    => 'PHONE',
        ];
        $resPhones = \CCrmFieldMulti::GetListEx([],$arFilter,false,false,['VALUE']);
        while($arPhone = $resPhones->fetch()){ $arraytel2[]=trim($arPhone['VALUE']);}

        $resultdifftelephone = array_diff($arraytel1, $arraytel2);
        if (count($arraytel1) != count($resultdifftelephone)) {
            $flatequaltel=true;
        }


        if ($contact1fio == $contact2fio) {
            $flatequalfio = true;
        }

        if(($flatequaltel)&&($flatequalfio))
        {
            return true;

        }
        else{return false;}


    }

    public static function CheckFioEmailTelQuality($idcontact1,$idcontact2)
    {

        $contact1fio="";$contact2fio=""; $arrayemail1=array();$arrayemail2=array();$arraytel1=array();$arraytel2=array();
        $flatequalemail = false;
        $flatequalfio = false;
        $flatequaltel = false;
        $result = CCrmContact::GetListEx(array(), array('=ID' => $idcontact1, 'CHECK_PERMISSIONS' => 'N'), false, false, array('*', 'UF_*'));
        $fields = is_object($result) ? $result->Fetch() : null;
        if (is_array($fields)) {
            $contact1fio=$fields['FULL_NAME'];
        }


        $result = CCrmContact::GetListEx(array(), array('=ID' => $idcontact2, 'CHECK_PERMISSIONS' => 'N'), false, false, array('*', 'UF_*'));
        $fields = is_object($result) ? $result->Fetch() : null;
        if (is_array($fields)) {
            $contact2fio=$fields['FULL_NAME'];
        }

        $arFilter = [
            'ELEMENT_ID' => $idcontact1,
            'TYPE_ID'    => 'PHONE',
        ];
        $resPhones = \CCrmFieldMulti::GetListEx([],$arFilter,false,false,['VALUE']);
        while($arPhone = $resPhones->fetch()){ $arraytel1[]=trim($arPhone['VALUE']);}

        $arFilter = [
            'ELEMENT_ID' => $idcontact2,
            'TYPE_ID'    => 'PHONE',
        ];
        $resPhones = \CCrmFieldMulti::GetListEx([],$arFilter,false,false,['VALUE']);
        while($arPhone = $resPhones->fetch()){ $arraytel2[]=trim($arPhone['VALUE']);}


        $arFilter = [
            'ELEMENT_ID' => $idcontact1,
            'TYPE_ID'    => 'EMAIL',
        ];
        $resPhones = \CCrmFieldMulti::GetListEx([],$arFilter,false,false,['VALUE']);
        while($arPhone = $resPhones->fetch()){ $arrayemail1[]=trim($arPhone['VALUE']);}

        $arFilter = [
            'ELEMENT_ID' => $idcontact2,
            'TYPE_ID'    => 'EMAIL',
        ];
        $resPhones = \CCrmFieldMulti::GetListEx([],$arFilter,false,false,['VALUE']);
        while($arPhone = $resPhones->fetch()){ $arrayemail2[]=trim($arPhone['VALUE']);}



        $resultdifftelephone = array_diff($arraytel1, $arraytel2);
        if (count($arraytel1) != count($resultdifftelephone)) {
            $flatequaltel=true;
        }



        $resultdiffemail = array_diff($arrayemail1, $arrayemail2);
        if (count($arrayemail1) != count($resultdiffemail)) {
            $flatequalemail=true;
        }
        if ($contact1fio == $contact2fio) {
            $flatequalfio = true;
        }

        if(($flatequaltel)&&($flatequalemail)&&($flatequalfio))
        {
            return true;

        }
        else{return false;}

    }

    public static function MergeBpmContacts($maincontact,$contacttomerge)
    {

     $data1 = array (
            "__type" => "Terrasoft.Nui.ServiceModel.DataContract.UpdateQuery",
            'RootSchemaName' => 'ContactCommunication',
            'QueryType' => 1,
            'ColumnValues' => array(
                'Items' =>
                    array(
                        'Contact' =>
                            array(
                                'ExpressionType' => 2,
                                'Parameter' =>
                                    array(
                                        'DataValueType' => 0,
                                        'Value' => $maincontact,
                                    ),
                            ),
                    ),
            ),

            'Filters' =>
                array (
                    'FilterType' => 6,
                    'ComparisonType' => 0,
                    'Items' =>
                        array (
                            'FilterId' =>
                                array (
                                    'FilterType' => 1,
                                    'ComparisonType' => 3,
                                    'LeftExpression' =>
                                        array (
                                            'ExpressionType' => 0,
                                            'ColumnPath' => 'Contact.Id',
                                        ),
                                    'RightExpression' =>
                                        array (
                                            'ExpressionType' => 2,
                                            'Parameter' =>
                                                array (
                                                    'DataValueType' => 0,
                                                    'Value' => $contacttomerge,
                                                ),
                                        ),
                                ),
                        ),
                ),
        );
        $batshContact[0] = $data1;


        $data2 = array (
            "__type" => "Terrasoft.Nui.ServiceModel.DataContract.UpdateQuery",
            'RootSchemaName' => 'ContactAddress',
            'QueryType' => 1,
            'ColumnValues' => array(
                'Items' =>
                    array(
                        'Contact' =>
                            array(
                                'ExpressionType' => 2,
                                'Parameter' =>
                                    array(
                                        'DataValueType' => 0,
                                        'Value' => $maincontact,
                                    ),
                            ),
                    ),
            ),

            'Filters' =>
                array (
                    'FilterType' => 6,
                    'ComparisonType' => 0,
                    'Items' =>
                        array (
                            'FilterId' =>
                                array (
                                    'FilterType' => 1,
                                    'ComparisonType' => 3,
                                    'LeftExpression' =>
                                        array (
                                            'ExpressionType' => 0,
                                            'ColumnPath' => 'Contact.Id',
                                        ),
                                    'RightExpression' =>
                                        array (
                                            'ExpressionType' => 2,
                                            'Parameter' =>
                                                array (
                                                    'DataValueType' => 0,
                                                    'Value' => $contacttomerge,
                                                ),
                                        ),
                                ),
                        ),
                ),
        );
        $batshContact[1] = $data2;




        $data3 = array (
            "__type" => "Terrasoft.Nui.ServiceModel.DataContract.UpdateQuery",
            'RootSchemaName' => 'ContactCareer',
            'QueryType' => 1,
            'ColumnValues' => array(
                'Items' =>
                    array(
                        'Contact' =>
                            array(
                                'ExpressionType' => 2,
                                'Parameter' =>
                                    array(
                                        'DataValueType' => 0,
                                        'Value' => $maincontact,
                                    ),
                            ),
                    ),
            ),

            'Filters' =>
                array (
                    'FilterType' => 6,
                    'ComparisonType' => 0,
                    'Items' =>
                        array (
                            'FilterId' =>
                                array (
                                    'FilterType' => 1,
                                    'ComparisonType' => 3,
                                    'LeftExpression' =>
                                        array (
                                            'ExpressionType' => 0,
                                            'ColumnPath' => 'Contact.Id',
                                        ),
                                    'RightExpression' =>
                                        array (
                                            'ExpressionType' => 2,
                                            'Parameter' =>
                                                array (
                                                    'DataValueType' => 0,
                                                    'Value' => $contacttomerge,
                                                ),
                                        ),
                                ),
                        ),
                ),
        );
        $batshContact[2] = $data3;



        $data4 = array (
            "__type" => "Terrasoft.Nui.ServiceModel.DataContract.UpdateQuery",
            'RootSchemaName' => 'Activity',
            'QueryType' => 1,
            'ColumnValues' => array(
                'Items' =>
                    array(
                        'Contact' =>
                            array(
                                'ExpressionType' => 2,
                                'Parameter' =>
                                    array(
                                        'DataValueType' => 0,
                                        'Value' => $maincontact,
                                    ),
                            ),
                    ),
            ),

            'Filters' =>
                array (
                    'FilterType' => 6,
                    'ComparisonType' => 0,
                    'Items' =>
                        array (
                            'FilterId' =>
                                array (
                                    'FilterType' => 1,
                                    'ComparisonType' => 3,
                                    'LeftExpression' =>
                                        array (
                                            'ExpressionType' => 0,
                                            'ColumnPath' => 'Contact.Id',
                                        ),
                                    'RightExpression' =>
                                        array (
                                            'ExpressionType' => 2,
                                            'Parameter' =>
                                                array (
                                                    'DataValueType' => 0,
                                                    'Value' => $contacttomerge,
                                                ),
                                        ),
                                ),
                        ),
                ),
        );
        $batshContact[3] = $data4;


        $data5 = array (
            "__type" => "Terrasoft.Nui.ServiceModel.DataContract.UpdateQuery",
            'RootSchemaName' => 'Case',
            'QueryType' => 1,
            'ColumnValues' => array(
                'Items' =>
                    array(
                        'Contact' =>
                            array(
                                'ExpressionType' => 2,
                                'Parameter' =>
                                    array(
                                        'DataValueType' => 0,
                                        'Value' => $maincontact,
                                    ),
                            ),
                    ),
            ),

            'Filters' =>
                array (
                    'FilterType' => 6,
                    'ComparisonType' => 0,
                    'Items' =>
                        array (
                            'FilterId' =>
                                array (
                                    'FilterType' => 1,
                                    'ComparisonType' => 3,
                                    'LeftExpression' =>
                                        array (
                                            'ExpressionType' => 0,
                                            'ColumnPath' => 'Contact.Id',
                                        ),
                                    'RightExpression' =>
                                        array (
                                            'ExpressionType' => 2,
                                            'Parameter' =>
                                                array (
                                                    'DataValueType' => 0,
                                                    'Value' => $contacttomerge,
                                                ),
                                        ),
                                ),
                        ),
                ),
        );
        $batshContact[4] = $data5;



        $data6 = array (
            "__type" => "Terrasoft.Nui.ServiceModel.DataContract.UpdateQuery",
            'RootSchemaName' => 'Call',
            'QueryType' => 1,
            'ColumnValues' => array(
                'Items' =>
                    array(
                        'Contact' =>
                            array(
                                'ExpressionType' => 2,
                                'Parameter' =>
                                    array(
                                        'DataValueType' => 0,
                                        'Value' => $maincontact,
                                    ),
                            ),
                    ),
            ),

            'Filters' =>
                array (
                    'FilterType' => 6,
                    'ComparisonType' => 0,
                    'Items' =>
                        array (
                            'FilterId' =>
                                array (
                                    'FilterType' => 1,
                                    'ComparisonType' => 3,
                                    'LeftExpression' =>
                                        array (
                                            'ExpressionType' => 0,
                                            'ColumnPath' => 'Contact.Id',
                                        ),
                                    'RightExpression' =>
                                        array (
                                            'ExpressionType' => 2,
                                            'Parameter' =>
                                                array (
                                                    'DataValueType' => 0,
                                                    'Value' => $contacttomerge,
                                                ),
                                        ),
                                ),
                        ),
                ),
        );
        $batshContact[5] = $data6;



        $data7 = array (
            "__type" => "Terrasoft.Nui.ServiceModel.DataContract.UpdateQuery",
            'RootSchemaName' => 'ContactFile',
            'QueryType' => 1,
            'ColumnValues' => array(
                'Items' =>
                    array(
                        'Contact' =>
                            array(
                                'ExpressionType' => 2,
                                'Parameter' =>
                                    array(
                                        'DataValueType' => 0,
                                        'Value' => $maincontact,
                                    ),
                            ),
                    ),
            ),

            'Filters' =>
                array (
                    'FilterType' => 6,
                    'ComparisonType' => 0,
                    'Items' =>
                        array (
                            'FilterId' =>
                                array (
                                    'FilterType' => 1,
                                    'ComparisonType' => 3,
                                    'LeftExpression' =>
                                        array (
                                            'ExpressionType' => 0,
                                            'ColumnPath' => 'Contact.Id',
                                        ),
                                    'RightExpression' =>
                                        array (
                                            'ExpressionType' => 2,
                                            'Parameter' =>
                                                array (
                                                    'DataValueType' => 0,
                                                    'Value' => $contacttomerge,
                                                ),
                                        ),
                                ),
                        ),
                ),
        );
        $batshContact[6] = $data7;


        $data8 = array (
            "__type" => "Terrasoft.Nui.ServiceModel.DataContract.UpdateQuery",
            'RootSchemaName' => 'Contact',
            'QueryType' => 1,
            'ColumnValues' => array(
                'Items' =>
                    array(
                        'Account' =>
                            array(
                                'ExpressionType' => 2,
                                'Parameter' =>
                                    array(
                                        'DataValueType' => 0,
                                        'Value' => null,
                                    ),
                            ),
                    ),
            ),

            'Filters' =>
                array (
                    'FilterType' => 6,
                    'ComparisonType' => 0,
                    'Items' =>
                        array (
                            'FilterId' =>
                                array (
                                    'FilterType' => 1,
                                    'ComparisonType' => 3,
                                    'LeftExpression' =>
                                        array (
                                            'ExpressionType' => 0,
                                            'ColumnPath' => 'Id',
                                        ),
                                    'RightExpression' =>
                                        array (
                                            'ExpressionType' => 2,
                                            'Parameter' =>
                                                array (
                                                    'DataValueType' => 0,
                                                    'Value' => $contacttomerge,
                                                ),
                                        ),
                                ),
                        ),
                ),
        );
        $batshContact[7] = $data8;
        //$batshContact[0] = $data8;

        $data9 = array (
            "__type" => "Terrasoft.Nui.ServiceModel.DataContract.DeleteQuery",
            'RootSchemaName' => 'Contact',
            'OperationType' => 3,
            'Filters' =>
                array (
                    'FilterType' => 6,
                    'ComparisonType' => 0,
                    'Items' =>
                        array (
                            'FilterId' =>
                                array (
                                    'FilterType' => 1,
                                    'ComparisonType' => 3,
                                    'LeftExpression' =>
                                        array (
                                            'ExpressionType' => 0,
                                            'ColumnPath' => 'Id',
                                        ),
                                    'RightExpression' =>
                                        array (
                                            'ExpressionType' => 2,
                                            'Parameter' =>
                                                array (
                                                    'DataValueType' => 0,
                                                    'Value' => $contacttomerge,
                                                ),
                                        ),
                                ),
                        ),
                ),
        );
        $batshContact[8] = $data9;
        //$batshContact[1] = $data9;

        $contact = array(
            "items" => $batshContact
        );

        //$Data = json_encode($contragent, true);
        //pre($Data);
        $que = QueryBpm::jsonDataBpm($contact, BPM_URL_QUERY);

//print_r($que);
        if($que["status"] == 403)
        {
            QueryBpm::login();
            $que = QueryBpm::jsonDataBpm($contact, BPM_URL_QUERY);
        }

        if ($que["status"] == 200)
        {
            return true;
        }
        else
        {
            return false;
        }
    }


    public static function MergeBitrixContacts($maincontact,$contacttomerge)
    {
        $name = "";
        $lastName = "";
        $secondName = "";

        $result = CCrmContact::GetListEx(array(), array('=ID' => $maincontact, 'CHECK_PERMISSIONS' => 'N'), false, false, array('*', 'UF_*'));
        $fields = is_object($result) ? $result->Fetch() : null;
        if (is_array($fields)){

            $name = $fields['NAME'];
            $lastName = $fields['LAST_NAME'];
            $secondName = $fields['SECOND_NAME'];

            if(empty($fields['LAST_NAME']))
            {
                $lastName = "";
            }
            if(empty($fields['NAME']))
            {
                $name = "";
            }
            if(empty($fields['SECOND_NAME']))
            {
                $secondName = "";
            }

        }

        $criterion = new Bitrix\Crm\Integrity\DuplicatePersonCriterion($lastName, $name, $secondName);
        /*$duplicate = $criterion->find(\CCrmOwnerType::Undefined, 50);
        if ($duplicate !== null) {
            print_r($duplicate->getEntityArray());
        }*/

        $maincontactGUID = GetUserField("CRM_CONTACT", $maincontact, "UF_BPMCONTACTID");
        $contacttomergeGUID = GetUserField("CRM_CONTACT", $contacttomerge, "UF_BPMCONTACTID");

        if (($maincontactGUID != $contacttomergeGUID)&&!empty($maincontactGUID)&&!empty($contacttomergeGUID)) {

            if (ContactBitrixBpmMerge::MergeBpmContacts($maincontactGUID, $contacttomergeGUID)) {
                $mergers = new Bitrix\Crm\Merger\ContactMerger();
                $mergers->merge($contacttomerge, $maincontact, $criterion);
                return $maincontact;
            } else {
                return "-1";
            }
        }
        else
        {
            $mergers = new Bitrix\Crm\Merger\ContactMerger();
            $mergers->merge($contacttomerge, $maincontact, $criterion);
            return $maincontact;
        }
    }





    public static function KKTTOBPM($idbitrixkkt)
    {

        $nameforbpm="";
        $guidkktforbpm="";
        $companyidusingkkt="";
        $companyguidusingkkt="";
        $rnmkkt="";
        $usrfactorynumber="";
        $adresskkt="";
        $usrratecost="";

        $arSelect = Array("PROPERTY_GUID_KTT","NAME","PROPERTY_COMPANY","PROPERTY_RNM_KKT","PROPERTY_NUM_FN","PROPERTY_ADDR_KKT","PROPERTY_COST_TARIF");
        $arFilter = Array("IBLOCK_ID"=>"76","ID"=>$idbitrixkkt, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
        $res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
        if($ob = $res->GetNextElement())
        {
            $arFields = $ob->GetFields();  // print_r($arFields);
            $nameforbpm=$arFields['NAME'];
            $guidkktforbpm=$arFields['PROPERTY_GUID_KTT_VALUE'];
            $companyidusingkkt=$arFields['PROPERTY_COMPANY_VALUE'];
            $companyguidusingkkt=GetUserField("CRM_COMPANY", $companyidusingkkt, "UF_CRM_COMPANY_GUID");
            $usrfactorynumber=$arFields['PROPERTY_NUM_FN_VALUE'];
            $adresskkt=$arFields['PROPERTY_ADDR_KKT_VALUE'];
            $usrratecost=$arFields['PROPERTY_COST_TARIF_VALUE'];
            $rnmkkt=$arFields['PROPERTY_RNM_KKT_VALUE'];

        }

if(empty($guidkktforbpm))
{
    $guidkktforbpm=RestBpmBitrix::generate_guid();
    $ELEMENT_ID = $idbitrixkkt;  // код элемента
    $PROPERTY_CODE = "GUID_KTT";  // код свойства
    $PROPERTY_VALUE = $guidkktforbpm;  // значение свойства
    CIBlockElement::SetPropertyValuesEx($ELEMENT_ID, "76", array($PROPERTY_CODE => $PROPERTY_VALUE));
}

        $data1 = array (
            "__type" => "Terrasoft.Nui.ServiceModel.DataContract.InsertQuery",
            'RootSchemaName' => 'ConfItem',
            'OperationType' => 1,
            'ColumnValues' => array(
                'Items' =>
                    array(
                        'Category' =>
                            array(
                                'ExpressionType' => 2,
                                'Parameter' =>
                                    array(
                                        'DataValueType' => 0,
                                        'Value' => "da2cf289-b4a2-40dd-9795-f35c5c9bc70a"
                                    ),
                            ),
                        'Id' =>
                            array(
                                'ExpressionType' => 2,
                                'Parameter' =>
                                    array(
                                        'DataValueType' => 0,
                                        'Value' => $guidkktforbpm
                                    ),
                            ),
                        'Name' =>
                            array(
                                'ExpressionType' => 2,
                                'Parameter' =>
                                    array(
                                        'DataValueType' => 1,
                                        'Value' => $nameforbpm
                                    ),
                            ),
                        'UsrModelCashbox' =>
                            array(
                                'ExpressionType' => 2,
                                'Parameter' =>
                                    array(
                                        'DataValueType' => 1,
                                        'Value' => ""
                                    ),
                            ),
                        'UsrRnmCashbox' =>
                            array(
                                'ExpressionType' => 2,
                                'Parameter' =>
                                    array(
                                        'DataValueType' => 1,
                                        'Value' => $rnmkkt
                                    ),
                            ),
                        'UsrFactoryNumber' =>
                            array(
                                'ExpressionType' => 2,
                                'Parameter' =>
                                    array(
                                        'DataValueType' => 1,
                                        'Value' => $usrfactorynumber
                                    ),
                            ),
                        'PurchaseDate' =>
                            array(
                                'ExpressionType' => 2,
                                'Parameter' =>
                                    array(
                                        'DataValueType' => 8,
                                        'Value' => ""
                                    ),
                            ),
                        'CancelDate' =>
                            array(
                                'ExpressionType' => 2,
                                'Parameter' =>
                                    array(
                                        'DataValueType' => 8,
                                        'Value' => ""
                                    ),
                            ),
                        'Address' =>
                            array(
                                'ExpressionType' => 2,
                                'Parameter' =>
                                    array(
                                        'DataValueType' => 1,
                                        'Value' => $adresskkt
                                    ),
                            ),
                        'Status' =>
                            array(
                                'ExpressionType' => 2,
                                'Parameter' =>
                                    array(
                                        'DataValueType' => 0,
                                        'Value' => "D0C0AE7D-827A-4B63-9006-3A9949F81246"
                                    ),
                            ),
                        'UsrRateCost' =>
                            array(
                                'ExpressionType' => 2,
                                'Parameter' =>
                                    array(
                                        'DataValueType' => 1,
                                        'Value' => $usrratecost
                                    ),
                            ),
                        'UsrActivationDate' =>
                            array(
                                'ExpressionType' => 2,
                                'Parameter' =>
                                    array(
                                        'DataValueType' => 8,
                                        'Value' => ""
                                    ),
                            ),
                        'UsrDeactivationDate' =>
                            array(
                                'ExpressionType' => 2,
                                'Parameter' =>
                                    array(
                                        'DataValueType' => 8,
                                        'Value' => ""
                                    ),
                            ),
                        'UsrPromotionalCode' =>
                            array(
                                'ExpressionType' => 2,
                                'Parameter' =>
                                    array(
                                        'DataValueType' => 1,
                                        'Value' => ""
                                    ),
                            ),
                        'UsrActivationPeriod' =>
                            array(
                                'ExpressionType' => 2,
                                'Parameter' =>
                                    array(
                                        'DataValueType' => 1,
                                        'Value' => ""
                                    ),
                            ),
                        'UsrDeactivationPeriod' =>
                            array(
                                'ExpressionType' => 2,
                                'Parameter' =>
                                    array(
                                        'DataValueType' => 1,
                                        'Value' => ""
                                    ),
                            ),
                        'UsrRate' =>
                            array(
                                'ExpressionType' => 2,
                                'Parameter' =>
                                    array(
                                        'DataValueType' => 1,
                                        'Value' => ""
                                    ),
                            ),

                    ),
            ),

        );
        $batshContact[0] = $data1;



        $data2 = array (
            "__type" => "Terrasoft.Nui.ServiceModel.DataContract.InsertQuery",
            'RootSchemaName' => 'ConfItemUser',
            'QueryType' => 1,
            'ColumnValues' => array(
                'Items' =>
                    array(
                        'ConfItem' =>
                            array(
                                'ExpressionType' => 2,
                                'Parameter' =>
                                    array(
                                        'DataValueType' => 0,
                                        'Value' =>  $guidkktforbpm,
                                    ),
                            ),
                        'Account' =>
                            array(
                                'ExpressionType' => 2,
                                'Parameter' =>
                                    array(
                                        'DataValueType' => 0,
                                        'Value' => ""/*$companyguidusingkkt*/,
                                    ),
                            ),

                    ),
            ),
        );

        $logger = Logger::getLogger('kkttosendbpm','ofd.bitrix24/kkttosendbpm.txt');



        $batshContact[1] = $data2;
        //$batshContact[1] = $data9;

        $contact = array(
            "items" => $batshContact
        );
        $logger->log(array($contact));
        //$Data = json_encode($contragent, true);
        //pre($Data);
        $que = QueryBpm::jsonDataBpm($contact, BPM_URL_QUERY);
        $logger->log(array($que));
        print_r($que);

        if($que["status"] == 403)
        {
            QueryBpm::login();
            $que = QueryBpm::jsonDataBpm($contact, BPM_URL_QUERY);
            $logger->log(array($que));
            echo "<br>";
            print_r($que);
        }


        if ($que["status"] == 200)
        {
            return true;
        }
        else
        {
            return false;
        }
    }






}