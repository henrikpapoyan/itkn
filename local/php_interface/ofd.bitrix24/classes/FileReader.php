<?

final class FileReader
{
    protected $handler = null;
    protected $fbuffer = "";


    /*
     * Конструктор класса, открывающий файл для работы
     *
     * @param string $filename
     */
    public function __construct($filename)
    {
        if (!($this->handler = fopen($filename, "rb")))
            throw new Exception("Cannot open the file");
    }


    /*
     * Построчное чтение всего файла с учетом сдвига
     *
     * @return string
     */
    public function ReadAll()
    {
        if (!$this->handler)
            throw new Exception("Invalid file pointer");

        while (!feof($this->handler))
            $this->fbuffer .= fgets($this->handler);

        return $this->fbuffer;
    }


    /*
     * @param int $line
     */
    public function SetOffset($line)
    {
        if (!$this->handler)
            throw new Exception("Invalid file pointer");

        while (!feof($this->handler) && $line--) {
            fgets($this->handler);
        }
    }
}