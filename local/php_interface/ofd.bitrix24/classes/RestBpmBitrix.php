<?
use Bitrix\Main\CUserTypeEntity,
    Bitrix\Main\Loader;

Loader::includeModule("iblock");
Loader::includeModule("crm");

class RestBpmBitrix
{
    public static $statusQuery;

    public static function OnRestServiceBuildDescription()
    {
        return array(
            'bpmbitrix' => array(
                'bpm.contact.add' => array(
                    'callback' => array(__CLASS__, 'contactadd'),
                    'params' => array("contactId" => "", "name" => "", "phone" => "", "email" => "", "accountId" => "", "inn" => "", "kpp" => "")
                ),
                'bpm.contact.update' => array(
                    'callback' => array(__CLASS__, 'contactupdate'),
                    'params' => array("contactId" => "", "name" => "", "phone" => "", "email" => "","accountId" => "")
                ),
                'bpm.connect' => array(
                    'callback' => array(__CLASS__, 'connect'),
                    'params' => array("inn" => "")
                ),
                'bpm.kkt.update' => array(
                    'callback' => array(__CLASS__, 'kktUpdate'),
                    'params' => array("id" => "")
                ),

                'bpm.contragent.add' => array(
                    'callback' => array(__CLASS__, 'add'),
                    'params' => array("inn" => "")
                ),
                'bpm.contragent.update' => array(
                    'callback' => array(__CLASS__, 'update'),
                    'params' => array("inn" => "", "AccountId" => "", "ActDate" => "")
                ),
                'bpm.tiket.add' => array(
                    'callback' => array(__CLASS__, 'tiketAdd'),
                    'params' => array("id" => "", "number" => "")
                ),
                'bpm.lead.add' => array(
                    'callback' => array(__CLASS__, 'leadAdd'),
                    'params' => array("accountName" => "", "inn" => "", "kpp" => "", "contactId" => "", "subjectCase" => "", "leadSource" => "", "leadType" => "")
                ),
            )
        );
    }

    public static function kktUpdate($query, $n, \CRestServer $server)
    {
        $logger = Logger::getLogger('kktUpdate', 'ofd.bitrix24/kktUpdate.txt');
        $logger->log(array($query));

        if ($query['error']) {
            throw new \Bitrix\Rest\RestException(
                'Message',
                'ERROR_CODE',
                \CRestServer::STATUS_PAYMENT_REQUIRED
            );
            return array('error' => $query);
        }
        if (!isset($_SESSION["BPM_COOKIES"])) {
            QueryBpm::login();
        }
        $verify = KktBitrixBpm::eventSelectKkt(null, $query["params"]["id"]);
        $logger->log(array($verify));
        if ($verify["id"] != "-101") {

            $que = KktBitrixBpm::updateKkt($verify);
            $status = $que["status"];

            if ($status == 200) {
                //Add report query Bpm and Bitrix
                $arFieldsReport = Reports::prepareDataToReportQueryBPM("Update", "200", '', json_encode($query), "Updated successful", "KKT");
                Reports::addReportQueryBpm($arFieldsReport);

                return array("code" => 100, "result" => array("message" => "Updated successful", "Id" => $verify["guid"], "method" => "update"));
            } else {
                //Add report query Bpm and Bitrix
                $arFieldsReport = Reports::prepareDataToReportQueryBPM("Update", $status, '', json_encode($query), "Error $status", "KKT");
                Reports::addReportQueryBpm($arFieldsReport);

                return array("code" => 102, "result" => array("message" => "Error $status", "method" => "updated"));
            }
        } elseif ($verify["id"] == "-101") {
            //Add report query Bpm and Bitrix
            $arFieldsReport = Reports::prepareDataToReportQueryBPM("Update", "200", '', json_encode($query), "Item not found", "KKT");
            Reports::addReportQueryBpm($arFieldsReport);

            return array("code" => 101, "result" => array("message" => "Item not found", "Id" => ""));
        }
    }


    public static function contactadd($query, $n, \CRestServer $server)
    {
        $logger = Logger::getLogger('contactadd', 'ofd.bitrix24/contactadd.txt');
        $logger->log(array($query));

        $canwork = "1";

        /* session_start();
          //$_SESSION["NOTSENDELEMENT"]["1000"] = "1111";
          $logger->log(array($_SESSION["NOTSENDELEMENT"]));
          $logger->log(array($_SESSION));

          foreach ($_SESSION["NOTSENDELEMENT"] as $key => $value) {
              $logger->log(array($key->$value));
              if ($value == $query["params"]["contactId"]) {
                  $canwork = "0";
                  $logger->log(array($key));
              }

          }*/

        $logger->log(array($canwork));
        if ($canwork == "1") {
            $logger->log(array($query['error']));
            if ($query['error']) {
                throw new \Bitrix\Rest\RestException(
                    'Message',
                    'ERROR_CODE',
                    \CRestServer::STATUS_PAYMENT_REQUIRED
                );
                return array('error' => $query);
            }

            //$query["params"]["name"] = iconv('Windows-1251', 'utf-8', $query["params"]["name"]);
            $logger->log(array($query["params"]["name"]));

            if (!empty($query["params"]["name"])) {
                $verifedcontactid = "-1";

                $logger->log(array($query["params"]["contactId"]));

                if (!empty($query["params"]["contactId"])) {
                    $verifedcontactid = self::verifycontactid($query["params"]["contactId"]);
                    $logger->log(array("verifedcontactid  " . $verifedcontactid));
                }

                $logger->log(array($query["params"]["name"]));
                if ((!empty($query["params"]["phone"])) && ($verifedcontactid == "-1")) {
                    $logger->log(array("phone  " . $query["params"]["phone"]));
                    $logger->log(array("checkformatPhone  " . checkformatPhone($query["params"]["phone"])));
                    if (!checkformatPhone($query["params"]["phone"])) {
                        $newphone = convertPhone($query["params"]["phone"]);
                        $query["params"]["phone"] = $newphone;
                        $logger->log(array("newphone  " . $query["params"]["phone"]));
                    }
                    $verifyphoneperson = self::verifyphone($query["params"]["phone"]);
                    $logger->log(array("verifyphoneperson  " . $verifyphoneperson));
                    if ($verifyphoneperson == "-1") {
                        //$idelement = CrmBitrixBpm::addcontact($query["params"]);

                        $idelement = ContactBpmBitrix::addcontactToBitrix($query["params"]);
                        $logger->log(array("idelement  " . $idelement));
                        if ($idelement != "-1") {
                            //Add report query Bpm and Bitrix
                            $arFieldsReport = Reports::prepareDataToReportQueryBPM("Insert", "200", '', json_encode($query), "Inserted successful", "Contact");
                            Reports::addReportQueryBpm($arFieldsReport);

                            return array("code" => 100, "result" => array("message" => "Inserted successful", "method" => "insert"));
                        } else {
                            //Add report query Bpm and Bitrix
                            $arFieldsReport = Reports::prepareDataToReportQueryBPM("Insert", "200", '', json_encode($query), "Error from adding Bitrix", "Contact");
                            Reports::addReportQueryBpm($arFieldsReport);

                            return array("code" => 102, "result" => array("message" => "Error from adding Bitrix", "method" => "insert"));
                        }
                    } else {
                        //Add report query Bpm and Bitrix
                        $arFieldsReport = Reports::prepareDataToReportQueryBPM("Insert", "200", '', json_encode($query), "Contact with this telephone number already exists", "Contact");
                        Reports::addReportQueryBpm($arFieldsReport);

                        return array("code" => 102, "result" => array("message" => "Contact with this telephone number already exists", "method" => "insert"));
                    }
                } else if ($verifedcontactid != "-1") {
                    //Add report query Bpm and Bitrix
                    $arFieldsReport = Reports::prepareDataToReportQueryBPM("Insert", "200", '', json_encode($query), "Contact already exist", "Contact");
                    Reports::addReportQueryBpm($arFieldsReport);

                    return array("code" => 102, "result" => array("message" => "Contact already exist", "method" => "insert"));
                } else {
                    $idelement = ContactBpmBitrix::addcontactToBitrix($query["params"]);
                    $logger->log(array("idelement  " . $idelement));
                    if ($idelement != "-1") {
                        //Add report query Bpm and Bitrix
                        $arFieldsReport = Reports::prepareDataToReportQueryBPM("Insert", "200", '', json_encode($query), "Inserted successful", "Contact");
                        Reports::addReportQueryBpm($arFieldsReport);

                        return array("code" => 100, "result" => array("message" => "Inserted successful", "method" => "insert"));
                    } else {
                        //Add report query Bpm and Bitrix
                        $arFieldsReport = Reports::prepareDataToReportQueryBPM("Insert", "200", '', json_encode($query), "Error from adding Bitrix", "Contact");
                        Reports::addReportQueryBpm($arFieldsReport);

                        return array("code" => 102, "result" => array("message" => "Error from adding Bitrix", "method" => "insert"));
                    }
                }
            } else {
                //Add report query Bpm and Bitrix
                $arFieldsReport = Reports::prepareDataToReportQueryBPM("Insert", "200", '', json_encode($query), "Empty contact", "Contact");
                Reports::addReportQueryBpm($arFieldsReport);

                return array("code" => 101, "result" => array("message" => "Empty contact", "method" => "insert"));
            }
        }

    }

    public static function contactupdate($query, $n, \CRestServer $server)
    {
        $logger = Logger::getLogger('contactupdate', 'ofd.bitrix24/contactupdate.log');
        $logger->log(array($query));

        if ($query['error']) {
            throw new \Bitrix\Rest\RestException(
                'Message',
                'ERROR_CODE',
                \CRestServer::STATUS_PAYMENT_REQUIRED
            );
            return array('error' => $query);
        }

        if ((!empty($query["params"]["name"])) || (!empty($query["params"]["contactId"]))) {
            $logger->log("query['params']");
            $logger->log(array($query["params"]));
            if (!empty($query["params"]["contactId"])) {
                $verifedcontactid = self::verifycontactid($query["params"]["contactId"]);
                $logger->log(array("verifedcontactid  " . $verifedcontactid));
                if ($verifedcontactid != "-1") {
                    if (empty($query["params"]["mobilePhone"])) {
                        $idpesronwiththisphone = "-1";
                    } else {

                        $logger->log(array("mobilePhone  " . $query["params"]["mobilePhone"]));
                        $logger->log(array("checkformatPhone  " . checkformatPhone($query["params"]["mobilePhone"])));
                        if (!checkformatPhone($query["params"]["mobilePhone"])) {
                            $newphone = convertPhone($query["params"]["mobilePhone"]);
                            $query["params"]["mobilePhone"] = $newphone;
                            $logger->log(array("newphone  " . $query["params"]["mobilePhone"]));
                        }
                        $idpesronwiththisphone = self::verifyphone($query["params"]["mobilePhone"]);
                    }
                    $logger->log(array("idpesronwiththisphone  " . $idpesronwiththisphone));
                    if (($idpesronwiththisphone == $verifedcontactid) || ($idpesronwiththisphone == "-1")) {
                        //Add report query Bpm and Bitrix
                        $arFieldsReport = Reports::prepareDataToReportQueryBPM("Update", "200", '', json_encode($query), "Update successful", "Contact");
                        Reports::addReportQueryBpm($arFieldsReport);

                        ContactBpmBitrix::updateToBitrix($verifedcontactid, $query["params"]);
                        return array("code" => 100, "result" => array("message" => "update successful", "method" => "update"));
                    } else {
                        //Add report query Bpm and Bitrix
                        $arFieldsReport = Reports::prepareDataToReportQueryBPM("Update", "200", '', json_encode($query), "Contact with this telephone number already exists", "Contact");
                        Reports::addReportQueryBpm($arFieldsReport);
                        return array("code" => 101, "result" => array("message" => "Contact with this telephone number already exists", "method" => "update"));
                    }
                } else {
                    //Add report query Bpm and Bitrix
                    $arFieldsReport = Reports::prepareDataToReportQueryBPM("Update", "200", '', json_encode($query), "СontactId not found", "Contact");
                    Reports::addReportQueryBpm($arFieldsReport);
                    return array("code" => 102, "result" => array("message" => "СontactId not found", "method" => "update"));
                }
            } else {
                //Add report query Bpm and Bitrix
                $arFieldsReport = Reports::prepareDataToReportQueryBPM("Update", "200", '', json_encode($query), "СontactId empty", "Contact");
                Reports::addReportQueryBpm($arFieldsReport);
                return array("code" => 101, "result" => array("message" => "СontactId empty", "method" => "update"));
            }


        } else {
            //Add report query Bpm and Bitrix
            $arFieldsReport = Reports::prepareDataToReportQueryBPM("Insert", "200", '', json_encode($query), "Empty contact", "Contact");
            Reports::addReportQueryBpm($arFieldsReport);
            return array("code" => 103, "result" => array("message" => "Empty contact", "method" => "update"));
        }

    }

    public static function verifycontactid($contactId)
    {
        $logger = Logger::getLogger('verifycontactid', 'ofd.bitrix24/verifycontactid.log');
        $res = CCrmContact::GetList(array(), array('=UF_BPMCONTACTID' => $contactId));
        if ($arRes = $res->Fetch()) {
            $logger->log(array($arRes));
            return $arRes['ID'];
        }
        return "-1";
    }

    public static function verifyphone($phone)
    {
        $res = CCrmFieldMulti::GetList(array('ID' => 'asc'), array('TYPE_ID' => "PHONE"/*,"VALUE_TYPE"=>"MOBILE"*/));
        $resultid = "-1";
        while ($arRes = $res->Fetch()) {
            if ($arRes['VALUE'] == $phone) {
                $resultid = $arRes['ELEMENT_ID'];
                $logger = Logger::getLogger('verifyphone', 'ofd.bitrix24/verifyphone.log');
                $logger->log(array($arRes));
                break;
            }
        }
        return $resultid;
    }


    public static function connect($query, $n, \CRestServer $server)
    {
        if ($query['error']) {
            throw new \Bitrix\Rest\RestException(
                'Message',
                'ERROR_CODE',
                \CRestServer::STATUS_PAYMENT_REQUIRED
            );
            return array('error' => $query);
        }
        $verify = self::verify($query["params"]["inn"]);

        if (!empty($verify)) {
            $result = array('message' => "OK", "accountId" => $verify["guid"]);
            return $result;
        } else {
            $result = array('message' => "Ошибка - контрагент не найден", "accountId" => "");
            return $result;
        }

    }

    public static function verify($inn = null, $id = null, $guid = null, $kpp = null)
    {
        $entity_id = "CRM_COMPANY";
        $uf_guid = "UF_CRM_COMPANY_GUID";
        if (!empty($guid)) {
            $guid = strtoupper($guid);
            $res = CCrmCompany::GetList(array(), array('=UF_CRM_COMPANY_GUID' => $guid));
            if ($arRes = $res->Fetch()) {
                $id = $arRes['ID'];
            }
        }
		

        $req = new \Bitrix\Crm\EntityRequisite();
        if ($id == null and $kpp == null) {
            $rser = $req->getList(array(
                "filter" => array(
                    'RQ_INN' => $inn,
                    "ENTITY_TYPE_ID" => CCrmOwnerType::Company,
                    'PRESET_ID' => array(1, 2)
                )
            ));
            $rows = $rser->fetchAll();
            foreach ($rows as $company) {
                $id = $company["ENTITY_ID"];
            }
        } elseif ($id == null and $kpp != null) {
            $rser = $req->getList(array(
                "filter" => array(
                    'RQ_INN' => $inn,
                    'RQ_KPP' => $kpp,
                    "ENTITY_TYPE_ID" => CCrmOwnerType::Company,
                    'PRESET_ID' => array(1, 2)
                )
            ));
            $rows = $rser->fetchAll();
            foreach ($rows as $company) {
            }
        } else {
            $rser = $req->getList(array(
                "filter" => array(
                    'ENTITY_ID' => $id,
                    "ENTITY_TYPE_ID" => CCrmOwnerType::Company,
                    'PRESET_ID' => array(1, 2)
                )
            ));
            $rows = $rser->fetchAll();
            foreach ($rows as $company) {
            }
	    }


        if (!empty($id) or !empty($inn)) {

            $guidget = GetUserField($entity_id, $id, $uf_guid);

            $actulDataTimeAdd = metkaColTime($id, CONTRAGENT_METKA_ADD, 0);
            $actulDataTime = metkaColTime($id, CONTRAGENT_METKA_UPDATE, 1);

            if (!empty($guidget)) {

                if ($company["ENTITY_TYPE_ID"] == 4) {
                    $result = array("dataUpdateTime" => $actulDataTime["t"], "id" => $id, "guid" => $guidget, "inn" => $company["RQ_INN"], "kpp" => $company["RQ_KPP"], "ogrnip" => $company["RQ_OGRNIP"], "okpo" => $company["RQ_OKPO"], "oktmo" => $company["RQ_OKTMO"]);
                } else {
                    $result = array("dataUpdateTime" => $actulDataTime["t"], "id" => $id, "guid" => $guidget, "inn" => $company["RQ_INN"], "kpp" => $company["RQ_KPP"], "fulname" => $company["RQ_COMPANY_FULL_NAME"], "ogrn" => $company["RQ_OGRN"], "okpo" => $company["RQ_OKPO"], "oktmo" => $company["RQ_OKTMO"]);
                }
            } else {
                $guid = self::generate_guid();
                SetUserField($entity_id, $id, $uf_guid, $guid);
                if ($company["ENTITY_TYPE_ID"] == 4) {
                    $result = array("dataUpdateTime" => $actulDataTime["t"], "id" => $id, "guid" => $guid, "inn" => $company["RQ_INN"], "kpp" => $company["RQ_KPP"], "ogrnip" => $company["RQ_OGRNIP"], "okpo" => $company["RQ_OKPO"], "oktmo" => $company["RQ_OKTMO"]);
                } else {
                    $result = array("dataUpdateTime" => $actulDataTime["t"], "id" => $id, "guid" => $guid, "inn" => $company["RQ_INN"], "kpp" => $company["RQ_KPP"], "fulname" => $company["RQ_COMPANY_FULL_NAME"], "ogrn" => $company["RQ_OGRN"], "okpo" => $company["RQ_OKPO"], "oktmo" => $company["RQ_OKTMO"]);
                }
            }
            return $result;
        } else {
            return false;
        }
    }

    public static function generate_guid()
    {
        if (function_exists('com_create_guid')) {
            return com_create_guid();
        } else {
            mt_srand((double)microtime() * 10000);
            $charid = strtoupper(md5(uniqid(rand(), true)));
            $hyphen = chr(45);// "-"
            $uuid = substr($charid, 0, 8) . $hyphen
                . substr($charid, 8, 4) . $hyphen
                . substr($charid, 12, 4) . $hyphen
                . substr($charid, 16, 4) . $hyphen
                . substr($charid, 20, 12);
            return $uuid;
        }
    }

    public static function add($query, $n, \CRestServer $server)
    {
        if ($query['error']) {
            throw new \Bitrix\Rest\RestException(
                'Message',
                'ERROR_CODE',
                \CRestServer::STATUS_PAYMENT_REQUIRED
            );
            return array('error' => $query);
        }
        if (!isset($_SESSION["BPM_COOKIES"])) {
            QueryBpm::login();
        }

        $verify = self::verify($query["params"]["inn"]);
        $logger = Logger::getLogger('ContragentInn', 'ofd.bitrix24/ContragentInn.log');
        $logger->log($query, $n, $server);
        if (!empty($verify)) {
            //собираю массив
            $que = CrmBitrixBpm::contragenAdd($verify);
            $status = $que["status"];
            if ($status == 200) {
                //Add report query Bpm and Bitrix
                $arFieldsReport = Reports::prepareDataToReportQueryBPM("Insert", "200", '', json_encode($query), "Inserted successful", "Company");
                Reports::addReportQueryBpm($arFieldsReport);

                return array("code" => 100, "result" => array("message" => "Inserted successful", "accountId" => $verify["guid"], "method" => "insert"));
            } else {
                //Add report query Bpm and Bitrix
                $arFieldsReport = Reports::prepareDataToReportQueryBPM("Insert", $status, '', json_encode($query), "Error $status", "Company");
                Reports::addReportQueryBpm($arFieldsReport);

                return array("code" => 102, "result" => array("message" => "Error $status", "method" => "insert"));
            }
        } else {
            //Add report query Bpm and Bitrix
            $arFieldsReport = Reports::prepareDataToReportQueryBPM("Insert", "200", '', json_encode($query), "Item not found", "Company");
            Reports::addReportQueryBpm($arFieldsReport);

            return array("code" => 101, "result" => array("message" => "Item not found", "accountId" => ""));
        }
    }

    public static function update($query, $n, \CRestServer $server)
    {
        if ($query['error']) {
            throw new \Bitrix\Rest\RestException(
                'Message',
                'ERROR_CODE',
                \CRestServer::STATUS_PAYMENT_REQUIRED
            );
            return array('error' => $query);
        }
		
		$logger = Logger::getLogger('updatetestbpm', 'ofd.bitrix24/updatetestbpm.txt');
		$logger->log("params");
        $logger->log($query["params"]);
		
        if (!isset($_SESSION["BPM_COOKIES"])) {
            QueryBpm::login();
        }
        if (!empty($query["params"]["AccountId"])) {
            $verify = self::verify($query["params"]["inn"], null, $query["params"]["AccountId"]);
			$logger->log("verify1");
			$logger->log($verify);
        }
        if (empty($verify["id"]) /*or empty($verify["inn"])*/) {
            $verify = self::verify($query["params"]["inn"]);
			$logger->log("verify2");
			$logger->log($verify);
        }
        if (empty($verify) or(strtoupper($verify['guid'])!=strtoupper($query["params"]["AccountId"]))) {
            //Add report query Bpm and Bitrix
            $arFieldsReport = Reports::prepareDataToReportQueryBPM("Update", "404", '', json_encode($query), "Item not found", "Company");
            Reports::addReportQueryBpm($arFieldsReport);
            $logger->log($arFieldsReport);
			
            return array("code" => 101, "result" => array("message" => "Item not found", "accountId" => '', "method" => ""));
        }
		
		if (empty($verify["inn"])) {
			$verify["inn"]=$query["params"]["inn"];
        }
		
		$guid = $verify["guid"];
        $que = CrmBitrixBpmUpdate::contragenUpdate($verify);
		$logger->log("que");
		$logger->log($que);
		$logger->log("guid");
		$logger->log($guid);




        $status = $que["status"];
        if ($status == 200) {
            //Add report query Bpm and Bitrix
            $arFieldsReport = Reports::prepareDataToReportQueryBPM("Update", "200", '', json_encode($query), "Updated successful", "Company");
            Reports::addReportQueryBpm($arFieldsReport);

            metkaColTime($verify["id"], CONTRAGENT_METKA_ADD, 1);
            metkaColTime($verify["id"], CONTRAGENT_METKA_UPDATE, 1);
            $result = array("code" => 100, "result" => array("message" => "Updated successful", "accountId" => "$guid", "method" => "update"));
        } else {
            //Add report query Bpm and Bitrix
            $arFieldsReport = Reports::prepareDataToReportQueryBPM("Update", $status, '', json_encode($query), "Error: update failed $status", "Company");
            Reports::addReportQueryBpm($arFieldsReport);

            $result = array("code" => 102, "result" => array("message" => "Error: update failed $status", "accountId" => "$guid", "method" => "update"));
        }
        return $result;
    }

    public static function tiketAdd($query, $n, \CRestServer $server)
    {
        if ($query['error']) {
            throw new \Bitrix\Rest\RestException(
                'Message',
                'ERROR_CODE',
                \CRestServer::STATUS_PAYMENT_REQUIRED
            );
            return array('error' => $query);
        }
        if (!isset($_SESSION["BPM_COOKIES"])) {
            QueryBpm::login();
        }
        $idTiket = strtoupper($query["params"]["id"]);
        if (!empty($idTiket)) {
            $logger = Logger::getLogger('TiketConnect', 'ofd.bitrix24/TiketConnect.log');
            $logger->log($query["params"]);
            $result = TiketBpmBitrix::getTiket($idTiket);
            $status = $result["status"];
            if ($result["add"] == "Y") {
                //Add report query Bpm and Bitrix
                $arFieldsReport = Reports::prepareDataToReportQueryBPM("Insert", "200", '', json_encode($query), "Adding succesfull", "Tiket");
                Reports::addReportQueryBpm($arFieldsReport);

                return array("code" => 100, "result" => array("message" => "Adding succesfull", "method" => "insert"));
            }
            if ($status == 200 and $result["add"] = "U") {
                //Add report query Bpm and Bitrix
                $arFieldsReport = Reports::prepareDataToReportQueryBPM("Insert", "200", '', json_encode($query), "Update", "Tiket");
                Reports::addReportQueryBpm($arFieldsReport);

                return array("code" => 101, "result" => array("message" => "update", "method" => "insert"));
            }
            if ($status != 200) {
                //Add report query Bpm and Bitrix
                $arFieldsReport = Reports::prepareDataToReportQueryBPM("Insert", $status, '', json_encode($query), "Error $status", "Tiket");
                Reports::addReportQueryBpm($arFieldsReport);

                return array("code" => 102, "result" => array("message" => "Error $status", "method" => "insert"));
            }
        }
    }

    public static function leadAdd($query, $n, \CRestServer $server)
    {
        if ($query['error']) {
            throw new \Bitrix\Rest\RestException(
                'Message',
                'ERROR_CODE',
                \CRestServer::STATUS_PAYMENT_REQUIRED
            );
            return array('error' => $query);
        }

        if (!isset($_SESSION["BPM_COOKIES"])) {
            QueryBpm::login();
        }



        $logger = Logger::getLogger('LeadAdd', 'ofd.bitrix24/LeadAdd.log');
        $logger->log($query, $n, $server);

        $que = LeadBpmBitrix::leadAdd($query["params"]);
        $logger->log("RESULT: " . $que['MESSAGE']);

        //Add report query Bpm and Bitrix
        $arFieldsReport = Reports::prepareDataToReportQueryBPM("Insert", "200", '', json_encode($query), $que['MESSAGE'], "Lead");
        Reports::addReportQueryBpm($arFieldsReport);

        return array("code" => $que['STATUS'], "result" => array("message" => $que['MESSAGE'], "method" => $que['METHOD']));
    }
}