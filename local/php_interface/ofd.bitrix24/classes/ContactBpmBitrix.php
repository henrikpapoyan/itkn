<?php

use Bitrix\Main\Loader;

Loader::includeModule("iblock");
Loader::includeModule("crm");
class ContactBpmBitrix
{





 public static function contragenAdd($verifity = array())
    {

		/* if(empty($verifity["id"]) || empty($verifity["inn"])){
			//print_r("444444");
            return false;
}*/
        $dbContragentGet = CrmBitrixBpm::GetListEx(
            array(),
            array('ID' => $verifity["id"]),
            false,
            false,
            array("TITLE","UF_CRM_COMPANY_GUID", "UF_CRM_1486035239", "UF_CRM_1486033688", "UF_CRM_1486035185", "COMPANY_TYPE", "INDUSTRY", "ASSIGNED_BY_ID", "UF_CRM_COMPANY_GUID")
        );
        while ($dbContragent = $dbContragentGet->GetNext()) {

            //COMPANY_TYPE тип компании
            switch ($dbContragent["COMPANY_TYPE"]) {
                case "PARTNER":
                    $typCompany = BPM_COMPANY_PARTNER;
                    break;
                case "CUSTOMER":
                    $typCompany = BPM_COMPANY_CLIENT;
                    break;
                case "2":
                    $typCompany = BPM_COMPANY_CLIENTAG;
                    break;
                case "RESELLER":
                    $typCompany = BPM_COMPANY_POSTAVCHIK;
                    break;
                case "OTHER":
                    $typCompany = BPM_COMPANY_PODRYDHIK;
                    break;
                case "1":
                    $typCompany = BPM_COMPANY_GPX;
                    break;
                default:
                    $typCompany = BPM_COMPANY_CLIENT;
            }
            //ответственный
            $manager = ownerIdGuid ($dbContragent["ASSIGNED_BY_ID"]);
            //дата подключения к офд
            if ($dbContragent["UF_CRM_1486033688"]) {
                $dataO = $dbContragent["UF_CRM_1486033688"];
                $dataOfd = dataTimeformat($dataO, null, "Y");
            }
            //организационная форма
            if ($dbContragent["UF_CRM_1486035185"]) {
                $dbContragent["USER"] = GetUserField("CRM_COMPANY", $dbContragent["ID"], "UF_CRM_1486035185");
                //орг форма
                switch ($dbContragent["UF_CRM_1486035185"]) {
                    case "63":
                        $Ownership = BPM_ORG_OOO;
                        break;
                    case "64":
                        $Ownership = BPM_ORG_OAO;
                        break;
                    case "65":
                        $Ownership = BPM_ORG_AO;
                        break;
                    case "66":
                        $Ownership = BPM_ORG_ZAO;
                        break;
                    case "67":
                        $Ownership = BPM_ORG_IP;
                        break;
                    case "92":
                        $Ownership = BPM_ORG_PAO;
                        break;
                    case "68":
                        $Ownership = BPM_ORG_FGUP;
                        break;
                    case "69":
                        $Ownership = BPM_ORG_KP;
                        break;
                    case "70":
                        $Ownership = BPM_ORG_GU;
                        break;
                    case "97":
                        $Ownership = BPM_ORG_HL;
                        break;
                    default:
                        $Ownership = BPM_ORG_OOO;
                }

            }

            //Industry - Вид деятельности компании (UF_CRM_1486035239)
            if ($dbContragent["UF_CRM_1486035239"]) {
                $dbContragent["USER"] = GetUserField("CRM_COMPANY", $dbContragent["ID"], "UF_CRM_1486035239");
                switch ($dbContragent["UF_CRM_1486035239"]) {
                    case "71":
                        $activity = BPM_ACTIVITY_PRODUCTS;
                        break;
                    case "72":
                        $activity = BPM_ACTIVITY_SERVICES;
                        break;
                    case "73":
                        $activity = BPM_ACTIVITY_TRADE;
                        break;
                    case "74":
                        $activity = BPM_ACTIVITY_CTO;
                        break;
                    case "75":
                        $activity = BPM_ACTIVITY_INTEGRATOR;
                        break;
                    case "76":
                        $activity = BPM_ACTIVITY_EDO;
                        break;
                    case "77":
                        $activity = BPM_ACTIVITY_MARKETPLACE;
                        break;
                    case "95":
                        $activity = BPM_ACTIVITY_DEVELOPMENT;
                        break;
                    case "96":
                        $activity = BPM_ACTIVITY_MORE;
                        break;
                    default:
                        $activity = BPM_ACTIVITY_MORE;
                }
            }

            $verifity["activity"] = $activity;
            $verifity["Ownership"] = $Ownership;
            $verifity["dataOfd"] = $dataOfd;
            $verifity["manager"] = $manager;
            $verifity["typCompany"] = $typCompany;

            if(empty($verifity["guid"])){
                $verifity["guid"] = $dbContragent["UF_CRM_COMPANY_GUID"];
            }
            if(empty($verifity["ogrn"])){
                $verifity["ogrn"] = $verifity["ogrnip"];
            }
            if (!empty($verifity["dataOfd"])) {
                $dtim = null;
            }else{
                $dt = dataTimeformat("Y");
                $dtim = "\"$dt\"";
            }
            if(empty($verifity["fulname"])){
                $verifity["fulname"] = trim($dbContragent["TITLE"]);
            }
        }
        $req = new \Bitrix\Crm\EntityRequisite();
        $rser = $req->getList(array(
            "filter" => array(
                "ENTITY_ID" => $verifity["id"],
                "ENTITY_TYPE_ID" => CCrmOwnerType::Company,
                "PRESET_ID" => array(1,2)
            )
        ));
        $rows = $rser->fetchAll();
        foreach ($rows as $company) {
            $params = array(
                'filter' => array(
                    'ENTITY_ID' => $company['ID'],
                    'ENTITY_TYPE_ID' => CCrmOwnerType::Requisite)
            );
            $bank = new \Bitrix\Crm\EntityBankDetail();
            $dbRes = $bank->getList(array(
                'filter' => array('ENTITY_ID' => $company['ID'])
            ));
            $rowsd = $dbRes->fetchAll();
            foreach ($rowsd as $k => $vale) {
                switch ($k) {
                    default:
                        $verifity["bank"][$k] = $vale;
                }

            }
            $adress = Bitrix\Crm\EntityRequisite::getAddresses($company['ID']);
            $dbResMultiFields = CCrmFieldMulti::GetList(array(), array('ENTITY_ID' => 'COMPANY', 'ELEMENT_ID' => $verifity["id"]));
            while ($arMultiFields = $dbResMultiFields->Fetch()) {
                $comunicetions[] = $arMultiFields;
            }
            foreach ($comunicetions as $com => $value) {
                if ($value["TYPE_ID"] == "PHONE") {
                    switch ($value["VALUE_TYPE"]) {
                        case "WORK":
                            $value["COMMUN_TYP"] = BPM_COMMUNICATION_WORK_PHONE;
                            break;
                        case "MOBILE":
                            $value["COMMUN_TYP"] = BPM_COMMUNICATION_MOBFHONE;
                            break;
                        case "FAX":
                            $value["COMMUN_TYP"] = BPM_COMMUNICATION_FAX;
                            break;
                        case "OTHER":
                            $value["COMMUN_TYP"] = BPM_COMMUNICATION_OTHER_PHONE;
                            break;
                    }
                }
                if ($value["TYPE_ID"] == "EMAIL") {
                    switch ($value["VALUE_TYPE"]) {
                        case "WORK":
                            $value["COMMUN_TYP"] = BPM_COMMUNICATION_EMAIL;
                            break;
                        case "OTHER":
                            $value["COMMUN_TYP"] = BPM_COMMUNICATION_EMAIL;
                            break;
                    }
                }
                if ($value["TYPE_ID"] == "IM") {
                    switch ($value["VALUE_TYPE"]) {
                        case "SKYPE":
                            $value["COMMUN_TYP"] = BPM_COMMUNICATION_SKYPE;
                            break;
                    }
                }
                if ($value["TYPE_ID"] == "WEB") {
                    switch ($value["VALUE_TYPE"]) {
                        case "WORK":
                            $value["COMMUN_TYP"] = BPM_COMMUNICATION_URL;
                            break;
                        case "TWITTER":
                            $value["COMMUN_TYP"] = BPM_COMMUNICATION_TWITTER;
                            break;
                        case "FACEBOOK":
                            $value["COMMUN_TYP"] = BPM_COMMUNICATION_FACEBOOK;
                            break;
                        default:
                            $value["COMMUN_TYP"] = BPM_COMMUNICATION_URL;
                    }
                }
                $comunic[] = $value;
            }
            foreach ($adress as $key => $val) {
                switch ($key) {
                    case 6:
                        $verifity["adress"][$key]["typAdress"] = BPM_ADRESS_UR;
                        $verifity["adress"][$key]["adressValu"] = $val["ADDRESS_1"] . " ";
                        break;
                    case 1:
                        $verifity["adress"][$key]["typAdress"] = BPM_ADRESS_REAL;
                        $verifity["adress"][$key]["adressValu"] = $val["ADDRESS_1"] . " ";
                        break;
                    default:
                        $verifity["adress"][$key]["typAdress"] = BPM_ADRESS_OTHER;
                        $verifity["adress"][$key]["adressValu"] = $val["ADDRESS_1"] . " ";
                }
            }
        }

        $datatime = dataTimeformat();
        //$verifity "id","guid","inn","kpp","fulname","ogrn","okpo","oktmo"

        //Основная информация о контрагенте
        $Account = array(
            "__type" => "Terrasoft.Nui.ServiceModel.DataContract.InsertQuery",
            "RootSchemaName" => "Account",
            "OperationType" => 1,
            "ColumnValues" => array(
                "Items" => array(
                    "Id" => array(
                        "ExpressionType" => 2,
                        "Parameter" => array(
                            "DataValueType" => 0,
                            "Value" => $verifity["guid"]
                        )
                    ),
                    "Name" => array(
                        "ExpressionType" => 2,
                        "Parameter" => array(
                            "DataValueType" => 1,
                            "Value" => htmlspecialcharsBack($verifity["fulname"]),
                        )
                    ),
                    "Usraidiagent" => array(
                        "ExpressionType" => 2,
                        "Parameter" => array(
                            "DataValueType" => 1,
                            "Value" => $verifity["inn"],
                        )
                    ),
                    "UsrKPP" => array(
                        "ExpressionType" => 2,
                        "Parameter" => array(
                            "DataValueType" => 1,
                            "Value" => $verifity["kpp"],
                        )
                    ),
                    "Ownership" => array(
                        "ExpressionType" => 2,
                        "Parameter" => array(
                            "DataValueType" => 0,
                            "Value" => $verifity["Ownership"],
                        ),
                    ),
                    "Type" => array(
                        "ExpressionType" => 2,
                        "Parameter" => array(
                            "DataValueType" => 0,
                            "Value" => $verifity["typCompany"],
                        ),
                    ),
                    "Industry" => array(
                        "ExpressionType" => 2,
                        "Parameter" => array(
                            "DataValueType" => 0,
                            "Value" => $verifity["activity"],
                        ),
                    ),
                    "Owner" => array(
                        "ExpressionType" => 2,
                        "Parameter" => array(
                            "DataValueType" => 0,
                            "Value" => $verifity["manager"],
                        ),
                    ),
                    "UsrOGRN" => array(
                        "ExpressionType" => 2,
                        "Parameter" => array(
                            "DataValueType" => 1,
                            "Value" => $verifity["ogrn"],
                        ),
                    ),
                    "UsrOFDConnectionDate" => array(
                        "ExpressionType" => 2,
                        "Parameter" => array(
                            "DataValueType" => 8,
                            "Value" => $dtim,
                        ),
                    ),
                    "UsrOKPO" => array(
                        "ExpressionType" => 2,
                        "Parameter" => array(
                            "DataValueType" => 1,
                            "Value" => $verifity["okpo"],
                        ),
                    ),
                    "UsrOKTMO" => array(
                        "ExpressionType" => 2,
                        "Parameter" => array(
                            "DataValueType" => 1,
                            "Value" => $verifity["oktmo"],
                        ),
                    ),
                    "UsrBPMActDate" => array(
                        "ExpressionType" => 2,
                        "Parameter" =>
                            array(
                                "DataValueType" => 7,
                                "Value" => "\"$datatime\"",
                            )
                    )
                )
            ));
        $batshContragent[0] = $Account;
        //Комуникации
        foreach ($comunic as $comu => $val) {
            if (!empty($val["COMMUN_TYP"])) {
                $AccountCommunication = array(
                    "__type" => "Terrasoft.Nui.ServiceModel.DataContract.InsertQuery",
                    'RootSchemaName' => 'AccountCommunication',
                    'OperationType' => 1,
                    'ColumnValues' => array(
                        'Items' =>
                            array(
                                'Account' =>
                                    array(
                                        'ExpressionType' => 2,
                                        'Parameter' =>
                                            array(
                                                'DataValueType' => 0,
                                                'Value' => $verifity["guidd"],
                                            ),
                                    ),
                                'Number' =>
                                    array(
                                        'ExpressionType' => 2,
                                        'Parameter' =>
                                            array(
                                                'DataValueType' => 1,
                                                'Value' => $val["VALUE"],
                                            ),
                                    ),
                                'CommunicationType' =>
                                    array(
                                        'ExpressionType' => 2,
                                        'Parameter' =>
                                            array(
                                                'DataValueType' => 0,
                                                'Value' => $val["COMMUN_TYP"],
                                            ),
                                    ),
                            ),
                    ),
                );
                $batshContragent[] = $AccountCommunication;
            }
        }
        //Множественные адреса
        foreach ($verifity["adress"] as $num => $adrval) {
            $addAddress = array(
                "__type" => "Terrasoft.Nui.ServiceModel.DataContract.InsertQuery",
                "RootSchemaName" => "AccountAddress",
                "OperationType" => 1,
                "ColumnValues" => array(
                    "Items" => array(
                        'Account' =>
                            array(
                                'ExpressionType' => 2,
                                'Parameter' =>
                                    array(
                                        'DataValueType' => 0,
                                        'Value' => $verifity["guid"],
                                    ),
                            ),
                        'Address' =>
                            array(
                                'ExpressionType' => 2,
                                'Parameter' =>
                                    array(
                                        'DataValueType' => 1,
                                        'Value' => $adrval["adressValu"],
                                    ),
                            ),
                        'AddressType' =>
                            array(
                                'ExpressionType' => 2,
                                'Parameter' =>
                                    array(
                                        'DataValueType' => 0,
                                        'Value' => $adrval["typAdress"],
                                    ),
                            ),

                    )
                )
            );
            if(!empty($adrval)){
                $batshContragent[] = $addAddress;
            }

        }
        //Множественные банки
        foreach ($verifity["bank"] as $n => $b) {
            $bank = array(
                "__type" => "Terrasoft.Nui.ServiceModel.DataContract.InsertQuery",
                "RootSchemaName" => "AccountBillingInfo",
                "OperationType" => 1,
                "ColumnValues" => array(
                    "Items" => array(
                        'Account' =>
                            array(
                                'ExpressionType' => 2,
                                'Parameter' =>
                                    array(
                                        'DataValueType' => 0,
                                        'Value' => $verifity["guid"],
                                    ),
                            ),
                        'Name' =>
                            array(
                                'ExpressionType' => 2,
                                'Parameter' =>
                                    array(
                                        'DataValueType' => 1,
                                        'Value' => $b["RQ_BANK_NAME"],
                                    ),
                            ),
                        'BillingInfo' =>
                            array(
                                'ExpressionType' => 2,
                                'Parameter' =>
                                    array(
                                        'DataValueType' => 1,
                                        'Value' => "Р/с: ".$b["RQ_ACC_NUM"]." Банк: ".$b["RQ_BANK_NAME"]." БИК: ".$b["RQ_BIK"]." К/с: ".$b["RQ_COR_ACC_NUM"],
                                    ),
                            ),

                    )
                )
            );
            if(!empty($b) and !empty($b["RQ_BANK_NAME"])){
                $batshContragent[] = $bank;
            }
        }

        $contragent = array(
            "items" => $batshContragent
        );

        //$Data = json_encode($contragent, true);
        //pre($Data);
        $que = QueryBpm::jsonDataBpmContr($contragent, BPM_URL_QUERY);
		//$logger = Logger::getLogger('creatContragent','ofd.bitrix24/contragent/creatContragent.log');
		// $logger->log(array($contragent,$que));

        if($que["status"] == 403){
            $q = QueryBpm::jsonDataBpmContr($contragent, BPM_URL_QUERY);
			// $logger = Logger::getLogger('creatContragent','ofd.bitrix24/contragent/creatContragent.log');
			// $logger->log(array($contragent,$que,$q));
			//unset($_SESSION["ADD_CONTR_BPM"][$verifity["id"]]);
			//print_r($q);
            return ($q);
        }else{
			// unset($_SESSION["ADD_CONTR_BPM"][$verifity["id"]]);
			//print_r($que);
			return ($que);
        }

    }




























    public static function addcontactToBitrix($verifity = array())
    {
        $logger = Logger::getLogger('addcontactToBitrix','ofd.bitrix24/addcontactToBitrix.txt');
        $logger->log(array($verifity));

        $ct=new CCrmContact(false);
        $arParams = array('HAS_PHONE'=>'Y');
        $arParams['HAS_EMAIL']='N';


        if (!empty($verifity['phone']))
        {
            $arParams['FM']['PHONE'] = array(
                'n0' => array(
                    'VALUE_TYPE' => 'MOBILE',
                    'VALUE' => $verifity['phone'],
                )
            );
            $arParams['HAS_PHONE']='Y';
        }

        if (!empty($verifity['email']))
        {
            $arParams['FM']['EMAIL'] = array(
                'n0' => array(
                    'VALUE' => $verifity['email'],
                )
            );
            $arParams['HAS_EMAIL']='Y';
        }

        //Формирование ФИО
        $fio = trim($verifity['name']);
        $pieces = explode(" ", $fio);
        $countelementimarray = count($pieces);

        if($countelementimarray == 1)
        {
            $arParams['LAST_NAME'] = $fio;
            $arParams['NAME'] = "BPM";
        }
        else if($countelementimarray == 2)
        {
            $arParams['NAME'] = $pieces['1'];
            $arParams['LAST_NAME'] = $pieces['0'];
        }
        else if($countelementimarray == 3)
        {
            $arParams['SECOND_NAME'] = $pieces['2'];
            $arParams['NAME'] = $pieces['1'];
            $arParams['LAST_NAME'] = $pieces['0'];
        }

        $arParams['FULL_NAME']= $fio;

        $arParams['TYPE_ID'] ='CLIENTBPM';
        $arParams['OPENED'] = 'Y';


        if (!empty($verifity['inn'])) {
            $companyinfo = RestBpmBitrix::verify($verifity['inn']);
            if (is_array($companyinfo)) {
                $arParams['COMPANY_ID'] = $companyinfo['id'];
            }

        }
        $logger->log("arParams");
        $logger->log(array($arParams));

        if ( $new_contact_id=$ct->Add( $arParams, true, array('DISABLE_USER_FIELD_CHECK' => true) ) )
        {
            $entity_id = "CRM_CONTACT";
            if (!empty($verifity['contactId'])) {
                $uf_guid = "UF_BPMCONTACTID";
                SetUserField($entity_id, $new_contact_id, $uf_guid, $verifity['contactId']);
                $logger->log(array("contactId  ".$verifity['contactId']));
            }

            if (!empty($verifity['inn'])) {
                $uf_guid = "UF_BPMINN";
                SetUserField($entity_id, $new_contact_id, $uf_guid, $verifity['inn']);
                $logger->log(array("inn  ".$verifity['inn']));
            }

            if (!empty($verifity['kpp'])) {
                $uf_guid = "UF_BPMKPP";
                SetUserField($entity_id, $new_contact_id, $uf_guid, $verifity['kpp']);
                $logger->log(array("kpp  ".$verifity['kpp']));
            }

            if (!empty($verifity['accountId'])) {
                $uf_guid = "UF_BPMACCOUNTID";
                SetUserField($entity_id, $new_contact_id, $uf_guid, $verifity['accountId']);
                $logger->log(array("accountId  ".$verifity['accountId']));
            }
        } else{
            $logger->log(array("ADD ERROR: ".$ct->LAST_ERROR));
            return "-1";
        }

        return $new_contact_id;

    }

    public static function updateToBitrix($idcontact,$verifity = array())
    {
        $logger = Logger::getLogger('updateToBitrix','ofd.bitrix24/updateToBitrix.txt');
        $logger->log(array($verifity));




        $logger->log(array("OLD DATA"));



        $ct=new CCrmContact(false);
        $arParams = array('HAS_PHONE'=>'Y');
        $arParams['HAS_EMAIL']='N';
        $arParams['HAS_PHONE']='N';
        $arParams['TYPE_ID'] ='CLIENTBPM';

        if (!empty($verifity['mobilePhone']))
        {
            $rs = CCrmFieldMulti::GetList(array(), array("ELEMENT_ID" => $idcontact));
            $update = array('PHONE'=>array());
            $object = 'CONTACT';$flagtodotel=true;

				if($ar = $rs->Fetch()){

                $ar['VALUE'] = $verifity['mobilePhone'];
                if ($ar['TYPE_ID']=='PHONE')
                {
                    $update['PHONE'][$ar['ID']] = $ar;$flagtodotel=false;
				}}
            	$multi = new CCrmFieldMulti();
            	$multi->SetFields($object,$idcontact,$update);
           		$arParams['HAS_PHONE']='Y';

			if($flagtodotel)
			{
				$arParams['FM']['PHONE'] = array(
                 'n0' => array(
                     'VALUE_TYPE' => 'MOBILE',
                     'VALUE' => $verifity['mobilePhone'],
                 )
             	);
			}
        }


        if (!empty($verifity['email']))
        {
            /* 
             $arParams['HAS_PHONE']='Y';*/
            $rs = CCrmFieldMulti::GetList(array(), array("ELEMENT_ID" => $idcontact));
            $update = array('EMAIL'=>array());
            $object = 'CONTACT';$flagtodoemail=true;
if($ar = $rs->Fetch())
{

$ar['VALUE'] = $verifity['email'];
if ($ar['TYPE_ID']=='EMAIL')
{
  $update['EMAIL'][$ar['ID']] = $ar;$flagtodoemail=false;
}
}
$multi = new CCrmFieldMulti();
$multi->SetFields($object,$idcontact,$update);
$arParams['HAS_EMAIL']='Y';

			if($flagtodoemail){
			$arParams['FM']['EMAIL'] = array(
                 'n0' => array(
                     'VALUE_TYPE' => 'EMAIL',
                     'VALUE' => $verifity['email'],
                 )
             ); 
			}
        }
        $arParams['FULL_NAME']= $verifity['name'];

		/* $findme    = ' ';
        $pos1 = stripos($verifity['name'], $findme);
        if ($pos1 === false) {
            $arParams['LAST_NAME'] = $verifity['name'];
            $arParams['NAME'] = "BPM";

        } else {
            $arParams['NAME'] = substr($verifity['name'], 0, $pos1);
            $arParams['LAST_NAME'] = substr($verifity['name'], $pos1 + 1);
}*/

$pieces = explode(" ", $verifity['name']);
$countelementimarray=count($pieces);

if($countelementimarray==1)
{
    $arParams['LAST_NAME'] = $verifity['name'];
    $arParams['NAME'] = "BPM";
}
else if($countelementimarray==2)
{
    $arParams['NAME'] = $pieces['1'];
    $arParams['LAST_NAME'] = $pieces['0'];
}
else if($countelementimarray==3)
{
    $arParams['SECOND_NAME'] = $pieces['2'];
    $arParams['NAME'] = $pieces['1'];
    $arParams['LAST_NAME'] = $pieces['0'];
}

        $logger->log("accountId");
        $logger->log(array($verifity));


        $logger->log("GUID контра  ".$verifity['accountId']);
        if (!empty($verifity['accountId'])) {

            $companyinfo = RestBpmBitrix::verify(null,null,$verifity['accountId']);

            $logger->log("companyinfo");
            $logger->log(array($companyinfo));

            if (is_array($companyinfo)) {
                $arParams['COMPANY_ID'] = $companyinfo['id'];
            }

        }
        else
        {
            $arParams['COMPANY_ID'] = "";
        }



        $arParams['OPENED'] = 'Y';

        $logger->log(array($arParams));
        $logger->log(array("idcontact  ".$idcontact));

        $ct->Update($idcontact,$arParams);

        $logger->log(array("NEW DATA"));


        $entity_id = "CRM_CONTACT";
        if (!empty($verifity['contactId'])) {
            $uf_guid = "UF_BPMCONTACTID";
            SetUserField($entity_id, $idcontact, $uf_guid, $verifity['contactId']);
            $logger->log(array("contactId  ".$verifity['contactId']));
        }

        /* if (!empty($verifity['inn'])) {
             $uf_guid = "UF_BPMINN";
             SetUserField($entity_id, $idcontact, $uf_guid, $verifity['inn']);
         }

         if (!empty($verifity['kpp'])) {
             $uf_guid = "UF_BPMKPP";
             SetUserField($entity_id, $idcontact, $uf_guid, $verifity['kpp']);
         }

         if (!empty($verifity['accountId'])) {
             $uf_guid = "UF_BPMACCOUNTID";
             SetUserField($entity_id, $idcontact, $uf_guid, $verifity['accountId']);
         }*/

        return "DONE";

    }





    public static function contactUpdateToBMP($idcontact,$guidcontact,$name,$mobilephone,$email)
    {
        $logger = Logger::getLogger('contactUpdateToBMP','ofd.bitrix24/contactUpdateToBMP.txt');
        $logger->log(array("ID=".$guidcontact."//NAME=".$name."//MOBILEPHONE=".$mobilephone."//EMAIl=".$email));

        /* $ct=new CCrmContact(false);
         $arParams['TYPE_ID'] ='CLIENT';
         $ct->Update($idcontact,$arParams);*/

        $ColumnValues = array (
            'Items' =>
                array (
                    'Name' =>
                        array
                        (
                            'ExpressionType' => 2,
                            'Parameter' =>
                                array (
                                    'DataValueType' => 1,
                                    'Value' => $name,
                                ),
                        ),
                    'MobilePhone' =>
                        array
                        (
                            'ExpressionType' => 2,
                            'Parameter' =>
                                array (
                                    'DataValueType' => 1,
                                    'Value' => $mobilephone,
                                ),
                        ),
                    'Email' =>
                        array
                        (
                            'ExpressionType' => 2,
                            'Parameter' =>
                                array (
                                    'DataValueType' => 1,
                                    'Value' => $email,
                                ),
                        ),
                ),
        );
        $tiketUp = array (
            'RootSchemaName' => 'Contact',
            'QueryType' => 1,
            'ColumnValues' => $ColumnValues,
            'Filters' =>
                array (
                    'FilterType' => 6,
                    'ComparisonType' => 0,
                    'Items' =>
                        array (
                            'FilterId' =>
                                array (
                                    'FilterType' => 1,
                                    'ComparisonType' => 3,
                                    'LeftExpression' =>
                                        array (
                                            'ExpressionType' => 0,
                                            'ColumnPath' => 'Id',
                                        ),
                                    'RightExpression' =>
                                        array (
                                            'ExpressionType' => 2,
                                            'Parameter' =>
                                                array (
                                                    'DataValueType' => 0,
                                                    'Value' => $guidcontact,
                                                ),
                                        ),
                                ),
                        ),
                ),
        );

        $que = QueryBpm::jsonDataBpm($tiketUp, BPM_URL_UPDATE);
        if($que["status"] == 403)
        {
            QueryBpm::login();
            $que = QueryBpm::jsonDataBpm($tiketUp, BPM_URL_UPDATE);
        }
        $tiketUp = json_encode($tiketUp);
        $logger->log("tiketUp");
        $logger->log(array($tiketUp));
        $logger->log("que");
        $logger->log(array($que));

        if (($que["status"] == 200)||($que["status"] == 0))
        {
            /*$connection = Bitrix\Main\Application::getConnection();
            $sql = 'UPDATE connectionbpmbitrix SET PersonID = \''."1".'\' WHERE IDBITRIX=\''.$id.'\' ';
            $connection->queryExecute($sql);*/
            unset($_SESSION["NOTSENDELEMENT"]["UPDATE"][$idcontact]);
            // $logger->log($_SESSION["NOTSENDELEMENT"]);
            // $logger->log($_SESSION);
        }

    }


    public static function contactInsertToBmpUsingIdContact($id)
    {

        if (isset($id)) {

            $guidperson = "";
            $guidcompany = "";
            $name = "";
            $mobilephone = "";
            $email = "";


            //получаем или устанавливаем GUID BPM контакт
            $entity_id = "CRM_CONTACT";
            $uf_guid = "UF_BPMCONTACTID";
            $guidperson_buffer = GetUserField($entity_id, $id, $uf_guid);

            if (isset($guidperson_buffer)) {
                $guidperson = $guidperson_buffer;
            } else {
                $guidperson = strtolower(RestBpmBitrix::generate_guid());
                $entity_id = "CRM_CONTACT";
                $uf_guid = "UF_BPMCONTACTID";
                SetUserField($entity_id, $id, $uf_guid, $guidperson);
            }

            // $guidcompany

            $res = CCrmContact::GetList(array(), array('ID' => $id, "CHECK_PERMISSIONS" => "N"));
            if ($arRes = $res->Fetch()) {
                $company_id = $arRes['COMPANY_ID'];
                if ($company_id != "") {
                    $entity_id = "CRM_COMPANY";
                    $uf_guid = "UF_CRM_COMPANY_GUID";
                    $guidcompany = GetUserField($entity_id, $company_id, $uf_guid);
                }
            }

            $result = CCrmContact::GetListEx(array(), array('=ID' => $id, 'CHECK_PERMISSIONS' => 'N'), false, false, array('*'));
            $fields = is_object($result) ? $result->Fetch() : null;
            if (is_array($fields)) {
                $name = $fields['FULL_NAME'];
            }

            $res = CCrmFieldMulti::GetList(array('ID' => 'ASC'), array('ELEMENT_ID' => $id, 'TYPE_ID' => "PHONE"));
            if ($arRes = $res->Fetch()) {
                $mobilephone = $arRes['VALUE'];
            }

            $res = CCrmFieldMulti::GetList(array('ID' => 'ASC'), array('ELEMENT_ID' => $id, 'TYPE_ID' => "EMAIL"));
            if ($arRes = $res->Fetch()) {
                $email = $arRes['VALUE'];
            }
            //echo("ID=" . $id . "//GUIDPERSON=" . $guidperson . "//GUIDCOMPANY=" . $guidcompany . "//NAME=" . $name . "//MOBILEPHONE=" . $mobilephone . "//EMAIl=" . $email);
           $resultwork= ContactBpmBitrix::contactInsertToBMP($id, $guidperson, $guidcompany, $name, $mobilephone, $email);
return $resultwork;

        }
    }


    public static function contactInsertToBMP($id,$guidperson,$guidcompany,$name,$mobilephone,$email)
    {

        $logger = Logger::getLogger('contactInsertToBMP','ofd.bitrix24/contactInsertToBMP.txt');
        $logger->log(array("ID=".$id."//guidperson=".$guidperson."//INN=".$guidcompany."//NAME=".$name."//MOBILEPHONE=".$mobilephone."//EMAIl=".$email));



        // $guidcompany="";
        /* if (!empty($inn)) {
             $guidcompany = ContactBpmBitrix::SelectINNFromBPM($inn);
         }*/


        $logger->log(array("guidcompany=".$guidcompany));



        if (($guidcompany!="")/*&&($guidcompany!="-200")&&($guidcompany!="-1")*/)
        {
            $ColumnValues = array
            (
                'Items' =>
                    array
                    (
                        'Id' =>
                            array
                            (
                                'ExpressionType' => 2,
                                'Parameter' =>
                                    array(
                                        'DataValueType' => 0,
                                        'Value' => $guidperson,
                                    ),
                            ),
                        'Name' =>
                            array
                            (
                                'ExpressionType' => 2,
                                'Parameter' =>
                                    array(
                                        'DataValueType' => 1,
                                        'Value' => $name,
                                    ),
                            ),
                        'MobilePhone' =>
                            array
                            (
                                'ExpressionType' => 2,
                                'Parameter' =>
                                    array(
                                        'DataValueType' => 1,
                                        'Value' => $mobilephone,
                                    ),
                            ),
                        'Email' =>
                            array
                            (
                                'ExpressionType' => 2,
                                'Parameter' =>
                                    array(
                                        'DataValueType' => 1,
                                        'Value' => $email,
                                    ),
                            ),
						'Account' =>
                            array
                            (
                                'ExpressionType' => 2,
                                'Parameter' =>
                                    array(
                                        'DataValueType' => 0,
                                        'Value' => $guidcompany,
                                    ),
),
                    )
            );
        }
        else
        {
            $ColumnValues = array
            (
                'Items' =>
                    array
                    (
                        'Id' =>
                            array
                            (
                                'ExpressionType' => 2,
                                'Parameter' =>
                                    array(
                                        'DataValueType' => 0,
                                        'Value' => $guidperson,
                                    ),
                            ),
                        'Name' =>
                            array
                            (
                                'ExpressionType' => 2,
                                'Parameter' =>
                                    array(
                                        'DataValueType' => 1,
                                        'Value' => $name,
                                    ),
                            ),
                        'MobilePhone' =>
                            array
                            (
                                'ExpressionType' => 2,
                                'Parameter' =>
                                    array(
                                        'DataValueType' => 1,
                                        'Value' => $mobilephone,
                                    ),
                            ),
                        'Email' =>
                            array
                            (
                                'ExpressionType' => 2,
                                'Parameter' =>
                                    array(
                                        'DataValueType' => 1,
                                        'Value' => $email,
                                    ),
                            ),
                    )
            );

        }
        $tiketUp = array(
            'RootSchemaName' => 'Contact',
            'OperationType' => 1,
            'ColumnValues' => $ColumnValues
        );

        $logger->log(array($ColumnValues));
        $mobilephoneverify="-1";

        /* if (!empty($mobilephone)) {
             $mobilephoneverify = ContactBpmBitrix::SelectTelFromBPM($mobilephone);
             $logger->log(array("mobilephoneverify  ".$mobilephoneverify));
         }*/


        if ($mobilephoneverify == "-1")
        {
            $que = QueryBpm::jsonDataBpm($tiketUp, BPM_URL_INSERT);
            if ($que["status"] == 403) {
                QueryBpm::login();
                $que = QueryBpm::jsonDataBpm($tiketUp, BPM_URL_INSERT);
            }
            $tiketUp = json_encode($tiketUp);

            $logger->log("tiketUp");
            $logger->log(array($tiketUp));
            $logger->log("que");
            $logger->log(array($que));

            $logger->log(array("success_id  ".$que["success"]["id"]));

            if (($que["status"] == 200)||($que["status"] == 0))
            {
                /*$connection = Bitrix\Main\Application::getConnection();
                $sql = 'UPDATE connectionbpmbitrix SET PersonID = \''."1".'\' WHERE IDBITRIX=\''.$id.'\' ';
                $connection->queryExecute($sql);*/
                unset($_SESSION["NOTSENDELEMENT"]["INSERT"][$id]);
                // $logger->log($_SESSION["NOTSENDELEMENT"]);
                // $logger->log($_SESSION);
            }
        }
        return $que["status"];
    }
    public static function SelectTelFromBPM($telephone)
    {

        $logger = Logger::getLogger('SelectTelFromBPM','ofd.bitrix24/SelectTelFromBPM.txt');
        $logger->log(array("telephone=".$telephone));

        $tiket = array (
            'RootSchemaName' => 'Contact',
            'QueryType' => 0,
            'Columns' =>
                array (
                    'Items' =>
                        array (
                            'Id' =>
                                array (
                                    'Expression' =>
                                        array (
                                            'ColumnPath' => 'Id',
                                        ),
                                ),
                        ),
                ),
            'Filters' =>
                array (
                    'FilterType' => 6,
                    'ComparisonType' => 0,
                    'Items' =>
                        array (
                            'FilterId' =>
                                array (
                                    'FilterType' => 1,
                                    'ComparisonType' => 3,
                                    'LeftExpression' =>
                                        array (
                                            'ExpressionType' => 0,
                                            'ColumnPath' => 'MobilePhone',
                                        ),
                                    'RightExpression' =>
                                        array (
                                            'ExpressionType' => 2,
                                            'Parameter' =>
                                                array (
                                                    'DataValueType' => 0,
                                                    'Value' => $telephone,
                                                ),
                                        ),
                                ),
                        ),
                ),
        );

        $arrTeket = QueryBpm::jsonDataBpm($tiket, BPM_URL_SELECT);
        //$logger->log(array("telephone=".$telephone));
        $logger->log(array($arrTeket));

        if($arrTeket["status"] == 403)
        { QueryBpm::login();
            $arrTeket = QueryBpm::jsonDataBpm($tiket, BPM_URL_SELECT);
        }

        $rezult["status"] = $arrTeket["status"];
        if($rezult["status"] != 200)
        {
            return "-200";
        }

        foreach ($arrTeket["success"]["rows"] as $row => $key) {
            $contactBitrixBpm[$row] = $key;
        }
        $logger->log(array($contactBitrixBpm['0']['Id']));
        if(!isset($contactBitrixBpm['0']['Id']))
        {
            return "-1";
        }
        else
        {
            return $contactBitrixBpm['0']['Id'];
        }

    }
    public static function SelectINNFromBPM($inn)
    {
        $tiket = array (
            'RootSchemaName' => 'Account',
            'QueryType' => 0,
            'Columns' =>
                array (
                    'Items' =>
                        array (
                            'Id' =>
                                array (
                                    'Expression' =>
                                        array (
                                            'ColumnPath' => 'Id',
                                        ),
                                ),
                        ),
                ),
            'Filters' =>
                array (
                    'FilterType' => 6,
                    'ComparisonType' => 0,
                    'Items' =>
                        array (
                            'FilterId' =>
                                array (
                                    'FilterType' => 1,
                                    'ComparisonType' => 3,
                                    'LeftExpression' =>
                                        array (
                                            'ExpressionType' => 0,
                                            'ColumnPath' => 'Usraidiagent',
                                        ),
                                    'RightExpression' =>
                                        array (
                                            'ExpressionType' => 2,
                                            'Parameter' =>
                                                array (
                                                    'DataValueType' => 1,
                                                    'Value' => $inn,
                                                ),
                                        ),
                                ),
                        ),
                ),
        );

        $arrTeket = QueryBpm::jsonDataBpm($tiket, BPM_URL_SELECT);
        if($arrTeket["status"] == 403)
        { QueryBpm::login();
            $arrTeket = QueryBpm::jsonDataBpm($tiket, BPM_URL_SELECT);
        }
        $rezult["status"] = $arrTeket["status"];

        if($rezult["status"] != 200)
        {
            return "-200";
        }

        foreach ($arrTeket["success"]["rows"] as $row => $key) {
            $contactBitrixBpm[$row] = $key;
        }

        if(!isset($contactBitrixBpm['0']['Id']))
        {
            return "-1";
        }
        else
        {
            return $contactBitrixBpm['0']['Id'];
        }
    }






    public static function SelectGUIDfromBPM($email,$phone)
    {
        $tiket = array (
            'RootSchemaName' => 'Contact',
            'QueryType' => 0,
            'Columns' =>
                array (
                    'Items' =>
                        array (
                            'Id' =>
                                array (
                                    'Expression' =>
                                        array (
                                            'ColumnPath' => 'Id',
                                        ),
                                ),
                        ),
                ),
            'Filters' =>
                array (
                    'FilterType' => 6,
                    'ComparisonType' => 0,
                    'Items' =>
                        array (
                            'FilterId' =>
                                array (
                                    'FilterType' => 1,
                                    'ComparisonType' => 3,
                                    'LeftExpression' =>
                                        array (
                                            'ExpressionType' => 0,
                                            'ColumnPath' => 'MobilePhone',
                                        ),
                                    'RightExpression' =>
                                        array (
                                            'ExpressionType' => 2,
                                            'Parameter' =>
                                                array (
                                                    'DataValueType' => 0,
                                                    'Value' => $phone,
                                                ),
                                        ),
                                ),
                        ),
                    array (
                        'FilterValue' =>
                            array (
                                'FilterType' => 1,
                                'ComparisonType' => 3,
                                'LeftExpression' =>
                                    array (
                                        'ExpressionType' => 0,
                                        'ColumnPath' => 'Email',
                                    ),
                                'RightExpression' =>
                                    array (
                                        'ExpressionType' => 2,
                                        'Parameter' =>
                                            array (
                                                'DataValueType' => 0,
                                                'Value' => $email,
                                            ),
                                    ),
                            ),
                    ),


                ),
        );

        $arrTeket = QueryBpm::jsonDataBpm($tiket, BPM_URL_SELECT);
        if($arrTeket["status"] == 403)
        { QueryBpm::login();
            $arrTeket = QueryBpm::jsonDataBpm($tiket, BPM_URL_SELECT);
        }

        print_r($arrTeket);

        /*  $rezult["status"] = $arrTeket["status"];
          if($rezult["status"] != 200)
          {
              return "-200";
          }
  */


        /*  foreach ($arrTeket["success"]["rows"] as $row => $key) {
              $contactBitrixBpm[$row] = $key;
          }
          $logger->log(array($contactBitrixBpm['0']['Id']));
          if(!isset($contactBitrixBpm['0']['Id']))
          {
              return "-1";
          }
          else
          {
              return $contactBitrixBpm['0']['Id'];
          }*/

    }

    public static function SelectGUIDfromBPMwithFIO($fio,$email,$phone)
    {
        $tiket = array (
            'RootSchemaName' => 'Contact',
            'QueryType' => 0,
            'Columns' =>
                array (
                    'Items' =>
                        array (
                            'Id' =>
                                array (
                                    'Expression' =>
                                        array (
                                            'ColumnPath' => 'Id',
                                        ),
                                ),
                        ),
                ),
            'Filters' =>
                array (
                    'FilterType' => 6,
                    'ComparisonType' => 0,
                    'Items' =>
                        array (
                            'FilterId' =>
                                array (
                                    'FilterType' => 1,
                                    'ComparisonType' => 3,
                                    'LeftExpression' =>
                                        array (
                                            'ExpressionType' => 0,
                                            'ColumnPath' => 'Name',
                                        ),
                                    'RightExpression' =>
                                        array (
                                            'ExpressionType' => 2,
                                            'Parameter' =>
                                                array (
                                                    'DataValueType' => 0,
                                                    'Value' => $fio,
                                                ),
                                        ),
                                ),
                        ),

                    array (
                        'FilterId' =>
                            array (
                                'FilterType' => 1,
                                'ComparisonType' => 3,
                                'LeftExpression' =>
                                    array (
                                        'ExpressionType' => 0,
                                        'ColumnPath' => 'MobilePhone',
                                    ),
                                'RightExpression' =>
                                    array (
                                        'ExpressionType' => 2,
                                        'Parameter' =>
                                            array (
                                                'DataValueType' => 0,
                                                'Value' => $phone,
                                            ),
                                    ),
                            ),
                    ),
                    array (
                        'FilterId' =>
                            array (
                                'FilterType' => 1,
                                'ComparisonType' => 3,
                                'LeftExpression' =>
                                    array (
                                        'ExpressionType' => 0,
                                        'ColumnPath' => 'Email',
                                    ),
                                'RightExpression' =>
                                    array (
                                        'ExpressionType' => 2,
                                        'Parameter' =>
                                            array (
                                                'DataValueType' => 0,
                                                'Value' => $email,
                                            ),
                                    ),
                            ),
                    ),



                ),
        );

        $arrTeket = QueryBpm::jsonDataBpm($tiket, BPM_URL_SELECT);
        if($arrTeket["status"] == 403)
        { QueryBpm::login();
            $arrTeket = QueryBpm::jsonDataBpm($tiket, BPM_URL_SELECT);
        }

        print_r($arrTeket);

        /*  $rezult["status"] = $arrTeket["status"];
          if($rezult["status"] != 200)
          {
              return "-200";
          }
  */


        /*  foreach ($arrTeket["success"]["rows"] as $row => $key) {
              $contactBitrixBpm[$row] = $key;
          }
          $logger->log(array($contactBitrixBpm['0']['Id']));
          if(!isset($contactBitrixBpm['0']['Id']))
          {
              return "-1";
          }
          else
          {
              return $contactBitrixBpm['0']['Id'];
          }*/

    }

    public static function SelectDatafromBPMusingGUID($guid)
    {

        /*$logger = Logger::getLogger('forsynchronization_11zadacha','ofd.bitrix24/forsynchronization_11zadacha.txt');

        $logger->log("///////////////////////////////////////////////////////////////////////////////");
        $logger->log("guid=".$guid);*/


        $tiket = array (
            'RootSchemaName' => 'Contact',
            'QueryType' => 0,
            'Columns' =>
                array (
                    'Items' =>
                        array (
                            'Name' =>
                                array (
                                    'Expression' =>
                                        array (
                                            'ColumnPath' => 'Name',
                                        ),
                                ),
                            'MobilePhone' =>
                                array (
                                    'Expression' =>
                                        array (
                                            'ColumnPath' => 'MobilePhone',
                                        ),
                                ),
                            'Email' =>
                                array (
                                    'Expression' =>
                                        array (
                                            'ColumnPath' => 'Email',
                                        ),
                                ),
                            'Account' =>
                                array (
                                    'Expression' =>
                                        array (
                                            'ColumnPath' => 'Account',
                                        ),
                                ),
                        ),
                ),
            'Filters' =>
                array (
                    'FilterType' => 6,
                    'ComparisonType' => 0,
                    'Items' =>
                        array (
                            'FilterId' =>
                                array (
                                    'FilterType' => 1,
                                    'ComparisonType' => 3,
                                    'LeftExpression' =>
                                        array (
                                            'ExpressionType' => 0,
                                            'ColumnPath' => 'Id',
                                        ),
                                    'RightExpression' =>
                                        array (
                                            'ExpressionType' => 2,
                                            'Parameter' =>
                                                array (
                                                    'DataValueType' => 0,
                                                    'Value' => $guid,
                                                ),
                                        ),
                                ),
                        ),
                ),
        );


        $arrTeket = QueryBpm::jsonDataBpm($tiket, BPM_URL_SELECT);
        if($arrTeket["status"] == 403)
        { //QueryBpm::login();
            $arrTeket = QueryBpm::jsonDataBpm($tiket, BPM_URL_SELECT);
        }

      //  $logger->log(array($arrTeket));
		//pre($arrTeket);
        if($arrTeket["status"] == 200)
        {

            return $arrTeket["success"]["rows"]["0"];
            $flagtoadd=1;
         } else {
			return "";
        }
    }



    public static function SelectINNFromBPMusingGUID($guidcompany)
    {
        $tiket = array (
            'RootSchemaName' => 'Account',
            'QueryType' => 0,
            'Columns' =>
                array (
                    'Items' =>
                        array (
                            'Usraidiagent' =>
                                array (
                                    'Expression' =>
                                        array (
                                            'ColumnPath' => 'Usraidiagent',
                                        ),
                                ),
                        ),
                ),
            'Filters' =>
                array (
                    'FilterType' => 6,
                    'ComparisonType' => 0,
                    'Items' =>
                        array (
                            'FilterId' =>
                                array (
                                    'FilterType' => 1,
                                    'ComparisonType' => 3,
                                    'LeftExpression' =>
                                        array (
                                            'ExpressionType' => 0,
                                            'ColumnPath' => 'Id',
                                        ),
                                    'RightExpression' =>
                                        array (
                                            'ExpressionType' => 2,
                                            'Parameter' =>
                                                array (
                                                    'DataValueType' => 0,
                                                    'Value' => $guidcompany,
                                                ),
                                        ),
                                ),
                        ),
                ),
        );

        $arrTeket = QueryBpm::jsonDataBpm($tiket, BPM_URL_SELECT);
        if($arrTeket["status"] == 403)
        {
            QueryBpm::login();
            $arrTeket = QueryBpm::jsonDataBpm($tiket, BPM_URL_SELECT);
        }

        if($arrTeket["status"] != 200)
        {
            $connection = Bitrix\Main\Application::getConnection();
            $sql = 'UPDATE synchbpmbitrix SET ID_BITRIX_COMPANY = \'' . $arrTeket["status"] . '\' WHERE GUID_BPM_COMPANY=\'' . $guidcompany . '\' ';
            $connection->queryExecute($sql);
        }
        else
        {

            $connection = Bitrix\Main\Application::getConnection();
            $sql = 'UPDATE synchbpmbitrix SET ID_BITRIX_COMPANY = \'' . $arrTeket["status"] . '\' WHERE GUID_BPM_COMPANY=\'' . $guidcompany . '\' ';
            $connection->queryExecute($sql);

            if(isset($arrTeket["success"]["rows"]["0"]["Usraidiagent"]))
            {
                $connection = Bitrix\Main\Application::getConnection();
                $sql = 'UPDATE synchbpmbitrix SET INN_BPM_COMPANY = \'' . $arrTeket["success"]["rows"]["0"]["Usraidiagent"] . '\' WHERE GUID_BPM_COMPANY=\'' . $guidcompany . '\' ';
                $connection->queryExecute($sql);
            }



        }
    }

    /*
     * Получение из BPM Имени компании, ИНН компании  по GUID
     */

    public static function SelectNAMEINNKPPFromBPMusingGUID($guidcompany)
    {
        $logger = Logger::getLogger('forsynchronization_4zadacha','ofd.bitrix24/forsynchronization_4zadacha.txt');
        $logger->log("SelectNAMEINNKPPFromBPMusingGUID");
        $tiket = array (
            'RootSchemaName' => 'Account',
            'QueryType' => 0,
            'Columns' =>
                array (
                    'Items' =>
                        array (
                            'Usraidiagent' =>
                                array (
                                    'Expression' =>
                                        array (
                                            'ColumnPath' => 'Usraidiagent',
                                        ),
                                ),
                            'UsrKPP' =>
                                array (
                                    'Expression' =>
                                        array (
                                            'ColumnPath' => 'UsrKPP',
                                        ),
                                ),

                            'Name' =>
                                array (
                                    'Expression' =>
                                        array (
                                            'ColumnPath' => 'Name',
                                        ),
                                ),
                        ),
                ),
            'Filters' =>
                array (
                    'FilterType' => 6,
                    'ComparisonType' => 0,
                    'Items' =>
                        array (
                            'FilterId' =>
                                array (
                                    'FilterType' => 1,
                                    'ComparisonType' => 3,
                                    'LeftExpression' =>
                                        array (
                                            'ExpressionType' => 0,
                                            'ColumnPath' => 'Id',
                                        ),
                                    'RightExpression' =>
                                        array (
                                            'ExpressionType' => 2,
                                            'Parameter' =>
                                                array (
                                                    'DataValueType' => 0,
                                                    'Value' => $guidcompany,
                                                ),
                                        ),
                                ),
                        ),
                ),
        );

        QueryBpm::login();
        // $logger->log(array($tiket));
        $arrTeket = QueryBpm::jsonDataBpm($tiket, BPM_URL_SELECT);
        if($arrTeket["status"] == 403)
        { QueryBpm::login();
            $arrTeket = QueryBpm::jsonDataBpm($tiket, BPM_URL_SELECT);
        }


		//return $arrTeket["success"]["rows"]["0"];
        if ($arrTeket["status"] == 200) {


            $connection = Bitrix\Main\Application::getConnection();
            $sql = 'UPDATE synchbpmbitrixcompanybpm SET INN_BPM_COMPANY = \'' .$arrTeket["success"]["rows"]["0"]['Usraidiagent']  . '\' WHERE GUIDBPM_COMPANY=\'' . $guidcompany . '\' ';
            $connection->queryExecute($sql);

            $connection = Bitrix\Main\Application::getConnection();
            $sql = 'UPDATE synchbpmbitrixcompanybpm SET KPP_BPM_COMPANY = \'' .$arrTeket["success"]["rows"]["0"]['UsrKPP'] . '\' WHERE GUIDBPM_COMPANY=\'' . $guidcompany . '\' ';
            $connection->queryExecute($sql);

            $connection = Bitrix\Main\Application::getConnection();
            $sql = 'UPDATE synchbpmbitrixcompanybpm SET NAME_BPM_COMPANY = \'' . $arrTeket["success"]["rows"]["0"]['Name'] . '\' WHERE GUIDBPM_COMPANY=\'' . $guidcompany . '\' ';
            $connection->queryExecute($sql);

            $connection = Bitrix\Main\Application::getConnection();
            $sql = 'UPDATE synchbpmbitrixcompanybpm SET STATYS_ANSWER_BPM_COMPANY = \'' . "TOINSERT_1" . '\' WHERE GUIDBPM_COMPANY=\'' . $guidcompany . '\' ';
            $connection->queryExecute($sql);
        }
    }


  public static function KKTTOBPM($idbitrixkkt)
    {

        $nameforbpm="";
        $guidkktforbpm="";
        $companyidusingkkt="";
        $companyguidusingkkt="";
        $rnmkkt="";
        $usrfactorynumber="";
        $adresskkt="";
        $usrratecost="";

        $arSelect = Array("PROPERTY_GUID_KTT","NAME","PROPERTY_COMPANY","PROPERTY_RNM_KKT","PROPERTY_NUM_FN","PROPERTY_ADDR_KKT","PROPERTY_COST_TARIF");
        $arFilter = Array("IBLOCK_ID"=>"76","ID"=>$idbitrixkkt, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
        $res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
        if($ob = $res->GetNextElement())
        {
            $arFields = $ob->GetFields();  // print_r($arFields);
            $nameforbpm=$arFields['NAME'];
            $guidkktforbpm=$arFields['PROPERTY_GUID_KTT_VALUE'];
            $companyidusingkkt=$arFields['PROPERTY_COMPANY_VALUE'];
            $companyguidusingkkt=GetUserField("CRM_COMPANY", $companyidusingkkt, "UF_CRM_COMPANY_GUID");
            $usrfactorynumber=$arFields['PROPERTY_NUM_FN_VALUE'];
            $adresskkt=$arFields['PROPERTY_ADDR_KKT_VALUE'];
            $usrratecost=$arFields['PROPERTY_COST_TARIF_VALUE'];
            $rnmkkt=$arFields['PROPERTY_RNM_KKT_VALUE'];


        }

if(empty($guidkktforbpm))
{
    $guidkktforbpm=RestBpmBitrix::generate_guid();
    $ELEMENT_ID = $idbitrixkkt;  // код элемента
    $PROPERTY_CODE = "GUID_KTT";  // код свойства
    $PROPERTY_VALUE = $guidkktforbpm;  // значение свойства
    CIBlockElement::SetPropertyValuesEx($ELEMENT_ID, "76", array($PROPERTY_CODE => $PROPERTY_VALUE));
}

        $data1 = array (
            "__type" => "Terrasoft.Nui.ServiceModel.DataContract.InsertQuery",
            'RootSchemaName' => 'ConfItem',
            'OperationType' => 1,
            'ColumnValues' => array(
                'Items' =>
                    array(
                        'Category' =>
                            array(
                                'ExpressionType' => 2,
                                'Parameter' =>
                                    array(
                                        'DataValueType' => 0,
                                        'Value' => "da2cf289-b4a2-40dd-9795-f35c5c9bc70a"
                                    ),
                            ),
                        //TODO изменить ID
                        'Id' =>
                            array(
                                'ExpressionType' => 2,
                                'Parameter' =>
                                    array(
                                        'DataValueType' => 0,
                                        'Value' => $guidkktforbpm
                                    ),
                            ),
                        'Name' =>
                            array(
                                'ExpressionType' => 2,
                                'Parameter' =>
                                    array(
                                        'DataValueType' => 1,
                                        'Value' => $nameforbpm
                                    ),
                            ),
                        'UsrModelCashbox' =>
                            array(
                                'ExpressionType' => 2,
                                'Parameter' =>
                                    array(
                                        'DataValueType' => 1,
                                        'Value' => ""
                                    ),
                            ),
                        'UsrRnmCashbox' =>
                            array(
                                'ExpressionType' => 2,
                                'Parameter' =>
                                    array(
                                        'DataValueType' => 1,
                                        'Value' => $rnmkkt
                                    ),
                            ),
                        'UsrFactoryNumber' =>
                            array(
                                'ExpressionType' => 2,
                                'Parameter' =>
                                    array(
                                        'DataValueType' => 1,
                                        'Value' => $usrfactorynumber
                                    ),
                            ),
                        'PurchaseDate' =>
                            array(
                                'ExpressionType' => 2,
                                'Parameter' =>
                                    array(
                                        'DataValueType' => 8,
                                        'Value' => ""
                                    ),
                            ),
                        'CancelDate' =>
                            array(
                                'ExpressionType' => 2,
                                'Parameter' =>
                                    array(
                                        'DataValueType' => 8,
                                        'Value' => ""
                                    ),
                            ),
                        'Address' =>
                            array(
                                'ExpressionType' => 2,
                                'Parameter' =>
                                    array(
                                        'DataValueType' => 1,
                                        'Value' => $adresskkt
                                    ),
                            ),
                        'Status' =>
                            array(
                                'ExpressionType' => 2,
                                'Parameter' =>
                                    array(
                                        'DataValueType' => 0,
                                        'Value' => "D0C0AE7D-827A-4B63-9006-3A9949F81246"
                                    ),
                            ),
                        'UsrRateCost' =>
                            array(
                                'ExpressionType' => 2,
                                'Parameter' =>
                                    array(
                                        'DataValueType' => 1,
                                        'Value' => $usrratecost
                                    ),
                            ),
						  'UsrActivationDate' =>
                            array(
                                'ExpressionType' => 2,
                                'Parameter' =>
                                    array(
                                        'DataValueType' => 8,
                                        'Value' => ""
                                    ),
),
						/* 'UsrDeactivationDate' =>
                            array(
                                'ExpressionType' => 2,
                                'Parameter' =>
                                    array(
                                        'DataValueType' => 8,
                                        'Value' => ""
                                    ),
),*/
                        'UsrPromotionalCode' =>
                            array(
                                'ExpressionType' => 2,
                                'Parameter' =>
                                    array(
                                        'DataValueType' => 1,
                                        'Value' => ""
                                    ),
                            ),
						/* 'UsrActivationPeriod' =>
                            array(
                                'ExpressionType' => 2,
                                'Parameter' =>
                                    array(
                                        'DataValueType' => 1,
                                        'Value' => ""
                                    ),
),*/
						/* 'UsrDeactivationPeriod' =>
                            array(
                                'ExpressionType' => 2,
                                'Parameter' =>
                                    array(
                                        'DataValueType' => 1,
                                        'Value' => ""
                                    ),
),*/
						/* 'UsrRate' =>
                            array(
                                'ExpressionType' => 2,
                                'Parameter' =>
                                    array(
                                        'DataValueType' => 1,
                                        'Value' => ""
                                    ),
),*/

                    ),
            ),

        );
        $batshContact[0] = $data1;





if(!empty($companyguidusingkkt))
{
        $data2 = array (
            "__type" => "Terrasoft.Nui.ServiceModel.DataContract.InsertQuery",
            'RootSchemaName' => 'ConfItemUser',
            'QueryType' => 1,
            'ColumnValues' => array(
                'Items' =>
                    array(
                        'ConfItem' =>
                            array(
                                'ExpressionType' => 2,
                                'Parameter' =>
                                    array(
                                        'DataValueType' => 0,
                                        'Value' =>  $guidkktforbpm,
                                    ),
                            ),
						'Account' =>
                            array(
                                'ExpressionType' => 2,
                                'Parameter' =>
                                    array(
                                        'DataValueType' => 0,
										'Value' => $companyguidusingkkt,
                                    ),
							),

                    ),
            ),
        );
	}
else
{
        $data2 = array (
            "__type" => "Terrasoft.Nui.ServiceModel.DataContract.InsertQuery",
            'RootSchemaName' => 'ConfItemUser',
            'QueryType' => 1,
            'ColumnValues' => array(
                'Items' =>
                    array(
                        'ConfItem' =>
                            array(
                                'ExpressionType' => 2,
                                'Parameter' =>
                                    array(
                                        'DataValueType' => 0,
                                        'Value' =>  $guidkktforbpm,
                                    ),
                            ),

                    ),
            ),
        );
}
        $logger = Logger::getLogger('kkttosendbpm','ofd.bitrix24/kkttosendbpm.txt');



        $batshContact[1] = $data2;
        //$batshContact[1] = $data9;

        $contact = array(
            "items" => $batshContact
        );
	// $logger->log(array($contact));
        //$Data = json_encode($contragent, true);
        //pre($Data);
        $que = QueryBpm::jsonDataBpm($contact, BPM_URL_QUERY);
	// $logger->log(array($que));

        if($que["status"] == 403)
        {
            QueryBpm::login();
            $que = QueryBpm::jsonDataBpm($contact, BPM_URL_QUERY);
			//$logger->log(array($que));
			// echo "<br>";
			//print_r($que);
        }

        if ($que["status"] == 200)
        {
            return true;
        }
        else
        {	

			$countsubstr=substr_count($que['success']['ResponseStatus']['Message'], 'Конфликт инструкции INSERT с ограничением FOREIGN KEY "FKMGj1QFuMGK2rC1ZzVUD3T8gXCms". Конфликт произошел в базе данных "ofd_prod1", таблица "dbo.Account", column ');
			if($countsubstr>0)
			{
				$result = RestBpmBitrix::verify(null, $companyidusingkkt);
				CrmBitrixBpm::contragenAdd($result);
			}

			$countsubstr=substr_count($que['success']['ResponseStatus']['Message'], 'Нарушено "PKLNLQcGsD4bb5dvwtY8C1Oc0SAvY" ограничения PRIMARY KEY. Не удается вставить повторяющийся ключ в объект "dbo.ConfItem". Повторяющееся значение ключа:');
			if($countsubstr>0)
			{
				return true;
			}


			$logger->log($companyguidusingkkt);
			$logger->log(array($que['success']['ResponseStatus']['Message']));
            return false;
        }
    }

 public static function SelectGUIDusingInnName($id)
    {


$inncompany= "";
$kppcompany= "";

$req = new \Bitrix\Crm\EntityRequisite();
        $rser = $req->getList(array(
            "filter" => array(
                "ENTITY_ID" => $id,
                "ENTITY_TYPE_ID" => CCrmOwnerType::Company,
                "PRESET_ID" => array(1,2)
            )
        ));
if($rows = $rser->fetch()){$inncompany = $rows["RQ_INN"];$kppcompany = $rows["RQ_KPP"];}



if(empty($inncompany)&&empty($kppcompany))
{
	$inncompany= GetUserField("CRM_COMPANY", $id, "UF_CRM_INN");
	$kppcompany= GetUserField("CRM_COMPANY", $id, "UF_CRM_KPP");
}

$companyresult=CCrmCompany::GetByID($id);
$namecompany=$companyresult['TITLE'];
		//print_r($companyresult);

//$namecompany= GetUserField("CRM_COMPANY", $record['ID_BITRIX_CONTR'], "UF_CRM_KPP");

/* $inncompany=$res['inn'];
$kppcompany=$res['kpp'];*/





        $tiket = array (
            'RootSchemaName' => 'Account',
            'QueryType' => 0,
            'Columns' =>
                array (
                    'Items' =>
                        array (
                            'Id' =>
                                array (
                                    'Expression' =>
                                        array (
                                            'ColumnPath' => 'Id',
                                        ),
                                ),
'CreatedOn' =>
                                array (
                                    'Expression' =>
                                        array (
                                            'ColumnPath' => 'CreatedOn',
                                        ),
                                ),

                        ),
                ),

            'Filters' =>
              array (
                    'FilterType' => 6,
                    'ComparisonType' => 0,
                    'Items' =>
                       array (
                            'FilterINN' =>
                                array (
                                    'FilterType' => 1,
                                    'ComparisonType' => 3,
                                    'LeftExpression' =>
                                        array (
                                            'ExpressionType' => 0,
                                            'ColumnPath' => 'Usraidiagent',
                                        ),
                                    'RightExpression' =>
                                        array (
                                            'ExpressionType' => 2,
                                            'Parameter' =>
                                                array (
                                                    'DataValueType' => 1,
                                                    'Value' => $inncompany,
                                                ),
                                        ),
                                ),
						   /*  'NameFilter' =>
                               array (
                                   'FilterType' => 1,
                                   'ComparisonType' => 3,
                                   'LeftExpression' =>
                                       array (
                                           'ExpressionType' => 0,
                                           'ColumnPath' => 'Name',
                                       ),
                                   'RightExpression' =>
                                       array (
                                           'ExpressionType' => 2,
                                           'Parameter' =>
                                               array (
                                                   'DataValueType' => 1,
                                                   'Value' => $namecompany,
                                               ),
                                       ),
),*/
                           'KppFilter' =>
                               array (
                                   'FilterType' => 1,
                                   'ComparisonType' => 3,
                                   'LeftExpression' =>
                                       array (
                                           'ExpressionType' => 0,
                                           'ColumnPath' => 'UsrKPP',
                                       ),
                                   'RightExpression' =>
                                       array (
                                           'ExpressionType' => 2,
                                           'Parameter' =>
                                               array (
                                                   'DataValueType' => 1,
                                                   'Value' => $kppcompany,
                                               ),
                                       ),
                               ),
                        ),
                ),
        );

		//pre($tiket);

        $arrTeket = QueryBpm::jsonDataBpm($tiket, BPM_URL_SELECT);

        if($arrTeket["status"] == 403)
        {
            QueryBpm::login();
            $arrTeket = QueryBpm::jsonDataBpm($tiket, BPM_URL_SELECT);
        }
		print_r($arrTeket['success']['rows']);echo count($arrTeket['success']['rows']);

$countelement=count($arrTeket['success']['rows']);
		$resultstring="";
		foreach($arrTeket['success']['rows'] as $arrelement)
		{


$findme    = '2017-09-26';
$pos1 = stripos($arrelement['CreatedOn'], $findme);
if (($pos1 !== false)&&($id<100000)) { $countelement--; continue;}

$findme    = '2017-09-27';
$pos1 = stripos($arrelement['CreatedOn'], $findme);
if (($pos1 !== false)&&($id<100000)) { $countelement--; continue;}

$findme    = '2017-09-28';
$pos1 = stripos($arrelement['CreatedOn'], $findme);
if (($pos1 !== false)&&($id<100000)) { $countelement--; continue;}

$findme    = '2017-09-29';
$pos1 = stripos($arrelement['CreatedOn'], $findme);
if (($pos1 !== false)&&($id<100000)) { $countelement--; continue;}





			if ($countelement>1)
			{
				$resultstring=$resultstring.";".$arrelement['Id'];
			}
			else
			{
				$resultstring=$arrelement['Id'];
			}


		}




if(!empty($resultstring))
{
	$connection = Bitrix\Main\Application::getConnection();
	$sql = 'UPDATE tocorrectguidcontr SET NEW_GUID_CONTR = \'' . $resultstring. '\' WHERE ID_BITRIX_CONTR=\'' . $id . '\' ';
	$connection->queryExecute($sql);

	$connection = Bitrix\Main\Application::getConnection();
	$sql = 'UPDATE tocorrectguidcontr SET STATYS_ANSWER_BPM_CONTR = \'' . "GETNEWGUIDDONE_1". '\' WHERE ID_BITRIX_CONTR=\'' . $id . '\' ';
	$connection->queryExecute($sql);

}
else
{
	$connection = Bitrix\Main\Application::getConnection();
	$sql = 'UPDATE tocorrectguidcontr SET STATYS_ANSWER_BPM_CONTR = \'' . "GETNEWGUIDCANCEL_1". '\' WHERE ID_BITRIX_CONTR=\'' . $id . '\' ';
	$connection->queryExecute($sql);

}


		if($arrTeket["status"] == 200){return true;}
		else{return false;}
		//return $arrTeket;

    }



}