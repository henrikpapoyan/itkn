<?php

use Bitrix\Main\CUserTypeEntity,
    Bitrix\Main\Loader;

Loader::includeModule("iblock");
Loader::includeModule("crm");

class SychBazaData
{



    public static function GetCountContactBitrix()
    {
        $select = array(
            "ID",
            "FULL_NAME"
        );

        $res = CCrmContact::GetList(array(), array('CHECK_PERMISSIONS' => 'N'),$select);
        $countcontact=0;
        while ($fields = $res->Fetch()) {
            $countcontact++;
        }
        return $countcontact;

    }

    public static function FindContactBitrixUsingGuid($guidbpm)
    {
        if(!empty($guidbpm)) {
            $select = array(
                "ID"
            );

            $res = CCrmContact::GetList(array(), array('=UF_BPMCONTACTID' => $guidbpm, 'CHECK_PERMISSIONS' => 'N'), $select);
            if ($fields = $res->Fetch()) {
                return $fields['ID'];
            }
            else{return "Нет в системе";}
        }
    }


    public static function FindAccountBitrixUsingGuid($guidbpm)
    {
        if(!empty($guidbpm)) {
            $select = array(
                "ID"
            );

            $res = CCrmCompany::GetList(array(), array('=UF_CRM_COMPANY_GUID' => $guidbpm, 'CHECK_PERMISSIONS' => 'N'), $select);
            if ($fields = $res->Fetch()) {
                return $fields['ID'];
            }
            else{return "Нет в системе";}
        }
    }



    public static function GetCountAccountBitrix()
    {
        $select = array(
            "ID",
            "FULL_NAME"
        );

        $res = CCrmCompany::GetList(array(), array('CHECK_PERMISSIONS' => 'N'),$select);
        $countcontact=0;
        while ($fields = $res->Fetch()) {
            $countcontact++;
        }
        return $countcontact;

    }

    public static function GetCountAccountBpm()
    {
        $que = QueryBpm::jsonDataBpm("", BPM_URL_GETCOUNTITEMS);
        if ($que["status"] == 403) {
            QueryBpm::login();
            $que = QueryBpm::jsonDataBpm("", BPM_URL_GETCOUNTITEMS);
        }
        if (($que["status"] == 200) && ($que["success"]["GetCountItemsResult"]["code"] == 100)) {
            return $que["success"]["GetCountItemsResult"]["countAccount"];
        } else {
            return "-1";
        }
    }

    public static function GetCountContactBPM()
    {
        $que = QueryBpm::jsonDataBpm("", BPM_URL_GETCOUNTITEMS);
        if ($que["status"] == 403) {
            QueryBpm::login();
            $que = QueryBpm::jsonDataBpm("", BPM_URL_GETCOUNTITEMS);
        }
        if (($que["status"] == 200) && ($que["success"]["GetCountItemsResult"]["code"] == 100)) {
            return $que["success"]["GetCountItemsResult"]["countContact"];
        } else {
            return "-1";
        }
    }
    public static function GetCountKKTBPM()
    {
        $que = QueryBpm::jsonDataBpm("", BPM_URL_GETCOUNTITEMS);
        if ($que["status"] == 403) {
            QueryBpm::login();
            $que = QueryBpm::jsonDataBpm("", BPM_URL_GETCOUNTITEMS);
        }
        if (($que["status"] == 200) && ($que["success"]["GetCountItemsResult"]["code"] == 100)) {
            return $que["success"]["GetCountItemsResult"]["countKkt"];
        } else {
            return "-1";
        }
    }


    public static function GetArrayKKTBPM()
    {
        $que = QueryBpm::jsonDataBpm("", BPM_URL_GETCOUNTITEMS);
        if ($que["status"] == 403) {
            QueryBpm::login();
            $que = QueryBpm::jsonDataBpm("", BPM_URL_GETCOUNTITEMS);
        }
        if (($que["status"] == 200) && ($que["success"]["GetCountItemsResult"]["code"] == 100)) {
            return $que["success"]["GetCountItemsResult"]["countKkt"];
        } else {
            return "-1";
        }
    }


    public static function GetCountKKTBITRIX()
    {

        $iblockId = "76";
        $arSelect = Array("ID");
        $countcontact = 0;

        $res = CIBlockElement::GetList(Array(), Array(), false, false, $arSelect);
        while ($ob = $res->GetNextElement()) {
            /* $arFields = $ob->GetFields();*/
            $countcontact++;
        }
        return $countcontact;
    }

    public static function BPMGetContactInfoBPMByDateArray($date)
    {
        $datetosend = array(
            "date" => $date
        );
        $que = QueryBpm::jsonDataBpm($datetosend, BPM_URL_GETCONTACTINFOBYDATE);
        if ($que["status"] == 403) {
            QueryBpm::login();
            $que = QueryBpm::jsonDataBpm("", BPM_URL_GETCONTACTINFOBYDATE);
        }
        //print_r($que);
        $answerarray = array();
       if (($que["status"] == 200) && ($que["success"]["GetContactInfoByDateResult"]["code"] == 100)) {

           $i=0;
           foreach($que["success"]["GetContactInfoByDateResult"]["contactCountInfo"] as $contactitem)
           {
               $answerarrayitem=array();
               $answerarrayitem["createdOn"]=$contactitem["createdOn"];
               $answerarrayitem["GUID"]=$contactitem["id"];
               $answerarrayitem["modifiedOn"]=$contactitem["modifiedOn"];
               $answerarray[$i]=$answerarrayitem; $i++;
               //print_r($answerarrayitem);echo "<br>";
           }

           if($que["success"]["GetContactInfoByDateResult"]["countContact"]==count($answerarray))
           {
               return $answerarray;
           } else {
               return "-1";
           }

        } else {
            return "-1";
        }
    }


    public static function BITRIXGetContactInfoByDateCount($date)
    {

        $pieces = explode(".", $date);
        $pieces[0]=$pieces[0]+1;
        $date_next = implode(".", $pieces);


        $res = CCrmContact::GetListEx(array(), array('>=DATE_CREATE'=>$date,'<DATE_CREATE'=>$date_next,'CHECK_PERMISSIONS' => 'N'),array('*'));
        $countelement=0;
        while ($fields = $res->Fetch()) {
            $countelement++;
        }
        return $countelement;

    }

    public static function BPMGetContactInfoByDateCount($date)
    {
        $datetosend = array(
            "date" => $date
        );
        $que = QueryBpm::jsonDataBpm($datetosend, BPM_URL_GETCONTACTINFOBYDATE);
        if ($que["status"] == 403) {
            QueryBpm::login();
            $que = QueryBpm::jsonDataBpm("", BPM_URL_GETCONTACTINFOBYDATE);
        }
        //print_r($que);
        $answerarray = array();
        if (($que["status"] == 200) && ($que["success"]["GetContactInfoByDateResult"]["code"] == 100)) {
                return $que["success"]["GetContactInfoByDateResult"]["countContact"];
        } else {
            return "-1";
        }
    }

    public static function BITRIXGetContactInfoByDateArray($date)
    {

        $pieces = explode(".", $date);
        $pieces[0]=$pieces[0]+1;
        $date_next = implode(".", $pieces);


        $res = CCrmContact::GetListEx(array(), array('>=DATE_CREATE'=>$date,'<DATE_CREATE'=>$date_next,'CHECK_PERMISSIONS' => 'N'),array('*'));
        $arrayelements=array();
        while ($fields = $res->Fetch()) {
            $arrayelements[]=$fields['ID'];
        }
        return $arrayelements;
    }


    public static function BITRIXGetAccountInfoByDateCount($date)
    {

        $pieces = explode(".", $date);
        $pieces[0]=$pieces[0]+1;
        $date_next = implode(".", $pieces);


        $res = CCrmCompany::GetListEx(array(), array('>=DATE_CREATE'=>$date,'<DATE_CREATE'=>$date_next,'CHECK_PERMISSIONS' => 'N'),array('*'));
        $countelement=0;
        while ($fields = $res->Fetch()) {
            $countelement++;
        }
        return $countelement;

    }



    public static function BITRIXGetAccountInfoByDateArray($date)
    {

        $pieces = explode(".", $date);
        $pieces[0]=$pieces[0]+1;
        $date_next = implode(".", $pieces);


        $res = CCrmCompany::GetListEx(array(), array('>=DATE_CREATE'=>$date,'<DATE_CREATE'=>$date_next,'CHECK_PERMISSIONS' => 'N'),array('*'));
        $arrayelements=array();
        while ($fields = $res->Fetch()) {
            $arrayelements[]=$fields['ID'];
        }
        return $arrayelements;
    }










    public static function BPMGetAccountInfoByDateArray($date)
    {
        $datetosend = array(
            "date" => $date
        );

        $que = QueryBpm::jsonDataBpm($datetosend, BPM_URL_GETACCOUNTINFOBYDATE);
        if ($que["status"] == 403) {
            QueryBpm::login();
            $que = QueryBpm::jsonDataBpm("", BPM_URL_GETACCOUNTINFOBYDATE);
        }
        //print_r($que);


        if (($que["status"] == 200) && ($que["success"]["GetAccountInfoByDateResult"]["code"] == 100)) {
            $answerarray = array();
            $i=0;
            foreach($que["success"]["GetAccountInfoByDateResult"]["accountCountInfo"] as $contactitem)
            {
                $answerarrayitem=array();
                $answerarrayitem["createdOn"]=$contactitem["createdOn"];
                $answerarrayitem["GUID"]=$contactitem["id"];
                $answerarrayitem["modifiedOn"]=$contactitem["modifiedOn"];
                $answerarray[]=$answerarrayitem;

            }
         //   print_r(count($answerarray));echo "<br>";
          //  print_r(que["success"]["accountCountInfo"]["countAccount"]);echo "<br>";

            if($que["success"]["GetAccountInfoByDateResult"]["countAccount"]==count($answerarray))
            {
                return $answerarray;
            } else {
                return "-1";
            }

        } else {
            return "-1";
        }
    }


    public static function BPMGetAccountInfoByDateCount($date)
    {
        $datetosend = array(
            "date" => $date
        );
        $que = QueryBpm::jsonDataBpm($datetosend, BPM_URL_GETACCOUNTINFOBYDATE);
        if ($que["status"] == 403) {
            QueryBpm::login();
            $que = QueryBpm::jsonDataBpm("", BPM_URL_GETACCOUNTINFOBYDATE);
        }
        //print_r($que);
        $answerarray = array();
        if (($que["status"] == 200) && ($que["success"]["GetAccountInfoByDateResult"]["code"] == 100)) {
            return $que["success"]["GetAccountInfoByDateResult"]["countAccount"];
        } else {
            return "-1";
        }
    }


}