<?
use Bitrix\Main\CUserTypeEntity,
    Bitrix\Main\Loader;

Loader::includeModule("iblock");
Loader::includeModule("crm");

class KktBitrixBpm
{
    public static function selectKtt($id)
    {
        $iblockId = CIBlockElement::GetIBlockByID($id);
        $arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM","PROPERTY_*");//IBLOCK_ID и ID обязательно должны быть указаны, см. описание arSelectFields выше
        $arFilter = Array("ID"=>IntVal($id),"IBLOCK_ID"=>IntVal($iblockId), "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
        $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
        while($ob = $res->GetNextElement()){
            $arFields = $ob->GetFields();
            krumo($arFields);
            $arProps = $ob->GetProperties();
            krumo($arProps);
        }
    }
    public static function GetGuidKktById($id)
    {
        $iblockId = CIBlockElement::GetIBlockByID($id);
        $arFilter = Array("ID"=>IntVal($id),"IBLOCK_ID"=>IntVal($iblockId), "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
        $arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM","PROPERTY_*");
        $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
        while($ob = $res->GetNextElement()){
            $arFields = $ob->GetFields();
            $arProps = $ob->GetProperties();
            return $arProps["GUID_KKT"]["VALUE"];
        }
    }


    public static function eventSelectKkt($id = null, $guidKkt = null)
    {
        if(!empty($id)){
            $iblockId = CIBlockElement::GetIBlockByID($id);
        }else{
            $iblockId = "76";
        }

        if(empty($guidKkt)){
            $arFilter = Array("ID"=>IntVal($id),"IBLOCK_ID"=>IntVal($iblockId), "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
        }else{
            $arFilter = Array("IBLOCK_ID"=>IntVal($iblockId), "PROPERTY_GUID_KTT" => strtoupper($guidKkt));
        }

        $arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM","PROPERTY_*");//IBLOCK_ID и ID обязательно должны быть указаны, см. описание arSelectFields выше

        $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
        while($ob = $res->GetNextElement()){
            $arFields = $ob->GetFields();
            $arProps = $ob->GetProperties();
            $result["Name"] = $arFields["NAME"];
            $result["ModelCashbox"] = $arProps["MODEL_KKT"]["VALUE"];
            $result["RnmCashbox"] = $arProps["RNM_KKT"]["VALUE"];
            $result["FactoryNumber"] = $arProps["NUM_KKT"]["VALUE"];
            $result["PurchaseDate"] = "";
            $result["CancelDate"] = "";
            $result["Address"] = $arProps["ADDR_KKT"]["VALUE"];
            $result["Status"] = $arProps["STATUS_KKT"]["VALUE"];
            $result["Price"] = $arProps["COST_TARIF"]["VALUE"];
            $result["ActivationDate"] = $arProps["DATE_ACTIVE"]["VALUE"];
            $result["DeactivationDate"] = $arProps["DATE_OFF"]["VALUE"];
            $result["PromotionalCode"] = $arProps["PROMO_KOD"]["VALUE"];
            $result["ActivationPeriod"] = $arProps["PERIOD_ACTIVE"]["VALUE"];
            $result["DeactivationPeriod"] = $arProps["PERIOD_OFF"]["VALUE"];
            $result["Rate"] = $arProps["TARIF"]["VALUE"];
            $result["COMPANY"] = $arProps["COMPANY"]["VALUE"];
            $result["GUID"] = $arProps["GUID_KTT"]["VALUE"];
            $result["ID"] = $arFields["ID"];
        }

        $guidContragent = null;
        if(!empty($result["COMPANY"])) {
            $arCompany = RestBpmBitrix::verify(null,$result["COMPANY"]);
            $guidContragent = $arCompany['guid'];
        }

        $result["idContragent"] = $guidContragent;

        if( !empty($result["GUID"]) ){
            $result["idKtt"] = $result["GUID"];
            $guidKkt = $result["GUID"];
        } else {
            $result["idKtt"] = RestBpmBitrix::generate_guid();
            CIBlockElement::SetPropertyValues($result["ID"], $iblockId, $result["idKtt"], "GUID_KTT");
        }

        $res = array();
		if( !empty($guidKkt) and empty($result["ID"]) ){
            $res["id"] = "-101";
            $res["guid"] = $guidKkt;
        } elseif ( !empty($guidKkt) and !empty($result["ID"]) ){
            $res = $result;
            $res["guid"] = $guidKkt;
        }

        return $res;
    }

    public static function addKkt($arrayKtt = array())
    {
        $PurchaseDate = $arrayKtt["PurchaseDate"];
        $CancelDate = $arrayKtt["CancelDate"];
        $ActivationDate = $arrayKtt["ActivationDate"];
        $DeactivationDate = $arrayKtt["DeactivationDate"];
        /*
        $;
        $;
        $PurchaseDate;
        $CancelDate;
        $Address;
        $Status;
        $Price;
        $ActivationDate;
        $DeactivationDate;
        $PromotionalCode;
        $ActivationPeriod;
        $DeactivationPeriod;
        $Rate;//тариф
        $idKtt;
        $idContragent; Status*/

        switch ($arrayKtt["Status"]) {
            case mb_strtolower("SKLAD"):
                $Status = KKT_STATUS_SKLAD;
                break;
            default:
                $Status = KKT_STATUS_DELAY;
        }


        $batshAddKkt = array(
            'items' =>
                array(
                    0 =>
                        array(
                            '__type' => 'Terrasoft.Nui.ServiceModel.DataContract.InsertQuery',
                            'RootSchemaName' => 'ConfItem',
                            'OperationType' => 1,
                            'ColumnValues' =>
                                array(
                                    'Items' =>
                                        array(
											  'Category' =>
                                                array(
                                                    'ExpressionType' => 2,
                                                    'Parameter' =>
                                                        array(
                                                            'DataValueType' => 0,
                                                            'Value' => "da2cf289-b4a2-40dd-9795-f35c5c9bc70a",
                                                        ),
),
                                            'Id' =>
                                                array(
                                                    'ExpressionType' => 2,
                                                    'Parameter' =>
                                                        array(
                                                            'DataValueType' => 0,
															'Value' => $arrayKtt["idKtt"],
                                                        ),
                                                ),
                                            'Name' =>
                                                array(
                                                    'ExpressionType' => 2,
                                                    'Parameter' =>
                                                        array(
                                                            'DataValueType' => 1,
                                                            'Value' => $arrayKtt["Name"],
                                                        ),
                                                ),
                                            'UsrModelCashbox' =>
                                                array(
                                                    'ExpressionType' => 2,
                                                    'Parameter' =>
                                                        array(
                                                            'DataValueType' => 1,
                                                            'Value' => $arrayKtt["ModelCashbox"],
                                                        ),
                                                ),
                                            'UsrRnmCashbox' =>
                                                array(
                                                    'ExpressionType' => 2,
                                                    'Parameter' =>
                                                        array(
                                                            'DataValueType' => 1,
                                                            'Value' => $arrayKtt["RnmCashbox"],
                                                        ),
                                                ),
                                            'UsrFactoryNumber' =>
                                                array(
                                                    'ExpressionType' => 2,
                                                    'Parameter' =>
                                                        array(
                                                            'DataValueType' => 1,
                                                            'Value' => $arrayKtt["FactoryNumber"],
                                                        ),
                                                ),
                                            'PurchaseDate' =>
                                                array(
                                                    'ExpressionType' => 2,
                                                    'Parameter' =>
                                                        array(
                                                            'DataValueType' => 8,
                                                            'Value' => "\"$PurchaseDate\"",
                                                        ),
                                                ),
                                            'CancelDate' =>
                                                array(
                                                    'ExpressionType' => 2,
                                                    'Parameter' =>
                                                        array(
                                                            'DataValueType' => 8,
                                                            'Value' => "\"$CancelDate\"",
                                                        ),
                                                ),
                                            'Address' =>
                                                array(
                                                    'ExpressionType' => 2,
                                                    'Parameter' =>
                                                        array(
                                                            'DataValueType' => 1,
                                                            'Value' => $arrayKtt["Address"],
                                                        ),
                                                ),
											/* 'Status' =>
                                                array(
                                                    'ExpressionType' => 2,
                                                    'Parameter' =>
                                                        array(
                                                            'DataValueType' => 0,
                                                            'Value' => $Status,
                                                        ),
),*/
                                            'UsrRateCost' =>
                                                array(
                                                    'ExpressionType' => 2,
                                                    'Parameter' =>
                                                        array(
                                                            'DataValueType' => 1,
                                                            'Value' => 'стоимость '.$arrayKtt["Price"].' руб',
                                                        ),
                                                ),
                                            'UsrActivationDate' =>
                                                array(
                                                    'ExpressionType' => 2,
                                                    'Parameter' =>
                                                        array(
                                                            'DataValueType' => 8,
                                                            'Value' => "\"$ActivationDate\"",
                                                        ),
                                                ),
                                            'UsrDeactivationDate' =>
                                                array(
                                                    'ExpressionType' => 2,
                                                    'Parameter' =>
                                                        array(
                                                            'DataValueType' => 8,
                                                            'Value' => "\"$DeactivationDate\"",
                                                        ),
                                                ),
                                            'UsrPromotionalCode' =>
                                                array(
                                                    'ExpressionType' => 2,
                                                    'Parameter' =>
                                                        array(
                                                            'DataValueType' => 1,
                                                            'Value' => $arrayKtt["PromotionalCode"],
                                                        ),
                                                ),
                                            'UsrActivationPeriod' =>
                                                array(
                                                    'ExpressionType' => 2,
                                                    'Parameter' =>
                                                        array(
                                                            'DataValueType' => 1,
                                                            'Value' => $arrayKtt["ActivationPeriod"],
                                                        ),
                                                ),
                                            'UsrDeactivationPeriod' =>
                                                array(
                                                    'ExpressionType' => 2,
                                                    'Parameter' =>
                                                        array(
                                                            'DataValueType' => 1,
                                                            'Value' => $arrayKtt["DeactivationPeriod"],
                                                        ),
                                                ),
                                            'UsrRate' =>
                                                array(
                                                    'ExpressionType' => 2,
                                                    'Parameter' =>
                                                        array(
                                                            'DataValueType' => 1,
                                                            'Value' => $arrayKtt["Rate"],
                                                        ),
                                                ),
                                        ),
                                ),
                        ),
                    1 =>
                        array(
                            '__type' => 'Terrasoft.Nui.ServiceModel.DataContract.InsertQuery',
                            'RootSchemaName' => 'ConfItemUser',
                            'OperationType' => 1,
                            'ColumnValues' =>
                                array(
                                    'Items' =>
                                        array(
                                            'ConfItem' =>
                                                array(
                                                    'ExpressionType' => 2,
                                                    'Parameter' =>
                                                        array(
                                                            'DataValueType' => 0,
															'Value' => $arrayKtt["idKtt"],
                                                        ),
                                                ),
											/*  'Account' =>
                                                array(
                                                    'ExpressionType' => 2,
                                                    'Parameter' =>
                                                        array(
                                                            'DataValueType' => 0,
                                                            'Value' => $arrayKtt["idContragent"],
                                                        ),
),*/
                                        ),
                                ),
                        ),
                )
        );

        $arrKTT = QueryBpm::jsonDataBpmContr($batshAddKkt, BPM_URL_QUERY);
        if($arrKTT["status"] == 403){
            $arrKTT = QueryBpm::jsonDataBpmContr($batshAddKkt, BPM_URL_SELECT);
        }
        $jsonBatch = json_encode($batshAddKkt);
        $logger = Logger::getLogger('KTTAdd','ofd.bitrix24/KTTAdd.json');
        $logger->log($jsonBatch);
        return $arrKTT;
    }

    public static function updateKkt($arrayKtt){
        $PurchaseDate = $arrayKtt["PurchaseDate"];
        $CancelDate = $arrayKtt["CancelDate"];
        $ActivationDate = $arrayKtt["ActivationDate"];
        $DeactivationDate = $arrayKtt["DeactivationDate"];

        switch ($arrayKtt["Status"]) {
            case mb_strtolower("SKLAD"):
                $Status = KKT_STATUS_SKLAD;
                break;
            default:
                $Status = KKT_STATUS_DELAY;
        }

        if (!empty($arrayKtt["idContragent"])) {
            $batshUpdateKkt = array(
                'items' =>
                    array(
                        0 =>
                            array(
                                '__type' => 'Terrasoft.Nui.ServiceModel.DataContract.UpdateQuery',
                                'RootSchemaName' => 'ConfItem',
                                'OperationType' => 1,
                                'ColumnValues' =>
                                    array(
                                        'Items' =>
                                            array(
                                                'Category' =>
                                                    array(
                                                        'ExpressionType' => 2,
                                                        'Parameter' =>
                                                            array(
                                                                'DataValueType' => 0,
                                                                'Value' => "da2cf289-b4a2-40dd-9795-f35c5c9bc70a",
                                                            ),
                                                    ),
                                                'Id' =>
                                                    array(
                                                        'ExpressionType' => 2,
                                                        'Parameter' =>
                                                            array(
                                                                'DataValueType' => 0,
                                                                'Value' => $arrayKtt["idKtt"],
                                                            ),
                                                    ),
                                                'Name' =>
                                                    array(
                                                        'ExpressionType' => 2,
                                                        'Parameter' =>
                                                            array(
                                                                'DataValueType' => 1,
                                                                'Value' => $arrayKtt["Name"],
                                                            ),
                                                    ),
                                                'UsrModelCashbox' =>
                                                    array(
                                                        'ExpressionType' => 2,
                                                        'Parameter' =>
                                                            array(
                                                                'DataValueType' => 1,
                                                                'Value' => $arrayKtt["ModelCashbox"],
                                                            ),
                                                    ),
                                                'UsrRnmCashbox' =>
                                                    array(
                                                        'ExpressionType' => 2,
                                                        'Parameter' =>
                                                            array(
                                                                'DataValueType' => 1,
                                                                'Value' => $arrayKtt["RnmCashbox"],
                                                            ),
                                                    ),
                                                'UsrFactoryNumber' =>
                                                    array(
                                                        'ExpressionType' => 2,
                                                        'Parameter' =>
                                                            array(
                                                                'DataValueType' => 1,
                                                                'Value' => $arrayKtt["FactoryNumber"],
                                                            ),
                                                    ),
                                                'PurchaseDate' =>
                                                    array(
                                                        'ExpressionType' => 2,
                                                        'Parameter' =>
                                                            array(
                                                                'DataValueType' => 8,
                                                                'Value' => "\"$PurchaseDate\"",
                                                            ),
                                                    ),
                                                'CancelDate' =>
                                                    array(
                                                        'ExpressionType' => 2,
                                                        'Parameter' =>
                                                            array(
                                                                'DataValueType' => 8,
                                                                'Value' => "\"$CancelDate\"",
                                                            ),
                                                    ),
                                                'Address' =>
                                                    array(
                                                        'ExpressionType' => 2,
                                                        'Parameter' =>
                                                            array(
                                                                'DataValueType' => 1,
                                                                'Value' => $arrayKtt["Address"],
                                                            ),
                                                    ),
                                                'Status' =>
                                                    array(
                                                        'ExpressionType' => 2,
                                                        'Parameter' =>
                                                            array(
                                                                'DataValueType' => 0,
                                                                'Value' => $Status,
                                                            ),
                                                    ),
                                                'UsrRateCost' =>
                                                    array(
                                                        'ExpressionType' => 2,
                                                        'Parameter' =>
                                                            array(
                                                                'DataValueType' => 1,
                                                                'Value' => 'стоимость ' . $arrayKtt["Price"] . ' руб',
                                                            ),
                                                    ),
                                                'UsrActivationDate' =>
                                                    array(
                                                        'ExpressionType' => 2,
                                                        'Parameter' =>
                                                            array(
                                                                'DataValueType' => 8,
                                                                'Value' => "\"$ActivationDate\"",
                                                            ),
                                                    ),
                                                'UsrDeactivationDate' =>
                                                    array(
                                                        'ExpressionType' => 2,
                                                        'Parameter' =>
                                                            array(
                                                                'DataValueType' => 8,
                                                                'Value' => "\"$DeactivationDate\"",
                                                            ),
                                                    ),
                                                'UsrPromotionalCode' =>
                                                    array(
                                                        'ExpressionType' => 2,
                                                        'Parameter' =>
                                                            array(
                                                                'DataValueType' => 1,
                                                                'Value' => $arrayKtt["PromotionalCode"],
                                                            ),
                                                    ),
                                                'UsrActivationPeriod' =>
                                                    array(
                                                        'ExpressionType' => 2,
                                                        'Parameter' =>
                                                            array(
                                                                'DataValueType' => 1,
                                                                'Value' => $arrayKtt["ActivationPeriod"],
                                                            ),
                                                    ),
                                                'UsrDeactivationPeriod' =>
                                                    array(
                                                        'ExpressionType' => 2,
                                                        'Parameter' =>
                                                            array(
                                                                'DataValueType' => 1,
                                                                'Value' => $arrayKtt["DeactivationPeriod"],
                                                            ),
                                                    ),
                                                'UsrRate' =>
                                                    array(
                                                        'ExpressionType' => 2,
                                                        'Parameter' =>
                                                            array(
                                                                'DataValueType' => 1,
                                                                'Value' => $arrayKtt["Rate"],
                                                            ),
                                                    ),
                                            ),
                                    ),
                            ),
                        1 =>
                            array(
                                '__type' => 'Terrasoft.Nui.ServiceModel.DataContract.InsertQuery',
                                'RootSchemaName' => 'ConfItemUser',
                                'OperationType' => 1,
                                'ColumnValues' =>
                                    array(
                                        'Items' =>
                                            array(
                                                'ConfItem' =>
                                                    array(
                                                        'ExpressionType' => 2,
                                                        'Parameter' =>
                                                            array(
                                                                'DataValueType' => 0,
                                                                'Value' => $arrayKtt["idKtt"],
                                                            ),
                                                    ),
                                                'Account' =>
                                                    array(
                                                        'ExpressionType' => 2,
                                                        'Parameter' =>
                                                            array(
                                                                'DataValueType' => 0,
                                                                'Value' => $arrayKtt["idContragent"],
                                                            ),
                                                    ),
                                            ),
                                    ),
                            ),
                    )
            );
        }
        else
        {
            $batshUpdateKkt = array(
                'items' =>
                    array(
                        0 =>
                            array(
                                '__type' => 'Terrasoft.Nui.ServiceModel.DataContract.UpdateQuery',
                                'RootSchemaName' => 'ConfItem',
                                'OperationType' => 1,
                                'ColumnValues' =>
                                    array(
                                        'Items' =>
                                            array(
                                                'Category' =>
                                                    array(
                                                        'ExpressionType' => 2,
                                                        'Parameter' =>
                                                            array(
                                                                'DataValueType' => 0,
                                                                'Value' => "da2cf289-b4a2-40dd-9795-f35c5c9bc70a",
                                                            ),
                                                    ),
                                                'Id' =>
                                                    array(
                                                        'ExpressionType' => 2,
                                                        'Parameter' =>
                                                            array(
                                                                'DataValueType' => 0,
                                                                'Value' => $arrayKtt["idKtt"],
                                                            ),
                                                    ),
                                                'Name' =>
                                                    array(
                                                        'ExpressionType' => 2,
                                                        'Parameter' =>
                                                            array(
                                                                'DataValueType' => 1,
                                                                'Value' => $arrayKtt["Name"],
                                                            ),
                                                    ),
                                                'UsrModelCashbox' =>
                                                    array(
                                                        'ExpressionType' => 2,
                                                        'Parameter' =>
                                                            array(
                                                                'DataValueType' => 1,
                                                                'Value' => $arrayKtt["ModelCashbox"],
                                                            ),
                                                    ),
                                                'UsrRnmCashbox' =>
                                                    array(
                                                        'ExpressionType' => 2,
                                                        'Parameter' =>
                                                            array(
                                                                'DataValueType' => 1,
                                                                'Value' => $arrayKtt["RnmCashbox"],
                                                            ),
                                                    ),
                                                'UsrFactoryNumber' =>
                                                    array(
                                                        'ExpressionType' => 2,
                                                        'Parameter' =>
                                                            array(
                                                                'DataValueType' => 1,
                                                                'Value' => $arrayKtt["FactoryNumber"],
                                                            ),
                                                    ),
                                                'PurchaseDate' =>
                                                    array(
                                                        'ExpressionType' => 2,
                                                        'Parameter' =>
                                                            array(
                                                                'DataValueType' => 8,
                                                                'Value' => "\"$PurchaseDate\"",
                                                            ),
                                                    ),
                                                'CancelDate' =>
                                                    array(
                                                        'ExpressionType' => 2,
                                                        'Parameter' =>
                                                            array(
                                                                'DataValueType' => 8,
                                                                'Value' => "\"$CancelDate\"",
                                                            ),
                                                    ),
                                                'Address' =>
                                                    array(
                                                        'ExpressionType' => 2,
                                                        'Parameter' =>
                                                            array(
                                                                'DataValueType' => 1,
                                                                'Value' => $arrayKtt["Address"],
                                                            ),
                                                    ),
                                                'Status' =>
                                                    array(
                                                        'ExpressionType' => 2,
                                                        'Parameter' =>
                                                            array(
                                                                'DataValueType' => 0,
                                                                'Value' => $Status,
                                                            ),
                                                    ),
                                                'UsrRateCost' =>
                                                    array(
                                                        'ExpressionType' => 2,
                                                        'Parameter' =>
                                                            array(
                                                                'DataValueType' => 1,
                                                                'Value' => 'стоимость ' . $arrayKtt["Price"] . ' руб',
                                                            ),
                                                    ),
                                                'UsrActivationDate' =>
                                                    array(
                                                        'ExpressionType' => 2,
                                                        'Parameter' =>
                                                            array(
                                                                'DataValueType' => 8,
                                                                'Value' => "\"$ActivationDate\"",
                                                            ),
                                                    ),
                                                'UsrDeactivationDate' =>
                                                    array(
                                                        'ExpressionType' => 2,
                                                        'Parameter' =>
                                                            array(
                                                                'DataValueType' => 8,
                                                                'Value' => "\"$DeactivationDate\"",
                                                            ),
                                                    ),
                                                'UsrPromotionalCode' =>
                                                    array(
                                                        'ExpressionType' => 2,
                                                        'Parameter' =>
                                                            array(
                                                                'DataValueType' => 1,
                                                                'Value' => $arrayKtt["PromotionalCode"],
                                                            ),
                                                    ),
                                                'UsrActivationPeriod' =>
                                                    array(
                                                        'ExpressionType' => 2,
                                                        'Parameter' =>
                                                            array(
                                                                'DataValueType' => 1,
                                                                'Value' => $arrayKtt["ActivationPeriod"],
                                                            ),
                                                    ),
                                                'UsrDeactivationPeriod' =>
                                                    array(
                                                        'ExpressionType' => 2,
                                                        'Parameter' =>
                                                            array(
                                                                'DataValueType' => 1,
                                                                'Value' => $arrayKtt["DeactivationPeriod"],
                                                            ),
                                                    ),
                                                'UsrRate' =>
                                                    array(
                                                        'ExpressionType' => 2,
                                                        'Parameter' =>
                                                            array(
                                                                'DataValueType' => 1,
                                                                'Value' => $arrayKtt["Rate"],
                                                            ),
                                                    ),
                                            ),
                                    ),
                            ),
                        1 =>
                            array(
                                '__type' => 'Terrasoft.Nui.ServiceModel.DataContract.InsertQuery',
                                'RootSchemaName' => 'ConfItemUser',
                                'OperationType' => 1,
                                'ColumnValues' =>
                                    array(
                                        'Items' =>
                                            array(
                                                'ConfItem' =>
                                                    array(
                                                        'ExpressionType' => 2,
                                                        'Parameter' =>
                                                            array(
                                                                'DataValueType' => 0,
                                                                'Value' => $arrayKtt["idKtt"],
                                                            ),
                                                    ),
                                            ),
                                    ),
                            ),
                    )
            );
        }

        $arrKTT = QueryBpm::jsonDataBpmContr($batshUpdateKkt, BPM_URL_QUERY);
        if($arrKTT["status"] == 403){
            $arrKTT = QueryBpm::jsonDataBpmContr($batshUpdateKkt, BPM_URL_SELECT);
        }
        $jsonBatch = json_encode($batshUpdateKkt);
        $logger = Logger::getLogger('KTTUPDATE','ofd.bitrix24/KTTUPDATE.json');
        $logger->log($jsonBatch);

        return $arrKTT;
    }
}