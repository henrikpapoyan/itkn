<?
use Bitrix\Main\CUserTypeEntity,
    Bitrix\Main\Loader;
Loader::includeModule("iblock");
Loader::includeModule("crm");

class MergeDubleContragent
{
    //сбор информации по дублям
    public static function dublesArrey($type = null){
        $connection = Bitrix\Main\Application::getConnection();
        $strSql = "SELECT
				b_crm_company.ID,
                b_crm_requisite.RQ_INN, 
                b_crm_requisite.RQ_KPP, 
                b_crm_requisite.ENTITY_ID
			  FROM b_crm_company 
			  LEFT JOIN b_crm_requisite ON b_crm_company.ID = b_crm_requisite.ENTITY_ID
				ORDER BY b_crm_company.ID ASC";

        $resCompany = $connection->query($strSql);
        while ($arCompany = $resCompany->Fetch()) {
            $arResult[$arCompany['ID']] = array(
                "INN" => $arCompany['RQ_INN'],
                "KPP" => $arCompany['RQ_KPP']
            );
        }
        foreach ($arResult as $key=>$val){
            if(!empty($val["INN"]) and !empty($val["KPP"]) and strlen($val["INN"]) == 10){
                //Контрагенты для проверки связанных сущностей
                $company[$key] = array($val["INN"],$val["KPP"]);
                continue;
            }
            if(!empty($val["INN"]) and empty($val["KPP"]) and strlen($val["INN"]) == 12){
                $ipe[$key] = array($val["INN"]);
            }
        }

        if($type == 1){
            $massiv["COMPANY"] = $company;
            $compArr = array_count_values(array_map("serialize", $massiv["COMPANY"]));
            $compRes = array_filter($compArr, function ($elem) {
                return ($elem > 1);
            });

            $result_array_company = [];
            foreach ($compRes as $key => $value) {
                $unser = unserialize($key);
                $idcompany = idForInnKpp($unser[0], $unser[1]);
                array_push($result_array_company, $idcompany);
            }

            return $result_array_company;

        }elseif($type == 2){
            $massiv["IP"] = $ipe;
            $ipArr = array_count_values(array_map("serialize", $massiv["IP"]));
            $ipRes = array_filter($ipArr, function ($elemip) {
                return ($elemip > 1);
            });

            $result_array_ip = [];
            foreach ($ipRes as $key => $value) {
                $ipser = unserialize($key);
                $idip = idForInnKpp($ipser[0]);
                array_push($result_array_ip, $idip);
            }

            return $result_array_ip;

        }else{
            $massiv["COMPANY"] = $company;
            $compArr = array_count_values(array_map("serialize", $massiv["COMPANY"]));
            $compRes = array_filter($compArr, function ($elem) {
                return ($elem > 1);
            });

            $result_array_company = [];
            foreach ($compRes as $key => $value) {
                $unser = unserialize($key);
                pre($unser);
                $idcompany = idForInnKpp($unser[0], $unser[1]);
                pre($idcompany);
                array_push($result_array_company, $idcompany);
            }

            $massiv["IP"] = $ipe;
            $ipArr = array_count_values(array_map("serialize", $massiv["IP"]));
            $ipRes = array_filter($ipArr, function ($elemip) {
                return ($elemip > 1);
            });

            $result_array_ip = [];
            foreach ($ipRes as $key => $value) {
                $ipser = unserialize($key);
                $idip = idForInnKpp($ipser[0]);
                array_push($result_array_ip, $idip);
            }

            $dubleMass = array($result_array_company,$result_array_ip);
            echo "а.) Дубли компаний: ".count($dubleMass[0])."<br>";
            echo "б.) Дубли ИП: ".count($dubleMass[1])."<br>";
            return $dubleMass;
        }
    }
    //сливание от двух и более дублированых контрагентов
    public static function arrayDubleContragentStart($arrIdContragents = array()){
        if(!is_array($arrIdContragents) or count($arrIdContragents) < 2){
            return false;
        }else{
            sort($arrIdContragents);
        }
        if(count($arrIdContragents) == 2){
            $oneID = $arrIdContragents[0];
            $twoID = $arrIdContragents[1];
            $result[$twoID] = $oneID;
        }
        if(count($arrIdContragents) > 2){
            $count = count($arrIdContragents);
            // берем самого старого
            $elem = array_kshift($arrIdContragents);
            $oneID = current($elem);
            // подсчитываем оставшихся
            for ($i = 1; $i < $count; ++$i){
                $elemTwo = array_kshift($arrIdContragents);
                $twoID = current($elemTwo);
                $result[$twoID] = $oneID;
            }

        }
        foreach ($result as $key=>$value){
            $res[] = self::mergDubleContragent(array($key,$value));
        }
        return ($res);
    }
    //Слияние дублей контрагента
    public static function mergDubleContragent($typContragent, $break){
        if(empty($typContragent)){
            echo "Укажите тип контрагента: 1 - компания, 2 - ИП";
            return false;
        }
        //количество обрабатываемых дубляжей контрагентов
        if(empty($break)){
            $break = 1;
        }

        if($typContragent === 1){
            $mDuble = self::dublesArrey(1);
            if(empty($mDuble)){
                echo "Дублей компаний нет!";
                return false;
            }
            $res['Дублей'] = "компаний - ".count($mDuble);
        }
        if($typContragent === 2){
            $mDuble = self::dublesArrey(2);
            if(empty($mDuble)){
                echo "Дублей ИП нет!";
                return false;
            }
            $res['Дублей'] = "ИП - ".count($mDuble);
        }

        $count = 0;
        foreach ($mDuble as $kDubles => $arEntityID) {

            if (count($arEntityID) != 2) {
                return false;
            } else {
                sort($arEntityID);
                $MainCompanyID = $arEntityID[0];
                $OldCompanyID = $arEntityID[1];
            }

            // источник
            $seedEntityID = (int)$OldCompanyID;
            // приемник
            $targEntityID = (int)$MainCompanyID;

            // СДЕЛКИ и ККТ
            $Deal = self::mergCrmDeal($arEntityID);
            $res[$MainCompanyID]['сделки и ктт'] = $Deal;
            // СЧЕТА
            $Invoce = self::margInvoice($arEntityID);
            $res[$MainCompanyID]['счета'] = $Invoce;
            // ДОГОВОРЫ
            $Dogovor = self::mergIblockDogovor($arEntityID);
            $res[$MainCompanyID]['договора'] = $Dogovor;
            // РЕЕСТР ПЛАТЕЖЕЙ
            $Pay = self::mergIblockPay($arEntityID);
            $res[$MainCompanyID]['платежи'] = $Pay;
            // ОБРАЩЕНИЯ
            $Tikets = self::mergIblockTiket($arEntityID);
            $res[$MainCompanyID]['обращения'] = $Tikets;
            //
            /*$entityTypeName = "COMPANY";
            $entityTypeID = CCrmOwnerType::ResolveID($entityTypeName);
            $currentUser = CCrmSecurityHelper::GetCurrentUser();
            $currentUserID = (int)$currentUser->GetID();

            $index_type_name = 'ORGANIZATION';
            $enablePermissionCheck = !CCrmPerms::IsAdmin($currentUserID);
            $typeID = \Bitrix\Crm\Integrity\DuplicateIndexType::resolveID($index_type_name);
            $matches = array();
            $criterion = \Bitrix\Crm\Integrity\DuplicateManager::createCriterion($typeID, $matches);


            $mergers = new Bitrix\Crm\Merger\CompanyMerger();
            $mergers->merge($seedEntityID, $targEntityID, $criterion);
            if(empty($guid)){
                $ver = RestBpmBitrix::verify(null,$seedEntityID);
                $result = CrmBitrixBpm::delContrContact($ver['guid']);
            }else{
                $result = CrmBitrixBpm::delContrContact($guid);
            }*/
            //РЕКВИЗИТЫ, КОНТАКТЫ, ИНФОРМАЦИЯ О КОМПАНИИ
            $entityTypeName = "COMPANY";
            $entityTypeID = CCrmOwnerType::ResolveID($entityTypeName);
            $currentUser = CCrmSecurityHelper::GetCurrentUser();
            $currentUserID = (int)$currentUser->GetID();
            $enablePermissionCheck = !CCrmPerms::IsAdmin($currentUserID);
            $merger = \Bitrix\Crm\Merger\EntityMerger::create($entityTypeID, $currentUserID, $enablePermissionCheck);
            $index_type_name = 'ORGANIZATION';
            $typeID = \Bitrix\Crm\Integrity\DuplicateIndexType::resolveID($index_type_name);
            $matches = array();
            $criterion = \Bitrix\Crm\Integrity\DuplicateManager::createCriterion($typeID, $matches);
            $merger->merge($seedEntityID, $targEntityID, $criterion);

            $res[$MainCompanyID]['источник']=  $OldCompanyID;
            if($count == $break){
                break;
            }
            $count++;
        }
        return $res;
    }
    //перепривязка слияние сделок
    public static function mergCrmDeal($arrCompanyId = array()){
        $connection = Bitrix\Main\Application::getConnection();
        if(count($arrCompanyId) != 2){
            return false;
        }else{
            sort($arrCompanyId);
            $MainCompanyID = $arrCompanyId[0];
            $OldCompanyID = $arrCompanyId[1];
        }

        //проверка сделок у приемника MAIN
        $sql = 'SELECT  ID, TYPE_ID, OPPORTUNITY FROM  b_crm_deal WHERE COMPANY_ID = \''.$MainCompanyID.'\'';
        $CCrmDeal = $connection->query($sql);

        while ($arDeal = $CCrmDeal->Fetch()) {
            //есть ли основная услуга у приемника
            if($arDeal["TYPE_ID"] == 2){
                $mainCompanyOfd = $arDeal;
            }
        }

        //нет основной сделки у приемника MAIN
        if(!is_array($mainCompanyOfd)){
            //проверка сделок у источника
            $sql = 'SELECT  ID, TYPE_ID FROM  b_crm_deal WHERE COMPANY_ID = \''.$OldCompanyID.'\'';
            $CCrmDeal = $connection->query($sql);
            while ($arDeal = $CCrmDeal->Fetch()) {
                if($arDeal["TYPE_ID"] == 2){
                    $oldOfdId = $arDeal["ID"];
                }
                $oldCompanyDel[] = $arDeal["ID"];
            }
            //привязываем все сделки источника к MAIN
            if(is_array($oldCompanyDel)){

                foreach ($oldCompanyDel as $idDel=>$valueDel){
                    $sql = 'UPDATE b_crm_deal SET COMPANY_ID = \'' . $MainCompanyID . '\' WHERE ID = \'' . $valueDel . '\' ';
                    $connection->query($sql);
                }
                //если основная сделка только у источника - перепривязываем ккт

                if(!empty($oldOfdId)){
                        $rKkt = self::mergIblockKkt($arrCompanyId, $oldOfdId);
                        return $rKkt;
                    }else{
                        $rKkt = self::mergIblockKkt($arrCompanyId);
                        return $rKkt;
                    }
                }else{
                    $rKkt = self::mergIblockKkt($arrCompanyId);
                    return array("Нет сделок у источника для перепривязки", $rKkt);
            }

        }
        elseif(is_array($mainCompanyOfd)) {
            //у приемника есть Сделка Основная услуга ОФД есть ли Основная услуга ОФД у источника?
            $sql = 'SELECT  ID, TYPE_ID, OPPORTUNITY FROM  b_crm_deal WHERE COMPANY_ID = \''.$OldCompanyID.'\'';
            $CCrmDeal = $connection->query($sql);

            while ($arDeal = $CCrmDeal->Fetch()) {
                if($arDeal["TYPE_ID"] == 2){
                    $oldCompanyOfd = $arDeal;
                }else{
                    $oldCompanyDel[] = $arDeal;
                }

            }

            //Основная услуга ОФД у источника и приемника есть
            if(is_array($oldCompanyOfd)){

                //суммирование суммы сделок
                $OPPORTUNITY = trim($mainCompanyOfd['OPPORTUNITY'] + $oldCompanyOfd['OPPORTUNITY']);

                //обновление сделки у приемника
                $idDelMian = trim($mainCompanyOfd["ID"]);

                $sql = 'UPDATE b_crm_deal SET OPPORTUNITY = \'' . $OPPORTUNITY . '\', OPPORTUNITY_ACCOUNT = \'' . $OPPORTUNITY . '\'  WHERE ID = \'' . $idDelMian . '\' ';
                $connection->query($sql);

                //удаление сделки источника
                $oldDel = trim($oldCompanyOfd["ID"]);
                $sql = 'DELETE FROM b_crm_deal WHERE ID = \'' . $oldDel . '\' ';
                $connection->query($sql);

                //обновление остальных сделок источника на контрагента источника
                if(is_array($oldCompanyDel)){
                    foreach ($oldCompanyDel as $idDel=>$valueDel){
                        $sql = 'UPDATE b_crm_deal SET COMPANY_ID = \'' . $MainCompanyID . '\' WHERE ID = \'' . $valueDel . '\' ';
                        $connection->query($sql);
                    }
                }
                //перепривязка ККТ обновление привязки к контрагенту и к сделки
                $rKkt = self::mergIblockKkt($arrCompanyId, $idDelMian);
                return $rKkt;

            }else{
                $idDelMian = trim($mainCompanyOfd["ID"]);
                $rKkt = self::mergIblockKkt($arrCompanyId, $idDelMian);
                return $rKkt;
            }

        }
        else{
            $rKkt = self::mergIblockKkt($arrCompanyId);
            return $rKkt;
        }

    }
    //перепривязка счетов + ккт
    public static function margInvoice($arrCompanyId = array()){
        $connection = Bitrix\Main\Application::getConnection();
        $oCrmInvoice = new CCrmInvoice;
        if(count($arrCompanyId) != 2){
            return false;
        }else{
            sort($arrCompanyId);
            $MainCompanyID = trim($arrCompanyId[0]);
            $OldCompanyID = trim($arrCompanyId[1]);
        }

        $sql = 'SELECT OWNER_ID FROM b_crm_invoice_sum_stat WHERE COMPANY_ID = \''.$OldCompanyID.'\'';
        $recordset = $connection->query($sql);

        while ($record = $recordset->fetch())
        {
            $Invoice[] =  $record['OWNER_ID'];
        }

        if(empty($Invoice[0])){
            return 'Нет у источника счетов';
        }else{
            $time = date('Y-m-d H');
            $arFields["UF_COMPANY_ID"] =  $MainCompanyID;
            foreach ($Invoice as $idInv=>$vInv){
                $result[$idInv] = $oCrmInvoice->Update($vInv, $arFields);
                //запись ID перепривязанных счетов
                writeArrayInFile($vInv, "InvoiceUpdateMerg - ".$time, "dubles");
            }
            if(!empty($result)){
                $res = 'Перепривязано счетов - '.count($result);
                return $res;
            }
            return false;
        }
    }
    //перепривязка договоров
    public static function mergIblockDogovor ($arrCompanyId = array()){
        $connection = Bitrix\Main\Application::getConnection();
        if(count($arrCompanyId) != 2){
            return false;
        }else{
            sort($arrCompanyId);
            $MainCompanyID = trim($arrCompanyId[0]);
            $OldCompanyID = trim($arrCompanyId[1]);
        }
        $sql = 'SELECT IBLOCK_ELEMENT_ID FROM b_iblock_element_prop_s40 WHERE PROPERTY_183 = \''.$OldCompanyID.'\'';
        $recordset = $connection->query($sql);

        while ($record = $recordset->fetch())
        {
            $Dogovors[] =  $record['IBLOCK_ELEMENT_ID'];
        }
        if(empty($Dogovors[0])){
            return "Нет у источника договоов";
        }else{

            foreach ($Dogovors as $key=>$val){
                $sql = 'UPDATE b_iblock_element_prop_s40 SET PROPERTY_183 = \'' . $MainCompanyID . '\' WHERE IBLOCK_ELEMENT_ID = \'' . $val . '\' ';
                $connection->query($sql);
            }
            $rez = "Перепривязанно договоров - ".count($Dogovors);
            return $rez;
        }
    }
    //перепривязка платежей
    public static function mergIblockPay ($arrCompanyId = array()){
        $connection = Bitrix\Main\Application::getConnection();
        if(count($arrCompanyId) != 2){
            return false;
        }else{
            sort($arrCompanyId);
            $MainCompanyID = trim($arrCompanyId[0]);
            $MainCompanyID = $MainCompanyID.".0000";
            $OldCompanyID = trim($arrCompanyId[1]);
            $OldCompanyID = $OldCompanyID.".0000";
        }
        $sql = 'SELECT IBLOCK_ELEMENT_ID FROM b_iblock_element_prop_s86 WHERE PROPERTY_683 = \''.$OldCompanyID.'\'';
        $recordset = $connection->query($sql);

        while ($record = $recordset->fetch())
        {
            $Pay[] =  $record['IBLOCK_ELEMENT_ID'];
        }
        if(empty($Pay[0])){
            return "Нет у источника платежей";
        }else{

            foreach ($Pay as $key=>$val){
                $sql = 'UPDATE b_iblock_element_prop_s86 SET PROPERTY_683 = \'' . $MainCompanyID . '\' WHERE IBLOCK_ELEMENT_ID = \'' . $val . '\' ';
                $connection->query($sql);
            }

            $rez = "Перепривязанно платежей - ".count($Pay);
            return $rez;
        }
    }
    //перепривязка обращений
    public static function mergIblockTiket ($arrCompanyId = array()){
        $connection = Bitrix\Main\Application::getConnection();
        if(count($arrCompanyId) != 2){
            return false;
        }else{
            sort($arrCompanyId);
            $MainCompanyID = trim($arrCompanyId[0]);
            $OldCompanyID = trim($arrCompanyId[1]);
        }
        //тикеты маркетинга
        $sql = 'SELECT IBLOCK_ELEMENT_ID FROM b_iblock_element_prop_s78 WHERE PROPERTY_615 = \''.$OldCompanyID.'\'';
        $recordset = $connection->query($sql);

        while ($record = $recordset->fetch())
        {
            $marketing[] =  $record['IBLOCK_ELEMENT_ID'];
        }
        if(!empty($marketing[0])) {
            foreach ($marketing as $key => $val) {
                $sql = 'UPDATE b_iblock_element_prop_s78 SET PROPERTY_615 = \'' . $MainCompanyID . '\' WHERE IBLOCK_ELEMENT_ID = \'' . $val . '\' ';
                $connection->query($sql);
            }
        }
        //тикеты продаж агент/партнер
        $sql = 'SELECT IBLOCK_ELEMENT_ID FROM b_iblock_element_prop_s90 WHERE PROPERTY_740 = \''.$OldCompanyID.'\'';
        $recordset = $connection->query($sql);

        while ($record = $recordset->fetch())
        {
            $partner[] =  $record['IBLOCK_ELEMENT_ID'];
        }
        if(!empty($partner[0])) {
            foreach ($partner as $key => $val) {
                $sql = 'UPDATE b_iblock_element_prop_s90 SET PROPERTY_740 = \'' . $MainCompanyID . '\' WHERE IBLOCK_ELEMENT_ID = \'' . $val . '\' ';
                $connection->query($sql);
            }
        }
        //тикеты продаж клиент
        $sql = 'SELECT IBLOCK_ELEMENT_ID FROM b_iblock_element_prop_s89 WHERE PROPERTY_710 = \''.$OldCompanyID.'\'';
        $recordset = $connection->query($sql);

        while ($record = $recordset->fetch())
        {
            $klient[] =  $record['IBLOCK_ELEMENT_ID'];
        }
        if(!empty($klient[0])) {
            foreach ($klient as $key => $val) {
                $sql = 'UPDATE b_iblock_element_prop_s89 SET PROPERTY_710 = \'' . $MainCompanyID . '\' WHERE IBLOCK_ELEMENT_ID = \'' . $val . '\' ';
                $connection->query($sql);
            }
        }
        //тикеты обращение на вторую линию
        $sql = 'SELECT IBLOCK_ELEMENT_ID FROM b_iblock_element_prop_s77 WHERE PROPERTY_496 = \''.$OldCompanyID.'\'';
        $recordset = $connection->query($sql);

        while ($record = $recordset->fetch())
        {
            $twoLine[] =  $record['IBLOCK_ELEMENT_ID'];
        }
        if(!empty($twoLine[0])) {
            foreach ($twoLine as $key => $val) {
                $sql = 'UPDATE b_iblock_element_prop_s77 SET PROPERTY_496 = \'' . $MainCompanyID . '\' WHERE IBLOCK_ELEMENT_ID = \'' . $val . '\' ';
                $connection->query($sql);
            }
        }
        $two = count($twoLine);
        $kli = count($klient);
        $mark = count($marketing);
        $part = count($partner);
        $rez = $two + $kli + $mark + $part;
        if($rez < 1){
            return "Нет обращений у источника";
        }else{
            return ("Перепривязанно обращений источника - ".count($rez));
        }
    }
    //перепривязка ККТ
    public static function mergIblockKkt($arrCompanyId = array(),$istohId = null){
        $connection = Bitrix\Main\Application::getConnection();
        if(count($arrCompanyId) != 2){
            return false;
        }else{
            sort($arrCompanyId);

            $MainCompanyID = trim($arrCompanyId[0]);
            $OldCompanyID = trim($arrCompanyId[1]);
        }

        $sql = 'SELECT IBLOCK_ELEMENT_ID FROM b_iblock_element_prop_s76 WHERE PROPERTY_487 = \''.$OldCompanyID.'\'';
        $recordset = $connection->query($sql);

        while ($record = $recordset->fetch())
        {
            $kkt[] =  $record['IBLOCK_ELEMENT_ID'];
        }
        //перепривязка ККТ обновление привязки к контрагенту и к сделки
        if(!empty($kkt[0]) and !empty($istohId)) {

            foreach ($kkt as $key => $val) {
                $sql = 'UPDATE b_iblock_element_prop_s76 SET PROPERTY_487 = \'' . $MainCompanyID . '\', PROPERTY_486 = \'' . $istohId . '\' WHERE IBLOCK_ELEMENT_ID = \'' . $val . '\' ';
                $connection->query($sql);
            }
            return true;
        }elseif(!empty($kkt[0]) and empty($istohId)) {
            //перепривязка ККТ обновление привязки ТОЛЬКО к контрагента, если нет основной сделки
            foreach ($kkt as $key => $val) {
                $sql = 'UPDATE b_iblock_element_prop_s76 SET PROPERTY_487 = \'' . $MainCompanyID . '\' WHERE IBLOCK_ELEMENT_ID = \'' . $val . '\' ';
                $connection->query($sql);
            }
            return true;
        }else{
            return "Нет у источника ККТ";
        }
    }
}