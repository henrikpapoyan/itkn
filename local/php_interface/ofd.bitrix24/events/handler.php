<?
\Bitrix\Main\EventManager::getInstance()->addEventHandler('rest', 'OnRestServiceBuildDescription', array('\RestBpmBitrix', 'OnRestServiceBuildDescription'));
\Bitrix\Main\EventManager::getInstance()->addEventHandler("iblock", "OnBeforeIBlockElementUpdate", array('\CIblockEvent', 'propElementUpdate'));
\Bitrix\Main\EventManager::getInstance()->addEventHandler("iblock", "OnAfterIBlockElementUpdate", array('\CIblockEvent', 'iblockElementUpdate'));
\Bitrix\Main\EventManager::getInstance()->addEventHandler("iblock", "OnAfterIBlockElementAdd", array('\CIblockEvent', 'iblockElementAdd'));
\Bitrix\Main\EventManager::getInstance()->addEventHandler("crm", "OnBeforeCrmCompanyUpdate", array('\CrmEvent', 'deleteContragent'));
\Bitrix\Main\EventManager::getInstance()->addEventHandler("crm", "OnAfterCrmCompanyUpdate", array('\CrmEvent', 'updateContragent'));
\Bitrix\Main\EventManager::getInstance()->addEventHandler("crm", "OnAfterCrmCompanyAdd", array('\CrmEvent', 'addContragent'));
\Bitrix\Main\EventManager::getInstance()->addEventHandler("crm", "OnAfterCrmContactAdd", array('\CrmEvent', 'addContactEvent'));
\Bitrix\Main\EventManager::getInstance()->addEventHandler("crm", "OnBeforeCrmContactAdd", array('\CrmEvent', 'addContactEventBefore'));
\Bitrix\Main\EventManager::getInstance()->addEventHandler("crm", "OnAfterCrmContactUpdate", array('\CrmEvent', 'updateContactEvent'));
\Bitrix\Main\EventManager::getInstance()->addEventHandler("crm", "OnBeforeCrmCompanyAdd", array('\CrmEvent', 'addContragentBefor'));
\Bitrix\Main\EventManager::getInstance()->addEventHandler("disk", "onAfterAddFile", array('\ParserXLSX', 'addFile'));