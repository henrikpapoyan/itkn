<?
//Подключаем серверные переменные, отключем статистику и подключаем ядро без header
    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
    define("NO_KEEP_STATISTIC", true);
    define("NOT_CHECK_PERMISSIONS", true);
    ini_set('memory_limit', '-1');
    @set_time_limit(0);
    @ignore_user_abort(true);
    define('CHK_EVENT', true);
    global $USER;
    global $DB;
    $USER->Authorize(372);