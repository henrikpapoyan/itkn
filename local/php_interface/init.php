<?
    include_once(".config.php"); //Конфигурационный файл. Объявление констант, зависимых от БД
    require_once("constants.php"); //Объявление констант, не зависящих от БД
    require_once("reqFiles.php"); //Подключение файлов (required)
    require_once("includedFiles.php"); //Подключение файлов (include)
    require_once("classLoader.php"); //Загрузчик классов
    require_once("system_functions.php"); //Функции для использования в любых модулях
    require_once("handlers.php"); //Регистрация обработчиков
    require_once("odd_functions.php"); //Функции, вызов которых не обнаружен (тестирование необходимости использования)