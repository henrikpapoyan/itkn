<?php
    /**
     * Return IBlock ID by CODE
     */
    function GetIBlockIDByCode($code, $type = '')
    {

        CModule::IncludeModule("iblock");
        $CIBlock = new CIBlock();

        $arrFilter = array(
            'ACTIVE'  => 'Y',
            'CODE'    => $code,
            'SITE_ID' => "s1",
        );

        if ($type) {
            $arrFilter['TYPE'] = $type;
        }

        $res        = $CIBlock->GetList(Array("SORT" => "ASC"), $arrFilter, false);
        $arIBlockId = "";

        if ($ar_res = $res->Fetch()) {
            $arIBlockId = $ar_res["ID"];
        }

        return $arIBlockId;
    }

    //Проверка на AJAX запрос
    function checkAjaxRequest()
    {
        return isset($_SERVER['HTTP_X_REQUESTED_WITH'])
               && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
    }

    //Возвращает ID свойства ИБ по его CODE
    function getPropIDbyCode($propCode = '', $iblockCode = '')
    {
        CModule::IncludeModule("iblock");
        $propID = 0;
        if ($propCode != '' && $iblockCode != '') {
            $ibProp     = new CIBlockProperty();
            $IBLOCK_ID  = GetIBlockIDByCode($iblockCode);
            $properties = $ibProp->GetList(
                array("sort" => "asc", "name" => "asc"),
                array("CODE" => $propCode, "IBLOCK_ID" => $IBLOCK_ID)
            );
            if ($prop_fields = $properties->GetNext()) {
                $propID = $prop_fields["ID"];
            }
        }

        return $propID;
    }

    // Преобразует первый символ строки в нижний регистр
    function mb_ucfirst($text)
    {
        return mb_strtolower(mb_substr($text, 0, 1)) . mb_substr($text, 1);
    }

    //Получение ID пользовательского свойства по XML_ID
    function GetUserFieldIDByXMLID($userPropXmlID, $returnParam = 'ID')
    {
        $userPropVal = 0;
        $obEnum      = new CUserTypeEntity();
        $rsEnum      = $obEnum->GetList(array(), array("XML_ID" => $userPropXmlID));
        if ($arF = $rsEnum->Fetch()) //AddMessage2Log('GetUserFieldIDByXMLID $arF', print_r($arF, true));
        {
            $userPropVal = $arF[$returnParam];
        }

        return $userPropVal;
    }

    function getIdElementListState($idXML)
    {
        if ($idXML) {
            $res = CUserFieldEnum::GetList(array(), array("XML_ID" => $idXML));
            if ($arElement = $res->GetNext()) {
                $idElement = $arElement[ID];
            }

            return $idElement;
        } else {
            return false;
        }
    }

    function getPropertyEnumIdByXmlId($iBlockID, $xmlId)
    {
        $property_enums = CIBlockPropertyEnum::GetList(Array(), Array("IBLOCK_ID" => $iBlockID, "XML_ID" => $xmlId));
        if ($enum_fields = $property_enums->GetNext()) {
            return $enum_fields["ID"];
        }

        return false;
    }

    //Array to CSV
    function array2csv($array, $arHeader = [], $path = '/log/temp.csv')
    {
        if ( ! is_array($array)) {
            return false;
        }

        $path = $_SERVER["DOCUMENT_ROOT"] . $path;

        if ( ! file_exists($path)) {
            $info = new SplFileInfo($path);
            mkdir($info->getPath(), 0755, true);
        }

        $fp = fopen($path, 'w');

        //Header
        if ( ! empty($arHeader)) {
            fputcsv($fp, $arHeader, ";");
        }

        //Body
        foreach ($array as $fields) {
            if ( ! is_array($fields)) {
                $fields = array($fields);
            }
            fputcsv($fp, $fields, ";");
        }

        fclose($fp);

        return true;
    }

    //Функция, которая по FIELD_NAME пользовательского свойства возвращает его ID
    function getUserPropIdByFieldName($fieldName)
    {
        $userPropID = false;
        $rsData     = CUserTypeEntity::GetList(array(), array("FIELD_NAME" => $fieldName));
        if ($arRes = $rsData->Fetch()) {
            $userPropID = $arRes["ID"];
        }

        return $userPropID;
    }

    //Функция, которая по возвращает информацию о пользовательском свойстве
    function getUserPropArray($filterName, $filterVal)
    {
        $arUserProp = array();
        $rsData     = CUserTypeEntity::GetList(array(), array($filterName => $filterVal));
        if ($arRes = $rsData->Fetch()) {
            $arUserProp = $arRes;
        }

        return $arUserProp;
    }

//Возвращает ID компании по ИНН
    function getCompanyIDbyINN($inn)
    {
        $requisite  = new \Bitrix\Crm\EntityRequisite();
        $fieldsInfo = $requisite->getFormFieldsInfo();
        $select     = array_keys($fieldsInfo);
        $arFilter   = array(
            '=ENTITY_TYPE_ID' => CCrmOwnerType::Company,
            '=RQ_INN'         => $inn
        );
        $res        = $requisite->getList(
            array(
                'order'  => array('SORT' => 'ASC', 'ID' => 'ASC'),
                'filter' => $arFilter,
                'select' => $select
            )
        );
        $RQ         = $res->fetch();

        return $RQ;
    }

//Функция, которая по XML_ID пользовательского свойства возвращает его FIELD_NAME (UF_CRM_*)
    function getUserPropFieldNameByXmlId($xmlCode)
    {
        $userFieldName = false;
        $rsData        = CUserTypeEntity::GetList(array(), array("XML_ID" => $xmlCode));
        if ($arRes = $rsData->Fetch()) {
            $userFieldName = $arRes["FIELD_NAME"];
        }

        return $userFieldName;
    }

/*
 * Функции из файла /php_interface/include/gk.php
*/
function gk_AddCompany($DATA)
{
    if(!CModule::IncludeModule("crm"))
        return;

    $loggergk_AddCompany = Logger::getLogger("gk_AddCompany", "company/gk_AddCompany.log");
    $loggergk_AddCompany->log("DATA=[".print_r($DATA, true)."]");
    //Поиск компании по ИНН/КПП
    $requisite = new \Bitrix\Crm\EntityRequisite();
    $fieldsInfo = $requisite->getFormFieldsInfo();
    $select = array_keys($fieldsInfo);

    $arFilter = array(
        '=RQ_INN' => $DATA['AGENT']['INN']
    );

    $rsRQ = $requisite->getList(
        array(
            'order' => array('SORT' => 'ASC', 'ID' => 'ASC'),
            'filter' => $arFilter,
            'select' => $select
        )
    );

    if ($arRQ = $rsRQ->fetch()) {
        $rsCompany = CCrmCompany::GetList(array(), array('ID' => $arRQ['ENTITY_ID']));
        $arCompany = $rsCompany->Fetch();
    }
    else
    {
        $arRQ = array();
    }

    $CCrmCompany = new CCrmCompany(false);
    if ($arCompany) {
        $id = $arCompany['ID'];
        $arFields = array('UF_LINK' => $DATA['AGENT']['ID'], "UF_FULL_NAME" => $DATA['AGENT']["OFICIAL_NAME"]);
        $CCrmCompany->Update($id, $arFields);
    }
    else
    {
        $arFields = array(
            'TITLE' => $DATA['AGENT']['AGENT_NAME'],
            'UF_FULL_NAME' => $DATA['AGENT']['OFICIAL_NAME'],
            'UF_LINK' => $DATA['AGENT']['ID'],
            'UF_CRM_INN' => $DATA['AGENT']['INN'],
            'FM' => Array
            (
                'EMAIL' => Array
                (
                    'n1' => Array
                    (
                        'VALUE' => $DATA['AGENT']['CONTACT']['EMAIL'],
                        'VALUE_TYPE' => 'WORK'
                    )

                ),

                'PHONE' => Array
                (
                    'n1' => Array
                    (
                        'VALUE' => $DATA['AGENT']['CONTACT']['PHONE'],
                        'VALUE_TYPE' => 'WORK'
                    )
                )

            )
        );
        $org_forma = '';
        if (strlen($DATA['AGENT']['INN']) == 12)
        {
            $org_forma = 67;
            $arFields['UF_FULL_NAME'] = $DATA['AGENT']['FULL_NAME'];
        }
        else
        {
            $org_forma = 63;
            $arFields['UF_CRM_KPP'] = $DATA['AGENT']['KPP'];
        }
        $arFields['UF_CRM_1486035185'] = $org_forma;
        $arFields["UF_CRM_COMPANY_GUID"] = RestBpmBitrix::generate_guid();

        $id = $CCrmCompany->Add(
            $arFields
        );
    }

    if ($id > 0)
    {
        if (count($arRQ) > 0 )
        {
            ;;
        }
        else
        {
            $info = array();
            $info['fields']['RQ_INN'] = $DATA['AGENT']['INN'];
            if (strlen($DATA['AGENT']['INN']) == 10)
            {
                $info['fields']['NAME'] = 'Организация';
                $info['fields']['PRESET_ID'] = 1;
                $info['fields']['RQ_COMPANY_NAME'] = $DATA['AGENT']['ITEM_NAME'];
                $info['fields']['RQ_COMPANY_FULL_NAME'] = $DATA['AGENT']['OFICIAL_NAME'];
                $info['fields']['RQ_KPP'] = $DATA['AGENT']['KPP'];
                $info['fields']['SORT'] = 500;

            }
            elseif (strlen($DATA['AGENT']['INN']) == 12)
            {
                $info['fields']['NAME'] = 'ИП';
                $info['fields']['PRESET_ID'] = 2;
                $arName =  explode(' ', $DATA['AGENT']['ITEM_NAME']);
                $info['fields']['RQ_FIRST_NAME'] = $arName[1];
                $info['fields']['RQ_SECOND_NAME'] = $arName[2];
                $info['fields']['RQ_LAST_NAME'] = $arName[0];
                $info['fields']['RQ_NAME'] = $DATA['AGENT']['ITEM_NAME'];
            }

            $info['fields']['ENTITY_TYPE_ID'] = 4;
            $info['fields']['ENTITY_ID'] = $id;

            $info['fields']['RQ_ADDR'][1] = array(
                'ADDRESS_1' => $DATA['AGENT']['ADDRESS']['PRESENTATION'],
                'ADDRESS_2' => '',
                'CITY' => '',
                'REGION' => '',
                'PROVINCE' => '',
                'POSTAL_CODE' => $DATA['AGENT']['ADDRESS']['POST_CODE'] ,
                'COUNTRY' =>  $DATA['AGENT']['ADDRESS']['COUNTRY'],
                'COUNTRY_CODE' => ''
            );

            $info['fields']['RQ_ADDR'][6] = array(
                'ADDRESS_1' => $DATA['AGENT']['REGISTRATION_ADDRESS']['PRESENTATION'],
                'ADDRESS_2' => '',
                'CITY' => '',
                'REGION' => '',
                'PROVINCE' => '',
                'POSTAL_CODE' => $DATA['AGENT']['REGISTRATION_ADDRESS']['POST_CODE'] ,
                'COUNTRY' =>  $DATA['AGENT']['REGISTRATION_ADDRESS']['COUNTRY'],
                'COUNTRY_CODE' => ''
            );



            $requisite_id = gk_AddRQ($info);

            $c = 0;
            foreach ($DATA['AGENT']["ACCOUNTS"] as $account) {
                $c++;

                $bankInfo = array(
                    'ENTITY_TYPE_ID' => 8,
                    'ENTITY_ID' => $requisite_id,
                    'COUNTRY_ID' => 1,
                    'NAME' => 'Банковские реквизиты '.$c,
                    'RQ_BANK_NAME' => $account['BANK']['NAME'],
                    'RQ_BANK_ADDR' => '',
                    'RQ_BIK' => $account['BANK']['BIK'],
                    'RQ_ACC_NUM' => $account['ACCOUNT_NUMBER'],
                    'RQ_ACC_CURRENCY' => '',
                    'RQ_COR_ACC_NUM' => $account['BANK']['CORR_NUMBER'],
                    'RQ_SWIFT' => '',
                    'COMMENTS' => ''
                );

                gk_AddBank($bankInfo);
            }
        }

        foreach ( $DATA['AGENT']['CONTACT']['CONTACT_PERSON'] as $Contact)
        {
            $Contact['COMPANY_ID'] = $id;
            gk_AddContact($Contact);
        }

    }
}

function gk_AddContact($DATA)
{
    $logger = Logger::getLogger("AddContact", "Contact/Add.log");
    $rsContact = gk_GetContact($DATA['ID']);
    $arName = explode(" ", $DATA['NAME']);
    $CCrmContact = new CCrmContact();
    if ($arContact = $rsContact->Fetch()){
        ;;
    }
    else
    {
        $logger->log("DATA=[".print_r($DATA, true));
        if ($DATA['NAME'] == '- -')
        {
            return;
        }
        $arFields = array(
            "LAST_NAME" => $arName[0],
            "NAME" => $arName[1],
            "SECOND_NAME" => $arName[2],
            "FULL_NAME" => $DATA['NAME'],
            'UF_LINK' => $DATA['ID'],
            'POST' => $DATA['POST'],
            'COMPANY_ID' => $DATA['COMPANY_ID'],
        );

        $CCrmContact->Add($arFields);
    }
}

function gk_AddRQ($info)
{
    $requisite = new \Bitrix\Crm\EntityRequisite();
    $requisiteFields = $info['fields'];
    $result = $requisite->add($requisiteFields);
    return $result->getId();
}

function gk_UpdateRQ($id, $info)
{
    $requisite = new \Bitrix\Crm\EntityRequisite();
    $requisiteFields = $info['fields'];
    $result = $requisite->update($id, $requisiteFields);
    return $result->getId();
}

function gk_AddBank($bankDetailFields)
{
    $bankDetail = new \Bitrix\Crm\EntityBankDetail();
    $bankDetail->add($bankDetailFields);
}

function gk_GetInterface($user_id = null)
{

    global $USER;
    if ($user_id == null)
    {
        $user_id = $USER->GetID();
    }
    if ($user_id > 0 )
    {
        $rsUser = $USER->GetByID($user_id);
        if ($arUser = $rsUser->Fetch())
        {

            if ($arUser['UF_INTERFACE'] > 0)
            {
                $rsInterface = CUserFieldEnum::GetList(array(), array(
                    "ID" => $arUser['UF_INTERFACE'],
                ));
                if ($arInterface = $rsInterface->Fetch())
                {
                    return $arInterface;
                }
            }
        }
    }
    return false;
}

function gk_GetContact($uf_link)
{
    $err_mess = "<br>Function: gk_GetContact<br>Line: ";
    global $DB;
    $strSql = "
        SELECT L.ID AS ID , BUF.UF_LINK
        FROM
          b_crm_contact L
        INNER JOIN b_uts_crm_contact BUF ON BUF.VALUE_ID = L.ID
        WHERE
          1=1 AND ( BUF.UF_LINK like '".$uf_link."')
        ";

    $res = $DB->Query($strSql, false, $err_mess.__LINE__);
    return $res;
}

function Pp($arMas){
    ?><pre><?print_r($arMas);?><pre><?
}

function gk_GetBpmIBlock()
{
    $arIBlock = array();
    $arCode = array(
        "MESSAGE_SECOND_LINE",
        "MESSAGE_MARKETING",
        "MESSAGE_SALES",
        "MESSAGE_SALES_PARTNER"
    );
    foreach ($arCode as $code) {
        $iblock_id = GetIBlockIDByCode($code);
        if ($iblock_id >0)
        {
            $arIBlock[] = $iblock_id;
        }
    }

    return $arIBlock;
}

function gk_GetBpmFastFilter()
{
    $arFastFilter = array
    (
        GetIBlockIDByCode("MESSAGE_SECOND_LINE") => 'Cопровождение',
        GetIBlockIDByCode("MESSAGE_SALES") => 'Продажи клиентам',
        GetIBlockIDByCode("MESSAGE_SALES_PARTNER") => 'Продажи агентам',
        1001 => 'Эксплуатация',
        GetIBlockIDByCode("MESSAGE_MARKETING") => 'Маркетинг',

    );
    return $arFastFilter;
}

function gk_GetBpmPropEnumSub()
{
    $arPropEnumSub = array("LOGIC" => "OR");
    $property_enums = CIBlockPropertyEnum::GetList(
        array(),
        array(
            'XML_ID' => '9EF9B517-88CA-4539-A228-FC1FD3AA0383'
        ));
    while($enum_fields = $property_enums->GetNext())
    {
        $arPropEnum[] = $enum_fields;

        $Prop = CIBlockProperty::GetByID($enum_fields['PROPERTY_ID']);
        $arProp = $Prop->Fetch();
        $arPropEnumSub[] = array("ID" => CIBlockElement::SubQuery("ID", array(
            "IBLOCK_ID" => $arProp['IBLOCK_ID'],
            'PROPERTY_'.$arProp['ID'] => $enum_fields['ID']
        )));
    }
    return array($arPropEnumSub);
}

function gk_GetBpmPropEnumIf()
{

    $arPropEnum = array("LOGIC" => "OR");
    $property_enums = CIBlockPropertyEnum::GetList(
        array(),
        array(
            'XML_ID' => '9EF9B517-88CA-4539-A228-FC1FD3AA0383'
        ));
    while($enum_fields = $property_enums->GetNext())
    {
        $arPropEnum[] = $enum_fields;
    }
    return $arPropEnum;
}

function gk_GetBpmPropEnum()
{

    $arPropEnum = array();
    $property_enums = CIBlockPropertyEnum::GetList(
        array(),
        array(
            'XML_ID' => '9EF9B517-88CA-4539-A228-FC1FD3AA0383'
        ));
    while($enum_fields = $property_enums->GetNext())
    {
        $arPropEnum[] = $enum_fields['ID'];
    }
    return $arPropEnum;
}

function gk_GetBpmPropCompanySub($company_id)
{

    $arPropCompanySub = array("LOGIC" => "OR");
    $property = CIBlockProperty::GetList(
        array(),
        array(
            'CODE' => 'KOMPANIYA_KLIENTA'
        ));
    while($arProp = $property->GetNext())
    {
        $arPropCompanySub[] = array("ID" => CIBlockElement::SubQuery("ID", array(
            "IBLOCK_ID" => $arProp['IBLOCK_ID'],
            'PROPERTY_'.$arProp['ID'] => $company_id
        )));
    }
    return array($arPropCompanySub);
}

function gk_GetOrderFastFilter()
{
    return array(
        '22' => 'ФН', 	//Форма запроса ФН на сайте 1-ofd	 	30	N
        '23' =>	'КА',     //Форма запроса сертификата на сайте 1-ofd	 	40	N
        '37' => 'ККТ',     // Форма покупки ККТ на сайте 1-ofd	 	430	N
        '41' => 'ККТ аренда',	//Форма аренды ККТ на сайте 1-ofd	 	450	N
        '38' => 'ДУ',      //Форма запроса заказа доп. услуг на сайте 1-ofd	 	440	N
        '44' => 'Биг дата',      //Форма запроса на анализ (Биг дата)
        '45' => 'ЭДО',      //Форма запроса на ЭДО
        '42' => 'Чат'	    //Форма запроса информации в чате
    );
}

function gk_GetOrderFilter()
{
    return array(
        '22',    //Форма запроса ФН на сайте 1-ofd	 	30	N
        '23',     //Форма запроса сертификата на сайте 1-ofd	 	40	N
        '37',     // Форма покупки ККТ на сайте 1-ofd	 	430	N
        '41',    //Форма аренды ККТ на сайте 1-ofd	 	450	N
        '38',      //Форма запроса заказа доп. услуг на сайте 1-ofd	 	440	N
        '44',     //Форма запроса на анализ (Биг дата)
        '45',     //Форма запроса на ЭДО
        '42'	    //Форма запроса информации в чате
    );
}

function gk_GetSectionVIP()
{
    $rsSection = CIBlockSection::GetList(array(), array('XML_ID' => 'fe864148-2dd7-456a-b9c9-37c6dae7be1d'));
    if($arSection = $rsSection->Fetch())
    {
        return $arSection['ID'];
    }
    return 0;
}

function gk_GetBpmPropSysName($code)
{
    $arPropSysName = array();
    $property = CIBlockProperty::GetList(
        array(),
        array(
            'CODE' => $code
        ));
    while($arProp = $property->GetNext())
    {
        $arPropSysName[] = 'PROPERTY_'.$arProp['ID'];
    }
    return $arPropSysName;
}

function gk_GetBpmPropSysName2($code, $iblock_id)
{
    $arPropSysName = array();
    $property = CIBlockProperty::GetList(
        array(),
        array(
            'CODE' => $code,
            'IBLOCK_ID' => $iblock_id,
        ));
    while($arProp = $property->GetNext())
    {
        $arPropSysName[] = 'PROPERTY_'.$arProp['ID'];
    }
    return $arPropSysName;
}

function gk_GetBpmGridColumns($iblock_id, $grid_columns, &$id2id)
{
    $newGridColumns = $grid_columns;
    $id2id = array();
    file_put_contents(__FILE__.".log", "iblock_id=[".print_r($iblock_id, true)."]\n", FILE_APPEND);
    foreach ($grid_columns as $key => $val) {
        if(substr($val, 0, 9) == 'PROPERTY_')
        {

            file_put_contents(__FILE__.".log", "val=[".print_r($val, true)."]\n", FILE_APPEND);
            $rsProp = CIBlockProperty::GetByID(substr($val, 9));
            $arProp = $rsProp->Fetch();
            $newProp = gk_GetBpmPropSysName2($arProp['CODE'], $iblock_id);
            $newGridColumns[$key] = $newProp[0];
            $id2id[$newProp[0]] = $val;
        }
    }

    return $newGridColumns;
}

/*
 * END Функции из файла /php_interface/include/gk.php
*/


/*
 * START Функции из файла local/php_interface/crm/functions/function.php END
*/

//расширение функционала карточки сделки
function dealFormus ($arResult = array()){
    $logger = Logger::getLogger('odd_func_log');
    $logger->log("Вызов функции dealFormus() от ".date('d.m.Y H:i:s'));
    $ELEMENT_ID = $arResult['ELEMENT_ID'];
    $contact = $arResult['CONTACT_ID'];
    $contragent = $arResult['COMPANY_ID'];
    $leadDefolt = $arResult['LEAD_ID'];
    //функционал получения id АВР и УПД
    if(!empty($contragent)){

    }

    $inn = GetUserField('CRM_DEAL', $ELEMENT_ID, 'UF_CRM_598B0A4A6959D');

    if (strlen($inn) == 10 and empty($contragent)) {
        $kpp        = GetUserField('CRM_DEAL', $ELEMENT_ID, 'UF_CRM_59F09A82571EE');
        $contragent = idForInnKpp($inn, $kpp);
    } elseif (strlen($inn) > 10 and empty($contragent)) {
        $contragent = idForInnKpp($inn);
    }


    //запрос на определения лид или заказ
    if ( ! empty($leadDefolt)) {
        //выполняем индификацию принадлежности к лиду или заказу
        $lead = '';
        if ($lead) {
            //лид
            SetUserField('CRM_DEAL', $ELEMENT_ID, 'UF_CRM_1515574232', $leadDefolt);
        } else {
            //заказ
            SetUserField('CRM_DEAL', $ELEMENT_ID, 'UF_CRM_1515683270', $leadDefolt);
        }
    }
}

function gk_GetRQ($id)
{

    $requisite = new \Bitrix\Crm\EntityRequisite();

    $fieldsInfo = $requisite->getFormFieldsInfo();

    $select   = array_keys($fieldsInfo);
    $arFilter = array(
        '=ENTITY_TYPE_ID' => 4,//$entityTypeId,
        '=ENTITY_ID'      => $id
    );
    $res      = $requisite->getList(
        array(
            'order'  => array('SORT' => 'ASC', 'ID' => 'ASC'),
            'filter' => $arFilter,
            'select' => $select
        )
    );
    $RQ       = array();
    while ($row = $res->fetch()) {
//    print_r($row);
        $bankDetail  = gk_GetBankDetail($row['ID']);
        $row['BANK'] = $bankDetail;
        $row['ADDR'] = \Bitrix\Crm\EntityRequisite::getAddresses($row['ID']);
        $RQ[]        = $row;
    }

    return $RQ;
}

function gk_GetBankDetail($requisite_id)
{
    $bankDetail = new \Bitrix\Crm\EntityBankDetail();
    $select     = array_merge(
        array('ID', 'ENTITY_TYPE_ID', 'ENTITY_ID', 'COUNTRY_ID', 'NAME'),
        $bankDetail->getRqFields(),
        array('COMMENTS')
    );
    $res        = $bankDetail->getList(
        array(
            'order'  => array('SORT', 'ID'),
            'filter' => array(
                '=ENTITY_TYPE_ID' => 8,//\CCrmOwnerType::Requisite,
                '=ENTITY_ID'      => $requisite_id
            ),
            'select' => $select
        )
    );
    $bank       = array();
    while ($row = $res->fetch()) {
        $bank[] = $row;
    }


    return $bank;
}

// Добавление взаимосвязи Лид - Компания
// $companyTypeCode = 'parent', материнский
// $companyTypeCode = 'child', дочерний
function addRelationship($INN, $companyId, $companyTypeCode = 'parent')
{
    if (empty($INN) || empty($companyId)) {
        return false;
    }

    CModule::IncludeModule('crm');
    $CCRMLead = new CCrmLead();
    global $USER;

    $resLead = $CCRMLead->GetList(array('ID' => 'desc'), array("UF_CRM_1499414186" => $INN));
    if ($resLead->SelectedRowsCount() > 0) {
        CModule::IncludeModule('iblock');
        $oElement = new CIBlockElement();

        $IBLOCK_ID              = GetIBlockIDByCode('lead_interrelationships');
        $IBLOCK_ID_COMPANY_TYPE = GetIBlockIDByCode('company_link_type');

        //Получение ID типа взаимосвязи
        $res = $oElement->GetList(array(),
            array("IBLOCK_ID" => $IBLOCK_ID_COMPANY_TYPE, "CODE" => $companyTypeCode), false, false, array('ID'));
        if ($arCompanyType = $res->Fetch()) {
            $idCompanyType = $arCompanyType['ID'];
        } else {
            return false;
        }
    }
    while ($arLead = $resLead->Fetch()) {
        $idLead = $arLead['ID'];

        //Проверка на дубликаты Взаимосвязи
        $resIblockInLead = $oElement->GetList(
            array(),
            array("IBLOCK_ID" => $IBLOCK_ID, "PROPERTY_LEAD_LINK" => $idLead),
            false,
            false,
            array(
                "ID",
                "IBLOCK_ID",
                "NAME",
                "PROPERTY_LEAD_LINK",
                "PROPERTY_COMPANY_LINK",
                "PROPERTY_COMPANY_LINK_TYPE"
            )
        );

        if ($resIblockInLead->SelectedRowsCount() == 0) {
            $PROP               = array(
                "LEAD_LINK"         => $arLead["ID"],
                "COMPANY_LINK"      => $companyId,
                "COMPANY_LINK_TYPE" => $idCompanyType,
            );
            $arLoadProductArray = Array(
                "MODIFIED_BY"       => $USER->GetID(), // элемент изменен текущим пользователем
                "IBLOCK_SECTION_ID" => false,          // элемент лежит в корне раздела
                "IBLOCK_ID"         => $IBLOCK_ID,
                "PROPERTY_VALUES"   => $PROP,
                "NAME"              => "Взаимосвязь для лида " . $arLead["ID"],
                "ACTIVE"            => "Y",
            );

            if ($PRODUCT_ID = $oElement->Add($arLoadProductArray)) {
                ;;
            } else {
                ;;
            }
        } else {
            $arInterrelationships = $resIblockInLead->GetNext();
            $arCurCompanys        = $arInterrelationships["PROPERTY_COMPANY_LINK_VALUE"];
            $arCurLinkTypes       = $arInterrelationships["PROPERTY_COMPANY_LINK_TYPE_VALUE"];

            if ( ! in_array($companyId, $arCurCompanys)) {
                $arCurCompanys[]  = $companyId;
                $arCurLinkTypes[] = $idCompanyType;
                CIBlockElement::SetPropertyValuesEx(
                    $arInterrelationships["ID"],
                    $arInterrelationships["IBLOCK_ID"],
                    array(
                        "COMPANY_LINK"      => $arCurCompanys,
                        "COMPANY_LINK_TYPE" => $arCurLinkTypes,
                    )
                );
            }
        }
    }

    return false;
}

function GetKontragentInfo($inn)
{
    $propertyTypeID = 'ITIN';
    $propertyValue  = $inn;
    $countryID      = 1;

    $result = \Bitrix\Crm\Integration\ClientResolver::resolve(
        $propertyTypeID,
        $propertyValue,
        $countryID
    );
    if (count($result) == 0) {
        $result   = array();
        $dataJSON = file_get_contents('https://xn--c1aubj.xn--80asehdb/%D0%B8%D0%BD%D1%82%D0%B5%D0%B3%D1%80%D0%B0%D1%86%D0%B8%D1%8F/%D0%BA%D0%BE%D0%BC%D0%BF%D0%B0%D0%BD%D0%B8%D0%B8/?%D0%B8%D0%BD%D0%BD=' .
            $inn);
        $data     = json_decode($dataJSON, true);

        if (strlen($inn) == 10 && count($data) > 0) {
            $arFields = array(
                'RQ_COMPANY_NAME'      => 'shortName',
                'RQ_COMPANY_FULL_NAME' => 'name',
                'RQ_INN'               => 'inn',
                'RQ_KPP'               => 'kpp',
                'RQ_OGRN'              => 'ogrn'
            );
            foreach ($data as $k => $v) {
                $rq = array();
                foreach ($arFields as $kf => $vf) {
                    $rq[$kf] = $v[$vf];
                }
                $result[] = array('fields' => $rq);
            }

        }
    }

    return $result;
}

    function getBitrixUserManager($user_id = false)
    {
        if ( ! $user_id) {
            $user_id = $GLOBALS["USER"]->GetID();
        }
        $arAllManagerIDs = getAllHeadDepartmentsIDs();

        if (in_array($user_id, $arAllManagerIDs)) {
            return [];
        }

        $arManagersResult = array_keys(CIntranetUtils::GetDepartmentManager(CIntranetUtils::GetUserDepartments($user_id),
            $user_id, true));

        $arExceptedManagers = [143]; //Исключаем этих пользователей из автоматической простановки в наблюдатели
        $arManagersResult   = array_diff($arManagersResult, $arExceptedManagers);

        return $arManagersResult;
    }

    function getAllHeadDepartmentsIDs()
    {
        $dbSection = CIBlockSection::GetList(
            Array("SORT" => "ASC"),
            Array("IBLOCK_ID" => 5),
            false,
            array("ID", "NAME", "UF_HEAD")
        );

        $arAllManagerIDs = [];
        while ($arSection = $dbSection->Fetch()) {
            if ( ! empty($arSection["UF_HEAD"])) {
                $arAllManagerIDs[] = $arSection["UF_HEAD"];
            }
        }

        return $arAllManagerIDs;
    }


/*
* Функция принимает на вход:
     * $IBlockID - ID ИБ
     * $propCode - CODE свойства типа список
     * $neededParam - параметр нужного элемента списка (ID, XML_ID, VALUE, PROPERTY_ID)
     * $neededVal - значение для параметра $neededParam
 * Возвращает массив с данными найденного элемента списка
*/
function getIblockPropEnum($IBlockID, $propCode, $neededParam, $neededVal){
    $arNeededEnum = false;
    if($IBlockID > 0 && !empty($propCode) && !empty($neededParam) && !empty($neededVal)){
        $property_enums = CIBlockPropertyEnum::GetList(Array("SORT"=>"ASC"), Array("IBLOCK_ID"=>$IBlockID, "CODE"=>$propCode));
        while($enum_fields = $property_enums->GetNext())
        {
            if($neededVal == $enum_fields[$neededParam]){
                $arNeededEnum = $enum_fields;
            }
        }
    }
    return $arNeededEnum;
}

/*Функция возвращает ID свойства ИБ по его символьному коду*/
function getIblockPropID($IBlockID, $propCode){
    $propID = 0;
    if($IBlockID > 0 && !empty($propCode)){
        $property_enums = CIBlockPropertyEnum::GetList(Array("SORT"=>"ASC"), Array("IBLOCK_ID"=>$IBlockID, "CODE"=>$propCode));
        while($enum_fields = $property_enums->GetNext())
        {
            $propID = $enum_fields["PROPERTY_ID"];
            break;
        }
    }
    return $propID;
}
/*
 * END Функции из файла local/php_interface/crm/functions/function.php END
*/