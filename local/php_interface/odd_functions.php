<?php
    $oddFuncLogger = Logger::getLogger('odd_func_log');


    /*
    if (!function_exists('dump')) {
        function dump($ar) {
            global $USER;
            if ($USER->IsAdmin()) {
                echo '<pre>';
                print_r($ar);
                echo '</pre>';
            } else return false;
        }
    }*/

    //Вызов функции обнаружен только в закомментированной части кода
    function vdump($a, $login)
    {
        //Логирование вызова функции
        global $oddFuncLogger;
        $oddFuncLogger->log("Called odd function 'vdump': " . date("Y-m-d H:i:s"));

        global $USER;
        $bt         = debug_backtrace();
        $bt         = $bt[0];
        $context    = \Bitrix\Main\Application::getInstance()->getContext();
        $server     = $context->getServer();
        $dRoot      = $server->getDocumentRoot();
        $dRoot      = str_replace("/", "\\", $dRoot);
        $bt["file"] = str_replace($dRoot, "", $bt["file"]);
        $dRoot      = str_replace("\\", "/", $dRoot);
        $bt["file"] = str_replace($dRoot, "", $bt["file"]);
        $login      = ( ! empty($login) && strlen($login) > 0) ? $login : 'ivan_ivan';
        if ($USER->isAdmin()) {
            if ( ! empty($login) && $USER->GetLogin() == $login) {
                echo '<div style="padding:3px 5px; background:#99CCFF; font-weight:bold;">File: ' . $bt["file"] . ' [' .
                     $bt["line"] . ']</div>';
                echo '<pre style="text-align: left;">';
                print_r($a);
                echo '</pre>';
            }
        }
    }

    //Вызов функции не обнаружен
    function NextStageEvent($deal_id)
    {
        //Логирование вызова функции
        global $oddFuncLogger;
        $oddFuncLogger->log("Called odd function 'NextStageEvent': " . date("Y-m-d H:i:s"));

        $rsDeal    = CCrmDeal::GetList(array(), array('ID' => $deal_id));
        $arDeal    = $rsDeal->Fetch();
        $stage_id  = $arDeal['STAGE_ID'];
        $event_id  = $arDeal['UF_EVENT'];
        $IBLOCK_ID = 31;
        $arOrder   = Array("SORT" => "ASC");
        $arFilter  = Array('IBLOCK_ID' => $IBLOCK_ID);
        $rsStage   = CIBlockSection::GetList($arOrder, $arFilter);
        $arStages  = array();
        $arEvents  = array();
        while ($arStage = $rsStage->Fetch()) {
            $StageID                  = substr($arStage['CODE'], 8);
            $arStages[$arStage['ID']] = $StageID;
            $arEvents[$StageID]       = array();
        }
        $arOrder  = Array("SORT" => "ASC");
        $arFilter = Array('IBLOCK_ID' => $IBLOCK_ID);
        $rsEvent  = CIBlockElement::GetList($arOrder, $arFilter);
        while ($arEvent = $rsEvent->Fetch()) {
            $arEvents[$arStages[$arEvent['IBLOCK_SECTION_ID']]][] = $arEvent['ID'];
        }
        $nextEvent = false;
        $newEvent  = 0;
        foreach ($arEvents[$stage_id] as $key => $val) {
            if ($nextEvent) {
                $newEvent = $val;
                break;
            }
            if ($val == $event_id) {
                $nextEvent = true;
            }
        }
        $endEvent  = end($arEvents[$stage_id]);
        $nextStage = false;
        $newStage  = "END";
        if ($newEvent == 0) {
            foreach ($arEvents as $key => $val) {
                if ($nextStage) {
                    $newStage = $key;
                    reset($arEvents[$newStage]);
                    $newEvent = current($arEvents[$newStage]);
                    break;
                }
                if ($key == $stage_id) {
                    $nextStage = true;
                }

            }
        } else {
            $newStage = $stage_id;
        }

        return array('stage' => $newStage, 'event' => $newEvent);
    }

    //Вызов функции не обнаружен
    function gk_FIO($FIO)
    {
        //Логирование вызова функции
        global $oddFuncLogger;
        $oddFuncLogger->log("Called odd function 'gk_FIO': " . date("Y-m-d H:i:s"));

        CModule::IncludeModule('htmls.docdesigner');
        $CDocDes = new CDocDesignerProcessing();

        $FIO = strtolower($FIO);
//$FIO = ucwords($FIO);
        $FIO = mb_convert_case($FIO, MB_CASE_TITLE, "UTF-8");
        echo $FIO . "<br>";
        $arFFIO                              = $CDocDes->getDeclination($FIO);
        $arFIO                               = explode(" ", $arFFIO[0]);
        $arFFIO['COMPANY_PERSON_SHORT_NAME'] = substr($arFIO[1], 0, 1) . '.' . substr($arFIO[2], 0, 1) . '.' .
                                               $arFIO[0];

        return $arFFIO;
    }

    //Вызов функции не обнаружен
    function gk_AddSLA(
        $company_id,
        $type,
        $time_work,
        $available,
        $time_wait,
        $time_process,
        $month_percent,
        $day_percent,
        $complaints
    ) {

        //Логирование вызова функции
        global $oddFuncLogger;
        $oddFuncLogger->log("Called odd function 'gk_AddSLA': " . date("Y-m-d H:i:s"));

//	time_work Время выполнения 1. Время выполнения работы (три группы критичности АБВ), часов
//	available Доступность 2. Доступность всех каналов обращений (в режиме окна обслуживания), %
//	time_wait Время ожидания 3. Обеспечение максимально короткого времени ожидания вызова (% вызовов принятых в первые 30 сек ожидания ответа), не менее%
//	time_process Время обрабоки 4. Обеспечение максимально короткого времени обработки заявки (% заявок, принятых, обработанных и направленных в работу в течении 30 мин с момента получения заявки), не менее%
//	Среднемесячный процент 5. Среднемесячный % потерянных вызовов (от объема включенного в абонементную плату), не более %
//	Среднесуточный процент 6. Треднесуточный % пропущенных заявок, писем (от объема включенного в абонементную плату), %
//	Объем жалоб 7. Обеспечение вежливого и внимательного обслуживания (Среднемесячный объем обоснованных жалоб ), не более %

        if ($company_id > 0) {
            $IBLOCK_ID      = GetIBlockIDByCode('registry_SLA');
            $arSelect       = Array("ID");
            $arFilter       = array(
                "IBLOCK_ID"           => $IBLOCK_ID,
                "PROPERTY_KONTRAGENT" => $company_id,
            );
            $resSLA         = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
            $CIBlockElement = new CIBlockElement();
            $PROP           = array(
                "VREMYA_VYPOLNENIYA"        => $time_work,
                "DOSTUPNOST"                => $available,
                "VREMYA_OZHIDANIYA"         => $time_wait,
                "VREMYA_OBRABOKI"           => $time_process,
                "SREDNEMESYACHNYY_PROTSENT" => $month_percent,
                "SREDNESUTOCHNYY_PROTSENT"  => $day_percent,
                "OBEM_ZHALOB"               => $complaints,
                "KONTRAGENT"                => $company_id
            );
            $arFields       = array(
                "IBLOCK_ID"       => $IBLOCK_ID,
                "NAME"            => $type,
                "PROPERTY_VALUES" => $PROP,
            );
            if ($arSLA = $resSLA->Fetch()) {
                $sla_id = $arSLA["ID"];
                $ret    = $CIBlockElement->Update($sla_id, $arFields);

            } else {

                $id = $CIBlockElement->Add($arFields);
            }
        }
    }

    //Вызов функции не обнаружен
    function gk_GetLastURL($url, $doc, $msg)
    {
        //Логирование вызова функции
        global $oddFuncLogger;
        $oddFuncLogger->log("Called odd function 'gk_GetLastURL': " . date("Y-m-d H:i:s"));

        $arUrl = explode('|', trim($url));
        file_put_contents(__FILE__ . ".log", "arUrl=[" . print_r($arUrl, true) . "]\n", FILE_APPEND);
        file_put_contents(__FILE__ . ".log", "doc=[" . print_r($doc, true) . "]\n", FILE_APPEND);
        file_put_contents(__FILE__ . ".log", "msg=[" . print_r($msg, true) . "]\n", FILE_APPEND);
        $lasturl = "";
        foreach ($arUrl as $url) {
            if ( ! empty($url)) {
                $lasturl = $url;
            }
        }
        $lasturl = str_replace("]" . $doc . "[", "]" . $msg . "[", $lasturl);
        file_put_contents(__FILE__ . ".log", "lasturl=[" . print_r($lasturl, true) . "]\n", FILE_APPEND);

        return $lasturl;
    }

    //Вызов функции не обнаружен
    function gk_UpdateFile($doc_id, $file_id)
    {

        //Логирование вызова функции
        global $oddFuncLogger;
        $oddFuncLogger->log("Called odd function 'gk_UpdateFile': " . date("Y-m-d H:i:s"));

        CModule::IncludeModule("iblock");
        $IBLOCK_ID = GetIBlockIDByCode('registry_contracts');
        $rsFile    = CIBlockElement::GetProperty(
            $IBLOCK_ID,
            $doc_id,
            'sort',
            'asc',
            array('CODE' => 'DOGOVOR')
        );
        while ($arFile = $rsFile->Fetch()) {
            $del = CIBlockElement::SetPropertyValueCode($doc_id, 'DOGOVOR', array(
                    $arFile['PROPERTY_VALUE_ID'] => array('del' => 'Y', 'tmp_name' => '')
                )
            );
            CFile::Delete($arFile['VALUE']);
        }

        $arFile = CFile::MakeFileArray($file_id);
        $ret    = CIBlockElement::SetPropertyValueCode($doc_id, "DOGOVOR", $arFile);
    }

    /*function TaskUpdate(){
    $arEventFields = array(
        'FIELDS_BEFORE' => array('ID' => $taskId), // было идентификатор изменяемой задачи
        'FIELDS_AFTER' => array('ID' => $taskId), // стало идентификатор изменяемой задачи
        'IS_ACCESSIBLE_BEFORE' => 'undefined', // проверка доступности задачи на чтение не производилась
        'IS_ACCESSIBLE_AFTER' => 'undefined' // проверка доступности задачи на чтение не производилась
    );

    return "TaskUpdate();";
    }*/

    //Вызов функции не обнаружен
    function getListDeal($arFilter)
    {

        //Логирование вызова функции
        global $oddFuncLogger;
        $oddFuncLogger->log("Called odd function 'getListDeal': " . date("Y-m-d H:i:s"));

        if ( ! CModule::IncludeModule('crm')) {
            return false;
        }

        $arDeal = [];
        $dbRes  = CCrmDeal::GetList(array(), $arFilter);
        while ($arRes = $dbRes->Fetch()) {
            $beginDate          = explode(" ", $arRes['BEGINDATE']);
            $arRes["FULL_NAME"] = $arRes['TITLE'] . " от " . $beginDate[0];
            array_push($arDeal, $arRes);
        }

        return $arDeal;
    }

    //Вызов функции не обнаружен
    function getDealById($id)
    {
        //Логирование вызова функции
        global $oddFuncLogger;
        $oddFuncLogger->log("Called odd function 'getDealById': " . date("Y-m-d H:i:s"));

        if ( ! CModule::IncludeModule('crm')) {
            return false;
        }

        $deal = CCrmDeal::GetByID($id);
        if (empty($deal)) {
            return false;
        }

        $deal["FULL_NAME"] = $deal['TITLE'] . " от " . $deal['BEGINDATE'];

        return $deal;
    }

    //Вызов функции не обнаружен
    function getListRegistryContractsDogovor($arFilter)
    {
        //Логирование вызова функции
        global $oddFuncLogger;
        $oddFuncLogger->log("Called odd function 'getListRegistryContractsDogovor': " . date("Y-m-d H:i:s"));

        if ( ! CModule::IncludeModule('iblock')) {
            return false;
        }

        $arResult = [];

        $arSelect = Array(
            "ID",
            "IBLOCK_ID",
            "NAME",
            "PROPERTY_TIP_KONTRAGENTA",
            "PROPERTY_KONTRAGENT",
            "PROPERTY__DOGOVORA",
            "PROPERTY_DATA_DOGOVORA"
        );
        $res      = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
        while ($ar_fields = $res->GetNext()) {
            $ar_fields['FULL_NAME'] = $ar_fields['NAME'] . " №" . $ar_fields['PROPERTY__DOGOVORA_VALUE'] . " от " .
                                      getDateCustomFormat($ar_fields['PROPERTY_DATA_DOGOVORA_VALUE']);
            if (stristr($ar_fields['NAME'], "договор")) {
                $arResult[] = $ar_fields;
            }
        }

        return $arResult;
    }

    //Вызов функции не обнаружен
    function getListRegistryContractsDS($arFilter)
    {
        //Логирование вызова функции
        global $oddFuncLogger;
        $oddFuncLogger->log("Called odd function 'getListRegistryContractsDS': " . date("Y-m-d H:i:s"));

        if ( ! CModule::IncludeModule('iblock')) {
            return false;
        }

        $arResult = [];

        $IBLOCK_ID             = GetIBlockIDByCode('registry_contracts');
        $arFilter['IBLOCK_ID'] = $IBLOCK_ID;

        $arSelect = Array(
            "ID",
            "IBLOCK_ID",
            "NAME",
            "PROPERTY_TIP_KONTRAGENTA",
            "PROPERTY_KONTRAGENT",
            "PROPERTY__DOGOVORA",
            "PROPERTY_DATA_DOGOVORA"
        );
        $res      = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
        while ($ar_fields = $res->GetNext()) {
            $ar_fields['FULL_NAME'] = $ar_fields['NAME'] . " №" . $ar_fields['PROPERTY__DOGOVORA_VALUE'] . " от " .
                                      getDateCustomFormat($ar_fields['PROPERTY_DATA_DOGOVORA_VALUE']);
            if (stristr($ar_fields['NAME'], "Доп. соглашение")) {
                $arResult[] = $ar_fields;
            }
        }

        return $arResult;
    }

    //Вызов функции не обнаружен
    function getNumberRegistryContractsById($id)
    {
        //Логирование вызова функции
        global $oddFuncLogger;
        $oddFuncLogger->log("Called odd function 'getNumberRegistryContractsById': " . date("Y-m-d H:i:s"));

        if ( ! CModule::IncludeModule('iblock')) {
            return false;
        }

        $IBLOCK_ID = GetIBlockIDByCode('registry_contracts');

        $arSelect = Array("ID", "NAME", "PROPERTY__DOGOVORA");
        $arFilter = Array("IBLOCK_ID" => $IBLOCK_ID, "ID" => $id);
        $res      = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
        if ($ob = $res->GetNextElement()) {
            $arFields = $ob->GetFields();

            return $arFields['PROPERTY__DOGOVORA_VALUE'];
        }
    }

    //Вызов функции не обнаружен
    function getPriceCustomFormat($source)
    {
        //Логирование вызова функции
        global $oddFuncLogger;
        $oddFuncLogger->log("Called odd function 'getPriceCustomFormat': " . date("Y-m-d H:i:s"));

        CModule::IncludeModule("sale");

        //price
        $part1 = Number2Word_Rus($source);
        $part2 = trim(Number2Word_Rus($source, "N"));
        $part1 = str_replace($part2, $part2 . ")", $part1);
        $res   = (string)((int)$source) . " (" . $part1;

        //НДС
        $priceNDS = $source / 1.18 * 0.18;
        $priceNDS = round($priceNDS, 2);
        $part1    = Number2Word_Rus($priceNDS);
        $part2    = trim(Number2Word_Rus($priceNDS, "N"));
        $part1    = str_replace($part2, $part2 . ")", $part1);
        $resNDS   = (string)((int)$priceNDS) . " (" . $part1;

        $result = $res . ", в том числе НДС (18%) - " . $resNDS;

        return $result;
    }

    //Вызов функции не обнаружен
    function getDateCustomFormat($date)
    {
        //Логирование вызова функции
        global $oddFuncLogger;
        $oddFuncLogger->log("Called odd function 'getDateCustomFormat': " . date("Y-m-d H:i:s"));

        //14.06.2017
        //«14» июня 2017

        $monthName = array(
            "1"  => "января",
            "2"  => "февраля",
            "3"  => "марта",
            "4"  => "апреля",
            "5"  => "мая",
            "6"  => "июня",
            "7"  => "июля",
            "8"  => "августа",
            "9"  => "сентября",
            "10" => "октября",
            "11" => "ноября",
            "12" => "декабря"
        );

        $date  = explode(".", $date);
        $day   = $date['0'];
        $month = (string)((int)$date['1']);
        $month = $monthName[$month];
        $year  = $date['2'];

        return "«" . $day . "» " . $month . " " . $year;
    }