<?php
    include_once(__DIR__ . "/../../.config.php");

    define("NO_KEEP_STATISTIC", true);
    define("NOT_CHECK_PERMISSIONS", true);
    define('CHK_EVENT', true);
    ini_set('memory_limit', '-1');

    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

    require_once($_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/Classes/common/CComplianceCompany.php");

    $CComplianceCompany = new CComplianceCompany;

    global $USER;
    $USER->Authorize($CComplianceCompany::MODIFIER_BITRIX_USER_ID);


    $offset = 0;
    if (array_key_exists(1, $argv)) {
        $offset = $argv[1];
    }

    $iblockID = GetIBlockIDByCode('import_company');
    if ($offset === 0) {
        $CComplianceCompany->deleteAllIblockElements($iblockID);
        $CComplianceCompany->updateElementsCsv2Db();
    }

    if ($CComplianceCompany->getCurrentStatus() !== $CComplianceCompany::STATUS_UPDATE_ALL_ELEMENTS_VALUE &&
        $CComplianceCompany->getCurrentStatus() !== $CComplianceCompany::STATUS_UPDATE_ALL_ELEMENTS_IN_PROGRESS_VALUE &&
        $CComplianceCompany->getCurrentStatus() !== $CComplianceCompany::STATUS_FAST_UPDATE_ELEMENTS_VALUE &&
        $CComplianceCompany->getCurrentStatus() !== $CComplianceCompany::STATUS_FAST_UPDATE_ELEMENTS_IN_PROGRESS_VALUE) {
        die();
    }

    if($CComplianceCompany->getCurrentStatus() == $CComplianceCompany::STATUS_UPDATE_ALL_ELEMENTS_VALUE ||
        $CComplianceCompany->getCurrentStatus() == $CComplianceCompany::STATUS_UPDATE_ALL_ELEMENTS_IN_PROGRESS_VALUE)
    {
        $CComplianceCompany->setStatus($CComplianceCompany::STATUS_UPDATE_ALL_ELEMENTS_IN_PROGRESS_ID);
    }

    if($CComplianceCompany->getCurrentStatus() == $CComplianceCompany::STATUS_FAST_UPDATE_ELEMENTS_VALUE ||
        $CComplianceCompany->getCurrentStatus() == $CComplianceCompany::STATUS_FAST_UPDATE_ELEMENTS_IN_PROGRESS_VALUE)
    {
        $CComplianceCompany->setStatus($CComplianceCompany::STATUS_FAST_UPDATE_ELEMENTS_IN_PROGRESS_ID);
    }

    @set_time_limit(0);
    @ignore_user_abort(true);

    CModule::IncludeModule('crm');
    CModule::IncludeModule('iblock');

    $oLead          = new CCrmLead;
    $oContact       = new CCrmContact;
    $oDeal          = new CCrmDeal;
    $oCompany       = new CCrmCompany;
    $CCrmEvent      = new CCrmEvent();
    $oIBlockElement = new CIBlockElement();
    $oUser          = new CUser;

    $IBLOCK_ID_KKT                     = getIblockIDByCode('registry_kkt');
    $IBLOCK_ID_CSV                     = getIblockIDByCode($CComplianceCompany::COMPLIANCE_IBLOCK_CODE);
    $IBLOCK_ID_PAY                     = getIblockIDByCode('reestr_pay');
    $IBLOCK_ID_CREATED_COMPANY_CONTACT = getIblockIDByCode('id_created_company_contact');

//    die; //DELETE STRING

    $START_TIME = mktime();
    $n          = 0;
    while ($n < 1000) {
        //break;

        //Проверяем время. Если время работы превышает 55 сек - делаем break.
        $passedTime = (mktime() - $START_TIME);
        if ($passedTime > 50) {
            break;
        }

        $arResult = getOneElement($offset);
        echo "\n--- $arResult[ID] ---\n";
		
        if ($offset === 0 && $arResult === false) {

            //Устанавливаем начальный статус и меняем дату последней обработки
            $CComplianceCompany->sendNotify([372,381,146,365]);//[372,381,146,365] ID пользователей кого уведомлять
            $CComplianceCompany->setStatus($CComplianceCompany::STATUS_IMPORT_CSV_COMPLETE_ID);
            $CComplianceCompany->setDate($CComplianceCompany::UF_FULL_PROC_DATE, date("d.m.Y"));
            break;
        }
        if ($arResult === false) {
            break;
        }

        if (parserRow($arResult) === false) {
            $oIBlockElement->SetPropertyValueCode($arResult['ID'], "STATUS", 3); //Устанавливаем статус 3 - ошибка
        } else {
            $oIBlockElement->SetPropertyValueCode($arResult['ID'], "STATUS", 2); //Устанавливаем статус 2 - успех
        }
        $n++;

        //break;
    }
    $TIME = (mktime() - $START_TIME);

    echo "\n\nВремя обработки, сек: $TIME \n\n";


    function getOneElement($offset = 1)
    {
        global $oIBlockElement,
               $IBLOCK_ID_CSV,
               $CComplianceCompany;

        $arFilter = array(
            "IBLOCK_ID"       => $IBLOCK_ID_CSV,
            "PROPERTY_STATUS" => 0 // берем записи со статусом 0 - не обработан
        );

        if($CComplianceCompany->getCurrentStatus() == $CComplianceCompany::STATUS_FAST_UPDATE_ELEMENTS_IN_PROGRESS_VALUE)
        {
            $arFilter["!PROPERTY_FAST_UPDATE"] = false;
        }

        $res = $oIBlockElement->GetList(array(), $arFilter, false, array('iNumPage' => $offset, 'nPageSize' => 1));

        if ($res->SelectedRowsCount() <= $offset) {
            return false;
        }

        if ($ob = $res->GetNextElement()) {
            $arElementFields = $ob->GetFields();
            $oIBlockElement->SetPropertyValueCode($arElementFields['ID'], "STATUS",
                1); //устанавливаем статус 1 - в работе

            $arElement = $ob->GetProperties();

            $arResult                       = [];
            $arResult['ID']                 = $arElementFields['ID'];
            $arResult['ID_PROD']             = $arElement['ID_PROD']['VALUE'];
            $arResult['INN']                = $arElement['INN']['VALUE'];
            $arResult['KPP']                = $arElement['KPP']['VALUE'];
            $arResult['COMPANY_TYPE']       = $arElement['COMPANY_TYPE']['VALUE']; //TID, Тенант, тип контрагента
            $arResult['NAME_KKT']           = $arElement['NAME_KKT']['VALUE']; //EXTERNAL_CODE, имя ККТ
            $arResult['DATE_CREATE']        = $arElement['DATE_CREATE']['VALUE']; //created_at, Дата добавления ККТ
            $arResult['MODEL']              = $arElement['MODEL']['VALUE'];
            $arResult['NUM_KKT']            = $arElement['NUM_KKT']['VALUE']; //serial_number, Заводской номер ККТ
            $arResult['RNM_KKT']            = $arElement['RNM_KKT']['VALUE']; //fns_ecr_id, РНМ, RegID
            $arResult['NUM_FN']             = $arElement['NUM_FN']['VALUE']; //fiscal_drive_number, Номер ФН
            $arResult['ADDRESS_KKT']        = $arElement['ADDRESS_KKT']['VALUE']; //full_address, Адрес ККТ
            $arResult['NAME_TARIFF']        = $arElement['NAME_TARIFF']['VALUE']; //name_tariff , Тариф
            $arResult['COST_TARIFF']        = $arElement['COST_TARIFF']['VALUE']; //full_period_price, Стоимость тарифа
            $arResult['DATE_FROM']          = $arElement['DATE_FROM']['VALUE']; //from_date, Дата начала тарифа
            $arResult['DATE_TO']            = $arElement['DATE_TO']['VALUE']; //to_date, Дата завершения тарифа
            $arResult['CODE_AGENT']         = $arElement['CODE_AGENT']['VALUE']; //agent_code, Код агента
            $arResult['PROMO_KOD']          = $arElement['PROMO_KOD']['VALUE']; //promo_code, ID Промокода
            $arResult['ACCOUNT_ID']         = $arElement['ACCOUNT_ID']['VALUE']; //promo_code, ID Промокода
            $arResult['BALANCE']            = $arElement['BALANCE']['VALUE']; //promo_code, ID Промокода
            $arResult['BALANCE']            = str_replace(",", ".", $arResult['BALANCE']);
            $arResult['ASSIGNED_LAST_NAME'] = $arElement['ASSIGNED_LAST_NAME']['VALUE']; //promo_code, ID Промокода

            $arResult['EMAIL'] = $arElement['EMAIL']['VALUE']; //responsible_person_email, Email
            $arResult['PHONE'] = $arElement['PHONE']['VALUE']; //contact_phone, Телефон

            $arResult['EMAIL'] = (empty($arResult['EMAIL'])) ? "" : $arResult['EMAIL'];
            $arResult['EMAIL'] = ($arResult['EMAIL'] == "null") ? "" : $arResult['EMAIL'];
            if (check_email($arResult['EMAIL']) === false) {
                $arResult['EMAIL'] = "";
            }
            $arResult['PHONE'] = (empty($arResult['PHONE'])) ? "" : $arResult['PHONE'];
            $arResult['PHONE'] = ($arResult['PHONE'] == "null") ? "" : $arResult['PHONE'];

            return $arResult;
        }

        return false;
    }

    function parserRow($arParams)
    {

        //Тип сделки по умолчанию
        $typedeal = 2;

        global $oLead,
               $oContact,
               $oDeal,
               $oCompany,
               $oUser,
               $CCrmEvent,
               $oIBlockElement,
               $IBLOCK_ID_KKT,
               $IBLOCK_ID_PAY;

        $isKKTDefault      = false;
        $isLead            = false;
        $isReestPayDefault = false;

        $arParams['INN'] = str_replace("OBSOLETE", "", $arParams['INN']);


        switch ($arParams['COMPANY_TYPE']) {
            case "0": //Название тенанта (прод) из выгрузки -ТЕСТ
                echo "Не валидный тенант ТЕСТ";

                return false;
                break;
            case "5": //Название тенанта (прод) из выгрузки -Бизнес администрирование
                echo "Не валидный тенант Бизнес администрирование";

                return false;
                break;
            case "7": //Название тенанта (прод) из выгрузки -ЛК для тестирования Рапкат
                echo "Не валидный тенант ЛК для тестирования Рапкат";

                return false;
                break;
            case "11": //Название тенанта (прод) из выгрузки -Trashcan
                echo "Не валидный тенант Trashcan";

                return false;
                break;
            default:
                break;
        }

        //Проверка на валидность ИНН
        switch (strlen($arParams['INN'])) {
            case 10:
                break;
            case 12:
                break;
            default:
                echo "Не валидный ИНН: " . $arParams['INN'];

                return false;
                break;
        }

        //Проверка на наличие RegID
        if ( ! empty($arParams['RNM_KKT'])) {
            $isKKTDefault = true;
        }

        //Проверка на наличие данных Реестра платежей.
        if ( ! empty($arParams['ACCOUNT_ID'])) {
            $isReestPayDefault = true;
        }

        /**  Поиск ID пользователя по LAST_NAME */
        $userId = 372; //По умолчанию Bitrix
        if ( ! empty($arParams['ASSIGNED_LAST_NAME'])) {
            $rsUsers = $oUser->GetList(($by = "ID"), ($order = "desc"),
                array('LAST_NAME' => $arParams['ASSIGNED_LAST_NAME']), array());
            if ($arResult['USER'] = $rsUsers->Fetch()) {
                $userId = $arResult['USER']['ID'];
                echo "Нашли пользователя по фамилии $arParams[ASSIGNED_LAST_NAME], ID: $userId.\n";
            }
        }

        /** Поиск и обновление Лида */

        //Поиск Лида по ИНН
        $resMultiADD = $oLead->GetList(array('ID' => 'asc'), array(
            'TITLE'             => 'регистрация на сайте www.1ofd.ru',
            'UF_CRM_1499414186' => $arParams['INN']
        ));
        if ($arMultiADD = $resMultiADD->Fetch()) {
            echo "Найден лид по ИНН\n"; //DEL
            $isLead         = true;
            $arSelectedLead = array(
                "ID"          => $arMultiADD['ID'],
                "LAST_NAME"   => ($arMultiADD['LAST_NAME'] == "noname") ? "" : $arMultiADD['LAST_NAME'],
                "NAME"        => ($arMultiADD['NAME'] == "noname") ? "" : $arMultiADD['NAME'],
                "SECOND_NAME" => ($arMultiADD['SECOND_NAME'] == "noname") ? "" : $arMultiADD['SECOND_NAME'],
            );
        }

        // Поиск лида по Email, если не удалось найти лида по ИНН
        if (empty($isLead) && ! empty($arParams['EMAIL'])) {
            $idLead = leadDublicateCheck('регистрация на сайте www.1ofd.ru', $arParams['EMAIL']);
            if ( ! empty($idLead)) {
                echo "Найден лид по Email\n"; //DEL
                $isLead = true;

                $resMultiADD = $oLead->GetList(array('ID' => 'asc'), array('ID' => $idLead));
                if ($arMultiADD = $resMultiADD->Fetch()) {
                    $arSelectedLead = array(
                        "ID"          => $arMultiADD['ID'],
                        "LAST_NAME"   => ($arMultiADD['LAST_NAME'] == "noname") ? "" : $arMultiADD['LAST_NAME'],
                        "NAME"        => ($arMultiADD['NAME'] == "noname") ? "" : $arMultiADD['NAME'],
                        "SECOND_NAME" => ($arMultiADD['SECOND_NAME'] == "noname") ? "" : $arMultiADD['SECOND_NAME'],
                        "EMAIL"       => $arParams['EMAIL'],
                    );
                }

                //Обновляем Лид, добавляем ИНН.
                $arFields  = array("UF_CRM_1499414186" => $arParams['INN']);
                $resUpdate = $oLead->Update($arSelectedLead['ID'], $arFields);
                if ($resUpdate) {
                    echo "Лид успешно обновлен\n"; //DEL
                } else {
                    echo "Ошибка обновления лида: (" . $arSelectedLead['ID'] . "). ";
                }
            }
        }

        //Переводим лид в статус зарегестрирован в лк или Архивный
        if ( ! empty($isLead)) {
            $arFields = Array(
                "STATUS_ID" => "2", //зарегистрирован в ЛК
            );

            //Если есть ККТ, переводим в архивный статус
            if ($isKKTDefault) {
                $arFields['STATUS_ID'] = 1;
            }

            $oLead->Update($arSelectedLead['ID'], $arFields);
            echo "Лид переведён в статус зарегистрирован в ЛК ID (" . $arSelectedLead['ID'] . ")\n";
        }

        /** Поиск/создание компании по реквизитам ИНН и КПП */

        $requisite  = new \Bitrix\Crm\EntityRequisite();
        $fieldsInfo = $requisite->getFormFieldsInfo();
        $select     = array_keys($fieldsInfo);

        $arFilter = array(
            'RQ_INN'         => $arParams['INN'],
            'ENTITY_TYPE_ID' => 4
        );
        if ( ! empty($arParams['KPP'])) {
            $arFilter['RQ_KPP'] = $arParams['KPP'];
        }

        $res = $requisite->getList(
            array(
                'order'  => array('SORT' => 'ASC', 'ID' => 'ASC'),
                'filter' => $arFilter,
                'select' => $select
            )
        );

        //Компания найдена
		$arFields = [];
		
		//Проверка на существование компании, по ID найденному в реквизите
		$neededCompany = true;
		$companyID = 0;
        if ($row = $res->fetch()) {
            $companyID = $row['ENTITY_ID'];
			if(!$oCompany->GetByID($companyID)){
				echo "<pre>row[] = ".print_r($row, true)."</pre>";
				$requisite->delete($row['ID']);
				echo "Удалили реквизит с ID[$companyID]\n\r";
				$neededCompany = false;
			}
		} else {
			$neededCompany = false;
		}
		
        if ($neededCompany && $companyID > 0) {
            echo "Найдена существующая компания ID (" . $companyID . ")\n";

            $emailFmId = getFmId($companyID, 'EMAIL');
            $phoneFmId = getFmId($companyID, 'PHONE');

            //Обновляем тип компании в зависимости от поля Тенант
            // $arFields["COMPANY_TYPE"] = $arParams['COMPANY_TYPE']; //Тип компании(Тенант)
            $arFields["UF_ID_PROD"] = $arParams['ID_PROD']; //ID компании в BPM

            //Обновление ID ответственного пользователя
            if ($userId != 372) {
                //$arFields ["ASSIGNED_BY_ID"] = $userId;
            }

            if ( ! empty($arParams['EMAIL'])) {
                $arFields['FM']['EMAIL'] = Array(
                    "$emailFmId" => Array(
                        "VALUE"      => $arParams['EMAIL'],
                        "VALUE_TYPE" => "WORK",
                    ),
                );
            }

            if ( ! empty($arParams['PHONE'])) {
                $arFields['FM']['PHONE'] = Array(
                    "$phoneFmId" => Array(
                        "VALUE"      => $arParams['PHONE'],
                        "VALUE_TYPE" => "WORK",
                    ),
                );
            }


            //ID статусы контрагента
            /*
            * 264-действующий
            * 265-потенциальный
            * 266-архивный
            * */
            //Установка типа компании
            /*
             *Тест - 0
             *ГПХ - 1
             *Корпорации - 10
             *Trashcan - 11
             *1c - 13
             *Атол-онлайн  16
             *Старрус  17
             *Клиент+Партнер 18
             *Клиент+Покупатель 19
             *Клиент+Поставщик 20
             *Клиент+Агент   2
             *Клиенты, анонсировавшие расторжение   3
             * Бывшие клиенты  4
             * Бизнес Администрирование   5
             * Тензор    6
             * Рапкат    7
             * Без блокировки касс    8
             * Временные пользователи     9
             * Клиент    CUSTOMER
             * Подрядчик   OTHER
             * Партнер  PARTNER
             * Поставщик  RESELLER
             *
            */
            /*
                 *1- Интеграционный процесс
                 *2- Основная услуга ОФД
                 *COMPLEX- Комплексная продажа
                 *GOODS- 	Продажа товара
                 *SALE- Продажа
                 *SERVICE- 	Сервисное обслуживание
                 *SERVICES- 	Продажа услуги
                ///////////
                Тип сделки - Тип компании
                Продажа – Агент
                Комплексная продажа - Покупатель
                Продажа товара - Покупатель
                Продажа услуги - Покупатель
                Сервисное обслуживание - Поставщик
                Основная услуга ОФд - Клиент
                 *
                 * */
            switch ($arParams['COMPANY_TYPE']) {
                case "CUSTOMER"://1
                    echo "Статус компании CUSTOMER\n";
                    //Установка тенанта компании
                    SetUserField("CRM_COMPANY", $companyID, "UF_TENANT", "286");
                    echo "Компании поставили в поле тенант Клиент\n";

                    $resultcompany = CCrmCompany::GetByID($companyID);
                    $companytype   = $resultcompany['COMPANY_TYPE'];

                    if ($companytype == "CUSTOMER") {
                        //Установка статуса компании
                        $entity_id     = "CRM_COMPANY";
                        $uf_guid       = "UF_STATUS";
                        $statuscompany = GetUserField($entity_id, $companyID, $uf_guid);
                        if ($statuscompany != "264") {
                            SetUserField($entity_id, $companyID, $uf_guid, "264");
                            echo "Компании поставили статус действующая\n";
                        }
                    }

                    if ($companytype != "CUSTOMER") {
                        echo "Если тип компании какой-либо, кроме Клиент, то проверка наличия сделок.\n";
                        $arFields["COMPANY_TYPE"] = "CUSTOMER";
                        $arOrder                  = Array('DATE_CREATE' => 'DESC');
                        $arFilter                 = Array("COMPANY_ID" => $companyID);
                        $arFilter["!=STAGE_ID"]   = Array(
                            "C1:EXIT",
                            "C5:LOSE",
                            "C4:LOSE",
                            "C3:LOSE",
                            "C2:LOSE",
                            "C1:LOSE",
                            "LOSE",
                            "WON",
                            "C1:WON",
                            "C2:WON",
                            "C3:WON",
                            "C4:WON",
                            "C5:WON"
                        );
                        $arFilter["!=TYPE_ID"]    = Array("2", "");
                        $arSelect                 = Array("*");
                        $result                   = $oDeal->GetListEx($arOrder, $arFilter, $arSelect, false);
                        while ($fields = $result->Fetch()) {
                            $DealTypeArray = $fields;
                            $dealid        = $DealTypeArray['ID'];
                            $dealstatus    = $DealTypeArray['TYPE_ID'];
                            echo "Сделка $dealid статус $dealstatus.\n";
                            if ($DealTypeArray['TYPE_ID'] == "COMPLEX") {
                                $newstatuscompany = "19";
                                echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
                                break;
                            } else if ($DealTypeArray['TYPE_ID'] == "GOODS") {
                                $newstatuscompany = "19";
                                echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
                                break;
                            } else if ($DealTypeArray['TYPE_ID'] == "SALE") {
                                $newstatuscompany = "2";
                                echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
                                break;
                            } else if ($DealTypeArray['TYPE_ID'] == "SERVICE") {
                                $newstatuscompany = "20";
                                echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
                                break;
                            } else if ($DealTypeArray['TYPE_ID'] == "SERVICES") {
                                $newstatuscompany = "19";
                                echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
                            }
                        }
                    }
                    break;
                case "3"://5
                    echo "Статус компании 3\n";

                    //Установка тенанта компании
                    SetUserField("CRM_COMPANY", $companyID, "UF_TENANT", "287");
                    echo "Компании поставили в поле тенант Клиенты, анонсировавшие расторжение\n";

                    //в комментарии записывается информация о запросе расторжения клиентского договора
                    $arFields["COMMENTS"] = "Запрос расторжения клиентского договора";

                    //Получаем тип компании
                    $resultcompany = CCrmCompany::GetByID($companyID);
                    $companytype   = $resultcompany['COMPANY_TYPE'];
                    if ($companytype == "CUSTOMER") {
                        //Установка статуса компании
                        $entity_id     = "CRM_COMPANY";
                        $uf_guid       = "UF_STATUS";
                        $statuscompany = GetUserField($entity_id, $companyID, $uf_guid);
                        if ($statuscompany != "264") {
                            SetUserField($entity_id, $companyID, $uf_guid, "264");
                            echo "Компании поставили статус действующая\n";
                        }
                    }
                    if ($companytype != "CUSTOMER") {
                        echo "Если тип компании какой-либо, кроме Клиент, то проверка наличия сделок.\n";
                        $arFields["COMPANY_TYPE"] = "CUSTOMER";
                        $arOrder                  = Array('DATE_CREATE' => 'DESC');
                        $arFilter                 = Array("COMPANY_ID" => $companyID);
                        $arFilter["!=TYPE_ID"]    = Array("2", "");
                        $arSelect                 = Array("*");
                        $result                   = $oDeal->GetListEx($arOrder, $arFilter, $arSelect, false);
                        while ($fields = $result->Fetch()) {
                            $DealTypeArray = $fields;
                            if ($DealTypeArray['TYPE_ID'] == "COMPLEX") {
                                $newstatuscompany         = "19";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
                                break;
                            } else if ($DealTypeArray['TYPE_ID'] == "GOODS") {
                                $newstatuscompany         = "19";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
                                break;
                            } else if ($DealTypeArray['TYPE_ID'] == "SALE") {
                                $newstatuscompany         = "2";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
                                break;
                            } else if ($DealTypeArray['TYPE_ID'] == "SERVICE") {
                                $newstatuscompany         = "20";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
                                break;
                            } else if ($DealTypeArray['TYPE_ID'] == "SERVICES") {
                                $newstatuscompany         = "19";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
                                break;
                            }

                        }
                    }
                    break;
                case "4"://6
                    echo "Статус компании 4\n";

                    //Установка тенанта компании
                    // SetUserField("CRM_COMPANY", $companyID, "UF_TENANT", "288");
                    // echo "Компании поставили в поле тенант Клиенты, с которыми расторгнут договор\n";

                    //Получаем тип компании
                    $resultcompany = CCrmCompany::GetByID($companyID);
                    $companytype   = $resultcompany['COMPANY_TYPE'];

                    if ($companytype == "CUSTOMER") {
                        //Установка статуса компании
                        $entity_id     = "CRM_COMPANY";
                        $uf_guid       = "UF_STATUS";
                        $statuscompany = GetUserField($entity_id, $companyID, $uf_guid);
                        if ($statuscompany != "266") {
                            SetUserField($entity_id, $companyID, $uf_guid, "266");
                            echo "Компании поставили статус архивный\n";
                        }
                    }

                    if ($companytype != "CUSTOMER") {
                        echo "Если тип компании какой-либо, кроме Клиент, то проверка наличия сделок.\n";
                        $arFields["COMPANY_TYPE"] = "CUSTOMER";
                        $findflag                 = false;
                        $arOrder                  = Array('DATE_CREATE' => 'DESC');
                        $arFilter                 = Array("COMPANY_ID" => $companyID);
                        $arFilter["!=STAGE_ID"]   = Array(
                            "C1:EXIT",
                            "C5:LOSE",
                            "C4:LOSE",
                            "C3:LOSE",
                            "C2:LOSE",
                            "C1:LOSE",
                            "LOSE",
                            "WON",
                            "C1:WON",
                            "C2:WON",
                            "C3:WON",
                            "C4:WON",
                            "C5:WON"
                        );
                        $arFilter["!=TYPE_ID"]    = Array("2", "");
                        $arSelect                 = Array("*");
                        $result                   = $oDeal->GetListEx($arOrder, $arFilter, $arSelect, false);
                        while ($fields = $result->Fetch()) {
                            $DealTypeArray = $fields;
                            if (($DealTypeArray['TYPE_ID'] != "2") && ($DealTypeArray['TYPE_ID'] != "")) {
                                if ($DealTypeArray['TYPE_ID'] == "COMPLEX") {
                                    $newstatuscompany = "19";
                                    echo "Новый статус компании равен $newstatuscompany.\n";
                                    $arFields["COMPANY_TYPE"] = $newstatuscompany;

                                } else if ($DealTypeArray['TYPE_ID'] == "GOODS") {
                                    $newstatuscompany = "19";
                                    echo "Новый статус компании равен $newstatuscompany.\n";
                                    $arFields["COMPANY_TYPE"] = $newstatuscompany;

                                    $arUpdateData = array(
                                        'STAGE_ID' => "C1:EXIT"
                                    );
                                    $oDeal->Update(
                                        $DealTypeArray['ID'],
                                        $arUpdateData,
                                        true,
                                        true,
                                        array('DISABLE_USER_FIELD_CHECK' => true));

                                } else if ($DealTypeArray['TYPE_ID'] == "SALE") {
                                    $newstatuscompany = "2";
                                    echo "Новый статус компании равен $newstatuscompany.\n";
                                    $arFields["COMPANY_TYPE"] = $newstatuscompany;

                                    $arUpdateData = array(
                                        'STAGE_ID' => "C1:EXIT"
                                    );
                                    $oDeal->Update(
                                        $DealTypeArray['ID'],
                                        $arUpdateData,
                                        true,
                                        true,
                                        array('DISABLE_USER_FIELD_CHECK' => true));

                                } else if ($DealTypeArray['TYPE_ID'] == "SERVICE") {
                                    $newstatuscompany = "20";
                                    echo "Новый статус компании равен $newstatuscompany.\n";
                                    $arFields["COMPANY_TYPE"] = $newstatuscompany;

                                    $arUpdateData = array(
                                        'STAGE_ID' => "C1:EXIT"
                                    );
                                    $oDeal->Update(
                                        $DealTypeArray['ID'],
                                        $arUpdateData,
                                        true,
                                        true,
                                        array('DISABLE_USER_FIELD_CHECK' => true));

                                } else if ($DealTypeArray['TYPE_ID'] == "SERVICES") {
                                    $newstatuscompany = "19";
                                    echo "Новый статус компании равен $newstatuscompany.\n";
                                    $arFields["COMPANY_TYPE"] = $newstatuscompany;

                                    $arUpdateData = array(
                                        'STAGE_ID' => "C1:EXIT"
                                    );
                                    $oDeal->Update(
                                        $DealTypeArray['ID'],
                                        $arUpdateData,
                                        true,
                                        true,
                                        array('DISABLE_USER_FIELD_CHECK' => true));

                                } else {
                                    echo "Новый статус компании равен CUSTOMER.\n";
                                    $arFields["COMPANY_TYPE"] = "CUSTOMER";
                                    $entity_id                = "CRM_COMPANY";
                                    $uf_guid                  = "UF_STATUS";
                                    SetUserField($entity_id, $companyID, $uf_guid, "266");
                                }

                            }
                        }
                    }
                    break;
                case "6"://8
                    echo "Статус компании 6\n";

                    //Установка тенанта компании
                    SetUserField("CRM_COMPANY", $companyID, "UF_TENANT", "289");
                    echo "Компании поставили в поле тенант Тензор\n";

                    $arFields["COMMENTS"] = "от Тензора";
                    //Получаем тип компании
                    $resultcompany = CCrmCompany::GetByID($companyID);
                    $companytype   = $resultcompany['COMPANY_TYPE'];

                    if ($companytype == "CUSTOMER") {
                        //Установка статуса компании
                        $entity_id     = "CRM_COMPANY";
                        $uf_guid       = "UF_STATUS";
                        $statuscompany = GetUserField($entity_id, $companyID, $uf_guid);
                        if ($statuscompany != "264") {
                            SetUserField($entity_id, $companyID, $uf_guid, "264");
                            echo "Компании поставили статус действующая\n";
                        }
                    }

                    if ($companytype != "CUSTOMER") {
                        echo "Если тип компании какой-либо, кроме Клиент, то проверка наличия сделок.\n";
                        $arFields["COMPANY_TYPE"] = "CUSTOMER";
                        $arOrder                  = Array('DATE_CREATE' => 'DESC');
                        $arFilter                 = Array("COMPANY_ID" => $companyID);
                        $arSelect                 = Array("*");
                        $arFilter["!=TYPE_ID"]    = Array("CLIENTTYPE", "");
                        $result                   = $oDeal->GetListEx($arOrder, $arFilter, $arSelect, false);
                        while ($fields = $result->Fetch()) {
                            $DealTypeArray = $fields;
                            if ($DealTypeArray['TYPE_ID'] == "COMPLEX") {
                                $newstatuscompany = "19";
                                echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
                                break;
                            } else if ($DealTypeArray['TYPE_ID'] == "GOODS") {
                                $newstatuscompany = "19";
                                echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
                                break;
                            } else if ($DealTypeArray['TYPE_ID'] == "SALE") {
                                $newstatuscompany = "2";
                                echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
                                break;
                            } else if ($DealTypeArray['TYPE_ID'] == "SERVICE") {
                                $newstatuscompany = "20";
                                echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
                                break;
                            } else if ($DealTypeArray['TYPE_ID'] == "SERVICES") {
                                $newstatuscompany = "19";
                                echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
                                break;
                            }
                        }
                    }
                    break;
                case "8"://10
                    echo "Статус компании 8\n";
                    //Установка тенанта компании
                    SetUserField("CRM_COMPANY", $companyID, "UF_TENANT", "290");
                    echo "Компании поставили в поле тенант Без блокировки касс\n";

                    $arFields["COMMENTS"] = "у клиента нет блокировки касс";
                    //Получаем тип компании
                    $resultcompany = CCrmCompany::GetByID($companyID);
                    $companytype   = $resultcompany['COMPANY_TYPE'];

                    if ($companytype == "CUSTOMER") {
                        //Установка статуса компании
                        $entity_id     = "CRM_COMPANY";
                        $uf_guid       = "UF_STATUS";
                        $statuscompany = GetUserField($entity_id, $companyID, $uf_guid);
                        if ($statuscompany != "264") {
                            SetUserField($entity_id, $companyID, $uf_guid, "264");
                            echo "Компании поставили статус действующая\n";
                        }
                    }

                    if ($companytype != "CUSTOMER") {
                        echo "Если тип компании какой-либо, кроме Клиент, то проверка наличия сделок.\n";
                        $arFields["COMPANY_TYPE"] = "CUSTOMER";
                        $arOrder                  = Array('DATE_CREATE' => 'DESC');
                        $arFilter                 = Array("COMPANY_ID" => $companyID);
                        $arFilter["!=TYPE_ID"]    = Array("2", "");
                        $arSelect                 = Array("*");
                        $result                   = $oDeal->GetListEx($arOrder, $arFilter, $arSelect, false);
                        while ($fields = $result->Fetch()) {
                            $DealTypeArray = $fields;
                            if ($DealTypeArray['TYPE_ID'] == "COMPLEX") {
                                $newstatuscompany = "19";
                                echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
                                break;
                            } else if ($DealTypeArray['TYPE_ID'] == "GOODS") {
                                $newstatuscompany = "19";
                                echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
                                break;
                            } else if ($DealTypeArray['TYPE_ID'] == "SALE") {
                                $newstatuscompany = "2";
                                echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
                                break;
                            } else if ($DealTypeArray['TYPE_ID'] == "SERVICE") {
                                $newstatuscompany = "20";
                                echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
                                break;
                            } else if ($DealTypeArray['TYPE_ID'] == "SERVICES") {
                                $newstatuscompany = "19";
                                echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
                                break;
                            }

                        }
                    }
                    break;
                case "PARTNER"://11
                    echo "Статус компании PARTNER\n";

                    //Установка тенанта компании
                    SetUserField("CRM_COMPANY", $companyID, "UF_TENANT", "291");
                    echo "Компании поставили в поле тенант Агент\n";

                    $resultcompany = CCrmCompany::GetByID($companyID);
                    $companytype   = $resultcompany['COMPANY_TYPE'];

                    if ($companytype == "AGENT") {
                        $entity_id     = "CRM_COMPANY";
                        $uf_guid       = "UF_STATUS";
                        $statuscompany = GetUserField($entity_id, $companyID, $uf_guid);

                        if ($statuscompany != "264") {
                            SetUserField($entity_id, $companyID, $uf_guid, "264");
                            echo "Компании поставили статус действующая\n";
                        }
                    }

                    if ($companytype != "AGENT") {
                        echo "Если тип компании какой-либо, кроме Агент, то проверка наличия сделок.\n";
                        $newstatuscompany      = "AGENT";
                        $findflag              = false;
                        $arOrder               = Array('DATE_CREATE' => 'DESC');
                        $arFilter              = Array("COMPANY_ID" => $companyID);
                        $arFilter["!=TYPE_ID"] = Array("SALE", "");
                        $arSelect              = Array("*");
                        $result                = $oDeal->GetListEx($arOrder, $arFilter, $arSelect, false);
                        while ($fields = $result->Fetch()) {
                            $DealTypeArray = $fields;
                            if ($DealTypeArray['TYPE_ID'] == "2") {
                                $newstatuscompany = "2";
                                $findflag         = true;
                                echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
                                break;
                            } else if ($DealTypeArray['TYPE_ID'] == "COMPLEX") {
                                $newstatuscompany = "AGENTBYUER";
                                $findflag         = true;
                                echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
                                break;
                            } else if ($DealTypeArray['TYPE_ID'] == "GOODS") {
                                $newstatuscompany = "AGENTBYUER";
                                $findflag         = true;
                                echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
                                break;
                            } else if ($DealTypeArray['TYPE_ID'] == "SERVICE") {
                                $newstatuscompany = "AGENTPOST";
                                $findflag         = true;
                                echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
                                break;
                            } else if ($DealTypeArray['TYPE_ID'] == "SERVICES") {
                                $newstatuscompany = "AGENTBYUER";
                                $findflag         = true;
                                echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
                                break;
                            }
                        }
                        if ( ! $findflag) {
                            echo "Новый статус компании равен AGENT.\n";
                            $arFields["COMPANY_TYPE"] = "AGENT";
                            $entity_id                = "CRM_COMPANY";
                            $uf_guid                  = "UF_STATUS";
                            SetUserField($entity_id, $companyID, $uf_guid, "264");
                        }
                    }
                    break;
                case "9"://12
                    echo "Статус компании 9\n";
                    $resultcompany = CCrmCompany::GetByID($companyID);
                    $companytype   = $resultcompany['COMPANY_TYPE'];

                    if (($companytype == "CUSTOMER")
                        || ($companytype == "AGENT")
                        || ($companytype == "2")) {
                        //Установка статуса компании
                        $entity_id     = "CRM_COMPANY";
                        $uf_guid       = "UF_STATUS";
                        $statuscompany = GetUserField($entity_id, $companyID, $uf_guid);
                        if ($statuscompany == "266") {
                            SetUserField($entity_id, $companyID, $uf_guid, "265");
                            echo "Компании поставили статус потенциальный\n";
                        }
                    }

                    if (($companytype != "CUSTOMER")
                        && ($companytype != "AGENT")
                        && ($companytype != "2")) {
                        $arFields["COMPANY_TYPE"] = "WITHOUTTYPE";
                        $findflag                 = false;
                        $arOrder                  = Array('DATE_CREATE' => 'DESC');
                        $arFilter                 = Array("COMPANY_ID" => $companyID);
                        $arFilter["!=TYPE_ID"]    = Array("2", "SALE", "");
                        $arSelect                 = Array("*");
                        $result                   = $oDeal->GetListEx($arOrder, $arFilter, $arSelect, false);
                        while ($fields = $result->Fetch()) {
                            $DealTypeArray = $fields;
                            if ($DealTypeArray['TYPE_ID'] == "COMPLEX") {
                                $newstatuscompany = "19";
                                echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
                                break;
                            } else if ($DealTypeArray['TYPE_ID'] == "GOODS") {
                                $newstatuscompany = "19";
                                echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
                                break;
                            } else if ($DealTypeArray['TYPE_ID'] == "SALE") {
                                $newstatuscompany = "2";
                                echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
                                break;
                            } else if ($DealTypeArray['TYPE_ID'] == "SERVICE") {
                                $newstatuscompany = "20";
                                echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
                                break;
                            } else if ($DealTypeArray['TYPE_ID'] == "SERVICES") {
                                $newstatuscompany = "19";
                                echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
                                break;
                            }

                        }
                    }
                    break;
                case "13"://13
                    echo "Статус компании 13\n";

                    $arFields["COMMENTS"] = "от 1С";
                    //Установка тенанта компании
                    SetUserField("CRM_COMPANY", $companyID, "UF_TENANT", "292");
                    echo "Компании поставили в поле тенант 1c\n";

                    $resultcompany = CCrmCompany::GetByID($companyID);
                    $companytype   = $resultcompany['COMPANY_TYPE'];
                    if ($companytype == "CUSTOMER") {

                        //Установка статуса компании
                        $entity_id     = "CRM_COMPANY";
                        $uf_guid       = "UF_STATUS";
                        $statuscompany = GetUserField($entity_id, $companyID, $uf_guid);
                        if ($statuscompany != "264") {
                            SetUserField($entity_id, $companyID, $uf_guid, "264");
                            echo "Компании поставили статус действующая\n";
                        }
                    }

                    if ($companytype != "CUSTOMER") {
                        echo "Если тип компании какой-либо, кроме Клиент, то проверка наличия сделок.\n";
                        $arFields["COMPANY_TYPE"] = "CUSTOMER";
                        $arOrder                  = Array('DATE_CREATE' => 'DESC');
                        $arFilter                 = Array("COMPANY_ID" => $companyID);
                        $arFilter["!=TYPE_ID"]    = Array("CLIENTTYPE", "");
                        $arSelect                 = Array("*");
                        $result                   = $oDeal->GetListEx($arOrder, $arFilter, $arSelect, false);
                        while ($fields = $result->Fetch()) {
                            $DealTypeArray = $fields;
                            if ($DealTypeArray['TYPE_ID'] == "COMPLEX") {
                                $newstatuscompany = "19";
                                echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
                                break;
                            } else if ($DealTypeArray['TYPE_ID'] == "GOODS") {
                                $newstatuscompany = "19";
                                echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
                                break;
                            } else if ($DealTypeArray['TYPE_ID'] == "SALE") {
                                $newstatuscompany = "2";
                                echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
                                break;
                            } else if ($DealTypeArray['TYPE_ID'] == "SERVICE") {
                                $newstatuscompany = "20";
                                echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
                                break;
                            } else if ($DealTypeArray['TYPE_ID'] == "SERVICES") {
                                $newstatuscompany = "19";
                                echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
                                break;
                            } else if ($DealTypeArray['TYPE_ID'] == "2") {
                                $newstatuscompany = "CUSTOMER";
                                echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
                                break;
                            }
                        }
                    }
                    break;
                case "2"://14
                    echo "Статус компании 2\n";

                    //Установка тенанта компании
                    SetUserField("CRM_COMPANY", $companyID, "UF_TENANT", "293");
                    echo "Компании поставили в поле тенант Клиент+агент\n";

                    //Получаем тип компании
                    $resultcompany = CCrmCompany::GetByID($companyID);
                    $companytype   = $resultcompany['COMPANY_TYPE'];
                    if ($companytype == "2") {
                        //Установка статуса компании
                        $entity_id     = "CRM_COMPANY";
                        $uf_guid       = "UF_STATUS";
                        $statuscompany = GetUserField($entity_id, $companyID, $uf_guid);
                        if ($statuscompany != "264") {
                            SetUserField($entity_id, $companyID, $uf_guid, "264");
                            echo "Компании поставили статус действующая\n";
                        }
                    }

                    if ($companytype != "2") {
                        echo "Если тип компании какой-либо, кроме Клиент, то проверка наличия сделок.\n";
                        $arFields["COMPANY_TYPE"] = "2";
                        $findflag                 = false;
                        $arOrder                  = Array('DATE_CREATE' => 'DESC');
                        $arFilter                 = Array("COMPANY_ID" => $companyID);
                        $arFilter["!=TYPE_ID"]    = Array("2", "");
                        $arSelect                 = Array("*");
                        $result                   = $oDeal->GetListEx($arOrder, $arFilter, $arSelect, false);
                        while ($fields = $result->Fetch()) {
                            $DealTypeArray = $fields;
                            if ($DealTypeArray['TYPE_ID'] == "COMPLEX") {
                                $newstatuscompany = "CLIENTAGENTBUYER";
                                $findflag         = true;
                                echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
                                break;
                            } else if ($DealTypeArray['TYPE_ID'] == "GOODS") {
                                $newstatuscompany = "CLIENTAGENTBUYER";
                                $findflag         = true;
                                echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
                                break;
                            } else if ($DealTypeArray['TYPE_ID'] == "SALE") {
                                $newstatuscompany = "2";
                                $findflag         = true;
                                echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
                                break;
                            } else if ($DealTypeArray['TYPE_ID'] == "SERVICE") {
                                $newstatuscompany = "CLIENTAGENTPOST";
                                $findflag         = true;
                                echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
                                break;
                            } else if ($DealTypeArray['TYPE_ID'] == "SERVICES") {
                                $newstatuscompany = "CLIENTAGENTBUYER";
                                $findflag         = true;
                                echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
                                break;
                            }


                        }
                        if ( ! $findflag) {
                            echo "Новый статус компании равен 2.\n";
                            $entity_id = "CRM_COMPANY";
                            $uf_guid   = "UF_STATUS";
                            SetUserField($entity_id, $companyID, $uf_guid, "264");
                        }
                    }
                    break;
                case "10"://15
                    echo "Статус компании 10\n";

                    //Установка тенанта компании
                    SetUserField("CRM_COMPANY", $companyID, "UF_TENANT", "294");
                    echo "Компании поставили в поле тенант Корпорация\n";

                    //Получаем тип компании
                    $resultcompany = CCrmCompany::GetByID($companyID);
                    $companytype   = $resultcompany['COMPANY_TYPE'];
                    if ($companytype == "CUSTOMER") {
                        //Установка статуса компании
                        $entity_id     = "CRM_COMPANY";
                        $uf_guid       = "UF_STATUS";
                        $statuscompany = GetUserField($entity_id, $companyID, $uf_guid);
                        if ($statuscompany != "264") {
                            SetUserField($entity_id, $companyID, $uf_guid, "264");
                            echo "Компании поставили статус действующая\n";
                        }
                    }

                    if ($companytype != "CUSTOMER") {
                        echo "Если тип компании какой-либо, кроме Клиент, то проверка наличия сделок.\n";
                        $arFields["COMPANY_TYPE"] = "CUSTOMER";
                        $arOrder                  = Array('DATE_CREATE' => 'DESC');
                        $arFilter                 = Array("COMPANY_ID" => $companyID);
                        $arFilter["!=TYPE_ID"]    = Array("2", "");
                        $arSelect                 = Array("*");
                        $result                   = $oDeal->GetListEx($arOrder, $arFilter, $arSelect, false);
                        while ($fields = $result->Fetch()) {
                            $DealTypeArray = $fields;
                            if ($DealTypeArray['TYPE_ID'] == "COMPLEX") {
                                $newstatuscompany = "19";
                                echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
                                break;
                            } else if ($DealTypeArray['TYPE_ID'] == "GOODS") {
                                $newstatuscompany = "19";
                                echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
                                break;
                            } else if ($DealTypeArray['TYPE_ID'] == "SALE") {
                                $newstatuscompany = "2";
                                echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
                                break;
                            } else if ($DealTypeArray['TYPE_ID'] == "SERVICE") {
                                $newstatuscompany = "20";
                                echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
                                break;
                            } else if ($DealTypeArray['TYPE_ID'] == "SERVICES") {
                                $newstatuscompany = "19";
                                echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
                                break;
                            }
                        }
                    }
                    break;
                case "16"://16
                    echo "Статус компании 16\n";
                    //Установка тенанта компании
                    SetUserField("CRM_COMPANY", $companyID, "UF_TENANT", "295");
                    echo "Компании поставили в поле тенант Атол-онлайн\n";

                    $arFields["COMMENTS"] = "Атол-онлайн";
                    //Получаем тип компании
                    $resultcompany = CCrmCompany::GetByID($companyID);
                    $companytype   = $resultcompany['COMPANY_TYPE'];

                    if ($companytype == "CUSTOMER") {
                        //Установка статуса компании
                        $entity_id     = "CRM_COMPANY";
                        $uf_guid       = "UF_STATUS";
                        $statuscompany = GetUserField($entity_id, $companyID, $uf_guid);
                        if ($statuscompany != "264") {
                            SetUserField($entity_id, $companyID, $uf_guid, "264");
                            echo "Компании поставили статус действующая\n";
                        }
                    }

                    if ($companytype != "CUSTOMER") {
                        echo "Если тип компании какой-либо, кроме Клиент, то проверка наличия сделок.\n";
                        $arFields["COMPANY_TYPE"] = "CUSTOMER";
                        $findflag                 = false;
                        $arOrder                  = Array('DATE_CREATE' => 'DESC');
                        $arFilter                 = Array("COMPANY_ID" => $companyID);
                        $arSelect                 = Array("*");
                        $arFilter["!=TYPE_ID"]    = Array("2", "");
                        $result                   = $oDeal->GetListEx($arOrder, $arFilter, $arSelect, false);
                        while ($fields = $result->Fetch()) {
                            $DealTypeArray = $fields;
                            if ($DealTypeArray['TYPE_ID'] == "COMPLEX") {
                                $newstatuscompany = "19";
                                echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
                                break;
                            } else if ($DealTypeArray['TYPE_ID'] == "GOODS") {
                                $newstatuscompany = "19";
                                echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
                                break;
                            } else if ($DealTypeArray['TYPE_ID'] == "SALE") {
                                $newstatuscompany = "2";
                                echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
                                break;
                            } else if ($DealTypeArray['TYPE_ID'] == "SERVICE") {
                                $newstatuscompany = "20";
                                echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
                                break;
                            } else if ($DealTypeArray['TYPE_ID'] == "SERVICES") {
                                $newstatuscompany = "19";
                                echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
                                break;
                            }
                        }
                    }
                    break;
                case "17"://17
                    echo "Статус компании 17\n";

                    //Установка тенанта компании
                    SetUserField("CRM_COMPANY", $companyID, "UF_TENANT", "296");
                    echo "Компании поставили в поле тенант Старрус\n";

                    $arFields["COMMENTS"] = "от Старрус";
                    //Получаем тип компании
                    $resultcompany = CCrmCompany::GetByID($companyID);
                    $companytype   = $resultcompany['COMPANY_TYPE'];

                    if ($companytype == "CUSTOMER") {
                        //Установка статуса компании
                        $entity_id     = "CRM_COMPANY";
                        $uf_guid       = "UF_STATUS";
                        $statuscompany = GetUserField($entity_id, $companyID, $uf_guid);
                        if ($statuscompany != "264") {
                            SetUserField($entity_id, $companyID, $uf_guid, "264");
                            echo "Компании поставили статус действующая\n";
                        }
                    }

                    if ($companytype != "CUSTOMER") {
                        echo "Если тип компании какой-либо, кроме Клиент, то проверка наличия сделок.\n";
                        $arFields["COMPANY_TYPE"] = "CUSTOMER";
                        $arOrder                  = Array('DATE_CREATE' => 'DESC');
                        $arFilter                 = Array("COMPANY_ID" => $companyID);
                        $arFilter["!=TYPE_ID"]    = Array("2", "");
                        $arSelect                 = Array("*");
                        $result                   = $oDeal->GetListEx($arOrder, $arFilter, $arSelect, false);
                        while ($fields = $result->Fetch()) {
                            $DealTypeArray = $fields;
                            if ($DealTypeArray['TYPE_ID'] == "COMPLEX") {
                                $newstatuscompany = "19";
                                echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
                                break;
                            } else if ($DealTypeArray['TYPE_ID'] == "GOODS") {
                                $newstatuscompany = "19";
                                echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
                                break;
                            } else if ($DealTypeArray['TYPE_ID'] == "SALE") {
                                $newstatuscompany = "2";
                                echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
                                break;
                            } else if ($DealTypeArray['TYPE_ID'] == "SERVICE") {
                                $newstatuscompany = "20";
                                echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
                                break;
                            } else if ($DealTypeArray['TYPE_ID'] == "SERVICES") {
                                $newstatuscompany = "19";
                                echo "Новый статус компании равен $newstatuscompany.\n";
                                $arFields["COMPANY_TYPE"] = $newstatuscompany;
                                break;
                            }
                        }
                    }
                    break;
                default:
                    break;
            }

            if ($oCompany->Update($companyID, $arFields)) {
                echo "Обновили компанию: $companyID \n";
            } else {
                echo "<pre>arFields[] =".print_r($arFields, true)."</pre>";
                echo "Не удалось обновить компанию $companyID\n";
                echo $oCompany->LAST_ERROR;
                die();

                return false;
            }

        } else //Компания не найдена, создаем компанию
        {            
            switch ($arParams['COMPANY_TYPE']) {
                case "CUSTOMER"://1
                    $arFields['COMPANY_TYPE'] = "CUSTOMER";
                    $arFields['UF_STATUS']    = "264";
                    $arFields['UF_TENANT']    = "286";
                    $typedeal                 = "2";
                    break;
                case "3"://5
                    $arFields['COMPANY_TYPE'] = "CUSTOMER";
                    $arFields['UF_STATUS']    = "264";
                    $arFields['UF_TENANT']    = "287";
                    $typedeal                 = "2";
                    break;
                case "4"://6
                    $arFields['COMPANY_TYPE'] = "CUSTOMER";
                    $arFields['UF_STATUS']    = "266";
                    $arFields['UF_TENANT']    = "288";
                    break;
                case "6"://8
                    $arFields['COMPANY_TYPE'] = "CUSTOMER";
                    $arFields['UF_STATUS']    = "264";
                    $arFields['UF_TENANT']    = "289";
                    $typedeal                 = "CLIENTTYPE";
                    break;
                case "8"://10
                    $arFields['COMPANY_TYPE'] = "CUSTOMER";
                    $arFields['UF_STATUS']    = "264";
                    $arFields['COMMENTS']     = "у клиента нет блокировки касc";
                    $arFields['UF_TENANT']    = "290";
                    break;
                case "PARTNER"://11
                    $arFields['COMPANY_TYPE'] = "AGENT";
                    $arFields['UF_STATUS']    = "264";
                    $arFields['UF_TENANT']    = "291";
                    $typedeal                 = "SALE";
                    break;
                case "9"://12
                    $arFields['COMPANY_TYPE'] = "WITHOUTTYPE";
                    $arFields['UF_STATUS']    = "265";
                    break;
                case "13"://13
                    $arFields['COMPANY_TYPE'] = "CUSTOMER";
                    $arFields['UF_STATUS']    = "264";
                    $arFields['COMMENTS']     = "c 1С";
                    $arFields['UF_TENANT']    = "292";
                    break;
                case "2"://14
                    $arFields['COMPANY_TYPE'] = "2";
                    $arFields['UF_STATUS']    = "264";
                    $arFields['UF_TENANT']    = "293";
                    $typedeal                 = "SALE";
                    break;
                case "10"://15
                    $arFields['COMPANY_TYPE'] = "CUSTOMER";
                    $arFields['UF_STATUS']    = "264";
                    $arFields['UF_TENANT']    = "294";
                    $typedeal                 = "2";
                    break;
                case "16"://16
                    $arFields['COMPANY_TYPE'] = "CUSTOMER";
                    $arFields['UF_STATUS']    = "264";
                    $arFields['UF_TENANT']    = "295";
                    $typedeal                 = "2";
                    break;
                case "17"://17
                    $arFields['COMPANY_TYPE'] = "CUSTOMER";
                    $arFields['UF_STATUS']    = "264";
                    $arFields['UF_TENANT']    = "296";
                    $typedeal                 = "2";
                    break;
                default:
                    break;
            }
			
			$arFields['UF_CRM_INN'] = $arParams['INN'];
			$arFields['UF_CRM_KPP'] = $arParams['KPP'];
			
			//Получение информации от налоговой по ИНН компании
            $arCompanyInfo = GetInfo($arParams['INN']);

            //Добавление компании на основании данных от Налоговой
            if (array_key_exists(0, $arCompanyInfo) && ! empty($arCompanyInfo)) {
                $info = Array(
                    'fields' => Array(
                        'RQ_INN'         => $arParams['INN'],
                        'RQ_KPP'         => (empty($arParams['KPP'])) ? $arCompanyInfo[0]['fields']['RQ_KPP'] : $arParams['KPP'],
                        'RQ_OKVED'       => $arCompanyInfo[0]['fields']['RQ_OKVED'],
                        'SORT'           => 500,
                        'ENTITY_TYPE_ID' => 4,
                    )
                );

                $ogrn = ( ! empty($arCompanyInfo[0]['fields']['RQ_OGRNIP'])) ? $arCompanyInfo[0]['fields']['RQ_OGRNIP'] : $arCompanyInfo[0]['fields']['RQ_OGRN'];
                $ogrn = trim($ogrn);
                switch (strlen($ogrn)) {
                    case 15: //ИП
                        $info['fields']['NAME']           = 'ИП';
                        $info['fields']['RQ_OGRNIP']      = $ogrn;
                        $info['fields']['PRESET_ID']      = 2;
                        $info['fields']['RQ_LAST_NAME']   = $arCompanyInfo[0]['fields']['RQ_LAST_NAME'];
                        $info['fields']['RQ_FIRST_NAME']  = $arCompanyInfo[0]['fields']['RQ_FIRST_NAME'];
                        $info['fields']['RQ_SECOND_NAME'] = $arCompanyInfo[0]['fields']['RQ_SECOND_NAME'];

                        $arFields['TITLE']         = $arCompanyInfo[0]['fields']['RQ_NAME'];
                        $arFields['UF_FULL_NAME']  = $arCompanyInfo[0]['fields']['RQ_NAME'];
                        $arFields['COMPANY_TITLE'] = $arCompanyInfo[0]['fields']['RQ_NAME'];
                        break;
                    default: //Организация
                        $info['fields']['NAME']                 = 'Организация';
                        $info['fields']['RQ_COMPANY_NAME']      = ( ! empty($arCompanyInfo[0]['fields']['RQ_COMPANY_NAME'])) ? $arCompanyInfo[0]['fields']['RQ_COMPANY_NAME'] : $arCompanyInfo[0]['fields']['RQ_COMPANY_FULL_NAME'];
                        $info['fields']['RQ_COMPANY_FULL_NAME'] = ( ! empty($arCompanyInfo[0]['fields']['RQ_COMPANY_FULL_NAME'])) ? $arCompanyInfo[0]['fields']['RQ_COMPANY_FULL_NAME'] : "";
                        $info['fields']['RQ_OGRN']              = $ogrn;
                        $info['fields']['PRESET_ID']            = 1;

                        $arFields['TITLE']         = (empty($arCompanyInfo[0]['fields']['RQ_COMPANY_NAME'])) ? $arCompanyInfo[0]['fields']['RQ_COMPANY_FULL_NAME'] : $arCompanyInfo[0]['fields']['RQ_COMPANY_NAME'];
                        $arFields['UF_FULL_NAME']  = $arCompanyInfo[0]['fields']['RQ_COMPANY_FULL_NAME'];
                        $arFields['COMPANY_TITLE'] = (empty($arCompanyInfo[0]['fields']['RQ_COMPANY_NAME'])) ? $arCompanyInfo[0]['fields']['RQ_COMPANY_FULL_NAME'] : $arCompanyInfo[0]['fields']['RQ_COMPANY_NAME'];
                        break;
                }

                $arFields["ASSIGNED_BY_ID"] = $userId;
                //$arFields['COMPANY_TYPE']   = $arParams['COMPANY_TYPE'];
                $arFields["UF_ID_PROD"] = $arParams['ID_PROD']; //ID компании в BPM

                if ( ! empty($arParams['EMAIL'])) {
                    $arFields['FM']['EMAIL'] = Array(
                        "n0" => Array(
                            "VALUE"      => $arParams['EMAIL'],
                            "VALUE_TYPE" => "WORK",
                        ),
                    );
                }

                if ( ! empty($arParams['PHONE'])) {
                    $arFields['FM']['PHONE'] = Array(
                        "n0" => Array(
                            "VALUE"      => $arParams['PHONE'],
                            "VALUE_TYPE" => "WORK",
                        ),
                    );
                }

                //Создаем компанию
                if ($companyID = $oCompany->Add($arFields)) {
                    echo "Создали компанию: $companyID \n";

                    //Добавляем запись о новой компании в инфоблок "ID созданных компаний/контактов"
                    // addIdCreatedCompany($companyID);
                } else {
					echo "<pre>arFields = ".print_r($arFields, true)."</pre>";
                    echo "Не удалось создать компанию \n";
                    echo $oCompany->LAST_ERROR;

                    return false;
                }
                $info['fields']['ENTITY_ID'] = $companyID;
                if ($resGk = gk_AddRQ($info)) {
                    echo "Успешно добавлены реквизиты для компании $companyID\n";
                } else {
                    echo "Ошибка добавления реквизитов для компании $companyID\n";
                }
            }
            //Добавляем компанию, если нет информации от Налоговой
            if (empty($arCompanyInfo)) {
                echo "Нет информации от налоговой, создаем компанию на основании ИНН/КПП.\n";
                $arFields = array(
                    "TITLE"          => "Организация",
                    "ASSIGNED_BY_ID" => $userId,
                    "COMPANY_TYPE"   => $arParams['COMPANY_TYPE'],
                    "UF_ID_PROD"     => $arParams['ID_PROD'], //ID компании в BPM
                    "UF_CRM_INN"     => $arParams['INN'], //ID компании в BPM
                    "UF_CRM_KPP"     => !empty($arParams['KPP']) ? $arParams['KPP'] : '', //ID компании в BPM
                    "FM"             => Array(
                        "EMAIL" => Array(
                            "n0" => Array(
                                "VALUE"      => $arParams['EMAIL'],
                                "VALUE_TYPE" => "WORK",
                            ),
                        ),
                        "PHONE" => Array(
                            "n0" => Array(
                                "VALUE"      => $arParams['PHONE'],
                                "VALUE_TYPE" => "WORK",
                            ),
                        ),
                    )
                );

                switch ($arParams['COMPANY_TYPE']) {
                    case "CUSTOMER"://1
                        $arFields['COMPANY_TYPE'] = "CUSTOMER";
                        $arFields['UF_STATUS']    = "264";
                        $arFields['UF_TENANT']    = "286";
                        $typedeal                 = "2";
                        break;
                    case "3"://5
                        $arFields['COMPANY_TYPE'] = "CUSTOMER";
                        $arFields['UF_STATUS']    = "264";
                        $arFields['UF_TENANT']    = "287";
                        $typedeal                 = "2";
                        break;
                    case "4"://6
                        $arFields['COMPANY_TYPE'] = "CUSTOMER";
                        $arFields['UF_STATUS']    = "266";
                        $arFields['UF_TENANT']    = "288";
                        break;
                    case "6"://8
                        $arFields['COMPANY_TYPE'] = "CUSTOMER";
                        $arFields['UF_STATUS']    = "264";
                        $arFields['UF_TENANT']    = "289";
                        $typedeal                 = "CLIENTTYPE";
                        break;
                    case "8"://10
                        $arFields['COMPANY_TYPE'] = "CUSTOMER";
                        $arFields['UF_STATUS']    = "264";
                        $arFields['COMMENTS']     = "у клиента нет блокировки касc";
                        $arFields['UF_TENANT']    = "290";
                        break;
                    case "PARTNER"://11
                        $arFields['COMPANY_TYPE'] = "AGENT";
                        $arFields['UF_STATUS']    = "264";
                        $arFields['UF_TENANT']    = "291";
                        $typedeal                 = "SALE";
                        break;
                    case "9"://12
                        $arFields['COMPANY_TYPE'] = "WITHOUTTYPE";
                        $arFields['UF_STATUS']    = "265";
                        break;
                    case "13"://13
                        $arFields['COMPANY_TYPE'] = "CUSTOMER";
                        $arFields['UF_STATUS']    = "264";
                        $arFields['COMMENTS']     = "c 1С";
                        $arFields['UF_TENANT']    = "292";
                        break;
                    case "2"://14
                        $arFields['COMPANY_TYPE'] = "2";
                        $arFields['UF_STATUS']    = "264";
                        $arFields['UF_TENANT']    = "293";
                        $typedeal                 = "SALE";
                        break;
                    case "10"://15
                        $arFields['COMPANY_TYPE'] = "CUSTOMER";
                        $arFields['UF_STATUS']    = "264";
                        $arFields['UF_TENANT']    = "294";
                        $typedeal                 = "2";
                        break;
                    case "16"://16
                        $arFields['COMPANY_TYPE'] = "CUSTOMER";
                        $arFields['UF_STATUS']    = "264";
                        $arFields['UF_TENANT']    = "295";
                        $typedeal                 = "2";
                        break;
                    case "17"://17
                        $arFields['COMPANY_TYPE'] = "CUSTOMER";
                        $arFields['UF_STATUS']    = "264";
                        $arFields['UF_TENANT']    = "296";
                        $typedeal                 = "2";
                        break;
                    default:
                        break;
                }

                //Создаем компанию
                if ($companyID = $oCompany->Add($arFields)) {
                    echo "Создали компанию: $companyID \n";

                    //Добавляем запись о новой компании в инфоблок "ID созданных компаний/контактов"
                    // addIdCreatedCompany($companyID);
                } else {
					echo "<pre>arFields = ".print_r($arFields, true)."</pre>";
                    echo "Не удалось создать компанию \n";
                    echo $oCompany->LAST_ERROR;
                    return false;
                }

                //Добавляем реквизиты
                $info = Array(
                    'fields' => Array(
                        'NAME'           => "Организация",
                        'RQ_INN'         => $arParams['INN'],
                        'RQ_KPP'         => $arParams['KPP'],
                        'SORT'           => 500,
                        'ENTITY_TYPE_ID' => 4,
                        'ENTITY_ID'      => $companyID,
                        'PRESET_ID'      => 1,
                    )
                );

                if ($resGk = gk_AddRQ($info)) {
                    echo "Успешно добавлены реквизиты для компании $companyID\n";
                } else {
                    echo "Ошибка добавления реквизитов для компании $companyID\n";
                }
            }
        }

        /** Поиск/создание контакта по Email */
        //Поиск контакта по Email
        if ( ! empty($arParams['EMAIL'])) {
            $resMultiDublicateContact = CCrmFieldMulti::GetList(
                array('ID' => 'asc'),
                array('ENTITY_ID' => 'CONTACT', 'TYPE_ID' => 'EMAIL', 'VALUE' => $arParams['EMAIL'])
            );
            if ($arMultiDublicateContact = $resMultiDublicateContact->Fetch()) {
                echo "Найден существующий контакт по Email сообщения, ContactID $arMultiDublicateContact[ELEMENT_ID] \n";
                $contactID = $arMultiDublicateContact['ELEMENT_ID'];
            }
        }

        //Контакт не найден, создаем контакт на основании данных из лида
        if (empty($contactID)) {
            //Создаем контакт на основании данных из лида
            if ( ! empty($arParams['EMAIL']) &&
                 ( ! empty($arSelectedLead['LAST_NAME']) || ! empty($arSelectedLead['NAME']) ||
                   ! empty($arSelectedLead['SECOND_NAME']))) {
                $arContact = Array(
                    'NAME'           => $arSelectedLead['NAME'],
                    'SECOND_NAME'    => $arSelectedLead['SECOND_NAME'],
                    'LAST_NAME'      => $arSelectedLead['LAST_NAME'],
                    'ASSIGNED_BY_ID' => $userId,
                    'COMPANY_ID'     => $companyID,
                    "FM"             => Array(
                        "EMAIL" => Array(
                            "n0" => Array(
                                "VALUE"      => $arParams['EMAIL'],
                                "VALUE_TYPE" => "WORK",
                            ),
                        ),
                        "PHONE" => Array(
                            "n0" => Array(
                                "VALUE"      => $arParams['PHONE'],
                                "VALUE_TYPE" => "WORK",
                            ),
                        ),
                    ),
                );
                if ($contactID = $oContact->Add($arContact)) {
                    echo "Создан новый контакт на основании данных из лида: $contactID \n";

                    //Добавляем запись о новом контакте в инфоблок "ID созданных компаний/контактов"
                    //addIdCreatedContact($contactID);
                } else {
                    echo "Ошибка создания нового контакта \n";
                }
            }
        }

        /** Поиск/создание сделки по ID компании */
        $priceCurrentDeal = 0;

        $arDeal = findOptimalDealByCompanyID($companyID);

        //Сделка найдена
		$arFields = [];
        if ($arDeal && $arDeal['ID']) {
            $idDeal           = $arDeal['ID'];
            $priceCurrentDeal = (int)$arDeal['OPPORTUNITY'];
            echo "Найдена сделка ID: $idDeal \n";//DEL

            //Обновление ID ответственного пользователя за сделку
            /*if ($userId != 372) {
                $arFields ["ASSIGNED_BY_ID"] = $userId;

                if ($oDeal->Update($idDeal, $arFields)) {
                    echo "Обновили ответственного за сделку: $idDeal \n";
                } else {
                    echo "Не удалось обновить ответственного за сделку \n";
                    echo $oDeal->LAST_ERROR;
                }
            }*/


        } else //Сделка не найдена
        {
            $arFields = Array(
                "TITLE"          => "Основная услуга ОФД",
                "STAGE_ID"       => "C1:NEGOTIATION",
                "ASSIGNED_BY_ID" => $userId,
                "CATEGORY_ID"    => 1,
                'TYPE_ID'        => $typedeal, //Тип: Основная услуга ОФД
                'COMPANY_ID'     => $companyID,
                "UF_KKT"         => 0, //кол-во ККТ
            );
            $idDeal   = $oDeal->Add($arFields);
            if ($idDeal) {
                echo "Создали сделку ID: $idDeal \n";
            } else {
                echo "Ошибка создания сделки. \n";

                return false;
            }
        }

        /** Поиск/удаление/создание ККТ по RegID и DealID */
        //Если есть RegID
        if ( ! empty($isKKTDefault)) {
            $priceDeletedKKT = 0;
            $priceNewKKT     = (int)$arParams['COST_TARIFF'];

            $arKKT = findDublicateKKT($arParams['RNM_KKT']);

            //Удаление дубликат ККТ
            $isDeleteKKT = false;
            /*if (!empty($arKKT) && !empty($arKKT['ID'])) {
                CIBlockElement::Delete($arKKT['ID']);
                echo "Найден дубликат в реестре ККТ, дубликат удалён. \n";
                //Если ID сделки равняется ID сделки в реестре ККТ, значит делаем дикримент цены в сделке.
                if($idDeal == $arKKT['PROPERTY_DEAL_VALUE']){
                    $priceDeletedKKT = (int)$arKKT['PROPERTY_COST_TARIF_VALUE'];
                }
                $isDeleteKKT = true;
            }*/

            //Добавляем ККТ
            if ($arKKT === false) {
                $arFields = array(
                    "ACTIVE"          => "Y",
                    "IBLOCK_ID"       => $IBLOCK_ID_KKT,
                    "NAME"            => $arParams['NAME_KKT'] . ' Модель (' . $arParams['MODEL'] . ') C\Н (' .
                                         $arParams['NUM_KKT'] . ') Р\Н (' . $arParams['RNM_KKT'] . ')',
                    "PROPERTY_VALUES" => array(
                        "COMPANY"          => $companyID,
                        "DEAL"             => $idDeal,
                        "RNM_KKT"          => $arParams['RNM_KKT'],
                        "MODEL_KKT"        => $arParams['MODEL'],
                        "NUM_KKT"          => $arParams['NUM_KKT'],
                        "NUM_FN"           => $arParams['NUM_FN'],
                        "ADDR_KKT"         => $arParams['ADDRESS_KKT'],
                        "TARIF"            => $arParams['NAME_TARIFF'],
                        "COST_TARIF"       => $arParams['COST_TARIFF'],
                        "NAIMENOVANIE_KKT" => $arParams['NAME_KKT'],
                        "CODE_AGENT"       => $arParams['CODE_AGENT'],
                        "PROMO_KOD"        => $arParams['PROMO_KOD'],
                        "DATE_CONNECT"     => ( ! empty($arParams['DATE_CREATE'])) ? $arParams['DATE_CREATE'] : "",
                        "DATE_ACTIVE"      => ( ! empty($arParams['DATE_FROM'])) ? $arParams['DATE_FROM'] : "",
                        "DATE_OFF"         => ( ! empty($arParams['DATE_TO'])) ? $arParams['DATE_TO'] : "",
                    )
                );

                if ($kktID = $oIBlockElement->Add($arFields)) {
                    echo "Добавили ККТ ID: $kktID \n";
                } else {
                    echo "Ошибка добавления ККТ\n";

                    return false;
                }

                //Формируем актуальное кол-во ККТ
                $countKKT        = 0;
                $resDealCountKKT = $oDeal->GetList(array('ID' => 'desc'), array('ID' => $idDeal),
                    array("ID", "UF_KKT"));
                if ($arDealKKT = $resDealCountKKT->Fetch()) {
                    $countKKT = (int)$arDealKKT['UF_KKT'];
                    $countKKT = (empty($countKKT)) ? 0 : $countKKT;
                    if (empty($isDeleteKKT)) {
                        $countKKT++;
                    }
                }

                $arFields = Array(
                    'UF_KKT'  => $countKKT,
                    'TYPE_ID' => 2
                );

                //Обновление суммы сделки
                $newPriceDeal = $priceCurrentDeal + $priceNewKKT - $priceDeletedKKT;
                $newPriceDeal = ((int)$newPriceDeal < 0) ? "0" : (int)$newPriceDeal;

                if ($priceCurrentDeal != $newPriceDeal) {
                    $arFields['OPPORTUNITY'] = $newPriceDeal;
                }

                $resUpdate = $oDeal->Update($idDeal, $arFields);
                if ($resUpdate) {
                    echo "Обновлена сумма/кол-во_ККТ сделки ID: $idDeal\n";
                } else {
                    echo "Ошибка обновления суммы/кол-ва_ККТ сделки ID: $idDeal \n";
                }

                //Создание истории для сделки
                $idEventHistory = $CCrmEvent->Add(
                    array(
                        'ENTITY_TYPE'  => 'DEAL',
                        'ENTITY_ID'    => $idDeal,
                        'EVENT_ID'     => 'INFO',
                        'EVENT_TEXT_1' => 'Подключён ККТ с регистрационным номером ' . $arParams['RNM_KKT'],
                    )
                );

                if ($idEventHistory) {
                    echo "История сделки успешно создана\n";
                } else {
                    echo "Ошибка создания истории сделки\n";
                }

            }
        }

        /** Поиск/Создание реестра платежей*/
        $isReestPayAdd = true;
        if ( ! empty($isReestPayDefault)) {
            //Поиск реестра платежей по ID компании(сортировка по ID)
            if ($arReestrPay = getReestrPayByCompanyID($companyID)) {
                //Если баланс одинаковый - добавление реестра платежей не происходит
                if ($arReestrPay['PROPERTY_SOSTOYANIE_VALUE'] == $arParams['BALANCE']) {
                    $isReestPayAdd = false;
                }
            }

            if ($isReestPayAdd === true) {
                $arFields = array(
                    "ACTIVE"          => "Y",
                    "IBLOCK_ID"       => $IBLOCK_ID_PAY,
                    "NAME"            => "Актуализация Л/С",
                    "PROPERTY_VALUES" => array(
                        "NUM_LIC_SCHET"   => $arParams["ACCOUNT_ID"],
                        //Номер лицевого счета
                        "RNM_KKT"         => ( ! empty($arParams["RNM_KKT"])) ? $arParams["RNM_KKT"] : "",
                        "INN"             => $arParams["INN"],
                        "KPP"             => ( ! empty($arParams["KPP"])) ? $arParams["KPP"] : "",
                        "LAST"            => getPropertyEnumIdByXmlId($IBLOCK_ID_PAY, "LAST_ACTUALIZATION"),
                        //Последнее событие
                        "TYPE"            => getPropertyEnumIdByXmlId($IBLOCK_ID_PAY, "TYPE_ACTUALIZE"),
                        //Тип операции
                        "SUMM"            => 0,
                        //Сумма операции
                        "SOSTOYANIE"      => $arParams['BALANCE'],
                        //Текущее состояние Л/С
                        "COMPANY"         => $companyID,
                        "ASSIGNED"        => $userId,
                        "ACTIVATION_DATE" => date("11.10.2017"),
                    )
                );
                if ($idElement = $oIBlockElement->Add($arFields, false, false, true)) {
                    echo "Создан элемент в реестре платежей id " . $idElement . ". ";
                } else {
                    echo "Ошибка создания элемента в реестре платежей. ";
                }
            }
        }
    }

//Функция поиска дубликатов лидов по TITLE и EMAIL
    function leadDublicateCheck($title, $email)
    {
        $ResMulti = CCrmFieldMulti::GetList(
            array('ID' => 'desc'), array('ENTITY_ID' => 'LEAD', 'TYPE_ID' => 'EMAIL', 'VALUE' => $email)
        );
        if (intval($ResMulti->SelectedRowsCount()) <= 0) {
            return false;
        }
        while ($arMulti = $ResMulti->Fetch()) {
            $res = CCrmLead::GetList(array(), array("ID" => $arMulti['ELEMENT_ID'], "TITLE" => $title));
            if (intval($res->SelectedRowsCount()) > 0) {
                return $arMulti['ELEMENT_ID'];
            }
        }

        return false;
    }

//Поиск дубликата сделки
    function findDublicateDeal($arFilter)
    {
        $resDublicateDeal = CCrmDeal::GetListEx(array('ID' => 'ASC'), $arFilter);
        if ($arDublicateDeal = $resDublicateDeal->Fetch()) {
            return $arDublicateDeal;
        } else {
            return false;
        }
    }

//Поиск реестра платежей по ID компании
    function getReestrPayByCompanyID($companyID)
    {
        global $IBLOCK_ID_PAY;

        $arSelect = Array("ID", "IBLOCK_ID", "PROPERTY_COMPANY", "PROPERTY_SOSTOYANIE");
        $arFilter = array("IBLOCK_ID" => $IBLOCK_ID_PAY, "PROPERTY_COMPANY" => $companyID);
        $res      = CIBlockElement::GetList(Array("DATE_CREATE" => "DESC"), $arFilter, false, false, $arSelect);
        if ($arPay = $res->Fetch()) {
            return $arPay;
        }

        return false;
    }

    function findOptimalDealByCompanyID($companyID)
    {
        $arFilter = array(
            'CATEGORY_ID' => '1', //Продажа клиент
            'COMPANY_ID'  => $companyID,
        );

        $resDublicateDeal = CCrmDeal::GetList(array('ID' => 'DESC'), $arFilter);
        $n                = 0;
        $arResult         = [];
        while ($arDublicateDeal = $resDublicateDeal->Fetch()) {
            if ($n == 0) {
                $arResult = $arDublicateDeal;
            }

            if ( ! empty($arDublicateDeal['UF_DOGOVOR'])) {
                return $arResult;
            }
            $n++;
        }

        if ( ! empty($arResult)) {
            return $arResult;
        }

        return false;
    }

//Поиск дубликата ККТ
    function findDublicateKKT($kkmRegId)
    {
        if ($kkmRegId) {
            global $IBLOCK_ID_KKT;
            //$IBLOCK_ID_KKT = getIblockIDByCode('registry_kkt');
            $arSelect = Array("ID", "IBLOCK_ID", "PROPERTY_RNM_KKT", "PROPERTY_COST_TARIF", "PROPERTY_DEAL");
            $arFilter = array("IBLOCK_ID" => $IBLOCK_ID_KKT, "PROPERTY_RNM_KKT" => $kkmRegId);
            $res      = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
            if ($arKKT = $res->Fetch()) {
                return $arKKT;
            }

            return false;
        } else {
            return false;
        }
    }

//Получение информации с налоговой
    function GetInfo($inn)
    {
        $propertyTypeID = 'ITIN';
        $propertyValue  = $inn;
        $countryID      = 1;

        $result = \Bitrix\Crm\Integration\ClientResolver::resolve(
            $propertyTypeID,
            $propertyValue,
            $countryID
        );
        if (count($result) == 0) {
            $result = GetInfoOGRNOnline($inn);
        }

        return $result;
    }

    function GetInfoOGRNOnline($inn)
    {
        $dataJSON = file_get_contents('https://xn--c1aubj.xn--80asehdb/%D0%B8%D0%BD%D1%82%D0%B5%D0%B3%D1%80%D0%B0%D1%86%D0%B8%D1%8F/%D0%BA%D0%BE%D0%BC%D0%BF%D0%B0%D0%BD%D0%B8%D0%B8/?%D0%B8%D0%BD%D0%BD=' .
                                      $inn);
        $data     = json_decode($dataJSON, true);

        if (strlen($inn) == 10 && count($data) > 0) {
            $arFields = array(
                'RQ_COMPANY_NAME'      => 'shortName',
                'RQ_COMPANY_FULL_NAME' => 'name',
                'RQ_INN'               => 'inn',
                'RQ_KPP'               => 'kpp',
                'RQ_OGRN'              => 'ogrn'
            );
            $result   = array();
            foreach ($data as $k => $v) {
                $rq = array();
                foreach ($arFields as $kf => $vf) {
                    $rq[$kf] = $v[$vf];
                }
                $result[] = array('fields' => $rq);

            }

            return $result;

        } else {
            $data = array();
        }

        return $data;
    }

    function getFmId($elementID, $typeID)
    {
        $dbResFM = CCrmFieldMulti::GetList(
            array('ID' => 'asc'),
            array('ENTITY_ID' => 'COMPANY', 'TYPE_ID' => $typeID, 'ELEMENT_ID' => $elementID)
        );

        if ($arFM = $dbResFM->Fetch()) {
            return $arFM['ID'];
        }

        return "n0";
    }

    function addIdCreatedCompany($idCompany)
    {
        global $oIBlockElement,
               $IBLOCK_ID_CREATED_COMPANY_CONTACT;

        if (intval($idCompany) <= 0) {
            return false;
        }

        $PROP = array(
            "TYPE_ENTITY" => array("VALUE" => getPropertyEnumIdByXmlId($IBLOCK_ID_CREATED_COMPANY_CONTACT, "COMPANY")),
            "ID_ENTITY"   => $idCompany,
            "STATUS"      => 0,
        );

        $arLoadProductArray = Array(
            "IBLOCK_ID"       => $IBLOCK_ID_CREATED_COMPANY_CONTACT,
            "PROPERTY_VALUES" => $PROP,
            "NAME"            => "Новая компания, ID: $idCompany",
        );

        if ($PRODUCT_ID = $oIBlockElement->Add($arLoadProductArray)) {
            echo "New ID: " . $PRODUCT_ID . "\n";

            return $PRODUCT_ID;
        } else {
            echo "Error: " . $oIBlockElement->LAST_ERROR . "\n";

            return false;
        }
    }

    function addIdCreatedContact($idContact)
    {
        global $oIBlockElement,
               $IBLOCK_ID_CREATED_COMPANY_CONTACT;

        if (intval($idContact) <= 0) {
            return false;
        }

        $PROP = array(
            "TYPE_ENTITY" => array("VALUE" => getPropertyEnumIdByXmlId($IBLOCK_ID_CREATED_COMPANY_CONTACT, "CONTACT")),
            "ID_ENTITY"   => $idContact,
            "STATUS"      => 0,
        );

        $arLoadProductArray = Array(
            "IBLOCK_ID"       => $IBLOCK_ID_CREATED_COMPANY_CONTACT,
            "PROPERTY_VALUES" => $PROP,
            "NAME"            => "Новый контакт, ID: $idContact",
        );

        if ($PRODUCT_ID = $oIBlockElement->Add($arLoadProductArray)) {
            echo "New ID: " . $PRODUCT_ID . "\n";

            return $PRODUCT_ID;
        } else {
            echo "Error: " . $oIBlockElement->LAST_ERROR . "\n";

            return false;
        }
    }