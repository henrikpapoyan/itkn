<?
include_once(__DIR__ . "/../../.config.php");
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule('iblock');
global $USER;
$USER->Authorize(372);

echo date('d.m.Y H:i:s')."\n\r";
$oIBlockElement = new CIBlockElement();
$IBLOCK_ID_CSV = getIblockIDByCode('import_company');

$arFilter = array(
	"IBLOCK_ID"         => $IBLOCK_ID_CSV,
	"PROPERTY_STATUS"   => 0 // берем записи со статусом 0 - не обработан
);
$res = $oIBlockElement->GetList(array(), $arFilter);
echo "Кол-во не обработанных элементов: " .  $res->SelectedRowsCount() . "\n\r";

$arFilter = array(
	"IBLOCK_ID"         => $IBLOCK_ID_CSV,
	"PROPERTY_STATUS"   => 2 // берем записи со статусом 2 - обработан
);
$res = $oIBlockElement->GetList(array(), $arFilter);
echo "Кол-во обработанных элементов: " .  $res->SelectedRowsCount() . "\n\r";

$arFilter = array(
	"IBLOCK_ID"         => $IBLOCK_ID_CSV,
	"PROPERTY_STATUS"   => 1 // берем записи со статусом  1 - В Работе
);
$res = $oIBlockElement->GetList(array(), $arFilter);
while($ob = $res->GetNextElement())
{
	$arFields = $ob->GetFields();
	$arProps = $ob->GetProperties();
	$idElement = $arFields['ID'];
	$status = $arProps['STATUS']['VALUE'];
	if($status == "1"){
		$prop['STATUS'] = array('VALUE'=>"0");
		CIBlockElement::SetPropertyValuesEx($idElement, $IBLOCK_ID_CSV, $prop);
	}
}
echo "Кол-во элементов в работе: " .  $res->SelectedRowsCount() . "\n\r";

$arFilter = array(
	"IBLOCK_ID"         => $IBLOCK_ID_CSV,
	"PROPERTY_STATUS"   => 3 
);
$res = $oIBlockElement->GetList(array(), $arFilter);
/*while($ob = $res->GetNextElement())
{
	$arFields = $ob->GetFields();
	$arProps = $ob->GetProperties();
	$idElement = $arFields['ID'];
	$status = $arProps['STATUS']['VALUE'];
	if($status == "3"){
		$prop['STATUS'] = array('VALUE'=>"0");
		//CIBlockElement::SetPropertyValuesEx($idElement, $IBLOCK_ID_CSV, $prop);
	}
}*/
echo "Кол-во элементов с ошибками: " .  $res->SelectedRowsCount(). "\n\r";
?>