<?php

    use Bitrix\Main\Mail\Event;

    include_once(__DIR__ . "/../../.config.php");
    require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

    CModule::IncludeModule("learning");
    CModule::IncludeModule("disk");
    CModule::IncludeModule("im");

    list($arCourse, $currentStudents, $folder4ReportsID, $reportViewersIDs, $link2DetailReport) = getCourseData();

    $courseIsActual = strtotime($arCourse["ACTIVE_TO"]) > strtotime(date("d.m.Y H:i:s"));

    if (empty($currentStudents) || $courseIsActual || ! empty($link2DetailReport)) {
        echo "exit";

        return;
    }

    $arUserResults = getStatistic($arCourse, $currentStudents);

    $tempFileName = createCSV($arUserResults);

    $extLinkUrl = uploadFile2Disk($arCourse, $folder4ReportsID, $tempFileName);

    sendEmail($reportViewersIDs, $extLinkUrl);

    function getStatistic($arCourse, $students)
    {
        $arUserResults = array("success" => array(), "failed" => array(), "wait" => array(), "attempts" => array());

        $res    = CTest::GetList(array(), array("COURSE_ID" => $arCourse['ID']));
        $arTest = $res->Fetch();

        $res = CTestAttempt::GetList(
            Array("SCORE" => "DESC"),
            Array(
                "CHECK_PERMISSIONS" => "N",
                "TEST_ID"           => $arTest['ID'],
                "STUDENT_ID"        => $students,
                ">DATE_END"         => $arCourse["ACTIVE_FROM"]
            )
        );

        while ($arAttempt = $res->GetNext()) {
            $arUserResults["attempts"][$arAttempt["USER_ID"]]++;
            if ($arAttempt["COMPLETED"] == 'Y') {
                $arUserResults["success"][$arAttempt['USER_ID']] = $arAttempt;
            } elseif (empty($arUserResults["success"][$arAttempt["USER_ID"]])) {
                $arUserResults["failed"][$arAttempt['USER_ID']] = $arAttempt;
            }
        }

        foreach ($students as $student) {
            if (empty($arUserResults['success'][$student]) && empty($arUserResults['failed'][$student])) {
                $arWaitUser                      = CUser::GetByID($student)->Fetch();
                $arWaitUser["USER_NAME"]         = "(" . $arWaitUser["LOGIN"] . ") " . $arWaitUser["NAME"] . " " .
                                                   $arWaitUser["LAST_NAME"];
                $arUserResults["wait"][$student] = $arWaitUser;
            }
        }

        return $arUserResults;
    }

    function getCourseData()
    {
        $res      = CCourse::GetList(
            Array("SORT" => "ASC"),
            Array("CODE" => "COURSE_IB"));
        $arCourse = $res->Fetch();
        $lessonID = $arCourse["LESSON_ID"];

        $arUF = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields("LEARNING_LESSONS", $lessonID);

        $reportViewersIDs = array();
        foreach ($arUF["UF_DEF_REPORT_VIEWER"]["VALUE"] as $id) {
            $rsUser             = CUser::GetByID($id);
            $reportViewersIDs[] = $rsUser->Fetch();
        }

        $currentStudents = array();
        foreach ($arUF["UF_CURRENT_STUDENTS"]["VALUE"] as $id) {
            $currentStudents[] = $id;
        }

        $folder4ReportsID  = $arUF["UF_FOLDER_ID_4_REP"]["VALUE"];
        $link2DetailReport = $arUF["UF_URL_DETAIL_REPORT"]["VALUE"];

        return array($arCourse, $currentStudents, $folder4ReportsID, $reportViewersIDs, $link2DetailReport);
    }

    function createCSV($arUserResults)
    {
        $list = array();
        if ( ! empty($arUserResults["success"])) {
            $list[] = array();
            $list[] = array(
                'Пользователи, прошедшие обучение:',
                'ID',
                'Пользователь',
                'Попыток',
                'Результат прохождения'
            );
            foreach ($arUserResults["success"] as $student) {
                $list[] = array(
                    "",
                    $student["STUDENT_ID"],
                    $student["USER_NAME"],
                    $arUserResults["attempts"][$student["STUDENT_ID"]],
                    'Пройден'
                );
            }
        }

        if ( ! empty($arUserResults["failed"])) {
            $list[] = array();
            $list[] = array(
                'Пользователи, прошедшие обучение:',
                'ID',
                'Пользователь',
                'Попыток',
                'Результат прохождения'
            );

            foreach ($arUserResults["failed"] as $student) {
                $list[] = array(
                    "",
                    $student["STUDENT_ID"],
                    $student["USER_NAME"],
                    $arUserResults["attempts"][$student["STUDENT_ID"]],
                    'Не пройден'
                );
            }
        }

        if ( ! empty($arUserResults["wait"])) {
            $list[] = array();
            $list[] = array(
                'Пользователи, не принимавшие участия в тестировании:',
                'ID',
                'Пользователь'
            );
            foreach ($arUserResults["wait"] as $student) {
                $list[] = array(
                    "",
                    $student["ID"],
                    $student["USER_NAME"]
                );
            }
        }

        foreach ($list as $row => $dataRow) {
            foreach ($dataRow as $column => $dataColumn) {
                $list[$row][$column] = iconv("UTF8", "Windows-1251", $dataColumn);
            }
        }

        $tempFileName = "Detailed_statistic_IB_course_" . date("d-m-Y_H:i:s") . ".csv";
        $fp           = fopen($tempFileName, 'w');
        foreach ($list as $fields) {
            fputcsv($fp, $fields, ';', '"');
        }
        fclose($fp);

        return $tempFileName;
    }

    function uploadFile2Disk($arCourse, $folderID, $tempFileName)
    {
        $folder    = \Bitrix\Disk\Folder::loadById($folderID);
        $fileArray = \CFile::MakeFileArray(__DIR__ . "/" . $tempFileName);

        $mergedFileName = "Детальная статистика прохождения курса ИБ " . date("d-m-Y H:i:s") . ".csv";

        $file = $folder->uploadFile($fileArray, array(
            "NAME"       => $mergedFileName,
            'CREATED_BY' => 372
        ));

        if ($file) {
            $urlManager = \Bitrix\Disk\Driver::getInstance()->getUrlManager();
            $extLink    = $file->addExternalLink(
                array(
                    'CREATED_BY' => 1,
                    'TYPE'       => \Bitrix\Disk\Internals\ExternalLinkTable::TYPE_MANUAL,
                )
            );
            $extLinkUrl = $urlManager->getShortUrlExternalLink(
                array(
                    'hash'   => $extLink->getHash(),
                    'action' => 'default',
                ),
                true
            );
        }

        $GLOBALS["USER_FIELD_MANAGER"]->Update("LEARNING_LESSONS", $arCourse["LESSON_ID"],
            array("UF_URL_DETAIL_REPORT" => $extLinkUrl));

        if ($file) {
            unlink(__DIR__ . "/" . $tempFileName);
        }

        return $extLinkUrl;
    }

    function sendEmail($reportViewers, $extLinkUrl)
    {
        $emailTo = array();
        foreach ($reportViewers as $reportViewer) {
            $arMessageFields = array(
                "TO_USER_ID"     => $reportViewer["ID"],
                "FROM_USER_ID"   => 0,
                "NOTIFY_TYPE"    => IM_NOTIFY_SYSTEM,
                "NOTIFY_MODULE"  => "im",
                "NOTIFY_TAG"     => "IM_CONFIG_NOTICE",
                "NOTIFY_MESSAGE" => "Сформирован отчёт о прохождении курса по информационной безопасности. Скачать файл: " .
                                    $extLinkUrl,
            );
            CIMNotify::Add($arMessageFields);

            $emailTo[] = $reportViewer["EMAIL"];
        }

        $emailsString = implode(",", $emailTo);

        Event::send(array(
            "EVENT_NAME" => "DETAIL_REPORT_READY",
            "LID"        => "s1",
            "C_FIELDS"   => array(
                "AR_EMAIL_TO" => $emailsString,
                "LINK"        => $extLinkUrl
            ),
        ));
    }
