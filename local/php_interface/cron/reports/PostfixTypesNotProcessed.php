<?

/**
 * Cron-скрипт для сбора не обработанных событий Postfix за предыдущий день и обнуления статуса в работе
 * для повторного запуска обработки события
 */

/**
 * Подключение конфиг файлов и модулей
 * .config.php -
 * cronconfig.php - конфиг файл для работы работы cron файла
 * postfixtypes.php - конфиг файл для инициации реализованных postfix типов
 * highloadblock - модуль для работы с highloadblock блоком, в котором записаны Postfix сообщения.
 * */
include_once(__DIR__ . "/../../.config.php");
include_once(__DIR__ . "../../include/cronconfig.php");
include_once(__DIR__ . "/../../include/postfixtypes.php");
\Bitrix\Main\Loader::includeModule('highloadblock');
use Bitrix\Highloadblock as HL;

/**
 * Инициализируем переменные для работы скрипта.
 * $toTime - хранит дату предыдущего дня
 * $HL_Infoblock_ID - ID highloadblock блока, в котором запписаны Postfix сообщения
 * $arFilter - фильтр для уточнения выборки Postfix сообщений использует:
 * 1) Фильтр по типам событий (значение - реализованные события) - UF_EVENT_TYPE
 * 2) Фильтр по состоянию Обработано(значение - Нет) - UF_PROCESSED
 * 3) фильтр по дате получения (значение до вчерашней даты) - UF_DATE
 * 4) фильтр по состоянию в работе (значение да) - UF_IN_WORK
 * */

$toTime = date("d.m.Y", time() - 24 * 60 * 60 * 1);
$HL_Infoblock_ID = 3;
$arFilter = array();
$arFilter['=UF_EVENT_TYPE'] = $arMessageType;
$arFilter['UF_PROCESSED'] = false;
$arFilter['<=UF_DATE'] = $toTime;
$arFilter['UF_IN_WORK'] = 1;

/**
 * Алгоритм:
 * Выбираем HL-блок и данные HL-блока на основе фильтра
 * Для каждого элемента выборки обнуляем статус в работе
 * */

$hlblock = HL\HighloadBlockTable::getById($HL_Infoblock_ID)->fetch();
$entity = HL\HighloadBlockTable::compileEntity($hlblock);
$entity_data_class = $entity->getDataClass();
$rsData = $entity_data_class::getList(array(
    'select' => array('ID', 'UF_EVENT_TYPE'),
    'filter' => $arFilter
));
while ($el = $rsData->fetch()) {
    $entity_data_class::update($el['ID'], array("UF_IN_WORK" => "0"));
}
?>