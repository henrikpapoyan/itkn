<?

/**
 * Крон скрипт для оповещения администратора о поступивших за 4 часа событиях Postfix
 */

/**
 * Подключение конфиг файлов и модулей
 * .config.php -
 * cronconfig.php - конфиг файл для работы работы cron файла
 * postfixtypes.php - конфиг файл для инициации реализованных postfix типов
 * highloadblock - модуль для работы с highloadblock блоком, в котором записаны Postfix сообщения.
 * */

include_once(__DIR__ . "/../../.config.php");
include_once(__DIR__ . "/../../include/cronconfig.php");
include_once(__DIR__ . "/../../include/postfixtypes.php");
\Bitrix\Main\Loader::includeModule('highloadblock');
use Bitrix\Highloadblock as HL;

/**
 * Инициализируем переменные для работы скрипта.
 * $fromTime - минус 4 часа от текущего времени
 * $toTime - текушее время
 * $headers - заголов отправлемого сообщения (Установка Mime и Charset)
 * $message - тело сообщение для отправки по email
 * $HL_Infoblock_ID - ID highloadblock блока, в котором запписаны Postfix сообщения
 * $arFilter - фильтр для уточнения выборки Postfix сообщений использует:
 * 1) фильтр по дате получения (значение до текущего времени) - UF_DATE
 * 2) фильтр по дате получения (значение после -4 часов) - UF_DATE
 * */

$fromTime = date('d.m.Y H:i:s', time() - 4 * 60 * 60);
$toTime = date("d.m.Y H:i:s", time());
$arResultCountMessagePerDay = array();
$HL_Infoblock_ID = 3; //ID предварительно созданного HL ИБ
$arFilter = array();
$arFilter['>=UF_DATE'] = $fromTime;
$arFilter['<=UF_DATE'] = $toTime;
$arFilter['!UF_EVENT_TYPE'] = "";
$headers = "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
$message = "";



/*Инициализируем типы сообщений, которые имеют реализацию для контроля не пришедших сообщений по ним*/

$arResultCountMessagePerDay = array();
foreach ($arMessageType as $typeMessage) {
    if (!isset($arResultCountMessagePerDay[$typeMessage])) {
        $arResultCountMessagePerDay[$typeMessage] = 0;
    }
}



/**
 * Алгоритм:
 * Выбираем HL-блок и данные HL-блока на основе фильтра
 * Производим подсчет общего количества сообщений по каждому событию
 * и отправляем на почту it_otdel@energocomm.ru и onizamov@energocomm.ru
 * */

/**
 * Выбираем данные по событиям и подсчитываем общее количество сообщений для каждого события
 * с сохренением их в массив $arResultCountMessagePerDay
 * */

$hlblock = HL\HighloadBlockTable::getById($HL_Infoblock_ID)->fetch();
$entity = HL\HighloadBlockTable::compileEntity($hlblock);
$entity_data_class = $entity->getDataClass();
$rsData = $entity_data_class::getList(array(
    'select' => array('ID', 'UF_EVENT_TYPE', 'UF_DATE'),
    'filter' => $arFilter
));
while ($el = $rsData->fetch()) {
    if (!isset($arResultCountMessagePerDay[$el['UF_EVENT_TYPE']])) {
        $arResultCountMessagePerDay[$el['UF_EVENT_TYPE']] = 1;
    } else {
        $arResultCountMessagePerDay[$el['UF_EVENT_TYPE']]++;
    }
}

/**
 * Генерация html таблицы для отправки на почту
 * */

$message = '<html><body>';
$message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
foreach ($arResultCountMessagePerDay as $index => $elementResultCountMessagePerDay) {
    $message .= '<tr>';
    if ($elementResultCountMessagePerDay == 0) {
        $message .= "<td style='color:red'>" . $index . "</td>";
        $message .= "<td>" . $elementResultCountMessagePerDay . "</td>";
    } else {
        $message .= "<td>" . $index . "</td>";
        $message .= "<td>" . $elementResultCountMessagePerDay . "</td>";
    }
    $message .= '</tr>';
}
$message .= "</table>";
$message .= "</body></html>";

/**
 * Отправка на почту сообщений
 * */

//mail('it_otdel@energocomm.ru', 'Информация о Postfix сообщениях за последние 4 часа', $message, $headers);
mail('onizamov@energocomm.ru', 'Информация о Postfix сообщениях за последние 4 часа', $message, $headers);
?>