<?
/*
 * Метод для формирования данных для “Контроль продаж компании” для страницы start раздела crm.
 * Формирует данные за все основные периоды для того, чтобы сразу отображать результат.
 */
include_once(__DIR__ . "/../../.config.php");

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
ini_set('memory_limit', '-1');

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
global $USER;
global $DB;
$USER->Authorize(372);
$arPeriodTypes = array("D0", "D7", "D30", "M0", "D60", "D90", "Q0");
$START_TIME = mktime();
foreach($arPeriodTypes as $periodType){
    $START_TIME_ONE = mktime();
    $chanelInfo = new controlSalesCompanyByLead($periodType);
    $TIME_ONE = (mktime() - $START_TIME_ONE) / 60;
    echo "\n\rВремя обработки периода [$periodType], мин: $TIME_ONE \n";
}
$TIME = (mktime() - $START_TIME) / 60;
echo "\n\nВремя обработки, мин: $TIME \n\n";
?>