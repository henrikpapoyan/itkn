<?php
include_once(__DIR__ . "/../../.config.php");
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('CHK_EVENT', true);

$date = date(" Y-m-d H:i:s ");
file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/log/test_cron.log', $date . PHP_EOL, FILE_APPEND);

$sConsumer = "AgentMessagePostfix.php";
$sConsumerPath = "/home/bitrix/www/local/php_interface/cron/postfix/";

exec ("ps ax| grep " . $sConsumer , $arOutput, $bReval);

file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/log/test_cron.log', print_r($arOutput, true) . PHP_EOL, FILE_APPEND);

$bScriptFounded = false;
foreach ($arOutput as $key => $sOutput) {
    if (
        strpos($sOutput, "php AgentMessagePostfix.php") ||
        strpos($sOutput, "/usr/bin/php -f " . $sConsumerPath . $sConsumer) ||
        strpos($sOutput, "/usr/bin/php AgentMessagePostfix.php")
    ) {
        $bScriptFounded = true;
        file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/log/test_cron.log', "Скрипт завершен, т.к. имеется дубль процесса" . PHP_EOL, FILE_APPEND);
        die("Скрипт завершен, т.к. имеется дубль процесса");
    }
}
file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/log/test_cron.log', "Запустилась обработка элементов" . PHP_EOL, FILE_APPEND);

if (!$bScriptFounded) {
    exec("/usr/bin/php -f " . $sConsumerPath . "AgentMessagePostfix.php > /dev/null 2>&1 &");
    file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/log/test_cron.log', "Произведен запуск скрипта AgentMessagePostfix.php" . PHP_EOL, FILE_APPEND);
}
