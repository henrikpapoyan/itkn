<?
include_once(__DIR__ . "/../../.config.php");
//die();
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('CHK_EVENT', true);

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/Classes/postfix/AgentMessagePostfix.class.php");

\Bitrix\Main\Loader::includeModule('highloadblock');

@set_time_limit(0);
@ignore_user_abort(true);
//авторизация файла по ID пользователя
global $USER;
$USER->Authorize(372);

$logger = Logger::getLogger("AgentMessagePostfix", "AgentMessagePostfix/result.log");
$logger->log('Run script AgentMessagePostfix');

$AM = new AgentMessagePostfix();

//Получаем письмо из HigloadBlock
use Bitrix\Highloadblock as HL; 

$HL_Infoblock_ID = 3; //ID предварительно созданного HL ИБ

$hlblock = HL\HighloadBlockTable::getById($HL_Infoblock_ID)->fetch(); 
$entity = HL\HighloadBlockTable::compileEntity($hlblock);
$entity_data_class = $entity->getDataClass();

//Список обрабатываемых событий
$arEventType = array(
    'Event Type NewLead',
    'Temporary_account',
    /*'Full_account',
    'add_one_kkt',
    'kkt_tu_tariff',
    'Activation_KKT',
	'Dectivation_KKT',
    'TopUp_personal_accaunt',
	'Down_personal_accaunt',
    'Back_personal_accaunt',*/
    'Event Type NewLeadSer',
    'Event Type NewLeadFN',
    'Event Type NewLeadBD',
    'OrderKKT',
    'OrderEDO',
    'dopserv',
    'RentKKT',
    'RequestClosingDoc',
);

$START_TIME = mktime();
$rsData = $entity_data_class::getList(array(
	'order'  => array('ID' => 'ASC'),
    'select' => array('*'),
    'filter' => array(
        'UF_PROCESSED'      => 'false', //Берем не обработанные элементы
        'UF_EVENT_TYPE'     => $arEventType,
    )
));
$TIME = (mktime() - $START_TIME);
echo "\nВремя выборки всех не обработанных элементов, сек: $TIME \n";

echo "Кол-во выбранных элементов:" . $rsData->getSelectedRowsCount() . " \n\n";

$START_TIME = mktime();
$count = 0;
while ($el = $rsData->fetch()) {
	//break;

    //Проверяем время. Если время работы превышает 55 сек - делаем break.
    $passedTime = (mktime() - $START_TIME);
    if($passedTime > 55) {
        break;
    }

    $msg = $el['UF_TEXTMAIL'];
    if ($AM->parseOne($msg, $el['ID'], $entity_data_class) === false) {		
        continue;
    }	

	if(!empty($AM->error)){
		print_r("\nEvent Type: " . $el['UF_EVENT_TYPE'] . "\n");
		print_r("ID Element: " . $el['ID'] . "\n");
		print_r("AM->status: " . $AM->status . "\n");
		print_r("AM->error: " . $AM->error . "\n\n");
	}

    $data = array();
    $data['UF_PROCESSED'] = "1";
    $data['UF_IN_WORK'] = "0";

    //Работа завершена с ошибками
    if (!empty($AM->error)) {
        $data['UF_ERROR'] = "1";
        $data['UF_ERROR_TEXT'] = $AM->error;
    }

    //Работа завершена c успешно
    if (!empty($AM->status)) {
        $data['UF_RESULT'] = $AM->status;
    }

    $result = $entity_data_class::update($el['ID'], $data);

    //Очистить статус
    $AM->status = "";
    $AM->error = "";
		
	$count++;

    if($count >= 1000){
        break;
    }

	//break;
}
$TIME = (mktime() - $START_TIME);
echo "\nВремя обработки, сек: $TIME \n\n";
echo "Всего обработалось элементов: $count \n\n";

$USER->Logout(); //делогируем пользователя
$logger->log('End script AgentMessagePostfix');
