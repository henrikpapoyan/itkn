<?php

class CatalogHandler
{
    function setDefaultNdsForProduct(&$arFields)
    {
//        define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"] . "/log.txt");
        AddMessage2Log('setDefaultNdsForProduct ДО ' . print_r($arFields, true));
        
        if (empty($arFields["VAT_ID"])) {
            $arFields["VAT_ID"] = 3;
        }
        if (!empty($arFields["VAT_ID"])) {
            $arFields["VAT_INCLUDED"] = "Y";
        }
        
        //Testing measure id
        if (empty($arFields["MEASURE"])) {
            $arFields["MEASURE"] = 112;
        }
        
        AddMessage2Log('setDefaultNdsForProduct ПОСЛЕ ' . print_r($arFields, true));
    }
}