<?php
/**
 * Created by PhpStorm.
 * User: Рустам
 * Date: 28.02.2019
 * Time: 19:21
 */

include_once(__DIR__ . "/../../.config.php");
//die();
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('CHK_EVENT', true);

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/Classes/postfix/AgentMessagePostfix.class.php");

\Bitrix\Main\Loader::includeModule('highloadblock');

@set_time_limit(0);
@ignore_user_abort(true);
//авторизация файла по ID пользователя
global $USER;
$USER->Authorize(372);

//Получаем письмо из HigloadBlock
use Bitrix\Highloadblock as HL;

$HL_Infoblock_ID = 3; //ID предварительно созданного HL ИБ

$hlblock = HL\HighloadBlockTable::getById($HL_Infoblock_ID)->fetch();
$entity = HL\HighloadBlockTable::compileEntity($hlblock);
$entity_data_class = $entity->getDataClass();

$START_TIME = mktime();
$dateDelimetr = new DateTime('-28 days');
$rsData = $entity_data_class::getList(array(
    'order'  => array('ID' => 'ASC'),
    'select' => array('ID'),
    'filter' => array("<UF_DATE" => $dateDelimetr->format('d.m.Y H:i:s')),
));
$TIME = (mktime() - $START_TIME);
echo "\nВремя выборки всех не обработанных элементов, сек: $TIME \n";
echo "Кол-во выбранных элементов:" . $rsData->getSelectedRowsCount() . " \n";

$START_TIME = mktime();
$count = 0;
while ($el = $rsData->fetch()) {
    $entity_data_class::delete($el["ID"]);
    $count++;
};
$TIME = (mktime() - $START_TIME);
echo "Время удаления $count элементов, сек: $TIME \n";
?>