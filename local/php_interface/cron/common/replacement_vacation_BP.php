<?
include_once(__DIR__ . "/../../.config.php");
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('CHK_EVENT', true);

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
global $USER;
$USER->Authorize(372);
if (!CModule::IncludeModule("bizproc"))
    return false;
CModule::IncludeModule("iblock");
define("CONST_ZAMENA_VYPOLNENA_ENUM_ID", getPropertyEnumIdByXmlId(GetIBlockIDByCode("replace_list_for_holidays"), 'REPLACE_NEW'));
define("CONST_RETURN_ZAMENA_ENUM_ID", getPropertyEnumIdByXmlId(GetIBlockIDByCode("replace_list_for_holidays"), 'RETURN_REPLACE'));
$el = new CIBlockElement();
$curDate = new DateTime();
//echo "curDate = ".$curDate->format('d.m.Y H:i:s')."\n\r";
$rsReplacmentList = $el->GetList(
    array("ID" => "DESC"),
    array("IBLOCK_ID" => GetIBlockIDByCode("replace_list_for_holidays"), "ACTIVE" => "Y"),
    false,
    false,
    array(
        "ID",
        "NAME",
        "PROPERTY_SOTRUDNIK_UKHODYASHCHIY_V_OTPUSK",
        "PROPERTY_DATA_NACHALA_OTPUSKA",
        "PROPERTY_DATA_OKONCHANIYA_OTPUSKA",
        "PROPERTY_ZAMENYAYUSHCHIY_SOTRUDNIK",
        "PROPERTY_ZAMENA_VYPOLNENA",
        "PROPERTY_DANNYE_PO_VYPOLNENNYM_ZAMENAM",
    )
);
$arVacationsStartUsers = [];
$arUsersGoingVacation = []; //Хранится user_id пользователей, для которых необходимо сделать замену
$arUsersReturnVacation = []; //Хранится user_id пользователей, для которых необходимо сделать ОБРАТНУЮ замену
$arVacationsEndUsers = [];
$arTotalReplacmentUsers = []; //Общий список пользователей которых нужно заменить
while($arReplacmentList = $rsReplacmentList->GetNext()){
    //pre($arReplacmentList);
    $vacationStart = new DateTime($arReplacmentList["PROPERTY_DATA_NACHALA_OTPUSKA_VALUE"]);
    $vacationEnd = new DateTime($arReplacmentList["PROPERTY_DATA_OKONCHANIYA_OTPUSKA_VALUE"]." 23:59:59");
    $vacationingEmployee = "user_".$arReplacmentList["PROPERTY_SOTRUDNIK_UKHODYASHCHIY_V_OTPUSK_VALUE"];
    $substituteEmployee = "user_".$arReplacmentList["PROPERTY_ZAMENYAYUSHCHIY_SOTRUDNIK_VALUE"];
    //Проверяем, что даты окончания больше дата начала
    if($vacationEnd >= $vacationStart) {
       // echo "Дата верная\n\r";

        //Формуриуем список у которых только начался отпуск
        //Проверяем, что текущая дата находится в этом промежутке и замена еще не выполнялась
        if($curDate>=$vacationStart && $curDate<=$vacationEnd && is_null($arReplacmentList["PROPERTY_ZAMENA_VYPOLNENA_ENUM_ID"]) ) {
            if( !in_array($vacationingEmployee, $arUsersGoingVacation) )
                $arUsersGoingVacation[$arReplacmentList["ID"]] = $vacationingEmployee;
            $arVacationsStartUsers[$arReplacmentList["ID"]] = array(
                "VACATIONING_EMPLOYEE" => $vacationingEmployee,
                "SUBSTITUTE_EMPLOYEE" => $substituteEmployee,
            );
        }

        //Формируем список у которых только завершился отпуск
        //Текущая дата больше даты окончания и замена выполнена
        if($curDate>$vacationEnd && $arReplacmentList["PROPERTY_ZAMENA_VYPOLNENA_ENUM_ID"] == CONST_ZAMENA_VYPOLNENA_ENUM_ID) {
            //echo "Дата верная\n\r";
            if( !in_array($substituteEmployee, $arUsersReturnVacation) )
                $arUsersReturnVacation[$arReplacmentList["ID"]] = $substituteEmployee;
            $arVacationsEndUsers[$arReplacmentList["ID"]] = $arReplacmentList["PROPERTY_DANNYE_PO_VYPOLNENNYM_ZAMENAM_VALUE"];
        }
    } else {
        echo "Дата начала больше даты окончания\n\r";
    }
}
pre(array('arVacationsStartUsers' => $arVacationsStartUsers, 'arVacationsEndUsers' => $arVacationsEndUsers, '$arUsersGoingVacation' => $arUsersGoingVacation, '$arUsersReturnVacation' => $arUsersReturnVacation));

$arNeededTypes = array('PARAMETERS', 'VARIABLES', 'CONSTANTS'); //Массив чисто для удобства
//Выполняем замену пользователей, которые вернулись с отпуска
if(!empty($arVacationsEndUsers)) {
    //pre(array('$arVacationsEndUsers' => $arVacationsEndUsers));
    $arTotalHistoryElemID = [];
    $arResult = [];
    foreach($arVacationsEndUsers as $elemID => $arHistoryElemID){
        $arTotalHistoryElemID = array_merge($arTotalHistoryElemID, $arHistoryElemID);
    }
    //pre(array('$arTotalHistoryElemID' => $arTotalHistoryElemID));
    $el = new CIBlockElement();
    $rsVacationReplaceHistory = $el->GetList(
        array("ID" => "DESC"),
        array("IBLOCK_ID" => GetIBlockIDByCode("history_replacements_vacations_BP"), "ID" => $arTotalHistoryElemID),
        false,
        false,
        array(
            "ID",
            "NAME",
            "PROPERTY_ID_BP",
            "PROPERTY_PARAMETERS",
            "PROPERTY_VARIABLES",
            "PROPERTY_CONSTANTS",
            "PROPERTY_SOTRUDNIK_USHEDSHIY_V_OTPUSK",
            "PROPERTY_SOTRUDNIK_ZAMENYAYUSHCHIY",
        )
    );
    while($arVacationReplaceHistory = $rsVacationReplaceHistory->GetNext()) {
        $bpID = $arVacationReplaceHistory["PROPERTY_ID_BP_VALUE"];
        $arResult[$bpID][$arVacationReplaceHistory["ID"]]["SOTRUDNIK_USHEDSHIY_V_OTPUSK"] = "user_".$arVacationReplaceHistory["PROPERTY_SOTRUDNIK_USHEDSHIY_V_OTPUSK_VALUE"];
        $arResult[$bpID][$arVacationReplaceHistory["ID"]]["SOTRUDNIK_ZAMENYAYUSHCHIY"] = "user_".$arVacationReplaceHistory["PROPERTY_SOTRUDNIK_ZAMENYAYUSHCHIY_VALUE"];
        foreach($arNeededTypes as $paramType){
            if(!empty($arVacationReplaceHistory["PROPERTY_".$paramType."_VALUE"])) {
                $arResult[$bpID][$arVacationReplaceHistory["ID"]]["VARIABLE_TYPE"][$paramType] = $arVacationReplaceHistory["PROPERTY_".$paramType."_VALUE"];
            }
        }
    }
    //pre(array('$arResult' => $arResult));
    $loader = CBPWorkflowTemplateLoader::GetLoader();
    $runtime = CBPRuntime::GetRuntime();
    $runtime->StartRuntime();
    $DocumentService = $runtime->GetService("DocumentService");
    $arNeededTypes = array('PARAMETERS', 'VARIABLES', 'CONSTANTS'); //Массив чисто для удобства
    $arAddedElemID = []; // Массив с ID добавленных элементов в историю изменений.

    //Получаем данные по БП
    $arBpID = array_keys($arResult);
    //pre(array('$arBpID' => $arBpID));
    $dbTemplatesList = CBPWorkflowTemplateLoader::GetList(
        array("ID" => "DESC"),
        array("ID" => $arBpID),
        false,
        false,
        array('ID', 'DOCUMENT_TYPE', 'NAME', 'PARAMETERS', 'VARIABLES', 'CONSTANTS')
    );
    while($arTemplate = $dbTemplatesList->GetNext()) {
        $bpID = $arTemplate["ID"];
        $neededUpdate = false;
        $arUpdateFields = [];
        //if ($bpID == 48) {
            echo "<b>" . $arTemplate["NAME"] . "</b>\n\r";
            /*pre(
                array(
                    '$arTemplate[PARAMETERS]' => $arTemplate["PARAMETERS"],
                    '$arTemplate[VARIABLES]' => $arTemplate["VARIABLES"],
                    '$arTemplate[CONSTANTS]' => $arTemplate["CONSTANTS"],
                )
            );*/
            $arUsers = [];
            $arUpdateFields = []; //Массив с данными БП, которые нужно перезаписать
            $arBpUpdateHistory = $arResult[$bpID];

            //Проверяем на совпадение, пользователей которых нужно заменить
            foreach($arBpUpdateHistory as $historyElemID => $arHistory){
                foreach($arHistory["VARIABLE_TYPE"] as $variableType => $arVariables){
                    $neededUpdate = false;
                    foreach($arVariables as $variableName){
                        if(is_array($arTemplate[$variableType][$variableName]["Default"])){
                            foreach($arTemplate[$variableType][$variableName]["Default"] as $key => $userID) {
                                if($userID == $arHistory["SOTRUDNIK_ZAMENYAYUSHCHIY"]) {
                                    if(!isset($arUpdateFields[$variableType])){
                                        $arUpdateFields[$variableType] = $arTemplate[$variableType];
                                    };
                                    $arUpdateFields[$variableType][$variableName]["Default"][$key] = $arHistory["SOTRUDNIK_USHEDSHIY_V_OTPUSK"];
                                    $neededUpdate = true;
                                }
                            }
                        } else {
                            if(!isset($arUpdateFields[$variableType])){
                                $arUpdateFields[$variableType] = $arTemplate[$variableType];
                            };
                            $arUpdateFields[$variableType][$variableName]["Default"] = $arHistory["SOTRUDNIK_USHEDSHIY_V_OTPUSK"];
                            $neededUpdate = true;
                        }

                        if($neededUpdate){
                            //Получаем текстовое значение для выбранных пользователей
                            $arUpdateFields[$variableType][$variableName]['Default_printable'] = $DocumentService->GetFieldInputValuePrintable(
                                $arTemplate["DOCUMENT_TYPE"],
                                $arUpdateFields[$variableType][$variableName],
                                $arUpdateFields[$variableType][$variableName]["Default"]
                            );
                        }
                    }
                }
            }
            if(!empty($arUpdateFields)) {
                pre(array('$arUpdateFields' => $arUpdateFields));
                if ($loader->UpdateTemplate($bpID, $arUpdateFields)) {
                    echo "Успешно обновлен БП[$bpID]\n\r";
                } else {
                    echo "Ошибка обновления константы БП[$bpID]\n\r";
                }
            }
        //}

        //Проверяем на совпадение, пользователей которых нужно заменить
    }

    foreach($arVacationsEndUsers as $elemID => $arElemData){
        $el->SetPropertyValuesEx($elemID, GetIBlockIDByCode("replace_list_for_holidays"), array("ZAMENA_VYPOLNENA" => CONST_RETURN_ZAMENA_ENUM_ID));
        foreach($arElemData as $historyElemID) {
            $res = $el->Update($historyElemID, Array("ACTIVE" => "N"));
        }
    }
}


//Если есть пользователи ушедшие в отпуск, которых нужно заменить.
if(!empty($arVacationsStartUsers)) {
    $el = new CIBlockElement();
    $loader = CBPWorkflowTemplateLoader::GetLoader();
    $runtime = CBPRuntime::GetRuntime();
    $runtime->StartRuntime();
    $DocumentService = $runtime->GetService("DocumentService");
    $arAddedElemID = []; // Массив с ID добавленных элементов в историю изменений.

    //Получаем данные по БП
    $dbTemplatesList = CBPWorkflowTemplateLoader::GetList(
        array("ID" => "DESC"),
        array(),
        false,
        false,
        array('ID', 'DOCUMENT_TYPE', 'NAME', 'PARAMETERS', 'VARIABLES', 'CONSTANTS')
    );
    while($arTemplate = $dbTemplatesList->GetNext()) {
        $bpID = $arTemplate["ID"];
        $bpName = $arTemplate["NAME"];
        $arUsers = [];
        $arUpdateFields = []; //Массив с данными БП, которые нужно перезаписать
        $arUpdateHistory = []; //Массив для истории изменений, чтобы правильно выполнить обратную замену
        //Проверяем на совпадение, пользователей которых нужно заменить
        foreach ($arNeededTypes as $paramName) {
            foreach ($arTemplate[$paramName] as $variableName => $arVariable) {
                if ( ($arVariable["Type"] == 'user' || $arVariable["Type"] == 'S:employee') && !empty($arVariable["Default"])) {
                    $updateUser = false;
                    if(is_array($arVariable["Default"])){
                        $result = array_intersect($arVariable["Default"], $arUsersGoingVacation);
                        if(sizeOf($result) > 0){
                            //Перебираем текущих пользователей и ищем совпадение в списке сотрудников уходящих в отпуск
                            foreach($arVariable["Default"] as $key => $userVal) {
                                foreach($arVacationsStartUsers as $elemID => $arVal){
                                    if($userVal == $arVal["VACATIONING_EMPLOYEE"]){
                                        if(!isset($arUpdateFields[$paramName])){
                                            $arUpdateFields[$paramName] = $arTemplate[$paramName];
                                        };
                                        $arUpdateHistory[$elemID][$bpID][$paramName][] = $variableName;
                                        $arUpdateFields[$paramName][$variableName]["Default"][$key] = $arVal["SUBSTITUTE_EMPLOYEE"];
                                    }
                                }
                            }
                            $updateUser = true;
                        }
                    } else {
                        if(in_array($arVariable["Default"], $arUsersGoingVacation)){
                            //Перебираем сотрудников уходящих в отпуск ищем совпадение c $arVariable["Default"]
                            foreach($arVacationsStartUsers as $elemID => $arVal){
                                if($arVariable["Default"] == $arVal["VACATIONING_EMPLOYEE"]){
                                    if(!isset($arUpdateFields[$paramName])){
                                        $arUpdateFields[$paramName] = $arTemplate[$paramName];
                                    };
                                    $arUpdateHistory[$elemID][$bpID][$paramName][] = $variableName;
                                    $arUpdateFields[$paramName][$variableName]["Default"] = $arVal["SUBSTITUTE_EMPLOYEE"];
                                }
                            }
                            $updateUser = true;
                        }
                    }
                    if($updateUser){
                        //Получаем текстовое значение для выбранных пользователей
                        $arUpdateFields[$paramName][$variableName]['Default_printable'] = $DocumentService->GetFieldInputValuePrintable(
                            $arTemplate["DOCUMENT_TYPE"],
                            $arUpdateFields[$paramName][$variableName],
                            $arUpdateFields[$paramName][$variableName]["Default"]
                        );
                    }
                }
            }
        }
        if(!empty($arUpdateFields)) {
            //pre(array('$arUpdateFields' => $arUpdateFields));
            if ($loader->UpdateTemplate($bpID, $arUpdateFields)) {
                echo "Успешно обновлена константа БП[$bpID]\n\r";
                foreach($arUpdateHistory as $elemID => $arBP){
                    if(!empty($arBP[$bpID])) {
                        $arProp = [];
                        $vacationingEmployee = $arVacationsStartUsers[$elemID]['VACATIONING_EMPLOYEE'];
                        $substituteEmployee = $arVacationsStartUsers[$elemID]['SUBSTITUTE_EMPLOYEE'];
                        $arElemFields = array(
                            "ACTIVE" => "Y",
                            "IBLOCK_ID" => GetIBlockIDByCode("history_replacements_vacations_BP"),
                            "NAME" => "Замена в БП \"$bpName\"",
                        );
                        $arProp["ID_BP"] = $bpID;
                        $arProp["SOTRUDNIK_USHEDSHIY_V_OTPUSK"] = preg_replace("/[^0-9]/", '', $vacationingEmployee);
                        $arProp["SOTRUDNIK_ZAMENYAYUSHCHIY"] = preg_replace("/[^0-9]/", '', $substituteEmployee);;
                        foreach($arBP[$bpID] as $paramType => $arVariableName){
                            $arProp[$paramType] = $arVariableName;
                        }
                        $arElemFields["PROPERTY_VALUES"] = $arProp;
                        if($PRODUCT_ID = $el->Add($arElemFields)) {
                            $arAddedElemID[$elemID][] = $PRODUCT_ID;
                        } else {
                            echo "Error: ".$el->LAST_ERROR."\n\r";
                        }
                    }
                }
            } else {
                echo "Ошибка обновления константы БП[$bpID]\n\r";
            }
        }
        //}
    }
    pre(array('$arAddedElemID' => $arAddedElemID));
    foreach($arAddedElemID as $elemID => $arElemIDHistory) {
        $el->SetPropertyValuesEx($elemID, GetIBlockIDByCode("replace_list_for_holidays"), array("DANNYE_PO_VYPOLNENNYM_ZAMENAM" => $arElemIDHistory, "ZAMENA_VYPOLNENA" => CONST_ZAMENA_VYPOLNENA_ENUM_ID));
    }
}
?>