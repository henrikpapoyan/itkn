<?php
/*
 * Запуск из под крон раз в минуту.
 * Выполняет проверку реквизитов компании.
 * агент, который срабатывает раз в 1 минуту. Делает выборку компаний за последние 24 часа минус 1 минута от сейчас, у которых не указаны реквизиты и создает их
 */

include_once(__DIR__ . "/../../.config.php");

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('CHK_EVENT', true);
ini_set('memory_limit', '-1');

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
global $DB;
global $USER_FIELD_MANAGER;
global $USER;
$USER->Authorize(372);
$time_start = microtime(true);
$periodStart = date("Y-m-d H:i:s", strtotime('-24 hours', time()));
$periodEnd = date("Y-m-d H:i:s", strtotime('-1 minutes', time()));
$err_mess = "Line: ";
$strSql = 'SELECT
      comp.ID,
      comp.TITLE
    FROM
      b_crm_company comp
    LEFT JOIN b_crm_requisite req
    ON comp.ID = req.ENTITY_ID
    WHERE 
      req.ENTITY_ID IS NULL AND comp.DATE_CREATE > "'.$periodStart.'" AND comp.DATE_CREATE <= "'.$periodEnd.'"';
$res = $DB->Query($strSql, false, $err_mess.__LINE__);
while ($arRes= $res->fetch())
{
    $companyID = $arRes["ID"];
    $inn = $USER_FIELD_MANAGER->GetUserFieldValue('CRM_COMPANY', 'UF_CRM_INN', $companyID);
    if (!empty($inn)) {
        $kpp = $USER_FIELD_MANAGER->GetUserFieldValue('CRM_COMPANY', 'UF_CRM_KPP', $companyID);
        $arRequisite = array();
        $requisite = new \Bitrix\Crm\EntityRequisite();
        $kontragentInfo = GetKontragentInfo($inn);
        if(!empty($kontragentInfo[0]["fields"])) {
            $arRequisite = $kontragentInfo[0]["fields"];
        } else {
            $arRequisite["RQ_INN"] = $inn;
            if(strlen($inn) == 10)
            {
                $arRequisite['RQ_COMPANY_NAME'] = $arRes['TITLE'];
                $arRequisite['RQ_KPP'] = $kpp;
            }
        }
        if(strlen($inn) == 10)
        {
            $arRequisite["PRESET_ID"] = 1;
            $arRequisite["NAME"] = "Организация";
        }
        if(strlen($inn) == 12)
        {
            $arRequisite["PRESET_ID"] = 2;
            $arRequisite["NAME"] = "ИП";
        }
        $arRequisite["ENTITY_ID"] = $companyID;
        $arRequisite["ENTITY_TYPE_ID"] = CCrmOwnerType::Company;
        $result = $requisite->add($arRequisite);
    }
}
$time_end = microtime(true);
$time = $time_end - $time_start;
echo "Затрачено $time секунд\n";
?>