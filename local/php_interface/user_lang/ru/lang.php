<?php
/**
 * User: German
 * Date: 10.05.2017
 * Time: 15:55
 */

//$MESS["/local/components/bitrix/crm.control_panel/lang/ru/component.php"]["CRM_CTRL_PANEL_ITEM_DEAL"] = "Проекты222";
$MESS["/local/components/itk/crm.control_panel/lang/ru/component.php"]["CRM_CTRL_PANEL_ITEM_COMPANY"] = "Контрагенты";
$MESS["/local/components/itk/crm.control_panel/lang/ru/component.php"]["CRM_CTRL_PANEL_ITEM_COMPANY_TITLE"] = "Список контрагентов";
$MESS["/local/components/itk/crm.control_panel/templates/.default/lang/ru/template.php"]["CRM_CONTROL_PANEL_SEARCH_PLACEHOLDER"] = "Искать контрагента, контакт, лид, сделку...";

$MESS["/bitrix/components/bitrix/crm.control_panel/lang/ru/component.php"]["CRM_CTRL_PANEL_ITEM_COMPANY"] = "Контрагенты";
$MESS["/bitrix/components/bitrix/crm.control_panel/lang/ru/component.php"]["CRM_CTRL_PANEL_ITEM_COMPANY_TITLE"] = "Список контрагентов";
$MESS["/bitrix/components/bitrix/crm.control_panel/templates/.default/lang/ru/template.php"]["CRM_CONTROL_PANEL_SEARCH_PLACEHOLDER"] = "Искать контрагента  , контакт, лид, сделку...";

$MESS["/bitrix/components/bitrix/mobile.crm.lead.list/lang/ru/component.php"]["M_CRM_LEAD_LIST_DECLINE"] = "Закрыть"; //"Забраковать"
$MESS["/bitrix/modules/crm/lang/ru/classes/general/crm_view_helper.php"]["CRM_LEAD_STATUS_MANAGER_FAILURE_TTL"] = "Закрыть"; //"Забраковать"
$MESS["/local/modules/crm/lang/ru/classes/general/crm_view_helper.php"]["CRM_LEAD_STATUS_MANAGER_FAILURE_TTL"] = "Закрыть"; //"Забраковать"

$MESS["/bitrix/modules/crm/lang/ru/lib/conversion/leadconversionscheme.php"]["CRM_LEAD_CONV_DEAL_CONTACT_COMPANY"] = "Сделка + Контакт + Контрагент";//"Сделку + Контакт + Компанию";
$MESS["/bitrix/modules/crm/lang/ru/lib/conversion/leadconversionscheme.php"]["CRM_LEAD_CONV_DEAL_CONTACT"] = "Сделка + Контакт";//"Сделку + Контакт";
$MESS["/bitrix/modules/crm/lang/ru/lib/conversion/leadconversionscheme.php"]["CRM_LEAD_CONV_DEAL_COMPANY"] = "Сделка + Контрагент";//"Сделку + Компанию";
$MESS["/bitrix/modules/crm/lang/ru/lib/conversion/leadconversionscheme.php"]["CRM_LEAD_CONV_DEAL"] = "Сделка";//"Сделку";
$MESS["/bitrix/modules/crm/lang/ru/lib/conversion/leadconversionscheme.php"]["CRM_LEAD_CONV_CONTACT_COMPANY"] = "Контакт + Контрагент";//"Контакт + Компанию";
$MESS["/bitrix/modules/crm/lang/ru/lib/conversion/leadconversionscheme.php"]["CRM_LEAD_CONV_COMPANY"] = "Контрагент";//"Компанию";

$MESS["/local/modules/crm/lang/ru/lib/conversion/leadconversionscheme.php"]["CRM_LEAD_CONV_DEAL_CONTACT_COMPANY"] = "Сделка + Контакт + Контрагент";//"Сделку + Контакт + Компанию";
$MESS["/local/modules/crm/lang/ru/lib/conversion/leadconversionscheme.php"]["CRM_LEAD_CONV_DEAL_CONTACT"] = "Сделка + Контакт";//"Сделку + Контакт";
$MESS["/local/modules/crm/lang/ru/lib/conversion/leadconversionscheme.php"]["CRM_LEAD_CONV_DEAL_COMPANY"] = "Сделка + Контрагент";//"Сделку + Компанию";
$MESS["/local/modules/crm/lang/ru/lib/conversion/leadconversionscheme.php"]["CRM_LEAD_CONV_DEAL"] = "Сделка";//"Сделку";
$MESS["/local/modules/crm/lang/ru/lib/conversion/leadconversionscheme.php"]["CRM_LEAD_CONV_CONTACT_COMPANY"] = "Контакт + Контрагент";//"Контакт + Компанию";
$MESS["/local/modules/crm/lang/ru/lib/conversion/leadconversionscheme.php"]["CRM_LEAD_CONV_COMPANY"] = "Контрагент";//"Компанию";

$MESS["/bitrix/modules/intranet/lang/ru/public/crm/.left.menu_ext.php"]["MENU_CRM_COMPANY"] = "Контрагенты";

$MESS["/local/components/bitrix/crm.company.list/lang/ru/component.php"]["CRM_COMPANY_NAV_TITLE_LIST_SHORT"] = "Контрагенты";

if(substr($_SERVER['REQUEST_URI'], 0, 11) == '/crm/order/') {
    $MESS["/bitrix/modules/crm/lang/ru/classes/general/crm_view_helper.php"]["CRM_LEAD_STATUS_MANAGER_DLG_TTL"] = "Выберите результат, с которым будет завершена обработка заказа.";
    $MESS["/bitrix/modules/crm/lang/ru/classes/general/crm_view_helper.php"]["CRM_LEAD_STATUS_MANAGER_CONVERTED_STEP_HINT"] = "Завершить обработку заказа";
    $MESS["/bitrix/templates/bitrix24/components/bitrix/crm.lead.list/lead_list_itk/lang/ru/template.php"]["CRM_LEAD_SHOW_TITLE"] = "Просмотреть заказ";
    $MESS["/bitrix/templates/bitrix24/components/bitrix/crm.lead.list/lead_list_itk/lang/ru/template.php"]["CRM_LEAD_SHOW"] = "Просмотреть заказ";
}