<?php
//GK[
    function OnTaskAddHandler($id, $arFields)
    {

        if ($arFields['TITLE'] == 'Разработать проектную документацию') {
            CModule::IncludeModule("tasks");
            CModule::IncludeModule("checklist");
            $oTask     = CTaskItem::getInstanceFromPool($id, $arFields["RESPONSIBLE_ID"]);
            $checkList = array(
                "Установить срок выполнения",
                "Паспорт проекта",
                "Дорожную карту проекта",
                "Бюджет проекта",
                "Ресурсный план проекта"
            );
            foreach ($checkList as $list) {
                $arFields       = array(
                    'TITLE' => $list,
                );
                $oCheckListItem = CTaskCheckListItem::add($oTask, $arFields);
            }
        }

        if ($arFields['TITLE'] == 'Произвести оплату счетов по утвержденному платежному календарю') {
            CModule::IncludeModule("tasks");
            CModule::IncludeModule("iblock");
            CModule::IncludeModule("checklist");
            $oTask = CTaskItem::getInstanceFromPool($id, $arFields["RESPONSIBLE_ID"]);

            $arFilter = Array("IBLOCK_ID" => 45, "ACTIVE" => "Y");
            $res      = CIBlockElement::GetList(Array(), $arFilter, false, false, Array());
            while ($ob = $res->GetNextElement()) {
                $arItem               = $ob->GetFields();
                $arItem["PROPERTIES"] = $ob->GetProperties();
                $arResult["SPISOK"][] = $arItem;
            }

            foreach ($arResult["SPISOK"] as $item) {
                if ($item['PROPERTIES']['SOGLASOVANIE_KALENDARYA']['VALUE'] == "soglas") {
                    $checkList[] = "№" . $item["ID"] . " " . $item["NAME"] . " от " .
                                   $item['PROPERTIES']['KTO_SOZDAL_ZAYAVKU']['VALUE'];
                }
            }

            foreach ($checkList as $list) {
                $arFields       = array(
                    'TITLE' => $list,
                );
                $oCheckListItem = CTaskCheckListItem::add($oTask, $arFields);
            }
        }

        return true;
    }

    #+ добавление всех новых задач в нужную группу от департамента развития продукта
    function OnBeforeTaskAddHandler(&$arFields)
    {
        $logger = Logger::getLogger('OnBeforeTaskAddHandler');
        $logger->Log('$arFields[] =>'. print_r($arFields, true));
        //Добавляем в НАБЛЮДАТЕЛИ выбранный отдел, группу
        if(!empty($arFields["AUDITORS"]) && !empty($arFields["AUDITOR_TYPE"])){
            $arNewAuditors = [];
            foreach($arFields["AUDITORS"] as $key => $entityID){
                //Если выбран отдел
                $entityType = $arFields["AUDITOR_TYPE"][$key];
                if($entityType == "DR") {
                    $arDepartments = array($entityID);
                    $departmentEmployees = CIntranetUtils::GetDepartmentEmployees($arDepartments, $bRecursive = true, $bSkipSelf = false, $onlyActive = 'Y');
                    while($arEmployees = $departmentEmployees->GetNext()) {
                        if (!in_array($arEmployees['ID'], $arNewAuditors)){
                            $arNewAuditors[] = $arEmployees['ID'];
                        }
                    }
                } elseif ($entityType == "SG") {
                    if(CModule::IncludeModule("socialnetwork")){
                        $rsUsers = CSocNetUserToGroup::GetList(
                            array("ID" => "ASC"),
                            array("GROUP_ID" => $entityID),
                            false,
                            false,
                            array("ID", "USER_ID", "USER_ACTIVE")
                        );

                        while($arGroupUser = $rsUsers->Fetch()) {
                            //pre($arGroupUser);
                            if($arGroupUser['USER_ACTIVE'] == "Y" && !in_array($arGroupUser['USER_ID'], $arNewAuditors)){
                                $arNewAuditors[] = $arGroupUser['USER_ID'];
                            }
                        }
                    }
                } else {
                    if (!in_array($entityID, $arNewAuditors)){
                        $arNewAuditors[] = $entityID;
                    }
                }
            }
            $arFields["AUDITORS"] = $arNewAuditors;
        }

        //Добавляем в СОИСПОЛНИТЕЛИ выбранный отдел, группу
        if(!empty($arFields["ACCOMPLICES"]) && !empty($arFields["ACCOMPLICE_TYPE"])){
            $arNewAccomplices = [];
            foreach($arFields["ACCOMPLICES"] as $key => $entityID){
                //Если выбран отдел
                $entityType = $arFields["ACCOMPLICE_TYPE"][$key];
                if($entityType == "DR") {
                    $arDepartments = array($entityID);
                    $departmentEmployees = CIntranetUtils::GetDepartmentEmployees($arDepartments, $bRecursive = true, $bSkipSelf = false, $onlyActive = 'Y');
                    while($arEmployees = $departmentEmployees->GetNext()){
                        if (!in_array($arEmployees['ID'], $arNewAccomplices)){
                            $arNewAccomplices[] = $arEmployees['ID'];
                        }
                    }
                } elseif ($entityType == "SG") {
                    if(CModule::IncludeModule("socialnetwork")){
                        $rsUsers = CSocNetUserToGroup::GetList(
                            array("ID" => "ASC"),
                            array("GROUP_ID" => $entityID),
                            false,
                            false,
                            array("ID", "USER_ID", "USER_ACTIVE")
                        );

                        while($arGroupUser = $rsUsers->Fetch()) {
                            //pre($arGroupUser);
                            if($arGroupUser['USER_ACTIVE'] == "Y" && !in_array($arGroupUser['USER_ID'], $arNewAccomplices)){
                                $arNewAccomplices[] = $arGroupUser['USER_ID'];
                            }
                        }
                    }
                } else {
                    if (!in_array($entityID, $arNewAccomplices)) {
                        $arNewAccomplices[] = $entityID;
                    }
                }
            }
            $arFields["ACCOMPLICES"] = $arNewAccomplices;
        }

        $rsUser = CUser::GetByID($arFields["CREATED_BY"]);
        $arUser = $rsUser->Fetch();
        if (in_array(86, $arUser["UF_DEPARTMENT"])) {
            if ($arFields["GROUP_ID"] == 0) {
                $arFields["GROUP_ID"] = 7;
            }
        }
        if ($arFields["RESPONSIBLE_ID"] != $arFields["CREATED_BY"]) {
            $userManager          = getBitrixUserManager($arFields["RESPONSIBLE_ID"]);
            $arFields["AUDITORS"] = array_merge($arFields["AUDITORS"], $userManager);
        }
    }

    #- добавление всех новых задач в нужную группу от департамента развития продукта
    //GK]

    function OnBeforeTaskUpdateHandler($taskId, &$arTask, &$arFields)
    {
        //Добавляем в НАБЛЮДАТЕЛИ выбранный отдел, группу
        if(!empty($arTask["AUDITORS"]) && !empty($arTask["AUDITOR_TYPE"])){
            $arNewAuditors = [];
            foreach($arTask["AUDITORS"] as $key => $entityID){
                //Если выбран отдел
                $entityType = $arTask["AUDITOR_TYPE"][$key];
                if($entityType == "DR") {
                    $arDepartments = array($entityID);
                    $departmentEmployees = CIntranetUtils::GetDepartmentEmployees($arDepartments, $bRecursive = true, $bSkipSelf = false, $onlyActive = 'Y');
                    while($arEmployees = $departmentEmployees->GetNext()) {
                        if (!in_array($arEmployees['ID'], $arNewAuditors)){
                            $arNewAuditors[] = $arEmployees['ID'];
                        }
                    }
                } elseif ($entityType == "SG") {
                    if(CModule::IncludeModule("socialnetwork")){
                        $rsUsers = CSocNetUserToGroup::GetList(
                            array("ID" => "ASC"),
                            array("GROUP_ID" => $entityID),
                            false,
                            false,
                            array("ID", "USER_ID", "USER_ACTIVE")
                        );

                        while($arGroupUser = $rsUsers->Fetch()) {
                            //pre($arGroupUser);
                            if($arGroupUser['USER_ACTIVE'] == "Y" && !in_array($arGroupUser['USER_ID'], $arNewAuditors)){
                                $arNewAuditors[] = $arGroupUser['USER_ID'];
                            }
                        }
                    }
                } else {
                    if (!in_array($entityID, $arNewAuditors)){
                        $arNewAuditors[] = $entityID;
                    }
                }
            }
            $arTask["AUDITORS"] = $arNewAuditors;
        }

        //Добавляем в СОИСПОЛНИТЕЛИ выбранный отдел, группу
        if(!empty($arTask["ACCOMPLICES"]) && !empty($arTask["ACCOMPLICE_TYPE"])){
            $arNewAccomplices = [];
            foreach($arTask["ACCOMPLICES"] as $key => $entityID){
                //Если выбран отдел
                $entityType = $arTask["ACCOMPLICE_TYPE"][$key];
                if($entityType == "DR") {
                    $arDepartments = array($entityID);
                    $departmentEmployees = CIntranetUtils::GetDepartmentEmployees($arDepartments, $bRecursive = true, $bSkipSelf = false, $onlyActive = 'Y');
                    while($arEmployees = $departmentEmployees->GetNext()){
                        if (!in_array($arEmployees['ID'], $arNewAccomplices)){
                            $arNewAccomplices[] = $arEmployees['ID'];
                        }
                    }
                } elseif ($entityType == "SG") {
                    if(CModule::IncludeModule("socialnetwork")){
                        $rsUsers = CSocNetUserToGroup::GetList(
                            array("ID" => "ASC"),
                            array("GROUP_ID" => $entityID),
                            false,
                            false,
                            array("ID", "USER_ID", "USER_ACTIVE")
                        );

                        while($arGroupUser = $rsUsers->Fetch()) {
                            //pre($arGroupUser);
                            if($arGroupUser['USER_ACTIVE'] == "Y" && !in_array($arGroupUser['USER_ID'], $arNewAccomplices)){
                                $arNewAccomplices[] = $arGroupUser['USER_ID'];
                            }
                        }
                    }
                } else {
                    if (!in_array($entityID, $arNewAccomplices)) {
                        $arNewAccomplices[] = $entityID;
                    }
                }
            }
            $arTask["ACCOMPLICES"] = $arNewAccomplices;
        }

        //Not AJAX Update
        if (isset($arTask["RESPONSIBLE_ID"]) && isset($arTask["TITLE"])) {

            if ($arTask["RESPONSIBLE_ID"] == $arTask["CREATED_BY"]) {

                $arTask["AUDITORS"][] = $arFields["RESPONSIBLE_ID"];

                if (in_array($arTask["RESPONSIBLE_ID"], $arTask["AUDITORS"])) {
                    $arTask["AUDITORS"] = array_diff($arTask["AUDITORS"], array($arTask["RESPONSIBLE_ID"]));
                }

                return;
            }

            $newUserManager = getBitrixUserManager($arTask["RESPONSIBLE_ID"]);

            if ( ! is_array($arTask["AUDITORS"])) {
                $arTask["AUDITORS"] = [];
            }

            $cropOldAuditor = $arTask["AUDITORS"];
            $addNewAuditor  = array_diff($newUserManager, $cropOldAuditor);
            $mergedAuditors = array_merge($cropOldAuditor, $addNewAuditor);

            $arTask["AUDITORS"] = ! empty($mergedAuditors) ? $mergedAuditors : $newUserManager;

            $arTask["AUDITORS"][] = $arFields["RESPONSIBLE_ID"];

            if (in_array($arTask["RESPONSIBLE_ID"], $arTask["AUDITORS"])) {
                $arTask["AUDITORS"] = array_diff($arTask["AUDITORS"], array($arTask["RESPONSIBLE_ID"]));
            }
            $arTask["AUDITORS"] = array_unique($arTask["AUDITORS"]);
        }

        //AJAX update
        if ( ! isset($arTask["TITLE"]) && (isset($arTask["RESPONSIBLE_ID"]) || isset($arTask["AUDITORS"]))) {

            if (empty($arTask["RESPONSIBLE_ID"])) {
                return;
            }

            if (empty($arTask["RESPONSIBLE_ID"]) || $arTask["RESPONSIBLE_ID"] == $arFields["CREATED_BY"]) {

                $oldUserManager = getBitrixUserManager($arFields["RESPONSIBLE_ID"]);

                if ( ! is_array($arTask["AUDITORS"])) {
                    $arTask["AUDITORS"] = $arFields["AUDITORS"];
                }

                $cropOldAuditor = $arTask["AUDITORS"];

                if ($arTask["RESPONSIBLE_ID"] == $arFields["CREATED_BY"]) {
                    $arTask["AUDITORS"]   = $cropOldAuditor;
                    $arTask["AUDITORS"][] = $arFields["RESPONSIBLE_ID"];
                } else {
                    $addNewAuditor        = array_diff($oldUserManager, $cropOldAuditor);
                    $mergedAuditors       = array_merge($cropOldAuditor, $addNewAuditor);
                    $arTask["AUDITORS"]   = ! empty($mergedAuditors) ? $mergedAuditors : $oldUserManager;
                    $arTask["AUDITORS"][] = $arFields["RESPONSIBLE_ID"];
                }

                if (in_array($arTask["RESPONSIBLE_ID"], $arTask["AUDITORS"])) {
                    $arTask["AUDITORS"] = array_diff($arTask["AUDITORS"], array($arTask["RESPONSIBLE_ID"]));
                }

                return;
            }

            $newUserManager   = getBitrixUserManager($arTask["RESPONSIBLE_ID"]);
            $newUserManager[] = $arFields["RESPONSIBLE_ID"];


            $cropOldAuditor = $arFields["AUDITORS"];

            $addNewAuditor  = array_diff($newUserManager, $cropOldAuditor);
            $mergedAuditors = array_merge($cropOldAuditor, $addNewAuditor);

            $arTask["AUDITORS"] = ! empty($mergedAuditors) ? $mergedAuditors : $newUserManager;

            if (in_array($arTask["RESPONSIBLE_ID"], $arTask["AUDITORS"])) {
                $arTask["AUDITORS"] = array_diff($arTask["AUDITORS"], array($arTask["RESPONSIBLE_ID"]));
            }
            $arTask["AUDITORS"] = array_unique($arTask["AUDITORS"]);
        }
    }