<?php
class kktHandler{
    function relationshipAgentKontragent ($agentID = 0, $kontragentID = 0) {
		return false;
        CModule::IncludeModule("iblock");
        $el = new CIBlockElement();

        //Типы взаимосвязей
        $rsLinkType = $el->GetList(
            array("ID"=>"ASC"),
            array("IBLOCK_ID" => GetIBlockIDByCode("company_link_type"), "ACTIVE" => "Y"),
            false,
            false,
            array("ID", "NAME")
        );
        $materinskayaID = 0;
        $dochernayaID = 0;
        while($arLinkType = $rsLinkType->GetNext()){
            if($arLinkType["NAME"]=="Материнская"){
                $materinskayaID = $arLinkType["ID"];
            }
            if($arLinkType["NAME"]=="Дочерняя"){
                $dochernayaID = $arLinkType["ID"];
            }
        }

        //Добавление взаимосвязи для АГЕНТА
        $dbDublikate = $el->GetList(
            array("ID" => "ASC"),
            array(
                "IBLOCK_ID" => GetIBlockIDByCode("company_interrelationships"),
                "ACTIVE" => "Y",
                "PROPERTY_MAIN_COMPANY" => $agentID
            ),
            false,
            false,
            array(
                "ID",
                "IBLOCK_ID",
                "NAME",
                "PROPERTY_MAIN_COMPANY",
                "PROPERTY_COMPANY_LINK",
                "PROPERTY_COMPANY_LINK_TYPE"
            )
        );
        if ($dbDublikate->SelectedRowsCount() == 0) {
            $PROP = array(
                "MAIN_COMPANY" => $agentID,
                "COMPANY_LINK" => array($kontragentID),
                "COMPANY_LINK_TYPE" => array(intval($dochernayaID)),
            );
            $arLoadProductArray = Array(
                "IBLOCK_SECTION_ID" => false,
                "IBLOCK_ID" => GetIBlockIDByCode("company_interrelationships"),
                "PROPERTY_VALUES" => $PROP,
                "NAME" => "Взаимосвязь для компании " . $agentID,
                "ACTIVE" => "Y",
            );
            $el->Add($arLoadProductArray);
        } else {
            $arInterrelationships = $dbDublikate->GetNext();
            $arCurCompanys = $arInterrelationships["PROPERTY_COMPANY_LINK_VALUE"];
            $arCurLinkTypes = $arInterrelationships["PROPERTY_COMPANY_LINK_TYPE_VALUE"];
            if (!in_array($kontragentID, $arCurCompanys)) {
                $arCurCompanys[] = $kontragentID;
                $arCurLinkTypes[] = intval($dochernayaID);
                CIBlockElement::SetPropertyValuesEx(
                    $arInterrelationships["ID"],
                    $arInterrelationships["IBLOCK_ID"],
                    array(
                        "COMPANY_LINK" => $arCurCompanys,
                        "COMPANY_LINK_TYPE" => intval($arCurLinkTypes),
                    )
                );
            }
        }

        //Добавление взаимосвязи для КОНТРАГЕНТА
        $dbDublikate = $el->GetList(
            array("ID" => "ASC"),
            array(
                "IBLOCK_ID" => GetIBlockIDByCode("company_interrelationships"),
                "ACTIVE" => "Y",
                "PROPERTY_MAIN_COMPANY" => $kontragentID
            ),
            false,
            false,
            array(
                "ID",
                "IBLOCK_ID",
                "NAME",
                "PROPERTY_MAIN_COMPANY",
                "PROPERTY_COMPANY_LINK",
                "PROPERTY_COMPANY_LINK_TYPE"
            )
        );
        if ($dbDublikate->SelectedRowsCount() == 0) {
            $PROP = array(
                "MAIN_COMPANY" => $kontragentID,
                "COMPANY_LINK" => $agentID,
                "COMPANY_LINK_TYPE" => intval($materinskayaID),
            );
            $arLoadProductArray = Array(
                "IBLOCK_SECTION_ID" => false,
                "IBLOCK_ID" => GetIBlockIDByCode("company_interrelationships"),
                "PROPERTY_VALUES" => $PROP,
                "NAME" => "Взаимосвязь для компании " . $kontragentID,
                "ACTIVE" => "Y",
            );
            $el->Add($arLoadProductArray);
        } else {
            $arInterrelationships = $dbDublikate->GetNext();
            $arCurCompanys = $arInterrelationships["PROPERTY_COMPANY_LINK_VALUE"];
            $arCurLinkTypes = $arInterrelationships["PROPERTY_COMPANY_LINK_TYPE_VALUE"];
            if (!in_array($agentID, $arCurCompanys)) {
                $arCurCompanys[] = $agentID;
                $arCurLinkTypes[] = $materinskayaID;
                CIBlockElement::SetPropertyValuesEx(
                    $arInterrelationships["ID"],
                    $arInterrelationships["IBLOCK_ID"],
                    array(
                        "COMPANY_LINK" => $arCurCompanys,
                        "COMPANY_LINK_TYPE" => intval($arCurLinkTypes),
                    )
                );
            }
        }
    }

    //Фунция для обновления данных КОНТРАГЕНТА по количеству ККТ (UF_KKT и UF_KKT2)
    function updateKktCountByContragent($contragentID = 0, $action = '')
    {
        $logger = Logger::getLogger('CompanyAjax','KKTHandler/updateKktCountByContragent.log');
        $USER_FIELD_MANAGER = new CAllUserTypeManager();

        //Получаем текущее количество ККТ (пользовательское свойство UF_KKT
        $kktCount = $USER_FIELD_MANAGER->GetUserFieldValue(
            "CRM_COMPANY",
            "UF_KKT",
            $contragentID
        );
        $kktCount = $kktCount != '' ? $kktCount : 0;

        //Получаем актуальные значение периодов для поля UF_KKT2
        $arKktPeriods = array();
        $CUserFieldEnum = new CUserFieldEnum();
        $resUserField = $CUserFieldEnum->GetList(array(), array("USER_FIELD_NAME" => "UF_KKT2"));
        while ($arUserField = $resUserField->GetNext()) {
            //echo "arKktPeriods = <pre>".print_r($arKktPeriods, true)."</pre>\n\r";
            if ($arUserField['~VALUE'] == "0>10") {
                $arKktPeriods["BEFORE_10"] = $arUserField['ID'];
            }
            if ($arUserField['~VALUE'] == "10>50") {
                $arKktPeriods["BEFORE_50"] = $arUserField['ID'];
            }
            if ($arUserField['~VALUE'] == "50>100") {
                $arKktPeriods["BEFORE_100"] = $arUserField['ID'];
            }
            if ($arUserField['~VALUE'] == "100>300") {
                $arKktPeriods["BEFORE_300"] = $arUserField['ID'];
            }
            if ($arUserField['~VALUE'] == "300>500") {
                $arKktPeriods["BEFORE_500"] = $arUserField['ID'];
            }
            if ($arUserField['~VALUE'] == "500>1000") {
                $arKktPeriods["BEFORE_1000"] = $arUserField['ID'];
            }
            if ($arUserField['~VALUE'] == ">1000") {
                $arKktPeriods["MORE_1000"] = $arUserField['ID'];
            }
        }

        //Получаем текущее значение пользовательского свойства UF_KKT2
        $kktPeriod = $USER_FIELD_MANAGER->GetUserFieldValue(
            "CRM_COMPANY",
            "UF_KKT2",
            $contragentID
        );
        if($kktPeriod == '')
        {
            $kktPeriod = $arKktPeriods["BEFORE_10"];
            if($kktCount > 10 && $kktCount <= 50)
            {
                $kktPeriod = $arKktPeriods["BEFORE_50"];
            }
            if($kktCount > 50 && $kktCount <= 100)
            {
                $kktPeriod = $arKktPeriods["BEFORE_100"];
            }
            if($kktCount > 100 && $kktCount <= 300)
            {
                $kktPeriod = $arKktPeriods["BEFORE_300"];
            }
            if($kktCount > 300 && $kktCount <= 500)
            {
                $kktPeriod = $arKktPeriods["BEFORE_500"];
            }
            if($kktCount > 500 && $kktCount <= 1000)
            {
                $kktPeriod = $arKktPeriods["BEFORE_1000"];
            }
            if($kktCount > 1000)
            {
                $kktPeriod = $arKktPeriods["MORE_1000"];
            }
        }
        $logger->log('updateKktCountByContragent $contragentID = '.$contragentID.' | $action = "'.$action.'". Начальные значения: $kktCount = '.$kktCount.' | $kktPeriod = '.$kktPeriod);

        //По умолчанию увеличиваем количества ККТ.
        if($action == '') {
            $newKktCount = $kktCount + 1;
            $arUpdateUserProp = array(
                'UF_KKT' => $newKktCount,
                'UF_KKT2' => $kktPeriod
            );

            //Проверяем, нужно ли менять значение для списка
            //Если текущее значение равно крайнему значению периода - выполняем переключение на след. период.
            if ($kktCount == 10 || $kktCount == 50 || $kktCount == 100 || $kktCount == 300 || $kktCount == 500 || $kktCount == 1000) {
                $newPeriodKKT = 0;
                if ($kktCount == 10) {
                    $newPeriodKKT = $arKktPeriods["BEFORE_50"];
                }
                if ($kktCount == 50) {
                    $newPeriodKKT = $arKktPeriods["BEFORE_100"];
                }
                if ($kktCount == 100) {
                    $newPeriodKKT = $arKktPeriods["BEFORE_300"];
                }
                if ($kktCount == 300) {
                    $newPeriodKKT = $arKktPeriods["BEFORE_500"];
                }
                if ($kktCount == 500) {
                    $newPeriodKKT = $arKktPeriods["BEFORE_1000"];
                }
                if ($kktCount == 1000) {
                    $newPeriodKKT = $arKktPeriods["MORE_1000"];
                }
                $arUpdateUserProp["UF_KKT2"] = $newPeriodKKT;
            };
        }

        //В случае изменения ККТ со сменой Контрагента, для старого контрагента уменьшаем кол-во ККТ
        if($action == 'diff') {
            $newKktCount = $kktCount - 1;
            $arUpdateUserProp = array(
                'UF_KKT' => $newKktCount,
                'UF_KKT2' => $kktPeriod
            );
            //Проверяем, нужно ли менять значение для списка
            //Если текущее значение равно крайнему значению периода - выполняем переключение на след. период.
            if ($kktCount == 11 || $kktCount == 51 || $kktCount == 101 || $kktCount == 301 || $kktCount == 501 || $kktCount == 1001) {
                $newPeriodKKT = 0;
                if ($kktCount == 11) {
                    $newPeriodKKT = $arKktPeriods["BEFORE_10"];
                }
                if ($kktCount == 51) {
                    $newPeriodKKT = $arKktPeriods["BEFORE_50"];
                }
                if ($kktCount == 101) {
                    $newPeriodKKT = $arKktPeriods["BEFORE_100"];
                }
                if ($kktCount == 301) {
                    $newPeriodKKT = $arKktPeriods["BEFORE_300"];
                }
                if ($kktCount == 501) {
                    $newPeriodKKT = $arKktPeriods["BEFORE_500"];
                }
                if ($kktCount == 1001) {
                    $newPeriodKKT = $arKktPeriods["BEFORE_1000"];
                }
                $arUpdateUserProp["UF_KKT2"] = $newPeriodKKT;
            };
        }
        $logger->log('updateKktCountByContragent $contragentID = '.$contragentID.'. $arUpdateUserProp = '.print_r($arUpdateUserProp, true));
        if($contragentID > 0) {
            $USER_FIELD_MANAGER->Update(
                'CRM_COMPANY',
                $contragentID,
                $arUpdateUserProp
            );
        }
    }

    function OnAfterIBlockElementAddHandler(&$arFields)
    {
        if($arFields["IBLOCK_ID"] == GetIBlockIDByCode("registry_kkt") && intval($arFields["RESULT"]) > 0){
            $logger = Logger::getLogger('CompanyAjax','KKTHandler/updateKktCountByContragent.log');
            $propAgentCodeID = "CODE_AGENT";
            $propKontragentID = "COMPANY";
            $propAgentCodeVal = '';
            $propKontragentVal = '';

            foreach($arFields["PROPERTY_VALUES"] as $propID=>$arProp){
                if($propID == $propAgentCodeID){
                    $propAgentCodeVal = $arProp;
                }
                if($propID == $propKontragentID){
                    $propKontragentVal = $arProp;
                }
            }

            if(empty($propKontragentVal)){
                $propAgentCodeID = getPropIDbyCode('CODE_AGENT', 'registry_kkt');
                $propKontragentID = getPropIDbyCode('COMPANY', 'registry_kkt');
                foreach($arFields["PROPERTY_VALUES"] as $propID=>$arProp){
                    if($propID == $propAgentCodeID){
                        foreach($arProp as $arPropVal) {
                            $propAgentCodeVal = $arPropVal["VALUE"];
                        }
                    }
                    if($propID == $propKontragentID){
                        foreach($arProp as $arPropVal) {
                            $propKontragentVal = $arPropVal["VALUE"];
                        }
                    }
                }
            }

            //Добавление взаимосвязи Агент/Клиент
            if($propAgentCodeVal > 0 && $propKontragentVal > 0){
                $req = new \Bitrix\Crm\EntityRequisite();
                $rs = $req->getList([
                    "filter" => [
                        "RQ_INN" => $propAgentCodeVal,
                    ]
                ]);
                $reqData = $rs->fetchAll();
                if(sizeOf($reqData)>0){
                    $agentCompanyID = $reqData[0]["ENTITY_ID"];
                    if(intval($agentCompanyID) > 0 && intval($propKontragentVal) > 0){
                        kktHandler::relationshipAgentKontragent($agentCompanyID, $propKontragentVal);
                    }
                }
            }

            //Выполняем обновление кол-ва ККТ для компании для фиксирования в пользовательских полях UF_KKT и UF_KKT2
            if($propKontragentVal > 0){
                $logger->log('OnAfterIBlockElementAddHandler KKT_ID["'.$arFields["ID"].'"]. ID_COMPANY["'.$propKontragentVal.'"]');
                kktHandler::updateKktCountByContragent($propKontragentVal);
            }
        }
    }

    function OnBeforeIBlockElementUpdateHandler(&$arFields)
    {
        if($arFields["IBLOCK_ID"]==GetIBlockIDByCode("registry_kkt"))
        {
            $logger = Logger::getLogger('CompanyAjax','KKTHandler/updateKktCountByContragent.log');
            CModule::IncludeModule("iblock");
            $el = new CIBlockElement();
            $propAgentCodeID = 'CODE_AGENT';//getPropIDbyCode('CODE_AGENT', 'registry_kkt');
            $propKontragentID = 'COMPANY';//getPropIDbyCode('COMPANY', 'registry_kkt');
            $propAgentCodeVal = '';
            $propKontragentVal = '';

            //Получаем значение старых данных
            $oldCodeAgent = 0;
            $db_props = $el->GetProperty(
                $arFields["IBLOCK_ID"],
                $arFields["ID"],
                array("sort" => "asc"),
                array("CODE"=>"CODE_AGENT"));
            if($ar_props = $db_props->Fetch())
                $oldCodeAgent = IntVal($ar_props["VALUE"]);

            $oldCompanyID = 0;
            $db_props = $el->GetProperty(
                $arFields["IBLOCK_ID"],
                $arFields["ID"],
                array("sort" => "asc"),
                array("CODE"=>"COMPANY"));
            if($ar_props = $db_props->Fetch())
                $oldCompanyID = IntVal($ar_props["VALUE"]);

            foreach($arFields["PROPERTY_VALUES"] as $propID=>$arProp){
                if($propID == $propAgentCodeID){
                    $propAgentCodeVal = $arProp;
                }
                if($propID == $propKontragentID){
                    $propKontragentVal = $arProp;
                }
            }

            if(empty($propKontragentVal)){
                $propAgentCodeID = getPropIDbyCode('CODE_AGENT', 'registry_kkt');
                $propKontragentID = getPropIDbyCode('COMPANY', 'registry_kkt');
                foreach($arFields["PROPERTY_VALUES"] as $propID=>$arProp){
                    if($propID == $propAgentCodeID){
                        foreach($arProp as $arPropVal) {
                            $propAgentCodeVal = $arPropVal["VALUE"];
                        }
                    }
                    if($propID == $propKontragentID){
                        foreach($arProp as $arPropVal) {
                            $propKontragentVal = $arPropVal["VALUE"];
                        }
                    }
                }
            }

            //Добавление взаимосвязи Агент/Клиент
            if($oldCodeAgent != $propAgentCodeVal || $oldCompanyID != $propKontragentVal) {
                if ($propAgentCodeVal != '' && $propKontragentVal != '') {
                    $req = new \Bitrix\Crm\EntityRequisite();
                    $rs = $req->getList([
                        "filter" => [
                            "RQ_INN" => $propAgentCodeVal
                        ]
                    ]);
                    $reqData = $rs->fetchAll();
                    if (sizeOf($reqData) > 0) {
                        $agentCompanyID = $reqData[0]["ENTITY_ID"];
                        if (intval($agentCompanyID) > 0 && intval($propKontragentVal) > 0) {
                            kktHandler::relationshipAgentKontragent($agentCompanyID, $propKontragentVal);
                        }
                    }
                }

                //Если по ККТ изменилась привязка к компании - уменьшаем кол-во ККТ по старой компании и суммируем к новой
                $logger->log('OnBeforeIBlockElementUpdateHandler $arFields = '.print_r($arFields, true));
                if($oldCompanyID != $propKontragentVal){
                    $logger->log('OnBeforeIBlockElementUpdateHandler $oldCompanyID = '.$oldCompanyID.' | $propKontragentVal '.$propKontragentVal);
                    if($oldCompanyID > 0){
                        $logger->log('OnBeforeIBlockElementUpdateHandler KKT_ID["'.$arFields["ID"].'"]. Уменьшение ID_COMPANY["'.$oldCompanyID.'"]');
                        kktHandler::updateKktCountByContragent($oldCompanyID, "diff");
                    }
                    if($propKontragentVal > 0) {
                        $logger->log('OnBeforeIBlockElementUpdateHandler KKT_ID["' . $arFields["ID"] . '"]. Увеличение ID_COMPANY["' . $propKontragentVal . '"]');
                        kktHandler::updateKktCountByContragent($propKontragentVal);
                    }
                }
            }
        }
    }

    function OnBeforeIBlockElementDeleteHandler($ID) {
        CModule::IncludeModule("iblock");
        $el = new CIBlockElement();
        $iblockID = $el->GetIBlockByID($ID);
        if($iblockID == GetIBlockIDByCode("registry_kkt")) {
            $logger = Logger::getLogger('CompanyAjax','KKTHandler/updateKktCountByContragent.log');
            $db_props = $el->GetProperty(GetIBlockIDByCode("registry_kkt"), $ID, array("sort" => "asc"), Array("CODE" => "COMPANY"));
            if ($ar_props = $db_props->Fetch()) {
                $kontragentID = IntVal($ar_props["VALUE"]);
                if($kontragentID > 0) {
                    $logger->log('OnBeforeIBlockElementDeleteHandler KKT_ID["'.$ID.'"]. ID_COMPANY["'.$kontragentID.'"]');
                    kktHandler::updateKktCountByContragent($kontragentID, "diff");
                }
            }
        }
    }
}

class vacationReplacement
{
    function OnBeforeIBlockElementAddHandler(&$arFields)
    {
        if ($arFields["IBLOCK_ID"] == GetIBlockIDByCode("replace_list_for_holidays")) {
            $el = new CIBlockElement();
            global $APPLICATION;
            //Получаем ID свойств инфоблока
            $propZamenaVypolnenaID = getPropIDbyCode("ZAMENA_VYPOLNENA", "replace_list_for_holidays");
            $propHistoryLinksID = getPropIDbyCode("DANNYE_PO_VYPOLNENNYM_ZAMENAM", "replace_list_for_holidays");
            $propVacationStartID = getPropIDbyCode("DATA_NACHALA_OTPUSKA", "replace_list_for_holidays");
            $propVacationEndID = getPropIDbyCode("DATA_OKONCHANIYA_OTPUSKA", "replace_list_for_holidays");
            $propVacationUserID = getPropIDbyCode("SOTRUDNIK_UKHODYASHCHIY_V_OTPUSK", "replace_list_for_holidays");
            $propReplacementUserID = getPropIDbyCode("ZAMENYAYUSHCHIY_SOTRUDNIK", "replace_list_for_holidays");
            $vacationUser = current($arFields["PROPERTY_VALUES"][$propVacationUserID]);
            $vacationUser = $vacationUser["VALUE"];
            $arFields["PROPERTY_VALUES"][$propZamenaVypolnenaID] = '';
            $arFields["PROPERTY_VALUES"][$propHistoryLinksID] = '';
            $dateVacationStart = current($arFields["PROPERTY_VALUES"][$propVacationStartID]);
            $dateVacationStart = $dateVacationStart["VALUE"];
            $dateVacationEnd = current($arFields["PROPERTY_VALUES"][$propVacationEndID]);
            $dateVacationEnd = $dateVacationEnd["VALUE"];
            $dateVacationStart = new DateTime($dateVacationStart);
            $dateVacationEnd = new DateTime($dateVacationEnd);
            if($dateVacationEnd<$dateVacationStart){
                $APPLICATION->throwException("\"Даты начала отпуска\" не может быть позже \"Дата окончания отпуска\"");
                return false;
            }
            $rsProblems = $el->GetList(
                array("ID" => "ASC"),
                array("IBLOCK_ID" => GetIBlockIDByCode("replace_list_for_holidays"), "PROPERTY_ZAMENYAYUSHCHIY_SOTRUDNIK" => $vacationUser, "ACTIVE" => "Y"),
                false,
                false,
                array("ID", "NAME", "PROPERTY_DATA_NACHALA_OTPUSKA", "PROPERTY_DATA_OKONCHANIYA_OTPUSKA")
            );
            if($rsProblems->SelectedRowsCount()>0){
                while($arProblem = $rsProblems->GetNext()){
                    $problemDateVacationStart = new DateTime($arProblem["PROPERTY_DATA_NACHALA_OTPUSKA_VALUE"]);
                    $problemDateVacationEnd = new DateTime($arProblem["PROPERTY_DATA_OKONCHANIYA_OTPUSKA"]);
                    if($dateVacationEnd >= $problemDateVacationStart && $dateVacationStart <= $problemDateVacationEnd){
                        $APPLICATION->throwException("Перекрестные замены запрещены. \n\r \"Сотрудник уходящий в отпуск\" указан как \"Заменяющий сотрудник\" в элементе \"".$arProblem["NAME"]."\". На период с ".$problemDateVacationStart->format("d.m.Y")." по ".$problemDateVacationEnd->format("d.m.Y"));
                        return false;
                    }
                }
            }
        }
    }
    function OnBeforeIBlockElementDeleteHandler($ID)
    {
        self::updateBpVariables($ID);
    }

    function OnBeforeIBlockElementUpdateHandler(&$arFields)
    {
        if ($arFields["IBLOCK_ID"] == GetIBlockIDByCode("replace_list_for_holidays")) {
            global $APPLICATION;
            $elemID = $arFields["ID"];
            $el = new CIBlockElement();

            $propVacationStartID = getPropIDbyCode("DATA_NACHALA_OTPUSKA", "replace_list_for_holidays");
            $dateVacationStart = current($arFields["PROPERTY_VALUES"][$propVacationStartID]);
            $dateVacationStart = $dateVacationStart["VALUE"];
            $dateVacationStart = new DateTime($dateVacationStart);

            $propVacationEndID = getPropIDbyCode("DATA_OKONCHANIYA_OTPUSKA", "replace_list_for_holidays");
            $dateVacationEnd = current($arFields["PROPERTY_VALUES"][$propVacationEndID]);
            $dateVacationEnd = $dateVacationEnd["VALUE"];
            $dateVacationEnd = new DateTime($dateVacationEnd);

            $propVacationUserID = getPropIDbyCode("SOTRUDNIK_UKHODYASHCHIY_V_OTPUSK", "replace_list_for_holidays");
            $vacationUser = current($arFields["PROPERTY_VALUES"][$propVacationUserID]);
            $vacationUser = $vacationUser["VALUE"];

            if($dateVacationEnd<$dateVacationStart) {
                $APPLICATION->throwException("\"Даты начала отпуска\" не может быть позже \"Дата окончания отпуска\"");
                return false;
            }
            $rsProblems = $el->GetList(
                array("ID" => "ASC"),
                array("IBLOCK_ID" => GetIBlockIDByCode("replace_list_for_holidays"), "PROPERTY_ZAMENYAYUSHCHIY_SOTRUDNIK" => $vacationUser),
                false,
                false,
                array("ID", "NAME", "PROPERTY_DATA_NACHALA_OTPUSKA", "PROPERTY_DATA_OKONCHANIYA_OTPUSKA")
            );
            if($rsProblems->SelectedRowsCount()>0){
                while($arProblem = $rsProblems->GetNext()){
                    $problemDateVacationStart = new DateTime($arProblem["PROPERTY_DATA_NACHALA_OTPUSKA_VALUE"]);
                    $problemDateVacationEnd = new DateTime($arProblem["PROPERTY_DATA_OKONCHANIYA_OTPUSKA_VALUE"]);
                    if($dateVacationEnd >= $problemDateVacationStart && $dateVacationStart <= $problemDateVacationEnd){
                        $APPLICATION->throwException("Перекрестные замены запрещены. \n\r \"Сотрудник уходящий в отпуск\" указан как \"Заменяющий сотрудник\" в элементе \"".$arProblem["NAME"]."\". На период с ".$problemDateVacationStart->format("d.m.Y")." по ".$problemDateVacationEnd->format("d.m.Y"));
                        return false;
                    }
                }
            }
            $db_props = $el->GetProperty(
                GetIBlockIDByCode("replace_list_for_holidays"),
                $elemID,
                array("sort" => "asc"),
                Array("CODE" => "ZAMENA_VYPOLNENA")
            );
            if ($ar_props = $db_props->Fetch()) {
                //Если значение свойства "Замена выполнена" равно "да", то перед удалением элемента необходимо выполнить обратную замену.
                if($ar_props["VALUE_XML_ID"] == "REPLACE_NEW" || $ar_props["VALUE_XML_ID"] == "RETURN_REPLACE") {
                    //Сравниваем данные до изменения с данными поступившими на изменения, если отличия нет, то ничего не делаем
                    $dbAllProps = $el->GetProperty(
                        GetIBlockIDByCode("replace_list_for_holidays"),
                        $elemID,
                        array("sort" => "asc"),
                        Array()
                    );
                    $neededUpdate = false;
                    while($arProp = $dbAllProps->GetNext()){
                        $propID = $arProp["ID"];
                        $propValue = $arProp["VALUE"];
                        if($arProp["CODE"] == "SOTRUDNIK_UKHODYASHCHIY_V_OTPUSK" || $arProp["CODE"] == "ZAMENYAYUSHCHIY_SOTRUDNIK"){
                            $newPropValue = $arFields["PROPERTY_VALUES"][$propID][$elemID.":".$propID]["VALUE"];
                            if($propValue != $newPropValue){
                                $neededUpdate = true;
                            }
                        }
                        if($arProp["CODE"] == "ZAMENA_VYPOLNENA"){
                            $propID1 = $arProp["ID"];
                        }
                        if($arProp["CODE"] == "DANNYE_PO_VYPOLNENNYM_ZAMENAM"){
                            $propID2 = $arProp["ID"];
                        }
                    }
                    if($neededUpdate){
                        if($ar_props["VALUE_XML_ID"] == "REPLACE_NEW") {
                            self::updateBpVariables($elemID);
                        }
                        $arFields["PROPERTY_VALUES"][$propID1] = '';
                        $arFields["PROPERTY_VALUES"][$propID2] = array();
                    }
                }
            }
        }
    }

    //Общая фукнция, которая по истории обновления БП, выполняет обратную замену.
    //Используется при удаление/изменение элемента
    function updateBpVariables($elemID)
    {
        CModule::IncludeModule("iblock");
        $el = new CIBlockElement();
        $iblockID = $el->GetIBlockByID($elemID);
        if ($iblockID == GetIBlockIDByCode("replace_list_for_holidays")) {
            $arNeededTypes = array('PARAMETERS', 'VARIABLES', 'CONSTANTS');
            $loader = CBPWorkflowTemplateLoader::GetLoader();
            $runtime = CBPRuntime::GetRuntime();
            $runtime->StartRuntime();
            $DocumentService = $runtime->GetService("DocumentService");
            $db_props = $el->GetProperty(
                GetIBlockIDByCode("replace_list_for_holidays"),
                $elemID,
                array("sort" => "asc"),
                Array("CODE" => "ZAMENA_VYPOLNENA")
            );
            if ($ar_props = $db_props->Fetch()) {
                //Если значение свойства "Замена выполнена" равно "да", то необходимо выполнить обратную замену.
                if($ar_props["VALUE_XML_ID"] == "REPLACE_NEW") {
                    $arHistoryElementsID = [];
                    $dbHistoryElemLink = $el->GetProperty(
                        GetIBlockIDByCode("replace_list_for_holidays"),
                        $elemID,
                        array("sort" => "asc"),
                        Array("CODE" => "DANNYE_PO_VYPOLNENNYM_ZAMENAM")
                    );
                    while($arHistoryElemLink = $dbHistoryElemLink->GetNext()){
                        $arHistoryElementsID[] = $arHistoryElemLink["VALUE"];
                    }
                    $rsHistory = $el->GetList(
                        array("ID"=>"DESC"),
                        array("IBLOCK_ID" => GetIBlockIDByCode("history_replacements_vacations_BP"), "ID" => $arHistoryElementsID),
                        false,
                        false,
                        array(
                            "ID",
                            "NAME",
                            "PROPERTY_ID_BP",
                            "PROPERTY_PARAMETERS",
                            "PROPERTY_VARIABLES",
                            "PROPERTY_CONSTANTS",
                            "PROPERTY_SOTRUDNIK_USHEDSHIY_V_OTPUSK",
                            "PROPERTY_SOTRUDNIK_ZAMENYAYUSHCHIY",
                        )
                    );
                    while($arHistory = $rsHistory->GetNext()){
                        $bpID = $arHistory["PROPERTY_ID_BP_VALUE"];
                        $arUpdateBpFields = [];
                        $dbTemplatesList = CBPWorkflowTemplateLoader::GetList(
                            array("ID" => "DESC"),
                            array("ID" => $bpID),
                            false,
                            false,
                            array('ID', 'DOCUMENT_TYPE', 'NAME', 'PARAMETERS', 'VARIABLES', 'CONSTANTS')
                        );
                        $replacedUser = "user_".$arHistory["PROPERTY_SOTRUDNIK_ZAMENYAYUSHCHIY_VALUE"];
                        $vacationUser = "user_".$arHistory["PROPERTY_SOTRUDNIK_USHEDSHIY_V_OTPUSK_VALUE"];
                        if($arTemplate = $dbTemplatesList->GetNext()) {
                            foreach($arNeededTypes as $variableType){
                                if(!empty($arHistory["PROPERTY_".$variableType."_VALUE"])){
                                    $neededUpdate = false;
                                    foreach($arHistory["PROPERTY_".$variableType."_VALUE"] as $variableName){
                                        if(is_array($arTemplate[$variableType][$variableName]["Default"])){
                                            foreach($arTemplate[$variableType][$variableName]["Default"] as $key => $userID) {
                                                if($userID == $replacedUser) {
                                                    if(!isset($arUpdateFields[$variableType])){
                                                        $arUpdateFields[$variableType] = $arTemplate[$variableType];
                                                    };
                                                    $arUpdateFields[$variableType][$variableName]["Default"][$key] = $vacationUser;
                                                    $neededUpdate = true;
                                                    break;
                                                }
                                            }
                                        } else {
                                            if(!isset($arUpdateFields[$variableType])){
                                                $arUpdateFields[$variableType] = $arTemplate[$variableType];
                                            };
                                            $arUpdateFields[$variableType][$variableName]["Default"] = $vacationUser;
                                            $neededUpdate = true;
                                        }

                                        if($neededUpdate){
                                            //Получаем текстовое значение для выбранных пользователей
                                            $arUpdateFields[$variableType][$variableName]['Default_printable'] = $DocumentService->GetFieldInputValuePrintable(
                                                $arTemplate["DOCUMENT_TYPE"],
                                                $arUpdateFields[$variableType][$variableName],
                                                $arUpdateFields[$variableType][$variableName]["Default"]
                                            );
                                        }
                                    }
                                }
                            }
                            if(!empty($arUpdateFields)) {
                                if ($loader->UpdateTemplate($bpID, $arUpdateFields)) {
                                    $el->Update($arHistory["ID"], Array("ACTIVE" => "N"));
                                    BXClearCache(true);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

class bpRequestFns
{
    function SetParamNameTaxpayer(&$arFields)
    {
        if ($arFields["IBLOCK_ID"] == GetIBlockIDByCode("request_fns"))
        {
            //Фиксируем название поля "ИНН налогоплательщика" для процесса "Запрос ФНС"
            $propINN = getPropIDbyCode("INN_NALOGOPLATELSHCHIKA", "request_fns");
            //Фиксируем название поля "Наименование налогоплательщика" для процесса "Запрос ФНС"
            $propNameTaxpayer = getPropIDbyCode("NAIMENOVANIE_NALOGOPLATELSHCHIKA", "request_fns");
            //$logger = Logger::getLogger('request_fns', 'iblock/request_fns.log');
            //$logger->log('$arFields["PROPERTY_VALUES"][$propINN] = ' . print_r($arFields["PROPERTY_VALUES"][$propINN], true));
            foreach($arFields["PROPERTY_VALUES"][$propINN] as $key => $arProp) {
                if( isset($arProp["COMPANY_TITLE"]) )
                {
                    if( !empty($arProp["COMPANY_TITLE"]) )
                    {
                        $arFields["PROPERTY_VALUES"][$propNameTaxpayer][$key] = $arProp["COMPANY_TITLE"];
                    }
                    else
                    {
                        unset($arFields["PROPERTY_VALUES"][$propINN][$key]);
                    }
                }
            }

            //Отрпавляем сообщения, если статус заявки = Исполнен
            $elem = new CIBlockElement();
            if(!empty($arFields["ID"])){
                $rsElem = $elem->GetList(
                    array("ID" => "DESC"),
                    array("ID" => $arFields["ID"], "IBLOCK_ID" => "103"),
                    false,
                    false,
                    array(
                        "ID",
                        "NAME",
                        "PROPERTY_FAYLY",
                        "PROPERTY_ACRONIS_LINK",
                        "PROPERTY_STATUS_ZAYAVKI",
                        "PROPERTY_EMAIL",
                        "PROPERTY_NOMER",
                        "PROPERTY_DATA"
                    )
                );
                if($arElem = $rsElem->Fetch())
                {
                    if($arElem["PROPERTY_STATUS_ZAYAVKI_VALUE"] == "Исполненно")
                    {
                        $messageText = "Ответ на запрос №".$arElem["PROPERTY_NOMER_VALUE"]." от ".$arElem["PROPERTY_DATA_VALUE"];
                        $arEventFields = array(
                            "SENDER" => "zapros_fns@energocomm.ru",
                            "RECEIVER" => $arElem["PROPERTY_EMAIL_VALUE"], //"dostromogilsky@energocomm.ru"
                            "TITLE" => $messageText,
                            "MESSAGE" => $messageText
                        );
                        $arEventFieldsCopy = array(
                            "SENDER" => "zapros_fns@energocomm.ru",
                            "RECEIVER" => "zapros_fns@energocomm.ru", //"dostromogilsky@energocomm.ru"
                            "TITLE" => $messageText,
                            "MESSAGE" => $messageText
                        );
                        $index = 1;
                        $acronikLinks = [];
                        foreach($arElem["PROPERTY_ACRONIS_LINK_VALUE"] as $linkVal) {
                            if(!empty($linkVal)){
                                $acronikLinks[] = "$index. $linkVal";
                                $index++;
                            }
                        }

                        if (!empty($acronikLinks)) {
                            $arEventFields["ACRONIS_LINKS"] = "<br /><br />Ссылки для скачивания: <br />".implode($acronikLinks, "<br />");
                            $arEventFieldsCopy["ACRONIS_LINKS"] = "<br /><br />Ссылки для скачивания: <br />".implode($acronikLinks, "<br />");
                        }

                        $messFilesID = $arElem["PROPERTY_FAYLY_VALUE"];

                        if(sizeof($messFilesID) > 1)
                        {
                            $pdfID = array_shift($messFilesID);
                            foreach ($messFilesID as $key => $fileID)
                            {
                                CEvent::Send("BIZPROC_HTML_MAIL_TEMPLATE", "S1", $arEventFields, "N", 150, array($pdfID, $fileID));
                                CEvent::Send("BIZPROC_HTML_MAIL_TEMPLATE", "S1", $arEventFieldsCopy, "N", 150, array($pdfID, $fileID));
                            }
                        }
                        else
                        {
                            CEvent::Send("BIZPROC_HTML_MAIL_TEMPLATE", "S1", $arEventFields, "N", 150, $messFilesID);
                            CEvent::Send("BIZPROC_HTML_MAIL_TEMPLATE", "S1", $arEventFieldsCopy, "N", 150, $messFilesID);
                        }
                    }
                }
            }
        }
    }
}

/*
 * Функции из файла local/php_interface/events/iblock/handler.php
*/
define("REESTR_PAY_IBLOCK_CODE", "reestr_pay");

function updateCompanyFields($arFields)
{
    $dbIblockResult = CIBlock::GetByID($arFields["IBLOCK_ID"]);
    $arIblockResult = $dbIblockResult->GetNext();

    if ($arIblockResult["CODE"] === REESTR_PAY_IBLOCK_CODE) {
        updateCompany($arFields["ID"]);
    }
}

function updateCompany($id = "", $companyID = "")
{
    $arFilter = array(
        "IBLOCK_CODE" => REESTR_PAY_IBLOCK_CODE,
        "ID"          => $id
    );

    if ( ! empty($companyID)) {
        unset($arFilter["ID"]);
        $arFilter["PROPERTY_COMPANY"] = $companyID;
    }

    $dbCurrentOperationFields = CIBlockElement::GetList(
        array('DATE_CREATE' => 'desc'),
        $arFilter,
        false,
        array("nTopCount" => 1),
        array(
            "ID",
            "IBLOCK_ID",
            "NAME",
            "PROPERTY_ACTIVATION_DATE",
            "PROPERTY_COMPANY",
            "PROPERTY_LAST",
            "PROPERTY_TYPE",
            "PROPERTY_SUMM",
            "PROPERTY_SOSTOYANIE"
        )
    );

    $arCurrentOperationFields = array();
    while ($obCurrentOperationFields = $dbCurrentOperationFields->GetNextElement()) {
        $arCurrentOperationFields = $obCurrentOperationFields->GetFields();
    }

    $arCompanyFields = array(
        "UF_LAST_OP_DATA"  => $arCurrentOperationFields["PROPERTY_ACTIVATION_DATE_VALUE"],
        "UF_LAST_OP_TYPE"  => $arCurrentOperationFields["PROPERTY_TYPE_VALUE"],
        "UF_ACCOUNT_STATE" => $arCurrentOperationFields["PROPERTY_SOSTOYANIE_VALUE"],
        "UF_LAST_OP_SUMM"  => $arCurrentOperationFields["PROPERTY_SUMM_VALUE"]
    );

    $CCrmCompany = new CCrmCompany();
    $CCrmCompany->Update(
        $arCurrentOperationFields["PROPERTY_COMPANY_VALUE"],
        $arCompanyFields,
        true,
        true,
        array('DISABLE_USER_FIELD_CHECK' => true)
    );
}

function changeAuthorityVacation($arFields)
{
    if($arFields["IBLOCK_ID"] == GetIBlockIDByCode("change_authority_on_vacation", "lists"))
    {
        global $USER;
        $logger = Logger::getLogger('changeAuthorityVacation','changeAuthorityVacation.log');
        $logger->log('changeAuthorityVacation arFields[]'.print_r($arFields, true));
        $substitutePropID = getPropIDbyCode("REPLACEMENT_EMPLOYEE", "change_authority_on_vacation");
        $bpPropID = getPropIDbyCode("ID_BP", "change_authority_on_vacation");
        $vacationStartPropID = getPropIDbyCode("START_VACATION", "change_authority_on_vacation");
        $vacationEndPropID = getPropIDbyCode("END_VACATION", "change_authority_on_vacation");
        $substituteUserID = 0;
        $arSelectedBpNames = [];

        if( !empty($arFields["PROPERTY_VALUES"][$substitutePropID]) )
        {
            $arPropSubstitureUser = $arFields["PROPERTY_VALUES"][$substitutePropID];
            $arPropSubstitureUser = array_shift($arPropSubstitureUser);
            $substituteUserID = $arPropSubstitureUser["VALUE"];
            $logger->log('changeAuthorityVacation $substituteUserID = '.$substituteUserID);

            if( !empty($arFields["PROPERTY_VALUES"][$bpPropID]) )
            {
                $arSelectedBP = $arFields["PROPERTY_VALUES"][$bpPropID];
                $arSelectedBP = array_values($arSelectedBP);
                $logger->log('changeAuthorityVacation $arSelectedBP[] = ' . print_r($arSelectedBP, true));
                $dbTemplatesList = \CBPWorkflowTemplateLoader::GetList(
                    array("ID" => "ASC"),
                    array("ACTIVE" => "Y", "ID" => $arSelectedBP),
                    false,
                    false,
                    array("NAME")
                );
                while ($arTemplate = $dbTemplatesList->GetNext()) {
                    $logger->log('changeAuthorityVacation $arTemplate[]' . print_r($arTemplate, true));
                    $arSelectedBpNames[] = $arTemplate["NAME"];
                }
                $logger->log('changeAuthorityVacation $arSelectedBpNames[]' . print_r($arSelectedBpNames, true));
            }


            if( !empty($arSelectedBpNames) && !empty($substituteUserID) )
            {
                $arVacationStart = $arFields["PROPERTY_VALUES"][$vacationStartPropID];
                $arVacationEnd = $arFields["PROPERTY_VALUES"][$vacationEndPropID];
                $arVacationStart = array_shift($arVacationStart);
                $arVacationEnd = array_shift($arVacationEnd);
                CModule::IncludeModule("im");
                $mess = "Вы выбраны как замещающий сотрудник в БП: <b>".implode(", ", $arSelectedBpNames)."</b> на время отсутствия <b>с ".$arVacationStart["VALUE"]." по ".$arVacationEnd["VALUE"]."</b>";
                $arMessageFields = array(
                    "TO_USER_ID" => $substituteUserID,
                    "FROM_USER_ID" => $USER->GetID(),
                    "NOTIFY_TYPE" => IM_NOTIFY_FROM,
                    "NOTIFY_MODULE" => "iblock",
                    "NOTIFY_MESSAGE" => $mess,
                );
                \CIMNotify::Add($arMessageFields);

                $arSubstituteUser = $USER->GetByID($substituteUserID)->Fetch();
                $arEventFields = array(
                    "USER_EMAIL" => $arSubstituteUser["EMAIL"],
                    "MESSAGE" => $mess,
                );
                CEvent::Send("BP_CHANGE_AUTHORITY", "s1", $arEventFields, "N", 149);
            }
        }
    }
}