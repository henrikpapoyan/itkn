<?php
    function MyonAfterMessageAddHandler(&$id, $message, $topicInfo, $forumInfo, $fields)
    {
        //Проверяем, указан ли АВТОР СООБЩЕНИЯ
        if ($message["AUTHOR_ID"] != '') {
            //В поле FT_TITLE хранится значение, по которому можно найти ID элемента ИБ через таблицу b_bp_workflow_state, но перед этим необходимо избавиться от приписки WF_
            $WFText = $message["FT_TITLE"];
            $posWF  = stripos($WFText, 'WF_');
            if ($posWF !== false) {
                global $DB;
                $posWF             = $posWF + 3;
                $workflowID        = substr($WFText, $posWF);
                $strSql            = "SELECT
                  `DOCUMENT_ID`
                FROM
                    `b_bp_workflow_state`
                WHERE
                  `ID` = '" . $workflowID . "'
            ";
                $err_mess          = 'Ошибка вызова запроса';
                $rsBpWorkflowState = $DB->Query($strSql, false, $err_mess . __LINE__);
                //Если в таблице b_bp_workflow_state есть строка с ID == $workflowID, то в поле DOCUMENT_ID хранится ID элемента ИБ
                if ($arBpWorkflowState = $rsBpWorkflowState->Fetch()) {

                    if (intval($arBpWorkflowState["DOCUMENT_ID"]) > 0) {
                        //Формируем массив в ID пользователей которых упомянули в комментарии
                        $arReferUsers = array();
                        if (preg_match_all("/\[USER=([0-9]{1,})\](.*?)\[\/USER\]/i", $message['POST_MESSAGE'],
                            $matches)) {
                            foreach ($matches[1] as $userId) {
                                $arReferUsers[] = $userId;
                            }
                        }
                        $elemID = $arBpWorkflowState["DOCUMENT_ID"];
                        CModule::IncludeModule("iblock");
                        CModule::IncludeModule("im");
                        $el            = new CIBlockElement();
                        $dbDocument    = $el->GetByID($elemID);
                        $arDocument    = $dbDocument->Fetch();
                        $db_props      = CIBlockElement::GetProperty(GetIBlockIDByCode("soglasovaniye_3"), $elemID,
                            array("sort" => "asc"), Array("CODE" => "NABLYUDATELI"));
                        $arNabludateli = array();
                        //Проверяем, установлены ли наблюдатели
                        while ($ob = $db_props->GetNext()) {
                            //Если этот пользователь есть в массиве с упомянутыми пользоателями, его не включаем сюда, т.к. ему полетит персональное уведомление об адресованном ему комментарии.
                            if ($ob['VALUE'] != '' && ! in_array($ob['VALUE'], $arReferUsers)) {
                                $arNabludateli[] = $ob['VALUE'];
                            }
                        }
                        //Необходимо проверить, не был ли упомянут кто то, чтобы ему отсылать уведомление о том, что его упомянули и не слать уведомление о добавление комментария
                        foreach ($arNabludateli as $nabludatelID) {
                            if ($message["AUTHOR_ID"] != $nabludatelID) {
                                $arMessageFields = array(
                                    "TO_USER_ID"     => $nabludatelID,
                                    "FROM_USER_ID"   => $message["AUTHOR_ID"],
                                    "NOTIFY_TYPE"    => IM_NOTIFY_FROM,
                                    "NOTIFY_MODULE"  => "lists",
                                    "NOTIFY_MESSAGE" => 'Добавлен комментарий к процессу "<a href="/company/personal/processes/">' .
                                                        $arDocument["NAME"] . '</a>". Текст комментария: "' .
                                                        $message['POST_MESSAGE'] . '"',
                                );
                                //AddMessage2Log('onAfterMessageAdd $arMessageFields = ', print_r($arMessageFields ,true));
                                CIMNotify::Add($arMessageFields);
                            }
                        }
                        foreach ($arReferUsers as $referID) {
                            if ($message["AUTHOR_ID"] != $referID) {
                                $arMessageFields = array(
                                    "TO_USER_ID"     => $referID,
                                    "FROM_USER_ID"   => $message["AUTHOR_ID"],
                                    "NOTIFY_TYPE"    => IM_NOTIFY_FROM,
                                    "NOTIFY_MODULE"  => "lists",
                                    "NOTIFY_MESSAGE" => 'Вас упомянули в комментарии к процессу "<a href="/company/personal/processes/">' .
                                                        $arDocument["NAME"] . '</a>". Текст комментария: "' .
                                                        $message['POST_MESSAGE'] . '"',
                                );
                                CIMNotify::Add($arMessageFields);
                            }
                        }
                    }
                }
            }
        }
    }