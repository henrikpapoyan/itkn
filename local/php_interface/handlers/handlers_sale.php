<?php
function OnAfterOrderUpdateHandlerGK(&$arFields)
{
    if ($arFields['STATUS_ID'] == 'P' && $arFields["ID"] != 4578)
    {

        $arInvoice = CCrmInvoice::GetByID($arFields["ID"]);
        if($arInvoice['STATUS_ID'] != 'P')
        {
            if(CModule::IncludeModule('im'))
            {
                $arMessageFields = array(
                    "TO_USER_ID" => $arInvoice['RESPONSIBLE_ID'],
                    "NOTIFY_TYPE" => IM_NOTIFY_SYSTEM,
                    "NOTIFY_MODULE" => "main",
                    "NOTIFY_MESSAGE" => 'Поступила оплата по счету <a href="/crm/invoice/show/'.$arFields["ID"].'/">'.$arFields['ACCOUNT_NUMBER'].' </a>',
                );
                CIMNotify::Add($arMessageFields);
            }
        }
    }


    // Обновление номера счета после получения информации от 1С системы
    define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/log.txt");
    AddMessage2Log('OnAfterOrderUpdateHandlerGK $arFields[] = '.print_r($arFields, true));

    if(!empty($arFields['ID_1C']) && !isset($arFields['CUSTOM_UPDATE'])) {
        $CCrmInvoice = new CAllCrmInvoice(false);
        $dbRes = $CCrmInvoice->GetList(array(), array("ID" => $arFields["ID"], "CHECK_PERMISSIONS" => "N"));
        if($arRes = $dbRes->Fetch())
        {
            AddMessage2Log('OnAfterOrderUpdateHandlerGK $arRes[] = '.print_r($arRes, true));
            $numberAccountWithDate = $arFields['ID_1C'];
            if(!empty($arRes['DATE_BILL'])){
                if(date("Y", strtotime($arRes['DATE_BILL'])) != 2017) {
                    $numberAccountWithDate = $arFields['ID_1C'] . " (" . date("Y", strtotime($arRes['DATE_BILL'])) . ")";
                }
            } else {
                if(date("Y") != 2017) {
                    $numberAccountWithDate = $arFields['ID_1C'] . " (" . date("Y") . ")";
                }
            }

            AddMessage2Log('OnAfterOrderUpdateHandlerGK $numberAccountWithDate = '.$numberAccountWithDate);
            AddMessage2Log('OnAfterOrderUpdateHandlerGK $arRes[\'ACCOUNT_NUMBER\'] = '.$arRes['ACCOUNT_NUMBER']);

            if($arRes['ACCOUNT_NUMBER'] != $numberAccountWithDate) {
                $arFieldsInvoice['ACCOUNT_NUMBER'] = $numberAccountWithDate;
                $arFieldsInvoice['CUSTOM_UPDATE'] = "Y";
                $arFieldsInvoice['UPDATED_1C'] = "Y";

                if ($arRes['STATUS_ID'] == 'N' || $arRes['STATUS_ID'] == "S") {
                    AddMessage2Log('OnAfterOrderUpdateHandlerGK STATUS_ID = '.$arRes['STATUS_ID']);
                    $arFieldsInvoice["STATUS_ID"] = "A";
                }

                AddMessage2Log('OnAfterOrderUpdateHandlerGK $arFieldsInvoice[] = '.print_r($arFieldsInvoice, true));
                if ($oResultUpdate = $CCrmInvoice->Update($arFields["ID"], $arFieldsInvoice)) {
                    AddMessage2Log('OnAfterOrderUpdateHandlerGK SUCCESS UPDATE ORDER');
                } else {
                    AddMessage2Log('OnAfterOrderUpdateHandlerGK ERROR UPDATE ORDER $oResultUpdate = '.$CCrmInvoice->LAST_ERROR);
                    AddMessage2Log('OnAfterOrderUpdateHandlerGK ERROR UPDATE ORDER $oResultUpdate = '.print_r($oResultUpdate, true));
                }
                /*if ($arRes['STATUS_ID'] == 'N') {
                    $statusParams= array(
                        'STATE_SUCCESS' => true,
                        'STATE_FAILED' => false,
                    );
                    if ($CCrmInvoice->SetStatus($arRes['ID'], 'A', $statusParams)) {
                        AddMessage2Log('OnAfterOrderUpdateHandlerGK for order['.$arFields["ID"].'] status change to A ');
                    } else {
                        AddMessage2Log('OnAfterOrderUpdateHandlerGK ERROR change status for order['.$arFields["ID"].']');
                    }
                }*/
            }
        }
    }
}