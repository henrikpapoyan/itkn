<?php
function OnAfterCrmDealAddHandler($arFields)
{
    $arUsers = $arFields['UF_PROJECT_TEAM'];

    if (($arFields['ASSIGNED_BY_ID'] > 0) && ! array_search($arFields['ASSIGNED_BY_ID'], $arUsers)) {
        $arUsers[] = $arFields['ASSIGNED_BY_ID'];
    }

    if (($arFields['UF_PROJECT_MANAGER'] > 0) && ! array_search($arFields['UF_PROJECT_MANAGER'], $arUsers)) {
        $arUsers[] = $arFields['UF_PROJECT_MANAGER'];
    }

    if (($arFields['UF_SUPERVISOR'] > 0) && ! array_search($arFields['UF_SUPERVISOR'], $arUsers)) {
        $arUsers[] = $arFields['UF_SUPERVISOR'];
    }

    if ($arFields['UF_PROJECT_TEAM'] != $arUsers) {
        $CCrmDeal  = new CCrmDeal();
        $newFields = array('UF_PROJECT_TEAM' => $arUsers);
        $ret       = $CCrmDeal->Update($arFields['ID'], $newFields);
    }
}

function OnAfterCrmLeadAddHandler($arFields)
{
    $logger = Logger::getLogger("LeadAdd", "Lead/Add.log");
    $logger->log("arFields=[".print_r($arFields, true)."]");
    if(!$arFields['UF_AUTO'])
    {
        $CCrmLead = new CCrmLead();
        if (array_search($arFields['UF_CHANNEL'], array(5276, 5275, 3336)) !==false )
        {
            $arNewFields = array('UF_AUTO' => 'N');
            if ($arFields['STATUS_ID'] == 'NEW')
            {
                $arNewFields['STATUS_ID'] = '4';
            }
            $CCrmLead->Update($arFields['ID'], $arNewFields);
        }else
        {
            $arNewFields = array('UF_AUTO' => 'Y');
            $CCrmLead->Update($arFields['ID'], $arNewFields);
        }
    }
    return true;
}

function OnAfterCrmInvoiceUpdateHandlerLog($arFields)
{
    global $DB, $USER;
    $logger = Logger::getLogger("InvoiceUpdate", "Invoice/Update/InvoiceUpdate.log");
    $logger2 = Logger::getLogger("InvoiceUpdate2", "Invoice/Update/InvoiceUpdate2.log");
    $logger3 = Logger::getLogger("InvoiceUpdate3", "Invoice/Update/InvoiceUpdate3.log");
    $logger->log(print_r($arFields, true));
    $user_id = $USER->GetID();
    if ($user_id > 0)
    {
        $rsUser = CUser::GetByID($USER->GetID());
        if($arUser = $rsUser->Fetch())
        {
            $curUser = array();
            $curUser['ID'] = $arUser['ID'];
            $curUser['LOGIN'] = $arUser['LOGIN'];
            $curUser['NAME'] = $arUser['NAME'];
            $curUser['LAST_NAME'] = $arUser['LAST_NAME'];
            $logger2->log($arFields['ID']." ".$arFields['ACCOUNT_NUMBER']." ".$arFields['~DATE_BILL']." ".implode(" ", $curUser).
                " ".$_SERVER['REQUEST_URI']);
        }
    }
    $logger3->log(json_encode($arFields));
    return true;
}

function OnAfterCrmInvoiceSetStatusHandlerLog($arFields)
{
    global $USER;
    $logger = Logger::getLogger("InvoiceStatus", "Invoice/Status/InvoiceStatus.log");
    $logger2 = Logger::getLogger("InvoiceStatus2", "Invoice/Status/InvoiceStatus2.log");
    $logger3 = Logger::getLogger("InvoiceStatus3", "Invoice/Status/InvoiceStatus3.log");
    $logger->log(print_r($arFields, true));
    $user_id = $USER->GetID();
    if ($user_id > 0)
    {
        $rsUser = CUser::GetByID($USER->GetID());
        if($arUser = $rsUser->Fetch())
        {
            $curUser = array();
            $curUser['ID'] = $arUser['ID'];
            $curUser['LOGIN'] = $arUser['LOGIN'];
            $curUser['NAME'] = $arUser['NAME'];
            $curUser['LAST_NAME'] = $arUser['LAST_NAME'];
            $arInvoice = CCrmInvoice::GetByID($arFields['ID']);
            $logger2->log($arFields['ID']." ".$arInvoice['ACCOUNT_NUMBER']." ".$arInvoice['DATE_BILL']." ".implode(" ", $curUser).
                " ".$_SERVER['REQUEST_URI']);
        }
    }
    $logger3->log(json_encode($arFields));

    return true;
}

function OnBeforeCrmInvoiceDeleteHandlerLog($ID)
{
    global $DB, $USER;
    $logger = Logger::getLogger("InvoiceDelete", "Invoice/Delete/InvoiceDelete.log");
    $logger2 = Logger::getLogger("InvoiceDelete2", "Invoice/Delete/InvoiceDelete2.log");
    $logger->log(print_r($ID, true));
    $user_id = $USER->GetID();
    if ($user_id > 0)
    {
        $rsUser = CUser::GetByID($USER->GetID());
        if($arUser = $rsUser->Fetch())
        {
            $curUser = array();
            $curUser['ID'] = $arUser['ID'];
            $curUser['LOGIN'] = $arUser['LOGIN'];
            $curUser['NAME'] = $arUser['NAME'];
            $curUser['LAST_NAME'] = $arUser['LAST_NAME'];
            $arInvoice = CCrmInvoice::GetByID($ID);
            $logger2->log($ID." ".$arInvoice['ACCOUNT_NUMBER']." ".$arInvoice['DATE_BILL']." ".implode(" ", $curUser).
                " ".$_SERVER['REQUEST_URI']);
        }
    }

    return true;
}

/**
 * Description of ProcessingCompany
 *  Класс обработки контрагентов
 * @author Klyopov Roman
 * метод обработки контрагентов (при добавлении).
 * По инн определяет регион и обновляет пользовательское поле “Регион” для контрагента.
 */

class ProcessingCompany
{
    // константы класса
    const FIELD_CRM_INN = "UF_CRM_INN";
    const REGIONS_LIST_IBLOCK_CODE = "region";
    const FIELD_REGION = "UF_REGION";
    const SALES_DEPARTMENT = 65; //ID Отдела продаж

    // метод обработки контрагентов (при добавлении)
    public function onAfterCompanyAdd($arFields)
    {
        $logger = Logger::getLogger("onAfterCompanyAdd");
        $logger->log('$arFields = '.print_r($arFields, true));
        if ( ! empty($arFields["UF_CRM_INN"]) && ! empty($arFields["ID"])) {
            $CCrmLead = new CCrmLead();
            $arFilter = array("UF_CRM_1499414186" => $arFields["UF_CRM_INN"]);
            if ( ! empty ($arFields["UF_CRM_INN"]) && strlen($arFields["UF_CRM_INN"]) !== 10) {
                $arFilter["UF_KPP"] = $arFields["UF_CRM_KPP"];
            }
            $obLead = CCrmLead::GetList(array(), $arFilter, array());
            $arLeads = array();
            while ($arLead = $obLead->GetNext()) {
                $arLeads[] = $arLead;
            }
            foreach ($arLeads as $lead) {
                $arField = array('UF_CRM_COMPANY' => $arFields["ID"]);
                $CCrmLead->Update($lead["ID"], $arField);
            }
        }

        if(!isset($arFields[self::FIELD_REGION]) or empty($arFields[self::FIELD_REGION]))
        {
            // если есть инн
            if(!empty($arFields[self::FIELD_CRM_INN]) and strlen($arFields[self::FIELD_CRM_INN]) > 0)
            {
                global $USER;
                if (!$USER->IsAuthorized()) $USER->Authorize(372);

                $firstDigits = substr($arFields[self::FIELD_CRM_INN], 0, 2);
                $regionId = 0;

                $arSelectRegion = array("ID", "NAME");
                $arFilterRegion = array("IBLOCK_CODE" => self::REGIONS_LIST_IBLOCK_CODE, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y", "PROPERTY_NUM_SUBJECT" => $firstDigits);
                $dbRegions = CIBlockElement::GetList(array(), $arFilterRegion, false, array("nPageSize" => 1), $arSelectRegion);
                if ($arRegion = $dbRegions->GetNext())
                {
                    if ($arRegion["ID"]) $regionId = $arRegion["ID"];
                }

                // если регион был найден, то привязываем его к компании
                if($regionId) setUserField("CRM_COMPANY", $arFields["ID"], self::FIELD_REGION, $regionId);
            }
        }
    }

    // метод обработки контрагентов (при добавлении)
    public function OnBeforeCrmCompanyUpdate(&$arFields)
    {
        define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/log.txt");
        AddMessage2Log('OnBeforeCrmCompanyUpdate $arFields[] = '.print_r($arFields, true));
        global $USER;
        $userID = $USER->GetID();
        $arSalesDepartmentEmployees = [];
        $rsSalesDepartmentEmployees = CIntranetUtils::GetDepartmentEmployees(array(self::SALES_DEPARTMENT), $bRecursive = false, $bSkipSelf = false, $onlyActive = 'Y');
        while($arSalesDepartmentEmployee = $rsSalesDepartmentEmployees->GetNext()){
            $arSalesDepartmentEmployees[$arSalesDepartmentEmployee["ID"]] = $arSalesDepartmentEmployee["ID"];
        }
        $arDepartmentManager = CIntranetUtils::GetDepartmentManager(array(self::SALES_DEPARTMENT), $skipUserId=false, $bRecursive=false);
        foreach($arDepartmentManager as $empID => $arEmp){
            unset($arSalesDepartmentEmployees[$empID]);
        }

        //Исключение для "Дмитрий Серебринский"
        unset($arSalesDepartmentEmployees["424"]);

        if(in_array($userID, $arSalesDepartmentEmployees)){
            global $APPLICATION;
            $CCrmCompany = new CCrmCompany();
            $arCompany = $CCrmCompany->GetByID($arFields["ID"]);
            if($arCompany["ASSIGNED_BY_ID"] != $arFields["ASSIGNED_BY_ID"]) {
                $arFields['RESULT_MESSAGE'] = "Сотрудникам \"Отдела продаж\" запрещено изменять ОТВЕТСТВЕННОГО.";
                return false;
            }
        }
    }
}


/*
 * Метод для обработки события добавления/изменения счета.
 * Обновление шаблона печати счета в зависимости от реквизита компании(ЭСК, ИС, РЭ)
*/

class ProcessingInvoice
{
    const ID_TEMPlATE_INVOICE_ESK   = 1; //ЭСК
    const ID_TEMPlATE_INVOICE_IS    = 9; //ИНТЕГРАЦИОННЫЕ СИСТЕМЫ
    const ID_TEMPlATE_INVOICE_RJe   = 10; //РУСЭКСПЕРТИЗА

    const ID_COMPANY_ESK            = 55060; //ЭСК
    const ID_COMPANY_IS             = 6426; //ИНТЕГРАЦИОННЫЕ СИСТЕМЫ
    const ID_COMPANY_RJe            = 146137; //РУСЭКСПЕРТИЗА

    // Обработа события добавления/изменения счета. Обновление шаблона печати счета в зависимости от реквизита компании(ЭСК, ИС, РЭ)
    public function onAfterCrmInvoiceAddUpdate($arFields)
    {
        CModule::IncludeModule('crm');
        $oCrmInvoice = new CCrmInvoice;

        //Проверяем наличие реквизита компании
        if(!empty($arFields['UF_MYCOMPANY_ID'])){

            if($arFields['UF_MYCOMPANY_ID'] == self::ID_COMPANY_ESK && $arFields['PAY_SYSTEM_ID'] != self::ID_TEMPlATE_INVOICE_ESK){
                $resUp = $oCrmInvoice->Update($arFields['ID'], array("PAY_SYSTEM_ID" => self::ID_TEMPlATE_INVOICE_ESK));
            }

            if($arFields['UF_MYCOMPANY_ID'] == self::ID_COMPANY_IS && $arFields['PAY_SYSTEM_ID'] != self::ID_TEMPlATE_INVOICE_IS){
                $resUp = $oCrmInvoice->Update($arFields['ID'], array("PAY_SYSTEM_ID" => self::ID_TEMPlATE_INVOICE_IS));
            }

            if($arFields['UF_MYCOMPANY_ID'] == self::ID_COMPANY_RJe && $arFields['PAY_SYSTEM_ID'] != self::ID_TEMPlATE_INVOICE_RJe){
                $resUp = $oCrmInvoice->Update($arFields['ID'], array("PAY_SYSTEM_ID" => self::ID_TEMPlATE_INVOICE_RJe));
            }
        }

    }
}


/**
 * Description of ProcessingLead
 *  Класс обработки лидов
 * @author Klyopov Roman
 */

class ProcessingLead
{
    // константы класса
    const GROUP_REG_WITH_OFD   = 24;
    const LIST_ID_FIRST =  10569057;
    const LIST_ID_SECOND = 10569061;
    const LIST_ID_THIRD =  10569065;
    const LIST_ID_FOURTH = 11728205;
    const FIELD_INN              = "UF_CRM_1499414186";
    const REESTR_KKT_IBLOCK_CODE = "registry_kkt";
    const FIELD_CRM_INN          = "UF_CRM_INN";

    const STATE_LEAD_PAUSE      = "XML_STATE_LEAD_PAUSE";
    const STATE_LEAD_ACTIVITY   = "XML_STATE_LEAD_ACTIVITY";
    const STATE_LEAD_PROCESSED  = "XML_STATE_LEAD_PROCESSED";

    const WORK_DAYS_TEMP_ACC    = 5;
    const WORK_DAYS_FULL_ACC    = 5;
    const WORK_DAYS_ADD_KKT     = 7;

    // метод обработки лидов раз в 15 минут (для unisender)
    public static function CheckLeadAdd()
    {
        global $USER;
        $USER->Authorize(372);

        $res = \Bitrix\Main\Loader::includeModule("crm");

        $uniSenderIntegration = new UniSenderIntegration();

        // определяем ip лида
        $leadIp = "";

        // мин. и макс. даты для фильтрации
        $minDate = ConvertTimeStamp(time() - 1800, "FULL");
        $maxDate = ConvertTimeStamp(time() - 900, "FULL");

        $arFilter = array(
            array(
                "LOGIC"            => "AND",
                ">DATE_CREATE"  => $minDate,
                "<=DATE_CREATE" => $maxDate
            ),
            self::FIELD_INN                => false,
            "SOURCE_ID" => self::GROUP_REG_WITH_OFD,
            "STATUS_ID"                     => "NEW"
        );

        $dbLeads = CCrmLead::GetList(
            array(),
            $arFilter,
            array(
                "ID",
                "NAME",
                "DATE_CREATE"
            )
        );

        while ($arLead = $dbLeads->GetNext())
        {
            // форматируем дату создания лида
            $leadDate = date("Y-m-d%20h:i:s");
            if($arLead["DATE_CREATE"])
                $leadDate = FormatDate("Y-m-d%20h:i:s", MakeTimeStamp($arLead["DATE_CREATE"]) - 11100);

            // имя лида
            $leadName = "";
            if($arLead["NAME"]) $leadName = urlencode($arLead["NAME"]);
            else $leadName = "none";

            // получаем мульти поля
            $dbMultiFields = CCrmFieldMulti::GetList(
                array("ID" => "ASC"),
                array(
                    "ENTITY_ID"         => "LEAD",
                    "ELEMENT_ID" => $arLead["ID"],
                    "TYPE_ID"          => "EMAIL"
                )
            );

            if ($arMultiField = $dbMultiFields->Fetch())
            {
                // email лида
                $leadEmail = "";
                if ($arMultiField["TYPE_ID"] == "EMAIL")
                    $leadEmail = trim($arMultiField["VALUE"]);
            }

            $dbMultiFields = CCrmFieldMulti::GetList(
                array("ID" => "ASC"),
                array(
                    "ENTITY_ID" => "LEAD",
                    "ELEMENT_ID" => $arLead["ID"],
                    "TYPE_ID" => "PHONE"
                )
            );

            if ($arMultiField = $dbMultiFields->Fetch())
            {
                // телефон лида
                $leadPhone = "";
                if($arMultiField["TYPE_ID"] == "PHONE")
                    $leadPhone = urlencode(trim($arMultiField["VALUE"]));
            }

            // параметры запроса
            $params = "&confirm_time=" . $leadDate . "&request_time=" . $leadDate . "&request_ip=" . $leadIp . "&confirm_ip=" . $leadIp . "&list_ids=" . self::LIST_ID_FIRST . "&fields[email]=" . $leadEmail . "&fields[phone]=" . $leadPhone . "&fields[Name]=" . $leadName;

            $result = $uniSenderIntegration->action("addToList", $params);
        }

        $USER->Logout();
        return "ProcessingLead::CheckLeadAdd();";
    }

    // метод обработки лидов раз в 2 дня (для unisender)
    public static function CheckLeadPreRegistration()
    {
        global $USER;
        $USER->Authorize(372);

        \Bitrix\Main\Loader::includeModule("crm");
        $uniSenderIntegration = new UniSenderIntegration();

        // определяем ip лида
        $leadIp = "";

        // мин. и макс. даты для фильтрации
        $minDate = ConvertTimeStamp(time() - 3600 * 24 * 4, "FULL");
        $maxDate = ConvertTimeStamp(time() - 3600 * 24 * 2, "FULL");

        $dbLeads = CCrmLead::GetList(
            array(),
            array(
                array(
                    "LOGIC" => "AND",
                    ">DATE_CREATE" => $minDate,
                    "<=DATE_CREATE" => $maxDate
                ),
                "!" . self::FIELD_INN => false,
                "STATUS_ID" => "ASSIGNED"
            ),
            array(
                "ID",
                "NAME",
                "DATE_CREATE"
            )
        );

        while ($arLead = $dbLeads->GetNext())
        {
            // форматируем дату создания лида
            $leadDate = date("Y-m-d%20h:i:s");
            if ($arLead["DATE_CREATE"])
                $leadDate = FormatDate("Y-m-d%20h:i:s", MakeTimeStamp($arLead["DATE_CREATE"]) - 11100);

            // имя лида
            if ($arLead["NAME"])
                $leadName = urlencode($arLead["NAME"]);
            else $leadName = "none";

            // получаем мульти поля
            $dbMultiFields = CCrmFieldMulti::GetList(
                array("ID" => "ASC"),
                array(
                    "ENTITY_ID" => "LEAD",
                    "ELEMENT_ID" => $arLead["ID"],
                    "TYPE_ID" => "EMAIL"
                )
            );

            if($arMultiField = $dbMultiFields->Fetch())
            {
                // email лида
                $leadEmail = "";
                if ($arMultiField["TYPE_ID"] == "EMAIL")
                {
                    $leadEmail = trim($arMultiField["VALUE"]);
                }
            }

            $dbMultiFields = CCrmFieldMulti::GetList(
                array("ID" => "ASC"),
                array(
                    "ENTITY_ID" => "LEAD",
                    "ELEMENT_ID" => $arLead["ID"],
                    "TYPE_ID" => "PHONE"
                )
            );

            if ($arMultiField = $dbMultiFields->Fetch())
            {
                // телефон лида
                $leadPhone = "";
                if($arMultiField["TYPE_ID"] == "PHONE")
                    $leadPhone = urlencode(trim($arMultiField["VALUE"]));
            }

            // параметры запроса
            $params = "&confirm_time=" . $leadDate . "&request_time=" . $leadDate . "&request_ip=" . $leadIp . "&confirm_ip=" . $leadIp . "&list_ids=" . self::LIST_ID_SECOND . "&fields[email]=" . $leadEmail . "&fields[phone]=" . $leadPhone . "&fields[Name]=" . $leadName;

            // длаем дополнительную проверку на существование контрагента (по email)
            if ($leadEmail)
            {
                $dbMultiFields = CCrmFieldMulti::GetList(
                    array("ID" => "ASC"),
                    array(
                        "ENTITY_ID" => "COMPANY",
                        "TYPE_ID" => "EMAIL",
                        "VALUE" => $leadEmail
                    )
                );

                if (!$arMultiField = $dbMultiFields->Fetch())
                {
                    $result = $uniSenderIntegration->action("addToList", $params);
                }
            }
        }

        $USER->Logout();
        return "ProcessingLead::CheckLeadPreRegistration();";
    }

    // метод обработки контрагентов раз в 2 дня
    public static function CheckKontragent3Days()
    {
        global $USER;
        $USER->Authorize(372);

        \Bitrix\Main\Loader::includeModule("crm");

        $uniSenderIntegration = new UniSenderIntegration();

        // мин. и макс. даты для фильтрации
        $minDate = ConvertTimeStamp(time() - 3600 * 24 * 4, "FULL");
        $maxDate = ConvertTimeStamp(time() - 3600 * 24 * 2, "FULL");

        // массив для всех контрагентов, без проверки ккт
        $arCompanies = array();
        // массив связки лидов и контрагентов
        $arLeads = array();

        $arCompanyOrder = array("ID" => "ASC");
        $arCompanyFilter = array(
            "!" . self::FIELD_CRM_INN => false,
            array(
                "LOGIC" => "AND",
                ">DATE_CREATE" => $minDate,
                "<=DATE_CREATE" => $maxDate
            )
        );

        $arCompanySelect = array(
            "ID",
            "TITLE",
            "DATE_CREATE",
            "LEAD_ID"
        );

        $dbCompanies = CCrmCompany::GetList($arCompanyOrder, $arCompanyFilter, $arCompanySelect);
        while ($arCompany = $dbCompanies->GetNext())
        {
            if ($arCompany["LEAD_ID"])
            {
                $arLeads[$arCompany["LEAD_ID"]] = $arCompany["ID"];
                $arCompanies[$arCompany["ID"]] = array(
                    $arCompany["ID"],
                    $arCompany["TITLE"],
                    $arCompany["DATE_CREATE"]
                );

                // выбираем телефон
                $dbCompPhone = CCrmFieldMulti::GetList(
                    array("ID" => "ASC"),
                    array(
                        "ELEMENT_ID" => $arCompany["ID"],
                        "TYPE_ID" => "PHONE"
                    )
                );

                if ($arCompPhone = $dbCompPhone->Fetch())
                {
                    $arCompanies[$arCompany["ID"]][] = trim($arCompPhone["VALUE"]);
                }

                else
                {
                    $arCompanies[$arCompany["ID"]][] = "";
                }

                // выбираем email
                $dbCompEmail = CCrmFieldMulti::GetList(
                    array("ID" => "ASC"),
                    array(
                        "ELEMENT_ID" => $arCompany["ID"],
                        "TYPE_ID" => "EMAIL"
                    )
                );

                if ($arCompEmail = $dbCompEmail->Fetch())
                {
                    $arCompanies[$arCompany["ID"]][] = trim($arCompEmail["VALUE"]);
                }

                else
                {
                    $arCompanies[$arCompany["ID"]][] = "";
                }

                // если нет ни телефона, ни email, то исключаем данные из поиска
                if (empty($arCompanies[$arCompany["ID"]]["3"])
                    || empty($arCompanies[$arCompany["ID"]]["4"])
                )
                {
                    unset($arCompanies[$arCompany["ID"]]);
                }
            }
        }

        // проверяем историю изменения лида, если ее нет - удаляем контакты из массива списка
        if(!empty($arLeads))
        {
            $dbEvents = CCrmEvent::GetListEx(
                array("ID" => "DESC"),
                array(
                    "ENTITY_TYPE" => "LEAD",
                    "ENTITY_ID" => array_keys($arLeads)
                ),
                false,
                false,
                array());

            while ($arEvent = $dbEvents->GetNext())
            {
                if (stripos($arEvent["EVENT_NAME"], "статус") !== false)
                {
                    if ($arEvent["EVENT_NAME"] == "LEAD" and $arEvent["ENTITY_ID"])
                    {
                        unset($arLeads[$arEvent["ENTITY_ID"]]);
                    }
                }
            }
        }

        if(!empty($arLeads))
        {
            foreach($arLeads as $leadKeyId => $companyValueId)
            {
                if(isset($arCompanies[$companyValueId]))
                {
                    unset($arCompanies[$companyValueId]);
                }
            }
        }

        if(!empty($arCompanies))
        {
            // выбираем контрагентов, у которых есть привязка к ккт
            $arSelectReestrKkt = array("PROPERTY_COMPANY");
            $arFilterReestrKkt = array(
                "IBLOCK_CODE" => self::REESTR_KKT_IBLOCK_CODE,
                "ACTIVE_DATE" => "Y",
                "ACTIVE" => "Y",
                "!PROPERTY_COMPANY" => array_keys($arCompanies)
            );

            $dbReestrKkt = CIBlockElement::GetList(
                array(),
                $arFilterReestrKkt,
                false,
                false,
                $arSelectReestrKkt
            );

            while ($arReestrKkt = $dbReestrKkt->GetNext())
            {
                // удаляем этих контрагентов из нашего списка (останутся только те, которые не привязанны к ккт)
                if (isset($arCompanies[$arReestrKkt["PROPERTY_COMPANY_VALUE"]]))
                    unset($arCompanies[$arReestrKkt["PROPERTY_COMPANY_VALUE"]]);
            }

            if (!empty($arCompanies))
            {
                // добавляем соответствующих лидов в список Unisender
                foreach($arCompanies as $company)
                {
                    // дата контрагента
                    $companyDate = date("Y-m-d%20h:i:s");
                    if ($company["2"])
                        $companyDate = FormatDate("Y-m-d%20h:i:s", MakeTimeStamp($company["2"]));

                    // телефон лида
                    $companyPhone = "";
                    if ($company["3"])
                        $companyPhone = $company["3"];

                    // email контрагента
                    $companyEmail = "";
                    if ($company["4"])
                        $companyEmail = $company["4"];

                    $companyName = "";
                    if ($company["1"])
                        $companyName = urlencode($company["1"]);

                    // проверяем наличие всех обязательных параметров
                    if ($companyName and ($companyEmail or $companyPhone))
                    {
                        // параметры запроса
                        $params = "&confirm_time=" . $companyDate . "&request_time=" . $companyDate . "&request_ip=&confirm_ip=&list_ids=" . self::LIST_ID_THIRD . "&fields[email]=" . $companyEmail . "&fields[phone]=" . $companyPhone . "&fields[Name]=" . $companyName;

                        $result = $uniSenderIntegration->action("addToList", $params);
                    }
                }
            }
        }

        $USER->Logout();
        return "ProcessingLead::CheckKontragent3Days();";
    }

    // метод обработки контрагентов, которые регистрировались по прямой ссылке
    public static function CheckKontragentStraightLink()
    {
        global $USER;
        $USER->Authorize(372);

        $res = \Bitrix\Main\Loader::includeModule("crm");

        $uniSenderIntegration = new UniSenderIntegration();

        // определяем ip лида
        $leadIp = "";

        // мин. и макс. даты для фильтрации
        $minDate = ConvertTimeStamp(time() - 300, "FULL");
        $maxDate = ConvertTimeStamp(time(), "FULL");

        // массив для всех контрагентов
        $arCompanies = array();
        // массив связки лидов и контрагентов
        $arLeads = array();

        $arCompanyOrder = array("ID" => "ASC");
        $arCompanyFilter = array(
            "!" . self::FIELD_CRM_INN => false,
            array(
                "LOGIC" => "AND",
                ">DATE_CREATE" => $minDate,
                "<=DATE_CREATE" => $maxDate
            )
        );

        $arCompanySelect = array(
            "ID",
            "TITLE",
            "DATE_CREATE",
            "LEAD_ID"
        );

        $dbCompanies = CCrmCompany::GetList($arCompanyOrder, $arCompanyFilter, $arCompanySelect);

        while ($arCompany = $dbCompanies->GetNext())
        {
            pre($arCompany);
            exit;
            if($arCompany["LEAD_ID"])
            {
                $arLeads[$arCompany["LEAD_ID"]] = $arCompany["ID"];
            }

            $arCompanies[$arCompany["ID"]] = array(
                $arCompany["ID"],
                $arCompany["TITLE"],
                $arCompany["DATE_CREATE"]
            );

            // выбираем телефон
            $dbCompPhone = CCrmFieldMulti::GetList(
                array("ID" => "ASC"),
                array(
                    "ELEMENT_ID" => $arCompany["ID"],
                    "TYPE_ID" => "PHONE"
                )
            );

            if ($arCompPhone = $dbCompPhone->Fetch())
            {
                $arCompanies[$arCompany["ID"]][] = trim($arCompPhone["VALUE"]);
            }

            else
            {
                $arCompanies[$arCompany["ID"]][] = "";
            }

            // выбираем email
            $dbCompEmail = CCrmFieldMulti::GetList(
                array("ID" => "ASC"),
                array(
                    "ELEMENT_ID" => $arCompany["ID"],
                    "TYPE_ID" => "EMAIL"
                )
            );

            if ($arCompEmail = $dbCompEmail->Fetch())
            {
                $arCompanies[$arCompany["ID"]][] = trim($arCompEmail["VALUE"]);
            }

            else
            {
                $arCompanies[$arCompany["ID"]][] = "";
            }

            // если нет ни телефона, ни email, то исключаем данные из поиска
            if (empty($arCompanies[$arCompany["ID"]]["3"])
                || empty($arCompanies[$arCompany["ID"]]["4"])
            )
            {
                unset($arCompanies[$arCompany["ID"]]);
            }
        }

        // проверяем историю изменения лида, если статус менялся, то удаляем эту компанию
        if (!empty($arLeads))
        {
            $dbEvents = CCrmEvent::GetListEx(
                array("ID" => "DESC"),
                array(
                    "ENTITY_TYPE" => "LEAD",
                    "ENTITY_ID" => array_keys($arLeads)
                ),
                false,
                false,
                array()
            );

            while ($arEvent = $dbEvents->GetNext())
            {
                if (stripos($arEvent["EVENT_NAME"], "статус") !== false)
                {
                    if ($arEvent["EVENT_NAME"] == "LEAD" and $arEvent["ENTITY_ID"])
                    {
                        unset($arLeads[$arEvent["ENTITY_ID"]]);
                    }
                }
            }
        }

        if (!empty($arLeads))
        {
            foreach($arLeads as $leadKeyId => $companyValueId)
            {
                if (!isset($arCompanies[$companyValueId]))
                {
                    unset($arCompanies[$companyValueId]);
                }
            }
        }

        if (!empty($arCompanies))
        {
            // добавляем соответствующих лидов в список Unisender
            foreach($arCompanies as $company) {
                // дата контрагента
                $companyDate = date("Y-m-d%20h:i:s");
                if ($company["2"])
                    $companyDate = FormatDate("Y-m-d%20h:i:s", MakeTimeStamp($company["2"]));

                // телефон лида
                $companyPhone = "";
                if ($company["3"])
                    $companyPhone = $company["3"];

                // email контрагента
                $companyEmail = "";
                if ($company["4"])
                    $companyEmail = $company["4"];

                $companyName = "";
                if ($company["1"])
                    $companyName = urlencode($company["1"]);

                // проверяем наличие всех обязательных параметров
                if ($companyName and ($companyEmail or $companyPhone))
                {
                    // параметры запроса
                    $params = "";
                    $params = "&confirm_time=" . $companyDate . "&request_time=" . $companyDate . "&request_ip=&confirm_ip=&list_ids=" . self::LIST_ID_FOURTH . "&fields[email]=" . $companyEmail . "&fields[phone]=" . $companyPhone . "&fields[Name]=" . $companyName;

                    $result = $uniSenderIntegration->action("addToList", $params);
                }
            }
        }
    }

    public static function processStateLeadOnPause(){
        global $USER;
        $USER->Authorize(372);

        CModule::IncludeModule('crm');

        $resLeads = CCrmLead::GetList(array(), array("UF_STATE_LEAD" => getIdElementListState(self::STATE_LEAD_PAUSE), "TITLE" => "регистрация на сайте www.1ofd.ru"));

        while($arLead = $resLeads->Fetch()){

            //Проверка наличия ИНН. Если нет ИНН, значит TemproryAccount не пришел
            if(empty($arLead[self::FIELD_INN])){

                //Если прошло 5 рабочих дней с момента изменения лида
                $countWorkDays = self::getWorkingDays($arLead['DATE_MODIFY'], date('Y-m-d H:i:s'));
                if($countWorkDays > self::WORK_DAYS_TEMP_ACC) {
                    //Переводим лид в состояние Активный
                    $resUpdateLead = self::updateLead($arLead['ID'], array("UF_STATE_LEAD" => getIdElementListState(self::STATE_LEAD_ACTIVITY)));

                    //Добавляем дело
                    $resCrmActivity  = self::addCRMActivity(
                        'Звонок: Регистрация в ЛК',
                        'Звонок: Регистрация в ЛК',
                        $arLead['ID'],
                        \CCrmOwnerType::Lead,
                        $arLead['ASSIGNED_BY_ID'],
                        true
                    );
                }

                continue;
            }

            /** Поиск компании по ИНН */
            $requisite = new \Bitrix\Crm\EntityRequisite();
            $fieldsInfo = $requisite->getFormFieldsInfo();
            $select = array_keys($fieldsInfo);

            $arFilter = array(
                'RQ_INN' => $arLead[self::FIELD_INN],
                'ENTITY_TYPE_ID' => 4 //Company
            );

            $res = $requisite->getList(
                array(
                    'order' => array('SORT' => 'ASC', 'ID' => 'ASC'),
                    'filter' => $arFilter,
                    'select' => $select
                )
            );

            //Если нет компании с привязкой по ИНН, значит FullAccaunt не пришел
            if(intval($res->getSelectedRowsCount()) <= 0){

                //Если прошло 5 рабочих дней с момента изменения лида
                $countWorkDays = self::getWorkingDays($arLead['DATE_MODIFY'], date('Y-m-d H:i:s'));
                if($countWorkDays > self::WORK_DAYS_FULL_ACC) {
                    //Переводим лид в состояние Активный
                    $resUpdateLead = self::updateLead($arLead['ID'], array("UF_STATE_LEAD" => getIdElementListState(self::STATE_LEAD_ACTIVITY)));

                    //Добавляем дело
                    $resCrmActivity  = self::addCRMActivity(
                        'Звонок: Подключение ККТ',
                        'Звонок: Подключение ККТ',
                        $arLead['ID'],
                        \CCrmOwnerType::Lead,
                        $arLead['ASSIGNED_BY_ID'],
                        true
                    );
                }

                continue;
            }

            //Компания найдена
            CModule::IncludeModule('iblock');
            $isKKT = false;
            while ($arCompany = $res->fetch()) {
                $companyID = $arCompany['ENTITY_ID'];

                //Поиск ККТ по ID компании
                $IBLOCK_ID = getIblockIDByCode('registry_kkt');
                $arSelect = Array("ID");
                $arFilter = Array("IBLOCK_ID" => $IBLOCK_ID, "PROPERTY_COMPANY" => $companyID);
                $resArKKT = CIBlockElement::GetList(Array("ID"=>"DESC"), $arFilter, false, false, $arSelect);

                if (intval($resArKKT->SelectedRowsCount()) > 0){
                    $isKKT = true;
                    break;
                }
            }

            //Если нет ККТ
            if(empty($isKKT)) {

                $countWorkDays = self::getWorkingDays($arLead['DATE_MODIFY'], date('Y-m-d H:i:s'));
                //Если прошло 7 рабочих дней с момента изменения лида
                if($countWorkDays > self::WORK_DAYS_ADD_KKT) {
                    //Переводим лид в состояние Активный
                    $resUpdateLead = self::updateLead($arLead['ID'], array("UF_STATE_LEAD" => getIdElementListState(self::STATE_LEAD_ACTIVITY)));

                    //Добавляем дело
                    $resCrmActivity  = self::addCRMActivity(
                        'Звонок: Подключение ККТ',
                        'Звонок: Подключение ККТ',
                        $arLead['ID'],
                        \CCrmOwnerType::Lead,
                        $arLead['ASSIGNED_BY_ID'],
                        true
                    );
                }

                continue;
            }
        }

        return "ProcessingLead::processStateLeadOnPause();";
    }

    private function addCRMActivity($subject, $description, $owner_id, $owner_type, $responsible_id, $sonet_event_register) {
        CModule::IncludeModule('crm');

        $activityFields = array(
            'TYPE_ID' => \CCrmActivityType::Call,
            'SUBJECT' => $subject,
            'COMPLETED' => 'N',
            'PRIORITY' => \CCrmActivityPriority::Medium,
            'DESCRIPTION' => $description,
            'DESCRIPTION_TYPE' => \CCrmContentType::PlainText,
            'LOCATION' => '',
            'DIRECTION' => \CCrmActivityDirection::Outgoing,
            'NOTIFY_TYPE' => \CCrmActivityNotifyType::None,
            'SETTINGS' => array(),
            'STORAGE_TYPE_ID' => \CCrmActivity::GetDefaultStorageTypeID(),
            'START_TIME' => ConvertTimeStamp(false, "FULL", false, false),
            'OWNER_ID' => $owner_id,
            'OWNER_TYPE_ID' => $owner_type,
            'RESPONSIBLE_ID' => $responsible_id,
            'BINDINGS' => '',
        );

        if (\CCrmActivity::Add($activityFields, false, true, array('REGISTER_SONET_EVENT' => $sonet_event_register))) {
            return true;
        } else {
            return false;
        }
    }

    private function updateLead($idLead, $arFields) {
        CModule::IncludeModule('crm');

        $oLead = new CCrmLead;
        $resUpdate = $oLead->Update($idLead, $arFields);
        if($resUpdate) {
            return true;
        } else {
            return false;
        }
    }

    private function getWorkingDays($startDate, $endDate, $holidays = array()){
        $endDate = strtotime($endDate);
        $startDate = strtotime($startDate);

        $days = ($endDate - $startDate) / 86400 + 1;

        $no_full_weeks = floor($days / 7);
        $no_remaining_days = fmod($days, 7);

        $the_first_day_of_week = date("N", $startDate);
        $the_last_day_of_week = date("N", $endDate);

        if ($the_first_day_of_week <= $the_last_day_of_week) {
            if ($the_first_day_of_week <= 6 && 6 <= $the_last_day_of_week) $no_remaining_days--;
            if ($the_first_day_of_week <= 7 && 7 <= $the_last_day_of_week) $no_remaining_days--;
        }
        else {
            if ($the_first_day_of_week == 7) {
                $no_remaining_days--;

                if ($the_last_day_of_week == 6) {
                    $no_remaining_days--;
                }
            }
            else {
                $no_remaining_days -= 2;
            }
        }

        $workingDays = $no_full_weeks * 5;
        if ($no_remaining_days > 0 )
        {
            $workingDays += $no_remaining_days;
        }

        foreach($holidays as $holiday){
            $time_stamp=strtotime($holiday);
            if ($startDate <= $time_stamp && $time_stamp <= $endDate && date("N",$time_stamp) != 6 && date("N",$time_stamp) != 7)
                $workingDays--;
        }

        return $workingDays;
    }

    // метод изменяющий состояние лида
    public function ChangeStateLead($arFields)
    {
        \Bitrix\Main\Loader::includeModule("crm");

        // проверяем менялся ли ответственный
        $changeOwner = false;

        $dbEvents = CCrmEvent::GetListEx(array("ID" => "DESC"), array("ENTITY_TYPE" => "LEAD", "ENTITY_ID" => $arFields["ID"]), false, false, array());

        while($arEvent = $dbEvents->GetNext())
        {
            if($arEvent["EVENT_NAME"] == 'Изменено поле &quot;Ответственный&quot;')
            {
                $changeOwner = true;
                break;
            }
        }

        // получаем статус лида
        $arOrderLead = array();
        $arFilterLead = array(array("ID" => $arFields["ID"]));
        $arSelectLead = array("STATUS_ID");

        $leadData = "";
        $leadProcessing = false;

        $dbLeads = CCrmLead::GetList($arOrderLead, $arFilterLead, $arSelectLead);
        while($arLead = $dbLeads->GetNext())
        {
            if($arLead["STATUS_ID"]) $leadData = $arLead["STATUS_ID"];
        }

        // делаем проверку статуса
        switch($leadData)
        {
            case "1":
            case "CONVERTED":
            case "JUNK":
            case "JUNK":
            case "ON_HOLD":
            case "RESTORED":
                $leadProcessing = true;
                break;
        }

        // если лид обработан, то меняем у него состояние на "Обработан"
        if($leadProcessing)
        {

            $leadStateId = getIdElementListState("XML_STATE_LEAD_PROCESSED");
            if($leadStateId)
                SetUserField("CRM_LEAD", (int)$arFields["ID"], "UF_STATE_LEAD", $leadStateId);
        }
    }
}



/*
 * функция из файла local/php_interface/events/invoice/handler.php
 * Обновление номера счета, добавляется постфикс с номером года ХХХХ-ХХХХХХ (2018),
 * год берется от даты выставления счета DATE_BILL
 */

function updateAccountNumber($arFields) {

    if(empty($arFields['ID']) || !is_int($arFields['ID']) || isset($arFields["CUSTOM_UPDATE"]))
    {
        return false;
    }

    if (!CModule::IncludeModule('crm'))
    {
        return false;
    }

    if(isset($arFields["CUSTOM_UPDATE"])) {
        unset($arFields["CUSTOM_UPDATE"]);
        return false;
    }

    $CCrmInvoice = new CAllCrmInvoice(false);
    $dbRes = $CCrmInvoice->GetList(array(), array("ID" => $arFields['ID'], "CHECK_PERMISSIONS" => "N"));
    if($arRes = $dbRes->Fetch())
    {
        if(!empty($arRes['DATE_BILL'])){
            $year = date("Y", strtotime($arRes['DATE_BILL']));

            //Проверяем номер счета на наличие номера года.
            //Если нету - добавляем
            //Если есть - ничего не делаем
            if(strpos($arRes['ACCOUNT_NUMBER'], "($year)") === false && $year != 2017){
                //Удаляем постфикс с номером года
                $arTemp = explode(' (', $arRes['ACCOUNT_NUMBER']);
                $numberAccountNotDate = trim($arTemp[0]);

                $numberAccountWithDate = $numberAccountNotDate . " (" . $year . ")";
                $arFieldsInvoice['ACCOUNT_NUMBER'] = $numberAccountWithDate;
                $arFieldsInvoice['CUSTOM_UPDATE'] = "Y";
                $CCrmInvoice->Update($arFields['ID'], $arFieldsInvoice);
            }
        }
    }
}


/*
 * функция для обновления товарных позиций проблемного счета
 * решает проблему с
 */

function updateProblemeInvoice($arFields) {
    Bitrix\Main\Loader::includeModule('sale');
    Bitrix\Main\Loader::includeModule('crm');
    $logger = Logger::getLogger('updateProblemeInvoice');
    $logger->log('updateProblemeInvoice $arFields[] = ' . print_r($arFields, true));
    if(empty($arFields['ID']))
    {
        //$logger->log('updateProblemeInvoice Не установлено $arFields["ID"]');
        return false;
    }

    if (!CModule::IncludeModule('crm'))
    {
        //$logger->log('updateProblemeInvoice !CModule::IncludeModule(crm)');
        return false;
    }

    $CCRmInvoice = new CCRmInvoice();
    $CSaleBasket = new CSaleBasket();
    $orderID = $arFields['ID'];
    //$logger->log('$orderID = ' . $orderID);
    $order = \Bitrix\Sale\Order::load($orderID);
    $shipmentCollection = $order->getShipmentCollection();
    $onlySystemShipment = true;
    foreach ($shipmentCollection as $shipment)
    {
        if (!$shipment->isSystem())
            $onlySystemShipment = false;
    }
    if($onlySystemShipment){
        $logger->log('Только системная доставка для счета $invoiceID = ' . $orderID);
        $arInvoice = $CCRmInvoice->GetByID($orderID);
        $paySystemID = $arInvoice["PAY_SYSTEM_ID"];
        if($paySystemID == 5) { //Внутренний счет
            $paySystemID = Reports::updateInvoicePaySystemID($orderID);
        }
        if($paySystemID){
            $arUpdFields = array(
                "PERSON_TYPE_ID" => $arInvoice["PERSON_TYPE_ID"],
                "PAY_SYSTEM_ID" => $paySystemID,
                "INVOICE_PROPERTIES" => Reports::getInvoiceProp($orderID, $arInvoice["PERSON_TYPE_ID"]),
            );
            $dbBasketItems = $CSaleBasket->GetList(
                array(
                    "NAME" => "ASC",
                    "ID" => "ASC"
                ),
                array(
                    "ORDER_ID" => $orderID
                ),
                false,
                false,
                array(
                    "ID",
                    "PRODUCT_ID",
                    "NAME",
                    "QUANTITY",
                    "PRICE",
                    "VAT_INCLUDED",
                    "VAT_RATE",
                    "DISCOUNT_PRICE",
                    "MEASURE_CODE",
                    "MEASURE_NAME",
                    "CUSTOM_PRICE",
                    "SORT",
                )
            );
            if($dbBasketItems->SelectedRowsCount() > 0){
                while ($arItem = $dbBasketItems->Fetch()) {
                    $arUpdFields["PRODUCT_ROWS"][] = array(
                        "ID" => 0,
                        "PRODUCT_ID" => $arItem["PRODUCT_ID"],
                        "PRODUCT_NAME" => $arItem["NAME"],
                        "QUANTITY" => $arItem["QUANTITY"],
                        "PRICE" => $arItem["PRICE"],
                        "~PRICE" => $arItem["PRICE"],
                        "VAT_INCLUDED" => $arItem["VAT_INCLUDED"],
                        "VAT_RATE" => $arItem["VAT_RATE"],
                        "DISCOUNT_PRICE" => $arItem["DISCOUNT_PRICE"],
                        "MEASURE_CODE" => $arItem["MEASURE_CODE"],
                        "MEASURE_NAME" => $arItem["MEASURE_NAME"],
                        "CUSTOMIZED" => $arItem["CUSTOM_PRICE"],
                        "SORT" => $arItem["SORT"],
                    );
                }
                $arUpdFields["CUSTOM_UPDATE"] = "Y";
                if(isset($arFields["CUSTOM_UPDATE"]) && $arFields["CUSTOM_UPDATE"] == "Y"){
                    //$logger->log('Повторное событие на изменение счета $invoiceID = ' . $orderID);
                } else {
                    $logger->log('updateProblemeInvoice $orderID = '.$orderID.' | $arUpdFields[] = ' . print_r($arUpdFields, true));
                    /*if ($resUpd = $CCRmInvoice->update($orderID, $arUpdFields)) {
                        $logger->log('УСПЕШНО обновление счета $invoiceID = ' . $orderID);
                    } else {
                        $logger->log('ОШИБКА обновления счета $invoiceID = ' . $orderID . ' | $CCRmInvoice->LAST_ERROR = ' . $CCRmInvoice->LAST_ERROR);
                    }*/
                }
            }
        }
    } else {
        //$logger->log('Проблем с доставкой не обнаружено $invoiceID = ' . $orderID);
    }
}


/*
 * Обновление данных обращения, при изменение лида
*/
function updateObracheniye($arFields)
{
    $logger = Logger::getLogger('updateTreatment');
    $logger->log('updateTreatment $arFields = ' . print_r($arFields, true));
    //Проверяем, не вызвано ли событие обновления лида при обновление обращения, связанного с этим лидом
    //в случае вызова события при обновление обращение в параметре $arFields["UPDATE_OBRACHENIYA"] передается значение Y
    if(empty($arFields["UPDATE_OBRACHENIYA"])){
        $leadID = $arFields["ID"];
        $CCRmLead = new CCrmLead();
        $el = new CIBlockElement();
        $rsLead = $CCRmLead->GetList(
            array("ID" => "ASC"),
            array("ID" => $leadID),
            array("ID", "ASSIGNED_BY_ID", "STATUS_ID", "UF_OBRACHENIYE_ID", "UF_CLOSURE_TEXT", "UF_RESULT")
        );
        $arLead = $rsLead->Fetch();
        $logger->log('updateTreatment $arLead = ' . print_r($arLead, true));
        //ID обращения
        $elemID = $arLead["UF_OBRACHENIYE_ID"];
        if($elemID > 0){
            //Причина отмены по лиду XML_ID
            $reasonClosureXML = $arLead["UF_CLOSURE_TEXT"];
            //Решение
            $resheniyeText = $arLead["UF_CLOSURE_TEXT"];
            $leadStatusID = $arLead["STATUS_ID"];
            $rsElem = $el->GetList(
                array("ID" => "ASC"),
                array("ID" => $elemID),
                false,
                false,
                array("ID", "IBLOCK_ID", "NAME")
            );
            if($ob = $rsElem->GetNextElement()){
                $arElem = $ob->GetFields();
                $arElemProps = $ob->GetProperties();
                $logger->log('updateTreatment $arElem = ' . print_r($arElem, true));
                $arUpdProps = array();

                //Сверяем статусы лида и обращения
                //Массив с соответсвием статусов ЛИДА и ОБРАЩЕНИЯ
                $arConformityStatusLidObracheniye = array(
                    "4" =>          "AE5F2F10-F46B-1410-FD9A-0050BA5D6C38", //Новый -> Новое обращение
                    "5" =>          "7E9F1204-F46B-1410-FB9A-0050BA5D6C38", //В работе -> В работе
                    "6" =>          "3E7F420C-F46B-1410-FC9A-0050BA5D6C38", //Консультация оказана -> Задача закрыта
                    "CONVERTED" =>  "3E7F420C-F46B-1410-FC9A-0050BA5D6C38", //Создать на основании... -> Задача закрыта
                    "JUNK" =>       "6E5F4218-F46B-1410-FE9A-0050BA5D6C38", //Не успешно -> Отменено
                );
                //Получаем информация по свойству "Статус обращения" из ИБ по данным статуса лида
                $arStatusObracheniya = getIblockPropEnum($arElem["IBLOCK_ID"], "STATUS_OBRASHCHENIYA", "XML_ID", $arConformityStatusLidObracheniye[$leadStatusID]);
                $logger->log('updateTreatment $arStatusObracheniya = ' . print_r($arStatusObracheniya, true));
                //Проверяем, совпадают ли статусы Лида и Обращения.
                if($arStatusObracheniya["ID"] != $arElemProps["STATUS_OBRASHCHENIYA"]["VALUE_ENUM_ID"])
                {
                    $arUpdProps["STATUS_OBRASHCHENIYA"] = $arStatusObracheniya["ID"];
                    if($leadStatusID != "JUNK"){
                        $arUpdProps["REASON_CLOSURE"] = '';
                    }
                    if($leadStatusID == "6" || $leadStatusID == "CONVERTED" || $leadStatusID == "JUNK") {
                        $arUpdProps["TEKST_SOOBSHCHENIYA"] = $arLead["UF_RESULT"];
                    }
                    if($leadStatusID == "4" || $leadStatusID == "5") {
                        $arUpdProps["TEKST_SOOBSHCHENIYA"] = '';
                    }
                }

                //Сверяем "Причина отмены" лида и Обращения
                if(!empty($reasonClosureXML)){
                    $arReasonClosure = getIblockPropEnum($arElem["IBLOCK_ID"], "REASON_CLOSURE", "XML_ID", $reasonClosureXML);
                    $logger->log('updateTreatment $arReasonClosure = ' . print_r($arReasonClosure, true));
                    if($arReasonClosure["ID"] != $arElem["PROPERTY_REASON_CLOSURE_ENUM_ID"])
                    {
                        $arUpdProps["REASON_CLOSURE"] = $arReasonClosure["ID"];
                    }
                }

                //Сверяем "Ответственных" лида и Обращения
                if($arLead["ASSIGNED_BY_ID"] != $arElemProps["Owner_display"]["VALUE"] && !empty($arElemProps["Owner_display"]["VALUE"]))
                {
                    $arUpdProps["Owner_display"] = $arLead["ASSIGNED_BY_ID"];
                }


                if(!empty($arUpdProps)){
                    $arUpdObracheniye = array(
                        "ACTIVE" => "Y",
                        "UPD_FROM_LEAD" => "Y",
                    );
                    $logger->log('updateTreatment $arUpdProps = ' . print_r($arUpdProps, true));
                    $el->SetPropertyValuesEx($arElem["ID"], $arElem["IBLOCK_ID"], $arUpdProps);
                    $logger->log('updateTreatment $arUpdObracheniye = ' . print_r($arUpdObracheniye, true));
                    if($res = $el->Update($arElem["ID"], $arUpdObracheniye)){
                        $logger->log("Обращение с ID = $elemID успешно обновлено");
                    } else {
                        $logger->log("Ошибка обновления обращения с ID = $elemID. LAST_ERROR = ".$el->LAST_ERROR);
                    }
                }
            }
        }
    }
}