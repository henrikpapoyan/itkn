<?php

class CatalogHandler
{
    public function setMeasureNdsForProduct(&$arFields)
    {
        if (!CModule::IncludeModule('iblock')) {
            return;
        }

        if (empty($arFields["VAT_ID"])) {
            $vatData = CIBlockElement::GetProperty(GOODS_IBLOCK_ID, $arFields["ID"], [],
                ["CODE" => VAT_PROPERTY_CODE])->Fetch();
            if (defined($constName = 'NDS_VAT_' . $vatData["VALUE"])) {
                $arFields["VAT_ID"] = constant($constName);
            }
        }
        
        if (!empty($arFields["VAT_ID"])) {
            $arFields["VAT_INCLUDED"] = "Y";
        } else {
            $arFields["VAT_INCLUDED"] = "N";
        }
        
        if (empty($arFields["MEASURE"])) {
            $measureData = CIBlockElement::GetProperty(GOODS_IBLOCK_ID, $arFields["ID"], [],
                ["CODE" => MEASURE_PROPERTY_CODE])->Fetch();
            $arFields["MEASURE"] = $measureData["DESCRIPTION"] ?: DEFAULT_MEASURE_ID;
        }
    }
    
    public function setMeasureNdsForProductUpdate($id, &$arFields)
    {
        $arFields["ID"] = $id;
        self::setMeasureNdsForProduct($arFields);
        unset($arFields["ID"]);
    }
    
}