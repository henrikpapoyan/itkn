<?
function OnFillSocNetMenuHandlerGK(&$arResult, $arParams)
{
    if ($arParams ['ENTITY_TYPE'] == 'G' && $arParams ['ENTITY_ID'] == 26) {
        $loggerLine2 = Logger::getLogger("Line2", "Line2/menu.log");
        $loggerLine2->log("arResult=[".print_r($arResult, true));
        $arResult['CanView']['group26_list77'] = true;
        $arResult['Title']['group26_list77'] = "Обращения";
        $arResult['Urls']['group26_list77'] = "/workgroups/group/26/lists/77/view/0/";
        $userOptionsCategory = 'ui';
        $name = 'group_panel_menu_26';
        $setting = CUserOptions::GetOption($userOptionsCategory, $name);
        $arSetting = json_decode($setting['settings'], true);
        $loggerLine2->log( "arSetting=[" . print_r($arSetting, true));
        $group26_list77 = -1;
        $c = 0;
        foreach ($arSetting as $key => $val) {
            if ($key == 'group_panel_menu_26_group26_list77') {
                $group26_list77 = $c;
                $c++;
            }
        }
        if ($group26_list77 == -1) {
            $newSetting = array();
            $c = 0;
            if (count($arSetting) > 5)
            {

                foreach ($arSetting as $key => $val) {
                    if ($c == 5) {
                        $newSetting['group_panel_menu_26_group26_list77'] = array('sort' => $c,
                            'isDisabled' => '');
                        $c++;
                        continue;
                    }

                    $newSetting[$key] = $val;
                    $newSetting[$key]['sort'] = $c;
                    $c++;
                }
            }
            else
            {
                $newSetting = $arSetting;
                if (!isset($arSetting['group_panel_menu_26_group26_list77']))
                {
                    $newSetting['group_panel_menu_26_group26_list77'] = array('sort' => 5,
                        'isDisabled' => '');
                }
            }
            $setting['settings'] = json_encode($newSetting);
            CUserOptions::SetOption($userOptionsCategory, $name, $setting);
        }

        $loggerLine2->log("2arResult=[".print_r($arResult, true));
    }
    return true;
}