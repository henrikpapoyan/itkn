<?php
/**
 * Created by PhpStorm.
 * User: Rustam Atnabaev
 * Date: 03.04.2018
 * Time: 19:49
 */
/*
Методы по обновлению свойств инфоблоков (“Закрывающие документы УПД” и “Закрывающие документы АВР”) и обновление информации по Сделке.
dealAddPropertyIBlock() отвечает за обновление свойст ИБ “Закрывающие документы УПД” и “Закрывающие документы АВР”
AddPropertyIBlockDeal() отвечает за обновление пользовательского свойства Сделки
*/

use Bitrix\Main\Loader;

Loader::includeModule("iblock");
Loader::includeModule("crm");

class DealNew
{
    static protected $ibIDArray = array(98, 99);

    static protected $typ = array(
        '22',    //Форма запроса ФН на сайте 1-ofd	 	30	N
        '23',     //Форма запроса сертификата на сайте 1-ofd	 	40	N
        '37',     // Форма покупки ККТ на сайте 1-ofd	 	430	N
        '41',    //Форма аренды ККТ на сайте 1-ofd	 	450	N
        '38',      //Форма запроса заказа доп. услуг на сайте 1-ofd	 	440	N
        '44',     //Форма запроса на анализ (Биг дата)
        '42'	    //Форма запроса информации в чате
    );

    static protected function access($idIblock)
    {
        if (in_array($idIblock, self::$ibIDArray)){
            return true;
        }else{
            return false;
        }
    }

    public function dealAddPropertyIBlock (&$arFields){
        if (self::access($arFields['IBLOCK_ID'])){
            $oDeal = new CCrmDeal;
            $oElemIBlock = new CIBlockElement;

            $elementId = $arFields["ID"];
            $IBLOCK_ID = $arFields['IBLOCK_ID'];
            $arElSelect = Array("ID", "IBLOCK_ID", "PROPERTY_CONTRAGENT");

            $arElFilter = Array("IBLOCK_ID" => $IBLOCK_ID, "ID" => $elementId);
            $dbElem = $oElemIBlock->GetList(Array(), $arElFilter, false, false, $arElSelect);

            while ($ob = $dbElem->GetNextElement()) {
                $arProps = $ob->GetProperties();
                $idContragent = $arProps["CONTRAGENT"]['VALUE'];
                $idDEal[] = $arProps["DEAL"]['VALUE'];
            }

            $arFilter = array("COMPANY_ID"=>$idContragent);
            $arSelect = array('ID');
            $arOrder = array('DATE_CREATE' => 'DESC');
            $resDeal = $oDeal->GetList($arOrder, $arFilter, $arSelect, false);
            while ($arDeal = $resDeal->Fetch()) {
                $arrayDeal[] = $arDeal['ID'];

            }
            $PROPERTY_VALUES = array("DEAL"=>$arrayDeal);
            $oElemIBlock->SetPropertyValuesEx($elementId, $IBLOCK_ID, $PROPERTY_VALUES);
        }
    }

    public function AddPropertyIBlockDeal (&$arFields){
        $oDeal = new CCrmDeal;
        if(empty($arFields['LEAD_ID'])) {

            $arFilter = array("ID" => $arFields['ID']);
            $arSelect = array();
            $arOrder = array('DATE_CREATE' => 'DESC');
            $resDeal = $oDeal->GetList($arOrder, $arFilter, $arSelect, false);
            while ($arDeal = $resDeal->Fetch()) {
                $arrayDeal['LEAD_ID'] = $arDeal['LEAD_ID'];
            }
        }else{

            $arrayDeal['LEAD_ID'] = $arFields['LEAD_ID'];
        }

        if(!empty($arrayDeal['LEAD_ID'])){
            $leadDefolt = $arrayDeal['LEAD_ID'];
            $oLead = new CCrmLead;
            $resLead = $oLead->GetList(array('ID' => 'asc'), array(
                'ID' => $leadDefolt
            ));
            while ($arLead = $resLead->Fetch()) {
                $typ_lead = $arLead['SOURCE_ID'];
            }
            $zakaz = self::$typ;

            //выполняем индификацию принадлежности к лиду или заказу
            if (in_array($typ_lead, $zakaz)) {
                //заказ
                SetUserField('CRM_DEAL', $arFields['ID'], 'UF_CRM_ZAKAZ', $leadDefolt);
            } elseif (!in_array($typ_lead, $zakaz)) {
                //лид
                SetUserField('CRM_DEAL', $arFields['ID'], 'UF_CRM_LEAD', $leadDefolt);
            }

        }

        if(!empty($arFields['COMPANY_ID'])){
            $contactIDs = \Bitrix\Crm\Binding\ContactCompanyTable::getCompanyContactIDs($arFields['COMPANY_ID']);
            $arFieldsContact = array(
                'CONTACT_ID' => $contactIDs[0]
            );

            $oDeal->Update($arFields['ID'], $arFieldsContact, $bCompare = true, $bUpdateSearch = true, $options = array());

            $oElemIBlock = new CIBlockElement;
            $IBLOCK_ARRAY_ID = self::$ibIDArray;
            //получение данных закрывающих документов где указан контрагент сделки
            foreach ($IBLOCK_ARRAY_ID as $key=>$IBLOCK_ID){
                $arElSelect = Array("ID", "IBLOCK_ID", "PROPERTY_CONTRAGENT");
                $arElFilter = Array("IBLOCK_ID" => $IBLOCK_ID, "PROPERTY_CONTRAGENT" => $arFields['COMPANY_ID']);
                $dbElem = $oElemIBlock->GetList(Array(), $arElFilter, false, false, $arElSelect);

                while ($ob = $dbElem->GetNextElement()) {
                    $arFieldsOld = $ob->GetFields();
                    $arProps = $ob->GetProperties();

                    $idDoc[$arFieldsOld["ID"]]["DEAL"] = $arProps["DEAL"]['VALUE'];
                    $idDoc[$arFieldsOld["ID"]]["IBLOCK_ID"] = $arFieldsOld["IBLOCK_ID"];

                }
            }
            //проверка и привязка если не указана сделка в закрывающем документе
            if (array_keys($idDoc) and !array_key_exists(0, $idDoc)){
                foreach ($idDoc as $idEl=>$massEl){
                    if(!in_array($arFields['ID'],$massEl["DEAL"])){
                        array_push($massEl["DEAL"], $arFields['ID']);
                        $PROPERTY_VALUES = array($massEl["DEAL"]);
                        $oElemIBlock->SetPropertyValuesEx($idEl, $massEl["IBLOCK_ID"], $PROPERTY_VALUES);
                    }
                }
            }
        }

    }
}