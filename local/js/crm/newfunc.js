/**
 * Created by German on 17.08.2017.
 */
BX.ready(function(){
    let tempClosure = '';
    if(typeof(BX.CrmProcessTerminationDialog) !== "undefined") {
        BX.CrmProcessTerminationDialog.prototype._prepareContent = function () {
            this._wrapper = BX.create("DIV");
            var table = BX.create("TABLE",
                {
                    attrs: {className: "crm-list-end-deal-block"},
                    props: {cellSpacing: "0", cellPadding: "0", border: "0"}
                }
            );
            this._wrapper.appendChild(table);

            var cell = table.insertRow(-1).insertCell(-1);
            cell.className = "crm-list-end-deal-text";
            cell.innerHTML = this.getSetting("title", "");

            cell = table.insertRow(-1).insertCell(-1);
            cell.className = "crm-list-end-deal-buttons-block";

            var controls = this._terminationControl !== null
                ? this._terminationControl.prepareDialogControls(this)
                : null;

            if (!BX.type.isPlainObject(controls)) {
                controls = {};
            }
            if(this.getSetting('ufAuto', 'Y') == 'N') //GK
            {
                //START.
                var consultationText = "Консультация оказана";
                var consultationButton = BX.create(
                    "A",
                    {
                        attrs: {className: "webform-small-button webform-small-button-accept", href: "#"},
                        children: [
                            BX.create("SPAN", {attrs: {className: "webform-small-button-left"}}),
                            BX.create("SPAN", {
                                attrs: {className: "webform-small-button-text"},
                                text: consultationText
                            }),
                            BX.create("SPAN", {attrs: {className: "webform-small-button-right"}})
                        ]
                    }
                );
                cell.appendChild(consultationButton);

                //END.
                var consultationId = "consultation";
                BX.CrmSubscriber.subscribe(
                    this.getId() + "_" + consultationId,
                    consultationButton, "click", BX.delegate(this._onButtonClick, this),
                    BX.CrmParamBag.create({id: consultationId, preventDefault: true})
                );
            }

            if (BX.type.isElementNode(controls["successButton"])) {
                cell.appendChild(controls["successButton"]);
            }
            else {
                var success = this.getSetting("success");
                //GK
                if (success && this._id.substring(0, 41) != "CRM_INVOICE_LIST_V12_PROGRESS_BAR_INVOICE"
                    && this._id != "crm_invoice_show_v12_qpv_TERMINATION" ) {
                    var successText = BX.type.isNotEmptyString(success["name"]) ? success["name"] : "Success";
                    var successButton = BX.create(
                        "A",
                        {
                            attrs: {className: "webform-small-button webform-small-button-accept", href: "#"},
                            children: [
                                BX.create("SPAN", {attrs: {className: "webform-small-button-left"}}),
                                BX.create("SPAN", {
                                    attrs: {className: "webform-small-button-text"},
                                    text: successText
                                }),
                                BX.create("SPAN", {attrs: {className: "webform-small-button-right"}})
                            ]
                        }
                    );
                    cell.appendChild(successButton);
                    var successId = BX.type.isNotEmptyString(success["id"]) ? success["id"] : "success";
                    BX.CrmSubscriber.subscribe(
                        this.getId() + "_" + successId,
                        successButton, "click", BX.delegate(this._onButtonClick, this),
                        BX.CrmParamBag.create({id: successId, preventDefault: true})
                    );
                }
            }

            if (BX.type.isElementNode(controls["failureButton"])) {
                cell.appendChild(controls["failureButton"]);
            }
            else {
                var failure = this.getSetting("failure");
                if (failure) {
                    // Check if custom failure text is defined
                    var failureTitle = this.getSetting("failureTitle", "");
                    if(this.getSetting('ufAuto', 'Y') == 'N') //GK
                    {
                        failureTitle = 'Не успешно';
                    }
                    if (failureTitle === "") {
                        failureTitle = BX.type.isNotEmptyString(failure["name"]) ? failure["name"] : "Failure";
                    }
                    var failureButton = BX.create(
                        "A",
                        {
                            attrs: {className: "webform-small-button webform-small-button-decline", href: "#"},
                            children: [
                                BX.create("SPAN", {attrs: {className: "webform-small-button-left"}}),
                                BX.create("SPAN", {
                                    attrs: {className: "webform-small-button-text"},
                                    text: failureTitle
                                }),
                                BX.create("SPAN", {attrs: {className: "webform-small-button-right"}})
                            ]
                        }
                    );
                    cell.appendChild(failureButton);
                    var failureId = BX.type.isNotEmptyString(failure["id"]) ? failure["id"] : "failure";
                    BX.CrmSubscriber.subscribe(
                        this.getId() + '_' + failureId,
                        failureButton, "click", BX.delegate(this._onButtonClick, this),
                        BX.CrmParamBag.create({id: failureId, preventDefault: true})
                    );
                }
            }
            return this._wrapper;
        }
    }
    if(typeof(BX.CrmProgressControl) !== "undefined") {
        BX.CrmProgressControl.prototype.setCurrentStep = function (step) {
            this._closeTerminationDialog();

            if (this._isReadOnly || this._isFrozen) {
                return;
            }
            var stepIndex = this._findStepInfoIndex(step.getId());
            if (stepIndex < 0) {
                return;
            }
            if(this.getSetting('currentStepId','') == '6') //GK
            {
                return;
            }

            if ((stepIndex === (this._steps.length - 1)
                || step.getId() == 'CONVERTED') //GK
                && this._findStepInfoBySemantics("success")
                && this._findStepInfoBySemantics("failure")
                && step.getId() != '1' //GK
            ) {
                //User have to make choice
                this._openTerminationDialog();
                return;
            }

            if (this._currentStepId !== step.getId()) {
                this._currentStepId = step.getId();
                this._layout();
                this._save();
            }
        }
        BX.CrmProgressControl.prototype._openTerminationDialog = function () {
            this._enableStepHints(false);

            if (this._terminationDlg) {
                this._terminationDlg.close();
                this._terminationDlg = null;
            }

            var apologies = this._findAllStepInfoBySemantics("apology");
            this._terminationDlg = BX.CrmProcessTerminationDialog.create(
                (this._id + "_TERMINATION"),
                BX.CrmParamBag.create(
                    {
                        "title": this._manager.getMessage("dialogTitle"),
                        //"apologyTitle": this._manager.getMessage("apologyTitle"),
                        "failureTitle": apologies.length > 0 ? this._manager.getMessage("failureTitle") : "",
                        "anchor": this._container,
                        "success": this._findStepInfoBySemantics("success"),
                        "failure": this._findStepInfoBySemantics("failure"),
                        "apologies": apologies,
                        "callback": BX.delegate(this._onTerminationDialogClose, this),
                        "terminationControl": this._terminationControl,
                        "ufAuto": this.getSetting("ufAuto",'Y') //GK
                    }
                )
            );
            this._terminationDlg.open();

        }
        BX.CrmProgressControl.prototype._openFailureDialog = function()
        {
            this._enableStepHints(false);

            if(this._failureDlg)
            {
                this._failureDlg.close();
                this._failureDlg = null;
            }

            var currentStepIndex = this._findStepInfoIndex(this._currentStepId);
            var info = currentStepIndex >= 0 ? this._stepInfos[currentStepIndex] : null;
            var initValue = info ? info["id"] : "";

            var apologies = this._findAllStepInfoBySemantics("apology");
            this._failureDlg = BX.CrmProcessFailureDialog.create(
                (this._id + "_FAILURE"),
                BX.CrmParamBag.create(
                    {
                        //"title": this._manager.getMessage("dialogTitle"),
                        "entityType": this._entityType,
                        "entityId": this._entityId,
                        "initValue": initValue,
                        "failureTitle": apologies.length > 0 ? this._manager.getMessage("failureTitle") : "",
                        "selectorTitle": this._manager.getMessage("selectorTitle"),
                        "anchor": this._container,
                        "success": this._findStepInfoBySemantics("success"),
                        "failure": this._findStepInfoBySemantics("failure"),
                        "apologies": apologies,
                        "callback": BX.delegate(this._onFailureDialogClose, this),
                        "ufAuto": this.getSetting("ufAuto",'Y'), //GK
                        "consultation": this.getSetting("consultation", false)
                    }
                )
            );
            this._failureDlg.open();
        }
        BX.CrmProcessFailureDialog.prototype.open = function()
        {
            //if(this.getSetting('ufAuto', 'Y') == 'N') //GK
            //{
            //    this._bid = 'accept';
            //    this._value = 'JUNK';
            //    this._onAcceptButtonClick();
            //    return;
            //}
            console.log('this = ', this);
            if(this._popup)
            {
                this._popup.show();
                return;
            }
            var pc = '';
            var pt = '';
            var abc ='';
            if (this.getSetting('ufAuto', 'Y') == 'N')
            {
                pc = this._prepareContentGK();
                console.log('pc = ', pc);
                //pt = this._prepareTitleGK();
                abc = this._onAcceptButtonClick;
            }
            else
            {
                pc = this._prepareContent();
                pt = this._prepareTitle();
                abc = this._onAcceptButtonClick;
            }
            console.log('abc = ', abc);
            this._popup = new BX.PopupWindow(
                this._id,
                this.getSetting("anchor"),
                {
                    closeByEsc: true,
                    autoHide: true,
                    offsetLeft: -50,
                    closeIcon: true,
                    className: "crm-list-fail-deal",
                    titleBar: { content: pt }, //GK
                    content: pc, //GK
                    events: { onPopupClose: BX.delegate(this._onPopupClose, this) },
                    buttons:
                        [
                            new BX.PopupWindowButton(
                                {
                                    text: BX.message["JS_CORE_WINDOW_SAVE"],
                                    className: "popup-window-button-accept",
                                    events: { click: BX.delegate(abc, this) }//GK
                                }
                            ),
                            new BX.PopupWindowButtonLink(
                                {
                                    text: BX.message["JS_CORE_WINDOW_CANCEL"],
                                    className: "popup-window-button-link-cancel",
                                    events: { click: BX.delegate(this._onCancelButtonClick, this) }
                                }
                            )
                        ]
                }
            );
            this._popup.show();
        }
        BX.CrmProcessFailureDialog.prototype._prepareContentGK = function()
        {
            var wrapper = this._wrapper = BX.create("DIV", { attrs: { className: "crm-list-fail-deal-block" } });
            var title = this.getSetting("title", "");
            if(title !== "")
            {
                wrapper.appendChild(BX.create("DIV", { attrs: { className: "crm-list-end-deal-text" }, text: title }));
            }

            this._radioButtonBlock = BX.create("DIV", { attrs: { className: "crm-list-end-deal-block-section" } });


            var form = BX.create('FORM', { props: { name: this._id + '_form' } });
            var CommentWrapper = BX.create("DIV", { attrs: { className: "crm-list-end-deal-button-wrapper" } });


            //Проверяем, что выбрано, положительный или отрицательный статус лида
            if(this.getSetting('consultation') === false) {

                //Создаем обрамляющий див для каждого параметра формы.
                var InputWrapper = BX.create("DIV", { attrs: { className: "crm-list-end-deal-button-input-wrapper" } });

                //Выбор "Причина отмены" для BPM при завершение лида в статусе "Неуспешно"
                var curReasonClosureId = 'reasonclosure';
                var buttonReasonClosureId = this._id + '_' + curReasonClosureId;

                InputWrapper.appendChild(
                    BX.create(
                        "LABEL",
                        {
                            attrs:
                                {
                                    className: "crm-list-fail-deal-button-label",
                                    "for": buttonReasonClosureId
                                },
                            text: 'Причина отмены'
                        }
                    )
                );

                var selecList = BX.create(
                    'select',
                    {
                        attrs:
                            {
                                id: buttonReasonClosureId,
                                name: this._id,
                            }
                    }
                );
                //Добавить option  в select
                tempClosure = '068EA63F-F5DA-4D0E-B061-95A64AD120E7';
                BX.selectUtils.addNewOption(selecList, '068EA63F-F5DA-4D0E-B061-95A64AD120E7', 'Отменен пользователем');
                BX.selectUtils.addNewOption(selecList, '298B5A1A-18D7-4E8A-9F42-00C7C1AE1FBF', 'Электронное письмо с автоматическим ответом');
                BX.selectUtils.addNewOption(selecList, '2B74EA83-21F8-4F5D-8B92-FA2B8D823CBA', 'Отмечено как спам');
                BX.selectUtils.addNewOption(selecList, '3E5F2F44-F36B-1410-149B-0050BA5D6C38', 'Отклонен: не воспроизводится');
                BX.selectUtils.addNewOption(selecList, '521CDF30-5ADB-4D47-8445-FF2E143BAE3F', 'Получено с адреса службы поддержки');
                BX.selectUtils.addNewOption(selecList, '5F0D0A0A-BFB2-4CA9-A6EE-438CE12AA05C', 'Пришло от пользователя из черного списка');
                BX.selectUtils.addNewOption(selecList, '6B8834BE-C347-4888-A2C6-9D0892AC1691', 'Передано в департамент продаж');
                BX.selectUtils.addNewOption(selecList, '7643E27F-51B2-4226-9001-E2A0AA415B3F', 'Закрыто пользователем');
                BX.selectUtils.addNewOption(selecList, '7F594707-E239-441D-B884-F6AE4D4409ED', 'Отклонен по SLA');
                BX.selectUtils.addNewOption(selecList, '7F9E3154-F36B-1410-149B-0050BA5D6C38', 'Отменен пользователем');
                BX.selectUtils.addNewOption(selecList, '6B8834BE-C347-4888-A2C6-9D0892AC1691', 'Предоставлено обходное решение');
                BX.selectUtils.addNewOption(selecList, '99BF3041-6210-4F5D-B84D-2686E9E0E9BD', 'Тестирование');
                BX.selectUtils.addNewOption(selecList, 'A69816FD-0770-4B55-9A46-76DA52BABE7D', 'Сбой звонка');
                BX.selectUtils.addNewOption(selecList, 'B69F315C-F36B-1410-159B-0050BA5D6C38', 'Предоставлено полное решение');
                BX.selectUtils.addNewOption(selecList, 'DAD53523-AFDB-4189-9C6A-83FC950AAD48', 'Отклонено: пользователь не предоставил информацию');
                BX.selectUtils.addNewOption(selecList, 'FFBF3234-F46B-1410-8D9A-00155D054C04', 'Нецелевое обращение');
                //Выставить в дом собранный узел select
                InputWrapper.appendChild(selecList);
                CommentWrapper.appendChild(InputWrapper);
                BX.CrmSubscriber.subscribe(
                    this._id + "_" + curReasonClosureId,
                    selecList, "change", BX.delegate(this._onReasonClosureChange, this),
                    BX.CrmParamBag.create({"id": curReasonClosureId})
                );
            }

            //Создаем обрамляющий див для каждого параметра формы.
            var InputWrapper = BX.create("DIV", { attrs: { className: "crm-list-end-deal-button-input-wrapper" } });
            //var info = infos[j];
            var curInfoId = 'comment';//info["id"];
            var buttonId = this._id + '_' + curInfoId;
            var commentInput = BX.create(
                "textarea",
                {
                    attrs:
                    {
                        id: buttonId,
                        name: this._id,
                        //className: "crm-list-fail-deal-button",
                        //type: "text"
                        rows: 10,
                        style: "width:354px"
                    }
                }
            );

            InputWrapper.appendChild(
                BX.create(
                    "LABEL",
                    {
                        attrs:
                            {
                                className: "crm-list-fail-deal-button-label",
                                "for": buttonId
                            },
                        text: 'Комментарий'
                    }
                )
            );

            //button.checked = this._value === curInfoId;
            BX.CrmSubscriber.subscribe(
                this._id + "_" + curInfoId,
                commentInput, "change", BX.delegate(this._onCommentChange, this),
                BX.CrmParamBag.create({ "id": curInfoId })
            );

            InputWrapper.appendChild(commentInput);
            CommentWrapper.appendChild(InputWrapper);


            form.appendChild(CommentWrapper);
            this._radioButtonBlock.appendChild(form);
            //}

            //if(this._value === this._successInfo["id"] || apologies.length === 0)
            //{
            //    this._radioButtonBlock.style.display = "none";
            //}

            wrapper.appendChild(this._radioButtonBlock);

            BX.onCustomEvent(this, 'CrmProcessFailureDialogContentCreated', [ this, wrapper ]);

            return wrapper;
        }
        BX.CrmProcessFailureDialog.prototype._prepareTitleGK =  function()
        {
            var wrapper = BX.create("DIV", { "attrs": { "class": "crm-list-fail-deal-selector-block" } });

            wrapper.appendChild(
                BX.create("SPAN", { "text": this._selectorTitle + ": " })
            );

            //var isSuccess = this._value === this._successInfo["id"];
            //this._selector = BX.create(
            //    "DIV",
            //    {
            //        attrs: { className: "crm-list-end-deal-option crm-list-end-deal-option-" + (isSuccess ? "success" : "fail") },
            //        events: { click: BX.delegate(this._onSelectorClick, this) },
            //        text: isSuccess ? this.getSuccessTitle() : this.getFailureTitle()
            //    }
            //);
            //
            //wrapper.appendChild(this._selector);
            return wrapper;
        }
        BX.CrmProcessFailureDialog.prototype._onCommentChange = function(subscriber, params)
        {
            var form = document.forms[this._id + '_form'];
            var comment = '';
            if(form)
            {
                try
                {
                    comment = form.elements[this._id + '_comment'];
                }
                catch(e)
                {
                }
            }
            this._comment = comment.value;
            if(this._comment.length > 0)
            {
                if(this.getSetting('consultation', false) === true)
                {
                    this.setValue("CONVERTED", false);
                }
                else
                {
                    this.setValue("JUNK", false);
                }
            }
            else
            {
                this.setValue(subscriber.getSetting("id", ""), false);
            }
        }

        //Изменение "Причина отказа"
        BX.CrmProcessFailureDialog.prototype._onReasonClosureChange = function(subscriber, params)
        {
            var form = document.forms[this._id + '_form'];
            var reasonclosure = '';
            if(form)
            {
                try
                {
                    reasonclosure = form.elements[this._id + '_reasonclosure'];
                }
                catch(e)
                {
                }
            }
            this._reasonclosure = reasonclosure.value;
            tempClosure = reasonclosure.value;console.log('_onReasonClosureChange this._reasonclosure = ', this._reasonclosure);
        }
        BX.CrmProcessFailureDialog.prototype._executeCallback = function()
        {
            if(this._enableCallback)
            {

                var callback = this._callback;
                if(BX.type.isFunction(callback))
                {
                    if(this.getSetting('ufAuto', 'Y') == 'N')
                    {
                        callback(this, { "bid": this._bid, "result": this._value, "comment": this._comment, "reasonclosure": this._reasonclosure, 'consultation': this.getSetting('consultation', false)});
                    }
                    else
                    {
                        callback(this, { "bid": this._bid, "result": this._value });
                    }
                }
            }
        }
        BX.CrmProgressControl.prototype._onFailureDialogClose = function(dialog, params)
        {
            if(this._failureDlg !== dialog)
            {
                return;
            }
            if(this.getSetting('ufAuto', 'Y') == 'N') //GK
            {
                this._comment = params["comment"];
            }
            BX.onCustomEvent(this, 'CrmProgressControlBeforeFailureDialogClose', [ this, this._failureDlg ]);
            this._closeFailureDialog();
            var bid = BX.type.isNotEmptyString(params["bid"]) ? params["bid"] : "";
            if(bid !== "accept")
            {
                return;
            }

            var id = BX.type.isNotEmptyString(params["result"]) ? params["result"] : "";
            var index = this._findStepInfoIndex(id);
            if(index >= 0)
            {
                var info = this._stepInfos[index];
                if(info["semantics"] === "success" && params['consultation'] !== true)
                {
                    var finalScript = this.getSetting("finalScript", "");
                    if(finalScript !== "")
                    {
                        eval(finalScript);
                        return;
                    }

                    var finalUrl = this.getSetting("finalUrl", "");
                    if(finalUrl !== "")
                    {
                        window.location = finalUrl;
                        return;
                    }

                    var verboseMode = !!this.getSetting("verboseMode", false);
                    if(verboseMode)
                    {
                        //User have to make choice
                        this._openTerminationDialog();
                        return;
                    }
                }
                this._currentStepId = info["id"];
                this._layout();
                this._save();
            }
        }
        BX.CrmProgressControl.prototype._save = function()
        {
            var serviceUrl = this.getSetting("serviceUrl");
            var value = this.getCurrentStepId();
            var type = this.getEntityType();
            var id = this.getEntityId();

            if(serviceUrl === "" || value === "" || type === "" || id <= 0)
            {
                return;
            }
            var data = "";
            if(this.getSetting('ufAuto', 'Y') == 'N') //GK
            {
                serviceUrl ="/local/components/bitrix/crm.lead.list/list.ajax.php";
                data =
                {
                    "ACTION" : "SAVE_PROGRESS",
                    "VALUE": value,
                    "TYPE": type,
                    "ID": id,
                    "COMMENT": this._comment,
                    "REASON_CLOSURE_XML": tempClosure
                };
            }
            else
            {
                data =
                {
                    "ACTION" : "SAVE_PROGRESS",
                    "VALUE": value,
                    "TYPE": type,
                    "ID": id
                };

            }

            BX.onCustomEvent(this, 'CrmProgressControlBeforeSave', [ this, data ]);
            var self = this;
            BX.ajax(
                {
                    "url": serviceUrl,
                    "method": "POST",
                    "dataType": 'json',
                    "data": data,
                    "onsuccess": function(data)
                    {
                        BX.onCustomEvent(self, 'CrmProgressControlAfterSaveSucces', [ self, data ]);
                        BX.CrmProgressControl._synchronize(self);
                    },
                    "onfailure": function(data)
                    {
                        BX.onCustomEvent(self, 'CrmProgressControlAfterSaveFailed', [ self, data ]);
                    }
                }
            );
        }
        BX.CrmProgressControl.prototype.initialize = function(id, settings)
        {
            this._id = id;
            this._settings = settings ? settings : BX.CrmParamBag.create(null);
            this._container = BX(this.getSetting("containerId"));

            var legendContainerId = this.getSetting("legendContainerId");
            if(BX.type.isNotEmptyString(legendContainerId))
            {
                this._legendContainer = BX(legendContainerId);
            }

            if(!BX.type.isElementNode(this._legendContainer))
            {
                this._legendContainer = BX.findNextSibling(this._container, { "className": "crm-list-stage-bar-title" });
            }

            this._entityId = parseInt(this.getSetting("entityId", 0));
            this._entityType = this.getSetting("entityType");
            this._currentStepId = this.getSetting("currentStepId");
            this._infoTypeId = this.getSetting("infoTypeId", "");

            this._enableCustomColors = this.getSetting("enableCustomColors");

            if(this._entityType === 'DEAL')
            {
                this._manager = BX.CrmDealStageManager.current;
            }
            else if(this._entityType === 'LEAD')
            {
                this._manager = BX.CrmLeadStatusManager.current;
                this._terminationControl = BX.CrmLeadTerminationControl.create(
                    this._id,
                    BX.CrmParamBag.create({ entityId: this._entityId, conversionScheme: this.getSetting("conversionScheme", null),
                        'ufAuto': this.getSetting('ufAuto', 'Y')}) //GK
                );
            }
            else if(this._entityType === 'QUOTE')
            {
                this._manager = BX.CrmQuoteStatusManager.current;
            }
            else if(this._entityType === 'INVOICE')
            {
                this._manager = BX.CrmInvoiceStatusManager.current;
            }

            var stepInfos = this._stepInfos = this._manager.getInfos(this._infoTypeId);
            var currentStepIndex = this._findStepInfoIndex(this._currentStepId);
            var currentStepInfo = currentStepIndex >= 0 ? stepInfos[currentStepIndex] : null;

            this._isReadOnly = this.getSetting("readOnly", false);
            this._isFrozen = this._isReadOnly
            || (currentStepInfo && BX.type.isBoolean(currentStepInfo["isFrozen"]) ? currentStepInfo["isFrozen"] : false);
            for(var i = 0; i < stepInfos.length; i++)
            {
                var info = stepInfos[i];
                var stepContainer = this.getStepContainer(info["id"]);
                if(!stepContainer)
                {
                    continue;
                }
                var sort = parseInt(info["sort"]);
                this._steps.push(
                    BX.CrmProgressStep.create(
                        info["id"],
                        BX.CrmParamBag.create(
                            {
                                "name": info["name"],
                                "hint": BX.type.isNotEmptyString(info["hint"]) ? info["hint"] : '',
                                "sort": sort,
                                "isPassed": i <= currentStepIndex,
                                "isReadOnly": this._isReadOnly,
                                "control": this
                            }
                        )
                    )
                );
            }
        }
        BX.CrmProgressControl.prototype.setColor= function(stepIndex)
        {
            if(!this._stepInfos[stepIndex] || !this._enableCustomColors)
            {
                return;
            }

            var wrappers = BX.findChildren(this._container,
                {'tag': 'td', 'attribute': {'class': "crm-list-stage-bar-part"}}, true);

            var stepIndex2 = stepIndex;

            if(this.getSetting('ufAuto', 'Y') == 'N' ) //GK
            {
                stepIndex2 = stepIndex - 5;
            }

            for(var k = 0; k < wrappers.length; k++)
            {
                if(k > stepIndex2)
                {
                    wrappers[k].style.background = "";
                }
                else
                {
                    var stepInfo = this._stepInfos[stepIndex];
                    var color = BX.type.isNotEmptyString(stepInfo["color"]) ? stepInfo["color"] : "";
                    if(color === "")
                    {
                        var semantics = BX.type.isNotEmptyString(stepInfo["semantics"]) ? stepInfo["semantics"] : "";
                        if(semantics === "success")
                        {
                            color = this._defaultSuccessSuccessColor;
                        }
                        else if(semantics === "failure")
                        {
                            color = this._defaultFailureColor;
                        }
                        else
                        {
                            color = this._defaultProcessColor;
                        }
                    }
                    wrappers[k].style.background = color;
                }
            }
        }

        BX.CrmProgressControl.prototype._layout = function()
        {
            var stepIndex = this._findStepInfoIndex(this._currentStepId);
            if(stepIndex < 0)
            {
                return;
            }

            for(var i = 0; i < this._steps.length; i++)
            {
                this._steps[i].setPassed(i <= stepIndex);
            }

            this.setColor(stepIndex);

            var stepInfo = this._stepInfos[stepIndex];

            this._isFrozen = BX.type.isBoolean(stepInfo["isFrozen"]) ? stepInfo["isFrozen"] : false;
            var semantics = BX.type.isNotEmptyString(stepInfo["semantics"]) ? stepInfo["semantics"] : "";

            if(semantics === "success")
            {
                if(this._enableCustomColors)
                {
                    this._container.style.background = stepInfo["color"];
                }
                else
                {
                    BX.addClass(this._container, "crm-list-stage-end-good");
                    BX.removeClass(this._container, "crm-list-stage-end-bad");
                }

            }
            else if(semantics === "failure" || semantics === "apology")
            {
                if(this._enableCustomColors)
                {
                    this._container.style.background = stepInfo["color"];
                }
                else
                {
                    BX.removeClass(this._container, "crm-list-stage-end-good");
                    BX.addClass(this._container, "crm-list-stage-end-bad");
                }
            }
            else
            {
                if(this._enableCustomColors)
                {
                    this._container.style.background = "";
                }
                else
                {
                    BX.removeClass(this._container, "crm-list-stage-end-good");
                    BX.removeClass(this._container, "crm-list-stage-end-bad");
                }
            }

            if(this._legendContainer)
            {
                if(this.getSetting('ufAuto', 'Y') == 'N' && this._comment != undefined
                    && this._currentStepId == "CONVERTED") //GK
                {
                    this._legendContainer.innerHTML = 'Консультация';
                }
                else
                {
                    this._legendContainer.innerHTML = BX.util.htmlspecialchars(BX.type.isNotEmptyString(stepInfo["name"]) ? stepInfo["name"] : stepInfo["id"]);
                }
                //console.log('_legendContainer', 'stepInfo', stepInfo);
            }
        }


        BX.CrmLeadTerminationControl.prototype.onDialogOpen = function(sender)
        {
            if(this._selector)
            {
                this._selector.release();
            }
            this._selector = BX.CrmLeadConversionSchemeSelector.create(
                this.prepareControlId("conv_scheme_selector"),
                {
                    entityId: this._entityId,
                    scheme: this._schemeData["schemeName"],
                    containerId: this.prepareControlId("success_btn_inner_wrapper"),
                    labelId: this.prepareControlId("success_btn"),
                    buttonId: this.prepareControlId("success_btn_menu"),
                    originUrl: this._schemeData["originUrl"],
                    enableHint: false,
                    ufAuto: this.getSetting('ufAuto','Y')
                }
            );
        }
        BX.CrmProcessTerminationDialog.prototype._onButtonClick = function(subscriber, params)
        {
            this._result = subscriber.getSetting("id", "");
            this._executeCallback();
        }


        BX.CrmProgressControl.prototype._onTerminationDialogClose = function(dialog, params)
        {
            if(this._terminationDlg !== dialog)
            {
                return;
            }
            var openFailureDialog = false;
            if(params['result'] == "consultation")//GK
            {
                this._settings.setParam('consultation', true);
                params['result'] = "CONVERTED";
                openFailureDialog = true;
            }

            if(params['result'] == "JUNK")//GK
            {
                this._settings.setParam('consultation', false);
            }

            this._closeTerminationDialog();

            var stepId = BX.type.isNotEmptyString(params["result"]) ? params["result"] : "";

            var index = this._findStepInfoIndex(stepId);
            if(index < 0)
            {
                return;
            }

            this._currentStepId = stepId;


            var info = this._stepInfos[index];
            var failure = this._findStepInfoBySemantics("failure");

            if(failure && failure["id"] === stepId)
            {
                openFailureDialog = true;
            }
            else if(info["semantics"] === "success")
            {
                if(typeof(info["hasParams"]) !== "undefined" && info["hasParams"] === true)
                {
                    openFailureDialog = true;
                }
                else
                {
                    var finalScript = this.getSetting("finalScript", "");
                    if(finalScript !== "")
                    {
                        eval(finalScript);
                        return;
                    }

                    var finalUrl = this.getSetting("finalUrl", "");
                    if(finalUrl !== "")
                    {
                        window.location = finalUrl;
                        return;
                    }
                }
            }

            if(openFailureDialog)
            {
                this._openFailureDialog();
                return;
            }

            this._layout();
            this._save();
        }

        BX.CrmDealCategorySelectDialog.prototype.ChangeINN = function()
        {
            var inn = BX('CRM_LEAD_LIST_V12_LEAD_INN');
            inn.value = inn.value.replace(/[^\d,]/g, '');
            var row_kpp = BX('CRM_LEAD_LIST_V12_LEAD_ROW_KPP');
            if(inn.value.length == 10)
            {
                row_kpp.style.display = '';
            }
            else
            {
                row_kpp.style.display = 'none';

            }
        }
        BX.CrmDealCategorySelectDialog.prototype.ChangeKPP = function()
        {
            var kpp = BX('CRM_LEAD_LIST_V12_LEAD_KPP');
            kpp.value = kpp.value.replace(/[^\d,]/g, '');
        }
        BX.CrmDealCategorySelectDialog.prototype.prepareContent = function()
        {
            var table = BX.create("TABLE",
                {
                    attrs:
                    {
                        className: "bx-crm-deal-category-selector-dialog",
                        cellspacing: "2"
                    }
                }
            );
            var r, c;
            r = table.insertRow(-1);
            c = r.insertCell(-1);
            c.appendChild(BX.create("LABEL", { text: "ИНН:" }));
            c = r.insertCell(-1);
            c.appendChild(
                BX.create("INPUT", {
                        attrs:
                        {
                            id: 'CRM_LEAD_LIST_V12_LEAD_INN',
                            //name: this._id,
                            className: "crm-list-fail-deal-button",
                            type: "text"
                        },
                        events: { keyup: BX.delegate(this.ChangeINN, this) }
                    }
                ));
            c.appendChild(
                BX.create("INPUT", {
                        attrs:
                        {
                            id: 'CRM_LEAD_LIST_V12_LEAD_INN2',
                            type: "hidden"
                        }
                    }
                )
            );
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////
            r = table.insertRow(-1);
            r.id = 'CRM_LEAD_LIST_V12_LEAD_ROW_KPP';
            r.style.display = 'none';
            console.log('r', r);
            c = r.insertCell(-1);
            c.appendChild(BX.create("LABEL", { text: "КПП:" }));
            c = r.insertCell(-1);
            c.appendChild(
                BX.create("INPUT", {
                        attrs:
                        {
                            id: 'CRM_LEAD_LIST_V12_LEAD_KPP',
                            //name: this._id,
                            className: "crm-list-fail-deal-button",
                            type: "text"
                        },
                        events: { keyup: BX.delegate(this.ChangeKPP, this) }
                    }
                )
            );
            c.appendChild(
                BX.create("INPUT", {
                        attrs:
                        {
                            id: 'CRM_LEAD_LIST_V12_LEAD_KPP2',
                            type: "hidden"
                        }
                    }
                )
            );
            c.appendChild(
                BX.create("INPUT", {
                        attrs:
                        {
                            id: 'CRM_LEAD_LIST_V12_LEAD_LEAD_ID',
                            type: "hidden"
                        }
                    }
                )
            );
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////
            r = table.insertRow(-1);
            c = r.insertCell(-1);
            c.appendChild(BX.create("LABEL", { text: this.getMessage("field") + ":" }));
            c = r.insertCell(-1);
            this._selector = BX.create("SELECT", {});
            BX.HtmlHelper.setupSelectOptions(this._selector, BX.CrmDealCategory.getListItems());
            this._selector.value = this._value;
            c.appendChild(this._selector);

            return table;
        }
        BX.CrmDealCategorySelectDialog.prototype.ClickButtonOK = function()
        {
            var inn = BX('CRM_LEAD_LIST_V12_LEAD_INN');
            if(inn.value == "")
            {
                alert('ИНН не заполнен!');
                return;
            }
            if(inn.value.length != 10 &&  inn.value.length != 12)
            {
                alert('Количество цифр в ИНН 10 или 12!');
                return;
            }
            if(inn.value.length != 12)
            {
                var kpp = BX('CRM_LEAD_LIST_V12_LEAD_KPP');
                if(kpp.value == "")
                {
                    alert('КПП не заполнен!');
                    return;
                }
                if(kpp.value.length != 9)
                {
                    alert('Количество цифр в КПП 9!');
                    return;
                }
            }
            var inn2 = BX('CRM_LEAD_LIST_V12_LEAD_INN2');
            var kpp2 = BX('CRM_LEAD_LIST_V12_LEAD_KPP2');
            if(inn.value != inn2.value || (kpp != undefined && kpp.value != kpp2.value))
            {
                var lead_id = BX('CRM_LEAD_LIST_V12_LEAD_LEAD_ID');
                var ajaxURL = '/local/components/bitrix/crm.lead.list/ajax.php';
                var post = {};
                post['c'] = 'set';
                post['lead_id'] = lead_id.value;
                post['inn'] = inn.value;
                console.log('kpp',kpp);
                if (kpp != undefined)
                {
                    post['kpp'] = kpp.value;
                }
                else
                {
                    post['kpp'] = '';

                }
                var self = this;
                BX.showWait();
                BX.ajax.loadJSON(
                    ajaxURL,
                    post,
                    function (data) {
                        console.log('data',data);
                        BX.closeWait();
                        self.processSave();
                    },
                    function (err) {
                        console.log('err',err);
                    }
                );

            }
            else
            {
                this.processSave();
            }
        }
        BX.CrmDealCategorySelectDialog.prototype.prepareButtons = function()
        {
            return(
                [
                    new BX.PopupWindowButton(
                        {
                            text: this.getMessage("saveButton"),
                            className: "popup-window-button-accept",
                            events: { click: BX.delegate(this.ClickButtonOK, this) }
                            //events: { click: BX.delegate(this.processSave, this) }
                        }
                    ),
                    new BX.PopupWindowButtonLink(
                        {
                            text: this.getMessage("cancelButton"),
                            className: "popup-window-button-link-cancel",
                            events: { click: BX.delegate(this.processCancel, this) }
                        }
                    )
                ]);
        }

        BX.CrmLeadConverter.prototype.convert = function(entityId, config, originUrl, contextData)
        {
            if(!BX.type.isPlainObject(config))
            {
                return;
            }
            var post = {};
            post['c'] = 'get';
            post['ajax'] = 'y';
            post['entityId'] = entityId;
            var ajaxURL = '/local/components/bitrix/crm.lead.list/ajax.php';
            BX.showWait();
            BX.ajax.loadJSON(
                ajaxURL,
                post,
                function (data) {
                    var inn = BX('CRM_LEAD_LIST_V12_LEAD_INN');
                    var inn2 = BX('CRM_LEAD_LIST_V12_LEAD_INN2');
                    inn.value = data['inn'];
                    inn2.value = data['inn'];
                    var kpp = BX('CRM_LEAD_LIST_V12_LEAD_KPP');
                    var kpp2 = BX('CRM_LEAD_LIST_V12_LEAD_KPP2');
                    kpp.value = data['kpp'];
                    kpp2.value = data['kpp'];
                    var lead_id = BX('CRM_LEAD_LIST_V12_LEAD_LEAD_ID');
                    lead_id.value = data['id'];
                    BX.closeWait();
                },
                function (err) {
                    console.log('err',err);
                }
            );

            this._entityId = entityId;
            this._contextData = BX.type.isPlainObject(contextData) ? contextData : null;
            this._originUrl = originUrl;

            this.registerConfig(config);

            if(!BX.CrmLeadConversionScheme.isEntityActive(this._config, BX.CrmEntityType.names.deal))
            {
                this.startRequest();
            }
            else
            {
                var categoryId = BX.type.isPlainObject(this._config["deal"]["initData"]) ?
                    this._config["deal"]["initData"]["categoryId"] : 0;
                if(!this._dealCategorySelectDialog)
                {
                    this._dealCategorySelectDialog = BX.CrmDealCategorySelectDialog.create(
                        this._id, { value: categoryId }
                    );
                    this._dealCategorySelectDialog.addCloseListener(this._dealCategorySelectListener);
                }
                this._dealCategorySelectDialog.open();
            }
        }

    }
});