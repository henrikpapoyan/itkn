<?php
/**
 * Created by PhpStorm.
 * User: Рустам
 * Date: 30.08.2019
 * Time: 15:45
 * Инструмент для объединения дублей компаний, в будущем любых сущностей CRM
 */
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

/**
 * @param $string1 исходная строка
 * @param $string2 строка с которой сравниваем
 * @return string возвращает исходную строку с подсвеченными красным символами отличающиеся от сравниваемой строки
 */

function stringsCompare($string1, $string2)
{
    $formedStr = '';
    $arStr1 = str_split($string1);
    $arStr2 = str_split($string2);
    foreach ($arStr1 as $key => $charVal)
    {
        if($charVal !== $arStr2[$key])
        {
            $formedStr .= "<span style=\"color: #FF0000\">$charVal</span>";
        }
        else
        {
            $formedStr .= $charVal;
        }
    }
    if( sizeof($arStr2) > sizeof($arStr1) )
    {
        $charCount = sizeof($arStr2) - sizeof($arStr1);
        for($i = 0; $i < $charCount; $i++)
        {
            $formedStr .= "<span style=\"color: #FF0000\">_</span>";
        }
    }
    return $formedStr;
}

CModule::IncludeModule("crm");

$requisite = new \Bitrix\Crm\EntityRequisite();
$CCrmCompany = new CCrmCompany();
$CCrmInvoice = new CCrmInvoice();
$CCrmDeal = new CCrmDeal();
$inn = $_GET["inn"] ?: 2722130813;
$kpp = $_GET["kpp"] ?: 272401001;
$companyID = $_GET["CID"] ?: 8224;
pre($_GET);
if( $arCompany = $CCrmCompany->GetByID($companyID) )
{
    echo "[$companyID]<br />";
    //pre($arCompany);
    $rsRQ = $requisite->getList(
        array(
            'order' => array('SORT' => 'ASC', 'ID' => 'ASC'),
            'filter' => array(
                '=ENTITY_TYPE_ID' => 4,//Компании,
                '=ENTITY_ID' => $companyID,//Компании,
            )
        )
    );
    $index = 1;
    while ($arRQ = $rsRQ->fetch())
    {
        $arRequisites[] = $arRQ;
        echo "$index. [$arRQ[ID]] $arRQ[RQ_COMPANY_NAME]. ИНН: $arRQ[RQ_INN], КПП: $arRQ[RQ_KPP]<br />";
        $index++;
    }

    $arFilter = array(
        '=ENTITY_TYPE_ID' => 4,//Компании,
        '=RQ_INN' => $inn
    );
    $rsRQ = $requisite->getList(
        array(
            'order' => array('SORT' => 'ASC', 'ID' => 'ASC'),
            'filter' => $arFilter
        )
    );
    echo "<br />По ИНН: $inn реквизитов найдено ".$rsRQ->getSelectedRowsCount()." шт.<br />";
    if($rsRQ->getSelectedRowsCount() > 0)
    {?>
        <form method="GET">
        <table cellpadding="5" cellspacing="0" border="1">
            <thead>
            <th>#</th>
            <th>Компания</th>
            <th>КПП</th>
            <th>Счетов</th>
            <th>Сделок</th>
            </thead>
            <tbody>
    <?  $arRequisites = [];
        $arCompanyIDs = [];
        $index = 1;
        while ($arRQ = $rsRQ->fetch()) {
            if ($arRQ["ENTITY_ID"] == $companyID)
                continue;

            $arCompanyIDs[] = $arRQ["ENTITY_ID"];
            $arRequisites[] = $arRQ;

            $rsInvoice = $CCrmInvoice->GetList(
                array("ID" => "DESC"),
                array("UF_COMPANY_ID" => $arRQ["ENTITY_ID"])
            );
            $invoiceCount = $rsInvoice->SelectedRowsCount();

            $rsDeal = $CCrmDeal->GetList(
                array("ID" => "DESC"),
                array("COMPANY_ID" => $arRQ["ENTITY_ID"])
            );
            $dealCount = $rsDeal->SelectedRowsCount();

            if ($_GET["update"] == "Y") {
                while ($arInvoice = $rsInvoice->GetNext()) {
                    $CCrmInvoice->Update($arInvoice["ID"], array('UF_COMPANY_ID' => $companyID));
                }

                $arUpdate = array('COMPANY_ID' => $companyID);
                while ($arDeal = $rsDeal->GetNext()) {
                    //pre($arDeal);
                    $CCrmDeal->Update($arDeal["ID"], $arUpdate);
                };

                if ($arCompany2 = $CCrmCompany->GetByID($arRQ["ENTITY_ID"]))
                {
                    $arUpdateCompany = array("TITLE" => "(Объединено с $companyID) " . $arCompany2["TITLE"]);
                    $CCrmCompany->Update($arRQ["ENTITY_ID"], $arUpdateCompany);
                }
            }

            echo "<tr>
                <td>$index</td>
                <td><a href=\"/crm/company/show/$arRQ[ENTITY_ID]/\">$arRQ[RQ_COMPANY_NAME]</a></td>
                <td>".stringsCompare($arRQ['RQ_KPP'], $kpp)."</td>
                <td>$invoiceCount</td>
                <td>$dealCount</td>
            </tr>";



            $index++;
        }?>
            </tbody>
        </table>
        Обновить: <input type="text" name="update" value="N" /><br />
        CID: <input type="text" name="CID" value="<?=$_GET["CID"] ?: ''?>" /><br />
        <input type="submit" value="Перевести сделки и счета" /><br />
        </form>
<?
    }
}
?>