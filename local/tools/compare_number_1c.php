<?php
/**
 * Created by PhpStorm.
 * User: Рустам
 * Date: 29.04.2019
 * Time: 15:20
 */

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
$file = fopen($_SERVER["DOCUMENT_ROOT"]."/local/tools/invoice_from_1c.csv", "r");
if(!$fp = fopen($_SERVER["DOCUMENT_ROOT"]."/local/tools/file_result.csv", 'w') ) {
    echo "Ошибка доступа к файлу </br >";
}
$fields = array(
    "ID счета в Б24",
    "Дата выставления счета в Б24",
    "Номер счета в Б24",
    "Номер счета в 1С",
);
fputcsv($fp, $fields);
$row = 0;
CModule::IncludeModule("crm");
$CCrmInvoice = new CCrmInvoice();
while (($data = fgetcsv($file, 10000, ";")) !== FALSE)
{
    if($row == 0) {
        $row++;
        continue;
    }
    if( !empty($data[4]) ) {
        $row++;
        //pre($data);
        $invoiceID = $data[4];
        if( $arInvoice = $CCrmInvoice->GetByID($invoiceID) )
        {
            $accountNumber = trim(stristr($arInvoice["ACCOUNT_NUMBER"], '(', true));
            //echo "[$accountNumber]<br />";
            if($accountNumber != $data[2]) {
                $fields = array(
                    $arInvoice["ID"],
                    $arInvoice["DATE_BILL"],
                    $accountNumber,
                    $data[2]
                );
                if( fputcsv($fp, $fields) )
                    echo "НЕ СОВПАДАЕТ! $arInvoice[ID]. ($arInvoice[DATE_INSERT]) $accountNumber из Б24 != $data[2] из 1С<br />";
                else
                    echo "Ошибка записи в файл<br />";
                //pre($arInvoice);
            } else {
                //echo "СОВПАДАЕТ $arInvoice[ID]. $accountNumber из Б24 == $data[2] из 1С<br />";
            }
            //pre($arInvoice);
        }
    }
}
fclose($fp);
fclose($file);

