<?php
/**
 * Created by PhpStorm.
 * User: Рустам
 * Date: 22.11.2019
 * Time: 18:16
 */
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

CModule::IncludeModule("crm");

//КЛЮЧ = ID старого пользователя, ЗНАЧЕНИЕ = ID нового пользователя
$userConformity = array(
    "787" => "791", //Роман Малюга
    "853" => "855", //Дмитрий Васильев
    "750" => "757", //Родионова	Тамара
);

$userID = 750;
//Обновляем ответственного по компаниям
$CCrmCompany = new CCrmCompany();
$rsCompany = $CCrmCompany->GetListEx(
    array("ID" => "DESC"),
    array("ASSIGNED_BY_ID" => $userID),
    false,
    false,
    array("ID", "TITLE")
);
$index = 0;
while($arCompany = $rsCompany->GetNext())
{
    //pre($arCompany);
    $arFields = array(
        "ASSIGNED_BY_ID" => $userConformity[$userID],
    );
    //$CCrmCompany->Update($arCompany["ID"], $arFields);
    $index ++;
}
echo "Обновлено компаний: $index шт.<br />";

//Обновляем ответственного по контактам
$CCrmContact = new CCrmContact();
$rsContact = $CCrmContact->GetListEx(
    array("ID" => "DESC"),
    array("ASSIGNED_BY_ID" => $userID),
    false,
    false,
    array("ID", "NAME", "SECOND_NAME")
);
$index = 0;
while($arContact = $rsContact->GetNext())
{
    //pre($arContact);
    $index ++;
    $arFields = array(
        "ASSIGNED_BY_ID" => $userConformity[$userID]
    );
    //$CCrmContact->Update($arContact["ID"], $arFields);
}
echo "Обновлено Контактов: $index шт.<br />";

//Обновляем ответственного по счетам
$CCrmInvoice = new CCrmInvoice();
$rsInvoice = $CCrmInvoice->GetList(
    array("ID" => "DESC"),
    array("RESPONSIBLE_ID" => $userID),
    false,
    false,
    array("ID", "ACCOUNT_NUMBER", "RESPONSIBLE_ID")
);
$index = 0;
while($arInvoice = $rsInvoice->GetNext())
{
    //pre($arInvoice);
    $index ++;
    $arFields = array(
        "RESPONSIBLE_ID" => $userConformity[$userID]
    );
    //$CCrmInvoice->Update($arInvoice["ID"], $arFields);
}
echo "Обновлено Счетов: $index шт.<br />";