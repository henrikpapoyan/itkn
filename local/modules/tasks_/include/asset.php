<?
////////////////////////
// assets

// basic asset, contains widely-used phrases and js-stuff required everywhere
// also contains media kit asset, contains sprites and common css used in components
/*
 * When doing redesign, implement conditionally added css, like
 * 'css' => [['condition' => function(){return *some condition*;}, 'file' => '/bitrix/js/tasks/css/media.css']];
 */
CJSCore::RegisterExt(
	'tasks',
	array(
		'js'  => '/bitrix/js/tasks/tasks.js',
		'css'  => '/bitrix/js/tasks/css/media.css',
		'lang' => BX_ROOT."/modules/tasks/lang/".LANGUAGE_ID."/include.php",
	)
);

// util asset, contains fx functions, helper functions and so on
CJSCore::RegisterExt(
	'tasks_util',
	array(
		'js'  => '/bitrix/js/tasks/util.js',
	)
);

// oop asset, contains basic class for making js oop emulation work
CJSCore::RegisterExt(
	'tasks_util_base',
	array(
		'js'  => '/bitrix/js/tasks/util/base.js',
		'rel' =>  array('core')
	)
);

// widget asset, allows to create widget-based js-controls
CJSCore::RegisterExt(
	'tasks_util_widget',
	array(
		'js'  => '/bitrix/js/tasks/util/widget.js',
		'rel' =>  array('tasks_util_base')
	)
);

// components asset, contains logic for components
CJSCore::RegisterExt(
	'tasks_component',
	array(
		'js'  => '/bitrix/js/tasks/component.js',
		'rel' =>  array('tasks_util_widget', 'tasks_util_query')
	)
);

// asset that imports an item accumulator
CJSCore::RegisterExt(
	'tasks_util_datacollection',
	array(
		'js'  => array(
			'/bitrix/js/tasks/util/datacollection.js',
		),
		'rel' =>  array('tasks_util_base')
	)
);

// asset that implements client-side interface for common ajax api
CJSCore::RegisterExt(
	'tasks_util_query',
	array(
		'js'  => '/bitrix/js/tasks/util/query.js',
		'rel' =>  array('tasks_util_base','ajax')
	)
);

// asset that implements an interface for page rounting
CJSCore::RegisterExt(
	'tasks_util_router',
	array(
		'js'  => '/bitrix/js/tasks/util/router.js',
		'rel' =>  array('tasks_util_base')
	)
);

// asset that implements templating mechanism
CJSCore::RegisterExt(
	'tasks_util_template',
	array(
		'js'  => '/bitrix/js/tasks/util/template.js',
	)
);

// asset that imports datepicker widget
CJSCore::RegisterExt(
	'tasks_util_datepicker',
	array(
		'js'  => array(
			'/bitrix/js/tasks/util/datepicker.js',
		),
		'rel' =>  array('tasks_util_widget', 'date')
	)
);

// asset that imports an util for implementing drag-n-drop
CJSCore::RegisterExt(
	'tasks_util_draganddrop',
	array(
		'js'  => array(
			'/bitrix/js/tasks/util/draganddrop.js',
		),
		'rel' =>  array('tasks_util_base', 'tasks_util')
	)
);

// asset that imports a list rendering control (abstract)
CJSCore::RegisterExt(
	'tasks_util_itemset',
	array(
		'js'  => array(
			'/bitrix/js/tasks/util/itemset.js',
		),
		'rel' =>  array('tasks_util_widget', 'tasks_util_datacollection')
	)
);

// asset that imports a family of scroll pane controls
CJSCore::RegisterExt(
	'tasks_util_scrollpane',
	array(
		'js'  => array(
			'/bitrix/js/tasks/util/scrollpane.js',
		),
		'rel' =>  array('tasks_util_widget', 'tasks_util_template', 'popup')
	)
);

// asset that imports a family of selector controls
CJSCore::RegisterExt(
	'tasks_util_selector',
	array(
		'js'  => array(
			'/bitrix/js/tasks/util/selector.js',
		),
		'rel' =>  array('tasks_util_widget', 'tasks_util_scrollpane', 'tasks_util_datacollection')
	)
);

// asset that imports a list rendering control different implementations
CJSCore::RegisterExt(
	'tasks_itemsetpicker',
	array(
		'js'  => array(
			'/bitrix/js/tasks/itemsetpicker.js',
		),
		'rel' =>  array('tasks_util_itemset', 'tasks_integration_socialnetwork')
	)
);

// asset that imports js-api for interacting with user day plan
CJSCore::RegisterExt(
	'tasks_dayplan',
	array(
		'js'  => array(
			'/bitrix/js/tasks/dayplan.js',
		),
		'rel' =>  array('tasks_ui_base', 'tasks_util_query')
	)
);

// asset that implements some integration with "socialnetwork" module
CJSCore::RegisterExt(
	'tasks_integration_socialnetwork',
	array(
		'js'  => array(
			'/bitrix/js/tasks/integration/socialnetwork.js',
		),
		'rel' =>  array('tasks_util_widget', 'tasks_util', 'tasks_util_query', 'socnetlogdest')
	)
);

// shared js parts
CJSCore::RegisterExt(
	'tasks_shared_form_projectplan',
	array(
		'js'  => array(
			'/bitrix/js/tasks/shared/form/projectplan.js',
		),
		'rel' =>  array('tasks_util_widget', 'tasks_util_datepicker')
	)
);

// assets for implementing gantt js api
CJSCore::RegisterExt(
	'task_date',
	array(
		'js' => '/bitrix/js/tasks/task-date.js',
	)
);
CJSCore::RegisterExt(
	'task_calendar',
	array(
		'js' => '/bitrix/js/tasks/task-calendar.js',
		'rel' => array('task_date')
	)
);

CJSCore::RegisterExt(
	'task_timeline',
	array(
		'js' => array(
			'/bitrix/js/tasks/scheduler/util.js',
			'/bitrix/js/tasks/scheduler/timeline.js'
		),
		'css' => '/bitrix/js/tasks/css/gantt.css',
		'rel' => array('task_date', 'task_calendar')
	)
);

CJSCore::RegisterExt(
	'task_scheduler',
	array(
		'js' => array(
			'/bitrix/js/tasks/scheduler/tree.js',
			'/bitrix/js/tasks/scheduler/scheduler.js',
		),
		'css' => '/bitrix/js/tasks/scheduler/css/scheduler.css',
		'rel' => array('task_timeline')
	)
);

CJSCore::RegisterExt(
	'gantt',
	array(
		'js' => array(
			'/bitrix/js/tasks/gantt.js',
			'/bitrix/js/main/dd.js'
		),
		'css' => '/bitrix/js/tasks/css/gantt.css',
		'lang' => BX_ROOT.'/modules/tasks/lang/'.LANGUAGE_ID.'/gantt.php',
		'rel' => array('popup', 'date', 'task_info_popup', 'task_calendar', 'task_date')
	)
);

// todo: deprecated assets, remove

CJSCore::RegisterExt(
	'task_info_popup',
	array(
		'js' => '/bitrix/js/tasks/task-info-popup.js',
		'css' => '/bitrix/js/tasks/css/task-info-popup.css',
		'lang' => BX_ROOT.'/modules/tasks/lang/'.LANGUAGE_ID.'/task-info-popup.php',
		'rel' => array('popup', 'tasks_util')
	)
);

CJSCore::RegisterExt(
	'task_popups',
	array(
		'js' => '/bitrix/js/tasks/task-popups.js',
		'css' => '/bitrix/js/tasks/css/task-popups.css',
		'lang' => BX_ROOT.'/modules/tasks/lang/'.LANGUAGE_ID.'/task-popups.php',
		'rel' => array('popup')
	)
);

CJSCore::RegisterExt(
	'CJSTask',
	array(
		'js'  => '/bitrix/js/tasks/cjstask.js',
		'rel' =>  array('ajax', 'json')
	)
);
CJSCore::RegisterExt(
	'taskQuickPopups',
	array(
		'js'  => '/bitrix/js/tasks/task-quick-popups.js',
		'rel' =>  array('popup', 'ajax', 'json', 'CJSTask')
	)
);

$GLOBALS["APPLICATION"]->AddJSKernelInfo(
	'tasks',
	array(
		'/bitrix/js/tasks/cjstask.js', '/bitrix/js/tasks/core_planner_handler.js',
		'/bitrix/js/tasks/task-iframe-popup.js', '/bitrix/js/tasks/task-quick-popups.js'
	)
);

$GLOBALS["APPLICATION"]->AddCSSKernelInfo('tasks', array('/bitrix/js/tasks/css/tasks.css', '/bitrix/js/tasks/css/core_planner_handler.css'));