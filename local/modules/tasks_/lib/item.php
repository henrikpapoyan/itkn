<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage tasks
 * @copyright 2001-2016 Bitrix
 *
 * This API is in the draft status, it may be modified in the near future, so relying on it is strongly discouraged.
 *
 * @access private
 */

namespace Bitrix\Tasks;

use Bitrix\Main\ArgumentException;
use Bitrix\Main\SystemException;
use Bitrix\Main\Application;
use Bitrix\Main\Type\Dictionary;
use Bitrix\Tasks\Internals\DataBase\LazyAccess;
use Bitrix\Tasks\Item\Context;
use Bitrix\Tasks\Item\Result;
use Bitrix\Tasks\Item\State;
use Bitrix\Tasks\Util\Collection;
use Bitrix\Tasks\Util\Assert;
use Bitrix\Tasks\Util\Error;
use Bitrix\Tasks\Util\Type;
use Bitrix\Tasks\Util\UserField;

abstract class Item extends LazyAccess
{
	protected $id = 0;
	protected $userId = 0;
	protected $transitionState = null;
	protected $pristineState = null; // todo
	protected $runtimeCtx = null;
	protected $instanceCached = false; // indicates if this instance use global instance cache

	protected $readingFailed = false;
	protected $loadFlags = array();

	protected static $cache = array();

	/**
	 * @return string
	 * @throws \Bitrix\Main\NotImplementedException
	 */
	public static function getDataSourceClass()
	{
		throw new \Bitrix\Main\NotImplementedException('No data source class defined');
	}
	public static function getAccessControllerClass()
	{
		return '\\Bitrix\\Tasks\\Item\\Context\\Access';
	}
	public static function getUserFieldControllerClass()
	{
		return null;
	}

	protected static function getLegacyEventMap()
	{
		return array();
	}

	protected static function getCustomFieldMap()
	{
		return array();
	}

	public function __construct($source = 0, $userId = 0)
	{
		if(is_array($source))
		{
			$this->setData($source);
		}
		else
		{
			$this->setId($source);
		}

		$this->setUserId($userId);

		parent::__construct();
	}

	/**
	 * Get entire entity data (or download it)
	 * Returns null if item not found or no access to it under the current user
	 *
	 * @return array|null
	 * @throws \Bitrix\Main\SystemException
	 */
	/*
	 * todo: strongly need to implement smart behaviour of $select argument. cases:
	 * todo:    1) greedy selection: fetch entire data: basic, ufs, sub-entities
	 * todo:    2) un-greedy selection: fetch only several fields, such as e.g. TITLE and UF_CRM_TASK in one query, or from cache
	 * todo:    accessing fields via $this['SOME_FIELD'] is always greedy
	 *
	 * todo: remove $refresh argument, implement ->disableDataCache() method instead
	 * todo: choose the agreement of how we find out that we have no access to the entity, when calling ->getData()
	 * todo:    BAD: variant one: if getData() returns null, then we have no access
	 * todo:    BAD: variant two: if getData() returns array of null, then we have no access
	 * todo: orm may return NULL on null-valued fields, so have to check accessibility by asking for primary key and checking if it is legal
	 */
	// todo: implement what to select: all, basic, ufs, subEntity
	// todo: some of fields SHOULD NOT be loaded in greedy mode, like SE_LOG (it may be quite huge)
	// todo: therefore, introduce some flag like "noGreedy" in getMap() that will indicate such behaviour
	// todo: aliases mostly shoud go with noGreedy == true

	// todo: if item is attached to a not-existing db record, getData() should return null or array of null. $item['FIELD'] also should return null
	public function getData($select = array())
	{
		/*
		if($refresh)// try to re-get again
		{
			$this->readingFailed = false;
			$this->loadFlags = array();
		}
		*/

		$loadBase = !$this->loadFlags['B'];
		$loadUfs = !$this->loadFlags['UF'] && $this->getContext()->getUserFieldController(); // check if we even have ufs

		$base = $this->fetchBaseData($loadBase, $loadUfs);
		if($base === null)
		{
			$this->readingFailed = true;
		}
		else
		{
			$this->mixData($base);

			if($loadBase)
			{
				$this->loadFlags['B'] = true;
			}
			if($loadUfs)
			{
				$this->loadFlags['UF'] = true;
			}
		}

		$map = $this->getMap();
		// todo: this is quite brutal behavior, but could be tuned by implementing use of $select argument
		foreach($map as $k => $v)
		{
			// fetching custom fields
			// fetching all sub-entities
			if($v['src'] == 'SE' || $v['src'] == 'C') // todo: weak condition
			{
				$this[$k]; // lazy load will work here
			}
		}

		return $this->externalizeAllFieldValues($this->values);
	}

	/**
	 * Set instance data "in external format" (it means that each field will be passed through internalizeField())
	 *  (i.e. without any checking). Use with caution.
	 *
	 * @param array $data
	 * @return $this
	 */
	public function setData(array $data)
	{
		// todo: pristine state here

		// set basic data
		$this->values = $inRaw ? $data : $this->internalizeAllFieldValues($data);
		$this->loadFlags['B'] = true;

		// set user fields
		if($this->getContext()->getUserFieldController()) // check if even have a controller
		{
			$this->loadFlags['UF'] = UserField::checkContainsUFKeys(array_flip(array_keys($data)));
		}

		// also, deal with sub-entities
		$this->adaptAllSubEntityFieldValues($data);

		// also, update id from data
		if(!$this->getId() && intval($data['ID']))
		{
			$this->setId(intval($data['ID']));
		}

		return $this;
	}

	/**
	 * Add some data to existing data
	 *
	 * @param mixed $mixin
	 * @return $this
	 */
	public function mixData($mixin)
	{
		if(is_array($mixin))
		{
			foreach($mixin as $k => $v)
			{
				if($v === null && array_key_exists($k, $this->values))
				{
					unset($this->values[$k]);
				}
				else
				{
					$this->values[$k] = $v; // todo: this should be passed through $this->internalizeFieldValue()
				}
			}
		}

		return $this;
	}

	/**
	 * Returns item data as a static array.
	 * The behaviour is similar to getData(), but returns a static structure without any objects.
	 *
	 * @return array
	 */
	public function getArray()
	{
		$result = array();

		// todo: also, you should evaluate all aliases here!
		$data = static::getData();
		foreach($data as $field => $value)
		{
			if(Item::isA($value))
			{
				$result[$field] = $value->getArray();
			}
			elseif(Collection::isA($value))
			{
				$value = $value->toArray();
				foreach($value as $ix => $subValue)
				{
					if(Item::isA($subValue))
					{
						$result[$field][$ix] = $subValue->getArray();
					}
				}
			}
			elseif($value instanceof Dictionary)
			{
				$result[$field] = $value->toArray();
			}
			else
			{
				$result[$field] = $value;
			}
		}

		return $result;
	}

	protected function fetchField($field)
	{
		$map = $this->getMap();

		if($this->id && isset($map[$field]) && !$this->readingFailed)
		{
			$src = $map[$field]['src'];

			// todo: refactor this "if chain" in a better way
			if($src == 'B')
			{
				$base = $this->fetchBaseData(!$this->loadFlags['B'], false);
				if($base === null)
				{
					$this->readingFailed = true;
				}
				else
				{
					$this->mixData($base);
					$this->loadFlags['B'] = true;
				}
			}
			elseif($src == 'UF')
			{
				$base = $this->fetchBaseData(!$this->loadFlags['B'], !$this->loadFlags['UF']);
				if($base === null)
				{
					$this->readingFailed = true;
				}
				else
				{
					$this->mixData($base);
					$this->loadFlags['B'] = true;
					$this->loadFlags['UF'] = true;
				}
			}
			elseif($src == 'C')
			{
				if(!isset($this->values[$field]))
				{
					$base = $this->fetchBaseData(!$this->loadFlags['B'], false);
					if($base === null)
					{
						$this->readingFailed = true;
					}
					else
					{
						$this->mixData($base);
						$this->loadFlags['B'] = true;

						$this->values[$field] = $this->getCustomFieldValue($field);
					}
				}
			}
			elseif($src = 'SE')
			{
				if(!isset($this->values[$field]))
				{
					$base = $this->fetchBaseData(!$this->loadFlags['B'], false);
					if($base === null)
					{
						$this->readingFailed = true;
					}
					else
					{
						$this->mixData($base);
						$this->loadFlags['B'] = true;

						$this->values[$field] = $this->getSubEntityFieldValue($field);
					}
				}
			}
		}
	}

	protected function getCustomFieldValue($field)
	{
		return null;
	}

	protected function getSubEntityFieldValue($field)
	{
		$id = $this->getId();

		if($id)
		{
			$map = $this->getMap();

			$fieldDesc = $map[$field];
			if((string) $fieldDesc['class'] != '')
			{
				$seClass = $fieldDesc['class'];

				// todo: check if $seClass is_a() of SubItem class, throw exception
				// todo: also, take care of single-value fields, so in this case there should be findOneByParent()
				$found = $seClass::findByParent($id);
				if(Type::isIterable($found))
				{
					foreach($found as $sub)
					{
						$sub->setParent($this);
					}
				}

				return $found;
			}
		}

		return null;
	}

	protected function fetchBaseData($fetchBase = true, $fetchUFs = false)
	{
		if(!$fetchBase && !$fetchUFs)
		{
			return array();
		}

		$ac = $this->getContext()->getAccessController();
		$dc = static::getDataSourceClass();

		if($ac->canRead($this)) // formally ask access controller if we can read the item first. May be it would tell us with no query making
		{
			$filter = array('=ID' => $this->id); // todo: '=ID' may not be a correct condition for searching by primary
			$select = array('ID');
			if($fetchBase)
			{
				$select[] = '*';
			}
			if($fetchUFs)
			{
				$select = array_merge($select, $this->extractUserFields());
			}

			$queryParameters = $ac->addDataBaseAccessCheck(array(
				'filter' => $filter,
				'select' => $select,
			), $this);

			$result = $dc::getList($queryParameters)->fetch();
			if(!is_array($result) || !intval($result['ID']))
			{
				$result = null; // access denied or not found
			}
		}
		else // else access denied, definitely
		{
			$result = null;
		}

		return $result;
	}

	private function extractUserFields()
	{
		$map = $this->getMap();

		$ufs = array();
		foreach($map as $k => $v)
		{
			if(UserField::isUFKey($k))
			{
				$ufs[] = $k;
			}
		}

		return $ufs;
	}

	public function offsetSet($offset, $value)
	{
		// todo: pristine state here
		// todo: for multi-value offsets, implement "++FIELD_NAME => value" format, a shortcut to array_merge(prevValue, value)
		parent::offsetSet($offset, $value);

		$map = $this->getMap();
		if(isset($map[$offset]) && $map[$offset]['src'] == 'SE')
		{
			$this->adaptSubEntityFieldValue($offset, $value, false, $map);
		}
	}

	protected function adaptAllSubEntityFieldValues(array $data)
	{
		$map = $this->getMap();

		foreach($data as $name => $value)
		{
			if($map[$name] && $map[$name]['src'] == 'SE')
			{
				$this->values[$name] = $this->adaptSubEntityFieldValue($name, $value, $map);
			}
		}
	}

	protected function adaptSubEntityFieldValue($name, $value, $map = null)
	{
		// todo: make single-value fileds work later
		// todo: note that single-value field`s value will also math isIterable()

		if(!$map)
		{
			$map = $this->getMap();
		}

		if(Collection::isA($value) || is_array($value))
		{
			foreach($value as $k => $item)
			{
				if((!is_object($item) && !is_array($item)) || empty($item)) // todo: its a mess
				{
					unset($value[$k]);
					continue;
				}

				if(is_array($item)) // need to convert to item instance
				{
					$iClass = $map[$name]['class']; // todo: check if class is set

					$iItem = new $iClass(0, $this->getUserId());
					$iItem->setData($item);

					$item = $iItem;
				}

				$item->setParent($this); // update sub-item parent
				$value[$k] = $item;
			}

			if(is_array($value))
			{
				$value = new Collection($value);
			}
		}
		else
		{
			$value = null;
		}

		return $value;
	}

	/**
	 * Returns instance ID
	 *
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Sets or drops instance ID manually. Use with caution.
	 *
	 * @param $id
	 */
	public function setId($id)
	{
		$id = intval($id);

		if(!$id)
		{
			$this->id = 0;
		}
		else
		{
			$this->id = Assert::expectIntegerNonNegative($id, '$id'); // todo: do we need exception here?
		}
	}

	public function isAttached()
	{
		return $this->getId() > 0;
	}

	public function getTransitionState()
	{
		if(!$this->transitionState)
		{
			$this->transitionState = new State();
		}

		return $this->transitionState;
	}

	/**
	 * Returns user id that is used for rights checking
	 *
	 * @return int
	 */
	public function getUserId()
	{
		return $this->userId;
	}

	public function setUserId($userId)
	{
		$this->userId = static::defineUserId($userId);
	}

	public function getContext()
	{
		if($this->runtimeCtx)
		{
			return $this->runtimeCtx;
		}

		return static::getDefaultContext();
	}

	protected function setRunTimeContext($ctx)
	{
		/*
		if(!is_a($ctx, '\\Bitrix\\Tasks\\Item\\Controller', true))
		{
			throw new \Bitrix\Main\ArgumentException('Runtime context class must be a subclass of \\Bitrix\\Tasks\\Item\\Controller');
		}
		*/
		$this->runtimeCtx = $ctx;
	}

	protected static function getDefaultContext()
	{
		$cache =& static::getCache();

		if(!is_array($cache['INSTANCES']))
		{
			$cache['INSTANCES'] = array();
		}

		if(!isset($cache['INSTANCES']['RCTX']))
		{
			$ctx = new Context(get_called_class());
			$ctx->lock();
			$cache['INSTANCES']['RCTX'] = $ctx;
		}

		return $cache['INSTANCES']['RCTX'];
	}

	/**
	 * Get instance from the cache
	 *
	 * @param $id
	 * @param int $userId
	 * @return static|null
	 */
	public static function getInstance($id, $userId = 0)
	{
		$id = intval($id);
		if(!$id || $id < 0)
		{
			return null;
		}

		$userId = static::defineUserId($userId);

		$cache =& static::getCache();
		$key = $id.'-'.$userId;

		if(!is_array($cache['ITEMS']))
		{
			$cache['ITEMS'] = array();
		}

		if(!isset($cache['ITEMS'][$key]))
		{
			$instance = new static($id, $userId);
			$instance->instanceCached = true;

			$cache['ITEMS'][$key] = $instance;
		}

		return $cache['ITEMS'][$key];
	}

	/**
	 * Tries to add or update item depending on if $this->id is defined or not
	 *
	 * @return Result
	 * @throws SystemException
	 */
	public function save()
	{
		$dc = static::getDataSourceClass();
		$ac = $this->getContext()->getAccessController();

		$accessResult = null; // access check result
		$dbResult = null; // database result

		$state = $this->getTransitionState();

		if($state->isInProgress())
		{
			$result = new Result();
			$result->getErrors()->add('IN_TRANSITION', 'Item is in transition state, no CxUD operations available');

			return $result;
		}

		$isUpdate = $this->id;
		$valuesToSave = $this->values;

		if(!$isUpdate)
		{
			$this->setDefaultUserFieldValues($valuesToSave);
		}

		$state->enter($valuesToSave);

		$result = $state->getResult();

		// main action workflow
		// todo: with the current architecture it became possible to check all data (sub-item data too)
		// todo: before actual saving the entire structure, so better implement this. But
		// todo: currently only the legacy scheme implemented
		if($this->prepareData()->isSuccess() && $this->checkData()->isSuccess() && $this->checkUserFieldData()->isSuccess())
		{
			$action = $this->id ? 'UPDATE' : 'CREATE';

			if($isUpdate) // we want update
			{
				$accessResult = $ac->canUpdate($this);
			}
			else
			{
				$accessResult = $ac->canCreate($this);
			}

			if($accessResult->isSuccess())
			{
				if($this->doPreActions($action)->isSuccess())
				{
					if($this->executeLegacyEventHandlers($action, true)->isSuccess())
					{
						$fieldsToSave = $this->filterNativeData($state);

						if($isUpdate)
						{
							$dbResult = $dc::update($this->id, $fieldsToSave);
						}
						else
						{
							$dbResult = $dc::add($fieldsToSave);
						}

						if ($dbResult->isSuccess())
						{
							if(!$this->id)
							{
								$this->setId($dbResult->getId()); // bind current instance to the newly created item
								$state['ID'] = $this->getId(); // save id also to the current state
							}

							$this->actualizeUserFieldData();
							$this->executeLegacyEventHandlers($action, false);
							$this->doPostActions($action);

							// add\update all sub-entities
							$this->actualizeSubItemData();
						}
					} // $this->executeLegacyEventHandlers()
				}
			} // $accessResult->isSuccess()
		}

		if($accessResult && !$accessResult->isErrorsEmpty()) // action was restricted, collect errors
		{
			$rErrors = $result->getErrors();
			$aErrors = $accessResult->getErrors();

			$eCode = 'ACCESS_DENIED';

			if(!$aErrors->isEmpty())
			{
				$rErrors->load($aErrors->prefixCode($eCode.'.'));
			}
			else
			{
				$rErrors->add($eCode, $this->id ? 'Access denied for updating the item' : 'Access denied for creating an item');
			}
		}

		if($dbResult && !$dbResult->isSuccess()) // save failed, collect errors
		{
			$rErrors = $result->getErrors();

			foreach($dbResult->getErrors() as $error)
			{
				$rErrors->add($error->getCode(), $error->getMessage(), Error::TYPE_FATAL, array('FIELD' => $error->getField()));
			}
		}

		if($result->isSuccess())
		{
			$this->clearTemporalStructures();
		}

		$result->setInstance($this);
		$state->leave();

		return $result;
	}

	/**
	 * Tries to delete item
	 *
	 * @return Result
	 * @throws SystemException
	 */
	public function delete()
	{
		$result = new Result();

		if($this->id)
		{
			$dc = static::getDataSourceClass();
			$ac = $this->getContext()->getAccessController();

			$state = $this->getTransitionState();

			if($state->isInProgress())
			{
				$result->getErrors()->add('IN_TRANSITION', 'Item is in transition state, no C-UD operations available');

				return $result;
			}

			$state->enter($this->values);

			$result = $state->getResult();

			$dbResult = null;
			$accessResult = $ac->canDelete($this);

			if($accessResult->isSuccess())
			{
				if($this->doPreActions('DELETE')->isSuccess())
				{
					if($this->executeLegacyEventHandlers('DELETE', true)->isSuccess())
					{
						$dbResult = $dc::delete($this->id);
						$this->executeLegacyEventHandlers('DELETE', false);
						if($dbResult->isSuccess())
						{
							$this->setId(0);
						}

						// todo: implement removing sub-items
					}
				}
			}

			if($dbResult && !$dbResult->isSuccess())
			{
				$rErrors = $result->getErrors();

				foreach($dbResult->getErrors() as $error)
				{
					$rErrors->add($error->getCode(), $error->getMessage());
				}
			}

			if($accessResult && !$accessResult->isErrorsEmpty()) // action was restricted, collect errors
			{
				$rErrors = $result->getErrors();
				$aErrors = $accessResult->getErrors();

				$eCode = 'ACCESS_DENIED';

				if(!$aErrors->isEmpty())
				{
					$rErrors->load($aErrors->prefixCode($eCode.'.'));
				}
				else
				{
					$rErrors->add($eCode, 'Access denied for deleting the item');
				}
			}

			$this->transitionState->leave();
		}
		else
		{
			$result->getErrors()->add('NO_PRIMARY', 'Attempting to delete virtual item');
		}

		$result->setInstance($this);

		return $result;
	}

	// todo: implement settings object that contains access controller and other
	public static function find(array $parameters = array(), $settings = null)
	{
		$dc = static::getDataSourceClass();

		if(!is_array($parameters['select']))
		{
			$parameters['select'] = array();
		}
		$parameters['select'][] = '*';
		if(UserField::checkContainsUFKeys(array_flip($parameters['select'])))
		{
			// todo: make uf controller come from $settings, now just the default taken
			// todo: better to connect with getMap(), the current code is nasty
			$ufcClass = static::getUserFieldControllerClass();
			if($ufcClass)
			{
				$ufc = new $ufcClass();

				$parameters['select'] = array_merge($parameters['select'], array_keys($ufc->getScheme()));
			}
		}

		// todo: also check access here, access controller should come from $settings object

		$result = array();
		$res = $dc::getList($parameters);
		while($item = $res->fetch())
		{
			// todo: support checking if instance is already in cache, and prefer to add from there (or make this optional)
			// todo: smth like "prefer cached"
			$instance = new static($item['ID']);
			$instance->setData($item);

			$result[] = $instance;
		}

		return new Collection($result);
	}

	public static function findOne(array $parameters, $settings = null)
	{
		$parameters['limit'] = 1;

		// todo, return instance
	}

	protected function prepareData()
	{
		return $this->getTransitionState()->getResult(); // do nothing
	}

	protected function checkData()
	{
		return $this->getTransitionState()->getResult(); // do nothing
	}

	protected function checkUserFieldData()
	{
		$data = $this->getTransitionState();
		$result = $data->getResult();
		if($data->isInProgress())
		{
			$ufc = $this->getContext()->getUserFieldController();
			if($ufc)
			{
				$ufResult = $ufc->checkValues($data, $this->id, $this->userId);
				if(!$ufResult->isSuccess())
				{
					$result->getErrors()->load($ufResult->getErrors()->prefixCode('ACTION_FAILED.'));
				}
			}
		}

		return $result;
	}

	protected function actualizeUserFieldData()
	{
		// todo: delete here, in case of $this->id == 0

		$data = $this->getTransitionState();
		$result = $data->getResult();
		if($data->isInProgress())
		{
			$ufc = $this->getContext()->getUserFieldController();
			if($ufc)
			{
				$ufc->updateValues($data->toArray(), $this->id, $this->userId); // nothing interesting in the result
			}
		}

		return $result;
	}

	private function setDefaultUserFieldValues(array &$data)
	{
		$ufc = $this->getContext()->getUserFieldController();
		if($ufc)
		{
			$scheme = $ufc->getScheme();

			foreach($scheme as $field => $desc)
			{
				if(!array_key_exists($field, $data))
				{
					$default = $ufc->getDefaultValue($field, $this->getUserId());
					if($default !== null)
					{
						$data[$field] = $default;
					}
				}
			}
		}
	}

	/**
	 * This function might be removed just like as getCustomFieldValue()
	 *
	 * @access private
	 * @internal
	 */
	protected function actualizeSubItemData()
	{
		$data = $this->getTransitionState();
		$result = $data->getResult();
		if($data->isInProgress())
		{
			$map = $this->getMap();

			foreach($map as $k => $v)
			{
				if($v['src'] == 'SE' && $data[$k])
				{
					// todo: dont forget about single-value fields of that sort
					if(is_object($data[$k]) && Collection::isA($data[$k]))
					{
						foreach($data[$k] as $item)
						{
							$saveResult = $item->save();
							if(!$saveResult->getErrors()->isEmpty())
							{
								$result->getErrors()->load($saveResult->getErrors()->prefixCode($k.'.'));
							}
						}
					}
				}
			}
		}
	}

	protected function doPreActions($operation)
	{
		return $this->getTransitionState()->getResult(); // do nothing
	}

	protected function doPostActions($operation)
	{
		return $this->getTransitionState()->getResult(); // do nothing
	}

	protected static function getMapStaticPart()
	{
		$dc = static::getDataSourceClass();
		$entityMap = $dc::getMap();

		$map = array();

		foreach($entityMap as $k => $v)
		{
			$name = $k;
			if(is_object($v))
			{
				$name = $v->getName();
			}

			$isDate = is_object($v) ? is_a($v, '\\Bitrix\\Main\\Entity\\DateTimeField') : $v['data_type'] == 'datetime';
			$isReference = is_object($v) ? is_a($v, '\\Bitrix\\Main\\Entity\\ReferenceField') : isset($v['reference']);
			$isExpression = is_object($v) ? is_a($v, '\\Bitrix\\Main\\Entity\\ExpressionField') : isset($v['expression']);

			$map[$name] = array(
				'type' => $isDate ? 'datetime' : null,
				'virtual' => $isReference || $isExpression,
				'src' => 'B' // base
			);
		}

		$additionalPart = static::getCustomFieldMap();
		if(is_array($additionalPart))
		{
			foreach($additionalPart as $k => $v)
			{
				$map[$k] = $v;
			}
		}

		return $map;
	}

	// todo: refactor this: use field type classes, not just an array with poorly formed structure
	// todo: in the best way we must try to make a solid field namespace, where each field has its own type and
	// todo: behaviour, regardless of what actually it is: orm field, uf field, se-collection or smth else
	protected function generateMap()
	{
		$map = static::getMapStaticPart();

		$ufController = $this->getContext()->getUserFieldController();
		if($ufController)
		{
			$fields = $ufController->getScheme();
			foreach($fields as $k => $v)
			{
				$map[$k] = array(
					'src' => 'UF'
				);
			}
		}

		return $map;
	}

	/**
	 * Filter out data only for the entity, to pass to orm call. Optionally, convert datetime string to object
	 *
	 * @param $data
	 * @return array
	 */
	protected function filterNativeData($data)
	{
		$map = $this->getMap();
		$data = (array) $data === $data ? $data : $data->toArray();

		foreach($data as $k => $v)
		{
			// remove non-existing or "virtual" fields
			if(!isset($map[$k]) || $map[$k]['virtual'] || ($map[$k]['src'] != 'B'))
			{
				unset($data[$k]);
			}

			// convert date time to object, if can
			if(is_string($v) && $v != '' && $map[$k]['type'] == 'datetime' && UI::checkDateTime($v))
			{
				$data[$k] = \Bitrix\Main\Type\DateTime::createFromUserTime($v);
			}
		}

		return $data;
	}

	/**
	 * Execute old-style event handlers that may remain enabled
	 *
	 * @param $action
	 * @param $isBefore
	 * @return Result
	 */
	private function executeLegacyEventHandlers($action, $isBefore)
	{
		$data = $this->getTransitionState();
		$result = $data->getResult();
		if($data->isInProgress())
		{
			$events = static::getLegacyEventMap();
			$event = $events[$action][$isBefore ? 'BEFORE' : 'AFTER'];

			if($event)
			{
				$values = $data->toArray(); // extract values, as we can not pass the object to the event handlers :(

				global $APPLICATION;

				if($action == 'CREATE')
				{
					$args = $isBefore ? array(&$values) : array($this->getId(), &$values);
				}
				else
				{
					$args = array($this->getId(), &$values, &$values);
				}

				$events = \GetModuleEvents('tasks', $event, true);

				foreach($events as $event)
				{
					$lastEventName = ($event['CALLBACK'] ? $event['CALLBACK'] : $event['TO_CLASS'].'::'.$event['TO_METHOD']).'()';

					try
					{
						if (\ExecuteModuleEventEx($event, $args)===false)
						{
							$e = $APPLICATION->getException();

							if ($e)
							{
								if ($e instanceof \CAdminException)
								{
									if (is_array($e->messages))
									{
										foreach($e->messages as $msg)
										{
											$result->addError('LEGACY_EVENT.CANCELLED', $msg['text'], false, array('LEGACY_ID' => $msg['id']));
										}
									}
								}
								else
								{
									$result->addError('LEGACY_EVENT.CANCELLED.'.ToUpper($e->getId()), $e->getString(), false, array('EXCEPTION' => $e));
								}
							}
							else
							{
								$result->addError('LEGACY_EVENT.CANCELLED', 'Operation has been cancelled');
							}

							break;
						}
					}
					catch(\Exception $e)
					{
						Util::log($e);
						$result->getErrors()->add(
							'LEGACY_EVENT.EXCEPTION',
							$e->getMessage() != '' ?  $e->getMessage() : 'Exception in event handler: '.$lastEventName,
							Error::TYPE_WARNING, // todo: or ERROR::TYPE_FATAL here?
							array('EXCEPTION' => $e, 'EVENT' => $lastEventName)
						);

						// todo: should we do 'break' here?
					}
				}

				$data->set($values); // put changed values back
			}
		}

		return $result;
	}

	public function transformWith($converter)
	{
		$this->checkConverter($converter);

		// todo: also implement ability to set $converter as pre-defined string, like
		// todo: 'task' will apply \Bitrix\Tasks\Item\Converter\Task and so on...
		return $converter->convert($this);
	}

	public function abortTransformation($converter)
	{
		$this->checkConverter($converter);

		return $converter->abortConversion($this);
	}

	private function checkConverter($converter)
	{
		if(!is_object($converter) || !is_a($converter, '\\Bitrix\\Tasks\\Item\\Converter'))
		{
			throw new ArgumentException('Bad converter applied');
		}
	}

	public function getShortPreview()
	{
		return $this->getId();
	}

	protected static function getBatchState()
	{
		$cache =& static::getCache();

		if(!$cache['BATCH_STATE'])
		{
			$state = new State\Trigger();
			$state->setEnterCallback(get_called_class().'::processEnterBatchMode');
			$state->setLeaveCallback(get_called_class().'::processLeaveBatchMode');

			$cache['BATCH_STATE'] = $state;
		}

		return $cache['BATCH_STATE'];
	}

	public static function enterBatchState()
	{
		// todo: need for an event here

		static::getBatchState()->enter();
	}

	public static function leaveBatchState()
	{
		// todo: need for an event here, with detailed statistics on what items were created\updated\deleted

		static::getBatchState()->leave();
	}

	public static function processEnterBatchMode()
	{
	}

	public static function processLeaveBatchMode()
	{
	}

	public static function isA($object)
	{
		return is_a($object, get_called_class());
	}

	/**
	 * When instance becomes attached to some item, we need to clear temporal data
	 */
	protected function clearTemporalStructures()
	{
		$this->values = array();
		$this->readingFailed = false;
		$this->loadFlags = array();
	}

	protected static function &getCache()
	{
		$id = get_called_class();
		if(!array_key_exists($id, static::$cache))
		{
			static::$cache[$id] = array();
		}

		return static::$cache[$id];
	}

	private static function defineUserId($userId)
	{
		if(!$userId)
		{
			$userId = \Bitrix\Tasks\Util\User::getId();
		}

		$userId = Assert::expectIntegerNonNegative($userId, '$userId', 'Was unable to identify effective user id');

		return $userId;
	}
}