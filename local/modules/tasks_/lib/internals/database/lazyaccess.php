<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage tasks
 * @copyright 2001-2016 Bitrix
 */

namespace Bitrix\Tasks\Internals\DataBase;

use Bitrix\Main\Type\Dictionary;

abstract class LazyAccess extends Dictionary
{
	protected $mapCache = array();

	protected function generateMap()
	{
		return array();
	}

	public function getMap() // todo: this wont work out with static calls (i.e. find() and findOne()), so refactor it later
	{
		if(empty($this->mapCache)) // todo: empty() is slow?
		{
			$this->mapCache = $this->generateMap();
		}

		return $this->mapCache;
	}

	protected function resetMapCache()
	{
		$this->mapCache = array();
	}

	protected function fetchField($field)
	{
	}

	public function rewind()
	{
		$this->getMap();

		return reset($this->mapCache);
	}

	public function current()
	{
		return $this->offsetGet(key($this->mapCache));
	}

	/**
	 * Move forward to next element
	 */
	public function next()
	{
		return next($this->mapCache);
	}

	/**
	 * Return the key of the current element
	 */
	public function key()
	{
		return key($this->mapCache);
	}

	/**
	 * Checks if current position is valid
	 */
	public function valid()
	{
		return ($this->key() !== null);
	}

	/**
	 * Returns instance field. If none was loaded before, upload it.
	 *
	 * @param string $offset
	 * @return string | null
	 */
	public function offsetGet($offset)
	{
		$map = $this->getMap();

		if($this->values[$offset] === null && isset($map[$offset]))
		{
			$this->fetchField($offset);
		}

		return $this->externalizeFieldValue($offset, parent::offsetGet($offset));
	}

	public function offsetSet($offset, $value)
	{
		// todo: pristine state here

		parent::offsetSet($offset, $this->internalizeFieldValue($offset, $value));
	}

	public function externalizeFieldValue($name, $value, $map = null)
	{
		return $value;
	}

	public function internalizeFieldValue($name, $value, $map = null)
	{
		return $value;
	}

	/**
	 * Transform internal representation of values into a state suitable for going outside
	 *
	 * @param array $values
	 * @return array
	 */
	protected function externalizeAllFieldValues(array $values)
	{
		$map = $this->getMap();

		$external = array();
		foreach($values as $k => $v)
		{
			$external[$k] = $this->externalizeFieldValue($k, $v, $map);
		}

		return $external;
	}

	/**
	 * Transform back external values into an internal representation
	 *
	 * @param array $values
	 * @return array
	 */
	protected function internalizeAllFieldValues(array $values)
	{
		$map = $this->getMap();

		$internal = array();
		foreach($values as $k => $v)
		{
			$internal[$k] = $this->internalizeFieldValue($k, $v, $map);
		}

		return $internal;
	}

	public function __call($name, array $arguments)
	{
		// todo: implement functional way of setting and getting fields: i.e. setDescription() and getDescription()
	}
}