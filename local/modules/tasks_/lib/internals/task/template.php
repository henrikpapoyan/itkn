<?
/**
 * Class TemplateTable
 *
 * @package Bitrix\Tasks
 **/

namespace Bitrix\Tasks\Internals\Task;

use Bitrix\Main,
	Bitrix\Main\Localization\Loc;
//Loc::loadMessages(__FILE__);

use Bitrix\Tasks\Util\UserField;

class TemplateTable extends Main\Entity\DataManager
{
	/**
	 * Returns userfield entity code, to make userfields work with orm
	 *
	 * @return string
	 */
	public static function getUfId()
	{
		return UserField\Task\Template::getEntityCode();
	}

	/**
	 * Returns DB table name for entity.
	 *
	 * @return string
	 */
	public static function getTableName()
	{
		return 'b_tasks_template';
	}

	/**
	 * Returns entity map definition.
	 *
	 * @return array
	 */
	public static function getMap()
	{
		return array(

			// common with TaskTable
			'ID' => array(
				'data_type' => 'integer',
				'primary' => true,
				'autocomplete' => true,
			),
			'TITLE' => array(
				'data_type' => 'string',
				'validation' => array(__CLASS__, 'validateTitle'),
			),
			'DESCRIPTION' => array(
				'data_type' => 'text',
			),
			'DESCRIPTION_IN_BBCODE' => array(
				'data_type' => 'boolean',
				'values' => array('N', 'Y'),
				'default_value' => 'Y',
			),
			'PRIORITY' => array(
				'data_type' => 'string',
				'default_value' => '1',
				'validation' => array(__CLASS__, 'validatePriority'),
			),
			'STATUS' => array(
				'data_type' => 'string',
				'default_value' => '1',
				'validation' => array(__CLASS__, 'validateStatus'),
			),
			'RESPONSIBLE_ID' => array(
				'data_type' => 'integer',
			),
			'REPLICATE' => array(
				'data_type' => 'boolean',
				'values' => array('N', 'Y'),
			),
			'CREATED_BY' => array(
				'data_type' => 'integer',
			),
			'XML_ID' => array(
				'data_type' => 'string',
				'validation' => array(__CLASS__, 'validateXmlId'),
			),
			'ALLOW_CHANGE_DEADLINE' => array(
				'data_type' => 'boolean',
				'values' => array('N', 'Y'),
			),
			'ALLOW_TIME_TRACKING' => array(
				'data_type' => 'boolean',
				'values' => array('N', 'Y'),
			),
			'TASK_CONTROL' => array(
				'data_type' => 'boolean',
				'values' => array('N', 'Y'),
			),
			'ADD_IN_REPORT' => array(
				'data_type' => 'boolean',
				'values' => array('N', 'Y'),
			),
			'GROUP_ID' => array(
				'data_type' => 'integer',
			),
			'PARENT_ID' => array(
				'data_type' => 'integer',
			),
			'MULTITASK' => array(
				'data_type' => 'boolean',
				'values' => array('N', 'Y'),
			),
			'SITE_ID' => array(
				'data_type' => 'string',
				'required' => true,
				'validation' => array(__CLASS__, 'validateSiteId'),
			),

			// template-specific
			'REPLICATE_PARAMS' => array(
				'data_type' => 'text',
			),
			'TAGS' => array(
				'data_type' => 'text',
			),
			'ACCOMPLICES' => array(
				'data_type' => 'text',
			),
			'AUDITORS' => array(
				'data_type' => 'text',
			),
			'RESPONSIBLES' => array(
				'data_type' => 'text',
			),
			'DEPENDS_ON' => array(
				'data_type' => 'text',
			),
			'DEADLINE_AFTER' => array(
				'data_type' => 'integer',
			),
			'TASK_ID' => array(
				'data_type' => 'integer',
			),

			// template parameters
			'TPARAM_TYPE' => array(
				'data_type' => 'integer',
			),
			'TPARAM_REPLICATION_COUNT' => array(
				'data_type' => 'integer',
			),

			// deprecated
			'FILES' => array(
				'data_type' => 'string',
			),

			// references
			'CREATOR' => array(
				'data_type' => 'Bitrix\Main\User',
				'reference' => array('=this.CREATED_BY' => 'ref.ID')
			),
			'RESPONSIBLE' => array(
				'data_type' => 'Bitrix\Main\User',
				'reference' => array('=this.RESPONSIBLE_ID' => 'ref.ID')
			),
		);
	}
	/**
	 * Returns validators for TITLE field.
	 *
	 * @return array
	 */
	public static function validateTitle()
	{
		return array(
			new Main\Entity\Validator\Length(null, 255),
		);
	}
	/**
	 * Returns validators for PRIORITY field.
	 *
	 * @return array
	 */
	public static function validatePriority()
	{
		return array(
			new Main\Entity\Validator\Length(null, 1),
		);
	}
	/**
	 * Returns validators for STATUS field.
	 *
	 * @return array
	 */
	public static function validateStatus()
	{
		return array(
			new Main\Entity\Validator\Length(null, 1),
		);
	}
	/**
	 * Returns validators for XML_ID field.
	 *
	 * @return array
	 */
	public static function validateXmlId()
	{
		return array(
			new Main\Entity\Validator\Length(null, 50),
		);
	}
	/**
	 * Returns validators for SITE_ID field.
	 *
	 * @return array
	 */
	public static function validateSiteId()
	{
		return array(
			new Main\Entity\Validator\Length(null, 2),
		);
	}
}