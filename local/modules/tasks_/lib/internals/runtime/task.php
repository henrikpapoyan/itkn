<?
/**
 * @internal
 * @access private
 */

namespace Bitrix\Tasks\Internals\RunTime;

use Bitrix\Main\Entity;
use Bitrix\Main\DB\SqlExpression;

use Bitrix\Tasks\Internals\Task\MemberTable;
use Bitrix\Tasks\Internals\TaskTable;
use Bitrix\Tasks\Util\User;
use Bitrix\Tasks\Integration\SocialNetwork;
use Bitrix\Tasks\Integration\Intranet;

final class Task extends \Bitrix\Tasks\Internals\Runtime
{
	/**
	 * Returns runtime fields to attach legacy task filter to the orm-query
	 *
	 * @param $parameters
	 * @return \Bitrix\Main\Entity\ReferenceField[]
	 * @throws \Bitrix\Main\ArgumentException
	 */
	public static function getLegacyFilter(array $parameters)
	{
		$result = array('runtime' => array());

		$parameters = static::checkParameters($parameters);
		if(!is_array($parameters['FILTER_PARAMETERS']))
		{
			$parameters['FILTER_PARAMETERS'] = array();
		}
		$parameters['FILTER_PARAMETERS']['USER_ID'] = $parameters['USER_ID'];

		$selectSql = \CTasks::getSelectSqlByFilter($parameters['FILTER'], '', $parameters['FILTER_PARAMETERS']);

		$query = new \Bitrix\Main\Entity\Query('Bitrix\\Tasks\\Task');
		$query->setFilter(
			array(
				'@ID' => new SqlExpression($selectSql)
			)
		);
		$query->setSelect(array('ID'));

		$rf = $parameters['REF_FIELD'];
		$result['runtime'][] = new Entity\ReferenceField(
			$parameters['NAME'],
			\Bitrix\Main\Entity\Base::getInstanceByQuery($query),
			array(
				'=this.'.((string) $rf != '' ? $rf : 'ID') => 'ref.ID'
			),
			array('join_type' => 'inner')
		);

		return $result;
	}

	/**
	 * Returns runtime fields to attach rights checker
	 *
	 * @param $parameters
	 * @return mixed[]
	 * @throws \Bitrix\Main\ArgumentException
	 */
	public static function getAccessCheck(array $parameters)
	{
		$result = array();

		$parameters = static::checkParameters($parameters);

		if(!User::isAdmin($parameters['USER_ID']))
		{
			// get sql that selects all tasks USER_ID has access to
			$sql = static::getAccessibleTaskIdsSql($parameters);

			// make virtual entity to be able to join it
			$entity = Entity\Base::compileEntity('TasksAccessCheck'.randString().'Table', array(
				new Entity\IntegerField('TASK_ID', array(
					'primary' => true
				))
			), array(
				'table_name' => '('.$sql.')'
			));

			$rf = $parameters['REF_FIELD'];
			$result[] = new Entity\ReferenceField(
				(string) $parameters['NAME'] != '' ? (string) $parameters['NAME'] : 'ACCESS',
				$entity,
				array(
					'=this.'.((string) $rf != '' ? $rf : 'ID') => 'ref.TASK_ID',
				),
				array('join_type' => 'inner')
			);
		}

		return array('runtime' => $result);
	}

	public static function getAccessibleTaskIdsSql(array $parameters)
	{
		$result = array();

		$parameters = static::checkParameters($parameters);
		$parameters['USER_ID'] = intval($parameters['USER_ID']);

		// check if i am a participant of the task
		$q = new Entity\Query(TaskTable::getEntity());
		$q->setSelect(array('TASK_ID' => 'ID'));
		$q->registerRuntimeField('', new Entity\ReferenceField(
			'TM',
			MemberTable::getEntity(),
			array(
				'=this.ID' => 'ref.TASK_ID',
				'=ref.USER_ID' => array('?', $parameters['USER_ID']),
			),
			array('join_type' => 'inner')
		));
		$result[] = $q->getQuery();

		// check if task belongs to the accessible groups
		if(SocialNetwork::isInstalled())
		{
			$allowedGroups = SocialNetwork\Group::getIdsByAllowedAction('view_all', true, $parameters['USER_ID']);

			if(!empty($allowedGroups))
			{
				$q = new Entity\Query(TaskTable::getEntity());
				$q->setSelect(array('TASK_ID' => 'ID'));
				// todo: possible bottleneck here, in case of having lots of groups. refactor it when group access check is available on sql
				$q->setFilter(array('GROUP_ID' => $allowedGroups));
				$result[] = $q->getQuery();
			}
		}

		// check if my subordinate are participants of the task
		if(Intranet::isInstalled() && Intranet\User::isDirector($parameters['USER_ID']))
		{
			$q = new Entity\Query(TaskTable::getEntity());
			$q->setSelect(array('TASK_ID' => 'ID'));
			$q->registerRuntimeField('', new Entity\ReferenceField(
				'TM',
				MemberTable::getEntity(),
				array(
					'=this.ID' => 'ref.TASK_ID',
				),
				array('join_type' => 'inner')
			));

			$subordinate = Intranet\Internals\Runtime\UserDepartment::getSubordinateFilter(array(
				'USER_ID' => $parameters['USER_ID'],
				'REF_FIELD' => 'this.TM'
			));
			$q->registerRuntimeField('', $subordinate['runtime'][0]);

			$result[] = $q->getQuery();
		}

		return implode("\n\nUNION\n\n", $result);
	}

	/**
	 * Returns runtime field that indicates whether task is expired or not
	 *
	 * @param array $parameters
	 * @return array
	 */
	public static function getExpirationFlag(array $parameters)
	{
		$result = array();

		$parameters = static::checkParameters($parameters);
		$result[] = new Entity\ExpressionField(
			$parameters['NAME'],
			"CASE WHEN %s IS NOT NULL AND (%s < %s OR (%s IS NULL AND %s < ".$GLOBALS['DB']->currentTimeFunction().")) THEN 'Y' ELSE 'N' END",
			array('DEADLINE', 'DEADLINE', 'CLOSED_DATE', 'CLOSED_DATE', 'DEADLINE')
		);

		return array('runtime' => $result);
	}
}