<?php
/**
 * This class contains ui helper for a component template
 *
 * Bitrix Framework
 * @package bitrix
 * @subpackage tasks
 * @copyright 2001-2016 Bitrix
 */
namespace Bitrix\Tasks\UI\Component;

use Bitrix\Main\Localization\Loc;

use Bitrix\Tasks\Util\Error;

final class TemplateHelper
{
	private $template = null;
	private $id = '';
	private $name = 'ComponentTemplate';

	public function __construct($name, \CBitrixComponentTemplate $template, array $parameters = array())
	{
		$this->template = $template;
		$this->id = $template->__component->getSignature();
		$this->name = $name;

		Loc::loadMessages($_SERVER['DOCUMENT_ROOT'].'/'.$this->template->getFolder().'/js.php'); // make all js-messages available in php also

		if($this->findParameterValue('SET_TITLE'))
		{
			$title = Loc::getMessage('TASKS_'.ToUpper($this->name).'_TEMPLATE_TITLE');
			if($title != '')
			{
				$GLOBALS['APPLICATION']->setTitle($title);
			}
		}

		$this->registerExtension($parameters['RELATION']);
	}

	/**
	 * Register a js-controller extension for this template
	 *
	 * @param array $relations
	 */
	public function registerExtension(array $relations = array())
	{
		$extensionId = 'tasks_component_ext_'.$this->id;

		\CJSCore::Init('tasks');
		\CJSCore::registerExt(
			$extensionId,
			array(
				'js'  => $this->template->getFolder().'/logic.js',
				'rel' =>  array_merge($relations, array('tasks_component')),
				'lang' => $this->template->getFolder().'/lang/'.LANGUAGE_ID.'/js.php'
			)
		);
	}

	/**
	 * Initialize the js-controller for this template, using data from $arResult['JS_DATA'] as an input object of options.
	 * Each field in this object will be accessible via .option('fieldName') call inside js-controller.
	 *
	 * @param array $data
	 */
	public function initializeExtension(array $data = array())
	{
		\CJSCore::Init('tasks_component_ext_'.$this->id);

		$component = $this->template->__component;

		$arResult = $component->arResult;
		$jsData = $arResult['JS_DATA'];

		$data = array_merge((is_array($jsData) ? $jsData : array()), $data, array(
			'id' => $this->id, // to register in dispatcher
			'url' => $component->__path.'/ajax.php',
			'componentClassName' => $component->getComponentClassName(),
			'componentId' => $component->getId(), // md5() from component signature :)
			'hintState' => \Bitrix\Tasks\UI::getHintState(), // todo: when collection implemented, move this outside, leave handy shortcut in component.js
			'user' => is_array($arResult['AUX_DATA']['USER']) ? $arResult['AUX_DATA']['USER'] : array(), // todo: the same as above
			'userNameTemplate' => $this->findParameterValue('NAME_TEMPLATE'), // todo: the same as above
		));
		?>
		<script>new BX.Tasks.Component.<?=$this->name?>(<?=\Bitrix\Tasks\UI::toJSON($data)?>);</script>
		<?
	}

	public function getId()
	{
		return $this->id;
	}

	public function getScopeId()
	{
		return 'bx-component-scope-'.$this->getId();
	}

	/**
	 * Show blocks of fatal messages, if any
	 */
	public function displayFatals()
	{
		$errors = $this->getErrors();
		foreach($errors as $error)
		{
			if($error->getType() != Error::TYPE_FATAL)
			{
				continue;
			}

			?>
			<div class="task-message-label error"><?=htmlspecialcharsbx($error->getMessage())?></div>
			<?
		}
	}

	/**
	 * Show blocks of warnings, if any
	 */
	public function displayWarnings()
	{
		$errors = $this->getErrors();
		foreach($errors as $error)
		{
			if($error->getType() == Error::TYPE_FATAL)
			{
				continue;
			}

			?>
			<div class="task-message-label warning"><?=htmlspecialcharsbx($error->getMessage())?></div>
			<?
		}
	}

	public function checkHasFatals()
	{
		return $this->getErrors()->checkHasFatals();
	}

	/**
	 * Find a parameter in $arParams of the current component call (or of parent components nested calls, if any)
	 *
	 * @param $parameter
	 * @return mixed
	 */
	public function findParameterValue($parameter)
	{
		return $this->template->__component->findParameterValue($parameter);
	}

	public function fillTemplate($template, array $data)
	{
		$replacement = array();
		foreach($data as $k => $v)
		{
			$replacement['{{{'.$k.'}}}'] = $v;
			$replacement['{{'.$k.'}}'] = htmlspecialcharsbx($v);
		}

		return str_replace(array_keys($replacement), $replacement, $template);
	}

	public function getErrors()
	{
		return $this->template->__component->getErrors();
	}
}