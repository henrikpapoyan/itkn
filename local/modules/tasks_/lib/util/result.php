<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage sale
 * @copyright 2001-2015 Bitrix
 *
 * @access private
 */

namespace Bitrix\Tasks\Util;

use \Bitrix\Tasks\Util\Error\Collection;

class Result
{
	protected $errors = null;
	protected $data = null;

	public function __construct()
	{
		$this->errors = new Collection();
	}

	public function setData($data)
	{
		$this->data = $data;
	}

	public function getData()
	{
		return $this->data;
	}

	public function getErrors()
	{
		return $this->errors;
	}

	public function isSuccess()
	{
		return $this->getErrors()->checkNoFatals();
	}

	public function isErrorsEmpty()
	{
		return $this->getErrors()->isEmpty();
	}

	public function addError($code, $message, $type = Error::TYPE_FATAL)
	{
		$this->getErrors()->add($code, $message, $type);
	}

	public function addWarning($code, $message)
	{
		$this->addError($code, $message, Error::TYPE_WARNING);
	}

	public function loadErrors(Collection $errors)
	{
		$this->getErrors()->load($errors);
	}
}