<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage tasks
 * @copyright 2001-2016 Bitrix
 */

namespace Bitrix\Tasks\Util\Replicator\Task;

use Bitrix\Main\Localization\Loc;

use Bitrix\Tasks\Item;
use Bitrix\Tasks\Item\Result;
use Bitrix\Tasks\Util\Collection;
use Bitrix\Tasks\Util\User;
use Bitrix\Tasks\Util;
use Bitrix\Tasks\UI;
use Bitrix\Tasks\Item\Task\Template;
use Bitrix\Tasks\Item\Task\Template\SystemLogEntry;

Loc::loadMessages(__FILE__);

class FromTemplate extends Util\Replicator\Task
{
	protected static function getSourceClass()
	{
		return '\\Bitrix\\Tasks\\Item\\Task\\Template';
	}

	protected static function getConverterClass()
	{
		return '\\Bitrix\\Tasks\\Item\\Converter\\Task\\Template\\ToTask';
	}

	public function produceSub($source, $destination, array $parameters = array(), $userId = 0)
	{
		$result = new Result();

		Template::enterBatchState();

		$source = $this->getSourceInstance($source, $userId);
		$destination = $this->getDestinationInstance($destination, $userId);

		$data = $this->getSubItemData($source->getId());

		if(!empty($data)) // has sub-templates
		{
			$order = $this->getCreationOrder($data, $source->getId(), $destination->getId());

			if(!$order)
			{
				$result->getErrors()->add('ILLEGAL_STRUCTURE.LOOP', Loc::getMessage('TASKS_REPLICATOR_SUBTREE_LOOP'));
			}
			else
			{
				// disable copying disk files for each sub-task
				$this->getConverter()->setConfig('UF.FILTER', array('!=USER_TYPE_ID' => 'disk_file'));

				$result->setData($this->produceReplicas($result, $source, $destination, $data, $order, $parameters, $userId));
			}
		}

		Template::leaveBatchState();

		return $result;
	}

	/**
	 * Agent handler for repeating tasks.
	 * Create new task based on given template.
	 *
	 * @param integer $templateId - id of task template
	 * @param mixed[] $parameters
	 *
	 * @return string empty string.
	 */
	public static function repeatTask($templateId, array $parameters = array())
	{
		$templateId = (int) $templateId;
		if(!$templateId)
		{
			return '';
		}

		$rsTemplate = \CTaskTemplates::getList(array(),  array('ID' => $templateId), false, false, array('*', 'UF_*')); // todo: replace this with item\orm call
		$arTemplate = $rsTemplate->Fetch();

		if($arTemplate && $arTemplate['REPLICATE'] == 'Y')
		{
			$agentName = str_replace('#ID#', $templateId, $parameters['AGENT_NAME_TEMPLATE']); // todo: when AGENT_NAME_TEMPLATE is not set?

			if (intval($arTemplate['CREATED_BY']))
			{
				User::setOccurAsId($arTemplate['CREATED_BY']); // not admin in logs, but template creator
			}

			try
			{
				$replicator = new static();
				$result = $replicator->produce($templateId, static::getEffectiveUser());

				if(is_array($parameters['RESULT']))
				{
					$parameters['RESULT']['RESULT'] = $result;
				}

				if($result->isSuccess())
				{
					static::incrementReplicationCount($templateId);
					$taskId = $result->getInstance()->getId();

					if($result->getErrors()->isEmpty())
					{
						$message = Loc::getMessage('TASKS_REPLICATOR_TASK_CREATED');
					}
					else
					{
						$message = Loc::getMessage('TASKS_REPLICATOR_TASK_CREATED_WITH_ERRORS');
					}
				}
				else
				{
					$taskId = 0;
					$message = Loc::getMessage('TASKS_REPLICATOR_TASK_WAS_NOT_CREATED');
				}

				static::sendToSysLog($templateId, $taskId, $message, $result->getErrors());
			}
			catch(\Exception $e) // catch EACH exception, as we dont want the agent to repeat every 10 minutes in case of smth is wrong
			{
				Util::log($e); // write to b24 exception log
				static::sendToSysLog($templateId, 0, Loc::getMessage('TASKS_REPLICATOR_TASK_POSSIBLY_WAS_NOT_CREATED'), new Util\Error\Collection(array(
					new Util\Error($e->getMessage(), 'REPLICATION_FAILED', Util\Error::TYPE_FATAL)
				)));
			}

			$arTemplate['REPLICATE_PARAMS'] = unserialize($arTemplate['REPLICATE_PARAMS']);

			// get agent time
			$agentTime = false;
			$agent = \CAgent::getList(array(), array('NAME' => $agentName))->fetch();
			if($agent)
			{
				$agentTime = $agent['NEXT_EXEC']; // user time
			}

			// get next time task will be created, i.e. next Thursday if today is Thursday (and task marked as repeated) and so on;

			$tzCurrent = User::getTimeZoneOffsetCurrentUser();
			$lastTime = $agentTime;
			$iterations = 0;
			do
			{
				$nextResult = static::getNextTime($arTemplate, $lastTime);
				$nextData = $nextResult->getData();
				$nextTime = $nextData['TIME'];

				// next time is legal, but goes before or equals to the current time
				if(($nextTime && MakeTimeStamp($lastTime) >= MakeTimeStamp($nextTime)) || ($iterations > 10000))
				{
					if($iterations > 10000)
					{
						$message = 'insane iteration count reached while calculating next execution time';
					}
					else
					{
						$creator = $arTemplate['CREATED_BY'];
						$tzCreator =  User::getTimeZoneOffset($arTemplate['CREATED_BY']);

						$eDebug = array(
							$creator,
							time(),
							$tzCurrent,
							$tzCreator,
							$arTemplate['REPLICATE_PARAMS']['TIME'],
							$arTemplate['REPLICATE_PARAMS']['TIMEZONE_OFFSET'],
						);
						$message = 'getNextTime() loop detected for replication by template '.$templateId.' ('.$nextTime.' => '.$lastTime.') ('.implode(', ', $eDebug).')';
					}

					Util::log($message); // write to b24 exception log
					static::sendToSysLog($templateId, 0, Loc::getMessage('TASKS_REPLICATOR_PROCESS_ERROR'), null, true);

					$nextTime = false; // possible endless loop, this agent must be stopped
					break;
				}

				// $nextTime in current user`s time (or server time, if no user)

				$lastTime = $nextTime;
				// we can compare one user`s time only with another user`s time, we canna just take time() value
				$cTime = time() + $tzCurrent;

				$iterations++;
			}
			while(($nextResult->isSuccess() && $nextTime) && MakeTimeStamp($nextTime) < $cTime);

			if ($nextTime)
			{
				// we can not use CAgent::Update() here, kz the agent will be updated again just after this function ends ...
				global $pPERIOD;

				$nextTimeFormatted = $nextTime;
				$nextTime = MakeTimeStamp($nextTime);
				// still have $nextTime in current user timezone, we need server time now, so:
				$nextTime -= $tzCurrent;

				// ... but we may set some global var called $pPERIOD
				// "why ' - time()'?" you may ask. see CAgent::ExecuteAgents(), in the last sql we got:
				// NEXT_EXEC=DATE_ADD(".($arAgent["IS_PERIOD"]=="Y"? "NEXT_EXEC" : "now()").", INTERVAL ".$pPERIOD." SECOND),
				$pPERIOD = $nextTime - time();

				static::sendToSysLog($templateId, 0, Loc::getMessage('TASKS_REPLICATOR_NEXT_TIME', array(
					'#TIME#' => $nextTimeFormatted,
					'#PERIOD#' => $pPERIOD,
				)));

				return $agentName; // keep agent working
			}
			else
			{
				$reason = $nextResult->getErrors()->first()->getMessage();
				static::sendToSysLog($templateId, 0, Loc::getMessage('TASKS_REPLICATOR_PROCESS_STOPPED').': '.$reason);
			}
		}

		return ''; // agent will be simply deleted
	}

	public static function getNextTime(array $templateData, $agentTime = false, $nowTime = false)
	{
		$result = new Util\Result();

		$arParams = \CTaskTemplates::parseReplicationParams($templateData['REPLICATE_PARAMS']); // todo: replace with just $template['REPLICATE_PARAMS']

		// get users and their time zone offsets
		$currentTimeZoneOffset = User::getTimeZoneOffsetCurrentUser(); // set 0 to imitate working on agent
		$creatorTimeZoneOffset = 0;
		if(array_key_exists('TIMEZONE_OFFSET', $arParams))
		{
			$creatorTimeZoneOffset = intval($arParams['TIMEZONE_OFFSET']);
		}
		elseif(intval($templateData['CREATED_BY']))
		{
			$creatorTimeZoneOffset = User::getTimeZoneOffset(intval($templateData['CREATED_BY']));
		}

		// prepare base time
		$baseTime = time(); // server time
		if($nowTime)
		{
			$nowTime = MakeTimeStamp($nowTime); // from string to stamp
			if($nowTime) // time parsed normally
			{
				// $agentTime is in current user`s time, but we want server time here
				$nowTime -= $currentTimeZoneOffset;
				$baseTime = $nowTime;
			}
		}

		if($agentTime) // agent were found and had legal next_time
		{
			$agentTime = MakeTimeStamp($agentTime); // from string to stamp
			if($agentTime) // time parsed normally
			{
				// $agentTime is in current user`s time, but we want server time here
				$agentTime -= $currentTimeZoneOffset;
				$baseTime = $agentTime;
			}
		}

		// prepare time limits
		$startTime = 0;
		if($arParams["START_DATE"])
		{
			$startTime = MakeTimeStamp($arParams["START_DATE"]); // from string to stamp
			$startTime -= $creatorTimeZoneOffset; // to server time (initially in $creatorTimeZoneOffset offset)
		}
		$endTime = PHP_INT_MAX; // never ending
		if($arParams["END_DATE"])
		{
			$endTime = MakeTimeStamp($arParams["END_DATE"]); // from string to stamp
			$endTime -= $creatorTimeZoneOffset; // to server time (initially in $creatorTimeZoneOffset offset)
		}

		// now get max of dates and add time
		$baseTime = max($baseTime, $startTime);

		// prepare time to be forced to
		$creatorPreferredTime = UI::parseTimeAmount($arParams["TIME"], 'HH:MI');

		//////////////////////////////////////////////////////////////
		// now calculate next time based on current $baseTime

		// to format suitable for php date functions
		$startDate = date("Y-m-d H:i:s", $baseTime);

		$arPeriods = array("daily", "weekly", "monthly", "yearly");
		$arOrdinals = array("first", "second", "third", "fourth", "last");
		$arWeekDays = array("mon", "tue", "wed", "thu", "fri", "sat", "sun");
		$type = in_array($arParams["PERIOD"], $arPeriods) ? $arParams["PERIOD"] : "daily";

		$date = 0;
		switch ($type)
		{
			case "daily":
				$num = intval($arParams["EVERY_DAY"]) + intval($arParams["DAILY_MONTH_INTERVAL"])*30;

				$date = strtotime($startDate." +".$num." days");

				if ($arParams["WORKDAY_ONLY"] == "Y")
				{
					// get server datetime as string and create an utc-datetime object with this string, as Calendar works only with utc datetime object
					$dateInst = Util\Type\DateTime::createFromUserTimeGmt(UI::formatDateTime($date));
					$calendar = new Util\Calendar();

					if(!$calendar->isWorkTime($dateInst))
					{
						$cwt = $calendar->getClosestWorkTime($dateInst); // get closest time in UTC
						$cwt = $cwt->convertToLocalTime(); // change timezone to server timezone

						$date = $cwt->getTimestamp(); // set server timestamp
					}
				}

				break;

			case "weekly":
				$num = $arParams["EVERY_WEEK"];
				$days = is_array($arParams["WEEK_DAYS"]) && sizeof(array_filter($arParams["WEEK_DAYS"])) ? $arParams["WEEK_DAYS"] : array(1);

				$currentDay = date("N", strtotime($startDate));
				$nextDay = false;
				foreach ($days as $day)
				{
					if ($day > $currentDay)
					{
						$nextDay = $day;
						break;
					}
				}
				if ($nextDay)
				{
					$date = strtotime($startDate." +".($nextDay - $currentDay)." days");
				}
				else
				{
					$date = strtotime($startDate." +".$num." weeks ".($days[0] - $currentDay)." days");
				}
				break;

			case "monthly":
				$subType = $arParams["MONTHLY_TYPE"] == 2 ? "weekday" : "monthday";
				if ($subType == "weekday")
				{
					$ordinal = array_key_exists($arParams["MONTHLY_WEEK_DAY_NUM"], $arOrdinals) ? $arOrdinals[$arParams["MONTHLY_WEEK_DAY_NUM"]] : $arOrdinals[0];
					$weekDay = array_key_exists($arParams["MONTHLY_WEEK_DAY"], $arWeekDays) ? $arWeekDays[$arParams["MONTHLY_WEEK_DAY"]] : $arWeekDays[0];
					$num = intval($arParams["MONTHLY_MONTH_NUM_2"]) > 0 ? intval($arParams["MONTHLY_MONTH_NUM_2"]) : 1;

					$date = strtotime($ordinal." ".$weekDay." of this month");
					if (strtotime($startDate) >= $date)
					{
						$date = strtotime($startDate." +".$num." months");
						$date = strtotime($ordinal." ".$weekDay." of ".date("Y-m-d", $date));
					}
				}
				else
				{
					$day = intval($arParams["MONTHLY_DAY_NUM"]) >= 1 && intval($arParams["MONTHLY_DAY_NUM"]) <= 31 ? intval($arParams["MONTHLY_DAY_NUM"]) : 1;
					$num = intval($arParams["MONTHLY_MONTH_NUM_1"]) > 0 ? intval($arParams["MONTHLY_MONTH_NUM_1"]) : 1;

					$date = strtotime(date("Y-m-".sprintf("%02d", $day), strtotime($startDate)));
					if (strtotime($startDate) >= $date)
					{
						$date = strtotime($startDate." +".$num." months");
						$date = strtotime(date("Y-m-".sprintf("%02d", $day), $date));
					}
				}
				break;

			case "yearly":
				$subType = $arParams["YEARLY_TYPE"] == 2 ? "weekday" : "monthday";
				if ($subType == "weekday")
				{
					$ordinal = array_key_exists($arParams["YEARLY_WEEK_DAY_NUM"], $arOrdinals) ? $arOrdinals[$arParams["YEARLY_WEEK_DAY_NUM"]] : $arOrdinals[0];
					$weekDay = array_key_exists($arParams["YEARLY_WEEK_DAY"], $arWeekDays) ? $arWeekDays[$arParams["YEARLY_WEEK_DAY"]] : $arWeekDays[0];
					$month = intval($arParams["YEARLY_MONTH_2"]) >= 0 && intval($arParams["YEARLY_MONTH_2"]) < 12 ? intval($arParams["YEARLY_MONTH_2"]) : 0;
					$month += 1;

					$date = strtotime($ordinal." ".$weekDay." of ".date("Y", strtotime($startDate))."-".sprintf("%02d", $month)."-01");
					if (strtotime($startDate) >= $date)
					{
						$date = strtotime($ordinal." ".$weekDay." of ".(date("Y", strtotime($startDate)) + 1)."-".sprintf("%02d", $month)."-01");
					}
				}
				else
				{
					$day = intval($arParams["YEARLY_DAY_NUM"]) >= 1 && intval($arParams["YEARLY_DAY_NUM"]) <= 31 ? intval($arParams["YEARLY_DAY_NUM"]) : 1;
					$month = intval($arParams["YEARLY_MONTH_1"]) >= 0 && intval($arParams["YEARLY_MONTH_1"]) < 12 ? intval($arParams["YEARLY_MONTH_1"]) : 0;
					$month += 1;

					$date = strtotime(date("Y", strtotime($startDate))."-".sprintf("%02d", $month)."-".sprintf("%02d", $day));
					if (strtotime($startDate) >= $date)
					{
						$date = strtotime((date("Y", strtotime($startDate)) + 1)."-".sprintf("%02d", $month)."-".sprintf("%02d", $day));
					}
				}
				break;
		}

		$nextTime = $date; // timestamp

		//////////////////////////////////////////////////////////////

		// now check if we can proceed
		if ($nextTime)
		{
			$proceed = true;

			// about end date
			if(array_key_exists("REPEAT_TILL", $arParams) && $arParams['REPEAT_TILL'] != 'endless')
			{
				if($arParams['REPEAT_TILL'] == 'date')
				{
					$proceed = !($endTime && $nextTime > $endTime);

					if(!$proceed)
					{
						$result->addError('STOP_CONDITION.END_DATE_REACHED', Loc::getMessage('TASKS_REPLICATOR_END_DATE_REACHED'));
					}
				}
				elseif($arParams['REPEAT_TILL'] == 'times' && $templateData)
				{
					$proceed = intval($templateData['TPARAM_REPLICATION_COUNT']) < intval($arParams['TIMES']);

					if(!$proceed)
					{
						$result->addError('STOP_CONDITION.LIMIT_REACHED', Loc::getMessage('TASKS_REPLICATOR_LIMIT_REACHED'));
					}
				}
			}

			if($proceed)
			{
				// here we got $nextTime in server timezone
				// to current user time to save under current user`s time
				$nextTime += $currentTimeZoneOffset;

				// creator will get in result:
				$creatorResultAgentTime = $nextTime + $creatorTimeZoneOffset - $currentTimeZoneOffset;
				$creatorResultAgentDate = static::stripTime($creatorResultAgentTime);

				// now we got to set exact time creator specified
				$creatorResultTime = $creatorResultAgentTime - $creatorResultAgentDate;

				if($creatorResultTime != $creatorPreferredTime)
				{
					$creatorResultAgentTime = $creatorResultAgentDate + $creatorPreferredTime;
					$nextTime = $creatorResultAgentTime - $creatorTimeZoneOffset; // to server time

					// ensure we wont get nextTime in past or equal to the input time
					if($nextTime <= $baseTime)
					{
						$nextTime += 86400; // one day forward
					}

					$nextTime += $currentTimeZoneOffset; // current user time
				}
			}
			else
			{
				$nextTime = 0;
			}
		}
		else // $nextTime was not calculated
		{
			if($result->getErrors()->isEmpty())
			{
				$result->addError('STOP_CONDITION.ILLEGAL_NEXT_TIME', Loc::getMessage('TASKS_REPLICATOR_ILLEGAL_NEXT_TIME'));
			}
		}

		$result->setData(array(
			'TIME' => $nextTime ? UI::formatDateTime($nextTime) : '',
		));

		return $result;
	}

	protected function isMultitaskSource($source, array $parameters = array())
	{
		return
			Item\Task\Template::isA($source) &&
			$source['MULTITASK'] == 'Y' &&
			!empty($source['RESPONSIBLES']) &&
			!$parameters['MULTITASKING.DISABLE'];
	}

	private function getSubItemData($id)
	{
		$result = array();

		$id = intval($id);
		if(!$id)
		{
			return $result;
		}

		// todo: move CTaskTemplates to orm then replace here
		$res = \CTaskTemplates::getList(array('BASE_TEMPLATE_ID' => 'asc'), array('BASE_TEMPLATE_ID' => $id), false, array('INCLUDE_TEMPLATE_SUBTREE' => true), array('*', 'UF_*', 'BASE_TEMPLATE_ID'));
		while($item = $res->fetch())
		{
			if($item['ID'] == $id)
			{
				continue;
			}

			// unpack values
			$item['ACCOMPLICES'] = unserialize($item['ACCOMPLICES']);
			$item['AUDITORS'] = unserialize($item['AUDITORS']);
			$item['TAGS'] = unserialize($item['TAGS']);
			$item['REPLICATE_PARAMS'] = unserialize($item['REPLICATE_PARAMS']);
			$item['DEPENDS_ON'] = unserialize($item['DEPENDS_ON']);

			$result[$item['ID']] = $item;
		}

		// get checklist data
		// todo: convert getListByTemplateDependency() to a runtime mixin for the template entity
		$res = \Bitrix\Tasks\Internals\Task\Template\CheckListTable::getListByTemplateDependency($id, array(
			'order' => array('SORT' => 'ASC'),
			'select' => array('ID', 'TEMPLATE_ID', 'CHECKED', 'SORT', 'TITLE')
		));
		while($item = $res->fetch())
		{
			if(isset($result[$item['TEMPLATE_ID']]))
			{
				$result[$item['TEMPLATE_ID']]['SE_CHECKLIST'][$item['ID']] = $item;
			}
		}

		return $result;
	}

	private static function sendToSysLog($templateId, $taskId, $message, Util\Error\Collection $errors = null, $forceTypeError = false)
	{
		$record = new SystemLogEntry(array(
			'ENTITY_ID' => $templateId,
			'MESSAGE' => $message,
		));
		if($taskId)
		{
			$record['PARAM_A'] = $taskId;
		}

		if($forceTypeError)
		{
			$record['TYPE'] = SystemLogEntry::TYPE_ERROR;
		}
		elseif($errors instanceof Util\Error\Collection && !$errors->isEmpty())
		{
			$record['TYPE'] = $errors->find(array('TYPE' => Util\Error::TYPE_FATAL))->isEmpty() ? SystemLogEntry::TYPE_WARNING : SystemLogEntry::TYPE_ERROR;
		}

		$record['ERROR'] = $errors;
		$record->save();
	}

	private static function incrementReplicationCount($templateId)
	{
		$template = Item\Task\Template::getInstance($templateId, static::getEffectiveUser());

		// todo: replace the following with $template->clone()->save() when ready
		$templateInst = new \CTaskTemplates();
		$templateInst->update($templateId, array(
			'TPARAM_REPLICATION_COUNT' => intval($template['TPARAM_REPLICATION_COUNT']) + 1
		));
	}

	private static function getEffectiveUser()
	{
		return User::getAdminId();
	}

	private static function stripTime($nextTime)
	{
		$m = (int) date("n", $nextTime);
		$d = (int) date("j", $nextTime);
		$y = (int) date("Y", $nextTime);

		return mktime(0, 0, 0, $m, $d, $y);
	}

	private static function printDebugTime($l, $t)
	{
		Util::printDebug($l.UI::formatDateTime($t));
	}

	/**
	 * @param Result $result
	 * @param $source
	 * @param $destination
	 * @param $data
	 * @param $tree
	 * @param array $parameters
	 * @param int $userId
	 * @return Util\Collection
	 */
	private function produceReplicas(Result $result, $source, $destination, $data, $tree, array $parameters = array(), $userId = 0)
	{
		$created = new Collection();
		$wereErrors = false;

		// create list of responsibles to create tasks to
		$multiTemplate = $this->isMultitaskSource($source, $parameters);

		// choose or make "root" tasks (several in case of multitasking)
		$parentTemplates = array();
		if($multiTemplate)
		{
			// create duplicates of $destination for each sub-responsible
			foreach($source['RESPONSIBLES'] as $responsibleId)
			{
				$subResult = $this->saveItemFromSource($source, array(
					'PARENT_ID' => $destination->getId(),
					'RESPONSIBLE_ID' => $responsibleId,
				), $userId);

				if($subResult->isSuccess())
				{
					$parentTemplates[] = array(
						'ID' => $subResult->getInstance()->getId(),
						'RESPONSIBLE_ID' => $responsibleId,
					);
				}
				else
				{
					$wereErrors = true;
				}

				$created->push($subResult);
			}
		}
		else
		{
			$parentTemplates[] = array(
				'ID' => $destination->getId(),
				'RESPONSIBLE_ID' => null,
			);
		}

		// now create sub-templates for each "main" template
		foreach($parentTemplates as $subTemplate)
		{
			$itemId2ReplicaId = array($source->getId() => $subTemplate['ID']);

			$cTree = $tree;

			$srcId = $source->getId();
			$walkQueue = array($srcId);
			while(!empty($walkQueue)) // walk sub-item tree
			{
				$topTemplate = array_shift($walkQueue);

				if(is_array($cTree[$topTemplate]))
				{
					foreach($cTree[$topTemplate] as $template)
					{
						$dataMixin = array_merge(array(
							'PARENT_ID' => $itemId2ReplicaId[$topTemplate],
						), $parameters);

						if($subTemplate['RESPONSIBLE_ID'])
						{
							$dataMixin['RESPONSIBLE_ID'] = $subTemplate['RESPONSIBLE_ID'];
						}

						$creationResult = $this->saveItemFromSource($data[$template], $dataMixin, $userId);
						if($creationResult->isSuccess())
						{
							$walkQueue[] = $template; // walk further on that template
							$itemId2ReplicaId[$template] = $creationResult->getInstance()->getId();
						}
						else
						{
							$wereErrors = true;
						}

						$created->push($creationResult); // add sub-item creation result
					}
				}
				unset($cTree[$topTemplate]);
			}
		}

		if($wereErrors)
		{
			$result->addError('SUB_ITEMS_CREATION_FAILURE', 'Some of the sub-tasks was not properly created');
		}

		return $created;
	}

	/**
	 * Check if template->sub-templates relation tree is correct and return it
	 *
	 * @param array $subEntitiesData
	 * @param $srcId
	 * @return array|bool
	 */
	private function getCreationOrder(array $subEntitiesData, $srcId)
	{
		$walkQueue = array($srcId);
		$treeBundles = array();

		foreach($subEntitiesData as $subTemplate)
		{
			$treeBundles[$subTemplate['BASE_TEMPLATE_ID']][] = $subTemplate['ID'];
		}

		$tree = $treeBundles;
		$met = array();
		while(!empty($walkQueue))
		{
			$topTemplate = array_shift($walkQueue);
			if(isset($met[$topTemplate])) // hey, i`ve met this guy before!
			{
				return false;
			}
			$met[$topTemplate] = true;

			if(is_array($treeBundles[$topTemplate]))
			{
				foreach($treeBundles[$topTemplate] as $template)
				{
					$walkQueue[] = $template;
				}
			}
			unset($treeBundles[$topTemplate]);
		}

		return $tree;
	}
}