<?
namespace Bitrix\Tasks\Util\Type;

use Bitrix\Tasks\Util\User;

abstract class ArrayOption
{
	/**
	 * @throws \Bitrix\Main\NotImplementedException
	 * @return string
	 */
	protected static function getFilterOptionName()
	{
		throw new \Bitrix\Main\NotImplementedException();
	}

	public function set(array $value)
	{
		User::setOption(static::getFilterOptionName(), \Bitrix\Tasks\Util\Type::serializeArray(
			static::check($value)
		));
	}

	public function get()
	{
		return static::check(
			\Bitrix\Tasks\Util\Type::unSerializeArray($this->fetchOptionValue()),
			!$this->checkOptionValueExists()
		);
	}

	public function remove()
	{
		User::unSetOption(static::getFilterOptionName());
	}

	public function check($value, $initial = false)
	{
		static $checker;

		if(!$checker)
		{
			$checker = new \Bitrix\Tasks\Util\Type\StructureChecker($this->getRules());
		}

		return $checker->check($value, $initial);
	}

	public function removeForAllUsers()
	{
		\CUserOptions::deleteOptionsByName('tasks', static::getFilterOptionName());
	}

	protected function fetchOptionValue()
	{
		return User::getOption(static::getFilterOptionName());
	}

	protected function checkOptionValueExists()
	{
		return User::getOption(static::getFilterOptionName()) != '';
	}

	protected function getRules()
	{
		return array();
	}
}