<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage tasks
 * @copyright 2001-2016 Bitrix
 */

namespace Bitrix\Tasks\Util;

use Bitrix\Main\Type\Dictionary;
use Bitrix\Tasks\Item;

class Collection extends Dictionary
{
	public function find(array $conditions = array(), $limit = -1)
	{
		$filtered = new static();
		$filter = new Filter($conditions);

		foreach($this->values as $k => $v)
		{
			// todo: remove this spike by implementing special kind of condition: '=getData().SOME_FIELD' => 'lala'
			$data = $v;
			if(Item::isA($v))
			{
				$data = $v->getData();
			}

			if($filter->match($data))
			{
				$filtered->push($v);
			}
		}

		return $filtered;
	}

	public function findOne(array $conditions = array())
	{
		return $this->find($conditions, 1)->get(0);
	}

	public function sort()
	{
		// todo: return collection instance
	}

	public function push($data)
	{
		array_push($this->values, $data);
	}

	public function unShift($data)
	{
		array_unshift($this->values, $data);
	}

	public function remove()
	{
	}

	public static function isA($object)
	{
		return is_a($object, get_called_class());
	}
}