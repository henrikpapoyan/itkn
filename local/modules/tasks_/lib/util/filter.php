<?
/**
 * @access private
 * @internal
 */

namespace Bitrix\Tasks\Util;

final class Filter
{
	private $conditions = null;

	public function __construct($conditions)
	{
		$this->conditions = static::parseConditions($conditions);
	}

	public function match($item)
	{
		foreach($this->conditions as $condition)
		{
			$field = $condition['F'];

			if($condition['I'])
			{
				return $item[$field] != $condition['V'];
			}
			else
			{
				return $item[$field] == $condition['V'];
			}
		}

		return false;
	}

	public static function isA($object)
	{
		return is_a($object, get_called_class());
	}

	private static function parseConditions($conditions)
	{
		$parsed = array();

		// todo: only one level supported currently
		if(is_array($conditions))
		{
			foreach($conditions as $c => $v)
			{
				$inverse = false;
				$equals = false;

				$c = trim((string) $c);

				// todo: make rich syntax here, currently only = and != supported
				if($c[0] == '!')
				{
					$inverse = true;
					$c = substr($c, 1);
				}
				if($c[0] == '=')
				{
					$equals = true;
					$c = substr($c, 1);
				}

				if($c != '')
				{
					$parsed[] = array('I' => $inverse, 'E' => $equals, 'F' => $c, 'V' => $v);
				}
			}
		}

		return $parsed;
	}
}