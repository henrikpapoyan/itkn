<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage tasks
 * @copyright 2001-2016 Bitrix
 */

namespace Bitrix\Tasks\Item\Context\Access;

use Bitrix\Tasks\Integration\SocialNetwork\Group;
use Bitrix\Tasks\Item\Result;
use Bitrix\Tasks\Util\User;
use Bitrix\Tasks\Item;
use Bitrix\Tasks\Internals\Runtime;

final class Task extends \Bitrix\Tasks\Item\Context\Access
{
	public static function getActionMap()
	{
		return array_merge(parent::getActionMap(), array(
			// todo: place custom actions here
		));
	}

	/**
	 * Alters query parameters to check access rights on database side
	 *
	 * @param mixed[]|\Bitrix\Main\Entity\Query query parameters or query itself
	 * @param $instance
	 * @return array
	 */
	public function addDataBaseAccessCheck($query, $instance)
	{
		$userId = $instance->getUserId();
		if(!User::isSuper($userId)) // no access check for admins
		{
			$query = Runtime::apply($query, array(
				Runtime\Task::getAccessCheck(array(
					'USER_ID' => $userId,
				)
			)));
		}

		return $query;
	}

	protected function testAction($action, $instance)
	{
		$result = new Result();

		/*
		if(User::isSuper($instance->getUserId()))
		{
			return $result; // admin can do everything
		}
		*/

		if($action == 'READ')
		{
			// for task entity we can not say yes or no without data fetching, so say yes (and apply filter later)
		}
		elseif($action == 'CREATE')
		{
			return $this->checkCanAdd($instance, $result);
		}
		elseif($action == 'UPDATE')
		{
			return $this->checkCanUpdate($instance, $result);
		}

		return $result;
	}

	private function checkCanAdd(Item\Task $task, Result $result)
	{
		$userId = $task->getUserId();
		if(User::isSuper($userId)) // todo: some b24 restrictions here?
		{
			return $result; // admin can add any kind of tasks
		}

		$rErrors = $result->getErrors();
		$data = $task;

		$state = $task->getTransitionState();
		if($state->isInProgress())
		{
			$data = $state;
		}

		if($data['RESPONSIBLE_ID'] != $userId && $data['CREATED_BY'] != $userId)
		{
			$rErrors->add('RESPONSIBLE_AND_ORIGINATOR_NOT_ALLOWED', 'You can not add task from other person to another person');
		}

		$groupId = intval($data['GROUP_ID']);
		if($groupId)
		{
			if(!Group::can($groupId, Group::ACTION_CREATE_TASKS, $userId))
			{
				$rErrors->add('PROJECT_ACCESS_DENIED', 'You are not allowed to create tasks in the group [group: '.$groupId.', user: '.$userId.']');
			}
		}

		return $result;
	}

	private function checkCanUpdate($task, Result $result)
	{
		/*
		 //todo
			$actionChangeDeadlineFields = array('DEADLINE', 'START_DATE_PLAN', 'END_DATE_PLAN', 'DURATION');
			$arGivenFieldsNames = array_keys($arFields);

			if (
				array_key_exists('CREATED_BY', $arFields)
				&& ( ! $this->isActionAllowed(self::ACTION_CHANGE_DIRECTOR) )
			)
			{
				throw new TasksException('Access denied for originator to be updated', TasksException::TE_ACTION_NOT_ALLOWED);
			}

			if (
				// is there fields to be checked for ACTION_CHANGE_DEADLINE?
				array_intersect($actionChangeDeadlineFields, $arGivenFieldsNames)
				&& ( ! $this->isActionAllowed(self::ACTION_CHANGE_DEADLINE) )
			)
			{
				throw new TasksException('Access denied for plan dates to be updated', TasksException::TE_ACTION_NOT_ALLOWED);
			}

			// Get list of fields, except just checked above
			$arGeneralFields = array_diff(
				$arGivenFieldsNames,
				array_merge($actionChangeDeadlineFields, array('CREATED_BY'))
			);

			// Is there is something more for update?
			if ( ! empty($arGeneralFields) )
			{
				if ( ! $this->isActionAllowed(self::ACTION_EDIT) )
					throw new TasksException('Access denied for task to be updated', TasksException::TE_ACTION_NOT_ALLOWED);
			}
		 */

		return $result;
	}
}