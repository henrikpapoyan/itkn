<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage tasks
 * @copyright 2001-2016 Bitrix
 */

namespace Bitrix\Tasks\Item\Context;

use Bitrix\Tasks\Item\Result;

class Access
{
	public static function getActionMap()
	{
		return array(
			'CREATE',
			'READ',
			'UPDATE',
			'DELETE',
		);
	}

	public function __call($name, array $arguments)
	{
		static $map;

		$name = trim((string) $name);

		if($name != '')
		{
			if(strpos($name, 'can') === 0)
			{
				$action = substr($name, 3);
				if($action != '')
				{
					$action = ToUpper($action);

					if(!$map)
					{
						$map = array_flip(static::getActionMap());
					}

					if(isset($map[$action]))
					{
						return $this->testAction($action, $arguments[0]);
					}

					return true; // unknown action is allowed
				}
			}
		}
	}

	/**
	 * Alters query parameters to check access rights on database side
	 *
	 * @param mixed[]|\Bitrix\Main\Entity\Query query parameters or query itself
	 * @param $instance
	 * @return array
	 */
	public function addDataBaseAccessCheck($query, $instance)
	{
		return $query;
	}

	/**
	 * By default, everything is allowed
	 *
	 * @param $action
	 * @param $instance
	 * @return Result
	 */
	protected function testAction($action, $instance)
	{
		$result = new Result();

		if($action == 'READ')
		{
			// DO NOT USE $instance->getData() here, unless you want endless recursion
			// find out about readability by some other way
		}

		return $result;
	}
}