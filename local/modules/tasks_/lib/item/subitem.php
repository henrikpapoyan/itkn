<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage tasks
 * @copyright 2001-2016 Bitrix
 */

namespace Bitrix\Tasks\Item;

use Bitrix\Main\ArgumentException;
use Bitrix\Main\NotImplementedException;

use Bitrix\Tasks\Util\Collection;

abstract class SubItem extends \Bitrix\Tasks\Item
{
	protected $parent = null;

	/**
	 * Returns field name that is used to connect entity with its parent in database (i.e. foreign key)
	 *
	 * @throws NotImplementedException
	 * @return string
	 */
	protected static function getParentConnectorField()
	{
		throw new NotImplementedException('No parent connector field defined');
	}

	public function getParent()
	{
		return $this->parent;
	}

	public function setParent($instance)
	{
		if($instance !== null)
		{
			if(!is_a($instance, '\\Bitrix\\Tasks\\Item'))
			{
				throw new ArgumentException('Illegal parent instance passed');
			}
		}

		$this->parent = $instance;
	}

	protected function prepareData()
	{
		$data = $this->getTransitionState();
		$result = $data->getResult();
		if($data->isInProgress())
		{
			$id = $this->getId();
			$field = static::getParentConnectorField();

			if(!$id && !isset($data[$field])) // this is CREATE, no foreign key passed. we can define it by ourselves
			{
				$parent = $this->getParent();
				if($parent)
				{
					$parentId = $parent->getId();
					if(!$parentId)
					{
						$result->getErrors()->add('ILLEGAL_PARENT_ID', 'Attempting to save sub-items without saving parent item?');
					}
					else
					{
						$data[$field] = $parentId;
					}
				}
			}
		}

		return $result;
	}

	/**
	 * @param $parentId
	 * @param array $parameters
	 * @param null $settings
	 * @return Collection
	 */
	public static function findByParent($parentId, array $parameters = array(), $settings = null)
	{
		$parentId = intval($parentId); // todo: this wont work in case of compound or non-integer primary

		if(!$parentId)
		{
			return new Collection(); // if no parent id passed, just return empty collection, no exceptions!
		}

		if(!is_array($parameters['filter']))
		{
			$parameters['filter'] = array();
		}
		$parameters['filter'] = static::getBindCondition($parentId) + $parameters['filter'];

		return static::find($parameters, $settings);
	}

	public static function deleteByParent($parentId)
	{
		$result = new Result();

		$items = static::findByParent($parentId);
		$wereErrors = false;
		$dResults = new Collection();
		foreach($items as $item)
		{
			$dResult = $item->delete();
			if(!$dResult->isSuccess())
			{
				$wereErrors = true;
			}

			$dResults->push($dResult);
		}

		if($wereErrors)
		{
			$result->addWarning('ACTION_INCOMPLETE', 'Some of the items was not removed properly');
		}

		$result->setData($dResults);

		return $result;
	}

	protected static function getBindCondition($parentId)
	{
		return array('='.static::getParentConnectorField() => $parentId);
	}
}