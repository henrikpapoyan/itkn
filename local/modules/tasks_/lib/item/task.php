<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage tasks
 * @copyright 2001-2016 Bitrix
 */

namespace Bitrix\Tasks\Item;

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Type\Dictionary;

use Bitrix\Tasks\Integration\Pull;
use Bitrix\Tasks\Integration\SocialNetwork\Group;
use Bitrix\Tasks\Internals\Task\FavoriteTable;
use Bitrix\Tasks\Internals\Task\MemberTable;
use Bitrix\Tasks\Internals\Task\RelatedTable;
use Bitrix\Tasks\Item\Task\Log;
use Bitrix\Tasks\Util;
use Bitrix\Tasks\Util\Type;
use Bitrix\Tasks\UI;

final class Task extends \Bitrix\Tasks\Item
{
	public static function getDataSourceClass()
	{
		return '\\Bitrix\\Tasks\\Internals\\TaskTable';
	}
	public static function getAccessControllerClass()
	{
		return '\\Bitrix\\Tasks\\Item\\Context\\Access\\Task';
	}
	public static function getUserFieldControllerClass()
	{
		return '\\Bitrix\\Tasks\\Util\\UserField\\Task';
	}

	protected static function getCustomFieldMap()
	{
		return array(

			// legacy-custom
			'ACCOMPLICES' => array(
				'src' => 'C'
			),
			'AUDITORS' => array(
				'src' => 'C'
			),
			'TAGS' => array(
				'src' => 'C'
			),
			'DEPENDS_ON' => array(
				'src' => 'C'
			),

			// todo: there should be one legal sub-entity lile SE_MEMBER
			// todo: ACCOMPLICES, AUDITORS, SE_ACCOMPLICE, SE_AUDITOR, SE_ORIGINATOR, SE_RESPONSIBLE - just aliases for SE_MEMBER with filtering
			// todo: provide read and write access for the aliases through adapters

			// sub-entity
			'SE_CHECKLIST' => array(
				'src' => 'SE',
				'class' => '\\Bitrix\\Tasks\\Item\\Task\\CheckList'
			),
			'SE_PROJECTDEPENDENCE' => array(
				'src' => 'SE',
				'class' => '\\Bitrix\\Tasks\\Item\\Task\\ProjectDependence'
			),
		);
	}

	protected static function getLegacyEventMap()
	{
		return array(
			'CREATE' => array(
				'BEFORE' => 'OnBeforeTaskAdd',
				'AFTER' => 'OnTaskAdd'
			)
		);
	}

	/**
	 * This function will be replaced with field type`s getter(), so DO NOT USE IT
	 *
	 * @param $field
	 * @return Dictionary|null
	 *
	 * @internal
	 * @access private
	 */
	protected function getCustomFieldValue($field)
	{
		if(!isset($this->values[$field]))
		{
			if($field == 'TAGS')
			{
				return $this->getTags(); // todo: later move to SE_TAG sub-entity
			}
			elseif($field == 'ACCOMPLICES') // todo: later move to SE_ACCOMPLICE sub-entity, alias for SE_MEMBER
			{
				return $this->getAccomplice();
			}
			elseif($field == 'AUDITORS') // todo: later move to SE_AUDITOR sub-entity, alias for SE_MEMBER
			{
				return $this->getAuditor();
			}
			elseif($field == 'DEPENDS_ON')
			{
				return $this->getRelatedTask();
			}
		}
	}

	// todo: later move to SE_TAG sub-entity
	private function getTags()
	{
		$id = $this->getId();

		if($id)
		{
			$arTagsFilter = array("TASK_ID" => $id);
			$arTagsOrder = array("NAME" => "ASC");
			$rsTags = \CTaskTags::getList($arTagsOrder, $arTagsFilter);
			$tags = array();
			while ($arTag = $rsTags->Fetch())
			{
				$tags[] = $arTag["NAME"];
			}

			return new Dictionary($tags);
		}

		return null;
	}

	// todo: remove it later
	private function getAccomplice()
	{
		if(array_key_exists('ACCOMPLICES', $this->values))
		{
			return $this->values['ACCOMPLICES'];
		}

		$result = array();

		$id = $this->getId();
		if($id)
		{
			$res = MemberTable::getList(array('filter' => array(
				'=TASK_ID' => $id,
				'=TYPE' => array('A')
			), 'select' => array(
				'USER_ID'
			)));
			while($item = $res->fetch())
			{
				$result[] = $item['USER_ID'];
			}
		}

		return new Dictionary($result);
	}

	// todo: remove it later
	private function getAuditor()
	{
		if(array_key_exists('AUDITORS', $this->values))
		{
			return $this->values['AUDITORS'];
		}

		$result = array();

		$id = $this->getId();
		if($id)
		{
			$res = MemberTable::getList(array('filter' => array(
				'=TASK_ID' => $id,
				'=TYPE' => array('U')
			), 'select' => array(
				'USER_ID'
			)));
			while($item = $res->fetch())
			{
				$result[] = $item['USER_ID'];
			}
		}

		return new Dictionary($result);
	}

	// todo: remove it later
	private function getRelatedTask()
	{
		if(array_key_exists('DEPENDS_ON', $this->values))
		{
			return $this->values['DEPENDS_ON'];
		}

		$result = array();

		$id = $this->getId();
		if($id)
		{
			$res = RelatedTable::getList(array('filter' => array(
				'=TASK_ID' => $id
			), 'select' => array(
				'DEPENDS_ON_ID'
			)));
			while($item = $res->fetch())
			{
				$result[] = $item['DEPENDS_ON_ID'];
			}
		}

		return new Dictionary($result);
	}

	protected function prepareData()
	{
		$data = $this->getTransitionState();
		$result = $data->getResult();
		if($data->isInProgress())
		{
			$id = $this->getId();
			$now = $data->getEnterTimeFormatted();

			if(!$data->containsKey('CHANGED_DATE'))
			{
				$data['CHANGED_DATE'] = $now;
			}

			// move 0 to null in PARENT_ID to avoid constraint and query problems
			// todo: move PARENT_ID, GROUP_ID and other "foreign keys" to the unique way of keeping absence of relation: null, 0 or ''
			if($data->containsKey('PARENT_ID'))
			{
				$parentId = intval($data['PARENT_ID']);
				if(!intval($parentId))
				{
					$data['PARENT_ID'] = false;
				}
			}

			if($id)
			{
				// todo
			}
			else
			{
				if(!$data->containsKey('SITE_ID'))
				{
					$data['SITE_ID'] = SITE_ID;
				}
				if(!$data->containsKey('RESPONSIBLE_ID'))
				{
					$data['RESPONSIBLE_ID'] = $this->getUserId();
				}
				if(!$data->containsKey('CREATED_BY'))
				{
					$data['CREATED_BY'] = $this->getUserId();
				}
				if(!$data->containsKey('GUID'))
				{
					$data['GUID'] = Util::generateUUID();
				}

				// force GROUP_ID to 0 if not set (prevent occur as NULL in database)
				$data['GROUP_ID'] = intval($data['GROUP_ID']);

				self::processDurationPlanFields($data); // todo: refactor this

				if(!$data->containsKey('CHANGED_BY'))
				{
					$data['CHANGED_BY'] = $data['CREATED_BY'];
				}
				if(!$data->containsKey('STATUS_CHANGED_BY'))
				{
					$data['STATUS_CHANGED_BY'] = $data['CHANGED_BY'];
				}

				if(!$data->containsKey('CREATED_DATE')) // created date was not set manually
				{
					$data['CREATED_DATE'] = $now;
				}
				if(!$data->containsKey('STATUS_CHANGED_DATE'))
				{
					$data['STATUS_CHANGED_DATE'] = $now;
				}

				if($data->containsKey('DESCRIPTION_IN_BBCODE') && $data['DESCRIPTION_IN_BBCODE'] != 'Y')
				{
					$result->addError('ILLEGAL_DESCRIPTION', 'Tasks with HTML description are not allowed');
				}
			}
		}

		return $result;
	}

	protected function checkData()
	{
		$data = $this->getTransitionState();
		$result = $data->getResult();
		if($data->isInProgress())
		{
			$id = $this->getId();

			// checking for update()
			if($id)
			{
				// todo: update action is not implemented currently

				/*
				if($ID && ((isset($arFields['END_DATE_PLAN']) && (string) $arFields['END_DATE_PLAN'] == '')))
				{
					if(DependenceTable::checkItemLinked($ID))
					{
						$this->_errors[] = array("text" => GetMessage("TASKS_IS_LINKED_END_DATE_PLAN_REMOVE"), "id" => "ERROR_TASKS_IS_LINKED");
					}
				}

				if($ID && (isset($arFields['PARENT_ID']) && intval($arFields['PARENT_ID']) > 0))
				{
					if(DependenceTable::checkLinkExists($ID, $arFields['PARENT_ID'], array('BIDIRECTIONAL' => true)))
					{
						$this->_errors[] = array("text" => GetMessage("TASKS_IS_LINKED_SET_PARENT"), "id" => "ERROR_TASKS_IS_LINKED");
					}
				}

				if ($ID !== false && $ID == $arFields["PARENT_ID"])
				{
					$this->_errors[] = array("text" => GetMessage("TASKS_PARENT_SELF"), "id" => "ERROR_TASKS_PARENT_SELF");
				}

				if ($ID !== false && is_array($arFields["DEPENDS_ON"]) && in_array($ID, $arFields["DEPENDS_ON"]))
				{
					$this->_errors[] = array("text" => GetMessage("TASKS_DEPENDS_ON_SELF"), "id" => "ERROR_TASKS_DEPENDS_ON_SELF");
				}
				 */
			}

			// common checking for both add() and update()

			// if plan dates were set
			if (isset($data['START_DATE_PLAN'])
				&& isset($data['END_DATE_PLAN'])
				&& ($data['START_DATE_PLAN'] != '')
				&& ($data['END_DATE_PLAN'] != '')
			)
			{
				$startDate = \MakeTimeStamp($data['START_DATE_PLAN']);
				$endDate   = \MakeTimeStamp($data['END_DATE_PLAN']);

				// and they were really set
				if (($startDate > 0)
					&& ($endDate > 0)
				)
				{
					// and end date is before start date => then emit error
					if ($endDate < $startDate)
					{
						$result->getErrors()->add('ILLEGAL_PLAN_DATES', Loc::getMessage('TASKS_BAD_PLAN_DATES')); // todo: include lang file
					}
				}
			}

			// todo: remove it, make ACCOMPLICES and AUDITORS as aliases for SE_MEMBER and use their adapters
			// accomplices & auditors
			$data['ACCOMPLICES'] = Type::normalizeArrayOfUInteger(static::castValueToArray($data['ACCOMPLICES']));
			$data['AUDITORS'] = Type::normalizeArrayOfUInteger(static::castValueToArray($data['AUDITORS']));
		}

		return $result;
	}

	protected function doPreActions($operation)
	{
		$data = $this->getTransitionState();
		if($data->isInProgress())
		{
			// todo: to "actions"
			if($operation == 'CREATE')
			{
				\CTaskCountersProcessor::getInstance()->onBeforeTaskAdd($data, $this->getUserId());
			}
		}

		return $data->getResult();
	}

	protected function doPostActions($operation)
	{
		$data = $this->getTransitionState();
		if($data->isInProgress())
		{
			if($operation == 'CREATE')
			{
				$id = $this->getId();
				$userId = $this->getUserId();

				// todo: think about "config object" that will allow to switch off some of these actions,
				// todo: this config object may support hierarchy and partial update
				// todo: example: ['notification' => ['enable' => true, 'add' => ['enable' => false]], 'cache' => ['reset' = ['enable' => true]]]
				// todo: we can use \Bitrix\Tasks\Util\Type\StructureChecker to check such option structure, set default one, etc...

				// todo: refactor this later, get rid of CTasks completely

				// todo: NOTE: for add() this is okay, but for update() some fields may be missing,
				// todo: so its better to use actual data here, not from state (which eventually may be incomplete)

				// todo: refactor this nasty crap
				\Bitrix\Tasks\Internals\Task\MemberTable::add(array(
					'TASK_ID' => $id,
					'USER_ID' => $data['CREATED_BY'],
					'TYPE' => 'O',
				));
				\Bitrix\Tasks\Internals\Task\MemberTable::add(array(
					'TASK_ID' => $id,
					'USER_ID' => $data['RESPONSIBLE_ID'],
					'TYPE' => 'R',
				));

				\CTasks::addAccomplices($id, static::castValueToArray($data["ACCOMPLICES"]));
				\CTasks::addAuditors($id, static::castValueToArray($data["AUDITORS"]));

				\CTasks::addTags($id, $data["CREATED_BY"], static::castValueToArray($data["TAGS"]) , $userId);
				\CTasks::addPrevious($id, static::castValueToArray($data["DEPENDS_ON"]));

				// todo: BELOW: do other stuff related with SE_ managing and other additional "actions"
				// todo: it must not be hardcoded (as we do traditionally),
				// todo: but should look as some kind of high-level event handling to be able to register, un-register and optionally turn off additional "actions"

				// add to favorite, if parent is in the favorites too
				if(intval($data['PARENT_ID']) && FavoriteTable::check(array('TASK_ID' => $data['PARENT_ID'], 'USER_ID' => $userId)))
				{
					FavoriteTable::add(array('TASK_ID' => $id, 'USER_ID' => $userId), array('CHECK_EXISTENCE' => false));
				}

				\CTasks::index($data, static::castValueToArray($data["TAGS"])); // search index
				\CTasks::__updateViewed($id, $userId, $onTaskAdd = true);
				\CTaskCountersProcessor::onAfterTaskAdd($data, $userId);

				// note that setting occur as is deprecated. use access checker switch off instead
				$occurAsUserId = \Bitrix\Tasks\Util\User::getOccurAsId();
				if(!$occurAsUserId)
				{
					$occurAsUserId = $this->getUserId();
				}

				// changes log
				$logItem = new Log(array(
					"TASK_ID" => $id,
					"USER_ID" => $occurAsUserId,
					"CREATED_DATE" => UI::formatDateTime($data->getEnterTime()),
					"FIELD" => "NEW"
				), $this->getUserId());
				$logItem->save();

				\CTaskSync::addItem($data); // MS Exchange

				$this->sendPullEvents();

				$batchState = static::getBatchState();
				$batchState->accumulateArray('USER', array_merge(array($data["CREATED_BY"], $data["RESPONSIBLE_ID"]), $data["ACCOMPLICES"], $data["AUDITORS"]));
				if($data['GROUP_ID'])
				{
					$batchState->accumulateArray('GROUP', array($data['GROUP_ID']));
				}

				\CTaskNotifications::sendAddMessage(
					array_merge($data->toArray(), array('CHANGED_BY' => $occurAsUserId)),
					array('SPAWNED_BY_AGENT' => $data['SPAWNED_BY_AGENT'] === 'Y' || $data['SPAWNED_BY_AGENT'] === true)
				);

				// todo: use Scheduler to adjust dates in case of links exist

				// if batch state is off, this will fire immediately.
				// otherwise, this will fire only when somebody calls ::leaveBatchState() on this class
				$batchState->fireLeaveCallback();
			}
		}

		return $data->getResult();
	}

	public static function processEnterBatchMode(State\Trigger $state)
	{
		\CTaskNotifications::disableAutoDeliver(); // start buffering notifications
	}

	public static function processLeaveBatchMode(State\Trigger $state)
	{
		global $CACHE_MANAGER;

		$users = $state->getArray('USER');
		$groups = $state->getArray('GROUP');

		// todo: think about "config object" that will allow to switch off some of these actions

		foreach($groups as $group)
		{
			$CACHE_MANAGER->ClearByTag("tasks_group_".$group);
			Group::updateLastActivity($group);
		}
		foreach($users as $userId)
		{
			$CACHE_MANAGER->ClearByTag("tasks_user_".$userId);
		}

		\CTaskNotifications::enableAutoDeliver(); // flush buffer and stop buffering

		return new Result(); // formally
	}

	public function getShortPreview()
	{
		return $this['TITLE'].' ['.$this->getId().']';
	}

	protected function sendPullEvents()
	{
		$data = $this->getTransitionState();
		$result = $data->getResult();
		if($data->isInProgress())
		{
			$id = $this->getId();
			$recipients = array_merge(array($data["CREATED_BY"], $data["RESPONSIBLE_ID"]), $data["ACCOMPLICES"], $data["AUDITORS"]);

			try
			{
				$groupId = intval($data['GROUP_ID']);

				$arPullData = array(
					'TASK_ID' => $id,
					'AFTER' => array(
						'GROUP_ID' => $groupId
					),
					'TS' => time(),
					'event_GUID' => isset($data['META::EVENT_GUID']) ? $data['META::EVENT_GUID'] : sha1(uniqid('AUTOGUID', true))
				);

				Pull::emitMultiple($recipients, 'TASKS_GENERAL_#USER_ID#', 'task_add', $arPullData);
				Pull::emitMultiple($recipients, 'TASKS_TASK_'.$id, 'task_add', $arPullData);
			}
			catch (\Exception $e)
			{
				$result->getErrors()->addWarning('POST_ACTION_FAILURE.PULL', $e->getMessage(), array('EXCEPTION' => $e));
			}
		}

		return $result;
	}

	private static function processDurationPlanFields(&$data)
	{
		$type = $data['DURATION_TYPE'];

		$durationPlan = false;
		if(isset($data['DURATION_PLAN_SECONDS']))
		{
			$durationPlan = $data['DURATION_PLAN_SECONDS'];
		}
		elseif(isset($data['DURATION_PLAN']))
		{
			$durationPlan = self::convertDurationToSeconds($data['DURATION_PLAN'], $type);
		}

		if($durationPlan !== false) // smth were done
		{
			$data['DURATION_PLAN'] = $durationPlan;
			unset($data['DURATION_PLAN_SECONDS']);
		}
	}

	/**
	 * Converts duration value according to the unit specifed
	 *
	 * @param $value
	 * @param $type
	 * @return int Number of seconds
	 */
	private static function convertDurationToSeconds($value, $type)
	{
		if($type == \CTasks::TIME_UNIT_TYPE_HOUR)
		{
			// hours to seconds
			return intval($value) * 3600;
		}
		elseif($type == \CTasks::TIME_UNIT_TYPE_DAY || (string) $type == ''/*days by default, see install/db*/)
		{
			// days to seconds
			return intval($value) * 86400;
		}

		return $value;
	}

	private static function castValueToArray($value)
	{
		if(is_array($value))
		{
			return $value;
		}
		if(Util\Collection::isA($value) || $value instanceof Dictionary)
		{
			return $value->toArray();
		}
		return (array) $value;
	}
}