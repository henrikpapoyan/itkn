<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage tasks
 * @copyright 2001-2016 Bitrix
 *
 * @internal
 */

namespace Bitrix\Tasks\Item;

use Bitrix\Tasks\Util\Collection;
use Bitrix\Tasks\Util\Filter;

abstract class Converter
{
	protected $subConverterCache = array();
	private $config = array(); // todo: configure object here later

	/**
	 * Returns destination item class
	 *
	 * @return string
	 * @throws \Bitrix\Main\NotImplementedException
	 */
	public static function getTargetItemClass()
	{
		throw new \Bitrix\Main\NotImplementedException('No default destination item class');
	}

	/**
	 * Returns a map of converter classes for sub-entities
	 *
	 * @return array
	 */
	protected static function getSubEntityConverterClassMap()
	{
		// only the following sub-entites will be converted
		return array();
	}

	public function setConfig($fields, $value)
	{
		$this->config[$fields] = $value;
	}

	/**
	 * Do item data conversion (main data + user fields)
	 *
	 * @param $instance
	 * @return Result
	 */
	public function convert($instance)
	{
		$result = new Result();

		$dstClass = $this->getTargetItemClass();
		$newInstance = null;
		$userId = $instance->getUserId();

		// todo: check instance, DONT fire exception, add error

		$data = $instance->getData(); // will get all data: fields, user fields, sub-item collections, etc...
		if(empty($data))
		{
			$result->getErrors()->add('NO_SOURCE_DATA', 'No source data to work with (does item even exist?)');
		}
		else
		{
			$newInstance = new $dstClass(0, $userId);
			unset($data['ID']); // todo: ID may not be a column of primary, and primary may be compound
			$newData = $this->transformData($data, $instance, $newInstance);

			// convert user fields
			$srcUFCtrl = $instance->getContext()->getUserFieldController();
			$dstUFCtrl = $newInstance->getContext()->getUserFieldController();

			$cloneParameters = array();
			if($this->config['UF.FILTER'])
			{
				$cloneParameters['FILTER'] = new Filter($this->config['UF.FILTER']);
			}

			if($srcUFCtrl && $dstUFCtrl)
			{
				// remove fields that are not present in $newInstance
				foreach($newData as $k => $v)
				{
					if($dstUFCtrl->isUFKey($k) && !$dstUFCtrl->checkFieldExists($k))
					{
						unset($newData[$k]);
					}
				}

				// then clone the remaining
				$ufCloneResult = $srcUFCtrl->cloneValues($newData, $dstUFCtrl, $userId, $cloneParameters);
				if(!$ufCloneResult->getErrors()->isEmpty())
				{
					$result->getErrors()->load($ufCloneResult->getErrors());
				}

				if($ufCloneResult->isSuccess())
				{
					$newData = $ufCloneResult->getData();
				}
			}

			// convert sub-entities
			$dstMap = $newInstance->getMap();
			foreach($dstMap as $k => $v)
			{
				if($v['src'] == 'SE' && isset($newData[$k]) && is_object($newData[$k]))
				{
					$subConverter = $this->getConverterForSubEntity($k);
					if($subConverter)
					{
						if(Collection::isA($newData[$k]))
						{
							$subData = array();
							foreach($newData[$k] as $j)
							{
								$subResult = $subConverter->convert($j);
								if($subResult->isSuccess())
								{
									$subData[] = $subResult->getInstance();
								}
								$result->getErrors()->load($subResult->getErrors()->prefixCode($k.'.'));
							}

							$newData[$k] = null;
							if(!empty($subData))
							{
								$newData[$k] = new Collection($subData);
							}
						}
						elseif(is_a($newData[$k], '\\Bitrix\\Tasks\\Item'))
						{
							// todo: also do for the single item
						}
					}
				}
			}

			$newInstance->setData($newData);
		}

		$result->setInstance($newInstance);

		return $result;
	}

	/**
	 * Dispose temporal resources taken with the conversion made on this item
	 *
	 * @param $instance
	 * @return Result
	 */
	public function abortConversion($instance)
	{
		$result = new Result();

		if(is_a($instance, '\\Bitrix\\Tasks\\Item'))
		{
			if(!$instance->isAttached())
			{
				// do dispose
				$srcUFCtrl = $instance->getContext()->getUserFieldController();
				if($srcUFCtrl)
				{
					$srcUFCtrl->cancelCloneValues($instance->getData(), $instance->getId());
				}

				// todo: also abort for all sub-entities
			}
			else
			{
				$result->getErrors()->add('ALREADY_SAVED', 'Item was already saved or is not a product of conversion at all');
			}
		}

		return $result;
	}

	protected function transformData(array $data, $srcInstance, $dstInstance)
	{
		return array();
	}

	protected function getConverterForSubEntity($name)
	{
		if(!$this->subConverterCache[$name])
		{
			$map = $this->getSubEntityConverterClassMap();

			if(isset($map[$name]))
			{
				$cClass = $map[$name]['class'];
				// todo: check class here
				$this->subConverterCache[$name] = new $cClass();
			}
		}

		return $this->subConverterCache[$name];
	}
}