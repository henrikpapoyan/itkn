<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage tasks
 * @copyright 2001-2016 Bitrix
 *
 * @internal
 */

namespace Bitrix\Tasks\Item\Converter\Task;

use Bitrix\Tasks\Item\Converter;
use Bitrix\Tasks\UI;

class ToTask extends Converter
{
	public static function getTargetItemClass()
	{
		return '\\Bitrix\\Tasks\\Item\\Task';
	}

	protected static function getSubEntityConverterClassMap()
	{
		// only the following sub-entites will be converted
		return array(
			'SE_CHECKLIST' => array(
				'class' => '\\Bitrix\\Tasks\\Item\\Converter\\Task\\CheckList\\ToTaskCheckList',
			)
		);
	}

	protected function transformData(array $data, $srcInstance, $dstInstance)
	{
		// drop unwanted
		unset($data['ID']);
		unset($data['GUID']);
		unset($data['STATUS']);
		unset($data['FORUM_TOPIC_ID']);
		unset($data['COMMENTS_COUNT']);

		unset($data['DATE_START']);
		unset($data['CREATED_DATE']);
		unset($data['CHANGED_DATE']);
		unset($data['VIEWED_DATE']);
		unset($data['STATUS_CHANGED_DATE']);
		unset($data['CLOSED_DATE']);

		unset($data['CHANGED_BY']);
		unset($data['STATUS_CHANGED_BY']);
		unset($data['CLOSED_BY']);

		unset($data['FILES']);

		unset($data['CREATED_BY_NAME']);
		unset($data['CREATED_BY_LAST_NAME']);
		unset($data['CREATED_BY_SECOND_NAME']);
		unset($data['CREATED_BY_LOGIN']);
		unset($data['CREATED_BY_WORK_POSITION']);
		unset($data['CREATED_BY_PHOTO']);

		unset($data['RESPONSIBLE_NAME']);
		unset($data['RESPONSIBLE_LAST_NAME']);
		unset($data['RESPONSIBLE_SECOND_NAME']);
		unset($data['RESPONSIBLE_LOGIN']);
		unset($data['RESPONSIBLE_WORK_POSITION']);
		unset($data['RESPONSIBLE_PHOTO']);

		unset($data['OUTLOOK_VERSION']);
		unset($data['FORKED_BY_TEMPLATE_ID']);
		unset($data['XML_ID']);

		//unset($data['DURATION_PLAN']);
		//unset($data['DURATION_FACT']);
		unset($data['ZOMBIE']);

		// do not spawn tasks with description in html format
		if($data['DESCRIPTION_IN_BBCODE'] != 'Y')
		{
			if($data['DESCRIPTION'] != '')
			{
				$data['DESCRIPTION'] = UI::convertHtmlToBBCode($data['DESCRIPTION']);
			}

			$data['DESCRIPTION_IN_BBCODE'] = 'Y';
		}

		unset($data['SE_PROJECTDEPENDENCE']); // cant copy that yet (and should I?)

		return $data;
	}
}