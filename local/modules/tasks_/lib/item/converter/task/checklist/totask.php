<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage tasks
 * @copyright 2001-2016 Bitrix
 *
 * @internal
 */

namespace Bitrix\Tasks\Item\Converter\Task\CheckList;

use Bitrix\Tasks\Item\Converter;

class ToTask extends Converter
{
	public static function getTargetItemClass()
	{
		return '\\Bitrix\\Tasks\\Item\\Task';
	}

	protected function transformData(array $data, $srcInstance, $dstInstance)
	{
		// transforming the checklist item into a sub-task, just for fun
		return array(
			'TITLE' => $data['TITLE'],
			'RESPONSIBLE_ID' => $data['CREATED_BY'], // todo: implement $srcInstance->getParent() then
			'CREATED_BY' => $data['CREATED_BY'],
			'PARENT_ID' => $data['TASK_ID']
		);
	}
}