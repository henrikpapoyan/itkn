<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage tasks
 * @copyright 2001-2016 Bitrix
 */

namespace Bitrix\Tasks\Item\Converter\Task\Template;

use Bitrix\Tasks\Item\Converter;
use Bitrix\Tasks\UI;

class ToTask extends Converter
{
	public static function getTargetItemClass()
	{
		return '\\Bitrix\\Tasks\\Item\\Task';
	}

	protected static function getSubEntityConverterClassMap()
	{
		// only the following sub-entites will be converted
		return array(
			'SE_CHECKLIST' => array(
				'class' => '\\Bitrix\\Tasks\\Item\\Converter\\Task\\Template\\CheckList\\ToTaskCheckList',
			)
		);
	}

	protected function transformData(array $data, $srcInstance, $dstInstance)
	{
		$data['FORKED_BY_TEMPLATE_ID'] = $srcInstance->getId();

		// drop unwanted
		unset($data['ID']);
		unset($data['GUID']);
		unset($data['STATUS']);
		unset($data['REPLICATE']);
		unset($data['REPLICATE_PARAMS']);
		unset($data['RESPONSIBLES']);
		unset($data['FORUM_TOPIC_ID']);
		unset($data['COMMENTS_COUNT']);
		unset($data['DATE_START']);
		unset($data['CREATED_DATE']);
		unset($data['CHANGED_DATE']);
		unset($data['VIEWED_DATE']);
		unset($data['STATUS_CHANGED_DATE']);
		unset($data['CHANGED_BY']);
		unset($data['STATUS_CHANGED_BY']);
		unset($data['CLOSED_BY']);
		unset($data['TASK_ID']);
		unset($data['TPARAM_TYPE']);
		unset($data['TPARAM_REPLICATION_COUNT']);
		unset($data['MULTITASK']);
		unset($data['BASE_TEMPLATE_ID']);
		unset($data['FILES']);

		unset($data['CREATED_BY_NAME']);
		unset($data['CREATED_BY_LAST_NAME']);
		unset($data['CREATED_BY_SECOND_NAME']);
		unset($data['CREATED_BY_LOGIN']);
		unset($data['CREATED_BY_WORK_POSITION']);
		unset($data['CREATED_BY_PHOTO']);

		unset($data['RESPONSIBLE_NAME']);
		unset($data['RESPONSIBLE_LAST_NAME']);
		unset($data['RESPONSIBLE_SECOND_NAME']);
		unset($data['RESPONSIBLE_LOGIN']);
		unset($data['RESPONSIBLE_WORK_POSITION']);
		unset($data['RESPONSIBLE_PHOTO']);

		if((string) $data['DEADLINE_AFTER'] != '')
		{
			$data['DEADLINE'] = static::getDeadLine($data['DEADLINE_AFTER']);
		}
		unset($data['DEADLINE_AFTER']);

		// do not spawn tasks with description in html format
		if($data['DESCRIPTION_IN_BBCODE'] != 'Y')
		{
			if($data['DESCRIPTION'] != '')
			{
				$data['DESCRIPTION'] = UI::convertHtmlToBBCode($data['DESCRIPTION']);
			}

			$data['DESCRIPTION_IN_BBCODE'] = 'Y';
		}

		return $data;
	}

	private static function getDeadLine($deadlineAfter)
	{
		$deadlineAfter = intval($deadlineAfter);

		if ($deadlineAfter)
		{
			$deadlineAfter = $deadlineAfter / 86400; // to days
			return \Bitrix\Tasks\UI::formatDateTime(strtotime(date("Y-m-d 00:00")." +".$deadlineAfter." days"));
		}

		return '';
	}
}