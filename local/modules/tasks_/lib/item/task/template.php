<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage tasks
 * @copyright 2001-2016 Bitrix
 */

namespace Bitrix\Tasks\Item\Task;

use Bitrix\Tasks\Util\Type;

final class Template extends \Bitrix\Tasks\Item
{
	public static function getDataSourceClass()
	{
		return '\\Bitrix\\Tasks\\Internals\\Task\\TemplateTable';
	}
	public static function getUserFieldControllerClass()
	{
		return '\\Bitrix\\Tasks\\Util\\UserField\\Task\\Template';
	}

	protected static function getCustomFieldMap()
	{
		return array(

			// legacy-custom

			// sub-entity
			'SE_CHECKLIST' => array(
				'src' => 'SE',
				'class' => '\\Bitrix\\Tasks\\Item\\Task\\Template\\CheckList'
			)
		);
	}

	public function externalizeFieldValue($name, $value)
	{
		if($name == 'TAGS' || $name == 'ACCOMPLICES' || $name == 'AUDITORS' || $name == 'DEPENDS_ON' || $name == 'RESPONSIBLES' || $name == 'REPLICATE_PARAMS' || $name == 'FILES')
		{
			return Type::unSerializeArray($value);
		}

		return parent::externalizeFieldValue($name, $value);
	}

	public function internalizeFieldValue($name, $value)
	{
		if($name == 'TAGS' || $name == 'ACCOMPLICES' || $name == 'AUDITORS' || $name == 'DEPENDS_ON' || $name == 'RESPONSIBLES' || $name == 'REPLICATE_PARAMS' || $name == 'FILES')
		{
			return Type::serializeArray($value);
		}

		return $value;
	}

	protected function prepareData()
	{
		$data = $this->getTransitionState();
		$result = $data->getResult();
		if ($data->isInProgress())
		{
			$id = $this->getId();
			//$now = $data->getEnterTimeFormatted();

			if($id)
			{
				// todo
			}
			else
			{
				// DO NOT ASSIGN RESPONSIBLE_ID, null is a legal value here

				if(!$data->containsKey('STATUS'))
				{
					$data['STATUS'] = \CTasks::STATE_NEW;
				}
				if(!$data->containsKey('SITE_ID'))
				{
					$data['SITE_ID'] = SITE_ID;
				}
				if(!$data->containsKey('CREATED_BY'))
				{
					$data['CREATED_BY'] = $this->getUserId();
				}
			}
		}

		return $result;
	}
}