<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage tasks
 * @copyright 2001-2016 Bitrix
 */

namespace Bitrix\Tasks\Item\Task;

use Bitrix\Main\HttpApplication;

final class CheckList extends \Bitrix\Tasks\Item\SubItem
{
	protected static function getParentConnectorField()
	{
		return 'TASK_ID';
	}

	public static function getDataSourceClass()
	{
		return '\\Bitrix\\Tasks\\Internals\\Task\\CheckListTable';
	}

	protected function prepareData()
	{
		$data = $this->getTransitionState();
		$result = $data->getResult();

		if(parent::prepareData()->isSuccess())
		{
			if($data->isInProgress())
			{
				if(!array_key_exists('SORT_INDEX', $data))
				{
					$data['SORT_INDEX'] = $this->getMaxSort();
				}

				if(!$this->getId()) // its CREATE
				{
					if(!array_key_exists('CREATED_BY', $data))
					{
						$data['CREATED_BY'] = $this->getUserId();
					}
				}
			}
		}

		return $result;
	}

	private function getMaxSort()
	{
		$index = 0;

		$parent = $this->getParent();
		if($parent)
		{
			$parentId = $parent->getId();
			if($parentId)
			{
				$connection = HttpApplication::getConnection();
				$dc = static::getDataSourceClass();

				$result = $connection->query("
					SELECT MAX(SORT_INDEX) AS MAX_SORT_INDEX
					FROM ".$dc::getTableName()."
					WHERE TASK_ID = '".intval($parentId)."'")->fetch();

				if($result && isset($result['MAX_SORT_INDEX']))
				{
					$index = intval($result['MAX_SORT_INDEX']) + 1;
				}
			}
		}

		return $index;
	}
}