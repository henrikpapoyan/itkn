<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage tasks
 * @copyright 2001-2016 Bitrix
 */

namespace Bitrix\Tasks\Item\Task\Template;

final class CheckList extends \Bitrix\Tasks\Item\SubItem
{
	protected static function getParentConnectorField()
	{
		return 'TEMPLATE_ID';
	}

	public static function getDataSourceClass()
	{
		return '\\Bitrix\\Tasks\\Internals\\Task\\Template\\CheckListTable';
	}
}