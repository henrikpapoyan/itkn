<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage tasks
 * @copyright 2001-2016 Bitrix
 */

namespace Bitrix\Tasks\Item;

use Bitrix\Main\Type\Dictionary;

final class Collection extends Dictionary
{
	// todo: may be some functionality for simple searching and filtering on data loaded here, or like that

	public function filter()
	{
		// todo: return collection instance
	}

	public function sort()
	{
		// todo: return collection instance
	}

	public function getOne()
	{
		// todo: return item instance
	}

	public function add()
	{
	}

	public function remove()
	{
	}

	public static function isA($object)
	{
		return is_object($object) && is_a($object, get_called_class());
	}
}