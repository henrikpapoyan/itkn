<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage tasks
 * @copyright 2001-2016 Bitrix
 */

namespace Bitrix\Tasks\Item;

use Bitrix\Main\Type\Dictionary;
use Bitrix\Tasks\Util\User;
use Bitrix\Tasks\UI;

class State extends Dictionary
{
	protected $isInside = false;
	protected $result = null;
	protected $time = 0;

	public function __construct(array $values = null)
	{
		parent::__construct($values);
		$this->leave();
	}

	public function enter(array $values = array())
	{
		$this->isInside = true;
		$this->values = $values;
		$this->time = User::getTime(); // get current user`s local time
	}

	public function leave()
	{
		$this->isInside = false;
		$this->values = array();
		$this->time = 0;
		$this->result = new Result();
	}

	public function isInProgress()
	{
		return $this->isInside;
	}

	public function getResult()
	{
		return $this->result;
	}

	public function getEnterTime()
	{
		return $this->time;
	}

	public function getEnterTimeFormatted()
	{
		return UI::formatDateTime($this->time);
	}

	public function containsKey($key)
	{
		return array_key_exists($key, $this->values);
	}
}