<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage tasks
 * @copyright 2001-2016 Bitrix
 */

namespace Bitrix\Tasks\Item;

use Bitrix\Main\SystemException;

final class Context
{
	protected $entity = null;
	protected $accessController = null;
	protected $userFieldController = null;
	protected $configurable = true;

	public function __construct($entity)
	{
		$this->entity = $entity;
	}

	public function lock()
	{
		$this->configurable = false;
	}

	public function getAccessController()
	{
		if($this->accessController === null)
		{
			$entity = $this->entity;

			$cClass = $entity::getAccessControllerClass();

			$this->accessController = new $cClass;
		}

		return $this->accessController;
	}

	public function setAccessController($instance)
	{
		if(!$this->configurable)
		{
			throw new SystemException('Controller is non-configurable');
		}

		$this->accessController = $instance;
	}

	public function getUserFieldController()
	{
		if($this->userFieldController === null)
		{
			$entity = $this->entity;

			$cClass = $entity::getUserFieldControllerClass();
			if($cClass)
			{
				$this->userFieldController = new $cClass;
			}
			else
			{
				$this->userFieldController = false; // no uf controller for this entity
			}
		}

		return $this->userFieldController;
	}

	public function setUserFieldController($instance)
	{
		if(!$this->configurable)
		{
			throw new SystemException('Controller is non-configurable');
		}

		$this->userFieldController = $instance;
	}

	/*
	private function checkAccessController($cClass)
	{
		if(!is_a($cClass, 'Bitrix\\Tasks\\Item\\Context\\Access', true))
		{
			throw new \Bitrix\Main\ArgumentException('Access controller class must be a subclass of \\Bitrix\\Tasks\\Item\\Context\\Access');
		}
	}

	private function checkUserFieldController($cClass)
	{
		if(!is_a($cClass, 'Bitrix\\Tasks\\Util\\UserField', true))
		{
			throw new \Bitrix\Main\ArgumentException('User field controller class must be a subclass of \\Bitrix\\Tasks\\Util\\UserField');
		}
	}
	*/
}