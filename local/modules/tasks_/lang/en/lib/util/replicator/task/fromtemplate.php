<?
$MESS["TASKS_REPLICATOR_TASK_CREATED"] = "Scheduled task created";
$MESS["TASKS_REPLICATOR_TASK_CREATED_WITH_ERRORS"] = "Scheduled task created, there were errors";
$MESS["TASKS_REPLICATOR_TASK_WAS_NOT_CREATED"] = "Scheduled task was not created";
$MESS["TASKS_REPLICATOR_TASK_POSSIBLY_WAS_NOT_CREATED"] = "It is possible that scheduled task was not created";
$MESS["TASKS_REPLICATOR_PROCESS_STOPPED"] = "Task iteration stopped";
$MESS["TASKS_REPLICATOR_END_DATE_REACHED"] = "End date reached";
$MESS["TASKS_REPLICATOR_LIMIT_REACHED"] = "Maximum iterations reached";
$MESS["TASKS_REPLICATOR_ILLEGAL_NEXT_TIME"] = "Could not calculate the time of the next run";
$MESS["TASKS_REPLICATOR_PROCESS_ERROR"] = "Error calculating the time of the next run";
$MESS["TASKS_REPLICATOR_NEXT_TIME"] = "Next run scheduled at: #TIME# (in #PERIOD# seconds)";
$MESS["TASKS_REPLICATOR_SUBTREE_LOOP"] = "It is possible that some of the subtemplates are self-referenced. Subtasks have not been created.";
?>