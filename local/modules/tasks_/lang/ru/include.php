<?
// statuses
$MESS["TASKS_TASK_STATUS_1"] = "Новая";
$MESS["TASKS_TASK_STATUS_2"] = "Ждет выполнения";
$MESS["TASKS_TASK_STATUS_3"] = "Выполняется";
$MESS["TASKS_TASK_STATUS_4"] = "Ждет контроля";
$MESS["TASKS_TASK_STATUS_5"] = "Завершена";
$MESS["TASKS_TASK_STATUS_6"] = "Отложена";
$MESS["TASKS_TASK_STATUS_7"] = "Отклонена";
$MESS["TASKS_TASK_STATUS_-1"] = "Просрочена";

// commonly used phrases
$MESS["TASKS_COMMON_TASK"] = "Задача";
$MESS["TASKS_COMMON_TASKS"] = "Задачи";

$MESS["TASKS_COMMON_CONFIRM_DELETE"] = 'Вы уверены, что хотите удалить #ENTITY_NAME#?';
$MESS["TASKS_COMMON_LOADING"] = "Загрузка";
$MESS["TASKS_COMMON_CANCEL"] = "Отмена";
$MESS["TASKS_COMMON_SAVE"] = "Сохранить";
$MESS["TASKS_COMMON_SAVE_CHANGES"] = "Сохранить изменения";
$MESS["TASKS_COMMON_CANCEL_SELECT"] = "Отменить выбор";
$MESS["TASKS_COMMON_CHANGE"] = "Сменить";
$MESS["TASKS_COMMON_ADD"] = "Добавить";
$MESS["TASKS_COMMON_ADD_MORE"] = "Добавить еще";
$MESS["TASKS_COMMON_DONT_SHOW_AGAIN"] = "Больше не показывать";
$MESS["TASKS_COMMON_DONT_ASK_AGAIN"] = "Больше не спрашивать об этом";
$MESS["TASKS_COMMON_NOT_SELECTED"] = "Не выбрано";
$MESS["TASKS_COMMON_NO"] = "Нет";
$MESS["TASKS_COMMON_ALL"] = "Все";
$MESS["TASKS_COMMON_ANY"] = "Любая";
$MESS["TASKS_COMMON_YES"] = "Да";
$MESS["TASKS_COMMON_NO"] = "Нет";

$MESS["TASKS_COMMON_ACCESS_DENIED"] = "Доступ запрещен";
$MESS["TASKS_COMMON_ACTION_NOT_ALLOWED"] = "Действие запрещено";

$MESS["TASKS_COMMON_HIDE"] = "Скрыть";
$MESS["TASKS_COMMON_DELETE"] = "Удалить";
$MESS["TASKS_COMMON_CLOSE"] = "Закрыть";