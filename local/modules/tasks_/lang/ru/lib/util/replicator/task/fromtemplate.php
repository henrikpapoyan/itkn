<?
$MESS['TASKS_REPLICATOR_TASK_CREATED'] = 'Создана задача по расписанию';
$MESS['TASKS_REPLICATOR_TASK_CREATED_WITH_ERRORS'] = 'Создана задача по расписанию, с ошибками';
$MESS['TASKS_REPLICATOR_TASK_WAS_NOT_CREATED'] = 'Задача по расписанию не была создана';
$MESS['TASKS_REPLICATOR_TASK_POSSIBLY_WAS_NOT_CREATED'] = 'Задача по расписанию, возможно, не была создана';
$MESS['TASKS_REPLICATOR_PROCESS_STOPPED'] = 'Процесс повторения задачи остановлен';
$MESS['TASKS_REPLICATOR_END_DATE_REACHED'] = 'Достигнута дата окончания';
$MESS['TASKS_REPLICATOR_LIMIT_REACHED'] = 'Достигнут лимит повтора';
$MESS['TASKS_REPLICATOR_ILLEGAL_NEXT_TIME'] = 'Время следующего повтора не вычислено';
$MESS['TASKS_REPLICATOR_PROCESS_ERROR'] = 'Ошибка процесса рассчета времени повтора';
$MESS['TASKS_REPLICATOR_NEXT_TIME'] = 'Время следующего повтора: #TIME# (через #PERIOD# секунд)';
$MESS['TASKS_REPLICATOR_SUBTREE_LOOP'] = 'Кажется, в дереве под-шаблонов есть зацикливание. Под-задачи не были созданы';