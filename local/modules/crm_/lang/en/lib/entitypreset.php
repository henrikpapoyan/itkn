<?
$MESS["CRM_ENTITY_TYPE_REQUISITE"] = "Details";
$MESS["CRM_ENTITY_TYPE_REQUISITE_DESC"] = "Templates for contact or company details";
$MESS["CRM_ENTITY_PRESET_ERR_DELETE_PRESET_USED"] = "The template cannot be deleted because there are details created on it.";
$MESS["CRM_ENTITY_PRESET_ERR_PRESET_NOT_FOUND"] = "Template was not found.";
$MESS["CRM_ENTITY_PRESET_ERR_INVALID_ENTITY_TYPE"] = "Invalid entity type for use with template.";
?>