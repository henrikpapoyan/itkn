<?php

namespace Bitrix\Crm\Rest;

use Bitrix\Main\Config\Option;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class CCrmExternalChannelConnector
{
	const Prefix = 'options_';
	const Suffix = 'ext_channel.';

	public $channel_id = null;
	public $server = null;

	public function setServer(\CRestServer $server)
	{
		$this->server = $server;
	}

	public function getServer()
	{
		return $this->server;
	}

	public function isRegistered()
	{
		return $this->getChannel() !== false;
	}

	public function setChannelId($channel_id)
	{
		$this->channel_id = $channel_id;
	}

	public function getChannelId()
	{
		return $this->channel_id;
	}

	public function getChannel()
	{
		return Option::get("crm", self::Prefix.$this->getChannelId(), false);
	}

	public function getOption($params)
	{
		if(($options = $this->getChannel()) && strlen($options) > 0)
		{
			$options = unserialize($options);
		}
		else
		{
			$options = array();
		}

		if(isset($params['option']))
		{
			return isset($options[$params['option']]) ? $options[$params['option']] : null;
		}
		else
		{
			return $options;
		}
	}

	public function getList()
	{
		$options = array();

		$con = \Bitrix\Main\Application::getConnection();
		$res = $con->query(
				"SELECT VALUE ".
				"FROM b_option o ".
				"WHERE module_id='crm' and name like('%".self::Prefix.self::Suffix."%')"
		);
		while ($ar = $res->fetch())
		{
			$options[] = unserialize($ar['VALUE']);
		}
		return $options;
	}

	public function getByOriginalId($originatorId)
	{
		$connectorLast = array();

		$result = $this->getList();

		$connectorList = array();
		if(count($result)>0)
		{
			foreach($result as $r)
			{
				if($r['ORIGINATOR_ID'] == $originatorId)
				{
					$connectorList[] = $r;
				}
			}

			$connectorLast =  array_pop($connectorList);
		}

		return $connectorLast;
	}

	public function checkFields(&$fields, &$errors)
	{
		if(!is_set($fields, 'NAME') || $fields['NAME'] =='')
		{
			$fields['NAME'] = Loc::getMessage("CRM_REST_EXTERNAL_CHANNEL_CONNECTOR_DEFAULT_NAME");
		}
		elseif(!is_set($fields, 'ORIGINATOR_ID') || $fields['ORIGINATOR_ID'] =='')
		{
			$errors[] = "ORIGINATOR_ID is not defined";
		}
	}

	public function register($fields)
	{
		$channel_id = uniqid(self::Suffix, true);

		$setting = array(
				'APP_ID'=>$this->server->getAppId(),
				'VERSION'=>'1',
				'TIME'=> time() + \CTimeZone::GetOffset(),
				'NAME'=>$fields['NAME'],
				'ORIGINATOR_ID'=>$fields['ORIGINATOR_ID'],
				'EXTERNAL_SERVER_HOST'=>$fields['EXTERNAL_SERVER_HOST'],
		);

		Option::set("crm", self::Prefix.$channel_id, serialize($setting), false);

		return $channel_id;
	}

	public function unRegister($id)
	{
		if(strlen($id)<=0)
			return false;

		$filter = array(
			'name'=>self::Prefix.$id
		);

		Option::delete("crm", $filter);
	}
}