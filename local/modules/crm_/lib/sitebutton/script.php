<?php
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage intranet
 * @copyright 2001-2016 Bitrix
 */

namespace Bitrix\Crm\SiteButton;

use Bitrix\Crm\SiteButton\Internals\ButtonTable;
use \Bitrix\Main\Application;
use \Bitrix\Main\IO\File;
use Bitrix\Main\Loader;
use \Bitrix\Main\Web\Json;

/**
 * Class Script
 * @package Bitrix\Crm\SiteButton
 */
class Script
{
	const UPLOAD_PATH = '/crm';
	const UPLOAD_PATH_ADD = '/site_button/';
	const UPLOAD_FILE_NAME = 'loader_#id#_#sec#.js';
	const LOADER_PATH = '/bitrix/js/crm/button_loader.js';

	protected $id = null;
	protected $button = null;
	protected $errors = array();
	protected $resources = array(
		'style.css' => '/bitrix/components/bitrix/crm.button.button/templates/.default/style.css',
		'webform_style.css' => '/bitrix/components/bitrix/crm.button.webform/templates/.default/style.css',
	);

	public function __construct(Button $button)
	{
		$this->button = $button;
	}

	protected function getLayout()
	{
		$data = $this->button->getData();
		$typeList = Manager::getTypeList();
		$widgets = array();
		foreach ($typeList as $typeId => $typeName)
		{
			if ($this->button->hasActiveItem($typeId))
			{
				$widgets[] = $typeId;
			}
		}

		ob_start();

		/*@var $APPLICATION CMain*/
		global $APPLICATION;
		$APPLICATION->IncludeComponent("bitrix:crm.button.button", ".default", array(
			'PREVIEW' => false,
			'WIDGETS' => $widgets,
			'LOCATION' => (int) $data['LOCATION'],
			'COLOR_ICON' => $data['ICON_COLOR'],
			'COLOR_BACKGROUND' => $data['BACKGROUND_COLOR'],
		));

		return ob_get_clean();
	}

	public function getLoaderFileName()
	{
		$buttonData = $this->button->getData();
		return str_replace(
			array('#id#', '#sec#'),
			array($this->button->getId(), $buttonData['SECURITY_CODE']),
			self::UPLOAD_FILE_NAME
		);
	}

	public static function removeCache(Button $button)
	{
		$resourceLoader = self::getResourceLoader($button->getId(), $button);
		ResourceManager::removeFiles($resourceLoader);
	}

	public static function saveCache(Button $button)
	{
		$resourceLoader = self::getResourceLoader($button->getId(), $button);
		$result = ResourceManager::uploadFiles($resourceLoader);

		//$resourceFiles = self::getResourceFiles();
		//$result = ResourceManager::uploadFiles($resourceFiles);

		/*
		$result = $this->saveFile(
			str_replace('#id#', $this->button->getId(), self::UPLOAD_FILE_NAME),
			$this->getCache()
		);
		foreach ($this->resources as $resourceName => $resourcePath)
		{
			$result = $this->saveFile(
				$resourceName,
				File::getFileContents(Application::getDocumentRoot() . $resourcePath)
			);
		}
		*/

		return $result;
	}

	public static function getScript(Button $button)
	{
		//$loaderLink = self::getDomainPath(self::UPLOAD_PATH . self::UPLOAD_PATH_ADD . str_replace('#id#', $id, self::UPLOAD_FILE_NAME));
		$instance = new static($button);
		$fileName = $instance->getLoaderFileName();
		$loaderLink = ResourceManager::getFileUrl($fileName);

		if(!$loaderLink)
		{
			return null;
		}

		return
			"<script data-skip-moving=\"true\">
	(function(w,d,u,b){
		s=d.createElement('script');r=1*new Date();s.async=1;s.src=u+'?'+r;
		h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
	})(window,document,'" . $loaderLink . "');
</script>";
	}

	protected function getCache()
	{
		$loaderContents = File::getFileContents(Application::getDocumentRoot() . self::LOADER_PATH);
		if(!$loaderContents)
		{
			$this->errors[] = 'Can\'t find button_loader.js';
			return null;
		}

		return $loaderContents . "\n\n" . $this->getLoaderStarter();
	}

	/*
		Bitrix24ButtonLoader.init({
			resources: [
				'/bitrix/components/bitrix/crm.button.button/templates/.default/style.css'
			],
			delay: 0,
			widgets: [
				{
					id: '',
					script: '',
					show: 'BX.LiveChat.openLiveChat()',
					pages: {
						mode: 'INCLUDE',
						list: [
							''
						]
					}
				}
			],
			layout: ''
		});
	 * */
	protected function getLoaderStarter()
	{
		$widgets = array();
		$typeList = Manager::getTypeList();
		foreach ($typeList as $typeId => $typeName)
		{
			if(!$this->button->hasActiveItem($typeId))
			{
				continue;
			}

			$item = $this->button->getItemByType($typeId);
			if(!is_callable($item['FUNCTION_GET_WIDGET']))
			{
				continue;
			}
			$script = $item['FUNCTION_GET_WIDGET']($item['EXTERNAL_ID']);
			if(!$script)
			{
				continue;
			}

			$pages = array(
				'mode' => 'EXCLUDE',
				'list' => array()
			);
			if($this->button->hasItemPages($typeId))
			{
				$pages['mode'] = $item['PAGES']['MODE'];
				$pages['list'] = $item['PAGES']['LIST'][$pages['mode']];
			}

			$widgets[] = array(
				'id' => $typeId,
				'script' => $script,
				'show' => $item['JS_SHOW_WIDGET']($item['EXTERNAL_ID']),
				'hide' => $item['JS_HIDE_WIDGET']($item['EXTERNAL_ID']),
				'pages' => $pages
			);
		}

		$data = $this->button->getData();

		$resources = array();
		foreach ($this->resources as $resourceName => $resourcePath)
		{
			//$resources[] = self::getDomainPath(self::UPLOAD_PATH . self::UPLOAD_PATH_ADD . $resourceName);
			$resources[] = self::getFileContents($resourcePath);
		}
		$params = array(
			'isActivated' => $data['ACTIVE'] != 'N',
			'resources' => $resources,
			'delay' => (int) $data['DELAY'],
			'widgets' => $widgets,
			'layout' => $this->getLayout()
		);

		return 'window.BX.SiteButton.init(' . Json::encode($params) . ');';
	}

	protected static function getFileContents($path)
	{
		$path = Application::getDocumentRoot() . $path;

		$minPathPos = strrpos($path, '.');
		if ($minPathPos !== false)
		{
			$minPathSub = substr($path, 0, $minPathPos);
			$minPathExt = substr($path, $minPathPos);
			$minPath = $minPathSub . '.min' . $minPathExt;
			if (File::isFileExists($minPath))
			{
				$path = $minPath;
			}
		}

		return File::getFileContents($path);
	}

	protected static function getDomainPath($path)
	{
		//TODO: upload path from settings
		$host = Application::getInstance()->getContext()->getServer()->getHttpHost();
		return 'http://' . $host . '/upload' . $path;
	}

	protected function isB24()
	{
		return (bool) Loader::includeModule('bitrix24');
	}

	public function getErrors()
	{
		return $this->errors;
	}

	public static function getResourceLoader($buttonId, Button $button = null)
	{
		if(!$button)
		{
			$button = new Button($buttonId);
		}

		$instance = new static($button);

		return array(
			array(
				'name' => $instance->getLoaderFileName(),
				'type' => 'text/javascript',
				'path' => '',
				'contents' => $instance->getCache(),
				'provider_function' => '\\Bitrix\\Crm\\SiteButton\\Script::getResourceLoader',
				'provider_params' => $buttonId,
				'provider_module_id' => 'crm',
			)
		);
	}

	public static function getResourceFiles($fileName = null)
	{
		$resources = array(
			array(
				'name' => 'style.css',
				'type' => 'text/css',
				'path' => '/bitrix/components/bitrix/crm.button.button/templates/.default/style.css',
			),
			array(
				'name' => 'webform_style.css',
				'type' => 'text/css',
				'path' => '/bitrix/components/bitrix/crm.button.webform/templates/.default/style.css',
			),
		);

		$result = array();
		$resourcesLength = count($resources);
		for ($i = 0; $i < $resourcesLength; $i++)
		{
			if($fileName && $resources[$i]['name'] != $fileName)
			{
				continue;
			}

			$resources[$i]['content'] = null;
			$resources[$i]['provider_function'] = '\\Bitrix\\Crm\\SiteButton\\Script::getResourceFiles';
			$resources[$i]['provider_params'] = $resources[$i]['name'];
			$resources[$i]['provider_module_id'] = 'crm';

			$result[] = $resources[$i];
		}

		return $result;
	}
}
