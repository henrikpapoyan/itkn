<?php
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage intranet
 * @copyright 2001-2016 Bitrix
 */

namespace Bitrix\Crm\SiteButton;

use Bitrix\Main\Loader;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

/**
 * Class Manager
 * @package Bitrix\Crm\SiteButton
 */
class Manager
{
	const ENUM_TYPE_OPEN_LINE = 'openline';
	const ENUM_TYPE_CRM_FORM = 'crmform';
	const ENUM_TYPE_CALLBACK = 'callback';

	public static function canUseOpenLine()
	{
		return (bool) Loader::includeModule('imopenlines');
	}

	public static function canUseCrmForm()
	{
		return (bool) Loader::includeModule('crm');
	}

	public static function canUseCallback()
	{
		return (bool) Loader::includeModule('voximplant') && (bool) Loader::includeModule('crm');
	}

	public static function getTypeList()
	{
		$result = array();
		$widgetList = self::getWidgetList();
		foreach ($widgetList as $widget)
		{
			$result[$widget['TYPE']] = $widget['NAME'];
		}

		return $result;
	}

	public static function getWidgetList()
	{
		static $list = null;

		if ($list !== null)
		{
			return $list;
		}

		$list = array();

		if(self::canUseOpenLine())
		{
			$type = self::ENUM_TYPE_OPEN_LINE;
			$list[] = array(
				'TYPE' => $type,
				'NAME' => Loc::getMessage('CRM_BUTTON_MANAGER_TYPE_NAME_' . strtoupper($type)),
				'PATH_LIST' => \Bitrix\ImOpenLines\Common::getPublicFolder()."edit.php?ID=0",
				'PATH_ADD' => \Bitrix\ImOpenLines\Common::getPublicFolder()."edit.php?ID=0",
				'PATH_EDIT' => array(
					'path' => \Bitrix\ImOpenLines\Common::getPublicFolder()."edit.php?ID=#ID#",
					'id' => '#ID#'
				),
				'LIST' => \Bitrix\Imopenlines\Model\LivechatTable::getList(array(
					'select' => array('ID' => 'CONFIG_ID', 'NAME' => 'CONFIG.LINE_NAME'),
					'filter' => array(
						'=CONFIG.ACTIVE' => 'Y'
					),
				))->fetchAll(),
				'FUNCTION_GET_PRESETS' => function ()
				{
					return \Bitrix\Imopenlines\Model\LivechatTable::getList(array(
						'select' => array('ID' => 'CONFIG_ID', 'NAME' => 'CONFIG.LINE_NAME'),
						'filter' => array(
							'=CONFIG.ACTIVE' => 'Y'
						),
					))->fetchAll();
				},
				'FUNCTION_GET_CONNECTORS' => function ($lineId)
				{
					return \Bitrix\ImConnector\Connector::getListConnectedConnector($lineId);
				},
				'FUNCTION_GET_WIDGET' => function ($externalId)
				{
					$liveChatManager = new \Bitrix\ImOpenLines\LiveChatManager($externalId);
					return $liveChatManager->getWidget(\Bitrix\ImOpenLines\LiveChatManager::TYPE_BUTTON);
					//return '<div style="width: 200px; height: 100px; background: red; font-size: 20px;">OPEN LINE</div>';
				},
				'JS_SHOW_WIDGET' => function($externalId)
				{
					return 'window.BX.LiveChat.openLiveChat();';
				},
				'JS_HIDE_WIDGET' => function($externalId)
				{
					return 'window.BX.LiveChat.closeLiveChat();';
				}
			);
		}

		if(self::canUseCrmForm())
		{
			$type = self::ENUM_TYPE_CRM_FORM;
			$list[] = array(
				'TYPE' => $type,
				'NAME' => Loc::getMessage('CRM_BUTTON_MANAGER_TYPE_NAME_' . strtoupper($type)),
				'PATH_LIST' => Option::get('crm', 'path_to_webform_list', ''),
				'PATH_ADD' => str_replace('#form_id#', '0', Option::get('crm', 'path_to_webform_edit', '')),
				'PATH_EDIT' => array(
					'path' => Option::get('crm', 'path_to_webform_edit', ''),
					'id' => '#form_id#'
				),
				'LIST' => \Bitrix\Crm\WebForm\Internals\FormTable::getList(array(
					'select' => array('ID', 'NAME'),
					'filter' => array(
						'=ACTIVE' => 'Y',
						'=IS_CALLBACK_FORM' => 'N'
					),
				))->fetchAll(),
				'FUNCTION_GET_PRESETS' => function ()
				{
					return \Bitrix\Crm\WebForm\Internals\FormTable::getList(array(
						'select' => array('ID', 'NAME'),
						'filter' => array(
							'=ACTIVE' => 'Y',
							'=IS_CALLBACK_FORM' => 'N',
							'=IS_SYSTEM' => 'Y',
							'=XML_ID' => 'crm_preset_fb',
						),
					))->fetchAll();
				},
				'FUNCTION_GET_WIDGET' => function ($externalId)
				{
					return \Bitrix\Crm\WebForm\Script::getCrmButtonWidget($externalId, array('IS_CALLBACK' => false));
				},
				'JS_SHOW_WIDGET' => function($externalId)
				{
					return \Bitrix\Crm\WebForm\Script::getCrmButtonWidgetShower($externalId);
				},
				'JS_HIDE_WIDGET' => function($externalId)
				{
					return 'BX.SiteButton.removeClass(document.getElementById(\'bx24_form_container_' . $externalId . '\'), \'open-sidebar\'); BX.SiteButton.onWidgetClose();';
				}
			);
		}

		if(self::canUseCallback())
		{
			$type = self::ENUM_TYPE_CALLBACK;
			$list[] = array(
				'TYPE' => $type,
				'NAME' => Loc::getMessage('CRM_BUTTON_MANAGER_TYPE_NAME_' . strtoupper($type)),
				'PATH_LIST' => Option::get('crm', 'path_to_webform_list', ''),
				'PATH_ADD' => str_replace('#form_id#', '0', Option::get('crm', 'path_to_webform_edit', '')),
				'PATH_EDIT' => array(
					'path' => Option::get('crm', 'path_to_webform_edit', ''),
					'id' => '#form_id#'
				),
				'LIST' => \Bitrix\Crm\WebForm\Internals\FormTable::getList(array(
					'select' => array('ID', 'NAME', 'CALL_FROM'),
					'filter' => array(
						'=ACTIVE' => 'Y',
						'=IS_CALLBACK_FORM' => 'Y'
					),
				))->fetchAll(),
				'FUNCTION_GET_PRESETS' => function ()
				{
					return \Bitrix\Crm\WebForm\Internals\FormTable::getList(array(
						'select' => array('ID', 'NAME'),
						'filter' => array(
							'=ACTIVE' => 'Y',
							'=IS_CALLBACK_FORM' => 'Y',
							'=IS_SYSTEM' => 'Y'
						),
					))->fetchAll();
				},
				'FUNCTION_GET_WIDGET' => function ($externalId)
				{
					return \Bitrix\Crm\WebForm\Script::getCrmButtonWidget($externalId, array('IS_CALLBACK' => true));
				},
				'JS_SHOW_WIDGET' => function($externalId)
				{
					return \Bitrix\Crm\WebForm\Script::getCrmButtonWidgetShower($externalId);
				},
				'JS_HIDE_WIDGET' => function($externalId)
				{
					return 'BX.SiteButton.removeClass(document.getElementById(\'bx24_form_container_' . $externalId . '\'), \'open-sidebar\'); BX.SiteButton.onWidgetClose();';
				}
			);
		}

		return $list;
	}

	public static function updateScriptCache($fromButtonId = null)
	{
		$filter = array();
		if ($fromButtonId)
		{
			$filter['>=ID'] = $fromButtonId;
		}
		$buttonDb = Internals\ButtonTable::getList(array(
			'filter' => $filter,
			'order' => array('ID' => 'ASC')
		));
		while($buttonData = $buttonDb->fetch())
		{
			$button = new Button();
			$button->loadByData($buttonData);
			if (!Script::saveCache($button))
			{
				return $buttonData['ID'];
			}
		}

		return null;
	}

	public static function updateScriptCacheAgent($fromButtonId = null)
	{
		/*@var $USER CUser*/
		global $USER;
		if (!is_object($USER))
		{
			$USER = new \CUser();
		}

		$resultButtonId = self::updateScriptCache($fromButtonId);
		if ($resultButtonId)
		{
			return '\\Bitrix\\Crm\\SiteButton\\Manager::updateScriptCacheAgent(' . $resultButtonId . ');';
		}
		else
		{
			return '';
		}
	}

	public static function getList($params = array('order' => array('ID' => 'DESC')))
	{
		$result = array();
		$typeList = self::getTypeList();
		$locationList = Internals\ButtonTable::getLocationList();
		$buttonDb = Internals\ButtonTable::getList($params);
		while($buttonData = $buttonDb->fetch())
		{
			$button = new Button();
			$button->loadByData($buttonData);

			$buttonData['IS_PAGES_USED'] = false;
			$items = array();
			foreach ($typeList as $typeId => $typeName)
			{
				if ($button->hasActiveItem($typeId))
				{
					$item = $button->getItemByType($typeId);
					$items[$typeId] = array(
						'ID' => $item['EXTERNAL_ID'],
						'NAME' => $item['EXTERNAL_NAME'],
						'TYPE_NAME' => $typeName,
					);
				}

				$buttonData['IS_PAGES_USED'] = $buttonData['IS_PAGES_USED'] || $button->hasItemPages($typeId);
			}
			$buttonData['ITEMS'] = $items;

			if ($buttonData['IS_PAGES_USED'])
			{
				$buttonData['PAGES_USE_DISPLAY'] = Loc::getMessage('CRM_BUTTON_MANAGER_PAGES_USE_DISPLAY_USER');
			}
			else
			{
				$buttonData['PAGES_USE_DISPLAY'] = Loc::getMessage('CRM_BUTTON_MANAGER_PAGES_USE_DISPLAY_ALL');
			}


			$buttonData['LOCATION_DISPLAY'] = $locationList[$buttonData['LOCATION']];
			$buttonData['SCRIPT'] = Script::getScript($button);
			$result[] = $buttonData;
		}

		return $result;
	}

	/**
	 * Is button in use
	 * @return bool
	 */
	public static function isInUse()
	{
		$resultDb = Internals\ButtonTable::getList(array(
			'select' => array('ID'),
			'limit' => 1
		));
		if ($resultDb->fetch())
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	/**
	 * Is button with callback in use
	 * @return bool
	 */
	public static function isCallbackInUse()
	{
		return \Bitrix\Crm\WebForm\Manager::isInUse('Y');
	}

	/**
	 * Check read permissions
	 * @param null|\CCrmAuthorizationHelper $userPermissions User permissions.
	 * @return bool
	 */
	public static function checkReadPermission($userPermissions = null)
	{
		return \CCrmAuthorizationHelper::CheckReadPermission('BUTTON', 0, $userPermissions);
	}

	/**
	 * Get path to button list page
	 * @return string
	 */
	public static function getUrl()
	{
		return Option::get('crm', 'path_to_button_list', '/crm/button/');
	}
}
