<?php
    require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

    CModule::IncludeModule("learning");
    CModule::IncludeModule("tasks");
    CModule::IncludeModule("intranet");
    CModule::IncludeModule("socialnetwork");

    if ( ! empty($_REQUEST["action"]) && $_REQUEST["action"] === "clearCurrentUsers") {
        clearCurrentStudents();

        return;
    }
    $deadLineDate = date("d.m.Y H:i:s", date("U") + (int)$_REQUEST["courseDuration"] * 24 * 60 * 60);
    $remindDate   = date("d.m.Y H:i:s", date("U") + ((int)$_REQUEST["courseDuration"] - 1) * 24 * 60 * 60);

    $ids = array(); //Массив для сбора id пользователей

    //Выбрать пользователей групп:

    getUsersByGroupDataID($_REQUEST["students"]["group"], $ids);

    //Добавить пользователей (отдельных):

    addUserIDsfromRequest($_REQUEST["students"]["user"], $ids);

    //Добавить пользователей внутренней сети:

    getIntranetUsers($_REQUEST["students"]["intranet"], $ids);

    //Добавить пользователей соцсети:

    getSocnetUsers($_REQUEST["students"]["socnetgroup"], $ids);

    //Обновление полей курса:

    $arCourse = startCourse($deadLineDate, $ids);
    $courseID = $arCourse["COURSE_ID"];

    //Создание задач:

    createTasks($courseID, $ids, $deadLineDate, $remindDate);

    $res    = CTest::GetList(array(), array("COURSE_ID" => $arCourse['ID']));
    $arTest = $res->Fetch();

    $arUserResults = getStatistic($arTest['ID'], $arCourse["ACTIVE_FROM"], $ids);

    $rq = array(
        "status"    => "Starting course sprint",
        "statistic" => array(
            "all"     => count($ids),
            "success" => count($arUserResults['success']),
            "failed"  => count($arUserResults['failed']),
            "wait"    => count($arUserResults['wait'])
        )
    );


    echo json_encode($rq);


    function getUsersByGroupDataID($group, &$ids)
    {
        if (empty($group)) {
            return;
        }
        $groupIDs = array();
        foreach ($group as $code) {
            $groupIDs[] = str_replace("G", "", $code);
        }
        if ( ! empty($groupIDs)) {
            $filter  = Array("GROUPS_ID" => $groupIDs);
            $rsUsers = CUser::GetList(($by = "ID"), ($order = "asc"), $filter);
            while ($arUser = $rsUsers->Fetch()) {
                $ids[$arUser["ID"]] = $arUser["ID"];
            }
        }
    }

    function addUserIDsfromRequest($rqUsers, &$ids)
    {
        if (empty($rqUsers)) {
            return;
        }

        foreach ($rqUsers as $code) {
            $userID       = str_replace("U", "", $code);
            $ids[$userID] = $userID;
        }

    }

    function getIntranetUsers($rqIntranetUsers, &$ids)
    {
        if (empty($rqIntranetUsers)) {
            return;
        }
        $provList = array("D" => array(), "DR" => array());
        foreach ($rqIntranetUsers as $depProvider) {
            $prov = substr($depProvider, 0, 2);
            if ($prov === "UI") {
                $userID       = str_replace("UI", "", $depProvider);
                $ids[$userID] = $userID;
            } else {
                if ($prov === "DR") {
                    $provList["DR"][] = str_replace("DR", "", $depProvider);
                } else {
                    $provList["D"][] = str_replace("D", "", $depProvider);
                }
            }
        }

        $structure = CIntranetUtils::GetStructure();

        foreach ($provList["D"] as $department) {
            foreach ($structure["DATA"][$department]["EMPLOYEES"] as $EMPLOYEE) {
                $ids[$EMPLOYEE] = $EMPLOYEE;
            }
        }

        foreach ($provList["DR"] as $department) {
            $dbEmp = CIntranetUtils::getDepartmentEmployees(array($department), true);
            while ($arEmp = $dbEmp->Fetch()) {
                $ids[$arEmp["ID"]] = $arEmp["ID"];
            }
        }
    }

    function getSocnetUsers($rqSocnetUsers, &$ids)
    {
        if (empty($rqSocnetUsers)) {
            return;
        }

        foreach ($rqSocnetUsers as $socnetUserDriver) {
            $socnetGroup   = str_replace("SG", "", $socnetUserDriver);
            $arSocnetGroup = explode("_", $socnetGroup);

            $dbNetUsers = CSocNetUserToGroup::GetList(
                array("ROLE" => "ASC"),
                array("=GROUP_ID" => $arSocnetGroup[0]),
                false,
                array(),
                array(
                    "USER_ID",
                    "ROLE"
                )
            );
            $netUsers   = array();
            while ($arNetUser = $dbNetUsers->GetNext()) {
                $netUsers[$socnetUserDriver][$arNetUser["ROLE"]][] = $arNetUser["USER_ID"];
            }

            foreach ($netUsers[$socnetUserDriver]["A"] as $moderator) {
                $ids[$moderator] = $moderator;
            }
            foreach ($netUsers[$socnetUserDriver]["E"] as $moderator) {
                $ids[$moderator] = $moderator;
            }
            if ($arSocnetGroup[1] == "K") {
                foreach ($netUsers[$socnetUserDriver]["K"] as $ex) {
                    $ids[$ex] = $ex;
                }
            }
        }
    }

    function startCourse($deadLineDate, &$ids)
    {
        $res      = CCourse::GetList(
            Array("SORT" => "ASC"),
            Array("CODE" => "COURSE_IB"));
        $arCourse = $res->Fetch();

        $lessonID = $arCourse["LESSON_ID"];

        global $USER;
        //Обновление списка ревьюверов:
        if (empty($_REQUEST["reviewers"])) {
            $_REQUEST["reviewers"][] = $USER->GetID();
        }

        foreach ($_REQUEST["reviewers"] as $key => $value) {
            $_REQUEST["reviewers"][$key] = str_replace("U", "", $value);
        }

        $GLOBALS["USER_FIELD_MANAGER"]->Update("LEARNING_LESSONS", $lessonID,
            array("UF_DEF_REPORT_VIEWER" => $_REQUEST["reviewers"]));

        //Обновление списка обучающихся на текущем этапе:
        $GLOBALS["USER_FIELD_MANAGER"]->Update("LEARNING_LESSONS", $lessonID,
            array("UF_CURRENT_STUDENTS" => array_keys($ids)));

        //Обновление даты окончания курса:
        $arFields = Array(
            "ACTIVE_FROM" => date("d.m.Y H:i:s"),
            "ACTIVE_TO"   => $deadLineDate,
            "ACTIVE"      => "Y",
        );
        $lesson   = new CLearnLesson;
        $lesson->Update($lessonID, $arFields);

        return $arCourse;
    }

    function createTasks($courseID, &$ids, $deadLineDate, $remindDate)
    {
        $linkCourse = '<a href="/services/learning/course.php?COURSE_ID=' . $courseID . '">Пройти курс</a>';

        foreach ($ids as $responsibleID) {
            $task                   = new \Bitrix\Tasks\Item\Task(0, $responsibleID);
            $task["TITLE"]          = 'Пройти курс обучения по информационной безопасности.';
            $task["DESCRIPTION"]    = "Ссылка на курс: $linkCourse";
            $task["RESPONSIBLE_ID"] = $responsibleID;
            $task["CREATED_BY"]     = $responsibleID;
            $task["DEADLINE"]       = $deadLineDate;

            $taskID = $task->save()->getInstance()->getId();

            $arFields = Array(
                "TASK_ID"     => $taskID,
                "USER_ID"     => $responsibleID,
                "REMIND_DATE" => $remindDate,
                "TYPE"        => CTaskReminders::REMINDER_TYPE_DEADLINE,
                "TRANSPORT"   => CTaskReminders::REMINDER_TRANSPORT_EMAIL
            );

            $obTaskReminders = new CTaskReminders;
            $obTaskReminders->Add($arFields);
        }
    }

    function clearCurrentStudents()
    {
        $res      = CCourse::GetList(
            Array("SORT" => "ASC"),
            Array("CODE" => "COURSE_IB"));
        $arCourse = $res->Fetch();

        $lessonID = $arCourse["LESSON_ID"];

        //Очистка списка обучающихся на текущем этапе:
        $GLOBALS["USER_FIELD_MANAGER"]->Update("LEARNING_LESSONS", $lessonID,
            array("UF_CURRENT_STUDENTS" => array("")));

        $GLOBALS["USER_FIELD_MANAGER"]->Update("LEARNING_LESSONS", $lessonID,
            array("UF_URL_DETAIL_REPORT" => ""));

        echo json_encode("Current users cleared");
    }

    function getStatistic($TEST_ID, $activeFromDate, $students)
    {
        $arUserResults = array("success" => array(), "failed" => array(), "wait" => array(), "attempts" => array());

        $res = CTestAttempt::GetList(
            Array("SCORE" => "DESC"),
            Array(
                "CHECK_PERMISSIONS" => "N",
                "TEST_ID"           => $TEST_ID,
                "STUDENT_ID"        => $students,
                ">DATE_END"         => $activeFromDate
            )
        );

        while ($arAttempt = $res->GetNext()) {
            $arUserResults["attempts"][$arAttempt["USER_ID"]]++;
            if ($arAttempt["COMPLETED"] == 'Y') {
                $arUserResults["success"][$arAttempt['USER_ID']] = $arAttempt;
            } elseif (empty($arUserResults["success"][$arAttempt["USER_ID"]])) {
                $arUserResults["failed"][$arAttempt['USER_ID']] = $arAttempt;
            }
        }

        foreach ($students as $student) {
            if (empty($arUserResults['success'][$student]) && empty($arUserResults['failed'][$student])) {
                $arWaitUser                      = CUser::GetByID($student)->Fetch();
                $arWaitUser["USER_NAME"]         = "(" . $arWaitUser["LOGIN"] . ") " . $arWaitUser["NAME"] . " " .
                                                   $arWaitUser["LAST_NAME"];
                $arUserResults["wait"][$student] = $arWaitUser;
            }
        }

        return $arUserResults;
    }
