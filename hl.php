<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
//require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
require($_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/Classes/postfix/AgentMessagePostfix.class.php");

\Bitrix\Main\Loader::includeModule('highloadblock');

@set_time_limit(0);
@ignore_user_abort(true);
//авторизация файла по ID пользователя
//global $USER;
//$USER->Authorize(372);

$logger = Logger::getLogger("AgentMessagePostfix", "AgentMessagePostfix/result.log");
$logger->log('Run script AgentMessagePostfix');

$AM = new AgentMessagePostfix();

//Получаем письмо из HigloadBlock
use Bitrix\Highloadblock as HL;

$HL_Infoblock_ID = 3; //ID предварительно созданного HL ИБ

$hlblock = HL\HighloadBlockTable::getById($HL_Infoblock_ID)->fetch();
$entity = HL\HighloadBlockTable::compileEntity($hlblock);
$entity_data_class = $entity->getDataClass();

$rsData = $entity_data_class::getList(array(
    'select' => array('*'),
    'filter' => array(
        'UF_PROCESSED'  => 'false', //Берем неотработанные письма
        'UF_EVENT_TYPE'  => 'TopUp_personal_accaunt'
    )
));

$count = 0;
while ($el = $rsData->fetch()) {
    //break;
    //print_r("Event Type: " . $el['UF_EVENT_TYPE'] . "\n");
    //print_r("ID Element: " . $el['ID'] . "\n");

    $msg = $el['UF_TEXTMAIL'];
    if ($AM->parseOne($msg, $el['ID'], $entity_data_class) == false) {
        continue;
    }

    $data = array();
    $data['UF_PROCESSED'] = "0";
    $data['UF_IN_WORK'] = "0";

    //Работа завершена с ошибками
    if (!empty($AM->error)) {
        $data['UF_ERROR'] = "1";
        $data['UF_ERROR_TEXT'] = $AM->error;
    }

    //Работа завершена c успешно
    if (!empty($AM->status)) {
        $data['UF_RESULT'] = $AM->status;
    }

    $result = $entity_data_class::update($el['ID'], $data);
    //pre($result);
    //Очистить статус
    $AM->status = "";
    $AM->error = "";

    $count++;
    if (substr_count($msg, 'TopUp_personal_accaunt')){
        if($count >= 5){
            break;
        }
    }else{
        if($count >= 20){
            break;
        }
    }

}
$companyID;
//$USER->Logout(); //делогируем пользователя
$logger->log('End script AgentMessagePostfix');
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>