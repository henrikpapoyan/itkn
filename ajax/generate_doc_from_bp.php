<?php
/**
 * Created by PhpStorm.
 * User: Рустам
 * Date: 01.04.2019
 * Time: 18:49
 */
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
global $USER;
if (!CModule::IncludeModule("iblock"))
    return false;

if( isset($_GET["bp_id"]) && isset($_GET["elem_id"]) && isset($_GET["tmpl_id"]) ) {
    $bpID = intval($_GET["bp_id"]);
    $elemID = intval($_GET["elem_id"]);
    $tmplID = intval($_GET["tmpl_id"]);
    if( $bpID > 0 && $elemID > 0 && $tmplID > 0 )
    {
        //Получаем данные по заявке
        $el = new CIBlockElement();
        $rsElem = $el->GetList(
            array("ID" => "DESC"),
            array("IBLOCK_ID" => GetIBlockIDByCode("request_fns", "bitrix_processes"), "ID" => $elemID),
            false,
            false,
            array("ID", "NAME", "CREATED_BY", "CREATED_USER_NAME", "PROPERTY_DATA", "PROPERTY_PODRAZDELENIE_FNS", "PROPERTY_MAILING_ADDRESS", "PROPERTY_NOMER", "PROPERTY_INN_NALOGOPLATELSHCHIKA", "PROPERTY_NAIMENOVANIE_NALOGOPLATELSHCHIKA")
        );
        if( $arElem = $rsElem->GetNext() ) {
            //pre($arElem);
            $arUser = $USER->GetByID($arElem["CREATED_BY"])->Fetch();
            $podpis = $arUser["LAST_NAME"]." ".substr($arUser["NAME"], 0, 1).".";
            $tableText = "<table border=\"1\" cellspacing=\"0\" cellpadding=\"5\" style=\"width:100%;\">
                <thead>
                    <tr>
                        <th style=\"text-align: center; vertical-align: middle; width:20%;\">Номер</th>
                        <th style=\"text-align: center; vertical-align: middle; width:20%;\">Дата</th>
                        <th style=\"text-align: center; vertical-align: middle; width:22%;\">ИНН налогоплательщика</th>
                        <th style=\"text-align: center; vertical-align: middle; width:38%;\">Наименование налогоплательщика</th>
                    </tr>
                 </thead>
                 <tbody>";
            foreach($arElem["PROPERTY_INN_NALOGOPLATELSHCHIKA_VALUE"] as $key => $inn) {
                $nameNalogoplotelchika = $arElem["PROPERTY_NAIMENOVANIE_NALOGOPLATELSHCHIKA_VALUE"][$key];
                $tableText .= "<tr>
                    <td style=\"text-align: center; width:20%;\">$arElem[PROPERTY_NOMER_VALUE]</td>
                    <td style=\"text-align: center; width:20%;\">$arElem[PROPERTY_DATA_VALUE]</td>
                    <td style=\"text-align: center; width:22%;\">$inn</td>
                    <td style=\"text-align: center; width:38%;\">$nameNalogoplotelchika</td>
                </tr>";
            }
            $tableText .= "<tbody></table>";
            //echo $tableText;
            $arProperties = array(
                "taxAddress" => "<p align=\"right\">$arElem[PROPERTY_PODRAZDELENIE_FNS_VALUE]</p><p align=\"right\">$arElem[PROPERTY_MAILING_ADDRESS_VALUE]</p>",
                "infoTable" => $tableText,
                "authReq" => $podpis,
                "currentDate" => date("d.m.Y")
            );

            $CDocDes  = new CDocDesignerProcessing();
            $arResult = $CDocDes->PrepareTable(array($bpID, intval($tmplID)), $arProperties);

            $arResult['fname']    = "б/н";
            $arResult['fontSize'] = 9;
            $arResult['mime']     = 'PDF';
            $arResult['params']['tm'] = 1;

            //pre($arResult);

            $DocId   = date("Ymd_His") . rand(1, 1000); //Название файла
            $filePDF = $CDocDes->CreateFile($arResult, $bpID, $DocId);
            //ОБЯЗАТЕЛЬНО ЗАМЕНИТЬ НА __DIR__ ПЕРЕД ВЫНОСОМ НА БОЙ
            $file_path = $_SERVER["DOCUMENT_ROOT"].$filePDF; //__DIR__
            if (file_exists($file_path)) {
                // отдаём файл на скачивание
                ob_clean();
                header("Cache-Control: public");
                header("Pragma: public");
                header("Expires: 0");
                header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
                header("Cache-Control: public");
                header("Content-Description: File Transfer");
                header("Content-type: application/pdf");
                header('Content-Disposition: attachment; filename="' . $DocId . '.pdf"');
                readfile($file_path);
                // удаляем zip файл если он существует
                unlink($file_path);
            } else {
                echo "По пути [$file_path] файл не найден";
            }
        } else {
            echo "Не найден элемент с ID[$elemID]<br />";
        }
    } else {
        echo "Не переданы все параметры необходимые для генерации документа.<br />";
    }
}
?>