<?
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

CModule::IncludeModule('iblock');

$ne_soglas_user = "";
$ne_soglas_user_id = '';
global $USER;

$res = CIBlockElement::GetByID($_POST["id"]);
while($ob = $res->GetNextElement()) {
     $arItem = $ob->GetFields();
     $arItem["PROPERTIES"] = $ob->GetProperties();
     $ar_res = $arItem;
}

if ($_POST["soglas"] == "ne_soglas") {
    $ne_soglas_user = $USER->GetFullName();
    $status = "Отклонена" . " " . $ne_soglas_user;
    $soglasovanie = "Отклонена";
    $ne_soglas_user_id = $USER->GetID();
}

if ($ar_res["PROPERTIES"]["SOGLASOVANIE_KALENDARYA"]["VALUE"] == "ne_soglas"){
    $soglas = "ne_soglas";
} else {
    $soglas = $_POST["soglas"];
}

if ($_POST["soglas"] == "soglas" && $ar_res["PROPERTIES"]["SOGLASOVANIE_KALENDARYA"]["VALUE"] != "ne_soglas") {
    $status = "Согласована";
} 

if ($_POST["soglas"] == "soglas" && $ar_res["PROPERTIES"]["SOGLASOVANIE_KALENDARYA"]["VALUE"] == "ne_soglas") {
    $status = $ar_res["PROPERTIES"]["STATUS"]["VALUE"];
    $ne_soglas_user = $ar_res["PROPERTIES"]["KTO_NE_SOGLASOVAL"]["VALUE"];
}
$arPropUpdate = array(
    "KTO_NE_SOGLASOVAL" => $ne_soglas_user,
    "KTO_NE_SOGLASOVAL_ID" => $ne_soglas_user_id,
    "STATUS" => $status,
    "First_ETAP" => $soglasovanie,
    "SOGLASOVANIE_EMELIN" => "",
    "SOGLASOVANIE_ZHUKOV" => "",
    "SOGLASOVANIE_NOVIKOV" => "",
);

$el = new CIBlockElement;
$el->SetPropertyValuesEx($_POST["id"], false, $arPropUpdate);
$result['status'] = 'success';
$result['message'] = 'Элемент '.$_POST["id"].' успешно обновлен';
echo json_encode($result);
?>