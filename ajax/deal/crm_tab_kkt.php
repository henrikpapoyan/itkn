<?php

//Проверка на AJAX запрос
if(isset($_SERVER['HTTP_BX_AJAX']) && strtolower($_SERVER['HTTP_BX_AJAX']) == 'true') {
} else {
    return false;
}

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);

$logger = Logger::getLogger('DealAjax','ajax/deal/kkt.log');

//Проверка наличия ID сделки
if(!isset($_POST['id']) || (int)$_POST['id'] <=0){
    $logger->log("Нет ID контакта.");
    return false;
}




//Подключаем модуль для работа с CRM
if (!CModule::IncludeModule('crm') || !CModule::IncludeModule('iblock'))
{
    $logger->log("Модуль CRM/iBlock не удалось подключить");
    return false;
}

$dealInfo = CCrmDeal::GetByID($_POST['id']);
if(!isset($dealInfo)){
    echo '</br><b>Сделка не найдена.</b>';
    return false;
}

$arResult       = [];
$dealID      = $_POST['id'];


// Получаем ККТ сделки из реестра ККТ
$IBLOCK_ID = getIblockIDByCode('registry_kkt');
$arSelect = Array("ID", "NAME","PROPERTY_*");
$arFilter = Array("IBLOCK_ID" => $IBLOCK_ID, "PROPERTY_DEAL" => $dealID);
$res = CIBlockElement::GetList(Array("ID"=>"DESC"), $arFilter, false, false, $arSelect);
if (intval($res->SelectedRowsCount()) <= 0){
    echo '</br><b>Данная сделка не имеет ККТ.</b>';
    return false;
}
while($ob = $res->GetNextElement()) {

    $arFields                    = $ob->GetFields();
    $arProps                     = $ob->GetProperties();

    $arTable['KKT_ID']           = $arFields['ID'];
    $arTable['KKT_NAME']         = $arFields['NAME'];
    $arTable['DATE_CONNECT']     = $arProps['DATE_CONNECT']['VALUE'];
    $arTable['MODEL_KKT']        = $arProps['MODEL_KKT']['VALUE'];
    $arTable['NUM_KKT']          = $arProps['NUM_KKT']['VALUE'];
    $arTable['RNM_KKT']          = $arProps['RNM_KKT']['VALUE'];
    $arTable['NUM_FN']           = $arProps['NUM_FN']['VALUE'];
    $arTable['ADDR_KKT']         = $arProps['ADDR_KKT']['VALUE'];
    $arTable['TARIF']            = $arProps['TARIF']['VALUE'];
    $arTable['COST_TARIF']       = $arProps['COST_TARIF']['VALUE'];
    $arTable['ACTIVE']           = $arProps['ACTIVE']['VALUE'];
    $arTable['DATE_ACTIVE']      = $arProps['DATE_ACTIVE']['VALUE'];
    $arTable['CODE_AGENT']       = $arProps['CODE_AGENT']['VALUE'];
    $arTable['PROMO_KOD']        = $arProps['PROMO_KOD']['VALUE'];
    $arTable['DATE_OFF']         = $arProps['DATE_OFF']['VALUE'];
    $arTable['PERIOD_OFF']       = $arProps['PERIOD_OFF']['VALUE'];
    $arTable['PERIOD_ACTIVE']    = $arProps['PERIOD_ACTIVE']['VALUE'];
    $arTable['NAIMENOVANIE_KKT'] = $arProps['NAIMENOVANIE_KKT']['VALUE'];

    $arTable['URL']="http://".$_SERVER['SERVER_NAME']."/services/lists/".$IBLOCK_ID."/element/0/".$arFields['ID']."/?list_section_id=";



    $arResult[] = $arTable;
}
?>
<div style="height: 100%;">
    <div class="bx-crm-view-fieldset">
        <div class="bx-crm-view-fieldset-content-tiny">
            <table class="bx-crm-view-fieldset-content-table">
                <tbody>
                <tr>
                    <td class="bx-field-value" colspan="2">
                        <div class="main-grid main-grid-load-animation"  style="display: block;">
                            <div class="main-grid-wrapper">
                                <div class="main-grid-fade">
                                    <div class="main-grid-container">

                                        <table class="main-grid-table" >
                                            <thead class="main-grid-header">
                                            <tr class="main-grid-row-head">
                                                <th class="main-grid-cell-head main-grid-cell-left main-grid-col-no-sortable  main-grid-draggable">
                                                        <span class="main-grid-cell-head-container">
                                                            <span class="main-grid-head-title">ИД</span>
                                                        </span>
                                                </th>
                                                <th class="main-grid-cell-head main-grid-cell-left main-grid-col-no-sortable  main-grid-draggable">
                                                        <span class="main-grid-cell-head-container">
                                                            <span class="main-grid-head-title">Название</span>
                                                        </span>
                                                </th>

                                                <th class="main-grid-cell-head main-grid-cell-left main-grid-col-no-sortable  main-grid-draggable">
                                                        <span class="main-grid-cell-head-container">
                                                            <span class="main-grid-head-title">Дата подключения к ККТ</span>
                                                        </span>
                                                </th>

                                                <th class="main-grid-cell-head main-grid-cell-left main-grid-col-no-sortable  main-grid-draggable">
                                                        <span class="main-grid-cell-head-container">
                                                            <span class="main-grid-head-title">Модель ККТ</span>
                                                        </span>
                                                </th>
                                                <th class="main-grid-cell-head main-grid-cell-left main-grid-col-no-sortable  main-grid-draggable">
                                                        <span class="main-grid-cell-head-container">
                                                            <span class="main-grid-head-title">Зав.№ ККТ</span>
                                                        </span>
                                                </th>
                                                <th class="main-grid-cell-head main-grid-cell-left main-grid-col-no-sortable  main-grid-draggable">
                                                        <span class="main-grid-cell-head-container">
                                                            <span class="main-grid-head-title">РНМ ККТ</span>
                                                        </span>
                                                </th>
                                                <th class="main-grid-cell-head main-grid-cell-left main-grid-col-no-sortable  main-grid-draggable">
                                                        <span class="main-grid-cell-head-container">
                                                            <span class="main-grid-head-title">Зав.№ ФН</span>
                                                        </span>
                                                </th>
                                                <th class="main-grid-cell-head main-grid-cell-left main-grid-col-no-sortable  main-grid-draggable">
                                                        <span class="main-grid-cell-head-container">
                                                            <span class="main-grid-head-title">Адрес установки ККТ</span>
                                                        </span>
                                                </th>
                                                <th class="main-grid-cell-head main-grid-cell-left main-grid-col-no-sortable  main-grid-draggable">
                                                        <span class="main-grid-cell-head-container">
                                                            <span class="main-grid-head-title">Тариф</span>
                                                        </span>
                                                </th>
                                                <th class="main-grid-cell-head main-grid-cell-left main-grid-col-no-sortable  main-grid-draggable">
                                                        <span class="main-grid-cell-head-container">
                                                            <span class="main-grid-head-title">Стоимость тарифа</span>
                                                        </span>
                                                </th>
                                                <th class="main-grid-cell-head main-grid-cell-left main-grid-col-no-sortable  main-grid-draggable">
                                                        <span class="main-grid-cell-head-container">
                                                            <span class="main-grid-head-title">Активирована</span>
                                                        </span>
                                                </th>
                                                <th class="main-grid-cell-head main-grid-cell-left main-grid-col-no-sortable  main-grid-draggable">
                                                        <span class="main-grid-cell-head-container">
                                                            <span class="main-grid-head-title">Дата активации</span>
                                                        </span>
                                                </th>
                                                <th class="main-grid-cell-head main-grid-cell-left main-grid-col-no-sortable  main-grid-draggable">
                                                        <span class="main-grid-cell-head-container">
                                                            <span class="main-grid-head-title">Код агента</span>
                                                        </span>
                                                </th>
                                                <th class="main-grid-cell-head main-grid-cell-left main-grid-col-no-sortable  main-grid-draggable">
                                                        <span class="main-grid-cell-head-container">
                                                            <span class="main-grid-head-title">Промо код</span>
                                                        </span>
                                                </th>
                                                <th class="main-grid-cell-head main-grid-cell-left main-grid-col-no-sortable  main-grid-draggable">
                                                        <span class="main-grid-cell-head-container">
                                                            <span class="main-grid-head-title">Период активации</span>
                                                        </span>
                                                </th>
                                                <th class="main-grid-cell-head main-grid-cell-left main-grid-col-no-sortable  main-grid-draggable">
                                                        <span class="main-grid-cell-head-container">
                                                            <span class="main-grid-head-title">Дата деактивации</span>
                                                        </span>
                                                </th>
                                                <th class="main-grid-cell-head main-grid-cell-left main-grid-col-no-sortable  main-grid-draggable">
                                                        <span class="main-grid-cell-head-container">
                                                            <span class="main-grid-head-title">Период деактивации</span>
                                                        </span>
                                                </th>
                                            </tr>
                                            </thead>

                                            <tbody>
                                            <?foreach ($arResult as $value):?>
                                                <tr class="main-grid-row main-grid-row-body">
                                                    <td class="main-grid-cell main-grid-cell-left">
                                                        <span class="main-grid-cell-content"><?=($value['KKT_ID'] != "")? $value['KKT_ID'] : '-';?></span>
                                                    </td>
                                                    <td class="main-grid-cell main-grid-cell-left">
                                                            <span class="main-grid-cell-content">
                                                                <a href="<?=$value['URL']?>/"><?=$value['KKT_NAME']?></a>
                                                            </span>
                                                    </td>
                                                    <td class="main-grid-cell main-grid-cell-left">
                                                        <span class="main-grid-cell-content"><?=($value['DATE_CONNECT'] != "")? $value['DATE_CONNECT'] : '-';?></span>
                                                    </td>
                                                    <td class="main-grid-cell main-grid-cell-left">
                                                        <span class="main-grid-cell-content"><?=($value['MODEL_KKT'] != "")? $value['MODEL_KKT'] : '-';?></span>
                                                    </td>
                                                    <td class="main-grid-cell main-grid-cell-left">
                                                        <span class="main-grid-cell-content"><?=($value['NUM_KKT'] != "")? $value['NUM_KKT'] : '-';?></span>
                                                    </td>
                                                    <td class="main-grid-cell main-grid-cell-left">
                                                        <span class="main-grid-cell-content"><?=($value['RNM_KKT'] != "")? $value['RNM_KKT'] : '-';?></span>
                                                    </td>
                                                    <td class="main-grid-cell main-grid-cell-left">
                                                        <span class="main-grid-cell-content"><?=($value['NUM_FN'] != "")? $value['NUM_FN'] : '-';?></span>
                                                    </td>
                                                    <td class="main-grid-cell main-grid-cell-left">
                                                        <span class="main-grid-cell-content"><?=($value['ADDR_KKT'] != "")? $value['ADDR_KKT'] : '-';?></span>
                                                    </td>
                                                    <td class="main-grid-cell main-grid-cell-left">
                                                        <span class="main-grid-cell-content"><?=($value['TARIF'] != "")? $value['TARIF'] : '-';?></span>
                                                    </td>
                                                    <td class="main-grid-cell main-grid-cell-left">
                                                        <span class="main-grid-cell-content"><?=($value['COST_TARIF'] != "")? $value['COST_TARIF'] : '-';?></span>
                                                    </td>
                                                    <td class="main-grid-cell main-grid-cell-left">
                                                        <span class="main-grid-cell-content"><?=($value['ACTIVE'] != "")? $value['ACTIVE'] : '-';?></span>
                                                    </td>
                                                    <td class="main-grid-cell main-grid-cell-left">
                                                        <span class="main-grid-cell-content"><?=($value['DATE_ACTIVE'] != "")? $value['DATE_ACTIVE'] : '-';?></span>
                                                    </td>
                                                    <td class="main-grid-cell main-grid-cell-left">
                                                        <span class="main-grid-cell-content"><?=($value['CODE_AGENT'] != "")? $value['CODE_AGENT'] : '-';?></span>
                                                    </td>
                                                    <td class="main-grid-cell main-grid-cell-left">
                                                        <span class="main-grid-cell-content"><?=($value['PROMO_KOD'] != "")? $value['PROMO_KOD'] : '-';?></span>
                                                    </td>
                                                    <td class="main-grid-cell main-grid-cell-left">
                                                        <span class="main-grid-cell-content"><?=($value['PERIOD_ACTIVE'] != "")? $value['PERIOD_ACTIVE'] : '-';?></span>
                                                    </td>
                                                    <td class="main-grid-cell main-grid-cell-left">
                                                        <span class="main-grid-cell-content"><?=($value['DATE_OFF'] != "")? $value['DATE_OFF'] : '-';?></span>
                                                    </td>
                                                    <td class="main-grid-cell main-grid-cell-left">
                                                        <span class="main-grid-cell-content"><?=($value['PERIOD_OFF'] != "")? $value['PERIOD_OFF'] : '-';?></span>
                                                    </td>
                                                </tr>
                                            <?endforeach;?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>