<? 
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

?>
 
<?
echo "<br />";
CModule::IncludeModule('iblock');



$res = CIBlockElement::GetByID($_POST["id"]);

while($ob = $res->GetNextElement())
	{
		 $arItem = $ob->GetFields();
		 $arItem["PROPERTIES"] = $ob->GetProperties();
		 $ar_res = $arItem;
	}



$arUpdate = Array(
  "ACTIVE" => "Y",  
  "DATE_CREATE" => $ar_res["DATE_CREATE"],
  "NAME" => $ar_res["NAME"], 
  "DETAIL_TEXT" => $ar_res["DETAIL_TEXT"], 
  "PROPERTY_VALUES" => array( 
     "ORGANIZATSIYA" => $ar_res["PROPERTIES"]["ORGANIZATSIYA"]["VALUE"],
     "PODRAZDELENIE" => $ar_res["PROPERTIES"]["PODRAZDELENIE"]["VALUE"],
     "KONTRAGENT" => $ar_res["PROPERTIES"]["KONTRAGENT"]["VALUE"],
     "DOGOVOR" => $ar_res["PROPERTIES"]["DOGOVOR"]["VALUE"],
     "DOKUMENT" => $ar_res["PROPERTIES"]["DOKUMENT"]["VALUE"],
     "NOMER_DOKUMENTA" => $ar_res["PROPERTIES"]["NOMER_DOKUMENTA"]["VALUE"],
     "DATA_DOKUMENTA" => $ar_res["PROPERTIES"]["DATA_DOKUMENTA"]["VALUE"],
     "SUMMA_SCHETA" => $ar_res["PROPERTIES"]["SUMMA_SCHETA"]["VALUE"],
     "STATYA_ZATRAT" => $ar_res["PROPERTIES"]["STATYA_ZATRAT"]["VALUE"],
     "VID_RASKHODA" => $ar_res["PROPERTIES"]["VID_RASKHODA"]["VALUE"],
     "SCHET_I_DRUGIE_DOKUMENTY" => $ar_res["PROPERTIES"]["SCHET_I_DRUGIE_DOKUMENTY"]["VALUE"],
     "KOMMENTARIY" => $ar_res["PROPERTIES"]["KOMMENTARIY"]["VALUE"],
     "NAZNACHENIE_PLATEZHA" => $ar_res["PROPERTIES"]["NAZNACHENIE_PLATEZHA"]["VALUE"],
     "SOGLASOVANIE_KALENDARYA" => $ar_res["PROPERTIES"]["SOGLASOVANIE_KALENDARYA"]["VALUE"],
     "KTO_NE_SOGLASOVAL" => $ar_res["PROPERTIES"]["KTO_NE_SOGLASOVAL"]["VALUE"],
     "OPLATA_ZAYAVKI" => $_POST["oplata"],
  ),
);  

$el = new CIBlockElement;
if ($upd = $el->Update($_POST["id"], $arUpdate)) {
    $result['status'] = 'success';
    $result['message'] = 'Элемент успешно добавлена';
} else {
    $result['status'] = 'error';
    $result['message'] = $el->LAST_ERROR;
}

$result = json_encode($result); 
?>


<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>