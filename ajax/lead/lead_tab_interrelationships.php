<?php

//Проверка на AJAX запрос
if(isset($_SERVER['HTTP_BX_AJAX']) && strtolower($_SERVER['HTTP_BX_AJAX']) == 'true') {
} else {
    return false;
}

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);

$logger = Logger::getLogger('CompanyAjax','ajax/company/address.log');
$leadId      = intval($_POST['id']);

//Проверка наличия ID лида
if(!isset($_POST['id']) || $leadId <=0){
    $logger->log("Нет ID лида.");
    return false;
}

//Подключаем модуль для работа с CRM
if (!CModule::IncludeModule('crm') || !CModule::IncludeModule('iblock'))
{
    $logger->log("Модуль CRM/iBlock не удалось подключить");
    return false;
}
$arResult       = [];
$leadInfo = CCrmLead::GetByID($leadId);
//echo '<pre>'.print_r($leadInfo, true).'</pre>';
$el = new CIBlockElement();
$rsIntShips = $el -> GetList(
    array("ID"=>"DESC"),
    array("IBLOCK_ID"=>GetIBlockIDByCode("lead_interrelationships"), "ACTIVE"=>"Y", "PROPERTY_LEAD_LINK"=>$leadId),
    false,
    false,
    array("ID", "NAME", "PROPERTY_LEAD_LINK", "PROPERTY_COMPANY_LINK", "PROPERTY_COMPANY_LINK_TYPE")
);
if($rsIntShips->SelectedRowsCount()>0){

    $dbLinkTypes = $el->GetList(
        array("SORT"=>"ASC"),
        array("IBLOCK_ID"=>GetIBlockIDByCode('company_link_type'), "ACTIVE"=>"Y"),
        false,
        false,
        array("ID", "NAME")
    );
    $arLinkTypes = array();
    while($arLinkType = $dbLinkTypes->GetNext()){
        $arLinkTypes[$arLinkType["ID"]] = $arLinkType["NAME"];
    }
    while($arIntShip = $rsIntShips->GetNext()){
        foreach($arIntShip["PROPERTY_COMPANY_LINK_VALUE"] as $key => $companyID){
            $arCompany = CCrmCompany::GetByID($companyID);
            $arResult[] = array(
                "ELEM_ID" => $arIntShip["ID"],
                "LINK_TYPE" => $arLinkTypes[$arIntShip["PROPERTY_COMPANY_LINK_TYPE_VALUE"][$key]],
                "LINK_TYPE_ID" => $arIntShip["PROPERTY_COMPANY_LINK_TYPE_VALUE"][$key],
                "COMPANY_NAME" => $arCompany["TITLE"],
                "COMPANY_ID" => $companyID,
                'URL'=>"/crm/company/show/$companyID"
            );
        }
    }
}

//echo '<pre>'.print_r($arResult, true).'</pre><hr />';?>
    <div class="crm-list-top-bar" id="lead_interrelationships_grid_editor_toolbar">
        <a class="crm-menu-bar-btn btn-new crm-activity-command-add-interrelationships" href="javascript:void(0);" title="Добавить взаимосвязь">
            <span class="crm-toolbar-btn-icon"></span>
            <span>Взаимосвязь</span>
        </a>
    </div>
<?if(sizeof($arResult)>0):?>
    <div id="interrelationships-list" style="height: 100%;">
        <div class="bx-crm-view-fieldset">
            <div class="bx-crm-view-fieldset-content-tiny">
                <table class="bx-crm-view-fieldset-content-table">
                    <tbody>
                    <tr>
                        <td class="bx-field-value" colspan="2">
                            <div class="main-grid main-grid-load-animation"  style="display: block;">
                                <div class="main-grid-wrapper">
                                    <div class="main-grid-fade">
                                        <div class="main-grid-container">

                                            <table class="main-grid-table" >
                                                <thead class="main-grid-header">
                                                <tr class="main-grid-row-head">
                                                    <th class="main-grid-cell-head main-grid-cell-left main-grid-col-no-sortable  main-grid-draggable">
                                                        <span class="main-grid-cell-head-container">
                                                            <span class="main-grid-head-title">Тип взаимосвязи</span>
                                                        </span>
                                                    </th>
                                                    <th class="main-grid-cell-head main-grid-cell-left main-grid-col-no-sortable  main-grid-draggable">
                                                        <span class="main-grid-cell-head-container">
                                                            <span class="main-grid-head-title">Компания</span>
                                                        </span>
                                                    </th>
                                                    <th class="main-grid-cell-head main-grid-cell-left main-grid-col-no-sortable  main-grid-draggable">
                                                        <span class="main-grid-cell-head-container">
                                                            <span class="main-grid-head-title"></span>
                                                        </span>
                                                    </th>
                                                </tr>
                                                </thead>

                                                <tbody>
                                                <?foreach ($arResult as $value):?>
                                                    <tr class="main-grid-row main-grid-row-body" id="line_<?=$value["COMPANY_ID"]?>">
                                                        <td class="main-grid-cell main-grid-cell-left">
                                                            <span class="main-grid-cell-content">
                                                                <?=($value['LINK_TYPE'] != "")? $value['LINK_TYPE'] : '-';?>
                                                            </span>
                                                        </td>
                                                        <td class="main-grid-cell main-grid-cell-left">
                                                            <span class="main-grid-cell-content">
                                                                <a href="<?=$value['URL']?>/"><?=$value['COMPANY_NAME']?></a>
                                                            </span>
                                                        </td>
                                                        <td class="main-grid-cell main-grid-cell-left nav_elem">
                                                            <span class="main-grid-cell-content" data-elem-id="<?=$value["ELEM_ID"]?>" data-link-type-id="<?=$value["LINK_TYPE_ID"]?>" data-company-id="<?=$value["COMPANY_ID"]?>">
                                                                <span class="crm-lead-header-inner-edit-btn"></span>
                                                                <span class="crm-offer-title-del"></span>
                                                            </span>
                                                        </td>
                                                    </tr>
                                                <?endforeach;?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
<?else:
    echo '</br><b>Для данного лида еще не добавлено ни одной взаимосвязи.</b>';
    return false;
endif;?>