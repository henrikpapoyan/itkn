<?php
//Проверка на AJAX запрос
if(isset($_SERVER['HTTP_BX_AJAX']) && strtolower($_SERVER['HTTP_BX_AJAX']) == 'true') {
} else {
    return false;
}
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
$logger = Logger::getLogger('CompanyAjax','ajax/lead/address.log');
//Подключаем модуль для работа с CRM
if (!CModule::IncludeModule('iblock'))
{
    $logger->log("Модуль iBlock не удалось подключить");
    return false;
}?>

<form name="add-interrelationships-form" id="add-interrelationships-form" action="/ajax/lead/add_interrelationships.php">
    <div class="param-block">
        <div class="param-label">
            Выберите тип взаимосвязи:
        </div>
        <div class="param-enter">
            <select id="interrelationships-type">
                <?$el = new CIBlockElement();
                $rsInterrShips = $el->GetList(
                    array("SORT"=>"ASC"),
                    array("IBLOCK_ID"=>GetIBlockIDByCode('company_link_type'), "ACTIVE"=>"Y"),
                    false,
                    false,
                    array("ID", "NAME")
                );
                while($arInterrShip = $rsInterrShips->GetNext()):?>
                    <option value="<?=$arInterrShip["ID"]?>"><?=$arInterrShip["NAME"]?></option>
                <?endwhile;?>
            </select>
        </div>
    </div>
    <div class="param-block">
        <div class="param-label">
            Выберите компанию:
        </div>
        <div class="param-enter">
            <input type="text" id="company-text" name="company-text" placeholder="Начните вводить название компании"/>

        </div>
    </div>
    <div id="company-list-search">
        <ul id="company-list-search-result"></ul>
    </div>
    <input type="hidden" value="" id="company-id" name="company-id" />
    <div id="add_interrelationships_result">

    </div>
</form>
<script type="text/javascript">
    BX.ready(function(){
        var lastSearchText = '';
        //функция для обхода повторного вызова результатов поиска. Параметр ignor от
        function searchCompany(searchText=''){
            //console.log(searchText+' = '+lastSearchText);
            if(searchText!=lastSearchText){
                var likeText = searchText,
                    innerTab = BX('company-list-search');
                if(likeText.length>2){
                    var waiter = BX.showWait(BX('add-interrelationships-form'));
                    BX.ajax({
                            url: '/ajax/lead/get_company_list.php',
                            method: 'POST',
                            dataType: 'html',
                            data: {
                                likeText: likeText
                            },
                            onsuccess: function(data)
                            {
                                innerTab.innerHTML = data;
                                BX.closeWait(innerTab, waiter);
                                $("#company-list-search").slideDown();
                                lastSearchText = searchText;
                            },
                            onfailure: function(data)
                            {
                                BX.closeWait(innerTab, waiter);
                            }
                        }
                    );
                }
            }
        }

        $("form#add-interrelationships-form").on('change paste keyup', "input#company-text", function(){
            var likeText = $(this).val();
            searchCompany(likeText);
        });

        $("#company-list-search").on('click', "li", function(){
            companyID = $(this).data('company-id');
            //console.log('companyID = ',companyID);
            $("input#company-id").val(companyID);
            $("input#company-text").val($(this).text());
            $("#company-list-search").slideUp();
        });


        $("div#add-interrelationships-block").on('submit', "form#add-interrelationships-form", function(){
            console.log('submit form');
            var linkType=$("div#add-interrelationships-block select#interrelationships-type").val(),
                companyID=$("div#add-interrelationships-block input#company-id").val(),
                innerTab = BX('add_interrelationships_result');
                waiter = BX.showWait(BX('add-interrelationships-form'));

            if (matches = window.location.href.match(/\/crm\/lead\/show\/([\d]+)\//i)) {
                var leadId = parseInt(matches[1]);
            }
            if(linkType>0&&companyID>0&&leadId>0){
                BX.ajax({
                        url: '/ajax/lead/add_new_interrelationships.php',
                        method: 'POST',
                        dataType: 'html',
                        data: {
                            leadId: leadId,
                            linkType: linkType,
                            companyID: companyID
                        },
                        onsuccess: function(data)
                        {
                            innerTab.innerHTML = data;
                            BX.closeWait(innerTab, waiter);
                        },
                        onfailure: function(data)
                        {
                            BX.closeWait(innerTab, waiter);
                        }
                    }
                );
            }
            return false;
        });
    });
</script>