<?php
//Проверка на AJAX запрос
if(isset($_SERVER['HTTP_BX_AJAX']) && strtolower($_SERVER['HTTP_BX_AJAX']) == 'true') {
} else {
    return false;
}
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
$logger = Logger::getLogger('interrelationshipsAjax','ajax/lead/interrelationships.log');
//Подключаем модуль для работа с CRM
if (!CModule::IncludeModule('iblock'))
{
    $logger->log("Модуль iBlock не удалось подключить");
    return false;
}
if(!isset($_REQUEST["action"])){
    $logger->log("Не передано значение action");
    return false;
}

if($_REQUEST["action"]=="add"):?>
    <form name="add-interrelationships-form" id="add-interrelationships-form" class="interrelationships-form" action="/ajax/lead/add_interrelationships.php">
        <div class="param-block">
            <div class="param-label">
                Выберите тип взаимосвязи:
            </div>
            <div class="param-enter">
                <select id="interrelationships-type">
                    <?$el = new CIBlockElement();
                    $rsInterrShips = $el->GetList(
                        array("SORT"=>"ASC"),
                        array("IBLOCK_ID"=>GetIBlockIDByCode('company_link_type'), "ACTIVE"=>"Y"),
                        false,
                        false,
                        array("ID", "NAME")
                    );
                    while($arInterrShip = $rsInterrShips->GetNext()):?>
                        <option value="<?=$arInterrShip["ID"]?>"><?=$arInterrShip["NAME"]?></option>
                    <?endwhile;?>
                </select>
            </div>
        </div>
        <div class="param-block">
            <div class="param-label">
                Выберите компанию:
            </div>
            <div class="param-enter">
                <input type="text" id="company-text" name="company-text" placeholder="Начните вводить название компании"/>
            </div>
        </div>
        <div id="company-list-search">
            <ul id="company-list-search-result"></ul>
        </div>
        <input type="hidden" value="" id="company-id" name="company-id" />
        <input type="hidden" value="add" id="action" name="action" />
        <div id="add_interrelationships_result">

        </div>
    </form>
<?endif;

if($_REQUEST["action"]=="edit"):
    if(!isset($_REQUEST["link_id"])||!isset($_REQUEST["company_id"])){
        $logger->log("Не передано значение link_id/company_id");
        return false;
    }
    if (!CModule::IncludeModule('crm'))
    {
        $logger->log("Модуль CRM не удалось подключить");
        return false;
    }
    $linkID = intval($_REQUEST["link_id"]);
    $companyID = intval($_REQUEST["company_id"]);
    $arCompany = CCrmCompany::GetByID($companyID);
    ?>
    <form name="edit-interrelationships-form" id="edit-interrelationships-form" class="interrelationships-form" action="/ajax/lead/add_interrelationships.php">
        <div class="param-block">
            <div class="param-label">
                <b>Компания:</b> <?=$arCompany["TITLE"]?>
            </div>
            <div class="param-enter">
                <input type="hidden" id="company-text" name="company-text" placeholder="Начните вводить название компании" value='<?=$arCompany["TITLE"]?>' readonly/>
            </div>
        </div>
        <div class="param-block">
            <div class="param-label">
                Выберите тип взаимосвязи:
            </div>
            <div class="param-enter">
                <select id="interrelationships-type">
                    <?$el = new CIBlockElement();
                    $rsInterrShips = $el->GetList(
                        array("SORT"=>"ASC"),
                        array("IBLOCK_ID"=>GetIBlockIDByCode('company_link_type'), "ACTIVE"=>"Y"),
                        false,
                        false,
                        array("ID", "NAME")
                    );
                    while($arInterrShip = $rsInterrShips->GetNext()):?>
                        <option value="<?=$arInterrShip["ID"]?>" <?=$arInterrShip["ID"]==$linkID?'selected':''?>><?=$arInterrShip["NAME"]?></option>
                    <?endwhile;?>
                </select>
            </div>
        </div>
        <div id="company-list-search">
            <ul id="company-list-search-result"></ul>
        </div>
        <input type="hidden" id="company-id" name="company-id" value="<?=$companyID?>" />
        <input type="hidden" id="old-company-id" name="old-company-id" value="<?=$companyID?>" />
        <input type="hidden" value="edit" id="action" name="action" />
        <div id="add_interrelationships_result"></div>
    </form>
<?endif;?>

<?if($_REQUEST["action"]=="del"):
    if(!isset($_REQUEST["link_id"])||!isset($_REQUEST["company_id"])){
        $logger->log("Не передано значение link_id/company_id");
        return false;
    }
    if (!CModule::IncludeModule('crm'))
    {
        $logger->log("Модуль CRM не удалось подключить");
        return false;
    }
    $linkID = intval($_REQUEST["link_id"]);
    $companyID = intval($_REQUEST["company_id"]);
    $arCompany = CCrmCompany::GetByID($companyID);
    ?>
    <form name="del-interrelationships-form" id="del-interrelationships-form" class="interrelationships-form" action="/ajax/lead/add_interrelationships.php">
        <?$el = new CIBlockElement();
        $rsInterrShips = $el->GetList(
            array("SORT"=>"ASC"),
            array("IBLOCK_ID"=>GetIBlockIDByCode('company_link_type'), "ACTIVE"=>"Y"),
            false,
            false,
            array("ID", "NAME")
        );
        while($arInterrShip = $rsInterrShips->GetNext()):
            if($arInterrShip["ID"]==$linkID){
                $linkTypeText = $arInterrShip["NAME"];
            }
        endwhile;?>
        <p><b>Тип взаимосвязи: </b><?=$linkTypeText?></p>
        <p><b>Компания:</b> <?=$arCompany["TITLE"]?></p>
        <input type="hidden" id="company-id" name="company-id" value="<?=$companyID?>" />
        <input type="hidden" value="del" id="action" name="action" />
        <div id="add_interrelationships_result"></div>
    </form>
<?endif;?>
