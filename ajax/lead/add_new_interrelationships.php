<?php
//Проверка на AJAX запрос
if(isset($_SERVER['HTTP_BX_AJAX']) && strtolower($_SERVER['HTTP_BX_AJAX']) == 'true') {
} else {
    return false;
}
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
$logger = Logger::getLogger('CompanyAjax','ajax/lead/address.log');
$arRezultat = array();
//Подключаем модуль для работа с CRM
if (!CModule::IncludeModule('iblock'))
{
    $logger->log("Модуль CRM/iBlock не удалось подключить");
    return false;
}
if(!isset($_REQUEST["action"])){
    $logger->log("Не передано значение action");
    return false;
}
$action = $_REQUEST["action"];
if(!isset($_REQUEST['linkType'])||!isset($_REQUEST['companyID'])||!isset($_REQUEST['leadId'])){
    $logger->log("Нет переменных linkType/companyID.");
    return false;
} else {
    $linkType = $_REQUEST['linkType'];
    $companyID = $_REQUEST['companyID'];
    $leadId = $_REQUEST['leadId'];
}
$el = new CIBlockElement();
if($action=="add") {
    //echo '<pre>'.print_r($_REQUEST, true).'</pre>';
    $dbDublikate = $el->GetList(
        array("ID" => "ASC"),
        array("IBLOCK_ID" => GetIBlockIDByCode("lead_interrelationships"), "ACTIVE" => "Y", "PROPERTY_LEAD_LINK" => $leadId),
        false,
        false,
        array("ID", "IBLOCK_ID", "NAME", "PROPERTY_LEAD_LINK", "PROPERTY_COMPANY_LINK", "PROPERTY_COMPANY_LINK_TYPE")
    );
    if ($dbDublikate->SelectedRowsCount() == 0) {
        $PROP = array(
            "LEAD_LINK" => $leadId,
            "COMPANY_LINK" => $companyID,
            "COMPANY_LINK_TYPE" => $linkType,
        );
        $arLoadProductArray = Array(
            "MODIFIED_BY" => $USER->GetID(), // элемент изменен текущим пользователем
            "IBLOCK_SECTION_ID" => false,          // элемент лежит в корне раздела
            "IBLOCK_ID" => GetIBlockIDByCode("lead_interrelationships"),
            "PROPERTY_VALUES" => $PROP,
            "NAME" => "Взаимосвязь для лида " . $leadId,
            "ACTIVE" => "Y",
        );

        if ($PRODUCT_ID = $el->Add($arLoadProductArray)) {
            $arRezultat["RESULT"] = "success";
            $arRezultat["MESS"] = "Взаимосвязь успешно добавлена.<br />";
            $arRezultat["ELEM_ID"] = $PRODUCT_ID;
            $arRezultat["LINK_TYPE_ID"] = $linkType;
            $arRezultat["COMPANY_ID"] = $companyID;
        } else {
            $arRezultat["RESULT"] = "error";
            $arRezultat["MESS"] = "Error: " . $el->LAST_ERROR;
        }
    } else {
        $arInterrelationships = $dbDublikate->GetNext();
        //    echo '<pre>'.print_r($arInterrelationships, true).'</pre>';
        $arCurCompanys = $arInterrelationships["PROPERTY_COMPANY_LINK_VALUE"];
        $arCurLinkTypes = $arInterrelationships["PROPERTY_COMPANY_LINK_TYPE_VALUE"];
        if (!in_array($companyID, $arCurCompanys)) {
            $arCurCompanys[] = $companyID;
            $arCurLinkTypes[] = $linkType;
            CIBlockElement::SetPropertyValuesEx(
                $arInterrelationships["ID"],
                $arInterrelationships["IBLOCK_ID"],
                array(
                    "COMPANY_LINK" => $arCurCompanys,
                    "COMPANY_LINK_TYPE" => $arCurLinkTypes,
                )
            );
            $arRezultat["RESULT"] = "success";
            $arRezultat["MESS"] = "Взаимосвязь успешно добавлена.<br />";
            $arRezultat["ELEM_ID"] = $arInterrelationships["ID"];
            $arRezultat["LINK_TYPE_ID"] = $linkType;
            $arRezultat["COMPANY_ID"] = $companyID;
        } else {
            $arRezultat["RESULT"] = "error";
            $arRezultat["MESS"] = "Данная компания уже добавлена.<br />";
        }
    }
}
if($action=="edit"){
    if(!isset($_REQUEST['oldCompanyID'])){
        $logger->log("Не указан старый ID компании.");
        return false;
    }
    $oldCompanyID = $_REQUEST['oldCompanyID'];
    $dbDublikate = $el->GetList(
        array("ID" => "ASC"),
        array("IBLOCK_ID" => GetIBlockIDByCode("lead_interrelationships"), "ACTIVE" => "Y", "PROPERTY_LEAD_LINK" => $leadId),
        false,
        false,
        array("ID", "IBLOCK_ID", "NAME", "PROPERTY_LEAD_LINK", "PROPERTY_COMPANY_LINK", "PROPERTY_COMPANY_LINK_TYPE")
    );
    $arInterrelationships = $dbDublikate->GetNext();
    //    echo '<pre>'.print_r($arInterrelationships, true).'</pre>';
    $arCurCompanys = $arInterrelationships["PROPERTY_COMPANY_LINK_VALUE"];
    $arCurLinkTypes = $arInterrelationships["PROPERTY_COMPANY_LINK_TYPE_VALUE"];
    $companyKey = array_search($companyID, $arCurCompanys);

    if ($companyKey>=0){
        if($companyID!=$oldCompanyID){
            if(!in_array($companyID, $arCurCompanys)){
                $arCurCompanys[$companyKey] = $companyID;
                $arCurLinkTypes[$companyKey] = $linkType;
                $el->SetPropertyValuesEx(
                    $arInterrelationships["ID"],
                    $arInterrelationships["IBLOCK_ID"],
                    array(
                        "COMPANY_LINK" => $arCurCompanys,
                        "COMPANY_LINK_TYPE" => $arCurLinkTypes,
                    )
                );
                $arRezultat["RESULT"] = "success";
                $arRezultat["MESS"] = "Взаимосвязь успешно изменена.<br />";
                $arRezultat["ELEM_ID"] = $arInterrelationships["ID"];
                $arRezultat["LINK_TYPE_ID"] = $linkType;
                $arRezultat["COMPANY_ID"] = $companyID;
            } else {
                $arRezultat["RESULT"] = "error";
                $arRezultat["MESS"] = "Данная компания уже добавлена.<br />";
            }
        } else {
            $arCurCompanys[$companyKey] = $companyID;
            $arCurLinkTypes[$companyKey] = $linkType;
            $el->SetPropertyValuesEx(
                $arInterrelationships["ID"],
                $arInterrelationships["IBLOCK_ID"],
                array(
                    "COMPANY_LINK" => $arCurCompanys,
                    "COMPANY_LINK_TYPE" => $arCurLinkTypes,
                )
            );
            $arRezultat["RESULT"] = "success";
            $arRezultat["MESS"] = "Взаимосвязь успешно изменена.<br />";
            $arRezultat["ELEM_ID"] = $arInterrelationships["ID"];
            $arRezultat["LINK_TYPE_ID"] = $linkType;
            $arRezultat["COMPANY_ID"] = $companyID;
        }
    } else {
        $arRezultat["RESULT"] = "error";
        $arRezultat["MESS"] = "Данная компания не найдена в существующих взаимосвязях.<br />";
    }
}
if($action=="del") {
    $dbDublikate = $el->GetList(
        array("ID" => "ASC"),
        array("IBLOCK_ID" => GetIBlockIDByCode("lead_interrelationships"), "ACTIVE" => "Y", "PROPERTY_LEAD_LINK" => $leadId),
        false,
        false,
        array("ID", "IBLOCK_ID", "NAME", "PROPERTY_LEAD_LINK", "PROPERTY_COMPANY_LINK", "PROPERTY_COMPANY_LINK_TYPE")
    );
    $arInterrelationships = $dbDublikate->GetNext();
    $arCurCompanys = $arInterrelationships["PROPERTY_COMPANY_LINK_VALUE"];
    $arCurLinkTypes = $arInterrelationships["PROPERTY_COMPANY_LINK_TYPE_VALUE"];
    $arRezultat["COMPANYS"] = $arCurCompanys;
    $arRezultat["COMPANY"] = $companyID;
    $arRezultat["COMPANY_SEARCH"] = array_search($companyID, $arCurCompanys);
    $companyKey = array_search($companyID, $arCurCompanys);

    if ($companyKey >= 0 ){
        unset($arCurCompanys[$companyKey]);
        unset($arCurLinkTypes[$companyKey]);
        if(sizeof($arCurCompanys)==0){
            $el->Delete($arInterrelationships["ID"]);
        } else {
            $el->SetPropertyValuesEx(
                $arInterrelationships["ID"],
                $arInterrelationships["IBLOCK_ID"],
                array(
                    "COMPANY_LINK" => $arCurCompanys,
                    "COMPANY_LINK_TYPE" => $arCurLinkTypes,
                )
            );
        }
        $arRezultat["RESULT"] = "success";
        $arRezultat["MESS"] = "Взаимосвязь успешно удалена.<br />";
        $arRezultat["ELEM_ID"] = $arInterrelationships["ID"];
        $arRezultat["LINK_TYPE_ID"] = $linkType;
        $arRezultat["COMPANY_ID"] = $companyID;
    } else {
        $arRezultat["RESULT"] = "error";
        $arRezultat["MESS"] = "Данная компания не найдена в существующих взаимосвязях.<br />";
    }
}
echo json_encode($arRezultat);
?>