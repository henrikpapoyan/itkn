<?php
//Проверка на AJAX запрос
if(isset($_SERVER['HTTP_BX_AJAX']) && strtolower($_SERVER['HTTP_BX_AJAX']) == 'true') {
} else {
    return false;
}
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
$logger = Logger::getLogger('CompanyAjax','ajax/lead/address.log');
//Проверка наличия строки поиска
if(!isset($_REQUEST['likeText'])){
    $logger->log("Нет строки поиска.");
    return false;
}
//Подключаем модуль для работа с CRM
if (!CModule::IncludeModule('crm') || !CModule::IncludeModule('iblock'))
{
    $logger->log("Модуль CRM/iBlock не удалось подключить");
    return false;
}
$likeText = $_REQUEST['likeText'];
//echo '<pre>'.print_r($_REQUEST, true).'</pre>';
$dbCompanys = CCrmCompany::GetList(Array("NAME"=>"ASC"), Array("%TITLE"=>$likeText));
if($dbCompanys->SelectedRowsCount()>0){
    echo '<ul id="company-list-search-result">';
    while($arCompany = $dbCompanys->GetNext()):
        //echo '<pre>'.print_r($arCompany, true).'</pre>';?>
        <li data-company-id="<?=$arCompany["ID"]?>"><?=$arCompany["TITLE"]?></li>
    <?endwhile;
    echo '</ul>';
} else {
    echo 'По тексту "'.$likeText.'" совпадений не найдено.';
}
?>