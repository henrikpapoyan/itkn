<?php
//Проверка на AJAX запрос
/*if(isset($_SERVER['HTTP_BX_AJAX']) && strtolower($_SERVER['HTTP_BX_AJAX']) == 'true') {
} else {
    return false;
}*/
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
$logger = Logger::getLogger('CompanyAjax','ajax/company/get_company_by_inn_kpp.log');
//Проверка наличия строки поиска
if(!isset($_REQUEST['paramINN'])){
    $logger->log("Не указан ИНН.");
    return false;
}
if(!isset($_REQUEST['paramKPP'])){
    $logger->log("Не указан КПП.");
    return false;
}
if(!isset($_REQUEST['action'])){
    $logger->log("Не указан action.");
    return false;
}
$logger->log("REQUEST[] = ".print_r($_REQUEST, true));
//Подключаем модуль для работа с CRM
if (!CModule::IncludeModule('crm'))
{
    $logger->log("Модуль CRM не удалось подключить");
    return false;
}
$logger->log("ID пользователя[".CUser::GetID()."]");
$inn = $_REQUEST['paramINN'];
$kpp = $_REQUEST['paramKPP'];
$action = $_REQUEST['action'];
$arResult = array();
//Выполняем поиск по CRM
if($action=="filter")
{
    $arFilter = array();
    if($inn != ''){
        $arFilter["RQ_INN"] = $inn;
    }
    if($kpp != ''){
        $arFilter["RQ_KPP"] = $kpp;
    }
    $CCrmCompany = new CCrmCompany();
    $req = new \Bitrix\Crm\EntityRequisite();
    $rs = $req->getList([
        "filter" => $arFilter
    ]);
    $reqData = $rs->fetchAll();
    $logger->log("Найдено ".sizeOf($reqData)." реквизитов в CRM по ИНН[$inn] КПП[$kpp]");
    if (sizeOf($reqData) > 0) {
        foreach($reqData as $arReq){
            $arCompany = $CCrmCompany->GetByID($arReq["ENTITY_ID"]);
            $arResult[] = array(
                "COMPANY_ID" => $arCompany["ID"],
                "TITLE" => $arCompany["TITLE"]
            );
        }
    } else {
        if(isset($arFilter["RQ_KPP"])){
            unset($arFilter["RQ_KPP"]);
            $rs = $req->getList([
                "filter" => $arFilter
            ]);
            $reqData = $rs->fetchAll();
            $logger->log("Найдено ".sizeOf($reqData)." реквизитов в CRM ТОЛЬКО ПО ИНН[$inn]");
            if (sizeOf($reqData) > 0) {
                foreach($reqData as $arReq){
                    $arCompany = $CCrmCompany->GetByID($arReq["ENTITY_ID"]);
                    $arResult[] = array(
                        "COMPANY_ID" => $arCompany["ID"],
                        "TITLE" => $arCompany["TITLE"],
                        "RQ_INN" => $arReq["RQ_INN"],
                        "RQ_KPP" => $arReq["RQ_KPP"],
                        "FIND_ONLY_BY_INN" => "Y"
                    );
                }
            }
        }
    }
}
//Выполняем поиск в Online
if($action=="search")
{
    $kontragentInfo = GetKontragentInfo($inn);
    if(!empty($kontragentInfo))
    {
        $arKontragent = $kontragentInfo[0];
        if(strlen($inn) == 12)
        {
            $arResult["TITLE"] = "ИП ".$arKontragent["caption"];
            $arResult["INN"] = $inn;
            $arResult["KPP"] = "";
        }
        else
        {
            $arResult["TITLE"] = $arKontragent["caption"];
            $arResult["INN"] = $inn;
            $arResult["KPP"] = $arKontragent["fields"]["RQ_KPP"] != '' ? $arKontragent["fields"]["RQ_KPP"] : '';
        }
    }
}
echo json_encode($arResult);
?>