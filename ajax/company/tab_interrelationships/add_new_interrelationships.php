<?php
//Проверка на AJAX запрос
if(isset($_SERVER['HTTP_BX_AJAX']) && strtolower($_SERVER['HTTP_BX_AJAX']) == 'true') {
} else {
    return false;
}
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
$logger = Logger::getLogger('CompanyAjax','ajax/lead/address.log');
$arRezultat = array();
//Подключаем модуль для работа с CRM
if (!CModule::IncludeModule('iblock'))
{
    $logger->log("Модуль CRM/iBlock не удалось подключить");
    return false;
}
if(!isset($_REQUEST["action"])){
    $logger->log("Не передано значение action");
    return false;
}
$action = $_REQUEST["action"];
if(!isset($_REQUEST['linkType']) || !isset($_REQUEST['companyID']) || !isset($_REQUEST['mainCompanyId'])){
    $logger->log("Нет переменных linkType/companyID.");
    return false;
} else {
    $linkType = $_REQUEST['linkType'];
    $companyID = $_REQUEST['companyID'];
    $mainCompanyId = $_REQUEST['mainCompanyId'];
}
$el = new CIBlockElement();
//Типы взаимосвязей
$rsLinkType = $el->GetList(
    array("ID"=>"ASC"),
    array("IBLOCK_ID" => GetIBlockIDByCode("company_link_type"), "ACTIVE" => "Y"),
    false,
    false,
    array("ID", "NAME")
);
$materinskayaID = 0;
$dochernayaID = 0;
while($arLinkType = $rsLinkType->GetNext()){
    if($arLinkType["NAME"]=="Материнская"){
        $materinskayaID = $arLinkType["ID"];
    }
    if($arLinkType["NAME"]=="Дочерняя"){
        $dochernayaID = $arLinkType["ID"];
    }
}
if($linkType==$materinskayaID){
    $reverceLinkType = $dochernayaID;
} else {
    $reverceLinkType = $materinskayaID;
}
if($action == "add") {
    //Добавление основной взаимосвязи
    $dbDublikate = $el->GetList(
        array("ID" => "ASC"),
        array("IBLOCK_ID" => GetIBlockIDByCode("company_interrelationships"), "ACTIVE" => "Y", "PROPERTY_MAIN_COMPANY" => $mainCompanyId),
        false,
        false,
        array("ID", "IBLOCK_ID", "NAME", "PROPERTY_MAIN_COMPANY", "PROPERTY_COMPANY_LINK", "PROPERTY_COMPANY_LINK_TYPE")
    );
    if ($dbDublikate->SelectedRowsCount() == 0) {
        $PROP = array(
            "MAIN_COMPANY" => $mainCompanyId,
            "COMPANY_LINK" => $companyID,
            "COMPANY_LINK_TYPE" => $linkType,
        );
        $arLoadProductArray = Array(
            "MODIFIED_BY" => $USER->GetID(),
            "IBLOCK_SECTION_ID" => false,
            "IBLOCK_ID" => GetIBlockIDByCode("company_interrelationships"),
            "PROPERTY_VALUES" => $PROP,
            "NAME" => "Взаимосвязь для компании " . $mainCompanyId,
            "ACTIVE" => "Y",
        );

        if ($PRODUCT_ID = $el->Add($arLoadProductArray)) {
            $arRezultat["RESULT"] = "success";
            $arRezultat["MESS"] = "Взаимосвязь успешно добавлена.<br />";
            $arRezultat["ELEM_ID"] = $PRODUCT_ID;
            $arRezultat["LINK_TYPE_ID"] = $linkType;
            $arRezultat["COMPANY_ID"] = $companyID;
        } else {
            $arRezultat["RESULT"] = "error";
            $arRezultat["MESS"] = "Error: " . $el->LAST_ERROR;
        }
    } else {
        $arInterrelationships = $dbDublikate->GetNext();
        //    echo '<pre>'.print_r($arInterrelationships, true).'</pre>';
        $arCurCompanys = $arInterrelationships["PROPERTY_COMPANY_LINK_VALUE"];
        $arCurLinkTypes = $arInterrelationships["PROPERTY_COMPANY_LINK_TYPE_VALUE"];
        if (!in_array($companyID, $arCurCompanys)) {
            $arCurCompanys[] = $companyID;
            $arCurLinkTypes[] = $linkType;
            CIBlockElement::SetPropertyValuesEx(
                $arInterrelationships["ID"],
                $arInterrelationships["IBLOCK_ID"],
                array(
                    "COMPANY_LINK" => $arCurCompanys,
                    "COMPANY_LINK_TYPE" => $arCurLinkTypes,
                )
            );
        }
    }

    //Добавление обратной взаимосвязи
    $dbDublikate = $el->GetList(
        array("ID" => "ASC"),
        array("IBLOCK_ID" => GetIBlockIDByCode("company_interrelationships"), "ACTIVE" => "Y", "PROPERTY_MAIN_COMPANY" => $companyID),
        false,
        false,
        array("ID", "IBLOCK_ID", "NAME", "PROPERTY_MAIN_COMPANY", "PROPERTY_COMPANY_LINK", "PROPERTY_COMPANY_LINK_TYPE")
    );
    if ($dbDublikate->SelectedRowsCount() == 0) {
        $PROP = array(
            "MAIN_COMPANY" => $companyID,
            "COMPANY_LINK" => $mainCompanyId,
            "COMPANY_LINK_TYPE" => $reverceLinkType,
        );
        $arLoadProductArray = Array(
            "MODIFIED_BY" => $USER->GetID(),
            "IBLOCK_SECTION_ID" => false,
            "IBLOCK_ID" => GetIBlockIDByCode("company_interrelationships"),
            "PROPERTY_VALUES" => $PROP,
            "NAME" => "Взаимосвязь для компании " . $companyID,
            "ACTIVE" => "Y",
        );

        if ($PRODUCT_ID = $el->Add($arLoadProductArray)) {
            $arRezultat["RESULT"] = "success";
            $arRezultat["MESS"] = "Взаимосвязь успешно добавлена.<br />";
            $arRezultat["ELEM_ID"] = $PRODUCT_ID;
            $arRezultat["LINK_TYPE_ID"] = $reverceLinkType;
            $arRezultat["COMPANY_ID"] = $mainCompanyId;
        } else {
            $arRezultat["RESULT"] = "error";
            $arRezultat["MESS"] = "Error: " . $el->LAST_ERROR;
        }
    } else {
        $arInterrelationships = $dbDublikate->GetNext();
        //    echo '<pre>'.print_r($arInterrelationships, true).'</pre>';
        $arCurCompanys = $arInterrelationships["PROPERTY_COMPANY_LINK_VALUE"];
        $arCurLinkTypes = $arInterrelationships["PROPERTY_COMPANY_LINK_TYPE_VALUE"];
        if (!in_array($mainCompanyId, $arCurCompanys)) {
            $arCurCompanys[] = $mainCompanyId;
            $arCurLinkTypes[] = $reverceLinkType;
            CIBlockElement::SetPropertyValuesEx(
                $arInterrelationships["ID"],
                $arInterrelationships["IBLOCK_ID"],
                array(
                    "COMPANY_LINK" => $arCurCompanys,
                    "COMPANY_LINK_TYPE" => $arCurLinkTypes,
                )
            );
            $arRezultat["RESULT"] = "success";
            $arRezultat["MESS"] = "Взаимосвязь успешно добавлена.<br />";
            $arRezultat["ELEM_ID"] = $arInterrelationships["ID"];
            $arRezultat["LINK_TYPE_ID"] = $reverceLinkType;
            $arRezultat["COMPANY_ID"] = $mainCompanyId;
        } else {
            $arRezultat["RESULT"] = "error";
            $arRezultat["MESS"] = "Данная компания уже добавлена.<br />";
        }
    }
}

if($action == "edit"){
    if(!isset($_REQUEST['oldCompanyID'])){
        $logger->log("Не указан старый ID компании.");
        return false;
    }

    //Изменение основной взаимосвязи
    $dbDublikate = $el->GetList(
        array("ID" => "ASC"),
        array("IBLOCK_ID" => GetIBlockIDByCode("company_interrelationships"), "ACTIVE" => "Y", "PROPERTY_MAIN_COMPANY" => $mainCompanyId),
        false,
        false,
        array("ID", "IBLOCK_ID", "NAME", "PROPERTY_MAIN_COMPANY", "PROPERTY_COMPANY_LINK", "PROPERTY_COMPANY_LINK_TYPE")
    );
    $arInterrelationships = $dbDublikate->GetNext();
    //    echo '<pre>'.print_r($arInterrelationships, true).'</pre>';
    $arCurCompanys = $arInterrelationships["PROPERTY_COMPANY_LINK_VALUE"];
    $arCurLinkTypes = $arInterrelationships["PROPERTY_COMPANY_LINK_TYPE_VALUE"];
    $companyKey = array_search($companyID, $arCurCompanys);

    if ($companyKey>=0){
        $arCurCompanys[$companyKey] = $companyID;
        $arCurLinkTypes[$companyKey] = $linkType;
        $el->SetPropertyValuesEx(
            $arInterrelationships["ID"],
            $arInterrelationships["IBLOCK_ID"],
            array(
                "COMPANY_LINK" => $arCurCompanys,
                "COMPANY_LINK_TYPE" => $arCurLinkTypes,
            )
        );
        $arRezultat["RESULT"] = "success";
        $arRezultat["MESS"] = "Взаимосвязь успешно изменена.<br />";
        $arRezultat["ELEM_ID"] = $arInterrelationships["ID"];
        $arRezultat["LINK_TYPE_ID"] = $linkType;
        $arRezultat["COMPANY_ID"] = $companyID;
    } else {
        $arRezultat["RESULT"] = "error";
        $arRezultat["MESS"] = "Данная компания не найдена в существующих взаимосвязях.<br />";
    }

    //Изменение обратной взаимосвязи
    $dbDublikate = $el->GetList(
        array("ID" => "ASC"),
        array("IBLOCK_ID" => GetIBlockIDByCode("company_interrelationships"), "ACTIVE" => "Y", "PROPERTY_MAIN_COMPANY" => $companyID),
        false,
        false,
        array("ID", "IBLOCK_ID", "NAME", "PROPERTY_MAIN_COMPANY", "PROPERTY_COMPANY_LINK", "PROPERTY_COMPANY_LINK_TYPE")
    );
    $arInterrelationships = $dbDublikate->GetNext();
    //    echo '<pre>'.print_r($arInterrelationships, true).'</pre>';
    $arCurCompanys = $arInterrelationships["PROPERTY_COMPANY_LINK_VALUE"];
    $arCurLinkTypes = $arInterrelationships["PROPERTY_COMPANY_LINK_TYPE_VALUE"];
    $companyKey = array_search($mainCompanyId, $arCurCompanys);

    if ($companyKey>=0){
        $arCurCompanys[$companyKey] = $mainCompanyId;
        $arCurLinkTypes[$companyKey] = $reverceLinkType;
        $el->SetPropertyValuesEx(
            $arInterrelationships["ID"],
            $arInterrelationships["IBLOCK_ID"],
            array(
                "COMPANY_LINK" => $arCurCompanys,
                "COMPANY_LINK_TYPE" => $arCurLinkTypes,
            )
        );
    }
}

if($action == "del") {
    //Удаление основной взаимосвязи
    $dbDublikate = $el->GetList(
        array("ID" => "ASC"),
        array("IBLOCK_ID" => GetIBlockIDByCode("company_interrelationships"), "ACTIVE" => "Y", "PROPERTY_MAIN_COMPANY" => $mainCompanyId),
        false,
        false,
        array("ID", "IBLOCK_ID", "NAME", "PROPERTY_MAIN_COMPANY", "PROPERTY_COMPANY_LINK", "PROPERTY_COMPANY_LINK_TYPE")
    );
    $arInterrelationships = $dbDublikate->GetNext();
    $arCurCompanys = $arInterrelationships["PROPERTY_COMPANY_LINK_VALUE"];
    $arCurLinkTypes = $arInterrelationships["PROPERTY_COMPANY_LINK_TYPE_VALUE"];
    $arRezultat["COMPANYS"] = $arCurCompanys;
    $arRezultat["COMPANY"] = $companyID;
    $arRezultat["COMPANY_SEARCH"] = array_search($companyID, $arCurCompanys);
    $companyKey = array_search($companyID, $arCurCompanys);

    if ($companyKey >= 0 ){
        unset($arCurCompanys[$companyKey]);
        unset($arCurLinkTypes[$companyKey]);
        if(sizeof($arCurCompanys)==0){
            $el->Delete($arInterrelationships["ID"]);
        } else {
            $el->SetPropertyValuesEx(
                $arInterrelationships["ID"],
                $arInterrelationships["IBLOCK_ID"],
                array(
                    "COMPANY_LINK" => $arCurCompanys,
                    "COMPANY_LINK_TYPE" => $arCurLinkTypes,
                )
            );
        }
        $arRezultat["RESULT"] = "success";
        $arRezultat["MESS"] = "Взаимосвязь успешно удалена.<br />";
        $arRezultat["ELEM_ID"] = $arInterrelationships["ID"];
        $arRezultat["LINK_TYPE_ID"] = $linkType;
        $arRezultat["COMPANY_ID"] = $companyID;
    } else {
        $arRezultat["RESULT"] = "error";
        $arRezultat["MESS"] = "Данная компания не найдена в существующих взаимосвязях.<br />";
    }

    //Удаление обратной взаимосвязи
    $dbDublikate = $el->GetList(
        array("ID" => "ASC"),
        array("IBLOCK_ID" => GetIBlockIDByCode("company_interrelationships"), "ACTIVE" => "Y", "PROPERTY_MAIN_COMPANY" => $companyID),
        false,
        false,
        array("ID", "IBLOCK_ID", "NAME", "PROPERTY_MAIN_COMPANY", "PROPERTY_COMPANY_LINK", "PROPERTY_COMPANY_LINK_TYPE")
    );
    $arInterrelationships = $dbDublikate->GetNext();
    $arCurCompanys = $arInterrelationships["PROPERTY_COMPANY_LINK_VALUE"];
    $arCurLinkTypes = $arInterrelationships["PROPERTY_COMPANY_LINK_TYPE_VALUE"];
    $arRezultat["COMPANYS"] = $arCurCompanys;
    $arRezultat["COMPANY"] = $mainCompanyId;
    $arRezultat["COMPANY_SEARCH"] = array_search($mainCompanyId, $arCurCompanys);
    $companyKey = array_search($mainCompanyId, $arCurCompanys);

    if ($companyKey >= 0 ){
        unset($arCurCompanys[$companyKey]);
        unset($arCurLinkTypes[$companyKey]);
        if(sizeof($arCurCompanys)==0){
            $el->Delete($arInterrelationships["ID"]);
        } else {
            $el->SetPropertyValuesEx(
                $arInterrelationships["ID"],
                $arInterrelationships["IBLOCK_ID"],
                array(
                    "COMPANY_LINK" => $arCurCompanys,
                    "COMPANY_LINK_TYPE" => $arCurLinkTypes,
                )
            );
        }
    }
}
echo json_encode($arRezultat);
?>