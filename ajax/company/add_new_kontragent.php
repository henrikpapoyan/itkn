<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
$CCrmContact = new CCrmContact();
$CCrmCompany = new CCrmCompany();
global $USER;

$arResult = array();
$message = array();
$error = false;
$errorText = array();
if(check_email($_REQUEST["contact_email"]))
{
    //Массив с параметрами Контрагента
    $arKontragentFields = Array (
        "TITLE" => $_REQUEST["kontragent_name"],
        "UF_CRM_INN" => $_REQUEST["kontragent_inn"],
        "UF_CRM_KPP" => (!empty($_REQUEST["kontragent_kpp"])) ? $_REQUEST["kontragent_kpp"] : '',
        "COMPANY_TYPE" => $_REQUEST["kontragent_type"],
        "ASSIGNED_BY_ID" => $USER->GetID(),
        "CREATED_BY_ID" => $USER->GetID(),
    );
    //Массив с параметрами контакта
    $arContactFields = Array (
        "NAME" => $_REQUEST["contact_name"],
        "SECOND_NAME" => $_REQUEST["contact_secondname"],
        "LAST_NAME" => $_REQUEST["contact_lastname"],
        "ASSIGNED_BY_ID" => $USER->GetID(),
        "CREATED_BY_ID" => $USER->GetID(),
        "FM" => Array(
            "EMAIL" => Array(
                "n0" => Array(
                    "VALUE" => $_REQUEST["contact_email"],
                    "VALUE_TYPE" => "WORK",
                ),
            ),
            "PHONE" => Array(
                "n0" => Array(
                    "VALUE" => $_REQUEST["contact_phone"],
                    "VALUE_TYPE" => "WORK",
                ),
            )
        )
    );

    $requisite = new \Bitrix\Crm\EntityRequisite();
    if($newContragentID = $CCrmCompany->Add($arKontragentFields)){
        $message[] = "Новая компания успешно добавлена (ID = $newContragentID)";
        $arContactFields["COMPANY_ID"] = $newContragentID;
        //echo '$arContactFields = <pre>'.print_r($arContactFields, true).'</pre>';

        if($newContactID = $CCrmContact->Add($arContactFields)){
            $message[] = "Новый контакт успешно добавлен (ID = $newContactID)";
            $kontragentInfo = GetKontragentInfo($_REQUEST["kontragent_inn"]);
            $arRequisite = $kontragentInfo[0]["fields"];
            if(strlen($_REQUEST["kontragent_inn"]) == 10)
            {
                $arRequisite["PRESET_ID"] = 1;
                $arRequisite["NAME"] = "Организация";
            }
            if(strlen($_REQUEST["kontragent_inn"]) == 12)
            {
                $arRequisite["PRESET_ID"] = 2;
                $arRequisite["NAME"] = "ИП";
            }
            $arRequisite["ENTITY_ID"] = $newContragentID;
            $arRequisite["ENTITY_TYPE_ID"] = CCrmOwnerType::Company;
            //echo '$arRequisite = <pre>'.print_r($arRequisite, true).'</pre>';
            $result = $requisite->add($arRequisite);
            //echo 'Реквизиты успешно добавлены '.$result->getId().'<br />';
        } else{
            $CCrmCompany->Delete($newContragentID);
            $errorText[] = "ERROR: ".$CCrmContact->LAST_ERROR;
            $error = true;
        }
    }
    else{
        $errorText[] = "ERROR: ".$CCrmCompany->LAST_ERROR;
        $error = true;
    }
}
else
{
    $errorText[] = "Укажите корректный e-mail";
    $error = true;
}
if($error){
    $arResult = array(
        "RESULT" => "ERROR",
        "MESSAGE" => $errorText
    );
} else {
    $arResult = array(
        "RESULT" => "SUCCESS",
        "MESSAGE" => $message,
        "KONTRAGENT_ID" => $newContragentID,
        "KONTRAGENT_TITLE" => $arKontragentFields["TITLE"],
    );
}
echo json_encode($arResult);
?>