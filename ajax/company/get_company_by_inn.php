<?php
/**
 * Created by PhpStorm.
 * User: Рустам
 * Date: 14.05.2019
 * Time: 19:51
 */

//Проверка на AJAX запрос
/*if(isset($_SERVER['HTTP_BX_AJAX']) && strtolower($_SERVER['HTTP_BX_AJAX']) == 'true') {
} else {
    return false;
}*/
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);

//Подключаем модуль для работа с CRM
if (!CModule::IncludeModule('crm'))
{
    $logger->log("Модуль CRM не удалось подключить");
    return false;
}
$arResult = [];
$inn = $_REQUEST['paramINN'];
$arFilter = array();
if( !empty($inn) ){
    $arFilter["RQ_INN"] = $inn;
    $CCrmCompany = new CCrmCompany();
    $req = new \Bitrix\Crm\EntityRequisite();
    $rs = $req->getList([
        "filter" => $arFilter
    ]);
    $reqData = $rs->fetchAll();
    if (sizeOf($reqData) > 0) {
        foreach($reqData as $arReq){
            $arCompany = $CCrmCompany->GetByID($arReq["ENTITY_ID"]);
            $companyTitle = $arCompany["TITLE"]." (ИНН: $arReq[RQ_INN]";
            $companyTitle .= !empty($arReq["RQ_KPP"]) ? ", КПП: $arReq[RQ_KPP])" : ")";
            $arResult[] = array(
                "COMPANY_ID" => $arCompany["ID"],
                "TITLE" => $companyTitle,
                "TITLE_VALUE" => !empty($arReq["RQ_KPP"]) ? "$arCompany[TITLE] (КПП: $arReq[RQ_KPP])" : $arCompany["TITLE"],
            );
        }
    }
    echo json_encode($arResult);
}