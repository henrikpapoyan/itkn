<?php

//Проверка на AJAX запрос
if(isset($_SERVER['HTTP_BX_AJAX']) && strtolower($_SERVER['HTTP_BX_AJAX']) == 'true') {
} else {
    return false;
}

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);

$logger = Logger::getLogger('CompanyAjax','ajax/company/registryPay.log');

//Проверка наличия ID компании
if(!isset($_POST['id']) || (int)$_POST['id'] <=0){
    $logger->log("Нет ID компании.");
    return false;
}

//Подключаем модуль для работа с CRM
if (!CModule::IncludeModule('crm') || !CModule::IncludeModule('iblock'))
{
    $logger->log("Модуль CRM/iBlock не удалось подключить");
    return false;
}

$arResult       = [];
$companyID      = $_POST['id'];

// Получаем информацию от инфоблока "Реестр платежей"
$IBLOCK_ID = getIblockIDByCode('reestr_pay');

$arSelect = Array("ID", "NAME","PROPERTY_*");
$arFilter = Array("IBLOCK_ID" => $IBLOCK_ID, "PROPERTY_COMPANY" => $companyID);
$res = CIBlockElement::GetList(Array("ID"=>"DESC"), $arFilter, false, false, $arSelect);

while($ob = $res->GetNextElement()) {

    $arFields                    = $ob->GetFields();
    $arProps                     = $ob->GetProperties();

    $arTable['ID']              = $arFields['ID'];
    $arTable['NAME']            = $arFields['NAME'];
    $arTable['NUM_LIC_SCHET']   = $arProps['NUM_LIC_SCHET']['VALUE'];
    $arTable['KRNM_KKT']        = $arProps['RNM_KKT']['VALUE'];
    $arTable['INN']             = $arProps['INN']['VALUE'];
    $arTable['KPP']             = $arProps['KPP']['VALUE'];
    $arTable['LAST']            = $arProps['LAST']['VALUE'];
    $arTable['TYPE']            = $arProps['TYPE']['VALUE'];
    $arTable['SUMM']            = $arProps['SUMM']['VALUE'];
    $arTable['SOSTOYANIE']      = $arProps['SOSTOYANIE']['VALUE'];
    $arTable['COMPANY']         = $arProps['COMPANY']['VALUE'];
    $arTable['ASSIGNED']        = $arProps['ASSIGNED']['VALUE'];
    $arTable['ACTIVATION_DATE'] = $arProps['ACTIVATION_DATE']['VALUE'];

    $arTable['URL']= "/services/lists/".$IBLOCK_ID."/element/0/".$arFields['ID']."/";

    $arResult[] = $arTable;
}

?>
<div style="height: 100%;">
    <div class="bx-crm-view-fieldset">
        <div class="bx-crm-view-fieldset-content-tiny">
            <table class="bx-crm-view-fieldset-content-table">
                <tbody>
                <tr>
                    <td class="bx-field-value" colspan="2">
                        <div class="main-grid main-grid-load-animation"  style="display: block;">
                            <div class="main-grid-wrapper">
                                <div class="main-grid-fade">
                                    <div class="main-grid-container">

                                        <table class="main-grid-table" >
                                            <thead class="main-grid-header">
                                            <tr class="main-grid-row-head">
                                                <th class="main-grid-cell-head main-grid-cell-left main-grid-col-no-sortable  main-grid-draggable">
                                                        <span class="main-grid-cell-head-container">
                                                            <span class="main-grid-head-title">Название</span>
                                                        </span>
                                                </th>
                                                <th class="main-grid-cell-head main-grid-cell-left main-grid-col-no-sortable  main-grid-draggable">
                                                        <span class="main-grid-cell-head-container">
                                                            <span class="main-grid-head-title">ИНН</span>
                                                        </span>
                                                </th>
                                                <th class="main-grid-cell-head main-grid-cell-left main-grid-col-no-sortable  main-grid-draggable">
                                                        <span class="main-grid-cell-head-container">
                                                            <span class="main-grid-head-title">КПП</span>
                                                        </span>
                                                </th>
                                                <th class="main-grid-cell-head main-grid-cell-left main-grid-col-no-sortable  main-grid-draggable">
                                                        <span class="main-grid-cell-head-container">
                                                            <span class="main-grid-head-title">РНМ ККТ</span>
                                                        </span>
                                                </th>
                                                <th class="main-grid-cell-head main-grid-cell-left main-grid-col-no-sortable  main-grid-draggable">
                                                        <span class="main-grid-cell-head-container">
                                                            <span class="main-grid-head-title">Номер Л/C</span>
                                                        </span>
                                                </th>
                                                <th class="main-grid-cell-head main-grid-cell-left main-grid-col-no-sortable  main-grid-draggable">
                                                        <span class="main-grid-cell-head-container">
                                                            <span class="main-grid-head-title">Последнее событие</span>
                                                        </span>
                                                </th>
                                                <th class="main-grid-cell-head main-grid-cell-left main-grid-col-no-sortable  main-grid-draggable">
                                                        <span class="main-grid-cell-head-container">
                                                            <span class="main-grid-head-title">Тип операции</span>
                                                        </span>
                                                </th>
                                                <th class="main-grid-cell-head main-grid-cell-left main-grid-col-no-sortable  main-grid-draggable">
                                                        <span class="main-grid-cell-head-container">
                                                            <span class="main-grid-head-title">Сумма операции</span>
                                                        </span>
                                                </th>
                                                <th class="main-grid-cell-head main-grid-cell-left main-grid-col-no-sortable  main-grid-draggable">
                                                        <span class="main-grid-cell-head-container">
                                                            <span class="main-grid-head-title">Текущее состояние Л/C</span>
                                                        </span>
                                                </th>
                                                <th class="main-grid-cell-head main-grid-cell-left main-grid-col-no-sortable  main-grid-draggable">
                                                        <span class="main-grid-cell-head-container">
                                                            <span class="main-grid-head-title">Дата события</span>
                                                        </span>
                                                </th>
                                            </tr>
                                            </thead>

                                            <tbody>
                                            <?foreach ($arResult as $value):?>
                                                <tr class="main-grid-row main-grid-row-body">
                                                    <td class="main-grid-cell main-grid-cell-left">
                                                            <span class="main-grid-cell-content">
                                                                <a href="<?=$value['URL']?>/"><?=$value['NAME']?></a>
                                                            </span>
                                                    </td>
                                                    <td class="main-grid-cell main-grid-cell-left">
                                                        <span class="main-grid-cell-content"><?=($value['INN'] != "")? $value['INN'] : '-';?></span>
                                                    </td>
                                                    <td class="main-grid-cell main-grid-cell-left">
                                                        <span class="main-grid-cell-content"><?=($value['KPP'] != "")? $value['KPP'] : '-';?></span>
                                                    </td>
                                                    <td class="main-grid-cell main-grid-cell-left">
                                                        <span class="main-grid-cell-content"><?=($value['KRNM_KKT'] != "")? $value['KRNM_KKT'] : '-';?></span>
                                                    </td>
                                                    <td class="main-grid-cell main-grid-cell-left">
                                                        <span class="main-grid-cell-content"><?=($value['NUM_LIC_SCHET'] != "")? $value['NUM_LIC_SCHET'] : '-';?></span>
                                                    </td>
                                                    <td class="main-grid-cell main-grid-cell-left">
                                                        <span class="main-grid-cell-content"><?=($value['LAST'] != "")? $value['LAST'] : '-';?></span>
                                                    </td>
                                                    <td class="main-grid-cell main-grid-cell-left">
                                                        <span class="main-grid-cell-content"><?=($value['TYPE'] != "")? $value['TYPE'] : '-';?></span>
                                                    </td>
                                                    <td class="main-grid-cell main-grid-cell-left">
                                                        <span class="main-grid-cell-content"><?=($value['SUMM'] != "")? $value['SUMM'] : '-';?></span>
                                                    </td>
                                                    <td class="main-grid-cell main-grid-cell-left">
                                                        <span class="main-grid-cell-content"><?=($value['SOSTOYANIE'] != "")? $value['SOSTOYANIE'] : '-';?></span>
                                                    </td>
                                                    <td class="main-grid-cell main-grid-cell-left">
                                                        <span class="main-grid-cell-content"><?=($value['ACTIVATION_DATE'] != "")? $value['ACTIVATION_DATE'] : '-';?></span>
                                                    </td>
                                                </tr>
                                            <?endforeach;?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>