<?php

//Проверка на AJAX запрос
if(isset($_SERVER['HTTP_BX_AJAX']) && strtolower($_SERVER['HTTP_BX_AJAX']) == 'true') {
} else {
    return false;
}

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);

$logger = Logger::getLogger('CompanyAjax','ajax/company/portrait.log');

//Проверка наличия ID контрагента
if(!isset($_POST['id']) || (int)$_POST['id'] <=0){
    $logger->log("Нет ID контрагента.");
    return false;
}

//Подключаем модуль для работа с CRM
if (!CModule::IncludeModule('crm') || !CModule::IncludeModule('iblock'))
{
    $logger->log("Модуль CRM/iBlock не удалось подключить");
    return false;
}

$companyID = (int)$_POST['id'];

SlaBpmBitrix::DisplaySlaMatrix($companyID);