<?php

//Проверка на AJAX запрос
if(isset($_SERVER['HTTP_BX_AJAX']) && strtolower($_SERVER['HTTP_BX_AJAX']) == 'true') {
} else {
    return false;
}

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);

$logger = Logger::getLogger('CompanyAjax','ajax/company/portrait.log');

//Проверка наличия ID контрагента
if(!isset($_POST['id']) || (int)$_POST['id'] <=0){
    $logger->log("Нет ID контрагента.");
    return false;
}

//Подключаем модуль для работа с CRM
if (!CModule::IncludeModule('crm') || !CModule::IncludeModule('iblock'))
{
    $logger->log("Модуль CRM/iBlock не удалось подключить");
    return false;
}

$arResult  = [];
$companyID = (int)$_POST['id'];

// счетчики сделок по статусам
$countDealStart = 0;
$countDealLose = 0;
$countDealWin = 0;

// количество сделок

// получаем все сделки по контрагенту
$arFilterDeals = array("COMPANY_ID" => $companyID);
$arSelectDeals = array("STAGE_ID", "OPPORTUNITY");

$dbDeals = CCrmDeal::GetList(array(), $arFilterDeals, $arSelectDeals);

while($arDeal = $dbDeals->GetNext())
{
	if(!empty(trim($arDeal["STAGE_ID"])))
	{
		$dealStatus = explode(":", $arDeal["STAGE_ID"]);

		// для обработки разных статусов
		if(count($dealStatus) == 1) $dealStatus = $dealStatus["0"];
		else $dealStatus = $dealStatus["1"];

		switch($dealStatus)
		{
			case "NEW":
			case "DETAILS":
			case "PROPOSAL":
			case "NEGOTIATION":
				$countDealStart += round($arDeal["OPPORTUNITY"], 2);
				break;

			case "LOSE":
				$countDealLose += round($arDeal["OPPORTUNITY"], 2);
				break;

			case "WON":
				$countDealWin += round($arDeal["OPPORTUNITY"], 2);
				break;
		}
	}
}

// количество счетов

// счетчики всех счетов и оплаченных вовремя
$countInvoiceAll = 0;
$countInvoiceComplete = 0;
$countIndexFinDes = 0;

$arFilterInvoices = array("=UF_COMPANY_ID" => $companyID);
$arSelectFieldsInvoices = array("STATUS_ID");

$dbInvoices = CCrmInvoice::GetList(array(), $arFilterInvoices, false, false, $arSelectFieldsInvoices);
while($arInvoice = $dbInvoices->GetNext())
{
	$countInvoiceAll += 1;
	if(!empty(trim($arInvoice["STATUS_ID"])))
	{
		if($arInvoice["STATUS_ID"] == "P")
		{
			$countInvoiceComplete += 1;
		}
	}
}

if($countInvoiceAll > 0) $countIndexFinDes = round(((100 * $countInvoiceComplete) / $countInvoiceAll));

// количество обращений
$countMessages = 0;

if(!empty($companyID))
{
	$arFilterMessages = array("IBLOCK_CODE" => array(IBLOK_TWO_LINE, IBLOK_OTDEL_MARKET, IBLOK_OTDEL_SALE, IBLOK_OTDEL_SALE_PARTNER), "PROPERTY_KOMPANIYA_KLIENTA" => $companyID);
	$arSelectMessages = array("ID");

	$dbMessages = CIBlockElement::GetList(array(), $arFilterMessages, false, false, $arSelectMessages);
	$countMessages = $dbMessages->SelectedRowsCount();
}

// количество дел
$countActivitiesAll = 0;

$arFilterActivities = array("OWNER_TYPE_ID" => 4, "OWNER_ID" => $companyID);
$arSelectActivities = array("ID");


$dbActivities = CCrmActivity::GetList(array(), $arFilterActivities, false, false, $arSelectActivities);
while($arActivity = $dbActivities->GetNext())
{
	$countActivitiesAll += 1;
}
?>
<div>&nbsp;</div><br />


<table cellspacing="0" class="reports-list-table" id="report-result-table">
	<caption><strong>Суммы сделок по статусам</strong><br /><br /></caption>
	<tbody>
		<tr>
			<td class="reports-first-column reports-total-column">Cумма заключенных сделок</td>
			<td class=" reports-total-column">Cумма сделок в работе</td>
			<td class="reports-last-column reports-total-column">Cумма проваленных сделок</td>
		</tr>
		<tr>
			<td class="reports-first-column"><?=$countDealWin . " руб.";?></td>
			<td class=" reports-numeric-column"><?=$countDealStart . " руб.";?></td>
			<td class=" reports-numeric-column"><?=$countDealLose . " руб.";?></td>
		</tr>
	</tbody>
</table>

<div>&nbsp;</div><br />


<table cellspacing="0" class="reports-list-table" id="report-result-table">
	<caption><strong>Контроль финансовой дисциплины</strong><br /><br /></caption>
	<tbody>
	<tr>
		<td class="reports-first-column reports-total-column">Количество оплаченных вовремя счетов</td>
		<td class=" reports-total-column">Количество выставленных счетов</td>
		<td class="reports-last-column reports-total-column">Индекс финансовой дисциплины</td>
	</tr>
	<tr>
		<td class="reports-first-column"><?=$countInvoiceComplete;?></td>
		<td class=" reports-numeric-column"><?=$countInvoiceAll;?></td>
		<td class=" reports-numeric-column"><?=$countIndexFinDes . " %";?></td>
	</tr>
	</tbody>
</table>

<div>&nbsp;</div><br />


<table cellspacing="0" class="reports-list-table" id="report-result-table">
	<caption><strong>Количество обращений</strong><br /><br /></caption>
	<tbody>
		<tr>
			<td class="reports-first-column"><?=$countMessages;?></td>
		</tr>
	</tbody>
</table>

<div>&nbsp;</div><br />


<table cellspacing="0" class="reports-list-table" id="report-result-table">
	<caption><strong>Количество дел</strong><br /><br /></caption>
	<tbody>
	<tr>
		<td class="reports-first-column"><?=$countActivitiesAll;?></td>
	</tr>
	</tbody>
</table>