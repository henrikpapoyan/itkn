<?php

//Проверка на AJAX запрос
if(isset($_SERVER['HTTP_BX_AJAX']) && strtolower($_SERVER['HTTP_BX_AJAX']) == 'true') {
} else {
    return false;
}

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);

$logger = Logger::getLogger('CompanyAjax','ajax/company/address.log');

//Проверка наличия ID контрагента
if(!isset($_POST['id']) || (int)$_POST['id'] <=0){
    $logger->log("Нет ID контрагента.");
    return false;
}

//Подключаем модуль для работа с CRM
if (!CModule::IncludeModule('crm') || !CModule::IncludeModule('iblock'))
{
    $logger->log("Модуль CRM/iBlock не удалось подключить");
    return false;
}

$contactInfo = CCrmContact::GetByID($_POST['id']);
if(!isset($contactInfo['COMPANY_ID'])){
    echo '</br><b>Данный контакт не имеет Контрагента и адресов.</b>';
    return false;
}

$arResult       = [];
$contactId      = $_POST['id'];
$companyID      = $contactInfo['COMPANY_ID'];

//Получаем реквизиты компании
$requisite = new \Bitrix\Crm\EntityRequisite();
$rs = $requisite->getList([
    "filter" => ["ENTITY_ID" => $contactId, "ENTITY_TYPE_ID" => CCrmOwnerType::Contact,]
]);
$arRekvizits = $rs->fetchAll();
//echo '<pre>'.print_r($arRekvizits, true).'</pre>';
$arAddressType = array(
    0 => "Undefined", //const Undefined = 0;
    1 => "Фактический адрес", //const Primary = 1
    2 => "Secondary", //const Secondary = 2;
    3 => "Third", //const Third = 3;
    4 => "Адрес регистрации", //const Home = 4;
    5 => "Work", //const Work = 5;
    6 => "Юридический адрес", //const Registered = 6;
    7 => "Custom", //const Custom = 7;
    8 => "Post", //const Post = 8;
    9 => "Адрес бенефициара", //const Beneficiary = 9;
    10 => "Bank", //const Bank = 10;
);
foreach($arRekvizits as $key => $arRekvizit){
    //echo '<pre>'.print_r($arRekvizit, true).'</pre>';
    $companyAddresses = Bitrix\Crm\EntityRequisite::getAddresses($arRekvizit['ID']);
    $arAddress = array();
    foreach ($companyAddresses as $addressType => $address){
        $emptyAddress = true;
        foreach ($address as $item){
            if($item!=''){
                $emptyAddress = false;
            }
        }
        if(!$emptyAddress){
            $arAddress = $address;
            $arAddress["ADDRESS_TYPE"] = $arAddressType[$addressType];
            $arAddress["RQ_COMPANY_NAME"] = $arRekvizit["RQ_COMPANY_NAME"];
            $arResult[] = $arAddress;
        }
    }
}
//echo '<pre>'.print_r($arResult, true).'</pre><hr />';
if(sizeof($arResult)>0){
?>
    <div style="height: 100%;">
        <div class="bx-crm-view-fieldset">
            <div class="bx-crm-view-fieldset-content-tiny">
                <table class="bx-crm-view-fieldset-content-table">
                    <tbody>
                    <tr>
                        <td class="bx-field-value" colspan="2">
                            <div class="main-grid main-grid-load-animation"  style="display: block;">
                                <div class="main-grid-wrapper">
                                    <div class="main-grid-fade">
                                        <div class="main-grid-container">

                                            <table class="main-grid-table" >
                                                <thead class="main-grid-header">
                                                <tr class="main-grid-row-head">
                                                    <th class="main-grid-cell-head main-grid-cell-left main-grid-col-no-sortable  main-grid-draggable">
                                                        <span class="main-grid-cell-head-container">
                                                            <span class="main-grid-head-title">Организация</span>
                                                        </span>
                                                    </th>
                                                    <th class="main-grid-cell-head main-grid-cell-left main-grid-col-no-sortable  main-grid-draggable">
                                                        <span class="main-grid-cell-head-container">
                                                            <span class="main-grid-head-title">Тип адреса</span>
                                                        </span>
                                                    </th>
                                                    <th class="main-grid-cell-head main-grid-cell-left main-grid-col-no-sortable  main-grid-draggable">
                                                        <span class="main-grid-cell-head-container">
                                                            <span class="main-grid-head-title">Улица, дом, корпус, строение</span>
                                                        </span>
                                                    </th>
                                                    <th class="main-grid-cell-head main-grid-cell-left main-grid-col-no-sortable  main-grid-draggable">
                                                        <span class="main-grid-cell-head-container">
                                                            <span class="main-grid-head-title">Квартира / офис</span>
                                                        </span>
                                                    </th>
                                                    <th class="main-grid-cell-head main-grid-cell-left main-grid-col-no-sortable  main-grid-draggable">
                                                        <span class="main-grid-cell-head-container">
                                                            <span class="main-grid-head-title">Город</span>
                                                        </span>
                                                    </th>
                                                    <th class="main-grid-cell-head main-grid-cell-left main-grid-col-no-sortable  main-grid-draggable">
                                                        <span class="main-grid-cell-head-container">
                                                            <span class="main-grid-head-title">Район</span>
                                                        </span>
                                                    </th>
                                                    <th class="main-grid-cell-head main-grid-cell-left main-grid-col-no-sortable  main-grid-draggable">
                                                        <span class="main-grid-cell-head-container">
                                                            <span class="main-grid-head-title">Область</span>
                                                        </span>
                                                    </th>
                                                    <th class="main-grid-cell-head main-grid-cell-left main-grid-col-no-sortable  main-grid-draggable">
                                                        <span class="main-grid-cell-head-container">
                                                            <span class="main-grid-head-title">Почтовый индекс</span>
                                                        </span>
                                                    </th>
                                                    <th class="main-grid-cell-head main-grid-cell-left main-grid-col-no-sortable  main-grid-draggable">
                                                        <span class="main-grid-cell-head-container">
                                                            <span class="main-grid-head-title">Страна</span>
                                                        </span>
                                                    </th>
                                                </tr>
                                                </thead>

                                                <tbody>
                                                <?foreach ($arResult as $value):?>
                                                    <tr class="main-grid-row main-grid-row-body">
                                                        <td class="main-grid-cell main-grid-cell-left">
                                                            <span class="main-grid-cell-content">
                                                                <?=($value['RQ_COMPANY_NAME'] != "")? $value['RQ_COMPANY_NAME'] : '-';?>
                                                            </span>
                                                        </td>
                                                        <td class="main-grid-cell main-grid-cell-left">
                                                            <span class="main-grid-cell-content">
                                                                 <?=($value['ADDRESS_TYPE'] != "")? $value['ADDRESS_TYPE'] : '-';?>
                                                            </span>
                                                        </td>
                                                        <td class="main-grid-cell main-grid-cell-left">
                                                            <span class="main-grid-cell-content">
                                                                <?=($value['ADDRESS_1'] != "")? $value['ADDRESS_1'] : '-';?>
                                                            </span>
                                                        </td>
                                                        <td class="main-grid-cell main-grid-cell-left">
                                                            <span class="main-grid-cell-content">
                                                                <?=($value['ADDRESS_2'] != "")? $value['ADDRESS_2'] : '-';?>
                                                            </span>
                                                        </td>
                                                        <td class="main-grid-cell main-grid-cell-left">
                                                            <span class="main-grid-cell-content">
                                                                <?=($value['CITY'] != "")? $value['CITY'] : '-';?>
                                                            </span>
                                                        </td>
                                                        <td class="main-grid-cell main-grid-cell-left">
                                                            <span class="main-grid-cell-content">
                                                                <?=($value['REGION'] != "")? $value['REGION'] : '-';?>
                                                            </span>
                                                        </td>
                                                        <td class="main-grid-cell main-grid-cell-left">
                                                            <span class="main-grid-cell-content">
                                                                <?=($value['PROVINCE'] != "")? $value['PROVINCE'] : '-';?>
                                                            </span>
                                                        </td>
                                                        <td class="main-grid-cell main-grid-cell-left">
                                                            <span class="main-grid-cell-content">
                                                                <?=($value['POSTAL_CODE'] != "")? $value['POSTAL_CODE'] : '-';?>
                                                            </span>
                                                        </td>
                                                        <td class="main-grid-cell main-grid-cell-left">
                                                            <span class="main-grid-cell-content">
                                                                <?=($value['COUNTRY'] != "")? $value['COUNTRY'] : '-';?>
                                                            </span>
                                                        </td>
                                                    </tr>
                                                <?endforeach;?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
<?
} else {
    echo '</br><b>Для данного контакта не указаны адреса в реквизитах или не заведены реквизиты.</b>';
    return false;
}?>