<?php

//Проверка на AJAX запрос
if(isset($_SERVER['HTTP_BX_AJAX']) && strtolower($_SERVER['HTTP_BX_AJAX']) == 'true') {
} else {
    return false;
}

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);

$logger = Logger::getLogger('ContactAjax','ajax/contact/contract.log');

//Проверка наличия ID контакта
if(!isset($_POST['id']) || (int)$_POST['id'] <=0){
    $logger->log("Нет ID контакта.");
    return false;
}

//Подключаем модуль для работа с CRM
if (!CModule::IncludeModule('crm') || !CModule::IncludeModule('iblock'))
{
    $logger->log("Модуль CRM/iBlock не удалось подключить");
    return false;
}

$contactInfo = CCrmContact::GetByID($_POST['id']);
if(!isset($contactInfo['COMPANY_ID'])){
    echo '</br><b>Данный контакт не имеет Контрагента и договоров.</b>';
    return false;
}

$arResult       = [];
$companyID      = $contactInfo['COMPANY_ID'];
$companyName    = $contactInfo['COMPANY_TITLE'];

// Получаем договора контрагента из реестра договоров
$arSelect = Array("ID", "NAME", "PROPERTY_KOMPANIYA_ZAKAZCHIK", "PROPERTY_OTVETSTVENNYY_MENEDZHER", "PROPERTY__DOGOVORA", "PROPERTY_DATA_DOGOVORA", "PROPERTY_DOGOVOR");
$arFilter = Array("IBLOCK_ID" => GetIBlockIDByCode('registry_contracts'), "PROPERTY_KONTRAGENT" => $companyID);
$res = CIBlockElement::GetList(Array("ID"=>"DESC"), $arFilter, false, false, $arSelect);
if (intval($res->SelectedRowsCount()) <= 0){
    echo '</br><b>Данный контакт не имеет договоров.</b>';
    return false;
}
while($arRes = $res->Fetch()) {
    $arTable['DOGOVOR_ID']                  = $arRes['ID'];
    $arTable['DOGOVOR_NAME']                = $arRes['NAME'];
    $arTable['DOGOVOR_NUMBER']              = $arRes['PROPERTY__DOGOVORA_VALUE'];
    $arTable['DOGOVOR_DATE']                = $arRes['PROPERTY_DATA_DOGOVORA_VALUE'];
    $arTable['DOGOVOR_COMPANY_ZAKAZCHIK']   = $arRes['PROPERTY_KOMPANIYA_ZAKAZCHIK_VALUE'];

    //Получаем информацию по пльзователю
    $arTable['FIO'] = "";
    if (isset($arRes['PROPERTY_OTVETSTVENNYY_MENEDZHER_VALUE']) && (int)$arRes['PROPERTY_OTVETSTVENNYY_MENEDZHER_VALUE'] > 0) {
        $rsUser = CUser::GetByID($arRes['PROPERTY_OTVETSTVENNYY_MENEDZHER_VALUE']);
        $arUser = $rsUser->Fetch();
        $arTable['FIO'] = trim($arUser['LAST_NAME'] . " " . $arUser['NAME'] . " " . $arUser['SECOND_NAME']);
    }

    //Формируем ссылку на скачивание договора
    $arTable['DOGOVOR_FILE'] = 'none'; //по-умолчанию файл отсутствет
    if (isset($arRes['PROPERTY_DOGOVOR_VALUE']) && (int)$arRes['PROPERTY_DOGOVOR_VALUE'] > 0) {
        $arTable['DOGOVOR_FILE'] = CFile::GetPath($arRes['PROPERTY_DOGOVOR_VALUE']);
    }

    $arResult[] = $arTable;
}
?>
<div style="height: 100%;">
    <div class="bx-crm-view-fieldset">
        <div class="bx-crm-view-fieldset-content-tiny">
            <table class="bx-crm-view-fieldset-content-table">
                <tbody>
                    <tr>
                        <td class="bx-field-value" colspan="2">
                            <div class="main-grid main-grid-load-animation"  style="display: block;">
                                <div class="main-grid-wrapper">
                                    <div class="main-grid-fade">
                                        <div class="main-grid-container">

                                            <table class="main-grid-table" >
                                                <thead class="main-grid-header">
                                                <tr class="main-grid-row-head">
                                                    <th class="main-grid-cell-head main-grid-cell-left main-grid-col-no-sortable  main-grid-draggable">
                                                        <span class="main-grid-cell-head-container">
                                                            <span class="main-grid-head-title">Контрагент</span>
                                                        </span>
                                                    </th>

                                                    <th class="main-grid-cell-head main-grid-cell-left main-grid-col-no-sortable  main-grid-draggable">
                                                        <span class="main-grid-cell-head-container">
                                                            <span class="main-grid-head-title">ФИО</span>
                                                        </span>
                                                    </th>

                                                    <th class="main-grid-cell-head main-grid-cell-left main-grid-col-no-sortable  main-grid-draggable">
                                                        <span class="main-grid-cell-head-container">
                                                            <span class="main-grid-head-title">Название договора</span>
                                                        </span>
                                                    </th>
                                                    <th class="main-grid-cell-head main-grid-cell-left main-grid-col-no-sortable  main-grid-draggable">
                                                        <span class="main-grid-cell-head-container">
                                                            <span class="main-grid-head-title">Компания заказчик</span>
                                                        </span>
                                                    </th>
                                                    <th class="main-grid-cell-head main-grid-cell-left main-grid-col-no-sortable  main-grid-draggable">
                                                        <span class="main-grid-cell-head-container">
                                                            <span class="main-grid-head-title">Номер договора</span>
                                                        </span>
                                                    </th>
                                                    <th class="main-grid-cell-head main-grid-cell-left main-grid-col-no-sortable  main-grid-draggable">
                                                        <span class="main-grid-cell-head-container">
                                                            <span class="main-grid-head-title">Дата договора</span>
                                                        </span>
                                                    </th>
                                                    <th class="main-grid-cell-head main-grid-cell-left main-grid-col-no-sortable  main-grid-draggable">
                                                        <span class="main-grid-cell-head-container">
                                                            <span class="main-grid-head-title">Договор</span>
                                                        </span>
                                                    </th>
                                                </tr>
                                                </thead>

                                                <tbody>
                                                <?foreach ($arResult as $value):?>
                                                    <tr class="main-grid-row main-grid-row-body">
                                                        <td class="main-grid-cell main-grid-cell-left">
                                                            <span class="main-grid-cell-content">
                                                                <a href="/crm/company/show/<?=$companyID?>/"><?=$companyName?></a>
                                                            </span>
                                                        </td>
                                                        <td class="main-grid-cell main-grid-cell-left">
                                                            <span class="main-grid-cell-content"><?=($value['FIO'] != "")? $value['FIO'] : '-';?></span>
                                                        </td>
                                                        <td class="main-grid-cell main-grid-cell-left">
                                                            <span class="main-grid-cell-content">
                                                                <a href="/services/lists/40/element/0/<?=$value['DOGOVOR_ID']?>/"><?=$value['DOGOVOR_NAME']?></a>
                                                            </span>
                                                        </td>
                                                        <td class="main-grid-cell main-grid-cell-left">
                                                            <span class="main-grid-cell-content"><?=($value['DOGOVOR_COMPANY_ZAKAZCHIK'] != "")? $value['DOGOVOR_COMPANY_ZAKAZCHIK'] : '-';?></span>
                                                        </td>
                                                        <td class="main-grid-cell main-grid-cell-left">
                                                            <span class="main-grid-cell-content"><?=($value['DOGOVOR_NUMBER'] != "")? $value['DOGOVOR_NUMBER'] : '-';?></span>
                                                        </td>
                                                        <td class="main-grid-cell main-grid-cell-left">
                                                            <span class="main-grid-cell-content"><?=($value['DOGOVOR_DATE'] != "")? $value['DOGOVOR_DATE'] : '-';?></span>
                                                        </td>
                                                        <td class="main-grid-cell main-grid-cell-left">
                                                            <span class="main-grid-cell-content">
                                                                <?=($value['DOGOVOR_FILE'] != "none")? '<a href="' . $value['DOGOVOR_FILE'] . '">Скачать</a>' : '-';?>
                                                            </span>
                                                        </td>
                                                    </tr>
                                                <?endforeach;?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>