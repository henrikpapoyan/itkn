<? 
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

?>
 
<?
echo "<br />";
CModule::IncludeModule('iblock');



$res = CIBlockElement::GetByID($_POST["id"]);

while($ob = $res->GetNextElement())
	{
		 $arItem = $ob->GetFields();
		 $arItem["PROPERTIES"] = $ob->GetProperties();
		 $ar_res = $arItem;
	}
$ne_soglas_user = "";    
if ($_POST["soglas"] == "ne_soglas") {
    $ne_soglas_user = $_POST["id_user"];
    $status = "Отклонена" . " " . $ne_soglas_user;
}
if ($ar_res["PROPERTIES"]["SOGLASOVANIE_KALENDARYA"]["VALUE"] == "ne_soglas"){
    $soglas = "ne_soglas";
 
} else {
    $soglas = $_POST["soglas"];
}

if ($_POST["soglas"] == "soglas" && $ar_res["PROPERTIES"]["SOGLASOVANIE_KALENDARYA"]["VALUE"] != "ne_soglas") {
    $status = "Согласована";
} 

if ($_POST["soglas"] == "soglas" && $ar_res["PROPERTIES"]["SOGLASOVANIE_KALENDARYA"]["VALUE"] == "ne_soglas") {
    $status = $ar_res["PROPERTIES"]["STATUS"]["VALUE"];
    $ne_soglas_user = $ar_res["PROPERTIES"]["KTO_NE_SOGLASOVAL"]["VALUE"];
}
$arUpdate = Array(
  "ACTIVE" => "Y",  
  "DATE_CREATE" => $ar_res["DATE_CREATE"],
  "NAME" => $ar_res["NAME"], 
  "DETAIL_TEXT" => $ar_res["DETAIL_TEXT"], 
  "PROPERTY_VALUES" => array( 
     "ORGANIZATSIYA" => $ar_res["PROPERTIES"]["ORGANIZATSIYA"]["VALUE"],
     "PODRAZDELENIE" => $ar_res["PROPERTIES"]["PODRAZDELENIE"]["VALUE"],
     "KONTRAGENT" => $ar_res["PROPERTIES"]["KONTRAGENT"]["VALUE"],
     //"DOGOVOR" => $ar_res["PROPERTIES"]["DOGOVOR"]["VALUE"],
     "DOKUMENT" => $ar_res["PROPERTIES"]["DOKUMENT"]["VALUE"],
     "NOMER_DOKUMENTA" => $ar_res["PROPERTIES"]["NOMER_DOKUMENTA"]["VALUE"],
     "DATA_DOKUMENTA" => $ar_res["PROPERTIES"]["DATA_DOKUMENTA"]["VALUE"],
     "SUMMA_SCHETA" => $ar_res["PROPERTIES"]["SUMMA_SCHETA"]["VALUE"],
     "STATYA_ZATRAT" => $ar_res["PROPERTIES"]["STATYA_ZATRAT"]["VALUE"],
     "VID_RASKHODA" => $ar_res["PROPERTIES"]["VID_RASKHODA"]["VALUE"],
     //"SCHET_I_DRUGIE_DOKUMENTY" => $ar_res["PROPERTIES"]["SCHET_I_DRUGIE_DOKUMENTY"]["VALUE"],
     "KOMMENTARIY" => $ar_res["PROPERTIES"]["KOMMENTARIY"]["VALUE"],
     "NAZNACHENIE_PLATEZHA" => $ar_res["PROPERTIES"]["NAZNACHENIE_PLATEZHA"]["VALUE"],
     "SOGLASOVANIE_KALENDARYA" => $soglas,
     "KTO_NE_SOGLASOVAL" => $ne_soglas_user,
     "KTO_SOZDAL_ZAYAVKU" => $ar_res["PROPERTIES"]["KTO_SOZDAL_ZAYAVKU"]["VALUE"],
     "STATUS" => $status,
  ),
);  

$el = new CIBlockElement;
if ($upd = $el->Update($_POST["id"], $arUpdate)) {
    $result['status'] = 'success';
    $result['message'] = 'Элемент успешно добавлена';
} else {
    $result['status'] = 'error';
    $result['message'] = $el->LAST_ERROR;
}

$result = json_encode($result); 
?>


<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>