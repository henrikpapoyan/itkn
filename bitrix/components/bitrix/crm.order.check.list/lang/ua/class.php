<?
$MESS["CRM_PERMISSION_DENIED"] = "Доступ заборонений";
$MESS["CRM_ERROR_WRONG_ORDER_ID"] = "Невірний ідентифікатор замовлення";
$MESS["CRM_ERROR_WRONG_ORDER"] = "Замовлення не знайдене";
$MESS["CRM_COLUMN_ORDER_CHECK_ID"] = "ID";
$MESS["CRM_COLUMN_ORDER_CHECK_CASHBOX_ID"] = "Назва каси";
$MESS["CRM_COLUMN_ORDER_CHECK_TYPE"] = "Тип чека";
$MESS["CRM_COLUMN_ORDER_CHECK_STATUS"] = "Статус чека";
$MESS["CRM_COLUMN_ORDER_CHECK_ORDER_ID"] = "ID замовлення";
$MESS["CRM_COLUMN_ORDER_CHECK_PAYMENT_ID"] = "ID оплати";
$MESS["CRM_COLUMN_ORDER_CHECK_PAYMENT_DESCR"] = "Оплата";
$MESS["CRM_COLUMN_ORDER_CHECK_TITLE"] = "Документ оплати";
$MESS["CRM_COLUMN_ORDER_CHECK_SHIPMENT_ID"] = "ID відвантаження";
$MESS["CRM_COLUMN_ORDER_CHECK_SHIPMENT_DESCR"] = "Відвантаження";
$MESS["CRM_COLUMN_ORDER_CHECK_DATE_CREATE"] = "Дата створення";
$MESS["CRM_COLUMN_ORDER_CHECK_SUM"] = "Сума чека";
$MESS["CRM_COLUMN_ORDER_CHECK_LINK"] = "Посилання на чек";
$MESS["CRM_ORDER_CASHBOX_STATUS_N"] = "Не надрукований";
$MESS["CRM_ORDER_CASHBOX_STATUS_P"] = "В процесі друку";
$MESS["CRM_ORDER_CASHBOX_STATUS_Y"] = "Надрукований";
$MESS["CRM_ORDER_CASHBOX_STATUS_E"] = "Помилка";
?>