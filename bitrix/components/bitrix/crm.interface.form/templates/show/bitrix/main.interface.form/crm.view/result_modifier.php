<?php
/**
 * User: German
 * Date: 02.11.2017
 * Time: 15:27
 */
$PPParent = $this->getComponent()->getParent()->getParent()->getParent();

if($PPParent->arParams['SECTION'] == 'ORDER')
{
    unset($arResult['TABS']['tab_product_rows']);
    unset($arResult['TABS']['tab_quote']);
    unset($arResult['TABS']['tab_bizproc']);
    unset($arResult['TABS']['tab_615638']);
}
