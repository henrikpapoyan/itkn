<?php
$MESS["SPOD_ACCESS_DENIED"] = "Доступ обмежено";
$MESS["SPP_MODULE_CATALOG_NOT_INSTALL"] = "Модуль каталогу не встановлений";
$MESS["SPP_MODULE_CRM_NOT_INSTALL"] = "Модуль CRM не встановлений.";
$MESS["SPP_MODULE_DOCUMENTGENERATOR_NOT_INSTALL"] = "Модуль Генератор документів не встановлений";
$MESS["SPP_MODULE_IBLOCK_NOT_INSTALL"] = "Модуль інфоблоків не встановлено";
$MESS["SPP_MODULE_SALE_NOT_INSTALL"] = "Модуль Інтернет-магазину не встановлено";
$MESS["SPP_ORDER_NOT_FOUND"] = "Замовлення не знайдено";
$MESS["SPP_PAYMENT_NOT_FOUND"] = "Оплата не знайдена";
$MESS["SPP_PAYSYSTEM_NOT_FOUND"] = "Платіжна система не знайдена";
