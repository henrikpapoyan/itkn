<?
$MESS["MD_COMPONENT_TASKS_LIST"] = "Завдання";
$MESS["TASKS_ROLE_VIEW_ALL"] = "Всі завдання";
$MESS["TASKS_ROLE_RESPONSIBLE"] = "Мої";
$MESS["TASKS_ROLE_ACCOMPLICE"] = "Допомагаю";
$MESS["TASKS_ROLE_AUDITOR"] = "Спостерігаю";
$MESS["TASKS_ROLE_ORIGINATOR"] = "Доручив";
$MESS["MD_EMPLOYEES_TITLE"] = "Співробітники";
$MESS["MD_EMPLOYEES_ALL"] = "Всі співробітники";
$MESS["MD_GROUPS_TITLE"] = "Робочі групи";
$MESS["MD_EXTRANET"] = "Екстранет";
$MESS["MD_DISK_TABLE_FOLDERS_FILES"] = "Папок: #FOLDERS# Файлів: #FILES#";
$MESS["MD_MOBILE_APPLICATION"] = "Мобільний застосунок";
$MESS["MD_GENERATE_BY_MOBILE"] = "Згенеровано додатком";
$MESS["MD_COMPONENT_IM_RECENT"] = "Чати";
$MESS["MD_COMPONENT_IM_OPENLINES"] = "Відкриті лінії";
$MESS["MD_COMPONENT_MORE"] = "Меню";
?>