<?
$MESS["CRM_QUOTE_PRODUCT_ROWS_SAVING_ERROR"] = "Під час збереження товарів сталася помилка.";
$MESS["CRM_QUOTE_DEAULT_TITLE"] = "Нова пропозиція";
$MESS["CRM_QUOTE_NOT_FOUND"] = "Пропозицію не знайдено";
$MESS["CRM_QUOTE_ACCESS_DENIED"] = "Доступ заборонено.";
$MESS["CRM_QUOTE_DELETION_ERROR"] = "При видаленні пропозиції сталася помилка.";
?>