<?
$MESS["CRM_QUOTE_DETAIL_HISTORY_STUB"] = "Sie erstellen jetzt ein Angebot...";
$MESS["CRM_QUOTE_CONV_ACCESS_DENIED"] = "Sie brauchen Zugriffsrechte zum Erstellen von Aufträgen und Rechnungen, um fortzufahren.";
$MESS["CRM_QUOTE_CONV_GENERAL_ERROR"] = "Allgemeiner Fehler.";
$MESS["CRM_QUOTE_CONV_DIALOG_TITLE"] = "Ein Element aus dem Angebot erstellen";
$MESS["CRM_QUOTE_CONV_DIALOG_CONTINUE_BTN"] = "Fortfahren";
$MESS["CRM_QUOTE_CONV_DIALOG_CANCEL_BTN"] = "Abbrechen";
$MESS["CRM_QUOTE_CONV_DIALOG_SYNC_LEGEND"] = "Ausgewählte Elemente haben keine Felder, in denen Angebotsdaten abgespeichert werden können. Wählen Sie bitte Elemente aus, wo fehlende Felder erstellt werden, damit alle verfügbaren Informationen gespeichert werden können.";
$MESS["CRM_QUOTE_CONV_DIALOG_SYNC_FILED_LIST_TITLE"] = "Diese Felder werden erstellt";
$MESS["CRM_QUOTE_CONV_DIALOG_SYNC_ENTITY_LIST_TITLE"] = "Wählen Sie Elemente aus, wo fehlende Felder erstellt werden";
$MESS["CRM_QUOTE_EDIT_CONV_DEAL_CATEGORY_DLG_TITLE"] = "Auftragseinstellungen";
$MESS["CRM_QUOTE_EDIT_CONV_DEAL_CATEGORY_DLG_FIELD"] = "Pipeline";
$MESS["CRM_QUOTE_EDIT_BUTTON_SAVE"] = "Speichern";
$MESS["CRM_QUOTE_EDIT_BUTTON_CANCEL"] = "Abbrechen";
?>