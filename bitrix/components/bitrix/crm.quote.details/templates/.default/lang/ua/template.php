<?
$MESS["CRM_QUOTE_DETAIL_HISTORY_STUB"] = "Прямо зараз ви створюєте пропозицію...";
$MESS["CRM_QUOTE_CONV_ACCESS_DENIED"] = "Для виконання операції необхідні дозволи на створення угод та рахунків.";
$MESS["CRM_QUOTE_CONV_GENERAL_ERROR"] = "Загальна помилка";
$MESS["CRM_QUOTE_CONV_DIALOG_TITLE"] = "Створення сутності на підставі пропозиції";
$MESS["CRM_QUOTE_CONV_DIALOG_CONTINUE_BTN"] = "Продовжити";
$MESS["CRM_QUOTE_CONV_DIALOG_CANCEL_BTN"] = "Скасування";
$MESS["CRM_QUOTE_CONV_DIALOG_SYNC_LEGEND"] = "В обраних сутностях немає полів, в які можна передати дані з комерційної пропозиції. Будь ласка, виберіть сутності, в яких будуть створені відсутні поля, щоб зберегти всю доступну інформацію.";
$MESS["CRM_QUOTE_CONV_DIALOG_SYNC_FILED_LIST_TITLE"] = "Які поля будуть створені";
$MESS["CRM_QUOTE_CONV_DIALOG_SYNC_ENTITY_LIST_TITLE"] = "Виберіть сутності, в яких будуть створені відсутні поля";
$MESS["CRM_QUOTE_EDIT_CONV_DEAL_CATEGORY_DLG_TITLE"] = "Налаштування угоди";
$MESS["CRM_QUOTE_EDIT_CONV_DEAL_CATEGORY_DLG_FIELD"] = "Напрямок";
$MESS["CRM_QUOTE_EDIT_BUTTON_SAVE"] = "Зберегти";
$MESS["CRM_QUOTE_EDIT_BUTTON_CANCEL"] = "Скасування";
?>