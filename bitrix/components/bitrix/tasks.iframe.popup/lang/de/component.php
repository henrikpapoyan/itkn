<?
$MESS["TASKS_MODULE_NOT_FOUND"] = "Das Modul Aufgaben ist nicht installiert.";

$MESS["TASKS_TASK_CONFIRM_START_TIMER_TITLE"] = "Die Zeiterfassung läuft bereits in einer anderen Aufgabe";
$MESS["TASKS_TASK_CONFIRM_START_TIMER"] = "Sie haben bereits die Zeiterfassung für die Aufgabe \"{{TITLE}}\" aktiviert. Die Zeiterfassung wird auf Pause gesetzt. Fortfahren?";
?>