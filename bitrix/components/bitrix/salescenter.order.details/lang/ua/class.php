<?
$MESS["SPOD_ACCESS_DENIED"] = "Доступ обмежено";
$MESS["SPOD_CATALOG_MODULE_NOT_INSTALL"] = "Для перегляду замовлення необхідно авторизуватись.";
$MESS["SPOD_NO_ORDER"] = "Замовлення не найдене";
$MESS["SPOD_SALE_MODULE_NOT_INSTALL"] = "Модуль Інтернет-магазин не встановлено.";
$MESS["SPOD_SALE_TAX_INPRICE"] = "включений в ціну";
$MESS["SPOD_TITLE"] = "Моє замовлення №#ID#";
?>