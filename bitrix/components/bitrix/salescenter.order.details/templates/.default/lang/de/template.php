<?
$MESS["SOD_COMMON_DISCOUNT"] = "Sie sparen";
$MESS["SOD_COMMON_SUM_NEW"] = "Gesamtbetrag";
$MESS["SOD_DELIVERY"] = "Lieferung";
$MESS["SOD_FREE"] = "Kostenlos";
$MESS["SOD_SUB_ORDER_TITLE"] = "Bestellung ##ACCOUNT_NUMBER# erstellt am #DATE_ORDER_CREATE#";
$MESS["SOD_SUB_ORDER_TITLE_SHORT"] = "Bestellung ##ACCOUNT_NUMBER#";
$MESS["SOD_SUMMARY"] = "Gesamt";
$MESS["SOD_TAX"] = "Steuersatz";
$MESS["SOD_TOTAL_WEIGHT"] = "Gesamtgewicht";
$MESS["SOD_TPL_SUMOF"] = "Bestellbetrag";
?>