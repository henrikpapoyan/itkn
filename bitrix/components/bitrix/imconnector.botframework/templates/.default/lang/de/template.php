<?
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_10_STEPS_TITLE"] = "Folgen Sie diesen Schritten, um Microsoft Bot Framework mit einem Kommunikationskanal zu verbinden";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_APP_ID_NAME"] = "App-ID (Microsoft App ID):";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_APP_SECRET_NAME"] = "Geheimpasswort der App:";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_BOT_HANDLE_NAME"] = "Bot-ID (Bot handle):";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_CHANGE_ANY_TIME"] = "Kann jederzeit bearbeitet oder abgeschaltet werden";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_CONNECTED"] = "Bot-Framework verbunden";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_CONNECTOR_ERROR_STATUS"] = "Während der Arbeit Ihres Bots ist ein Fehler aufgetreten. Überprüfen Sie bitte die Parameter, dann testen Sie den Bot erneut.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_CONNECT_DESCRIPTION"] = "Füllen Sie das Formular aus, um das Microsoft Bot Framework mit Ihrem Kommunikationskanal zu verbinden";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_CONNECT_HELP"] = "<div class=\"imconnector-field-button-subtitle\">Ich möchte</div><div class=\"imconnector-field-button-name\">einen Bot <span class=\"imconnector-field-box-text-bold\">verbinden</span></div>";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_COPY"] = "Kopieren";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_CREATE_NEW_BOT"] = "Einen neuen Bot erstellen";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_FORM_SETTINGS"] = "Ich habe bereits einen Bot";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_GET_LINKS"] = "Links auf der Website von Microsoft anfordern";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_INDEX_DESCRIPTION"] = "Verbinden Sie Microsoft Bot Framework mit einem Kommunikationskanal, um Nachrichten von Ihren Kunden via Skype oder auch andere unterstütze Quellen (Slack, Kik, GroupMe, SMS, E-Mail etc.) direkt in Ihrem Bitrix24 zu erhalten.<br><br>
Wir werden Ihnen helfen, einen Bot zu erstellen und es mit Ihrem Bitrix24 zu verbinden.
";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_INFO"] = "Information";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_INFO_CONNECT_ID"] = "redirect=detail&code=3601867";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_INSTRUCTION_TITLE"] = "<span class=\"imconnector-field-box-text-bold\">Anleitung der Botverbindung</span>:";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_LINKS_CHANNELS_COMMUNICATION_DESCRIPTION"] = "Sie können öffentliche Links zu den Kanälen angeben, die Sie mit Microsoft verknüpft haben, damit diese Links im Widget (Live-Chat) auf der Website angezeigt werden.
<br><br>
Um die Links anzufordern, wechseln Sie in #LINK_BEGIN#Ihren Microsoft Bot Framework Account#LINK_END# und klicken Sie auf \"Get bot embed codes\".
";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_LINKS_CHANNELS_COMMUNICATION_DESCRIPTION_MASTER"] = "Sie können öffentliche Links zu den Kanälen angeben, die Sie mit Microsoft verknüpft haben, damit diese Links im Widget (Live-Chat) auf der Website im #LINK_BEGIN#Formular mit Kanaleinstellungen#LINK_END# angezeigt werden.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_LINKS_CHANNELS_COMMUNICATION_TITLE"] = "Links zu Kommunikationskanälen angeben";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_SIMPLE_FORM_DESCRIPTION"] = "Geben Sie diese Adresse im Feld Messaging Webhook auf der Website von Microsoft, auf der Seite der Bot-Konfiguration an:";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_SIMPLE_FORM_DESCRIPTION_1"] = "Wenn Ihr Bot bereits registriert wurde, verbinden Sie ihn mit Bitrix24.<br><br>
Geben Sie diese Adresse im Feld \"Messaging Webhook\" auf der Website von Microsoft an:
";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_SIMPLE_FORM_DESCRIPTION_TESTED"] = "Sie können jetzt überprüfen, ob alle angegebenen Daten korrekt sind.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_TESTED"] = "Testverbindung";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_CHANNELS_DESCRIPTION"] = "In Ihrem #LINK_BEGIN#Microsoft Bot Framework Account#LINK_END# kopieren SieLinks zu den Kanälen, die Sie für Ihre Kunden im Live-Chat verfügbar machen möchten, dann fügen Sie diese Links unten ein.<br>
Beachten Sie, dass nur ein Teil des Links kopiert werden soll. Sie finden hier Beispiele dafür, was Sie kopieren und dann einfügen müssen.
";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_EMAILOFFICE365_EXAMPLE"] = "&lt;a href=\"mailto:<strong>pokoev@bitrix.onmicrosoft.com</strong>\"&gt;pokoev@bitrix.onmicrosoft.com&lt;/a&gt;";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_EMAILOFFICE365_PLACEHOLDER"] = "pokoev@bitrix.onmicrosoft.com";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_FACEBOOKMESSENGER_EXAMPLE"] = "&lt;a href='<strong>https://www.messenger.com/t/1343073279053039</strong>'&gt;&lt;img src='https://facebook.botframework.com/Content/MessageUs.png'&gt;&lt;/a&gt;";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_FACEBOOKMESSENGER_PLACEHOLDER"] = "https://www.messenger.com/t/1343073279053039";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_GROUPME_EXAMPLE"] = "&lt;a href='<strong>https://groupme.botframework.com/?botId=bitrix</strong>'&gt;@bitrix&lt;/a&gt;";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_GROUPME_PLACEHOLDER"] = "https://groupme.botframework.com/?botId=bitrix";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_KIK_EXAMPLE"] = "&lt;a href='<strong>https://bots.kik.com/#/bitrix</strong>'&gt;bitrix&lt;/a&gt;";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_KIK_PLACEHOLDER"] = "https://bots.kik.com/#/testbitrix";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_MSTEAMS_EXAMPLE"] = "&lt;a href='<strong>https://teams.microsoft.com/l/chat/0/0?users=28:0c237a9f-e52b-4bbd-92ed-d5a5a850c892</strong>'&gt;&lt;img height=\"30\" width=\"113\" src='https://dev.botframework.com/Client/Images/Add-To-MSTeams-Buttons.png'&gt;&lt;/a&gt;";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_MSTEAMS_PLACEHOLDER"] = "https://teams.microsoft.com/l/chat/0/0?users=28:0c237a9f-e52b-4bbd-92ed-d5a5a850c892";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_SKYPEBOT_EXAMPLE"] = "&lt;a href='<strong>https://join.skype.com/bot/0c237a9f-e52b-4bbd-92ed-d5a5a850c892</strong>'&gt;&lt;img src='https://dev.botframework.com/Client/Images/Add-To-Skype-Buttons.png'/&gt;&lt;/a&gt;";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_SKYPEBOT_PLACEHOLDER"] = "https://join.skype.com/bot/0c237a9f-e52b-4bbd-92ed-d5a5a850c892";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_SLACK_EXAMPLE"] = "&lt;a href=\"<strong>https://slack.com/oauth/authorize?scope=bot&client_id=58933734192.70978425654 &redirect_uri=https%3a%2f%2fslack.botframework.com%2fHome%2fauth&state=bitrix</strong>\"&gt;&lt;img height=\"40\" width=\"139\" src=\"https://platform.slack-edge.com/img/add_to_slack.png\" srcset=\"https://platform.slack-edge.com/img/add_to_slack.png 1x, https://platform.slack-edge.com/img/add_to_slack@2x.png 2x\"&gt;&lt;/a&gt;";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_SLACK_PLACEHOLDER"] = "https://slack.com/oauth/authorize?scope=bot&client_id=58933734192.70978425654&redirect_uri=https%3a%2f%2fslack.botframework.com%2fHome%2fauth&state=bitrix";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_TELEGRAM_EXAMPLE"] = "&lt;a href='<strong>https://telegram.me/pokoevBot</strong>'&gt;@pokoevBot&lt;/a&gt;";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_TELEGRAM_PLACEHOLDER"] = "https://telegram.me/pokoevBot";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_TWILIO_EXAMPLE"] = "&lt;a href=\"tel:<strong>+15615155866</strong>\"&gt;(561) 515-5866&lt;/a&gt;";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_TWILIO_PLACEHOLDER"] = "+15615155866";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_WEBCHAT_EXAMPLE"] = "Link anfordern:<br>
&lt;iframe src=\"<strong>https://webchat.botframework.com/embed/bitrix?s=</strong>YOUR_SECRET_HERE\" style=\"height: 502px; max-height: 502px;\"&gt;&lt;/iframe&gt;<br>Ersetzen Sie YOUR_SECRET_HERE durch die Inhalte aus dem Feld \"Secret\".
";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_WEBCHAT_PLACEHOLDER"] = "https://webchat.botframework.com/embed/bitrix?s=SZfvj3Wdqrw.cwA.7RE.iiQcU8IlUJsYFASNYXusu3BmAsHitiyvVSH4A0xjTF0";
$MESS["IMCONNECTOR_COMPONENT_FINAL_FORM_DESCRIPTION_OK_1"] = "Microsoft Bot Framework wurde mit Ihrem Kommunikationskanal verbunden.
Ab sofort werden alle Nachrichten, die an Ihren Bot gesendet werden, auch an Ihr Bitrix24 weitergeleitet.
";
$MESS["IMCONNECTOR_COMPONENT_FINAL_FORM_DESCRIPTION_OK_2"] = "Sie können andere Kommunikationsquellen mit Ihrem Bot verbinden oder die existierenden Quellen so einstellen, dass alle Nachrichten an den Chat weitergeleitet werden.";
?>