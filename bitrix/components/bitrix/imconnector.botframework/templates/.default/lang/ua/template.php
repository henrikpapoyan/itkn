<?
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_INSTRUCTION_TITLE"] = "<span class=\"imconnector-field-box-text-bold\">Інструкція</span> по підключенню бота:";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_CONNECT_HELP"] = "<div class=\"imconnector-field-button-subtitle\">Я хочу</div><div class=\"imconnector-field-button-name\"><span class=\"imconnector-field-box-text-bold\">Підключити</span> бота</div>";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_INFO"] = "Інформація";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_CONNECTED"] = "Botframework підключений";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_CHANGE_ANY_TIME"] = "Ви можете в будь-який час змінити або відключити";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_CONNECT_DESCRIPTION"] = "Щоб підключити Microsoft Bot Framework до Відкритої лінії, необхідно заповнити дані форми";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_SIMPLE_FORM_DESCRIPTION"] = "На стороні налаштувань бота на сайті Microsoft в полі Messaging Webhook вкажіть наступну адресу:";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_BOT_HANDLE_NAME"] = "Ідентифікатор бота (Bot handle):";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_APP_ID_NAME"] = "ID програми (Microsoft App ID):";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_APP_SECRET_NAME"] = "Секретний пароль додатка:";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_TESTED"] = "Тестування з'єднання";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_INDEX_DESCRIPTION"] = "Підключіть Microsoft Bot Framework до Відкритої лінії щоб приймати звернення ваших клієнтів зі Skype та інших каналів, які підтримує бот (Slack, Kik, GroupMe, SMS, email та ін) у вашому Бітрікс24.<br><br>
Ми допоможемо Вам створити бота в кілька кроків і підключити його до вашого Бітрікс24.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_CREATE_NEW_BOT"] = "Створити нового бота";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_FORM_SETTINGS"] = "Є бот, заповнити форму";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_CONNECTOR_ERROR_STATUS"] = "У процесі роботи вашого бота виникла помилка. Вам необхідно перевірити зазначені дані та протестувати бота повторно.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_GET_LINKS"] = "Отримати посилання на сайті Microsoft";
$MESS["IMCONNECTOR_COMPONENT_FINAL_FORM_DESCRIPTION_OK_1"] = "Microsoft Bot Framework успішно підключений до вашої Відкритої лінії.
Тепер всі звернення до вашого боту будуть автоматично спрямовані на ваш Бітрікс24.";
$MESS["IMCONNECTOR_COMPONENT_FINAL_FORM_DESCRIPTION_OK_2"] = "Ви можете підключити до боту інші канали комунікацій або провести налаштування підключених, щоб збирати всі повідомлення в єдиний чат.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_10_STEPS_TITLE"] = "Щоб підключити Microsoft Bot Framework до Відкритої лінії, необхідно виконати кілька кроків";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_SIMPLE_FORM_DESCRIPTION_1"] = "Якщо ви вже реєстрували бота, то залишилося підключити його до Бітрікс24<br><br>
На стороні налаштувань бота на сайті Microsoft в поле Messaging Webhook вкажіть наступну адресу:";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_SIMPLE_FORM_DESCRIPTION_TESTED"] = "Ви можете перевірити коректність введених даних.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_COPY"] = "Копіювати";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_LINKS_CHANNELS_COMMUNICATION_DESCRIPTION"] = "Ви можете вказати публічні посилання на канали, які ви підключили на стороні Microsoft, щоб вони відображалися у віджеті (онлайн-чаті) на сайті.
<br><br>
Отримати посилання в #LINK_BEGIN#особистому кабінеті Microsoft Bot Framework#LINK_END# за посиланням \"Get bot embed codes\"";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_LINKS_CHANNELS_COMMUNICATION_TITLE"] = "вказати посилання на канали комунікацій";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_CHANNELS_DESCRIPTION"] = "В #LINK_BEGIN#особистому кабінеті Microsoft Bot Framework#LINK_END# вам необхідно скопіювати посилання на ті канали, які ви хочете показати клієнтам в онлайн-чаті і вставити посилання в поля нижче.
<br><br>
Зверніть увагу, що потрібно копіювати тільки частину посилання. Для кожного каналу ми навели приклади що саме необхідно скопіювати і вставити в поле.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_LINKS_CHANNELS_COMMUNICATION_DESCRIPTION_MASTER"] = "Ви можете вказати публічні посилання на канали, які ви підключили на стороні Microsoft, щоб вони відображалися у віджеті (онлайн-чаті) на сайті в #LINK_BEGIN#формі налаштування каналу#LINK_END#.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_WEBCHAT_EXAMPLE"] = "Потрібно взяти посилання:<br>
&lt;iframe src=\"<strong>https://webchat.botframework.com/embed/bitrix?s=</strong>YOUR_SECRET_HERE\" style=\"height: 502px; max-height: 502px;\"&gt;&lt;/iframe&gt;<br>А замість YOUR_SECRET_HERE вставити код з поля \"Secret\".";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_EMAILOFFICE365_PLACEHOLDER"] = "pokoev@bitrix.onmicrosoft.com";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_TWILIO_PLACEHOLDER"] = "15615155866";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_SKYPEBOT_PLACEHOLDER"] = "https://join.skype.com/bot/0c237a9f-e52b-4bbd-92ed-d5a5a850c892";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_SKYPEBOT_EXAMPLE"] = "&lt;a href='<strong>https://join.skype.com/bot/0c237a9f-e52b-4bbd-92ed-d5a5a850c892</strong>'&gt;&lt;img src='https://dev.botframework.com/Client/Images/Add-To-Skype-Buttons.png'/&gt;&lt;/a&gt;";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_SLACK_PLACEHOLDER"] = "https://slack.com/oauth/authorize?scope=bot&client_id=58933734192.70978425654&redirect_uri=https%3a%2f%2fslack.botframework.com%2fHome%2fauth&state=bitrix";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_SLACK_EXAMPLE"] = "&lt;a href=\"<strong>https://slack.com/oauth/authorize?scope=bot&client_id=58933734192.70978425654 &redirect_uri=https%3a%2f%2fslack.botframework.com%2fHome%2fauth&state=bitrix</strong>\"&gt;&lt;img height=\"40\" width=\"139\" src=\"https://platform.slack-edge.com/img/add_to_slack.png\" srcset=\"https://platform.slack-edge.com/img/add_to_slack.png 1x, https://platform.slack-edge.com/img/add_to_slack@2x.png 2x\"&gt;&lt;/a&gt;";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_KIK_PLACEHOLDER"] = "https://bots.kik.com/#/testbitrix";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_KIK_EXAMPLE"] = "&lt;a href='<strong>https://bots.kik.com/#/bitrix</strong>'&gt;bitrix&lt;/a&gt;";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_GROUPME_PLACEHOLDER"] = "https://groupme.botframework.com/?botId=bitrix";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_GROUPME_EXAMPLE"] = "&lt;a href='<strong>https://groupme.botframework.com/?botId=bitrix</strong>'&gt;@bitrix&lt;/a&gt;";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_TWILIO_EXAMPLE"] = "&lt;a href=\"tel:<strong>+15615155866</strong>\"&gt;(561) 515-5866&lt;/a&gt;";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_MSTEAMS_PLACEHOLDER"] = "https://teams.microsoft.com/l/chat/0/0?users=28:0c237a9f-e52b-4bbd-92ed-d5a5a850c892";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_MSTEAMS_EXAMPLE"] = "&lt;a href='<strong>https://teams.microsoft.com/l/chat/0/0?users=28:0c237a9f-e52b-4bbd-92ed-d5a5a850c892</strong>'&gt;&lt;img height=\"30\" width=\"113\" src='https://dev.botframework.com/Client/Images/Add-To-MSTeams-Buttons.png'&gt;&lt;/a&gt;";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_WEBCHAT_PLACEHOLDER"] = "https://webchat.botframework.com/embed/bitrix?s=SZfvj3Wdqrw.cwA.7RE.iiQcU8IlUJsYFASNYXusu3BmAsHitiyvVSH4A0xjTF0";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_EMAILOFFICE365_EXAMPLE"] = "&lt;a href=\"mailto:<strong>pokoev@bitrix.onmicrosoft.com</strong>\"&gt;pokoev@bitrix.onmicrosoft.com&lt;/a&gt;";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_TELEGRAM_PLACEHOLDER"] = "https://telegram.me/pokoevBot";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_TELEGRAM_EXAMPLE"] = "&lt;a href='<strong>https://telegram.me/pokoevBot</strong>'&gt;@pokoevBot&lt;/a&gt;";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_FACEBOOKMESSENGER_PLACEHOLDER"] = "https://www.messenger.com/t/1343073279053039";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_FACEBOOKMESSENGER_EXAMPLE"] = "&lt;a href='<strong>https://www.messenger.com/t/1343073279053039</strong>'&gt;&lt;img src='https://facebook.botframework.com/Content/MessageUs.png'&gt;&lt;/a&gt;";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_INFO_CONNECT_ID"] = "redirect=detail&code=3601867";
?>