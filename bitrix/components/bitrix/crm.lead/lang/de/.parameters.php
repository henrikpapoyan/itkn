<?
$MESS["CRM_LEAD_VAR"] = "Name der Variablen der Lead-ID";
$MESS["CRM_SEF_PATH_TO_INDEX"] = "Vorlage des Pfads zur Indexseite";
$MESS["CRM_SEF_PATH_TO_LIST"] = "Vorlage des Pfads zur Seite mit Leads";
$MESS["CRM_SEF_PATH_TO_EDIT"] = "Vorlage des Pfads zur Seite der Leadbearbeitung";
$MESS["CRM_SEF_PATH_TO_SHOW"] = "Vorlage des Pfads zur Seite der Leadansicht";
$MESS["CRM_SEF_PATH_TO_CONVERT"] = "Vorlage des Pfads zur Seite der Leadkonvertierung";
$MESS["CRM_ELEMENT_ID"] = "Lead-ID";
$MESS["CRM_SEF_PATH_TO_SERVICE"] = "Vorlage des Pfads zur Seite des Web-Services";
$MESS["CRM_SEF_PATH_TO_IMPORT"] = "Vorlage des Pfads zur Seite des Leadimports";
$MESS["CRM_NAME_TEMPLATE"] = "Namensformat";
?>