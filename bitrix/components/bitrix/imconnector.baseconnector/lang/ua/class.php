<?
$MESS["IMCONNECTOR_COMPONENT_BASECONNECTOR_MODULE_NOT_INSTALLED"] = "Модуль коннекторів месенджерів не встановлено";
$MESS["IMCONNECTOR_COMPONENT_BASECONNECTOR_NO_ACTIVE_CONNECTOR"] = "Даний коннектор не активний";
$MESS["IMCONNECTOR_COMPONENT_BASECONNECTOR_SESSION_HAS_EXPIRED"] = "Ваша сесія закінчилася. Надішліть форму ще раз";
$MESS["IMCONNECTOR_COMPONENT_BASECONNECTOR_CONNECTOR_ERROR_STATUS"] = "В ході роботи сталася помилка. Будь ласка, перевірте налаштування.";
?>