<?
$MESS["IMOL_MAIL_DONT_REPLY_NEW"] = "Die E-Mail-Nachricht wurde vom System gesendet. Bitte antworten Sie nicht darauf.";
$MESS["IMOL_MAIL_MESSAGE_SYSTEM"] = "Systemnachricht";
$MESS["IMOL_MAIL_FORMAT_ERROR"] = "Fehler beim Erstellen der Nachricht.";
$MESS["IMOL_MAIL_WRITE_TO_LINE"] = "Erneut kontaktieren";
$MESS["IMOL_MAIL_BACK_TO_TALK"] = "Konversation fortsetzen";
?>