<?
$MESS["INTRANET_USER_PROFILE_SETTINGS_CANCEL"] = "Abbrechen";
$MESS["INTRANET_USER_PROFILE_SETTINGS_FIELDS_EDIT"] = "Editierbare Felder";
$MESS["INTRANET_USER_PROFILE_SETTINGS_FIELDS_VIEW"] = "Sichtbare Felder";
$MESS["INTRANET_USER_PROFILE_SETTINGS_SAVE"] = "Speichern";
$MESS["INTRANET_USER_PROFILE_SETTINGS_TITLE"] = "Feldeinstellungen";
?>