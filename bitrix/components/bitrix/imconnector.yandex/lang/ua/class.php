<?
$MESS["IMCONNECTOR_COMPONENT_YANDEX_MODULE_NOT_INSTALLED"] = "Модуль конекторів мессенджерів не встановлений";
$MESS["IMCONNECTOR_COMPONENT_YANDEX_NO_ACTIVE_CONNECTOR"] = "Даний конектор не активний";
$MESS["IMCONNECTOR_COMPONENT_YANDEX_SESSION_HAS_EXPIRED"] = "Ваша сесія закінчилася. Відправте форму ще раз";
$MESS["IMCONNECTOR_COMPONENT_YANDEX_NO_CONNECT"] = "Тестове з'єднання з вказаними даними не вдалося здійснити";
$MESS["IMCONNECTOR_COMPONENT_YANDEX_NO_REGISTER"] = "Під час реєстрації сталася помилка";
?>