<?
$MESS["IMCONNECTOR_COMPONENT_YANDEX_MODULE_NOT_INSTALLED"] = "Das Modul \"Externe IM-Connectors\" ist nicht installiert.";
$MESS["IMCONNECTOR_COMPONENT_YANDEX_NO_ACTIVE_CONNECTOR"] = "Dieser Connector ist nicht aktiv.";
$MESS["IMCONNECTOR_COMPONENT_YANDEX_SESSION_HAS_EXPIRED"] = "Ihre Sitzung ist abgelaufen. Bitte senden Sie das Formular erneut.";
$MESS["IMCONNECTOR_COMPONENT_YANDEX_NO_CONNECT"] = "Eine Testverbindung kann mit angegebenen Parametern nicht hergestellt werden";
$MESS["IMCONNECTOR_COMPONENT_YANDEX_NO_REGISTER"] = "Registrierungsfehler";
?>