<?
$MESS["IMCONNECTOR_COMPONENT_YANDEX_ID_CHAT"] = "ID Каналу (Ідентифікатор каналу, який потрібно транслювати в чат):";
$MESS["IMCONNECTOR_COMPONENT_YANDEX_ACTIVATE"] = "Активувати";
$MESS["IMCONNECTOR_COMPONENT_YANDEX_FINAL_FORM_DESCRIPTION"] = "Яндекс Чат успішно підключений до вашої Відкритої лінії. Тепер ви можете налаштувати підключення <a href=\"#URL#\" target=\"_blank\">в особистому кабінеті Яндекс Діалогів</a> згідно з <a onclick=\"top.BX.Helper.show('#ID#');\" class=\"imconnector-field-box-link\">інструкцією</a>.";
$MESS["IMCONNECTOR_COMPONENT_YANDEX_CONNECTED"] = "Яндекс.Чат підключений";
$MESS["IMCONNECTOR_COMPONENT_YANDEX_CHANGE_ANY_TIME"] = "Ви можете в будь-який час змінити або відключити";
$MESS["IMCONNECTOR_COMPONENT_YANDEX_CONNECT_STEP"] = "Для підключення необхідно активувати канал і виконати підключення в <a href=\"#URL#\" target=\"_blank\">Яндекс Діалогах</a> згідно з <a onclick=\"top.BX.Helper.show('#ID#');\" class=\"imconnector-field-box-link\">інструкцією</a>.";
$MESS["IMCONNECTOR_COMPONENT_YANDEX_INFO_CONNECT_ID"] = "redirect=detail&code=7837755";
$MESS["IMCONNECTOR_COMPONENT_YANDEX_INFO_CONNECT_URL"] = "https://dialogs.yandex.ru/developer";
$MESS["IMCONNECTOR_COMPONENT_YANDEX_COPY"] = "копіювати";
?>