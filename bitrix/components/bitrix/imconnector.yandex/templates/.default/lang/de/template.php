<?
$MESS["IMCONNECTOR_COMPONENT_YANDEX_ID_CHAT"] = "ID des Kanals (die ID, in einen Chat übermittelt werden soll):";
$MESS["IMCONNECTOR_COMPONENT_YANDEX_ACTIVATE"] = "Aktivieren";
$MESS["IMCONNECTOR_COMPONENT_YANDEX_FINAL_FORM_DESCRIPTION"] = "Yandex Chat wurde mit Ihrem Kommunikationskanal verbunden. Um die Verbindung zu konfigurieren, nutzen Sie Ihr <a href=\"#URL#\" target=\"_blank\">Profil der Yandex Konversationen</a>. Folgen Sie der Konfugurationsanleitung <a onclick=\"top.BX.Helper.show('#ID#');\" class=\"imconnector-field-box-link\">hier</a>.";
$MESS["IMCONNECTOR_COMPONENT_YANDEX_CONNECTED"] = "Yandex Chat verbunden";
$MESS["IMCONNECTOR_COMPONENT_YANDEX_CHANGE_ANY_TIME"] = "Sie können ihn jederzeit bearbeiten oder die Verbindung löschen";
$MESS["IMCONNECTOR_COMPONENT_YANDEX_CONNECT_STEP"] = "Bitte aktivieren Sie zuerst den Kanal. Um Yandex Chat zu verbinden, nutzen Sie die <a href=\"#URL#\" target=\"_blank\">Seite der Yandex Konversationen</a>. Folgen Sie der Verbindungsanleitung <a onclick=\"top.BX.Helper.show('#ID#');\" class=\"imconnector-field-box-link\">hier</a>.";
$MESS["IMCONNECTOR_COMPONENT_YANDEX_INFO_CONNECT_ID"] = "redirect=detail&code=7837755";
$MESS["IMCONNECTOR_COMPONENT_YANDEX_INFO_CONNECT_URL"] = "https://dialogs.yandex.ru/developer";
$MESS["IMCONNECTOR_COMPONENT_YANDEX_COPY"] = "kopieren";
?>