<?
$MESS["CRM_PRODUCT_LRP_DLG_BTN_START"] = "Starten";
$MESS["CRM_PRODUCT_LRP_DLG_BTN_STOP"] = "Stoppen";
$MESS["CRM_PRODUCT_LRP_DLG_BTN_CLOSE"] = "Schließen";
$MESS["CRM_PRODUCT_LRP_DLG_WAIT"] = "Bitte warten...";
$MESS["CRM_PRODUCT_LRP_DLG_REQUEST_ERR"] = "Verarbeitung der Anfrage ist fehlgeschlagen.";
?>