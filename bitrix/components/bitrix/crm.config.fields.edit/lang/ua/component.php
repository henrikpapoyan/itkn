<?
$MESS["CC_BLFE_BAD_FIELD_NAME"] = "Не введено назву.";
$MESS["CC_BLFE_BAD_FIELD_NAME_LANG"] = "Будь ласка, встановіть назву поля на мові  #LANG_NAME#.";
$MESS["CC_BLFE_CHAIN_FIELDS"] = "Налаштування полів";
$MESS["CC_BLFE_CHAIN_LIST_EDIT"] = "Налаштування списку";
$MESS["CC_BLFE_ERR_IBLOCK_ELEMENT_BAD_IBLOCK_ID"] = "Будь ласка, оберіть інфоблок для прив'язки.";
$MESS["CC_BLFE_FIELD_NAME_DEFAULT"] = "Нове поле";
$MESS["CC_BLFE_TITLE_EDIT"] = "Налаштування поля: #NAME#";
$MESS["CC_BLFE_TITLE_NEW"] = "Нове поле";
$MESS["CC_BLFE_WRONG_LINK_IBLOCK"] = "Вказано невірний список для властивості типу «Прив'язка».";
$MESS["CRM_FIELDS_EDIT_NAME_DEFAULT"] = "Нове поле";
$MESS["CRM_FIELDS_EDIT_TITLE_EDIT"] = "Налаштування поля: #NAME#";
$MESS["CRM_FIELDS_EDIT_WRONG_FIELD"] = "Зазначений невірний ID поля.";
$MESS["CRM_FIELDS_ENTITY_LIST"] = "Список типів";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Модуль CRM не встановлений.";
$MESS["CRM_PERMISSION_DENIED"] = "Доступ заборонений";
?>