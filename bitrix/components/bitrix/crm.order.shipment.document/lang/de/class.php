<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Das Modul CRM ist nicht installiert.";
$MESS["CRM_ORDER_SHIPMENT_DOCUMENT_TITLE"] = "Auslieferungsdokumente";
$MESS["CRM_ORDER_SHIPMENT_DOCUMENT_SUBTITLE"] = "Informationen zur Auslieferung";
$MESS["CRM_ORDER_SHIPMENT_FIELD_TRACKING_NUMBER"] = "Nummer zum Verfolgen";
$MESS["CRM_ORDER_SHIPMENT_FIELD_DELIVERY_DOC_NUM"] = "Auslieferungsdokument Nr.";
$MESS["CRM_ORDER_SHIPMENT_FIELD_DELIVERY_DOC_DATE"] = "Datum des Auslieferungsdokuments";
?>