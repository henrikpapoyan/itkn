<?
$MESS["LICENSE_RESTR_TITLE"] = "Порушення умов ліцензійної угоди";
$MESS["LICENSE_RESTR_TEXT"] = "На жаль, ви перевищили максимальну кількість співробітників у вашій Бітрікс24.CRM";
$MESS["LICENSE_RESTR_TEXT2"] = "За<a href=\"https://www.1c-bitrix.ua/legal/\" target=\"_blank\" class=\"intranet-license-restriction-link\">ліцензійною угодою</a> вам дозволено додати <span class=\"intranet-license-restriction-text-bold\">не більше #NUM# активних співробітників</span>.";
$MESS["LICENSE_RESTR_TEXT3"] = "Зараз у вашій Бітрікс24.CRM співробітників: <span class=\"intranet-license-restriction-count\">#NUM#</span>";
$MESS["LICENSE_RESTR_TEXT4"] = "Щоб це нагадування не показувалося знову, зверніться до адміністратора вашого Бітрікс24 або керівника.";
?>