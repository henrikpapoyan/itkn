<?
$MESS["LICENSE_RESTR_TITLE"] = "Verstoß gegen EULA-Bedingungen";
$MESS["LICENSE_RESTR_TEXT"] = "Sie haben leider die maximale Anzahl der Nutzer überschritten, die in Ihrer Edition Bitrix24.CRM erlaubt ist.";
$MESS["LICENSE_RESTR_TEXT2"] = "Entsprechend dem <a href=\"https://www.bitrix24.de/eula/\" target=\"_blank\" class=\"intranet-license-restriction-link\">EULA</a> können Sie <span class=\"intranet-license-restriction-text-bold\">bis zu #NUM# aktive Nutzer</span> hinzufügen.";
$MESS["LICENSE_RESTR_TEXT3"] = "Ihre aktuelle Edition Bitrix24.CRM enthält <span class=\"intranet-license-restriction-count\">#NUM#</span> Nutzer";
$MESS["LICENSE_RESTR_TEXT4"] = "Wenden Sie sich an Ihren Bitrix24 Administrator, wenn Sie diese Benachrichtigung ausblenden möchten.";
?>