<?
$MESS["TASKS_TL_SOCIALNETWORK_MODULE_NOT_INSTALLED"] = "Das Modul \"Soziales Netzwerk\" ist nicht installiert.";
$MESS["TASKS_TL_FORUM_MODULE_NOT_INSTALLED"] = "Das Modul \"Forum\" ist nicht installiert.";
$MESS["TASKS_TL_ACCESS_TO_GROUP_DENIED"] = "Sie können die Liste der Aufgaben für diese Gruppe nicht anzeigen.";
$MESS["TASKS_GROUP_ACTION_DAYS_NUM_INVALID_TITLE"] = "Ungültiger Eintrag";
$MESS["TASKS_GROUP_ACTION_DAYS_NUM_INVALID_TEXT"] = "Dieses Feld kann nur Zahlen enthalten.";
$MESS["TASKS_GROUP_ACTION_ERROR_TITLE"] = "Es sind Fehler aufgetreten bei Gruppenaktualisierung von Aufgaben";
$MESS["TASKS_GROUP_ACTION_ERROR_MESSAGE"] = "\"#MESSAGE#\" in folgenden Aufgaben: #TASK_IDS#";
?>