<?
$MESS["CRM_ORDER_VAR"] = "Name der Variablen der Bestell-ID";
$MESS["CRM_SEF_PATH_TO_INDEX"] = "Pfad zur Indexseite";
$MESS["CRM_SEF_PATH_TO_LIST"] = "Pfad zur Seite der Bestellungen";
$MESS["CRM_SEF_PATH_TO_EDIT"] = "Pfad zur Seite der Bestellbearbeitung";
$MESS["CRM_SEF_PATH_TO_SHOW"] = "Pfad zur Seite der Bestellansicht";
$MESS["CRM_SEF_PATH_TO_IMPORT"] = "Pfad zur Importseite";
$MESS["CRM_ELEMENT_ID"] = "Bestell-ID";
$MESS["CRM_NAME_TEMPLATE"] = "Namensformat";
?>