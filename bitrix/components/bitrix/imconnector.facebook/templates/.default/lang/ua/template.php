<?
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_AUTHORIZATION"] = "Авторизація";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_AUTHORIZE"] = "Авторизуватись";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_CHANGE_ANY_TIME"] = "Ви можете в будь-який час змінити або відключити";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_CHANGE_PAGE"] = "Змінити";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_CONNECTED"] = "Facebook підключений";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_CONNECTED_PAGE"] = "Підключена сторінка";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_CONNECTOR_ERROR_STATUS"] = "Під час роботи сталася помилка. Будь ласка, перевірте налаштування.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_DEL_REFERENCE"] = "Видалити прив'язку";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_DESCRIPTION"] = "<p class=\"im-connector-settings-header-description\">Підключіть публічну сторінку Facebook вашої компанії до відкритої лінії та спілкуйтеся з клієнтами в звичному чаті Бітрікс24.</p>
<ul class=\"im-connector-settings-header-list\">
<li class=\"im-connector-settings-header-list-item\">миттєві комунікації з відвідувачами вашої сторінки</li>
<li class=\"im-connector-settings-header-list-item\">автоматичний розподіл звернень за правилами черги</li>
<li class=\"im-connector-settings-header-list-item\">спілкування в звичному робочому чаті Бітрікс24</li>
 <li class=\"im-connector-settings-header-list-item\">автоматичне збереження клієнтів в CRM</li>
</ul>";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_EXPIRED_ACCOUNT_TOKEN"] = "Якщо необхідно внести зміни, авторизуйтеся під своїм акаунтом Facebook";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_EXPIRED_ACCOUNT_TOKEN_WARNING"] = "Зверніть увагу! Якщо ви авторизуєтесь під іншим акаунтом, до якого не прив'язана поточна сторінка, то вона буде відключена від відкритої лінії Бітрікс24";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_INDEX_DESCRIPTION"] = "Підключіть публічну сторінку Facebook вашої компанії до відкритої лінії та спілкуйтеся з клієнтами в звичному чаті Бітрікс24. Для підключення необхідно створити публічну сторінку Facebook або підключити вже існуючу. Ви повинні бути адміністратором цієї сторінки.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_INFO"] = "Інформація";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_LOG_IN_UNDER_AN_ADMINISTRATOR_ACCOUNT_PAGE"] = "Авторизуйтесь під акаунтом, який є адміністратором потрібної сторінки Facebook, щоб приймати повідомлення від ваших клієнтів усередині Бітрікс24";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_NO_SPECIFIC_PAGE"] = "Немає потрібної сторінки? Можливі причини описані #A# у статті. #A_END#";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_OTHER_PAGES"] = "Інші сторінки";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_PAGE"] = "Сторінка";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_PAGE_IM"] = "Мессенджер";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_SELECT_THE_PAGE"] = "Виберіть публічну сторінку Facebook, яку необхідно підключити до Відкритої лінії Бітрікс24";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_THERE_IS_NO_PAGE_WHERE_THE_ADMINISTRATOR"] = "У вас поки немає сторінок Facebook, де ви є адміністратором.<br>Ви можете створити свою сторінку прямо зараз.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_TITLE"] = "Відповідайте на звернення клієнтів Facebook в робочому чаті Бітрікс24";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_TO_CREATE_A_PAGE"] = "Створити";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_USER"] = "Акаунт";
?>