<?
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_AUTHORIZATION"] = "Einloggen";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_AUTHORIZE"] = "Einloggen";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_CHANGE_ANY_TIME"] = "Kann jederzeit bearbeitet oder abgeschaltet werden";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_CHANGE_PAGE"] = "Ändern";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_CONNECTED"] = "Facebook verbunden";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_CONNECTED_PAGE"] = "Verbundene Seite";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_CONNECTOR_ERROR_STATUS"] = "Ein Fehler ist aufgetreten. Überprüfen Sie Ihre Einstellungen.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_DEL_REFERENCE"] = "Verknüpfung löschen";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_DESCRIPTION"] = "<p class=\"im-connector-settings-header-description\">Verknüpfen Sie die Facebook-Seite Ihres Unternehmens mit Kommunikationskanälen und kommunizieren Sie mit Facebook Nutzern aus Ihrem Bitrix24 Account.</p>

     <ul class=\"im-connector-settings-header-list\">

     <li class=\"im-connector-settings-header-list-item\">automatische Verteilung von eingehenden Nachrichten entsprechend den Regeln der Warteschleife</li>

     <li class=\"im-connector-settings-header-list-item\">Benutzeroberfläche wie im Bitrix24 Chat</li>

     <li class=\"im-connector-settings-header-list-item\">alle Konversationen werden in der CRM-History abgespeichert</li>

    </ul>
";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_EXPIRED_ACCOUNT_TOKEN"] = "Sie müssen unter Ihrem Facebook Account autorisiert sein, um Einstellungen zu bearbeiten.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_EXPIRED_ACCOUNT_TOKEN_WARNING"] = "Beachten Sie bitte, dass wenn Sie sich unter einem Facebook Account autorisieren, in dem Sie keine Seiten verwalten können, die Verbindung zu Ihrem Kommunikationskanal in BItrix24 getrennt wird.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_INDEX_DESCRIPTION"] = "Verbinden Sie die Facebook-Seite Ihres Unternehmens mit einem Kommunikationskanal in Bitrix24 und kommunizieren Sie mit Ihren Kunden im Bitrix24 Chat. Sie müssen eine Facebook-Seite für Ihr Unternehmen erstellen oder eine bereits existierende verwenden. Sie müssen dabei der Seitenadministrator sein.   ";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_INFO"] = "Information";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_LOG_IN_UNDER_AN_ADMINISTRATOR_ACCOUNT_PAGE"] = "Autorisieren Sie sich unter Ihrem Facebook Account, in dem Sie Ihre Seiten verwalten können, um Nachrichten von Ihren Bitrix24 Kunden erhalten zu können.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_NO_SPECIFIC_PAGE"] = "Fehlt eine erforderliche Seite? Einen möglichen Grund finden Sie in #A#diesem Artikel#A_END#.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_OTHER_PAGES"] = "Andere Seiten";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_PAGE"] = "Seite";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_PAGE_IM"] = "Messenger";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_SELECT_THE_PAGE"] = "Wählen Sie bitte eine Facebook Seite aus, die mit Ihrem Kommunikationskanal in Bitrix24 verbunden werden soll.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_THERE_IS_NO_PAGE_WHERE_THE_ADMINISTRATOR"] = "Sie haben noch keine Facebook Seiten, wo Sie Administrator sind.<br>Sie können Ihre Facebook Seite jetzt erstellen.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_TITLE"] = "Kommunizieren Sie mit Facebook Nutzern via Bitrix24";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_TO_CREATE_A_PAGE"] = "Erstellen";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_USER"] = "Account";
?>