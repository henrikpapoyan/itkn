<?
$MESS["CRM_STATUS_DELETION_ALERT_DEAL_STATUS"] = "У цій стадії є активні елементи. Перемістіть угоди і повторіть видалення стадії.";
$MESS["CRM_STATUS_DELETION_ALERT_INVOICE_STATUS"] = "У цьому статусі є активні елементи. Перемістіть рахунки та повторіть видалення статусу.";
$MESS["CRM_STATUS_DELETION_ALERT_QUOTE_STATUS"] = "У цьому статусі є активні елементи. Перемістіть пропозиції та повторіть видалення статусу.";
$MESS["CRM_STATUS_DELETION_ALERT_STATUS"] = "У цьому статусі є активні елементи. Перемістіть ліди та повторіть видалення статусу.";
$MESS["CRM_STATUS_DELETION_ALERT_TITLE_DEAL_STATUS"] = "Видалення стадії";
$MESS["CRM_STATUS_DELETION_ALERT_TITLE_STATUS"] = "Видалення статусу";
?>