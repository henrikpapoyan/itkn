<?
$MESS["CRM_STATUS_DELETION_ALERT_DEAL_STATUS"] = "In dieser Phase gibt es aktive Elemente. Verschieben Sie die Aufträge, bevor Sie die Phase löschen.";
$MESS["CRM_STATUS_DELETION_ALERT_INVOICE_STATUS"] = "In diesem Status gibt es aktive Elemente. Verschieben Sie die Rechnungen, bevor Sie den Status löschen.";
$MESS["CRM_STATUS_DELETION_ALERT_QUOTE_STATUS"] = "In diesem Status gibt es aktive Elemente. Verschieben Sie die Angebote, bevor Sie den Status löschen.";
$MESS["CRM_STATUS_DELETION_ALERT_STATUS"] = "In diesem Status gibt es aktive Elemente. Verschieben Sie die Leads, bevor Sie den Status löschen.";
$MESS["CRM_STATUS_DELETION_ALERT_TITLE_DEAL_STATUS"] = "Phase löschen";
$MESS["CRM_STATUS_DELETION_ALERT_TITLE_STATUS"] = "Status löschen";
?>