<?
$MESS["IMCONNECTOR_COMPONENT_VIBER_CONNECT_HELP"] = "<div class=\"imconnector-field-button-subtitle\">Я хочу</div><div class=\"imconnector-field-button-name\"><span class=\"imconnector-field-box-text-bold\">Підключити</span> акаунт</div";
$MESS["IMCONNECTOR_COMPONENT_VIBER_API_KEY"] = "Введіть ключ свого публічного акаунта:";
$MESS["IMCONNECTOR_COMPONENT_VIBER_INFO_CONNECT_ID"] = "redirect=detail&code=7417097";
$MESS["IMCONNECTOR_COMPONENT_VIBER_INFO_ANDROID_CONNECT_ID"] = "redirect=detail&code=7417097";
$MESS["IMCONNECTOR_COMPONENT_VIBER_INFO_IOS_CONNECT_ID"] = "redirect=detail&code=7417097";
$MESS["IMCONNECTOR_COMPONENT_VIBER_I_KNOW_KEY"] = "Я знаю ключ";
$MESS["IMCONNECTOR_COMPONENT_VIBER_FINAL_FORM_DESCRIPTION"] = "Viber успішно підключений до вашої Відкритої лінії. Тепер всі звернення до вашого публічного акаунту будуть автоматично спрямовані на ваш Бітрікс24.";
$MESS["IMCONNECTOR_COMPONENT_VIBER_TESTED"] = "Тестування з'єднання";
$MESS["IMCONNECTOR_COMPONENT_VIBER_NAME_BOT"] = "Ім'я публічного акаунту";
$MESS["IMCONNECTOR_COMPONENT_VIBER_LINK_PUBLIC_ACCOUNT"] = "Посилання на публічний запис";
$MESS["IMCONNECTOR_COMPONENT_VIBER_LINK_CHAT_ONE_TO_ONE"] = "Посилання на чат один на один";
$MESS["IMCONNECTOR_COMPONENT_VIBER_CONNECTED"] = "Viber підключений";
$MESS["IMCONNECTOR_COMPONENT_VIBER_CHANGE_ANY_TIME"] = "Ви можете в будь-який час змінити або відключити";
$MESS["IMCONNECTOR_COMPONENT_VIBER_CONNECT_TITLE"] = "Підключіть Viber до Відкритої лінії";
$MESS["IMCONNECTOR_COMPONENT_VIBER_CONNECT_STEP"] = "Для підключення необхідно <a onclick=\"top.BX.Helper.show('#ID#');\" class=\"imconnector-field-box-link\">створити публічний акаунт в Viber</a>
 або підключити вже існуючий. Якщо у вас ще немає публічного акаунта, ми допоможемо створити його в кілька кроків і підключити до вашого Бітрікс24
";
$MESS["IMCONNECTOR_COMPONENT_VIBER_USING_TITLE"] = "Я використовую";
$MESS["IMCONNECTOR_COMPONENT_VIBER_USING_ANDROID"] = "<span class=\"imconnector-field-box-text-bold\">ANDROID</span> ЗАСТОСУНОК";
$MESS["IMCONNECTOR_COMPONENT_VIBER_USING_IOS"] = "<span class=\"imconnector-field-box-text-bold\">IOS</span> ЗАСТОСУНОК";
$MESS["IMCONNECTOR_COMPONENT_VIBER_INSTRUCTION_TITLE"] = "<span class=\"imconnector-field-box-text-bold\">Інструкція</span> по отриманню ключа публічного акаунта:";
$MESS["IMCONNECTOR_COMPONENT_VIBER_INFO"] = "Інформація";
?>