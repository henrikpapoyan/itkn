<?
$MESS["IMCONNECTOR_COMPONENT_VIBER_API_KEY"] = "Geben Sie den Schlüssel Ihres öffentlichen Accounts ein:";
$MESS["IMCONNECTOR_COMPONENT_VIBER_CHANGE_ANY_TIME"] = "Kann jederzeit bearbeitet oder abgeschaltet werden";
$MESS["IMCONNECTOR_COMPONENT_VIBER_CONNECTED"] = "Viber verbunden";
$MESS["IMCONNECTOR_COMPONENT_VIBER_CONNECT_HELP"] = "<div class=\"imconnector-field-button-subtitle\">Ich möchte</div><div class=\"imconnector-field-button-name\">einen Account <span class=\"imconnector-field-box-text-bold\">verbinden</span></div>";
$MESS["IMCONNECTOR_COMPONENT_VIBER_CONNECT_STEP"] = "Vor der Verbindung müssen Sie <a onclick=\"top.BX.Helper.show('#ID#');\" class=\"imconnector-field-box-link\">einen öffentlichen Viber-Account erstellen</a>
 oder einen existierenden verbinden. Wir können Ihnen helfen, einen öffentlichen Account zu erstellen und ihn mit Ihrem Bitrix24 Account zu verbinden.";
$MESS["IMCONNECTOR_COMPONENT_VIBER_CONNECT_TITLE"] = "Verbinden Sie Viber mit Ihrem Kommunikationskanal";
$MESS["IMCONNECTOR_COMPONENT_VIBER_FINAL_FORM_DESCRIPTION"] = "Viber wurde mit Ihrem Kommunikationskanal verbunden. Alle Nachrichten, die jetzt in Ihrem öffentlichen Account erscheinen, werden an Ihr Bitrix24 weitergeleitet.";
$MESS["IMCONNECTOR_COMPONENT_VIBER_INFO"] = "Information";
$MESS["IMCONNECTOR_COMPONENT_VIBER_INFO_ANDROID_CONNECT_ID"] = "redirect=detail&code=7417097";
$MESS["IMCONNECTOR_COMPONENT_VIBER_INFO_CONNECT_ID"] = "redirect=detail&code=7417097";
$MESS["IMCONNECTOR_COMPONENT_VIBER_INFO_IOS_CONNECT_ID"] = "redirect=detail&code=7417097";
$MESS["IMCONNECTOR_COMPONENT_VIBER_INSTRUCTION_TITLE"] = "<span class=\"imconnector-field-box-text-bold\">So erhalten Sie einen Schlüssel des öffentlichen Accounts</span>:";
$MESS["IMCONNECTOR_COMPONENT_VIBER_I_KNOW_KEY"] = "Ich habe den Schlüssel";
$MESS["IMCONNECTOR_COMPONENT_VIBER_LINK_CHAT_ONE_TO_ONE"] = "Link zum Unter-vier-Augen Chat";
$MESS["IMCONNECTOR_COMPONENT_VIBER_LINK_PUBLIC_ACCOUNT"] = "Link zum öffentlichen Account";
$MESS["IMCONNECTOR_COMPONENT_VIBER_NAME_BOT"] = "Name des öffentlichen Accounts";
$MESS["IMCONNECTOR_COMPONENT_VIBER_TESTED"] = "Testverbindung";
$MESS["IMCONNECTOR_COMPONENT_VIBER_USING_ANDROID"] = "<span class=\"imconnector-field-box-text-bold\">ANDROID</span> APP";
$MESS["IMCONNECTOR_COMPONENT_VIBER_USING_IOS"] = "<span class=\"imconnector-field-box-text-bold\">IOS</span> APP";
$MESS["IMCONNECTOR_COMPONENT_VIBER_USING_TITLE"] = "ich nutze";
?>