<?
$MESS["FACEID_TMS_START_SERVICES_MAIN_SECTION"] = "Bitrix24 Portal";
$MESS["FACEID_TMS_START_SERVICES_PARENT_SECTION"] = "Arbeitszeit erfassen";
$MESS["FACEID_TMS_START_SECTION_TEMPLATE_NAME"] = "Arbeitszeitmanagement mit Bitrix24.Time";
$MESS["FACEID_TMS_START_SECTION_TEMPLATE_DESCRIPTION"] = "Aktiviert Bitrix24.Time und indexiert Profilfotos der Mitarbeiter.";
?>