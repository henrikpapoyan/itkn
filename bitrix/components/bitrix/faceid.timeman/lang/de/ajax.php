<?
$MESS["FACEID_TRACKERWD_CMP_ERROR_ANON"] = "Fehler bei der Authentifizierung";
$MESS["FACEID_TRACKERWD_CMP_ERROR_TIMEMAN_ACCESS"] = "Zugriff auf Bearbeitung der Arbeitszeit der Mitarbeiter wurde verweigert";
$MESS["FACEID_TRACKERWD_CMP_ERROR_AGREEMENT"] = "Die Lizenzvereinbarung der Gesichtserkennung wurde nicht akzeptiert";
$MESS["FACEID_TRACKERWD_CMP_ERROR_OPEN_DAY"] = "Fehler beim Starten des Arbeitstags";
$MESS["FACEID_TRACKERWD_CMP_ERROR_PAUSE_DAY"] = "Fehler bei der Eingabe der Pause";
$MESS["FACEID_TRACKERWD_CMP_ERROR_REOPEN_DAY"] = "Fehler beim Fortsetzen des Arbeitstages";
$MESS["FACEID_TRACKERWD_CMP_ERROR_CLOSE_DAY"] = "Fehler beim Beenden des Arbeitstages";
$MESS["FACEID_TRACKERWD_CMP_ERROR_TIMEMAN_DISABLED"] = "Das Arbeitszeitmanagement ist für diesen Mitarbeiter deaktiviert";
$MESS["FACEID_TRACKERWD_CMP_ERROR_USER_NOT_FOUND_LOCAL"] = "Der Nutzer wurde in der Datenbank nicht gefunden";
$MESS["FACEID_TRACKERWD_CMP_ERROR_NO_FACE_LOCAL"] = "Das Gesicht wurde in der Datenbank nicht gefunden";
$MESS["FACEID_TRACKERWD_CMP_ERROR_INDEX_DISABLED"] = "Sie müssen Indexierung für Avatar-Fotos der Nutzer im Bereich Zeit und Berichte - Bitrix24.Time aktivieren.";
?>