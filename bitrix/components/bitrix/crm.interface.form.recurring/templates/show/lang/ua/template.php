<?
$MESS["CRM_RECURRING_HINT_BASE"] = "Рахунок виставляється #CONDITION##CONSTRAINT#.";
$MESS["CRM_RECURRING_DAILY"] = "день";
$MESS["CRM_RECURRING_WEEKLY"] = "тиждень";
$MESS["CRM_RECURRING_MONTHLY"] = "місяць";
$MESS["CRM_RECURRING_YEARLY"] = "рік";
$MESS["CRM_RECURRING_HINT_START_CONSTRAINT"] = ", починаючи з <nobr>#DATETIME#</nobr>";
$MESS["CRM_RECURRING_HINT_START_CONSTRAINT_NONE"] = "з моменту збереження";
$MESS["CRM_RECURRING_HINT_END_CONSTRAINT"] = ", із закінченням <nobr>#DATETIME#</nobr>";
$MESS["CRM_RECURRING_HINT_END_CONSTRAINT_NONE"] = ", без дати закінчення";
$MESS["CRM_RECURRING_HINT_END_CONSTRAINT_TIMES"] = ", із закінченням після <nobr>#TIMES# #TIMES_PLURAL#</nobr>";
$MESS["CRM_RECURRING_HINT_END_CONSTRAINT_TIMES_PLURAL_0"] = "повторення";
$MESS["CRM_RECURRING_HINT_END_CONSTRAINT_TIMES_PLURAL_1"] = "повторень";
$MESS["CRM_RECURRING_HINT_END_CONSTRAINT_TIMES_PLURAL_2"] = "повторень";
$MESS["CRM_RECURRING_HINT_DAILY_MONTH_INTERVAL"] = ", раз на #TIMES# #TIMES_PLURAL#";
$MESS["CRM_RECURRING_HINT_DAILY_MONTH_INTERVAL_PLURAL_0"] = "місяць";
$MESS["CRM_RECURRING_HINT_DAILY_MONTH_INTERVAL_PLURAL_1"] = "місяці";
$MESS["CRM_RECURRING_HINT_DAILY_MONTH_INTERVAL_PLURAL_2"] = "місяців";
$MESS["CRM_RECURRING_DAILY_EXT"] = "кожен#NUMBER##DAY_TYPE# день";
$MESS["CRM_RECURRING_WEEKLY_EXT"] = "кожен#NUMBER# тиждень";
$MESS["CRM_RECURRING_MONTHLY_EXT_TYPE_1"] = "кожне #DAY_NUMBER# число кожного#MONTH_NUMBER# місяця";
$MESS["CRM_RECURRING_MONTHLY_EXT_TYPE_1_WORKDAY"] = "кожний #DAY_NUMBER# робочий день кожного#MONTH_NUMBER# місяця";
$MESS["CRM_RECURRING_MONTHLY_EXT_TYPE_2"] = "#EACH# #DAY_NUMBER# #WEEKDAY_NAME# кожного#MONTH_NUMBER# місяця";
$MESS["CRM_RECURRING_YEARLY_EXT_TYPE_1"] = "кожен #DAY_NUMBER# день місяця #MONTH_NAME#";
$MESS["CRM_RECURRING_YEARLY_EXT_TYPE_1_WORKDAY"] = "кожен #DAY_NUMBER# робочий день місяця #MONTH_NAME#";
$MESS["CRM_RECURRING_YEARLY_EXT_TYPE_2"] = "#EACH# #DAY_NUMBER# #WEEKDAY_NAME# місяця #MONTH_NAME#";
$MESS["CRM_RECURRING_DAILY_WORK"] = "робочий";
$MESS["CRM_RECURRING_DAILY_WORK_ALT"] = "робочого";
$MESS["CRM_RECURRING_ANY"] = "календарний";
$MESS["CRM_RECURRING_ANY_ALT"] = "календарного";
$MESS["CRM_RECURRING_EACH_M"] = "кожен";
$MESS["CRM_RECURRING_EACH_M_ALT"] = "кожного";
$MESS["CRM_RECURRING_EACH_F"] = "кожну";
$MESS["CRM_RECURRING_EACH"] = "кожне";
$MESS["CRM_RECURRING_REPEAT_TYPE"] = "повторюваність";
$MESS["CRM_RECURRING_DAY_INTERVAL"] = "день";
$MESS["CRM_RECURRING_MONTH_SHORT"] = "міс";
$MESS["CRM_RECURRING_WEEK_ALT"] = "тиждень";
$MESS["CRM_RECURRING_MONTH_ALT"] = "місяця";
$MESS["CRM_RECURRING_EVERY_DAY"] = "щодня";
$MESS["CRM_RECURRING_NUMBER_OF_EACH_M_ALT"] = "день кожного";
$MESS["CRM_RECURRING_DAY_OF_MONTH"] = "день місяця";
$MESS["CRM_RECURRING_TASK_CTIME"] = "Час створення завдання";
$MESS["CRM_RECURRING_REPEAT_START"] = "Починати повторення";
$MESS["CRM_RECURRING_REPEAT_END"] = "Повторювати до";
$MESS["CRM_RECURRING_REPEAT_END_C_DATE"] = "дата закінчення";
$MESS["CRM_RECURRING_REPEAT_END_C_NONE"] = "немає дати закінчення";
$MESS["CRM_RECURRING_REPEAT_END_C_TIMES"] = "завершити після";
$MESS["CRM_RECURRING_REPEATS"] = "повторень";
$MESS["CRM_RECURRING_NUMBER_0_M"] = "перший";
$MESS["CRM_RECURRING_NUMBER_0_F"] = "першу";
$MESS["CRM_RECURRING_NUMBER_0"] = "перше";
$MESS["CRM_RECURRING_NUMBER_1_M"] = "другий";
$MESS["CRM_RECURRING_NUMBER_1_F"] = "другу";
$MESS["CRM_RECURRING_NUMBER_1"] = "друге";
$MESS["CRM_RECURRING_NUMBER_2_M"] = "третій";
$MESS["CRM_RECURRING_NUMBER_2_F"] = "третю";
$MESS["CRM_RECURRING_NUMBER_2"] = "третє";
$MESS["CRM_RECURRING_NUMBER_3_M"] = "четвертий";
$MESS["CRM_RECURRING_NUMBER_3_F"] = "четверту";
$MESS["CRM_RECURRING_NUMBER_3"] = "четверте";
$MESS["CRM_RECURRING_NUMBER_4_M"] = "останній";
$MESS["CRM_RECURRING_NUMBER_4_F"] = "останню";
$MESS["CRM_RECURRING_NUMBER_4"] = "останнє";
$MESS["CRM_RECURRING_WD_1"] = "понеділок";
$MESS["CRM_RECURRING_WD_2"] = "вівторок";
$MESS["CRM_RECURRING_WD_3"] = "середа";
$MESS["CRM_RECURRING_WD_4"] = "четвер";
$MESS["CRM_RECURRING_WD_5"] = "п'ятниця";
$MESS["CRM_RECURRING_WD_6"] = "субота";
$MESS["CRM_RECURRING_WD_7"] = "неділя";
$MESS["CRM_RECURRING_WD_3_ALT"] = "середу";
$MESS["CRM_RECURRING_WD_5_ALT"] = "п'ятницю";
$MESS["CRM_RECURRING_WD_6_ALT"] = "суботу";
$MESS["CRM_RECURRING_WD_SH_1"] = "пн";
$MESS["CRM_RECURRING_WD_SH_2"] = "вт";
$MESS["CRM_RECURRING_WD_SH_3"] = "ср";
$MESS["CRM_RECURRING_WD_SH_4"] = "чт";
$MESS["CRM_RECURRING_WD_SH_5"] = "пт";
$MESS["CRM_RECURRING_WD_SH_6"] = "сб";
$MESS["CRM_RECURRING_WD_SH_7"] = "нд";
$MESS["CRM_RECURRING_MONTH_1"] = "січень";
$MESS["CRM_RECURRING_MONTH_2"] = "лютий";
$MESS["CRM_RECURRING_MONTH_3"] = "березень";
$MESS["CRM_RECURRING_MONTH_4"] = "квітень";
$MESS["CRM_RECURRING_MONTH_5"] = "травень";
$MESS["CRM_RECURRING_MONTH_6"] = "червень";
$MESS["CRM_RECURRING_MONTH_7"] = "липень";
$MESS["CRM_RECURRING_MONTH_8"] = "серпень";
$MESS["CRM_RECURRING_MONTH_9"] = "вересень";
$MESS["CRM_RECURRING_MONTH_10"] = "жовтень";
$MESS["CRM_RECURRING_MONTH_11"] = "листопад";
$MESS["CRM_RECURRING_MONTH_12"] = "грудень";
$MESS["CRM_RECURRING_TEMPLATE_WILL_BE_CREATED"] = "Увага! На підставі рахунку буде створено новий шаблон, з вказаними параметрами повторення.";
$MESS["CRM_RECURRING_SWITCHER_BLOCK"] = "Зробити рахунок регулярним";
?>