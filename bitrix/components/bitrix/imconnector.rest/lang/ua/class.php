<?
$MESS["IMCONNECTOR_COMPONENT_REST_MODULE_NOT_INSTALLED"] = "Модуль коннекторів месенджерів не встановлений";
$MESS["IMCONNECTOR_COMPONENT_REST_NO_ACTIVE_CONNECTOR"] = "Даний коннектор не активний";
$MESS["IMCONNECTOR_COMPONENT_REST_SESSION_HAS_EXPIRED"] = "Ваша сесія закінчилася. Відправте форму ще раз";
$MESS["IMCONNECTOR_COMPONENT_REST_CONNECTOR_ERROR_STATUS"] = "В ході роботи сталася помилка. Будь ласка, перевірте налаштування.";
?>