<?
$MESS["IMCONNECTOR_COMPONENT_REST_INDEX_DESCRIPTION_NEW"] = "Nutzen Sie einen Connector mit Ihrem Kommunikationskanal, damit die Nachrichten Ihrer Kunden in Ihrem Bitrix24 Chat veröffentlicht werden.";
$MESS["IMCONNECTOR_COMPONENT_REST_CONNECT"] = "Verbinden";
$MESS["IMCONNECTOR_COMPONENT_REST_CONNECTOR_ERROR_STATUS"] = "Bei der Nutzung des Connectors ist ein Fehler aufgetreten. Bitte verbinden Sie erneut.";
$MESS["IMCONNECTOR_COMPONENT_REST_INFO_TITLE"] = "Kanal wurde erfolgreich konfiguriert";
$MESS["IMCONNECTOR_COMPONENT_REST_INFO_DESCRIPTION"] = "Mehr Informationen finden Sie auf der Seite der Einstellungen";
?>