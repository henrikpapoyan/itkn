<?
$MESS["IMCONNECTOR_COMPONENT_REST_INDEX_DESCRIPTION_NEW"] = "Підключіть коннектор до Відкритої лінії щоб приймати повідомлення від ваших клієнтів в робочому чаті Бітрікс24.";
$MESS["IMCONNECTOR_COMPONENT_REST_CONNECT"] = "Підключити";
$MESS["IMCONNECTOR_COMPONENT_REST_CONNECTOR_ERROR_STATUS"] = "В процесі роботи каналу сталася помилка. Вам необхідно виконати підключення повторно.";
$MESS["IMCONNECTOR_COMPONENT_REST_INFO_TITLE"] = "Канал успішно налаштований";
$MESS["IMCONNECTOR_COMPONENT_REST_INFO_DESCRIPTION"] = "Детальніше на сторінці налаштувань";
?>
