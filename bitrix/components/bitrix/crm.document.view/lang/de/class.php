<?
$MESS["CRM_DOCUMENT_VIEW_COMPONENT_MODULE_ERROR"] = "Nicht alle erforderlichen Module wurden gefunden";
$MESS["CRM_DOCUMENT_VIEW_COMPONENT_CSRF_ERROR"] = "Ungültige ID der Sitzung";
$MESS["CRM_DOCUMENT_VIEW_COMPONENT_SMS_TEXT"] = "Nutzen Sie diesen Link #LINK#, um #TITLE# herunterzuladen";
?>