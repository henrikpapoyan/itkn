<?
$MESS["CRM_COUNTER_ORDER_CAPTION"] = "Замовлення";
$MESS["CRM_COUNTER_ORDER_STUB"] = "Немає замовлень, що потребують оперативної реакції";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Модуль crm не встановлено.";
$MESS["CRM_COUNTER_ENTITY_TYPE_NOT_DEFINED"] = "Не заданий тип сутності.";
$MESS["CRM_COUNTER_DEAL_CAPTION"] = "Угоди";
$MESS["CRM_COUNTER_LEAD_CAPTION"] = "Ліди";
$MESS["CRM_COUNTER_CONTACT_CAPTION"] = "Контакти";
$MESS["CRM_COUNTER_COMPANY_CAPTION"] = "Компанії";
$MESS["CRM_COUNTER_DEAL_STUB"] = "Немає угод, що вимагають миттєвої реакції.";
$MESS["CRM_COUNTER_LEAD_STUB"] = "Немає лідів, що вимагають миттєвої реакції.";
$MESS["CRM_COUNTER_CONTACT_STUB"] = "Немає контактів, що вимагають миттєвої реакції.";
$MESS["CRM_COUNTER_COMPANY_STUB"] = "Немає компаній, які потребують миттєвої реакції.";
?>