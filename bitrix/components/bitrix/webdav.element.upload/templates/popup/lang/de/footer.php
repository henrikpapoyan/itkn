<?
$MESS["WD_WF_STATUS"] = "Status für neue hochgeladene Dateien";
$MESS["WD_WF_ATTENTION1"] = "Achtung! Sie können hochgeladene Dateien nicht ändern.";
$MESS["WD_WF_ATTENTION2"] = "Sie können Dateien mit dem Status #STATUS# bearbeiten.";
$MESS["WD_WF_ATTENTION3"] = "Sie können Dateien mit dem Status #STATUS# bearbeiten.";
$MESS["WD_OVERVIEW"] = "Existierende Dateien überschreiben";
$MESS["IBEL_BIZPROC_DATE"] = "Datum des aktuellen Status:";
$MESS["IBEL_BIZPROC_NA"] = "Es gibt keine initiierten Workflows.";
$MESS["IBEL_BIZPROC_RUN_CMD"] = "Befehl ausführen:";
$MESS["IBEL_BIZPROC_RUN_CMD_NO"] = "Nicht ausführen";
$MESS["IBEL_BIZPROC_STATE"] = "Aktueller Status:";
$MESS["WD_BP"] = "Workflows";
$MESS["WD_USER_FIELDS"] = "Benutzerdefinierte Felder";
?>