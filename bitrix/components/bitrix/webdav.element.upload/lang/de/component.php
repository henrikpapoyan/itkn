<?
$MESS["WD_ACCESS_DENIED"] = "Zugriff verweigert.";
$MESS["WD_ERROR_BAD_SECTION"] = "Der Ordner wurde nicht gefunden.";
$MESS["WD_TITLE"] = "Dateien hochladen";
$MESS["W_WEBDAV_IS_NOT_INSTALLED"] = "Das Modul \"WebDAV\" wurde nicht installiert.";
$MESS["WD_NO_BP_TEMPLATES"] = "Keine Workflows.";
$MESS["WD_NO_BP_TEMPLATES_ADMIN"] = "<a hreg=\"#LINK#\">Alle Workflows anzeigen</a>";
$MESS["WD_UPLOAD_ERROR_TITLE"] = "Fehler beim Hochladen des Dokumentes.";
$MESS["WD_NO_BP_AUTORUN"] = "Es gibt keine Workflows mit dem Autostart.";
$MESS["WD_BP_ACTIVE_STATES"] = "Es gibt keine Workflows mit verfügbaren Status.";
?>