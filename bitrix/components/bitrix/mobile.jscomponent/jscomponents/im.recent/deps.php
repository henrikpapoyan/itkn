<?
return [
	'db',
	'chat/tables',
	'chat/utils',
	'chat/messengercommon',
	'chat/dataconverter',
	'chat/restrequest',
	'chat/searchscopes',
	'chat/timer',
	'chat/dialogcache',
	'user/profile',
];