;
(function(){
	let items = [];
	let sections = [];
	let SITE_ID = BX.componentParameters.get("SITE_ID", "s1");
	BX.listeners = {};


	/**
	 * @let  BaseList menu
	 */

	let More = {
		findIn: function (items, query)
		{
			query = query.toUpperCase();
			let searchResult = items.filter(item =>
			{
				let section = sections.find(section => section.id === item.sectionCode);
				if (item.title && item.title.toUpperCase().indexOf(
					query) >= 0 || section && section.title && section.title.toUpperCase().indexOf(query) >= 0)
				{
					if(!item.type || (item.type != "button" && item.type != "userinfo"))
						return item;
				}
			})
				.map(item =>
				{
					let section = sections.find(section => section.id === item.sectionCode);
					item.subtitle = section ? section.title: "";
					item.useLetterImage = true;
					return item;
				});

			return searchResult;
		},
		find: function (query)
		{
			let result = this.findIn(items, query);
			let groupItems = [];
			items.forEach(item =>
			{
				if (item.type === "group")
				{
					let section = sections.find(section => section.id === item.sectionCode);
					groupItems = groupItems.concat(this.findIn(item.params.items, query)
						.map(groupItem =>
							{
								groupItem.subtitle = section.title + " -> " + item.title;
								return groupItem;
							}
						));
				}
			});

			return result.concat(groupItems);
		},
		updateCounters: function (siteCounters)
		{
			let counters = Object.keys(siteCounters);
			let totalCount = 0;
			let updateCountersData = counters.filter(counter => this.counterList.includes(counter))
				.map(counter =>
				{
					this.currentCounters[counter] = siteCounters[counter];
					return {filter: {"params.counter": counter}, element: {messageCount: siteCounters[counter]}}
				});

			if (updateCountersData.length > 0)
			{
				menu.updateItems(updateCountersData);
				Object.values(this.currentCounters).forEach(count => totalCount += count);
				Application.setBadges({more: totalCount});
			}
		},
		initCache:function(){
			let cachedResult = this.getCache();

			if(cachedResult)
				this.handleResult(cachedResult);
			else
				this.handleResult(result);


		},
		getCache:function()
		{
			let result = null;
			try
			{
				result = JSON.parse(Application.sharedStorage().get("more"));
			}
			catch (e)
			{
				//do nothing
			}

			return result;
		},
		updateMenu:function(){
			BX.ajax({url: component.resultUrl, dataType:"json"})
				.then(result => {
					if(result.menu)
					{
						Application.sharedStorage().set("more", JSON.stringify(result));
						this.handleResult(result);
						this.refreshList();
					}
				})
				.catch(e => console.error(e));
		},
		refreshList:function(){
			menu.setItems(items, sections);
			setTimeout(() =>
			{
				let cachedCounters = Application.sharedStorage().get('userCounters');
				if (cachedCounters)
				{
					try
					{
						let counters = JSON.parse(cachedCounters);
						if (counters[SITE_ID])
						{
							this.updateCounters(counters[SITE_ID]);
						}

					}
					catch (e)
					{
						//do nothing
					}
				}

			}, 0);
		},
		handleResult:function(result)
		{
			let menuStructure = result.menu;
			if(result.counterList)
			{
				this.counterList = result.counterList;
			}

			if(menuStructure)
			{
				items = [];
				sections = [];
				menuStructure.forEach(
					sec => {
						if (
							sec.min_api_version && sec.min_api_version > Application.getApiVersion()
							|| sec.hidden == true
						)
						{
							return;
						}

						let sectionCode = "section_" + sec.sort;
						let sectionItems = [];
						if (sec.items)
						{
							sectionItems = sec.items
								.filter(item => !item.hidden)
								.map(item =>
								{
									if (item.params && item.params.counter)
									{
										this.counterList.push(item.params.counter);
										if(typeof this.currentCounters[item.params.counter] != "undefined")
										{
											item.messageCount = this.currentCounters[item.params.counter]
										}
									}

									return item;
								});
						}

						sections.push({
							title: sec.title,
							id: sectionCode
						});

						items = items.concat(sectionItems)
					});
			}

		},
		init: function ()
		{
			let lastSavedVersion = Application.sharedStorage().get('version');
			let needToUpdate = (component.version == lastSavedVersion);
			Application.sharedStorage().set("version", component.version);

			menu.setListener((eventName, data)=>this.listener(eventName, data));
			items = items.filter((item) => item != false).map((item) =>
			{
				if (item.type != "destruct")
				{
					item.styles =
						{
							title: {
								color: "#FF4E5665"
							}
						};
				}

				if (item.params.counter)
				{
					this.counterList.push(item.params.counter);
				}

				return item;

			});
			BX.addCustomEvent("onPullEvent-main", (command, params) =>
			{
				if (command == "user_counter")
				{
					if (params[SITE_ID])
					{
						this.updateCounters(params[SITE_ID])
					}
				}
			});

			BX.addCustomEvent("onUpdateUserCounters", (data) =>
			{
				if (data[SITE_ID])
				{
					this.updateCounters(data[SITE_ID])
				}
			});


			BX.addCustomEvent("shouldReloadMenu", () => this.updateMenu());
			this.initCache();
			BX.onViewLoaded(() =>
			{
				this.refreshList();
				if(needToUpdate)
					this.updateMenu();
			});
		},
		listener: function (eventName, data)
		{
			let item = null;
			if (eventName === "onUserTypeText")
			{
				if (data.text.length > 0)
				{
					menu.setSearchResultItems(More.find(data.text), []);
				}
				else
				{
					menu.setSearchResultItems([], []);
				}
			}
			else if (eventName === "onItemAction")
			{
				item = data.item;
				if (item.params.actionOnclick)
				{
					eval(item.params.actionOnclick);
				}
			}
			else if (eventName === "onItemSelected" || eventName === "onSearchItemSelected")
			{
				item = data;
				if (item.type === "group")
				{
					PageManager.openComponent("JSComponentSimpleList", {
						title: item.title,
						params: {
							items: item.params.items
						}
					})
				}
				else if (item.params.onclick)
				{
					(function(){
						eval(item.params.onclick);
					}).call(item);
				}
				else if (item.params.action)
				{
					Application.exit();
				}
				else if (item.params.url)
				{
					if (item.params._type && item.params._type === "list")
					{
						PageManager.openList(item.params);
					}
					else
					{
						PageManager.openPage({url: item.params.url, title: item.title});
					}
				}
			}
			else if (eventName === "onRefresh")
			{
				menu.stopRefreshing();
				this.updateMenu();
			}
		},
		counterList: [],
		currentCounters: {}
	};

	More.init();

})();

