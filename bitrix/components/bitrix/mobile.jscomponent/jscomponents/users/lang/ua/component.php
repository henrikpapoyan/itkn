<?
$MESS["LOAD_MORE_USERS"] = "Завантажити щe";
$MESS["USER_LOADING"] = "Завантаження...";
$MESS["LOAD_MORE_RESULT"] = "Більше результатів";
$MESS["SEARCH_LOADING"] = "Пошук ...";
$MESS["RECENT_SEARCH"] = "Нещодавній пошук
";
$MESS["SEARCH_EMPTY_RESULT"] = "На жаль, за вашим запитом нічого не знайдено ...";
$MESS["SEARCH_PLACEHOLDER"] = "Введіть ім'я, прізвище або посаду";
$MESS["ACTION_DELETE"] = "Видалити";
$MESS["INVITE_USERS"] = "Запросити співробітників";
?>