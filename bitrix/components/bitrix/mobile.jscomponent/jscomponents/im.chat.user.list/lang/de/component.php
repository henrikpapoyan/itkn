<?
$MESS["IM_USER_CONNECTION_ERROR"] = "Fehler bei Verbindung mit Bitrix24. Überprüfen Sie bitte die Netzwerk-Verbindung.";
$MESS["IM_USER_API_ERROR"] = "Fehler bei der Ausführung der Anfrage. Versuchen Sie bitte später erneut.";
?>