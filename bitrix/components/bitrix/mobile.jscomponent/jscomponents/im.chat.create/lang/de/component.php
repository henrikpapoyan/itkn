<?
$MESS["IM_CREATE_CONNECTION_ERROR"] = "Fehler beim Verbinden mit Bitrix24. Überprüfen Sie bitte Ihre Netzwerk-Verbindung.";
$MESS["IM_CREATE_API_ERROR"] = "Fehler beim Erstellen eines Chats. Versuchen Sie bitte später erneut.";
$MESS["IM_CHAT_TYPE_OPEN"] = "Öffentlicher Chat";
$MESS["IM_CHAT_TYPE_CHAT"] = "Privater Chat";
$MESS["IM_DEPARTMENT_START"] = "Geben Sie den Abteilungsnamen ein, um die Suche zu starten.";
?>