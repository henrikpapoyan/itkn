<?
$MESS["SE_SYS_TITLE"] = "Andere Einstellungen";
$MESS["SE_SYS_ENERGY_BACKGROUND"] = "Im Hintergrund laufen lassen";
$MESS["SE_SYS_LOW_PUSH_ACTIVITY"] = "Benachrichtigungsintervall erhöhen";
$MESS["SE_SYS_LOW_PUSH_ACTIVITY_DESC"] = "Nutzen Sie diese Option, um Energie zu sparen. Sie werden weniger Service-Benachrichtigungen und Zähler-Updates erhalten. Die Inhaltsrelevanten Benachrichtigungen werden dadurch nicht betroffen.";
?>