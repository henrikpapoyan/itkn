<?
$MESS["IM_LIST_CHAT"] = "Закритий чат";
$MESS["IM_LIST_CHAT_OPEN"] = "Відкритий чат";
$MESS["IM_LIST_EMPLOYEE"] = "Співробітник";
$MESS["ELEMENT_MENU_DELETE"] = "Видалити";
$MESS["ELEMENT_MENU_LEAVE"] = "Покинути";
$MESS["ELEMENT_MENU_HIDE"] = "Приховати";
$MESS["ELEMENT_MENU_PIN"] = "Закріпити";
$MESS["ELEMENT_MENU_UNPIN"] = "Відкріпити";
$MESS["ELEMENT_MENU_CALL"] = "Зателефонувати";
$MESS["ELEMENT_MENU_ANSWER"] = "Відповісти";
$MESS["ELEMENT_MENU_SKIP"] = "Пропустити";
$MESS["ELEMENT_MENU_FINISH"] = "Завершити";
$MESS["ELEMENT_MENU_MUTE"] = "Не стежити";
$MESS["ELEMENT_MENU_UNMUTE"] = "Стежити";
$MESS["ELEMENT_MENU_SPAM"] = "Спам";
$MESS["ELEMENT_MENU_ABOUT_CHAT"] = "Про чат";
$MESS["IM_YOU"] = "це ви";
$MESS["IM_YOU_2"] = "Ви:";
?>