<?
$MESS["IM_LIST_CHAT"] = "Privater Chat";
$MESS["IM_LIST_CHAT_OPEN"] = "Öffentlicher Chat";
$MESS["IM_LIST_EMPLOYEE"] = "Mitarbeiter";
$MESS["ELEMENT_MENU_DELETE"] = "Löschen";
$MESS["ELEMENT_MENU_LEAVE"] = "Verlassen";
$MESS["ELEMENT_MENU_HIDE"] = "Ausblenden";
$MESS["ELEMENT_MENU_PIN"] = "Fixieren";
$MESS["ELEMENT_MENU_UNPIN"] = "Lösen";
$MESS["ELEMENT_MENU_CALL"] = "Anrufen";
$MESS["ELEMENT_MENU_ANSWER"] = "Antworten";
$MESS["ELEMENT_MENU_SKIP"] = "Überspringen";
$MESS["ELEMENT_MENU_FINISH"] = "Beenden";
$MESS["ELEMENT_MENU_MUTE"] = "Nicht mehr verfolgen";
$MESS["ELEMENT_MENU_UNMUTE"] = "Verfolgen";
$MESS["ELEMENT_MENU_SPAM"] = "Spam";
$MESS["ELEMENT_MENU_ABOUT_CHAT"] = "Über Chat ";
$MESS["IM_YOU"] = "Das sind Sie";
$MESS["IM_YOU_2"] = "Sie: ";
?>