<?php
$MESS['TASKS_ROLE_VIEW_ALL'] = 'Все задачи';
$MESS['TASKS_ROLE_RESPONSIBLE'] = 'Делаю';
$MESS['TASKS_ROLE_ACCOMPLICE'] = 'Помогаю';
$MESS['TASKS_ROLE_AUDITOR'] = 'Наблюдаю';
$MESS['TASKS_ROLE_ORIGINATOR'] = 'Поручил';