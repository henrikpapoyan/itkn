/**
* @bxjs_lang_path extension.php
*/
(()=>{

	this.ListHolder = {
		Loading:{type:"loading", title: BX.message("HOLDER_LOADING"), selectable:false},
		MoreButton:{type:"button", title: BX.message("HOLDER_LOAD_MORE")},
	};
})();