<?
$MESS["SC_CRM_STORE_CONTAINER_CONTENT"] = "Das CRM stellt nun eine voll funktionsfähige Plattform für den Onlinevertrieb mit moderner Zahlungsverarbeitung, mit den Kassenzetteln und der Lieferung. Je einfacher die Zahlungen funktionieren, umso mehr verkaufen Sie.<br>Aktivieren Sie den CRM-Onlineshop, um Ihre Verkaufszahlen zu erhöhen.";
$MESS["SC_CRM_STORE_CONTAINER_LINK"] = "Details";
$MESS["SC_CRM_STORE_CONTAINER_START_SELL"] = "Verkauf starten";
$MESS["SC_CRM_STORE_CONTAINER_SUB_TITLE"] = "Schauen Sie sich ein Kurzvideo an,<br>um zu erfahren, wie Sie mit dem Bitrix24 CRM-Onlineshop mehr verkaufen können.";
$MESS["SC_CRM_STORE_CONTAINER_TITLE"] = "Eine neue Ära im Onlinevertrieb";
$MESS["SC_CRM_STORE_TITLE"] = "CRM-Onlineshop";
?>