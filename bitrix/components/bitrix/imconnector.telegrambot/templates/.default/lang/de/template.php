<?
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_API_TOKEN"] = "Zugriffstoken:";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_BOT_LINK"] = "Link zum Bot";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_BOT_NAME"] = "Name des Bots";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_CHANGE_ANY_TIME"] = "Kann jederzeit bearbeitet oder abgeschaltet werden";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_CONNECTED"] = "Telegram verbunden";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_CONNECT_BOT"] = "<span class=\"imconnector-field-box-text-bold\">Einen Telegram-Bot verbinden</span>";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_CONNECT_HELP"] = "<div class=\"imconnector-field-button-subtitle\">Ich möchte</div>
				<div class=\"imconnector-field-button-name\">einen Telegram-Bot <span class=\"imconnector-field-box-text-bold\">verbinden</span></div>";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_CONNECT_STEP"] = "Vor der Verbindung müssen Sie <a onclick=\"top.BX.Helper.show('#ID#');\" class=\"imconnector-field-box-link\">einen Telegram-Bot erstellen</a>,
 oder einen Existierenden verbinden. Wir können Ihnen helfen, einen öffentlichen Account zu erstellen und ihn mit Ihrem Bitrix24 zu verbinden.";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_CONNECT_TITLE"] = "Verbinden Sie Telegram mit Ihrem Kommunikationskanal";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_CREATE_BOT"] = "<span class=\"imconnector-field-box-text-bold\">Einen Telegram-Bot erstellen</span>";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_FINAL_FORM_DESCRIPTION"] = "Telegram wurde nun mit Ihrem Kommunikationskanal erfolgreich verbunden. Alle Anfragen, die an Ihren Bot geschickt werden, werden an Ihr Bitrix24 weitergeleitet.";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_INFO"] = "Information";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_INFO_CONNECT_CONNECT_ID"] = "redirect=detail&code=6352401";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_INFO_CONNECT_ID"] = "redirect=detail&code=6352401";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_INFO_CREATE_CONNECT_ID"] = "redirect=detail&code=6352401";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_INSTRUCTION_TITLE"] = "<span class=\"imconnector-field-box-text-bold\">So erhalten Sie einen Schlüssel des öffentllichen Accounts</span>:";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_I_KNOW_TOKEN"] = "Ich weiß Token";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_TESTED"] = "Verbindung testen";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_USING_TITLE"] = "Ich möchte";
?>