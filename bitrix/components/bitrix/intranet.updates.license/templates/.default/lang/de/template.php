<?
$MESS["UPDATES_LICENSE_TITLE"] = "Bitrix24 Lizenz";
$MESS["UPDATES_LICENSE_KEY"] = "Lizenzschlüssel";
$MESS["UPDATES_LICENSE_SAVE"] = "Speichern";
$MESS["UPDATES_LICENSE_ACTIVATE"] = "Aktivieren";
$MESS["UPDATES_ACTIVATE_LICENSE_TITLE"] = "Lizenzschlüssel aktivieren";
$MESS["UPDATES_ACTIVATE_NAME"] = "Voller Name des Besitzers (Unternehmen oder Person)";
$MESS["UPDATES_ACTIVATE_SITE_URL_NEW"] = "Alle Domains inkl. der Testdomains, die<br/>mit dieser Systeminstanz betrieben werden";
$MESS["UPDATES_ACTIVATE_PHONE"] = "Telefonnummer des Besitzers dieser Produktkopie";
$MESS["UPDATES_ACTIVATE_EMAIL"] = "E-Mail-Adresse für Fragen zur Lizenzierung und Nutzung";
$MESS["UPDATES_ACTIVATE_CONTACT_PERSON"] = "Ansprechpartner, verantwortlich für diese Produktkopie";
$MESS["UPDATES_ACTIVATE_CONTACT_EMAIL"] = "E-Mail des Ansprechpartners";
$MESS["UPDATES_ACTIVATE_CONTACT_PHONE"] = "Telefon des Ansprechpartners";
$MESS["UPDATES_ACTIVATE_CONTACT_INFO"] = "Andere wichtige Kontaktdaten";
$MESS["UPDATES_ACTIVATE_SITE_TEXT"] = "Wenn Sie auf der Seite <a href=\"http://www.bitrix.de\" target=\"_blank\">www.bitrix.de</a> noch nicht registriert sind, markieren sie das
<br/>Kästchen \"Nutzer registrieren\" und tragen Ihre Daten in die entsprechenden Felder
<br/>ein (Vor- und Nachname, Loginname und Passwort). Durch die Registrierung auf der
<br/>www.bitrix.de Seite haben Sie Anspruch auf den <a href=\"http://www.bitrix.de/support/\" target=\"_blank\">Technischen Support</a> und auf die
<br/>Mitgliedschaft im <a href=\"http://www.bitrix.de/support/forum/\" target=\"_blank\">internen Forum</a>.";
$MESS["UPDATES_ACTIVATE_GENERATE_USER"] = "Account auf der Seite <a href=\"http://www.bitrix.de\" target=\"_blank\">www.bitrix.de</a> anlegen";
$MESS["UPDATES_ACTIVATE_GENERATE_USER_NO"] = "Ich habe mich bereits auf der Seite registriert und besitze einen Account. Ich möchte meine Daten für den Zugang zum Technischen Support und Downloads nutzen.";
$MESS["UPDATES_ACTIVATE_USER_NAME"] = "Vorname";
$MESS["UPDATES_ACTIVATE_USER_LAST_NAME"] = "Nachname";
$MESS["UPDATES_ACTIVATE_USER_LOGIN_A"] = "Login (3 oder mehr Zeichen)";
$MESS["UPDATES_ACTIVATE_USER_PASSWORD"] = "Passwort (6 oder mehr Zeichen)";
$MESS["UPDATES_ACTIVATE_USER_PASSWORD_CONFIRM"] = "Passwort bestätigen";
$MESS["UPDATES_ACTIVATE_USER_EMAIL"] = "E-Mail";
$MESS["UPDATES_COUPON_TITLE"] = "Gutschein aktivieren";
$MESS["UPDATES_COUPON_KEY"] = "Geben Sie den Gutschein ein:";
$MESS["UPDATES_COUPON_SUCCESS"] = "Der Gutschein wurde erfolgreich angewendet";
$MESS["UPDATES_ACTIVATE_SUCCESS"] = "Der Schlüssel wurde erfolgreich aktiviert";
$MESS["SUP_REGISTERED"] = "Registriert für";
$MESS["SUP_LICENSE_KEY"] = "Lizenzschlüssel";
$MESS["SUP_EDITION"] = "Produktedition";
$MESS["SUP_SITES"] = "Anzahl der Sites";
$MESS["SUP_USERS"] = "Erlaubte Nutzeranzahl";
$MESS["SUP_CURRENT_NUMBER_OF_USERS"] = "; bisherige Nutzer: #NUM#";
$MESS["SUP_CURRENT_NUMBER_OF_USERS1"] = "Aktive Nutzer: #NUM#.";
$MESS["SUP_USERS_IS_NOT_LIMITED"] = "Ihre Lizenz sieht keine Beschränkung für eine maximale Nutzeranzahl vor.";
$MESS["SUP_ACTIVE_TITLE"] = "Lizenzschlüssel ist gültig";
$MESS["SUP_ACTIVE_PERIOD_TO"] = "bis zum #DATE_TO#";
?>