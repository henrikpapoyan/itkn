<?
$MESS["SUPA_AERR_PASSW_CONF"] = "Das Passwort und die Passwortbestätigung unterscheiden sich.";
$MESS["SUPA_AERR_LOGIN1"] = "Loginname für den Account auf <a href=\"http://www.bitrixsoft.com\">www.bitrixsoft.com</a> muss mindesten 3 Zeichen lang sein";
$MESS["SUPA_AERR_EMAIL1"] = "Bitte überprüfen Sie, ob die Kontakt-E-Mail-Adresse richtig ist.";
$MESS["SUPA_AERR_CONTACT_EMAIL1"] = "Bitte überprüfen Sie, ob die Kontakt-E-Mail-Adresse der Kontaktperson richtig ist.";
$MESS["SUPA_AERR_URI"] = "Geben Sie die Adresse der Seite an, für die dieser Lizenzschlüssel benutzt wird";
$MESS["SUPA_AERR_EMAIL"] = "Geben Sie die E-Mail-Adresse für die Kontaktaufnahme ein";
$MESS["SUPA_AERR_FNAME"] = "Nutzername für den Account auf der Seite <a href=\"http://www.bitrixsoft.com\" target=\"_blank\">www.bitrixsoft.com</a> wurde nicht angegeben.";
$MESS["SUPA_AERR_LNAME"] = "Nachname des Account-Eigentümers auf <a href=\"http://www.bitrixsoft.com\" target=\"_blank\">www.bitrixsoft.com</a> wurde nicht definiert";
$MESS["SUPA_AERR_LOGIN"] = "Geben Sie den Loginnamen für  <a href=\"http://www.bitrixsoft.com\">www.bitrixsoft.com</a> ein";
$MESS["SUPA_AERR_NAME"] = "Geben Sie die Firma ein, der der Lizenzschlüssel gehört";
$MESS["SUPA_AERR_PASSW"] = "Geben Sie das Account-Passwort auf <a href=\"http://www.bitrixsoft.com\">www.bitrixsoft.com</a> ein";
$MESS["SUPA_AERR_CONTACT_EMAIL"] = "Geben Sie die E-Mail-Adresse der Kontaktperson ein";
$MESS["SUPA_AERR_CONTACT_PERSON"] = "Geben Sie den Vor- und Nachnamen der Kontaktperson ein.";
$MESS["SUPA_AERR_CONTACT_PHONE"] = "Geben Sie die Telefonnummer der Kontaktperson ein";
$MESS["SUPA_AERR_PHONE"] = "Geben Sie die Telefonnummer des Produkteigentümers ein";
$MESS["SUPA_ACE_CPN"] = "Der Gutscheincode für die Aktivierung wurde nicht angegeben";
$MESS["SUPA_ACE_ACT"] = "Fehler beim Aktivieren des Gutscheins";
?>