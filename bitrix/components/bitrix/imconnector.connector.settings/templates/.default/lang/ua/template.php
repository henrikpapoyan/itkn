<?
$MESS["IIMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_ERROR_ACTION"] = "Виникла помилка. Дію скасовано.";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_LINE_ACTIVATION_BUTTON_ACTIVE"] = "Активувати";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_LINE_ACTIVATION_BUTTON_NO"] = "Ні";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_LINE_ACTIVATION_DESCRIPTION"] = "Дана лінія неактивна. Хочете активувати її?";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_LINE_ACTIVATION_TITLE"] = "Активація лінії";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_QUEUE_DESCRIPTION"] = "Вкажіть відповідальних співробітників (операторів), які будуть відповідати на вхідні звернення у даній Відкритій лінії.";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_CLOSE"] = "Закрити";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_CONFIGURE"] = "налаштувати";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_CONFIGURE_CHANNEL"] = "Налаштування каналу";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_CONNECT"] = "Налаштування каналу";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_CREATE_OPEN_LINE"] = "Створити відкриту лінію";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_LM_ADD1"] = "Додати співробітників";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_LM_ADD2"] = "Додати ще";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_LM_ERROR_BUSINESS"] = "На вашому тарифному плані можна вибрати тільки бізнес користувачів";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_NO_OPEN_LINE"] = "У вас немає жодної відкритої лінії, у якій ви можете налаштувати канали комунікацій. Будь ласка, створіть нову відкриту лінію.";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_NO_OPEN_LINE_AND_NOT_ADD_OPEN_LINE"] = "У вас немає жодної відкритої лінії, у якій ви можете налаштувати канали комунікацій, і у вас немає прав на створення нової відкритої лінії.<br>Зверніться до адміністратора порталу.";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_OPEN_LINE"] = "Відкрита лінія";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_PERMISSIONS"] = "Права доступу";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_POPUP_LIMITED_BUSINESS_USERS_TEXT"] = "На вашому тарифному плані є обмеження на учасників черги.
<br><br>
У черзі можуть бути тільки бізнес-користувачі.
<br><br>
Кількість бізнес-користувачів залежить від тарифу вашого Бітрікс24.";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_QUEUE"] = "Черга відповідальних співробітників";
?>