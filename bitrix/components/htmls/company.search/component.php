<?
if( !defined( "B_PROLOG_INCLUDED" ) || B_PROLOG_INCLUDED !== true ) die();

if( !isset( $arParams["CACHE_TIME"] ) ) {
	$arParams["CACHE_TIME"] = 36000000;
}

if ( $arParams["CACHE_TYPE"] == "Y" || ($arParams["CACHE_TYPE"] == "A" && COption::GetOptionString("main", "component_cache_on", "Y") == "Y" ) ) {
	$arParams["CACHE_TIME"] = intval($arParams["CACHE_TIME"]);
} else {
	$arParams["CACHE_TIME"] = 0;
}

if( !CModule::IncludeModule( 'crm' ) ) {
	return;
}
?>
<?
$arResult	= array();

if( $this->StartResultCache( false ) ) {

	if( $arParams['COMPANY_SEARCH_NAME'] ) {
		define("NO_KEEP_STATISTIC", true);
		global $APPLICATION;
		$APPLICATION->RestartBuffer();

		$sCompanyName = htmlspecialcharsEx( trim( ( !defined( 'BX_UTF' ) || BX_UTF === false ) ? iconv( 'UTF-8', 'WINDOWS-1251', $_REQUEST['COMPANY_SEARCH_NAME'] ) : $_REQUEST['COMPANY_SEARCH_NAME'] ) );

		$aFilter = array( 'TITLE' => "%{$sCompanyName}%", "ACTIVE" => "Y" );

		$arResult['COMPANIES'] = array();
		$oRes = CCrmCompany::GetList( array( 'TITLE' => "ASC" ), $aFilter );
		while( $aCompany = $oRes->Fetch() ) {
			$aItems[$aCompany['ID']] = $aCompany['TITLE'];
		}

		$arResult['COMPANIES'] = $aItems;

		$this->IncludeComponentTemplate( 'ajax' );

		require $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php";
		die();
	}
    /*$arResult['CID']= $arParams['CID'];
    $arResult['CNAME'] = $arParams['CNAME'];*/
	$this->IncludeComponentTemplate();

}
?>