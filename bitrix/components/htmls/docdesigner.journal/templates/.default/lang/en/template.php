<?
$MESS["CT_BNL_ELEMENT_DELETE_CONFIRM"] = "All the information linked to this record will be deleted. Continue anyway?";
$MESS["HTMLS_DOCDESIGNER_OWN_COMPANY"] = "Company";
$MESS["HTMLS_DOCDESIGNER_DOCUMENT"] = "Document";
$MESS["HTMLS_DOCDESIGNER_NUMBER"] = "Number";
$MESS["HTMLS_DOCDESIGNER_DATE"] = "Date";
$MESS["HTMLS_DOCDESIGNER_SUMMA"] = "Sum";
$MESS["HTMLS_DOCDESIGNER_COMPANY"] = "Customer";
$MESS["HTMLS_DOCDESIGNER_DEAL"] = "Deal";
$MESS["HTMLS_DOCDESIGNER_FILE"] = "File";
$MESS["HTMLS_DOCDESIGNER_SELECT_ALL"] = "Применить действие для всех записей в списке";
$MESS["HTMLS_DOCDESIGNER_BUTTON"] = "Delete";
$MESS["HTMLS_DOCDESIGNER_FILTER"] = "Apply";
$MESS["HTMLS_DOCDESIGNER_CANCEL"] = "Cancel";
?>