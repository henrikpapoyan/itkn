<?
$MESS["CT_BNL_ELEMENT_DELETE_CONFIRM"] = "Будет удалена вся информация, связанная с этой записью. Продолжить?";
$MESS["HTMLS_DOCDESIGNER_TEMPLATE_NAME"] = "Название шаблона";
$MESS["HTMLS_DOCDESIGNER_COMMON"] = "Общий";
$MESS["HTMLS_DOCDESIGNER_IBLOCK"] = "Бизнес-процессы";
//$MESS["HTMLS_DOCDESIGNER_DEAL"] = "Дата";
$MESS["HTMLS_DOCDESIGNER_LEAD"] = "Лид";
$MESS["HTMLS_DOCDESIGNER_COMPANY"] = "Компания";
$MESS["HTMLS_DOCDESIGNER_DEAL"] = "Сделка";
$MESS["HTMLS_DOCDESIGNER_CONTACT"] = "Контакт";
$MESS["HTMLS_DOCDESIGNER_SELECT_ALL"] = "Применить действие для всех записей в списке";
$MESS["HTMLS_DOCDESIGNER_BUTTON"] = "Удалить";
$MESS["HTMLS_DOCDESIGNER_SAVE"] = "Сохранить";
$MESS["HTMLS_DOCDESIGNER_RESET"] = "Сбросить";
?>