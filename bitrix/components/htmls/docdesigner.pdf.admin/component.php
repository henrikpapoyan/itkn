<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

global $DB;
if(!CModule::IncludeModule("iblock"))
	return;
?>
<?
if(htmlspecialcharsEx($_REQUEST['apply']) && check_bitrix_sessid()){
	if(htmlspecialcharsEx($_REQUEST['htmls_docdesigner_delete_all_pdf_tmp'])){
		$res = $DB->Query("DELETE FROM htmls_docdesigner_templates");
		//$arFilter = Array("IBLOCK_ID"=>trim($arParams["IBLOCK_ID"]));
	}
	else{
		$res = $DB->Query("DELETE FROM htmls_docdesigner_templates WHERE id IN('".implode(',', htmlspecialcharsEx($_REQUEST['ID']))."')");;
	}
}

if((htmlspecialcharsEx($_REQUEST['apply']) || htmlspecialcharsEx($_REQUEST['save'])) && check_bitrix_sessid()){
    $REQUEST = htmlspecialcharsEx($_REQUEST);
	foreach($REQUEST['tmp_name'] as $tid => $tname){
		$NewCommon = '';
		$NewName = '';
		/*echo $tid."<br>";
			echo "comm old=".$_REQUEST['commonIDold'][$tid]."<br>";
			echo "comm new=".$_REQUEST['commonID'][$tid]."<br>";*/
		if($REQUEST['commonIDold'][$tid] != 'Y' && $REQUEST['commonID'][$tid] == $tid){
			$NewCommon = 'Y';

		}
		if($REQUEST['commonIDold'][$tid] == 'Y' && !$REQUEST['commonID'][$tid]){
			$NewCommon = 'N';
		}
		if($REQUEST['tmp_name'][$tid] != $REQUEST['tmp_name_old'][$tid]){
			$NewName = $REQUEST['tmp_name'][$tid];
		}
		if(!empty($NewName) || !empty($NewCommon)){
			$and = '';
			if(!empty($NewName) && !empty($NewCommon)){
				$and = ', ';
			}
			$query = "UPDATE htmls_docdesigner_templates set ";
			if(!empty($NewName)){
				$query .= "tmp_name='".$NewName."'";
			}
			$query .= $and;
			if(!empty($NewCommon)){
				$query .= "common='".$NewCommon."'";
			}
			$query .= " WHERE id='".$tid."'";
			$res = $DB->Query($query);
		}
	}
}

$res = $DB->Query("select distinct bizproc from htmls_docdesigner_templates order by bizproc");
while($row = $res->Fetch()){
	if(intval($row['bizproc']) > 0){
		$arr['IBLOCK'][] = $row['bizproc'];
	}
	else{
		$pos = strpos($row['bizproc'], '_');
		$arr[substr($row['bizproc'], 0, $pos)][] = substr($row['bizproc'], $pos+1);
	}
}

foreach($arr as $type => $arVal){
	if($type == 'IBLOCK'){
		foreach($arVal as $IBLOCK_ID){
			$Iblock = CIBlock::GetByID($IBLOCK_ID);
			$arIblock = $Iblock->GetNext();
			$arResult[$type][$IBLOCK_ID]['IBLOCK_NAME'] = $arIblock['NAME'];
			$res = $DB->Query("select id, tmp_name, common, bizproc from htmls_docdesigner_templates where bizproc ='".$IBLOCK_ID."' order by id asc");
			while($row = $res->Fetch()){
				$arResult[$type][$IBLOCK_ID][$row['id']] = $row;
			}
		}

	}
	else{
		foreach($arVal as $CRM_ID){
			$res = $DB->Query("select id, tmp_name, common, bizproc from htmls_docdesigner_templates where bizproc ='".$type.'_'.$CRM_ID."' order by id asc");
			while($row = $res->Fetch()){
				$arResult[$type][$CRM_ID][$row['id']] = $row;
			}
		}
	}
}
$this->IncludeComponentTemplate();
?>