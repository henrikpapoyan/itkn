$(document).ready(function(){
	$(".select-soglas").change(function(e){
		console.log('$(".select-soglas").change');
		var id = $(this).data("id");
		var formData = $('#' + id).serialize();

 		$.ajax({
			type: 'POST',
			url: '/ajax/soglasovanie.php', 
			data: formData,  
			dataType: 'json',
			async: true,
			cache: false,
			contentType: 'application/x-www-form-urlencoded',
			processData: true,			
			success: function(result){
				
					if (result.status=='success'){
						console.log(result.message);
					}
					if (result.status=='error'){
						console.log(result.message);
					}


			}
		});
		
		e.preventDefault();	
		return true;
	});

	$(".select-soglas-emelin").change(function(e){
		var id = $(this).data("id");
		var formData = $('#' + id).serialize();

 		$.ajax({
			type: 'POST',
			url: '/ajax/soglasovanie_emelin.php', 
			data: formData,  
			dataType: 'json',
			async: true,
			cache: false,
			contentType: 'application/x-www-form-urlencoded',
			processData: true,			
			success: function(result){
				
					if (result.status=='success'){
						console.log(result.message);
					}
					if (result.status=='error'){
						console.log(result.message);
					}
				
				
			}
		});
		
		e.preventDefault();	
		return true;  
	});

	$(".select-soglas-zhukov").change(function(e){
		var id = $(this).data("id");
		var formData = $('#' + id).serialize();

 		$.ajax({
			type: 'POST',
			url: '/ajax/soglasovanie_zhukov.php', 
			data: formData,  
			dataType: 'json',
			async: true,
			cache: false,
			contentType: 'application/x-www-form-urlencoded',
			processData: true,			
			success: function(result){
				
					if (result.status=='success'){
						console.log(result.message);
					}
					if (result.status=='error'){
						console.log(result.message);
					}

				
			}
		});
		
		e.preventDefault();	
		return true;  
	});


		$(".select-soglas-novikov").change(function(e){
		var id = $(this).data("id");
		var formData = $('#' + id).serialize();

 		$.ajax({
			type: 'POST',
			url: '/ajax/soglasovanie_novikov.php', 
			data: formData,  
			dataType: 'json',
			async: true,
			cache: false,
			contentType: 'application/x-www-form-urlencoded',
			processData: true,			
			success: function(result){
				
					if (result.status=='success'){
						console.log(result.message);
					}
					if (result.status=='error'){
						console.log(result.message);
					}
				
				
			}
		});
		
		e.preventDefault();	
		return true;  
	});


	$(".select-oplata").change(function(e){
		var idCheck = $(this).data("id");
		var formDataChek = $('#' + idCheck).serialize();
		var row = $('#tr_' + idCheck);
		$('#tr_' + idCheck).toggleClass("checked_oplata");

		$.ajax({
			type: 'POST',
			url: '/ajax/oplata.php', 
			data: formDataChek,  
			dataType: 'json',
			async: true,
			cache: false,
			contentType: 'application/x-www-form-urlencoded',
			processData: true,			
			success: function(result){
				
					if (result.status=='success'){
						console.log(result.message);
					}
					if (result.status=='error'){
						console.log(result.message);
					}
				
				
			}
		});
	});

	$("select").change(function() {
		var val149 = $(this).val();
	});

});

$(document).ready(function(){
	var stopAppend = 0;
	var stopFocus = 0;
	$("body").on('focus','input[name="PROPERTY_411[n0][VALUE]"]', function(){
		if (stopFocus == 0) {
			$(this).parent().append("<div class='vnebu db'>Сумму указывать в рос. руб. Копейки указывать через точку (например 123.45)</div>");
			stopFocus = 1;
		}	
	}),

	$("body").on('change','select[name="PROPERTY_223"]', function(){
		valVne = $(this).val();
		parentVne = $(this).parent().parent();
		
		if (stopAppend == 0) {
			$(this).parent().parent().append("<div class='vnebu dn'>Не забудьте прикрепить служебную записку</div>");
			stopAppend = 1;
		}	

		if (valVne == 498) {
			$(".dn").show();
		} else {
			$(".dn").hide();
		}
	});


    function PodrazdelenieShow (arId, property) {
        $(arId).each(function(index, elem){
            $('select[name="PROPERTY_'+ property +'"] option[value="'+ $(elem)[0] +'"]').show();
        });
    }

    function PodrazdelenieHide (arId, property) {
        $(arId).each(function(index, elem){
            $('select[name="PROPERTY_'+ property +'"] option[value="'+ $(elem)[0] +'"]').hide();
        });
    }

    function hideAllSelectElement(idSelectElement){
        $('select[name="PROPERTY_' + idSelectElement +'"] option').hide();
    }

	$("body").on('change','select[name="PROPERTY_211"]', function(){
		var valOrg = $(this).val();

        $('select[name="PROPERTY_212"]').val("");
        $('select[name="PROPERTY_220"]').val("");

		var arRusexp = [478, 479, 480, 481, 482, 483, 484, 485, 486, 507];
		if (valOrg == 477) {

				$(arRusexp).each(function(index, elem){
					$('select[name="PROPERTY_212"] option[value="'+ $(elem)[0] +'"]').show();
				});
				$('select[name="PROPERTY_220"] option[value="851"]').show();
				$('select[name="PROPERTY_220"] option[value="1220"]').show();
		} else {
				$(arRusexp).each(function(index, elem){
					$('select[name="PROPERTY_212"] option[value="'+ $(elem)[0] +'"]').hide();
				});
				$('select[name="PROPERTY_220"] option[value="851"]').hide();
				$('select[name="PROPERTY_220"] option[value="1220"]').hide();
		}
		var arEsk = [508, 509, 510, 511, 512, 513, 514, 515, 516, 517];
		if (valOrg == 476) {
				$(arEsk).each(function(index, elem){
					$('select[name="PROPERTY_212"] option[value="'+ $(elem)[0] +'"]').show();

				});
				$('select[name="PROPERTY_220"] option[value="852"]').show();

		} else {
				$(arEsk).each(function(index, elem){
					$('select[name="PROPERTY_212"] option[value="'+ $(elem)[0] +'"]').hide();

				});
				$('select[name="PROPERTY_220"] option[value="852"]').hide();
		}

		var intergSystem = [874, 875,876,877,878,879,880,881,882,883,889,954,1389,1410,1482,1483];

		if (valOrg == 840) {
			PodrazdelenieShow(intergSystem, 220);
			PodrazdelenieShow([479,955], 212);
		} else {
			PodrazdelenieHide(intergSystem, 220);
			PodrazdelenieHide(955, 212);
		}


	$("body").on('change','select[name="PROPERTY_212"]', function(){

		var valPodrazd = $(this).val();
        var valOrg = $('select[name="PROPERTY_211"]').val();

        $('select[name="PROPERTY_220"]').val("");

		var arEskAdminOtdel = [518, 519, 520, 521, 522, 523];
		var arEskBuhgalter = [524, 525, 526, 527, 528, 870];
		var arEskBiz = [529, 530, 531, 532, 533, 843, 906, 1210, 1454];
		var arEskFEO = [926, 947, 1470, 1150, 1474];
		var arEskDepartEkspl = [534,535,536,537,538,539,1166,1306,1489];
		var arEskIT = [540,541,542,543,544,545];
		var arEskOtdelKadrov = [546,547,548,549,550];
		var arEskUpravSafety = [551,552,553,554,555,556,1414,1430, 1437];
		var arEskApparat = [];
		var arEskUrist = [557,558,559,560];
		var allESK = [844, 1220];

        hideAllSelectElement(220);

        //ЭСК
        if(valOrg == 477) {
            $('select[name="PROPERTY_220"] option[value="852"]').show();

            if (valPodrazd == 478 || valPodrazd == 479 || valPodrazd == 480 || valPodrazd == 482 || valPodrazd == 483 || valPodrazd == 484 || valPodrazd == 485 || valPodrazd == 486 || valPodrazd == 507) {
                PodrazdelenieShow(allESK, 220);
            } else {
                PodrazdelenieHide(allESK, 220);
            }

            if (valPodrazd == 479 || valPodrazd == 513 || valPodrazd == 955) {
                PodrazdelenieShow(889, 220);
            } else {
                PodrazdelenieHide(889, 220);
            }

            if (valPodrazd == 478) {
                PodrazdelenieShow(arEskAdminOtdel, 220);
            } else {
                PodrazdelenieHide(arEskAdminOtdel, 220);
            }
            if (valPodrazd == 479) {
                PodrazdelenieShow(arEskBuhgalter, 220);
            } else {
                PodrazdelenieHide(arEskBuhgalter, 220);
            }
            if (valPodrazd == 480) {
                PodrazdelenieShow(arEskBiz, 220);
            } else {
                PodrazdelenieHide(arEskBiz, 220);
            }
            if (valPodrazd == 481) {
                PodrazdelenieShow(arEskFEO, 220);
            } else {
                PodrazdelenieHide(arEskFEO, 220);
            }
            if (valPodrazd == 482) {
                PodrazdelenieShow(arEskDepartEkspl, 220);
            } else {
                PodrazdelenieHide(arEskDepartEkspl, 220);
            }
            if (valPodrazd == 483) {
                PodrazdelenieShow(arEskIT, 220);
            } else {
                PodrazdelenieHide(arEskIT, 220);
            }
            if (valPodrazd == 484) {
                PodrazdelenieShow(arEskOtdelKadrov, 220);
            } else {
                PodrazdelenieHide(arEskOtdelKadrov, 220);
            }
            if (valPodrazd == 485) {
                PodrazdelenieShow(arEskUpravSafety, 220);
            } else {
                PodrazdelenieHide(arEskUpravSafety, 220);
            }
            if (valPodrazd == 486) {
                PodrazdelenieShow(arEskApparat, 220);
            } else {
                PodrazdelenieHide(arEskApparat, 220);
            }
            if (valPodrazd == 507) {
                PodrazdelenieShow(arEskUrist, 220);
            } else {
                PodrazdelenieHide(arEskUrist, 220);
            }
        }

/* RusExpert begin */
		var arRUSTexDepart = [561,562,563,564,565];
		var arRUSVnedrenie = [566,567,956];
		var arRUSIT = [568,569,570,571,572];
		var arRUSAdmin = [573,574,575,576,577,578];
		var arRUSKadry = [579,580,581,582,583];
		var arRUSBuhgalter = [584,585,586,587,588,871,889];
		var arRUSUrist = [589,590,591,592];
		var arRUSSafety = [593,594,595];
		var arRUSFEO = [883, 1470, 1474, 1478]

		//РусЭкспертиза
        if(valOrg == 476) {
            $('select[name="PROPERTY_220"] option[value="851"]').show();
            //$('select[name="PROPERTY_220"] option[value="1220"]').show();

            if (valPodrazd == 508 || valPodrazd == 509 || valPodrazd == 510 || valPodrazd == 511 || valPodrazd == 512 || valPodrazd == 513 || valPodrazd == 514 || valPodrazd == 515) {
                PodrazdelenieShow(845, 220);
            } else {
                PodrazdelenieHide(845, 220);
            }

            if (valPodrazd == 508) {
                PodrazdelenieShow(arRUSTexDepart, 220);
            } else {
                PodrazdelenieHide(arRUSTexDepart, 220);
            }
            if (valPodrazd == 509) {
                PodrazdelenieShow(arRUSVnedrenie, 220);
            } else {
                PodrazdelenieHide(arRUSVnedrenie, 220);
            }
            if (valPodrazd == 510) {
                PodrazdelenieShow(arRUSIT, 220);
            } else {
                PodrazdelenieHide(arRUSIT, 220);
            }
            if (valPodrazd == 511) {
                PodrazdelenieShow(arRUSAdmin, 220);
            } else {
                PodrazdelenieHide(arRUSAdmin, 220);
            }
            if (valPodrazd == 512) {
                PodrazdelenieShow(arRUSKadry, 220);
            } else {
                PodrazdelenieHide(arRUSKadry, 220);
            }
            if (valPodrazd == 513) {
                PodrazdelenieShow(arRUSBuhgalter, 220);
            } else {
                PodrazdelenieHide(arRUSBuhgalter, 220);
            }
            if (valPodrazd == 514) {
                PodrazdelenieShow(arRUSUrist, 220);
            } else {
                PodrazdelenieHide(arRUSUrist, 220);
            }
            if (valPodrazd == 515) {
                PodrazdelenieShow(arRUSSafety, 220);
            } else {
                PodrazdelenieHide(arRUSSafety, 220);
            }
            if (valPodrazd == 516) {
                PodrazdelenieShow(arRUSFEO, 220);
            } else {
                PodrazdelenieHide(arRUSFEO, 220);
            }
        }
/* RusExpert end */

		var arISBuhgalter = [889,1455,1456,1457,1493];
		var arISIntegrationSystem = [874, 875,876,877,878,879,880,881,882,883,889,954,1389,1410,1482,1483,1487];

		if(valOrg == 840) {
            if (valPodrazd == 479) {
                PodrazdelenieShow(arISBuhgalter, 220);
            } else {
                //PodrazdelenieHide(arISBuhgalter, 220);
            }

            if (valPodrazd == 955) {
                PodrazdelenieShow(arISIntegrationSystem, 220);
            } else {
                //PodrazdelenieHide(arISIntegrationSystem, 220);
            }
        }

});

		$("body").on('change','select[name="PROPERTY_220"]', function(){
			var vid = $('select[name="PROPERTY_221"]');
			var valZatrat = $(this).val();
			var ArendaOffice = [596,597,598,599,600,601];
			var SoderjOffice = [602,603,604,605,606];
			var OfficeRashod = [607,608,609,610,611,612,613,614];
			var RabMesto = [615,616];
			var Kanctovar = [617,618,619,620,621];
			var ProchieRashody = [622,623,624,625,626];
			var arrESKSecurity = [1412, 1413];
			var arrESKInformSafety = [1415,1416,1417,1418,1419,1420,1421,1422, 1423, 1424, 1425, 1426, 1427, 1428, 1429];
            var arrEskLicense = [1431, 1432,1433];
            var arrAutomotizatcia = [1438, 1439, 1440, 1441,1442,1443,1444,1445,1446, 1447, 1448, 1449, 1450, 1451, 1452,1453];
            
            var valP = $('select[name="PROPERTY_212"]').val();
            
            if (valZatrat == 844 && valP == 482) {
               
                    vidShow(846, 221);
                } else {
                    vidHide(846, 221);
                }
            
            if (valZatrat == 844 && valP == 480) {
               
                    vidShow(847, 221);
                } else {
                    vidHide(847, 221);
                }
            
            if (valZatrat == 844 && valP == 478 || valP == 479 || valP == 482 || valP == 483 || valP == 484 || valP == 485 || valP == 486 || valP == 507) {
               
                    vidShow(848, 221);
                } else {
                    vidHide(848, 221);
                }
            
            
            
            
             if (valZatrat == 845 &&  valP == 510 || valP == 511 || valP == 512 || valP == 513 || valP == 514 || valP == 515) {
               
                    vidShow(848, 221);
                } else {
                    vidHide(848, 221);
                }
            
            if (valZatrat == 845 && valP == 508) {
               
                    vidShow(849, 221);
                } else {
                    vidHide(849, 221);
                }
            
            if (valZatrat == 845 && valP == 509) { 
               
                    vidShow(850, 221);
                } else {
                    vidHide(850, 221);
                }
            

			if (valZatrat == 518) {
				vidShow(ArendaOffice, 221);
			} else {
				vidHide(ArendaOffice, 221);
			}
			if (valZatrat == 519) {
				vidShow(SoderjOffice, 221);
			} else {
				vidHide(SoderjOffice, 221);
			}
			if (valZatrat == 520) {
				vidShow(OfficeRashod, 221);
			} else {
				vidHide(OfficeRashod, 221);
			}
			if (valZatrat == 521) {
				vidShow(RabMesto, 221);
			} else {
				vidHide(RabMesto, 221);
			}
			if (valZatrat == 522) {
				vidShow(Kanctovar, 221);
			} else {
				vidHide(Kanctovar, 221);
			}
			if (valZatrat == 523) {
				vidShow(ProchieRashody, 221);
			} else {
				vidHide(ProchieRashody, 221);
			}
			
			if (valZatrat == 551) {
				vidShow(arrESKSecurity, 221);
			} else {
				vidHide(arrESKSecurity, 221);			
			}
			
			if (valZatrat == 1414) {
				vidShow(arrESKInformSafety, 221);
			} else {
				vidHide(arrESKInformSafety, 221);			
			}
			
			if (valZatrat == 1430) {
				vidShow(arrEskLicense, 221);
			} else {
				vidHide(arrEskLicense, 221);			
			}
			
			if (valZatrat == 1437) {
				vidShow(arrAutomotizatcia, 221);
			} else {
				vidHide(arrAutomotizatcia, 221);			
			}
			 /*******************/
			/*******************/

			var TexHelp = [634,635,636,637,638,639,640,1411];
			var ArendaCOD = [641,642,643];
			var COD = [644,645,646,647,648,649,650,651,652,653,654,655,656,657,658];
			var Oborud = [659,660,661,662,663,664,665,666,667,668,669];
			var Exspluatats = [670,671];
			var Prochee = [672];

			if (valZatrat == 534) {
				vidShow(TexHelp, 221);
			} else {
				vidHide(TexHelp, 221);
			}
			if (valZatrat == 535) {
				vidShow(ArendaCOD, 221);
			} else {
				vidHide(ArendaCOD, 221);
			}
			if (valZatrat == 536) {
				vidShow(COD, 221);
			} else {
				vidHide(COD, 221);
			}
			if (valZatrat == 537) {
				vidShow(Oborud, 221);
			} else {
				vidHide(Oborud, 221);
			}
			if (valZatrat == 538) {
				vidShow(Exspluatats, 221);
			} else {
				vidHide(Exspluatats, 221);
			}
			if (valZatrat == 539) {
				vidShow(Prochee, 221);
			} else {
				vidHide(Prochee, 221);
			}

			var Agent = [673];
			var SupportSale = [674,675,676,677,678,679];
			var PRandMarketing = [680,681,682,683,684,685,686,687,688,689,690,691,692];
			if (valZatrat == 529) {
				vidShow(Agent, 221);
			} else {
				vidHide(Agent, 221);
			}
			if (valZatrat == 530) {
				vidShow(SupportSale, 221);
			} else {
				vidHide(SupportSale, 221);
			}
			if (valZatrat == 531) {
				vidShow(PRandMarketing, 221);
			} else {
				vidHide(PRandMarketing, 221);
			}

			var APM = [693,694,695];
			var ServerPO = [696,697,698,699,700,701];
			var PO = [702,703,704,705,706,707,708,709,710,711,712];
			var TexObsluj = [713,714,715,716,1434];
			var Svyaz = [717,718,719,720,721,722, 1435, 1436];
			var NewOffice = [723,724,725,726,727];
			
			if (valZatrat == 540) {
				vidShow(APM, 221);
			} else {
				vidHide(APM, 221);
			}
			if (valZatrat == 541) {
				vidShow(ServerPO, 221);
			} else {
				vidHide(ServerPO, 221);
			}
			if (valZatrat == 542) {
				vidShow(PO, 221);
			} else {
				vidHide(PO, 221);
			}
			if (valZatrat == 543) {
				vidShow(TexObsluj, 221);
			} else {
				vidHide(TexObsluj, 221);
			}
			if (valZatrat == 544) {
				vidShow(Svyaz, 221);
			} else {
				vidHide(Svyaz, 221);
			}
			if (valZatrat == 545) {
				vidShow(NewOffice, 221);
			} else {
				vidHide(NewOffice, 221);
			}

			var Kadr = [728,729,730,731,732,733];
			var Search = [734];
			var Korpor = [735,736,737,738,739,740,741,742];
			var Obuchenie = [743,744,745,746,747,748];
			if (valZatrat == 546) {
				vidShow(Kadr, 221);
			} else {
				vidHide(Kadr, 221);
			}
			if (valZatrat == 547) {
				vidShow(Search, 221);
			} else {
				vidHide(Search, 221);
			}
			if (valZatrat == 548) {
				vidShow(Korpor, 221);
			} else {
				vidHide(Korpor, 221);
			}
			if (valZatrat == 549) {
				vidShow(Obuchenie, 221);
			} else {
				vidHide(Obuchenie, 221);
			}
			
/*******************************/
			var RazrabotkaPO = [749,750,751];
			var TexHelp = [752];
			var Other = [753];
			var Expluatatsiya = [754, 755];

			if (valZatrat == 561) {
			} else {
			}
			if (valZatrat == 562) {
				vidShow(RazrabotkaPO, 221);
			} else {
				vidHide(RazrabotkaPO, 221);
			}
			if (valZatrat == 563) {
				vidShow(TexHelp, 221);
			} else {
				vidHide(TexHelp, 221);
			}
			if (valZatrat == 564) {
				vidShow(Other, 221);
			} else {
				vidHide(Other, 221);
			}
			if (valZatrat == 565) {
				vidShow(Expluatatsiya, 221);
			} else {
				vidHide(Expluatatsiya, 221); 
			}

			var CreateProduct = [756,757,758,759,760];
			var ProjectWork = [761];
			if (valZatrat == 566) {
				vidShow(CreateProduct, 221);
			} else {
				vidHide(CreateProduct, 221); 
			}
			if (valZatrat == 567) {
				vidShow(ProjectWork, 221);
			} else {
				vidHide(ProjectWork, 221); 
			}

			var APMRus = [762,763,764];
			var ServeroborudRus = [765,766,767,768,769,770,771];
			var PoRus = [772,773,774,775,776,777,778];
			var TexObslujTexninki = [779,780,781];
			var rashodNaSvyaz = [782,783,784,785,786];

			if (valZatrat == 568) {
				vidShow(APMRus, 221);
			} else {
				vidHide(APMRus, 221); 
			}
			if (valZatrat == 569) {
				vidShow(ServeroborudRus, 221);
			} else {
				vidHide(ServeroborudRus, 221); 
			}
			if (valZatrat == 570) {
				vidShow(PoRus, 221);
			} else {
				vidHide(PoRus, 221); 
			}
			if (valZatrat == 571) {
				vidShow(TexObslujTexninki, 221);
			} else {
				vidHide(TexObslujTexninki, 221); 
			}
			if (valZatrat == 572) {
				vidShow(rashodNaSvyaz, 221);
			} else {
				vidHide(rashodNaSvyaz, 221); 
			}

			var Arenda = [787,788,789,790];
			var Soderjanie = [791,792,793,794,795];
			var OffRashod = [796,797,798,799,800,801,802,803];
			var RabMesto = [804,805];
			var KancTovary = [806,807,808,809,810];
			var ProcheeRashod = [811,812,813,814,815];

			if (valZatrat == 573) {
				vidShow(Arenda, 221);
			} else {
				vidHide(Arenda, 221); 
			}
			if (valZatrat == 574) {
				vidShow(Soderjanie, 221);
			} else {
				vidHide(Soderjanie, 221); 
			}
			if (valZatrat == 575) {
				vidShow(OffRashod, 221);
			} else {
				vidHide(OffRashod, 221); 
			}
			if (valZatrat == 576) {
				vidShow(RabMesto, 221);
			} else {
				vidHide(RabMesto, 221); 
			}
			if (valZatrat == 577) {
				vidShow(KancTovary, 221);
			} else {
				vidHide(KancTovary, 221); 
			}
			if (valZatrat == 578) {
				vidShow(ProcheeRashod, 221);
			} else {
				vidHide(ProcheeRashod, 221); 
			}

			var KadrDelo = [816,817,818,819,820,821];
			var SearchPerson = [822,823];
			var Korporat = [824,825,826,827,828,829,830,831];
			var Obuchenie = [832,833,834,835,836];

			if (valZatrat == 579) {
				vidShow(KadrDelo, 221);
			} else {
				vidHide(KadrDelo, 221); 
			}
			if (valZatrat == 580) {
				vidShow(SearchPerson, 221);
			} else {
				vidHide(SearchPerson, 221); 
			}
			if (valZatrat == 581) {
				vidShow(Korporat, 221);
			} else {
				vidHide(Korporat, 221); 
			}
			if (valZatrat == 582) {
				vidShow(Obuchenie, 221);
			} else {
				vidHide(Obuchenie, 221); 
			}
			
			var IntegrNaklad = [884,885,886,887,888];
			var DRB = [907, 908]

			if (valZatrat == 879) {
				vidShow(IntegrNaklad, 221);
			} else {
				vidHide(IntegrNaklad, 221); 
			}

			if (valZatrat == 906) {
				vidShow(DRB, 221);
			} else {
				vidHide(DRB, 221); 
			}

			//Документооборот
			var document_flow = [1475, 1476, 1477]

			if (valZatrat == 1474) {
				vidShow(document_flow, 221);
			} else {
				vidHide(document_flow, 221);
			}

			if (valZatrat == 541) {
				vidShow(1490, 221);
			} else {
				vidHide(1490, 221);
			}

			if (valZatrat == 1493) {
				vidShow(1494, 221);
			} else {
				vidHide(1494, 221);
			}


	function vidShow (arId, property) {
			$(arId).each(function(index, elem){
				$('select[name="PROPERTY_'+ property +'"] option[value="'+ $(elem)[0] +'"]').show();
			});
		}
	function vidHide (arId, property) {
			$(arId).each(function(index, elem){
				$('select[name="PROPERTY_'+ property +'"] option[value="'+ $(elem)[0] +'"]').hide();
			});
		}
		});


	});
});