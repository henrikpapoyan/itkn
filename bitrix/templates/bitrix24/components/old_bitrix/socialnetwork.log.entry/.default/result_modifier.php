<?
$arEvent = &$arResult["Event"];

if($posFilterStart = mb_strpos($arEvent["EVENT_FORMATTED"]["MESSAGE"], '~c~')){
	if($posFilterEnd = mb_strpos($arEvent["EVENT_FORMATTED"]["MESSAGE"], '~y')){
		$str = $arEvent["EVENT_FORMATTED"]["MESSAGE"];
		$companyID = mb_substr($str, 0, $posFilterEnd);
		$companyID = mb_substr($companyID, $posFilterStart+3, $posFilterEnd);

		$company = CCrmCompany::GetListEx($arOrder = Array('DATE_CREATE' => 'DESC'), $arFilter = array('ID' => $companyID), $arGroupBy = false, $arNavStartParams = false, $arSelectFields = array(), $arOptions = array());
		$ob = $company->Fetch();

		$newString = mb_substr($str, 0, $posFilterStart);
		$newString .= mb_substr($str, $posFilterEnd+3);

		$arResult["Event"]["EVENT_FORMATTED"]["MESSAGE"] = str_replace('**Компания**', $ob['TITLE'], $newString);
	}
}
?>