<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

/**
 * Bitrix vars
 * @global CUser $USER
 * @global CMain $APPLICATION
 * @global CDatabase $DB
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponent $component
 */

require_once('CCrmViewHelperGK.php');//GK
$APPLICATION->SetAdditionalCSS("/bitrix/themes/.default/crm-entity-show.css");
if(SITE_TEMPLATE_ID === 'bitrix24')
{
	$APPLICATION->SetAdditionalCSS("/bitrix/themes/.default/bitrix24/crm-entity-show.css");
}
if (CModule::IncludeModule('bitrix24') && !\Bitrix\Crm\CallList\CallList::isAvailable())
{
	CBitrix24::initLicenseInfoPopupJS();
}

Bitrix\Main\Page\Asset::getInstance()->addJs('/bitrix/js/crm/activity.js');
Bitrix\Main\Page\Asset::getInstance()->addJs('/bitrix/js/crm/interface_grid.js');
Bitrix\Main\Page\Asset::getInstance()->addJs('/bitrix/js/crm/autorun_proc.js');
Bitrix\Main\Page\Asset::getInstance()->addCss('/bitrix/js/crm/css/autorun_proc.css');

use \Bitrix\Crm\Conversion\LeadConversionScheme;
use \Bitrix\Crm\Category\DealCategory;
use \Bitrix\Crm\Conversion\EntityConverter;

Bitrix\Main\Page\Asset::getInstance()->addCss('/bitrix/components/bitrix/crm.deal_category.panel/templates/.default/style.css');?>
<?
foreach($arResult['LEAD'] as $leadList){
    $arTemp[$leadList['ID']] = $leadList['ID'].' - '.$leadList['DATE_CREATE'].' '.$leadList['LEAD_STATUS_NAME'].'['.$leadList['STATUS_ID'].']  '.' '.$leadList['LEAD_SOURCE_NAME'].'['.$leadList['SOURCE_ID'].']';
    $arLead[$leadList['ID']] = $leadList['STATUS_ID'];
    $arTempLog[$leadList['ID']]= $leadList['ID'];
}
AddMessage2Log(['LEAD_LIST'=>$arTempLog]);

$arSort = array('id'=>'asc');
$arFilter = array(
    'ENTITY_TYPE' => 'LEAD',
    'ENTITY_ID' => array_keys($arTemp),
);

$arOptions = array();
$obRes = CCrmEvent::GetListEx($arSort, $arFilter, false, false, array(), $arOptions);
while ($arEvent = $obRes->Fetch()){
    $arLeadEvents[$arEvent['ENTITY_ID']][] = $arEvent;
}

foreach ($arResult['LEAD'] as $leadID => $leadInfo) {
    $flag = false;
    foreach ($arLeadEvents[$leadID] as $event) {
        if ($event['EVENT_TEXT_1'] == 'Распределение' && $event['EVENT_TEXT_2'] == 'Квалифицирован') {
            $flag = true;
            break;
        }
    }
    if ((bool)($arLead[$leadID] == 'NEW')) {
        $arIdNew[] = $leadID . ' - '. $arTemp[$leadID];
        $arIdNewLog[] = $leadID;
    }
    if ($flag) {
        $arIdPerehod[] = $leadID . ' - ' . $event['EVENT_TEXT_1'].' - '.$event['EVENT_TEXT_2'];
        $arIdPerehodLog[] = $leadID;
    }
    if ($flag || $arLead[$leadID] == 'NEW') {
        if (!in_array($leadID, $arValues['RASPRED'])) {
            $arValues['RASPRED'][] = $leadID;
        }
    }
    else if (!$flag) {
        if (!in_array($leadID, $arValues['LIUBOY_STATUS'])) {
            $arValues['LIUBOY_STATUS'][] = $leadID;
            $arValLuiboyStatus[] = $leadID . ' - ' . $arTemp[$leadID];
            $arValLuiboyStatusLog[] = $leadID;
        }
    }
}
AddMessage2Log(['RASPRED'=>$arIdNewLog,'PEREHOD'=>$arIdPerehodLog,'LIUBOY_STATUS'=>$arValLuiboyStatusLog]);

$channelFieldName = "UF_CHANNEL";
$arFastFilter = array();

if ($arResult["SECTION"] == 'ORDER')
{

    $channelFieldName = "SOURCE_ID";

    $arOrderFastFilter = gk_GetOrderFastFilter(); //GK
    foreach($arResult["FILTER"] as $arProp){
        if($arProp["id"]==$channelFieldName){
            $arFastFilter = $arProp["items"];
        }
    }
    foreach ($arFastFilter as $k => $v) {
        if (isset($arOrderFastFilter[$k]))
        {
            $arFastFilter[$k] = $arOrderFastFilter[$k];
        }
        else
        {
            unset($arFastFilter[$k]);
        }
    }

}
else
{
    $channelFieldName = "UF_CHANNEL";
    foreach($arResult["FILTER"] as $arProp){
        if($arProp["id"]==$channelFieldName){
            $arFastFilter = $arProp["items"];
        }
    }

}
//echo '<pre>'.print_r($arFastFilter, true).'</pre>';
$selFastFilter = 'type_ALL';
if(is_set($arResult["DB_FILTER"]["$channelFieldName"])){
	$companyTypeFilter = $arResult["DB_FILTER"]["$channelFieldName"];
	if(sizeof($companyTypeFilter)==1){
		if (array_key_exists($companyTypeFilter[0], $arFastFilter)) {
			$selFastFilter = "type_".$companyTypeFilter[0];
		}
	}
}
//echo '$selFastFilter ='.$selFastFilter.'<br />';
//echo print_r($arFastFilter)."[[[";

?>
<?
if ($arResult["SECTION"] == 'ORDER') //GK
{
    $APPLICATION->IncludeComponent(
        'itk:crm.order_category.panel',
        '',
        array(
            'selFastFilter' => $selFastFilter,
            'arLeadChannels' => $arFastFilter,
        ),
        $component
    );
}
else
{
    $url_type_setting = '/local/components/itk/crm.lead_type.panel/ajax.php';
    $arFastFilterType = array( 3414 => 'Клиент',
        3415 => 'Партнер',
        3418 => 'Не распределено' );
    $typeFieldName = 'UF_TYPE';
    $selFastFilterType = "type_ALL";

    if(isset($arResult["TYPE_ID"]) && isset($arResult["TYPE_ID"]) > 0)
    {
        $selFastFilterType = "type_".$arResult["TYPE_ID"];


    }
    $APPLICATION->IncludeComponent(
        'itk:crm.lead_type.panel',
        '',
        array(
            'selFastFilter' => $selFastFilterType,
            'arLeadChannels' => $arFastFilterType,
        ),
        $component
    );
    $arFastFilter2 = array();
    foreach($arFastFilter as $ffID => $ffText){
        if($ffText != '1-ОФД'){
            $arFastFilter2[$ffID] = $ffText;
        }
    }
    $APPLICATION->IncludeComponent(
        'itk:crm.lead_category.panel',
        '',
        array(
            'selFastFilter' => $selFastFilter,
            'arLeadChannels' => $arFastFilter2,
        ),
        $component
    );
}
?>
<div id="rebuildMessageWrapper"><?
	if($arResult['NEED_FOR_REBUILD_DUP_INDEX']):
		?><div id="rebuildLeadDupIndexMsg" class="crm-view-message">
		<?=GetMessage('CRM_LEAD_REBUILD_DUP_INDEX', array('#ID#' => 'rebuildLeadDupIndexLink', '#URL#' => '#'))?>
		</div><?
	endif;

	if($arResult['NEED_FOR_REBUILD_SEARCH_CONTENT']):
		?><div id="rebuildLeadSearchWrapper"></div><?
	endif;

	if($arResult['NEED_FOR_REBUILD_LEAD_ATTRS']):
		?><div id="rebuildLeadAttrsMsg" class="crm-view-message">
		<?=GetMessage('CRM_LEAD_REBUILD_ACCESS_ATTRS', array('#ID#' => 'rebuildLeadAttrsLink', '#URL#' => $arResult['PATH_TO_PRM_LIST']))?>
		</div><?
	endif;
	?></div><?

if(isset($arResult['ERROR_HTML'])):
	ShowError($arResult['ERROR_HTML']);
endif;

$isInternal = $arResult['INTERNAL'];
$callListUpdateMode = $arResult['CALL_LIST_UPDATE_MODE'];
$allowWrite = $arResult['PERMS']['WRITE'] && !$callListUpdateMode;
$allowDelete = $arResult['PERMS']['DELETE'] && !$callListUpdateMode;
$currentUserID = $arResult['CURRENT_USER_ID'];
$activityEditorID = '';
if(!$isInternal):
	$activityEditorID = "{$arResult['GRID_ID']}_activity_editor";
	$APPLICATION->IncludeComponent(
		'bitrix:crm.activity.editor',
		'',
		array(
			'EDITOR_ID' => $activityEditorID,
			'PREFIX' => $arResult['GRID_ID'],
			'OWNER_TYPE' => 'LEAD',
			'OWNER_ID' => 0,
			'READ_ONLY' => false,
			'ENABLE_UI' => false,
			'ENABLE_TOOLBAR' => false
		),
		null,
		array('HIDE_ICONS' => 'Y')
	);
endif;

$gridManagerID = $arResult['GRID_ID'].'_MANAGER';
$gridManagerCfg = array(
	'ownerType' => 'LEAD',
	'gridId' => $arResult['GRID_ID'],
	'formName' => "form_{$arResult['GRID_ID']}",
	'allRowsCheckBoxId' => "actallrows_{$arResult['GRID_ID']}",
	'activityEditorId' => $activityEditorID,
	'serviceUrl' => '/bitrix/components/bitrix/crm.activity.editor/ajax.php?siteID='.SITE_ID.'&'.bitrix_sessid_get(),
	'listServiceUrl' => '/bitrix/components/bitrix/crm.lead.list/list.ajax.php?siteID='.SITE_ID.'&'.bitrix_sessid_get(),
	'filterFields' => array(),
	'userFilterHash' => $arResult['DB_FILTER_HASH'],
	'enableIterativeDeletion' => true,
	'messages' => array(
		'deletionDialogTitle' => GetMessage('CRM_LEAD_LIST_DEL_PROC_DLG_TITLE'),
		'deletionDialogSummary' => GetMessage('CRM_LEAD_LIST_DEL_PROC_DLG_SUMMARY')
	)
);

echo CCrmViewHelper::RenderLeadStatusSettings();
$prefix = $arResult['GRID_ID'];

$arResult['GRID_DATA'] = array();
$arColumns = array();
foreach ($arResult['HEADERS'] as $arHead)
	$arColumns[$arHead['id']] = false;

$now = time() + CTimeZone::GetOffset();

foreach($arResult['LEAD'] as $sKey => $arLead)
{
	$arActivityMenuItems = array();
	$arActivitySubMenuItems = array();
	$arActions = array();

	$arActions[] = array(
		'TITLE' => GetMessage('CRM_LEAD_SHOW_TITLE'),
		'TEXT' => GetMessage('CRM_LEAD_SHOW'),
		'ONCLICK' => "jsUtils.Redirect([], '".CUtil::JSEscape($arLead['PATH_TO_LEAD_SHOW'])."');",
		'DEFAULT' => true
	);
	if($arLead['EDIT'])
	{
		$arActions[] = array(
			'TITLE' => GetMessage('CRM_LEAD_EDIT_TITLE'),
			'TEXT' => GetMessage('CRM_LEAD_EDIT'), 'ONCLICK' => "jsUtils.Redirect([], '".CUtil::JSEscape($arLead['PATH_TO_LEAD_EDIT'])."');"
		);
		$arActions[] = array(
			'TITLE' => GetMessage('CRM_LEAD_COPY_TITLE'),
			'TEXT' => GetMessage('CRM_LEAD_COPY'),
			'ONCLICK' => "jsUtils.Redirect([], '".CUtil::JSEscape($arLead['PATH_TO_LEAD_COPY'])."');"
		);
	}

	if(!$isInternal && $arLead['DELETE'])
	{
		$pathToRemove = CUtil::JSEscape($arLead['PATH_TO_LEAD_DELETE']);
		$arActions[] = array(
			'TITLE' => GetMessage('CRM_LEAD_DELETE_TITLE'),
			'TEXT' => GetMessage('CRM_LEAD_DELETE'),
			'ONCLICK' => "BX.CrmUIGridExtension.processMenuCommand(
					'{$gridManagerID}', 
					BX.CrmUIGridMenuCommand.remove, 
					{ pathToRemove: '{$pathToRemove}' }
				)"
		);
	}

	$arActions[] = array('SEPARATOR' => true);

	if(!$isInternal)
	{
		if($arLead["CAN_CONVERT"] && $arResult['CAN_CONVERT'])
		{
			$isReturnCustomer = $arLead['IS_RETURN_CUSTOMER'] == 'Y';
			$arSchemeDescriptions = \Bitrix\Crm\Conversion\LeadConversionManager::create(array(
				'IS_RC' => $isReturnCustomer
			))->getSchemeJsDescriptions(true);
			//$arSchemeDescriptions = \Bitrix\Crm\Conversion\LeadConversionScheme::getJavaScriptDescriptions(true);
			$arSchemeList = array();
			foreach($arSchemeDescriptions as $name => $description)
			{
				$arSchemeList[] = array(
					'TITLE' => $description,
					'TEXT' => $description,
					'ONCLICK' => "BX.CrmLeadConverter.getCurrent().convert({$arLead['ID']}, BX.CrmLeadConversionScheme.createConfig('{$name}'), '".CUtil::JSEscape($APPLICATION->GetCurPage())."');"
				);
			}
			if(!empty($arSchemeList))
			{
				if (!$isReturnCustomer)
				{
					$arSchemeList[] = array(
						'TITLE' => GetMessage('CRM_LEAD_CONV_OPEN_ENTITY_SEL'),
						'TEXT' => GetMessage('CRM_LEAD_CONV_OPEN_ENTITY_SEL'),
						'ONCLICK' => "BX.CrmLeadConverter.getCurrent().openEntitySelector(function(result){ BX.CrmLeadConverter.getCurrent().convert({$arLead['ID']}, result.config, '".CUtil::JSEscape($APPLICATION->GetCurPage())."', result.data); });"
					);
				}

				$arActions[] = array('SEPARATOR' => true);
				$arActions[] = array(
					'TITLE' => GetMessage('CRM_LEAD_CREATE_ON_BASIS_TITLE'),
					'TEXT' => GetMessage('CRM_LEAD_CREATE_ON_BASIS'),
					'MENU' => $arSchemeList
				);
			}
		}

		$arActions[] = array('SEPARATOR' => true);
	}

	if(!$isInternal)
	{
		$arActions[] = $arActivityMenuItems[] = array(
			'TITLE' => GetMessage('CRM_LEAD_EVENT_TITLE'),
			'TEXT' => GetMessage('CRM_LEAD_EVENT'),
			'ONCLICK' => "BX.CrmUIGridExtension.processMenuCommand(
				'{$gridManagerID}', 
				BX.CrmUIGridMenuCommand.createEvent, 
				{ entityTypeName: BX.CrmEntityType.names.lead, entityId: {$arLead['ID']} }
			)"
		);

		if($arLead['EDIT'])
		{
			if(IsModuleInstalled('subscribe'))
			{
				$arActions[] = array(
					'TITLE' => GetMessage('CRM_LEAD_ADD_EMAIL_TITLE'),
					'TEXT' => GetMessage('CRM_LEAD_ADD_EMAIL'),
					'ONCLICK' => "BX.CrmUIGridExtension.processMenuCommand(
						'{$gridManagerID}', 
						BX.CrmUIGridMenuCommand.createActivity, 
						{ typeId: BX.CrmActivityType.email, settings: { ownerID: {$arLead['ID']} } }
					)"
				);

				$arActivityMenuItems[] = array(
					'TITLE' => GetMessage('CRM_LEAD_ADD_EMAIL_TITLE'),
					'TEXT' => GetMessage('CRM_LEAD_ADD_EMAIL'),
					'ONCLICK' => "BX.CrmUIGridExtension.processMenuCommand(
						'{$gridManagerID}', 
						BX.CrmUIGridMenuCommand.createActivity, 
						{ typeId: BX.CrmActivityType.email, settings: { ownerID: {$arLead['ID']} } }
					)"
				);
			}

			if(IsModuleInstalled(CRM_MODULE_CALENDAR_ID))
			{
				$arActivityMenuItems[] = array(
					'TITLE' => GetMessage('CRM_LEAD_ADD_CALL_TITLE'),
					'TEXT' => GetMessage('CRM_LEAD_ADD_CALL'),
					'ONCLICK' => "BX.CrmUIGridExtension.processMenuCommand(
						'{$gridManagerID}', 
						BX.CrmUIGridMenuCommand.createActivity, 
						{ typeId: BX.CrmActivityType.call, settings: { ownerID: {$arLead['ID']} } }
					)"
				);

				$arActivityMenuItems[] = array(
					'TITLE' => GetMessage('CRM_LEAD_ADD_MEETING_TITLE'),
					'TEXT' => GetMessage('CRM_LEAD_ADD_MEETING'),
					'ONCLICK' => "BX.CrmUIGridExtension.processMenuCommand(
						'{$gridManagerID}', 
						BX.CrmUIGridMenuCommand.createActivity, 
						{ typeId: BX.CrmActivityType.meeting, settings: { ownerID: {$arLead['ID']} } }
					)"
				);

				$arActivitySubMenuItems[] = array(
					'TITLE' => GetMessage('CRM_LEAD_ADD_CALL_TITLE'),
					'TEXT' => GetMessage('CRM_LEAD_ADD_CALL_SHORT'),
					'ONCLICK' => "BX.CrmUIGridExtension.processMenuCommand(
						'{$gridManagerID}', 
						BX.CrmUIGridMenuCommand.createActivity, 
						{ typeId: BX.CrmActivityType.call, settings: { ownerID: {$arLead['ID']} } }
					)"
				);

				$arActivitySubMenuItems[] = array(
					'TITLE' => GetMessage('CRM_LEAD_ADD_MEETING_TITLE'),
					'TEXT' => GetMessage('CRM_LEAD_ADD_MEETING_SHORT'),
					'ONCLICK' => "BX.CrmUIGridExtension.processMenuCommand(
						'{$gridManagerID}', 
						BX.CrmUIGridMenuCommand.createActivity, 
						{ typeId: BX.CrmActivityType.meeting, settings: { ownerID: {$arLead['ID']} } }
					)"
				);

				if(IsModuleInstalled('tasks'))
				{
					$arActivityMenuItems[] = array(
						'TITLE' => GetMessage('CRM_LEAD_TASK_TITLE'),
						'TEXT' => GetMessage('CRM_LEAD_TASK'),
						'ONCLICK' => "BX.CrmUIGridExtension.processMenuCommand(
						'{$gridManagerID}', 
						BX.CrmUIGridMenuCommand.createActivity, 
						{ typeId: BX.CrmActivityType.task, settings: { ownerID: {$arLead['ID']} } }
					)"
					);

					$arActivitySubMenuItems[] = array(
						'TITLE' => GetMessage('CRM_LEAD_TASK_TITLE'),
						'TEXT' => GetMessage('CRM_LEAD_TASK_SHORT'),
						'ONCLICK' => "BX.CrmUIGridExtension.processMenuCommand(
						'{$gridManagerID}', 
						BX.CrmUIGridMenuCommand.createActivity, 
						{ typeId: BX.CrmActivityType.task, settings: { ownerID: {$arLead['ID']} } }
					)"
					);
				}

				if(!empty($arActivitySubMenuItems))
				{
					$arActions[] = array(
						'TITLE' => GetMessage('CRM_LEAD_ADD_ACTIVITY_TITLE'),
						'TEXT' => GetMessage('CRM_LEAD_ADD_ACTIVITY'),
						'MENU' => $arActivitySubMenuItems
					);
				}
			}

			if(IsModuleInstalled('sale'))
			{
				$arActions[] = array(
					'TITLE' => GetMessage('CRM_LEAD_ADD_QUOTE_TITLE'),
					'TEXT' => GetMessage('CRM_LEAD_ADD_QUOTE'),
					'ONCLICK' => "jsUtils.Redirect([], '".CUtil::JSEscape($arLead['PATH_TO_QUOTE_ADD'])."');"
				);
			}
			if($arResult['IS_BIZPROC_AVAILABLE'])
			{
				$arActions[] = array('SEPARATOR' => true);
				if(isset($arContact['PATH_TO_BIZPROC_LIST']) && $arContact['PATH_TO_BIZPROC_LIST'] !== '')
				{
					$arActions[] = array(
						'TITLE' => GetMessage('CRM_LEAD_BIZPROC_TITLE'),
						'TEXT' => GetMessage('CRM_LEAD_BIZPROC'),
						'ONCLICK' => "jsUtils.Redirect([], '".CUtil::JSEscape($arLead['PATH_TO_BIZPROC_LIST'])."');"
					);
				}
				if(!empty($arLead['BIZPROC_LIST']))
				{
					$arBizprocList = array();
					foreach($arLead['BIZPROC_LIST'] as $arBizproc)
					{
						$arBizprocList[] = array(
							'TITLE' => $arBizproc['DESCRIPTION'],
							'TEXT' => $arBizproc['NAME'],
							'ONCLICK' => "jsUtils.Redirect([], '".CUtil::JSEscape($arBizproc['PATH_TO_BIZPROC_START'])."');"
						);
					}
					$arActions[] = array(
						'TITLE' => GetMessage('CRM_LEAD_BIZPROC_LIST_TITLE'),
						'TEXT' => GetMessage('CRM_LEAD_BIZPROC_LIST'),
						'MENU' => $arBizprocList
					);
				}
			}
		}
	}

	$eventParam = array(
		'ID' => $arLead['ID'],
		'CALL_LIST_ID' => $arResult['CALL_LIST_ID'],
		'CALL_LIST_CONTEXT' => $arResult['CALL_LIST_CONTEXT'],
		'GRID_ID' => $arResult['GRID_ID']
	);
	foreach(GetModuleEvents('crm', 'onCrmLeadListItemBuildMenu', true) as $event)
	{
		ExecuteModuleEventEx($event, array('CRM_LEAD_LIST_MENU', $eventParam, &$arActions));
	}

    $resultItem = array(
        'id' => $arLead['ID'],
        'actions' => $arActions,
        'data' => $arLead,
        'editable' => !$arLead['EDIT'] ? $arColumns : true,
        'columns' => array(
                //'LEAD_SUMMARY' => CCrmViewHelper::RenderInfo($arLead['PATH_TO_LEAD_SHOW'], isset($arLead['TITLE']) ? $arLead['TITLE'] : ('['.$arLead['ID'].']'), $arLead['LEAD_SOURCE_NAME'], '_self'),
                'COMMENTS' => htmlspecialcharsback($arLead['COMMENTS']),
                'ADDRESS' => nl2br($arLead['ADDRESS']),
                'ASSIGNED_BY' => $arLead['~ASSIGNED_BY_ID'] > 0
                    ? CCrmViewHelper::PrepareUserBaloonHtml(
                        array(
                            'PREFIX' => "LEAD_{$arLead['~ID']}_RESPONSIBLE",
                            'USER_ID' => $arLead['~ASSIGNED_BY_ID'],
                            'USER_NAME'=> $arLead['ASSIGNED_BY'],
                            'USER_PROFILE_URL' => $arLead['PATH_TO_USER_PROFILE']
                        )
                    ) : '',
                'STATUS_DESCRIPTION' => nl2br($arLead['STATUS_DESCRIPTION']),
                'SOURCE_DESCRIPTION' => nl2br($arLead['SOURCE_DESCRIPTION']),
                'DATE_CREATE' => $arLead['DATE_CREATE'],
                /*'DATE_CREATE' => FormatDate($arResult['TIME_FORMAT'], MakeTimeStamp($arLead['DATE_CREATE']), $now),*/
                /*'DATE_MODIFY' => FormatDate($arResult['TIME_FORMAT'], MakeTimeStamp($arLead['DATE_MODIFY']), $now),*/
                'DATE_MODIFY' => $arLead['DATE_MODIFY'],
                'SUM' => $arLead['FORMATTED_OPPORTUNITY'],
                'OPPORTUNITY' => $arLead['~OPPORTUNITY'],
                'CURRENCY_ID' => CCrmCurrency::GetCurrencyName($arLead['~CURRENCY_ID']),
                'PRODUCT_ID' => isset($arLead['PRODUCT_ROWS']) ? htmlspecialcharsbx(CCrmProductRow::RowsToString($arLead['PRODUCT_ROWS'])) : '',
                'IS_RETURN_CUSTOMER' => isset($arResult['BOOLEAN_VALUES_LIST'][$arLead['IS_RETURN_CUSTOMER']]) ? $arResult['BOOLEAN_VALUES_LIST'][$arLead['IS_RETURN_CUSTOMER']] : $arLead['IS_RETURN_CUSTOMER'],
                'STATUS_ID' => CCrmViewHelperGK::RenderLeadStatusControl(
                    array(
                        'PREFIX' => "{$arResult['GRID_ID']}_PROGRESS_BAR_",
                        'ENTITY_ID' => $arLead['~ID'],
                        'CURRENT_ID' => $arLead['~STATUS_ID'],
                        'SERVICE_URL' => '/bitrix/components/bitrix/crm.lead.list/list.ajax.php',
                        'CONVERSION_SCHEME' => $arResult['CONVERSION_SCHEME'],
                        'READ_ONLY' => !(isset($arLead['EDIT']) && $arLead['EDIT'] === true),
                        'UF_AUTO' => ($arResult['SECTION'] == 'ORDER') ? "N" : $arLead['UF_AUTO'] //GK
                    )
                ),
                'SOURCE_ID' => $arLead['LEAD_SOURCE_NAME'],
                'WEBFORM_ID' => isset($arResult['WEBFORM_LIST'][$arLead['WEBFORM_ID']]) ? $arResult['WEBFORM_LIST'][$arLead['WEBFORM_ID']] : $arLead['WEBFORM_ID'],
                'CREATED_BY' => $arLead['~CREATED_BY'] > 0
                    ? CCrmViewHelper::PrepareUserBaloonHtml(
                        array(
                            'PREFIX' => "LEAD_{$arLead['~ID']}_CREATOR",
                            'USER_ID' => $arLead['~CREATED_BY'],
                            'USER_NAME'=> $arLead['CREATED_BY_FORMATTED_NAME'],
                            'USER_PROFILE_URL' => $arLead['PATH_TO_USER_CREATOR'],
                        )
                    ) : '',
                'MODIFY_BY' => $arLead['~MODIFY_BY'] > 0
                    ? CCrmViewHelper::PrepareUserBaloonHtml(
                        array(
                            'PREFIX' => "LEAD_{$arLead['~ID']}_MODIFIER",
                            'USER_ID' => $arLead['~MODIFY_BY'],
                            'USER_NAME'=> $arLead['MODIFY_BY_FORMATTED_NAME'],
                            'USER_PROFILE_URL' => $arLead['PATH_TO_USER_MODIFIER']
                        )
                    ) : '',
            ) + CCrmViewHelper::RenderListMultiFields($arLead, "LEAD_{$arLead['ID']}_", array('ENABLE_SIP' => true, 'SIP_PARAMS' => array('ENTITY_TYPE' => 'CRM_'.CCrmOwnerType::LeadName, 'ENTITY_ID' => $arLead['ID']))) + $arResult['LEAD_UF'][$sKey]
    );

	if(isset($arLead['~BIRTHDATE']))
	{
		$resultItem['columns']['BIRTHDATE'] = '<nobr>'.FormatDate('SHORT', MakeTimeStamp($arLead['~BIRTHDATE'])).'</nobr>';
	}

	$userActivityID = isset($arLead['~ACTIVITY_ID']) ? intval($arLead['~ACTIVITY_ID']) : 0;
	$commonActivityID = isset($arLead['~C_ACTIVITY_ID']) ? intval($arLead['~C_ACTIVITY_ID']) : 0;
	if($userActivityID > 0)
	{
		$resultItem['columns']['ACTIVITY_ID'] = CCrmViewHelper::RenderNearestActivity(
			array(
				'ENTITY_TYPE_NAME' => CCrmOwnerType::ResolveName(CCrmOwnerType::Lead),
				'ENTITY_ID' => $arLead['~ID'],
				'ENTITY_RESPONSIBLE_ID' => $arLead['~ASSIGNED_BY'],
				'GRID_MANAGER_ID' => $gridManagerID,
				'ACTIVITY_ID' => $userActivityID,
				'ACTIVITY_SUBJECT' => isset($arLead['~ACTIVITY_SUBJECT']) ? $arLead['~ACTIVITY_SUBJECT'] : '',
				'ACTIVITY_TIME' => isset($arLead['~ACTIVITY_TIME']) ? $arLead['~ACTIVITY_TIME'] : '',
				'ACTIVITY_EXPIRED' => isset($arLead['~ACTIVITY_EXPIRED']) ? $arLead['~ACTIVITY_EXPIRED'] : '',
				'ALLOW_EDIT' => $arLead['EDIT'],
				'MENU_ITEMS' => $arActivityMenuItems,
				'USE_GRID_EXTENSION' => true
			)
		);

		$counterData = array(
			'CURRENT_USER_ID' => $currentUserID,
			'ENTITY' => $arLead,
			'ACTIVITY' => array(
				'RESPONSIBLE_ID' => $currentUserID,
				'TIME' => isset($arLead['~ACTIVITY_TIME']) ? $arLead['~ACTIVITY_TIME'] : '',
				'IS_CURRENT_DAY' => isset($arLead['~ACTIVITY_IS_CURRENT_DAY']) ? $arLead['~ACTIVITY_IS_CURRENT_DAY'] : false
			)
		);

		if(CCrmUserCounter::IsReckoned(CCrmUserCounter::CurrentLeadActivies, $counterData))
		{
			$resultItem['columnClasses'] = array('ACTIVITY_ID' => 'crm-list-deal-today');
		}
	}
	elseif($commonActivityID > 0)
	{
		$resultItem['columns']['ACTIVITY_ID'] = CCrmViewHelper::RenderNearestActivity(
			array(
				'ENTITY_TYPE_NAME' => CCrmOwnerType::ResolveName(CCrmOwnerType::Lead),
				'ENTITY_ID' => $arLead['~ID'],
				'ENTITY_RESPONSIBLE_ID' => $arLead['~ASSIGNED_BY'],
				'GRID_MANAGER_ID' => $gridManagerID,
				'ACTIVITY_ID' => $commonActivityID,
				'ACTIVITY_SUBJECT' => isset($arLead['~C_ACTIVITY_SUBJECT']) ? $arLead['~C_ACTIVITY_SUBJECT'] : '',
				'ACTIVITY_TIME' => isset($arLead['~C_ACTIVITY_TIME']) ? $arLead['~C_ACTIVITY_TIME'] : '',
				'ACTIVITY_RESPONSIBLE_ID' => isset($arLead['~C_ACTIVITY_RESP_ID']) ? intval($arLead['~C_ACTIVITY_RESP_ID']) : 0,
				'ACTIVITY_RESPONSIBLE_LOGIN' => isset($arLead['~C_ACTIVITY_RESP_LOGIN']) ? $arLead['~C_ACTIVITY_RESP_LOGIN'] : '',
				'ACTIVITY_RESPONSIBLE_NAME' => isset($arLead['~C_ACTIVITY_RESP_NAME']) ? $arLead['~C_ACTIVITY_RESP_NAME'] : '',
				'ACTIVITY_RESPONSIBLE_LAST_NAME' => isset($arLead['~C_ACTIVITY_RESP_LAST_NAME']) ? $arLead['~C_ACTIVITY_RESP_LAST_NAME'] : '',
				'ACTIVITY_RESPONSIBLE_SECOND_NAME' => isset($arLead['~C_ACTIVITY_RESP_SECOND_NAME']) ? $arLead['~C_ACTIVITY_RESP_SECOND_NAME'] : '',
				'NAME_TEMPLATE' => $arParams['NAME_TEMPLATE'],
				'PATH_TO_USER_PROFILE' => $arParams['PATH_TO_USER_PROFILE'],
				'ALLOW_EDIT' => $arLead['EDIT'],
				'MENU_ITEMS' => $arActivityMenuItems,
				'USE_GRID_EXTENSION' => true
			)
		);
	}
	else
	{
		$resultItem['columns']['ACTIVITY_ID'] = CCrmViewHelper::RenderNearestActivity(
			array(
				'ENTITY_TYPE_NAME' => CCrmOwnerType::ResolveName(CCrmOwnerType::Lead),
				'ENTITY_ID' => $arLead['~ID'],
				'ENTITY_RESPONSIBLE_ID' => $arLead['~ASSIGNED_BY'],
				'GRID_MANAGER_ID' => $gridManagerID,
				'ALLOW_EDIT' => $arLead['EDIT'],
				'MENU_ITEMS' => $arActivityMenuItems,
				'USE_GRID_EXTENSION' => true
			)
		);

		$counterData = array(
			'CURRENT_USER_ID' => $currentUserID,
			'ENTITY' => $arLead
		);

		if(CCrmUserCounter::IsReckoned(CCrmUserCounter::CurrentLeadActivies, $counterData))
		{
			$resultItem['columnClasses'] = array('ACTIVITY_ID' => 'crm-list-enitity-action-need');
		}
	}

	$arResult['GRID_DATA'][] = &$resultItem;
	unset($resultItem);
}

//region Action Panel
$controlPanel = array('GROUPS' => array(array('ITEMS' => array())));

if(!$isInternal
	&& ($allowWrite || $allowDelete || $callListUpdateMode))
{
	$yesnoList = array(
		array('NAME' => GetMessage('MAIN_YES'), 'VALUE' => 'Y'),
		array('NAME' => GetMessage('MAIN_NO'), 'VALUE' => 'N')
	);

	$snippet = new \Bitrix\Main\Grid\Panel\Snippet();
	$applyButton = $snippet->getApplyButton(
		array(
			'ONCHANGE' => array(
				array(
					'ACTION' => Bitrix\Main\Grid\Panel\Actions::CALLBACK,
					'DATA' => array(array('JS' => "BX.CrmUIGridExtension.processApplyButtonClick('{$gridManagerID}')"))
				)
			)
		)
	);

	$actionList = array(array('NAME' => GetMessage('CRM_LEAD_LIST_CHOOSE_ACTION'), 'VALUE' => 'none'));

	if($allowWrite)
	{
		//region Add Email
		if (IsModuleInstalled('subscribe'))
		{
			$actionList[] = array(
				'NAME' => GetMessage('CRM_LEAD_SUBSCRIBE'),
				'VALUE' => 'subscribe',
				'ONCHANGE' => array(
					array(
						'ACTION' => Bitrix\Main\Grid\Panel\Actions::CREATE,
						'DATA' => array($applyButton)
					),
					array(
						'ACTION' => Bitrix\Main\Grid\Panel\Actions::CALLBACK,
						'DATA' => array(array('JS' => "BX.CrmUIGridExtension.processActionChange('{$gridManagerID}', 'subscribe')"))
					)
				)
			);
		}
		//endregion
		//region Add Task
		if (IsModuleInstalled('tasks'))
		{
			$actionList[] = array(
				'NAME' => GetMessage('CRM_LEAD_TASK'),
				'VALUE' => 'tasks',
				'ONCHANGE' => array(
					array(
						'ACTION' => Bitrix\Main\Grid\Panel\Actions::CREATE,
						'DATA' => array($applyButton)
					),
					array(
						'ACTION' => Bitrix\Main\Grid\Panel\Actions::CALLBACK,
						'DATA' => array(array('JS' => "BX.CrmUIGridExtension.processActionChange('{$gridManagerID}', 'tasks')"))
					)
				)
			);
		}
		//endregion
		//region Set Status
		$statusList = array(array('NAME' => GetMessage('CRM_STATUS_INIT'), 'VALUE' => ''));
		foreach($arResult['STATUS_LIST_WRITE'] as $statusID => $statusName)
		{
			$statusList[] = array('NAME' => $statusName, 'VALUE' => $statusID);
		}
		$actionList[] = array(
			'NAME' => GetMessage('CRM_LEAD_SET_STATUS'),
			'VALUE' => 'set_status',
			'ONCHANGE' => array(
				array(
					'ACTION' => Bitrix\Main\Grid\Panel\Actions::CREATE,
					'DATA' => array(
						array(
							'TYPE' => Bitrix\Main\Grid\Panel\Types::DROPDOWN,
							'ID' => 'action_status_id',
							'NAME' => 'ACTION_STATUS_ID',
							'ITEMS' => $statusList
						),
						$applyButton
					)
				),
				array(
					'ACTION' => Bitrix\Main\Grid\Panel\Actions::CALLBACK,
					'DATA' => array(array('JS' => "BX.CrmUIGridExtension.processActionChange('{$gridManagerID}', 'set_status')"))
				)
			)
		);
		//endregion
		//region Assign To
		//region Render User Search control
		if(!Bitrix\Main\Grid\Context::isInternalRequest())
		{
			//action_assigned_by_search + _control
			//Prefix control will be added by main.ui.grid
			$APPLICATION->IncludeComponent(
				'bitrix:intranet.user.selector.new',
				'',
				array(
					'MULTIPLE' => 'N',
					'NAME' => "{$prefix}_ACTION_ASSIGNED_BY",
					'INPUT_NAME' => 'action_assigned_by_search_control',
					'SHOW_EXTRANET_USERS' => 'NONE',
					'POPUP' => 'Y',
					'SITE_ID' => SITE_ID,
					'NAME_TEMPLATE' => $arResult['NAME_TEMPLATE']
				),
				null,
				array('HIDE_ICONS' => 'Y')
			);
		}
		//endregion
		$actionList[] = array(
			'NAME' => GetMessage('CRM_LEAD_ASSIGN_TO'),
			'VALUE' => 'assign_to',
			'ONCHANGE' => array(
				array(
					'ACTION' => Bitrix\Main\Grid\Panel\Actions::CREATE,
					'DATA' => array(
						array(
							'TYPE' => Bitrix\Main\Grid\Panel\Types::TEXT,
							'ID' => 'action_assigned_by_search',
							'NAME' => 'ACTION_ASSIGNED_BY_SEARCH'
						),
						array(
							'TYPE' => Bitrix\Main\Grid\Panel\Types::HIDDEN,
							'ID' => 'action_assigned_by_id',
							'NAME' => 'ACTION_ASSIGNED_BY_ID'
						),
						$applyButton
					)
				),
				array(
					'ACTION' => Bitrix\Main\Grid\Panel\Actions::CALLBACK,
					'DATA' => array(
						array('JS' => "BX.CrmUIGridExtension.prepareAction('{$gridManagerID}', 'assign_to',  { searchInputId: 'action_assigned_by_search_control', dataInputId: 'action_assigned_by_id_control', componentName: '{$prefix}_ACTION_ASSIGNED_BY' })")
					)
				),
				array(
					'ACTION' => Bitrix\Main\Grid\Panel\Actions::CALLBACK,
					'DATA' => array(array('JS' => "BX.CrmUIGridExtension.processActionChange('{$gridManagerID}', 'assign_to')"))
				)
			)
		);
		//endregion
		//region Create call list
		if(IsModuleInstalled('voximplant'))
		{
			$actionList[] = array(
				'NAME' => GetMessage('CRM_LEAD_CREATE_CALL_LIST'),
				'VALUE' => 'create_call_list',
				'ONCHANGE' => array(
					array(
						'ACTION' => Bitrix\Main\Grid\Panel\Actions::CREATE,
						'DATA' => array(
							$applyButton
						)
					),
					array(
						'ACTION' => Bitrix\Main\Grid\Panel\Actions::CALLBACK,
						'DATA' => array(array('JS' => "BX.CrmUIGridExtension.processActionChange('{$gridManagerID}', 'create_call_list')"))
					)
				)
			);
		}
		//endregion
	}

	if($allowDelete)
	{
		$controlPanel['GROUPS'][0]['ITEMS'][] = $snippet->getRemoveButton();
		$actionList[] = $snippet->getRemoveAction();
	}

	if($allowWrite)
	{
		//region Edit Button
		$controlPanel['GROUPS'][0]['ITEMS'][] = $snippet->getEditButton();
		$actionList[] = $snippet->getEditAction();
		//endregion
		//region Mark as Opened
		$actionList[] = array(
			'NAME' => GetMessage('CRM_LEAD_MARK_AS_OPENED'),
			'VALUE' => 'mark_as_opened',
			'ONCHANGE' => array(
				array(
					'ACTION' => Bitrix\Main\Grid\Panel\Actions::CREATE,
					'DATA' => array(
						array(
							'TYPE' => Bitrix\Main\Grid\Panel\Types::DROPDOWN,
							'ID' => 'action_opened',
							'NAME' => 'ACTION_OPENED',
							'ITEMS' => $yesnoList
						),
						$applyButton
					)
				),
				array(
					'ACTION' => Bitrix\Main\Grid\Panel\Actions::CALLBACK,
					'DATA' => array(array('JS' => "BX.CrmUIGridExtension.processActionChange('{$gridManagerID}', 'mark_as_opened')"))
				)
			)
		);
		//endregion
	}

	if($callListUpdateMode)
	{
		$controlPanel['GROUPS'][0]['ITEMS'][] = array(
			"TYPE" => \Bitrix\Main\Grid\Panel\Types::BUTTON,
			"TEXT" => GetMessage("CRM_LEAD_UPDATE_CALL_LIST"),
			"ID" => "update_call_list",
			"NAME" => "update_call_list",
			'ONCHANGE' => array(
				array(
					'ACTION' => Bitrix\Main\Grid\Panel\Actions::CALLBACK,
					'DATA' => array(array('JS' => "BX.CrmUIGridExtension.updateCallList('{$gridManagerID}', {$arResult['CALL_LIST_ID']}, '{$arResult['CALL_LIST_CONTEXT']}')"))
				)
			)
		);
	}
	else
	{
		//region Start call list
		if(IsModuleInstalled('voximplant'))
		{
			$controlPanel['GROUPS'][0]['ITEMS'][] = array(
				"TYPE" => \Bitrix\Main\Grid\Panel\Types::BUTTON,
				"TEXT" => GetMessage('CRM_LEAD_START_CALL_LIST'),
				"VALUE" => "start_call_list",
				"ONCHANGE" => array(
					array(
						"ACTION" => Bitrix\Main\Grid\Panel\Actions::CALLBACK,
						"DATA" => array(array('JS' => "BX.CrmUIGridExtension.createCallList('{$gridManagerID}', true)"))
					)
				)
			);
		}
		//endregion
		$controlPanel['GROUPS'][0]['ITEMS'][] = array(
			"TYPE" => \Bitrix\Main\Grid\Panel\Types::DROPDOWN,
			"ID" => "action_button_{$prefix}",
			"NAME" => "action_button_{$prefix}",
			"ITEMS" => $actionList
		);
	}

	$controlPanel['GROUPS'][0]['ITEMS'][] = $snippet->getForAllCheckbox();
}
//endregion

// пути специально для order (типы отображения)
$arOrderPathType = array(
    "PATH_TO_ORDER_LIST" => "/crm/order/",
    "PATH_TO_ORDER_WIDGET" => "/crm/order/widget/"
);

//GK
$arBar = array(
    array(
    //'icon' => 'table',
    'id' => 'list',
    'name' => GetMessage('CRM_LEAD_LIST_FILTER_NAV_BUTTON_LIST'),
    'active' => true,
    'url' => $arOrderPathType['PATH_TO_ORDER_LIST']
    )
);
if ($arResult['SECTION'] != 'ORDER')
{
    $arBar[] = array(
					//'icon' => 'kanban',
					'id' => 'kanban',
					'name' => GetMessage('CRM_LEAD_LIST_FILTER_NAV_BUTTON_KANBAN'),
					'active' => false,
					'url' => $arParams['PATH_TO_LEAD_KANBAN']
				);
    $arBar[] = array(
					//'icon' => 'chart',
					'id' => 'widget',
					'name' => GetMessage('CRM_LEAD_LIST_FILTER_NAV_BUTTON_WIDGET'),
					'active' => false,
					'url' => $arParams['PATH_TO_LEAD_WIDGET']
				);
}
else
{
    $arBar[] = array(
        //'icon' => 'chart',
        'id'     => 'widget',
        'name'   => GetMessage('CRM_LEAD_LIST_FILTER_NAV_BUTTON_WIDGET'),
        'active' => false,
        'url'    => $arOrderPathType['PATH_TO_ORDER_WIDGET']
    );
}

$APPLICATION->IncludeComponent(
	'bitrix:crm.interface.grid',
	'titleflex',
	array(
		'GRID_ID' => $arResult['GRID_ID'],
		'HEADERS' => $arResult['HEADERS'],
		'SORT' => $arResult['SORT'],
		'SORT_VARS' => $arResult['SORT_VARS'],
		'ROWS' => $arResult['GRID_DATA'],
		'FORM_ID' => $arResult['FORM_ID'],
		'TAB_ID' => $arResult['TAB_ID'],
		'AJAX_ID' => $arResult['AJAX_ID'],
		'AJAX_OPTION_JUMP' => 'N',
		'AJAX_OPTION_HISTORY' => 'N',
		'FILTER' => $arResult['FILTER'],
		'FILTER_PRESETS' => $arResult['FILTER_PRESETS'],
		'ENABLE_LIVE_SEARCH' => true,
		'ACTION_PANEL' => $controlPanel,
		'PAGINATION' => isset($arResult['PAGINATION']) && is_array($arResult['PAGINATION'])
			? $arResult['PAGINATION'] : array(),
		'ENABLE_ROW_COUNT_LOADER' => true,
		'PRESERVE_HISTORY' => $arResult['PRESERVE_HISTORY'],
		'NAVIGATION_BAR' => array(
			'ITEMS' => $arBar, //GK
//                array(
//				array(
//					//'icon' => 'table',
//					'id' => 'list',
//					'name' => GetMessage('CRM_LEAD_LIST_FILTER_NAV_BUTTON_LIST'),
//					'active' => true,
//					'url' => $arParams['PATH_TO_LEAD_LIST']
//				),
//				array(
//					//'icon' => 'kanban',
//					'id' => 'kanban',
//					'name' => GetMessage('CRM_LEAD_LIST_FILTER_NAV_BUTTON_KANBAN'),
//					'active' => false,
//					'url' => $arParams['PATH_TO_LEAD_KANBAN']
//				),
//				array(
//					//'icon' => 'chart',
//					'id' => 'widget',
//					'name' => GetMessage('CRM_LEAD_LIST_FILTER_NAV_BUTTON_WIDGET'),
//					'active' => false,
//					'url' => $arParams['PATH_TO_LEAD_WIDGET']
//				)
//			),
			'BINDING' => array(
				'category' => 'crm.navigation',
				'name' => 'index',
				'key' => strtolower($arResult['NAVIGATION_CONTEXT_ID'])
			)
		),
		'IS_EXTERNAL_FILTER' => $arResult['IS_EXTERNAL_FILTER'],
		'EXTENSION' => array(
			'ID' => $gridManagerID,
			'CONFIG' => array(
				'ownerTypeName' => CCrmOwnerType::LeadName,
				'gridId' => $arResult['GRID_ID'],
				'activityEditorId' => $activityEditorID,
				'activityServiceUrl' => '/bitrix/components/bitrix/crm.activity.editor/ajax.php?siteID='.SITE_ID.'&'.bitrix_sessid_get(),
				'taskCreateUrl'=> isset($arResult['TASK_CREATE_URL']) ? $arResult['TASK_CREATE_URL'] : '',
				'serviceUrl' => '/bitrix/components/bitrix/crm.lead.list/list.ajax.php?siteID='.SITE_ID.'&'.bitrix_sessid_get(),
				'loaderData' => isset($arParams['AJAX_LOADER']) ? $arParams['AJAX_LOADER'] : null
			),
			'MESSAGES' => array(
				'deletionDialogTitle' => GetMessage('CRM_LEAD_DELETE_TITLE'),
				'deletionDialogMessage' => GetMessage('CRM_LEAD_DELETE_CONFIRM'),
				'deletionDialogButtonTitle' => GetMessage('CRM_LEAD_DELETE')
			)
		)
	),
	$component
);
?><script type="text/javascript">
    BX.ready(
        function()
        {
            BX.CrmLongRunningProcessDialog.messages =
                {
                    startButton: "<?=GetMessageJS('CRM_LEAD_LRP_DLG_BTN_START')?>",
                    stopButton: "<?=GetMessageJS('CRM_LEAD_LRP_DLG_BTN_STOP')?>",
                    closeButton: "<?=GetMessageJS('CRM_LEAD_LRP_DLG_BTN_CLOSE')?>",
                    wait: "<?=GetMessageJS('CRM_LEAD_LRP_DLG_WAIT')?>",
                    requestError: "<?=GetMessageJS('CRM_LEAD_LRP_DLG_REQUEST_ERR')?>"
                };
        }
    );
</script>
<script type="text/javascript">
    BX.ready(
        function()
        {
            BX.CrmSipManager.getCurrent().setServiceUrl(
                "CRM_<?=CUtil::JSEscape(CCrmOwnerType::LeadName)?>",
                "/bitrix/components/bitrix/crm.lead.show/ajax.php?<?=bitrix_sessid_get()?>"
            );

            if(typeof(BX.CrmSipManager.messages) === 'undefined')
            {
                BX.CrmSipManager.messages =
                    {
                        "unknownRecipient": "<?= GetMessageJS('CRM_SIP_MGR_UNKNOWN_RECIPIENT')?>",
                        "makeCall": "<?= GetMessageJS('CRM_SIP_MGR_MAKE_CALL')?>"
                    };
            }
        }
    );
</script>
<?if(!$isInternal):?>
	<script type="text/javascript">
        BX.ready(
            function()
            {
                BX.CrmActivityEditor.items['<?= CUtil::JSEscape($activityEditorID)?>'].addActivityChangeHandler(
                    function()
                    {
                        BX.Main.gridManager.reload('<?= CUtil::JSEscape($arResult['GRID_ID'])?>');
                    }
                );
                BX.namespace('BX.Crm.Activity');
                if(typeof BX.Crm.Activity.Planner !== 'undefined')
                {
                    BX.Crm.Activity.Planner.Manager.setCallback('onAfterActivitySave', function()
                    {
                        BX.Main.gridManager.reload('<?= CUtil::JSEscape($arResult['GRID_ID'])?>');
                    });
                }
            }
        );
	</script>
<?endif;?>
<?if($arResult['CAN_CONVERT'] && isset($arResult['CONVERSION_CONFIG'])):?>
	<script type="text/javascript">
        BX.ready(
            function()
            {
                BX.CrmLeadConversionScheme.messages =
				<?=CUtil::PhpToJSObject(LeadConversionScheme::getJavaScriptDescriptions(false))?>;
                BX.CrmLeadConverter.messages =
                    {
                        accessDenied: "<?=GetMessageJS("CRM_LEAD_CONV_ACCESS_DENIED")?>",
                        generalError: "<?=GetMessageJS("CRM_LEAD_CONV_GENERAL_ERROR")?>",
                        dialogTitle: "<?=GetMessageJS("CRM_LEAD_CONV_DIALOG_TITLE")?>",
                        syncEditorLegend: "<?=GetMessageJS("CRM_LEAD_CONV_DIALOG_SYNC_LEGEND")?>",
                        syncEditorFieldListTitle: "<?=GetMessageJS("CRM_LEAD_CONV_DIALOG_SYNC_FILED_LIST_TITLE")?>",
                        syncEditorEntityListTitle: "<?=GetMessageJS("CRM_LEAD_CONV_DIALOG_SYNC_ENTITY_LIST_TITLE")?>",
                        continueButton: "<?=GetMessageJS("CRM_LEAD_CONV_DIALOG_CONTINUE_BTN")?>",
                        cancelButton: "<?=GetMessageJS("CRM_LEAD_CONV_DIALOG_CANCEL_BTN")?>",
                        selectButton: "<?=GetMessageJS("CRM_LEAD_CONV_ENTITY_SEL_BTN")?>",
                        openEntitySelector: "<?=GetMessageJS("CRM_LEAD_CONV_OPEN_ENTITY_SEL")?>",
                        entitySelectorTitle: "<?=GetMessageJS("CRM_LEAD_CONV_ENTITY_SEL_TITLE")?>",
                        contact: "<?=GetMessageJS("CRM_LEAD_CONV_ENTITY_SEL_CONTACT")?>",
                        company: "<?=GetMessageJS("CRM_LEAD_CONV_ENTITY_SEL_COMPANY")?>",
                        noresult: "<?=GetMessageJS("CRM_LEAD_CONV_ENTITY_SEL_SEARCH_NO_RESULT")?>",
                        search : "<?=GetMessageJS("CRM_LEAD_CONV_ENTITY_SEL_SEARCH")?>",
                        last : "<?=GetMessageJS("CRM_LEAD_CONV_ENTITY_SEL_LAST")?>"
                    };
                BX.CrmLeadConverter.permissions =
                    {
                        contact: <?=CUtil::PhpToJSObject($arResult['CAN_CONVERT_TO_CONTACT'])?>,
                        company: <?=CUtil::PhpToJSObject($arResult['CAN_CONVERT_TO_COMPANY'])?>,
                        deal: <?=CUtil::PhpToJSObject($arResult['CAN_CONVERT_TO_DEAL'])?>
                    };
                BX.CrmLeadConverter.settings =
                    {
                        serviceUrl: "<?='/local/components/bitrix/crm.lead.show/ajax.php?action=convert&'.bitrix_sessid_get()?>",
                        config: <?=CUtil::PhpToJSObject($arResult['CONVERSION_CONFIG']->toJavaScript())?>
                    };
                BX.CrmDealCategory.infos = <?=CUtil::PhpToJSObject(
					DealCategory::getJavaScriptInfos(EntityConverter::getPermittedDealCategoryIDs())
				)?>;
                BX.CrmDealCategorySelectDialog.messages =
                    {
                        title: "<?=GetMessageJS('CRM_LEAD_LIST_CONV_DEAL_CATEGORY_DLG_TITLE')?>",
                        field: "<?=GetMessageJS('CRM_LEAD_LIST_CONV_DEAL_CATEGORY_DLG_FIELD')?>",
                        saveButton: "<?=GetMessageJS('CRM_LEAD_LIST_BUTTON_SAVE')?>",
                        cancelButton: "<?=GetMessageJS('CRM_LEAD_LIST_BUTTON_CANCEL')?>"
                    };
                BX.CrmEntityType.setCaptions(<?=CUtil::PhpToJSObject(CCrmOwnerType::GetJavascriptDescriptions())?>);
            }
        );
	</script>
<?endif;?>
<?if($arResult['NEED_FOR_REBUILD_DUP_INDEX']):?>
	<script type="text/javascript">
        BX.ready(
            function()
            {
                BX.CrmDuplicateManager.messages =
                    {
                        rebuildLeadIndexDlgTitle: "<?=GetMessageJS('CRM_LEAD_REBUILD_DUP_INDEX_DLG_TITLE')?>",
                        rebuildLeadIndexDlgSummary: "<?=GetMessageJS('CRM_LEAD_REBUILD_DUP_INDEX_DLG_SUMMARY')?>"
                    };

                var mgr = BX.CrmDuplicateManager.create("mgr", { entityTypeName: "<?=CUtil::JSEscape(CCrmOwnerType::LeadName)?>", serviceUrl: "<?=SITE_DIR?>bitrix/components/bitrix/crm.lead.list/list.ajax.php?&<?=bitrix_sessid_get()?>" });
                BX.addCustomEvent(
                    mgr,
                    'ON_LEAD_INDEX_REBUILD_COMPLETE',
                    function()
                    {
                        var msg = BX("rebuildLeadDupIndexMsg");
                        if(msg)
                        {
                            msg.style.display = "none";
                        }
                    }
                );

                var link = BX("rebuildLeadDupIndexLink");
                if(link)
                {
                    BX.bind(
                        link,
                        "click",
                        function(e)
                        {
                            mgr.rebuildIndex();
                            return BX.PreventDefault(e);
                        }
                    );
                }
            }
        );
	</script>
<?endif;?>
<?if($arResult['NEED_FOR_REBUILD_SEARCH_CONTENT']):?>
	<script type="text/javascript">
        BX.ready(
            function()
            {
                if(BX.AutorunProcessPanel.isExists("rebuildLeadSearch"))
                {
                    return;
                }

                BX.AutorunProcessManager.messages =
                    {
                        title: "<?=GetMessageJS('CRM_LEAD_REBUILD_SEARCH_CONTENT_DLG_TITLE')?>",
                        stateTemplate: "<?=GetMessageJS('CRM_REBUILD_SEARCH_CONTENT_STATE')?>"
                    };
                var manager = BX.AutorunProcessManager.create("rebuildLeadSearch",
                    {
                        serviceUrl: "<?='/bitrix/components/bitrix/crm.lead.list/list.ajax.php?'.bitrix_sessid_get()?>",
                        actionName: "REBUILD_SEARCH_CONTENT",
                        container: "rebuildLeadSearchWrapper",
                        enableLayout: true
                    }
                );
                manager.runAfter(100);
            }
        );
	</script>
<?endif;?>
<?if($arResult['NEED_FOR_REBUILD_LEAD_SEMANTICS']):?>
	<script type="text/javascript">
        BX.ready(
            function()
            {
                var builderPanel = BX.CrmLongRunningProcessPanel.create(
                    "rebuildLeadSemantics",
                    {
                        "containerId": "rebuildMessageWrapper",
                        "prefix": "",
                        "active": true,
                        "message": "<?=GetMessageJS('CRM_LEAD_REBUILD_SEMANTICS')?>",
                        "manager":
                            {
                                dialogTitle: "<?=GetMessageJS("CRM_LEAD_REBUILD_SEMANTICS_DLG_TITLE")?>",
                                dialogSummary: "<?=GetMessageJS("CRM_LEAD_REBUILD_SEMANTICS_DLG_SUMMARY")?>",
                                actionName: "REBUILD_SEMANTICS",
                                serviceUrl: "<?='/bitrix/components/bitrix/crm.lead.list/list.ajax.php?'.bitrix_sessid_get()?>"
                            }
                    }
                );
                builderPanel.layout();
            }
        );
	</script>
<?endif;?>
<?if($arResult['NEED_FOR_REBUILD_LEAD_ATTRS']):?>
	<script type="text/javascript">
        BX.ready(
            function()
            {
                var link = BX("rebuildLeadAttrsLink");
                if(link)
                {
                    BX.bind(
                        link,
                        "click",
                        function(e)
                        {
                            var msg = BX("rebuildLeadAttrsMsg");
                            if(msg)
                            {
                                msg.style.display = "none";
                            }
                        }
                    );
                }
            }
        );
	</script>
<?endif;?>

<script type="text/javascript">
    function addNewParamToFilter(linkText){
        companyTypeFindContainer = $("#CRM_LEAD_LIST_V12_search_container.popup-window div.main-ui-filter-field-container-list").children('[data-name="<?=$channelFieldName?>"]');
        //console.log('linkText = '+linkText);
        if (linkText == "Все") {
            companyTypeFindContainer.children('[data-name="<?=$channelFieldName?>"]').attr('data-value', '[]');
            companyTypeFindContainer.find('span.main-ui-square-container').html('');
        }
		<?foreach($arFastFilter as $id => $value):?>
        if (linkText == "<?=$value?>") {
            companyTypeFindContainer.children('[data-name="<?=$channelFieldName?>"]').attr('data-value', '[{"NAME":"<?=$value?>","VALUE":"<?=$id?>"}]');
            companyTypeFindContainer.find('span.main-ui-square-container').html('<span class="main-ui-square" data-item="{&quot;NAME&quot;:&quot;<?=$value?>&quot;,&quot;VALUE&quot;:&quot;<?=$id?>&quot;}"><span class="main-ui-square-item"><?=$value?></span><span class="main-ui-item-icon main-ui-square-delete"></span></span>');
        }
		<?endforeach;?>
        //console.log(companyTypeFindContainer.html());
        $('#CRM_LEAD_LIST_V12_search_container.popup-window button.main-ui-filter-field-button.main-ui-filter-find').click();
    }

    $(document).ready(function(){
        //Проверка введеных значений в парметре фильтрации "Канал" и переключение в быстром выборе на соответствующий пункт
        $(document).on('click', '#CRM_LEAD_LIST_V12_search_container.popup-window span.main-ui-filter-field-button.main-ui-filter-find', function(){
            //console.log('Клик по кнопке найти');
            companyTypeFindContainer = $("#CRM_LEAD_LIST_V12_search_container.popup-window div.main-ui-filter-field-container-list").children('[data-name="<?=$channelFieldName?>"]');
            $("#deal_category_panel_container .crm-deal-panel-tab div.crm-deal-panel-tab-item-active").removeClass('crm-deal-panel-tab-item-active');
            if(companyTypeFindContainer.length>0){
                //console.log('companyTypeFindContainer =', companyTypeFindContainer);
                companyTypeText = companyTypeFindContainer.children('[data-name="<?=$channelFieldName?>"]').attr('data-value');
                allSel = true;
				<?$indexNum = 1;
				foreach($arFastFilter as $id => $value):?>
                if(companyTypeText=='[{"NAME":"<?=$value?>","VALUE":"<?=$id?>"}]'){
                    $("#deal_category_panel_container .crm-deal-panel-tab div:eq(<?=$indexNum?>)").addClass('crm-deal-panel-tab-item-active');
                    allSel = false;
                }
				<?  $indexNum++;
				endforeach;?>
                //console.log('companyTypeText =', companyTypeText);
                if(allSel){
                    $("#deal_category_panel_container .crm-deal-panel-tab div:eq(0)").addClass('crm-deal-panel-tab-item-active');
                }
            } else {
                $("#deal_category_panel_container .crm-deal-panel-tab div:eq(0)").addClass('crm-deal-panel-tab-item-active');
            }
        });

        //Обработчик удаления пунтка из строки поиска
        $("#CRM_LEAD_LIST_V12_search_container").on("click", ".main-ui-item-icon.main-ui-square-delete", function(){
            //console.log("Клик удалить");
            //console.log($(this).parent().attr('data-item'));
            if($(this).parent().attr('data-item')=='{"type":"control","name":"<?=$channelFieldName?>"}'){
                $("#deal_category_panel_container .crm-deal-panel-tab div.crm-deal-panel-tab-item-active").removeClass('crm-deal-panel-tab-item-active');
                $("#deal_category_panel_container .crm-deal-panel-tab div:eq(0)").addClass('crm-deal-panel-tab-item-active');
            }
        })

        //Переключение по быстрым кнопкам фильтра
        $("#deal_category_panel_container a").click(function(){
            if(!$(this).parent().hasClass('crm-deal-panel-tab-item-active')) {
                linkText = $(this).text();
                $("#deal_category_panel_container div.crm-deal-panel-tab-item-active").removeClass('crm-deal-panel-tab-item-active');
                $(this).parent().addClass('crm-deal-panel-tab-item-active');
                companyTypeFindContainer = $("#CRM_LEAD_LIST_V12_search_container.popup-window div.main-ui-filter-field-container-list").children('[data-name="<?=$channelFieldName?>"]');
                //console.log('Количество элементов для фильтрации по типу компании = '+ companyTypeFindContainer.length);
                if(companyTypeFindContainer.length==0){
                    $('#CRM_LEAD_LIST_V12_search_container.popup-window .main-ui-filter-field-add-item').click();
                    $('#CRM_LEAD_LIST_V12_fields_popup.popup-window #popup-window-content-CRM_LEAD_LIST_V12_fields_popup div[data-id="field_<?=$channelFieldName?>"]').click();
                    addNewParamToFilter(linkText);
                } else {
                    addNewParamToFilter(linkText);
                }
                //console.log(companyTypeFindContainer.find('span.main-ui-square-container').html());
                //console.log('input.main-ui-square-search-item', companyTypeFindContainer.find('input.main-ui-square-search-item').val());

            }
        });
    });
</script>
<script type="text/javascript">
    function addNewParamToFilterType(linkText){
        companyTypeFindContainer = $("#CRM_LEAD_LIST_V12_search_container.popup-window div.main-ui-filter-field-container-list").children('[data-name="<?=$typeFieldName?>"]');
        //console.log('linkText = '+linkText);
        if (linkText == "Все") {
            companyTypeFindContainer.children('[data-name="<?=$typeFieldName?>"]').attr('data-value', '[]');
            companyTypeFindContainer.find('span.main-ui-square-container').html('');
        }
        <?foreach($arFastFilterType as $id => $value):?>
        if (linkText == "<?=$value?>") {
            companyTypeFindContainer.children('[data-name="<?=$typeFieldName?>"]').attr('data-value', '[{"NAME":"<?=$value?>","VALUE":"<?=$id?>"}]');
            companyTypeFindContainer.find('span.main-ui-square-container').html('<span class="main-ui-square" data-item="{&quot;NAME&quot;:&quot;<?=$value?>&quot;,&quot;VALUE&quot;:&quot;<?=$id?>&quot;}"><span class="main-ui-square-item"><?=$value?></span><span class="main-ui-item-icon main-ui-square-delete"></span></span>');
        }
        <?endforeach;?>
        //console.log(companyTypeFindContainer.html());
        $('#CRM_LEAD_LIST_V12_search_container.popup-window span.main-ui-filter-field-button.main-ui-filter-find').click();
    }

    $(document).ready(function(){
        //Проверка введеных значений в парметре фильтрации "Канал" и переключение в быстром выборе на соответствующий пункт
//        $(document).on('click', '#CRM_LEAD_LIST_V12_search_container.popup-window span.main-ui-filter-field-button.main-ui-filter-find', function(){
//            //console.log('Клик по кнопке найти');
//            debugger;
//            companyTypeFindContainer = $("#CRM_LEAD_LIST_V12_search_container.popup-window div.main-ui-filter-field-container-list").children('[data-name="<?//=$channelFieldName?>//"]');
//            $("#lead_type_panel_container .crm-deal-panel-tab div.crm-deal-panel-tab-item-active").removeClass('crm-deal-panel-tab-item-active');
//            if(companyTypeFindContainer.length>0){
//                //console.log('companyTypeFindContainer =', companyTypeFindContainer);
//                companyTypeText = companyTypeFindContainer.children('[data-name="<?//=$typeFieldName?>//"]').attr('data-value');
//                allSel = true;
//                <?//$indexNum = 1;
//                foreach($arFastFilterType as $id => $value):?>
//                if(companyTypeText=='[{"NAME":"<?//=$value?>//","VALUE":"<?//=$id?>//"}]'){
//                    $("#lead_type_panel_container .crm-deal-panel-tab div:eq(<?//=$indexNum?>//)").addClass('crm-deal-panel-tab-item-active');
//                    allSel = false;
//                }
//                <?//  $indexNum++;
//                endforeach;?>
//                //console.log('companyTypeText =', companyTypeText);
//                if(allSel){
//                    $("#lead_type_panel_container .crm-deal-panel-tab div:eq(0)").addClass('crm-deal-panel-tab-item-active');
//                }
//            } else {
//                $("#lead_type_panel_container .crm-deal-panel-tab div:eq(0)").addClass('crm-deal-panel-tab-item-active');
//            }
//        });

        //Обработчик удаления пунтка из строки поиска
        $("#CRM_LEAD_LIST_V12_search_container").on("click", ".main-ui-item-icon.main-ui-square-delete", function(){
            //console.log("Клик удалить");
            //console.log($(this).parent().attr('data-item'));
            if($(this).parent().attr('data-item')=='{"type":"control","name":"<?=$typeFieldName?>"}'){
                $("#lead_type_panel_container .crm-deal-panel-tab div.crm-deal-panel-tab-item-active").removeClass('crm-deal-panel-tab-item-active');
                $("#lead_type_panel_container .crm-deal-panel-tab div:eq(0)").addClass('crm-deal-panel-tab-item-active');
            }
        })

        //Переключение по быстрым кнопкам фильтра
        $("#lead_type_panel_container a").click(function(){
            console.log("uuu");
            if(!$(this).parent().hasClass('crm-deal-panel-tab-item-active')) {
//                debugger;
                linkText = $(this).text();
                $("#lead_type_panel_container div.crm-deal-panel-tab-item-active").removeClass('crm-deal-panel-tab-item-active');
                $(this).parent().addClass('crm-deal-panel-tab-item-active');
                $.ajax({
                    type: "POST",
                    url: "<?=$url_type_setting?>",
                    dataType: 'json',
                    data: "type="+this.id,
                    success: function(data){
                        $.each(data, function(key, val){
                            if ($('#'+key).parent().children(".crm-deal-panel-tab-item-message").length > 0)
                            {
                                if( val > 0)
                                {
                                    $('#'+key).parent().children(".crm-deal-panel-tab-item-message").text(val);
                                }
                                else
                                {
                                    $('#'+key).parent().children(".crm-deal-panel-tab-item-message").remove();
                                }
                            }
                            else
                            {
                                if( val > 0) {
                                    $('<span class="crm-deal-panel-tab-item-message">' + val + '</span>').insertBefore('#' + key);
                                }
                            }
                        });
                        $('#CRM_LEAD_LIST_V12_search_container.popup-window span.main-ui-filter-field-button.main-ui-filter-find').click();
//                        alert( "Прибыли данные: " + msg );
                    }
                });
//                companyTypeFindContainer = $("#CRM_LEAD_LIST_V12_search_container.popup-window div.main-ui-filter-field-container-list").children('[data-name="<?//=$typeFieldName?>//"]');
//                //console.log('Количество элементов для фильтрации по типу компании = '+ companyTypeFindContainer.length);
//                if(companyTypeFindContainer.length==0){
//                    $('#CRM_LEAD_LIST_V12_search_container.popup-window .main-ui-filter-field-add-item').click();
//                    $('#CRM_LEAD_LIST_V12_fields_popup.popup-window #popup-window-content-CRM_LEAD_LIST_V12_fields_popup div[data-id="field_<?//=$typeFieldName?>//"]').click();
//                    addNewParamToFilterType(linkText);
//                } else {
//                    addNewParamToFilterType(linkText);
//                }
                //console.log(companyTypeFindContainer.find('span.main-ui-square-container').html());
                //console.log('input.main-ui-square-search-item', companyTypeFindContainer.find('input.main-ui-square-search-item').val());

            }
        });
    });
</script>