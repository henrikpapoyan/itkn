<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

if ($arResult['SECTION'] == 'ORDER')
{
    foreach ($arResult['HEADERS'] as $key => $field) {
        if ($field['id'] == 'LEAD_SUMMARY')
        {
            $arResult['HEADERS'][$key]['name'] = "Заказ";
        }if ($field['id'] == 'TITLE')
        {
            $arResult['HEADERS'][$key]['name'] = "Название заказа";
        }

    }
}

// добавляем поля в фильтр
$arResult['FILTER'][] = ['id' => 'ACTIVITY_ID', 'name' => GetMessage('FILTER_FIELD_DEALS')];
$arResult['FILTER'][] = ['id' => 'STATUS_DESCRIPTION', 'name' => GetMessage('FILTER_FIELD_STATUS_DESCRIPTION')];
$arResult['FILTER'][] = ['id' => 'SOURCE_DESCRIPTION', 'name' => GetMessage('FILTER_FIELD_SOURCE_DESCRIPTION')];
