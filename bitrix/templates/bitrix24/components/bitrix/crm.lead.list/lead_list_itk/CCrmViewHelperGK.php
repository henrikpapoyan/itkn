<?php
/**
 * User: German
 * Date: 13.11.2017
 * Time: 12:31
 */

class CCrmViewHelperGK extends CCrmViewHelper
{
    private static $ENABLE_LEAD_STATUS_COLORS = true;

    public static function RenderLeadStatusControl($arParams)
    {
        if(!is_array($arParams))
        {
            $arParams = array();
        }

        $arParams['INFOS'] = self::PrepareLeadStatuses();
        if($arParams['UF_AUTO'] == 'N')
        {
            foreach ($arParams['INFOS'] as $k => $v) {
                if($arParams['CURRENT_ID'] == '6') //GK
                {
					if(array_search($v['STATUS_ID'], array('4','5','6', 'CONVERTED', 'JUNK')) === false)
                    {
                        unset($arParams['INFOS'][$k]);
                    }
                }
                else
                {
					if(array_search($v['STATUS_ID'], array('4','5', 'CONVERTED', 'JUNK')) === false)
                    {
                        unset($arParams['INFOS'][$k]);
                    }
                }
            }

        }
        elseif($arParams['UF_AUTO'] == 'Y')
        {
            foreach ($arParams['INFOS'] as $k => $v) {
                if(array_search($v['STATUS_ID'], array('4','5','6')) !== false)
                {
                    unset($arParams['INFOS'][$k]);
                }
            }

        }
        $arParams['FINAL_ID'] = 'CONVERTED';
        $arParams['FINAL_URL'] = isset($arParams['LEAD_CONVERT_URL']) ? $arParams['LEAD_CONVERT_URL'] : '';
        $arParams['VERBOSE_MODE'] = true;
        $arParams['ENTITY_TYPE_NAME'] = CCrmOwnerType::ResolveName(CCrmOwnerType::Lead);
        return self::RenderProgressControl($arParams);
    }
    public static function RenderProgressControl($arParams)
    {
        if(!is_array($arParams))
        {
            return '';
        }

        global $USER;
        if ($arParams["UF_AUTO"] == 'Y' && !$USER->IsAdmin()) {
            $arParams["READ_ONLY"] = 'Y';
        } else {
            unset($arParams["READ_ONLY"]);
        }

        CCrmComponentHelper::RegisterScriptLink('/bitrix/js/crm/progress_control.js');

        $entityTypeName = isset($arParams['ENTITY_TYPE_NAME']) ? $arParams['ENTITY_TYPE_NAME'] : '';
        $leadTypeName = CCrmOwnerType::ResolveName(CCrmOwnerType::Lead);
        $dealTypeName = CCrmOwnerType::ResolveName(CCrmOwnerType::Deal);
        $invoiceTypeName = CCrmOwnerType::ResolveName(CCrmOwnerType::Invoice);
        $quoteTypeName = CCrmOwnerType::ResolveName(CCrmOwnerType::Quote);

        $categoryID = isset($arParams['CATEGORY_ID']) ? $arParams['CATEGORY_ID'] : 0;
        $infos = isset($arParams['INFOS']) ? $arParams['INFOS'] : null;
        if(!is_array($infos) || empty($infos))
        {
            if($entityTypeName === $leadTypeName) //GK
            {
                $infos = self::PrepareLeadStatuses();
                if($arParams['UF_AUTO'] == 'N')
                {
                    foreach ($infos as $k => $v) {
                        if($arParams['CURRENT_ID'] == '6') //GK
                        {
							if(array_search($v['STATUS_ID'], array('4','5','6', 'CONVERTED', 'JUNK')) === false)
                            {
                                unset($infos[$k]);
                            }
                        }
                        else
                        {
							if(array_search($v['STATUS_ID'], array('4','5', 'CONVERTED', 'JUNK')) === false)
                            {
                                unset($infos[$k]);
                            }
                        }
                    }

                }
                elseif($arParams['UF_AUTO'] == 'Y')
                {
                    foreach ($infos as $k => $v) {
                        if(array_search($v['STATUS_ID'], array('4','5','6')) !== false)
                        {
                            unset($infos[$k]);
                        }
                    }

                }
            }
            elseif($entityTypeName === $dealTypeName)
            {
                $infos = self::PrepareDealStages($categoryID);
            }
            elseif($entityTypeName === $quoteTypeName)
            {
                $infos = self::PrepareQuoteStatuses();
            }
            elseif($entityTypeName === $invoiceTypeName)
            {
                $infos = self::PrepareInvoiceStatuses();
            }
        }

        $enableCustomColors = false;
        if($entityTypeName === $leadTypeName)
        {
            $enableCustomColors = self::$ENABLE_LEAD_STATUS_COLORS;
        }
        elseif($entityTypeName === $dealTypeName)
        {
            $enableCustomColors = isset(self::$ENABLE_DEAL_STAGE_COLORS[$categoryID])
                ? self::$ENABLE_DEAL_STAGE_COLORS[$categoryID] : false;
        }
        elseif($entityTypeName === $quoteTypeName)
        {
            $enableCustomColors = self::$ENABLE_QUOTE_STATUS_COLORS;
        }
        elseif($entityTypeName === $invoiceTypeName)
        {
            $enableCustomColors = self::$ENABLE_INVOICE_STATUS_COLORS;
        }

        if(!is_array($infos) || empty($infos))
        {
            return '';
        }

        $registerSettings = isset($arParams['REGISTER_SETTINGS']) && is_bool($arParams['REGISTER_SETTINGS'])
            ? $arParams['REGISTER_SETTINGS'] : false;

        $registrationScript = '';
        if($registerSettings)
        {
            if($entityTypeName === $leadTypeName)
            {
                $registrationScript = self::RenderLeadStatusSettings();
            }
            elseif($entityTypeName === $dealTypeName)
            {
                $registrationScript = self::RenderDealStageSettings();
            }
            elseif($entityTypeName === $quoteTypeName)
            {
                $registrationScript = self::RenderQuoteStatusSettings();
            }
            elseif($entityTypeName === $invoiceTypeName)
            {
                $registrationScript = self::RenderInvoiceStatusSettings();
            }
        }

        $finalID = isset($arParams['FINAL_ID']) ? $arParams['FINAL_ID'] : '';
        if($finalID === '')
        {
            if($entityTypeName === $leadTypeName)
            {
                $finalID = 'CONVERTED';
            }
            elseif($entityTypeName === $dealTypeName)
            {
                $finalID = DealCategory::prepareStageID($categoryID, 'WON');
            }
            elseif($entityTypeName === $quoteTypeName)
            {
                $finalID = 'APPROVED';
            }
            elseif($entityTypeName === $invoiceTypeName)
            {
                $finalID = 'P';
            }
        }

        $finalUrl = isset($arParams['FINAL_URL']) ? $arParams['FINAL_URL'] : '';
        if($finalUrl === '' && $entityTypeName === $leadTypeName)
        {
            $arParams['FINAL_URL'] = isset($arParams['LEAD_CONVERT_URL']) ? $arParams['LEAD_CONVERT_URL'] : '';
        }

        $currentInfo = null;
        $currentID = isset($arParams['CURRENT_ID']) ? $arParams['CURRENT_ID'] : '';
        if($currentID !== '' && isset($infos[$currentID]))
        {
            $currentInfo = $infos[$currentID];
        }
        $currentSort = is_array($currentInfo) && isset($currentInfo['SORT']) ? intval($currentInfo['SORT']) : -1;

        $finalInfo = null;
        if($finalID !== '' && isset($infos[$finalID]))
        {
            $finalInfo = $infos[$finalID];
        }
        $finalSort = is_array($finalInfo) && isset($finalInfo['SORT']) ? intval($finalInfo['SORT']) : -1;

        $isSuccessful = $currentSort === $finalSort;
        $isFailed = $currentSort > $finalSort;

        $defaultProcessColor = self::PROCESS_COLOR;
        $defaultSuccessColor = self::SUCCESS_COLOR;
        $defaultFailureColor = self::FAILURE_COLOR;

        $stepHtml = '';
        $color = isset($currentInfo['COLOR']) ? $currentInfo['COLOR'] : '';
        if($color === '')
        {
            $color = $defaultProcessColor;
            if($isSuccessful)
            {
                $color = $defaultSuccessColor;
            }
            elseif($isFailed)
            {
                $color = $defaultFailureColor;
            }
        }

        $finalColor = isset($finalInfo['COLOR']) ? $finalInfo['COLOR'] : '';
        if($finalColor === '')
        {
            $finalColor = $isSuccessful ? $defaultSuccessColor : $defaultFailureColor;
        }

        foreach($infos as $info)
        {
            $ID = isset($info['STATUS_ID']) ? $info['STATUS_ID'] : '';
            $sort = isset($info['SORT']) ? (int)$info['SORT'] : 0;

            if($sort > $finalSort)
            {
                break;
            }

            if($arParams['CURRENT_ID'] == '6' && $info['STATUS_ID'] == 'CONVERTED'
            || $arParams['UF_AUTO'] == 'Y' && $info['STATUS_ID'] == 'CONVERTED') //GK
            {
                break;
            }
            if($enableCustomColors)
            {
                $stepHtml .= '<td class="crm-list-stage-bar-part"';
                if($sort <= $currentSort)
                {
                    $stepHtml .= ' style="background:'.$color.'"';
                }
                $stepHtml .= '>';
            }
            else
            {
                $stepHtml .= '<td class="crm-list-stage-bar-part';
                if($sort <= $currentSort)
                {
                    $stepHtml .= ' crm-list-stage-passed';
                }
                $stepHtml .= '">';
            }
            $isAutoStageBar = ! empty($arParams['READ_ONLY']) ? "auto-stage-bar" : "";
            $stepHtml .= '<div class="crm-list-stage-bar-block '.$isAutoStageBar.' crm-stage-'.htmlspecialcharsbx(strtolower($ID)).'"><div class="crm-list-stage-bar-btn '.$isAutoStageBar.'"></div></div></td>';
        }

        $wrapperStyle = '';
        $wrapperClass = '';
        if($enableCustomColors)
        {
            if($isSuccessful || $isFailed)
            {
                $wrapperStyle = 'style="background:'.$finalColor.'"';
            }
        }
        else
        {
            if($isSuccessful)
            {
                $wrapperClass = ' crm-list-stage-end-good';
            }
            elseif($isFailed)
            {
                $wrapperClass =' crm-list-stage-end-bad';
            }
        }

        $prefix = isset($arParams['PREFIX']) ? $arParams['PREFIX'] : '';
        $entityID = isset($arParams['ENTITY_ID']) ? intval($arParams['ENTITY_ID']) : 0;
        $controlID = isset($arParams['CONTROL_ID']) ? $arParams['CONTROL_ID'] : '';

        if($controlID === '')
        {
            $controlID = $entityTypeName !== '' && $entityID > 0 ? "{$prefix}{$entityTypeName}_{$entityID}" : uniqid($prefix);
        }

        $isReadOnly = isset($arParams['READ_ONLY']) ? (bool)$arParams['READ_ONLY'] : false;
        $legendContainerID = isset($arParams['LEGEND_CONTAINER_ID']) ? $arParams['LEGEND_CONTAINER_ID'] : '';
        $displayLegend = $legendContainerID === '' && (!isset($arParams['DISPLAY_LEGEND']) || $arParams['DISPLAY_LEGEND']);
        $legendHtml = '';
        if($displayLegend)
        {
            $legendHtml = '<div class="crm-list-stage-bar-title">'.htmlspecialcharsbx(isset($infos[$currentID]) && isset($infos[$currentID]['NAME']) ? $infos[$currentID]['NAME'] : $currentID).'</div>';
        }

        $conversionScheme = null;
        if(isset($arParams['CONVERSION_SCHEME']) && is_array($arParams['CONVERSION_SCHEME']))
        {
            $conversionScheme = array();
            if(isset($arParams['CONVERSION_SCHEME']['ORIGIN_URL']))
            {
                $conversionScheme['originUrl'] = $arParams['CONVERSION_SCHEME']['ORIGIN_URL'];
            }
            if(isset($arParams['CONVERSION_SCHEME']['SCHEME_NAME']))
            {
                $conversionScheme['schemeName'] =  $arParams['CONVERSION_SCHEME']['SCHEME_NAME'];
            }
            if(isset($arParams['CONVERSION_SCHEME']['SCHEME_CAPTION']))
            {
                $conversionScheme['schemeCaption'] =  $arParams['CONVERSION_SCHEME']['SCHEME_CAPTION'];
            }
            if(isset($arParams['CONVERSION_SCHEME']['SCHEME_DESCRIPTION']))
            {
                $conversionScheme['schemeDescription'] =  $arParams['CONVERSION_SCHEME']['SCHEME_DESCRIPTION'];
            }
        }

        return $registrationScript.'<div class="crm-list-stage-bar'.$wrapperClass.'" '.$wrapperStyle.' id="'.htmlspecialcharsbx($controlID).'"><table class="crm-list-stage-bar-table"><tr>'
        .$stepHtml
        .'</tr></table>'
        .'<script type="text/javascript">BX.ready(function(){ BX.CrmProgressControl.create("'
        .CUtil::JSEscape($controlID).'"'
        .', BX.CrmParamBag.create({"containerId": "'.CUtil::JSEscape($controlID).'"'
        .', "entityType":"'.CUtil::JSEscape($entityTypeName).'"'
        .', "entityId":"'.CUtil::JSEscape($entityID).'"'
        .', "legendContainerId":"'.CUtil::JSEscape($legendContainerID).'"'
        .', "serviceUrl":"'.(isset($arParams['SERVICE_URL']) ? CUtil::JSEscape($arParams['SERVICE_URL']) : '').'"'
        .', "finalUrl":"'.(isset($arParams['FINAL_URL']) ? CUtil::JSEscape($arParams['FINAL_URL']) : '').'"'
        .', "verboseMode":'.(isset($arParams['VERBOSE_MODE']) && $arParams['VERBOSE_MODE'] ? 'true' : 'false')
        .', "conversionScheme":'.($conversionScheme !== null ? CUtil::PhpToJSObject($conversionScheme) : 'null')
        .', "currentStepId":"'.CUtil::JSEscape($currentID).'"'
        .', "infoTypeId":"'.CUtil::JSEscape("category_{$categoryID}").'"'
        .', "readOnly":'.($isReadOnly ? 'true' : 'false')
        .', "enableCustomColors":'.($enableCustomColors ? 'true' : 'false')
        .', "ufAuto":"'.$arParams['UF_AUTO'].'"'
        .' }));});</script>'
        .'</div>'.$legendHtml;
    }

}