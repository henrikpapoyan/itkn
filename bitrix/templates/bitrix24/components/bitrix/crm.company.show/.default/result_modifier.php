<?php
/**
 * User: German
 * Date: 05.12.2017
 * Time: 10:16
 */

$arResult['FIELDS']['tab_lists_77'][0]['componentData']['params']['IBLOCK_ID'] = 1000;

$arResult['FIELDS']['tab_lead'][] = array(
    'id' => 'COMPANY_LEAD',
    'name' => "Лиды",//GetMessage('CRM_FIELD_COMPANY_CONTACTS'),
    'colspan' => true,
    'type' => 'crm_lead_list',
    'componentData' => array(
        'template' => '',
        'enableLazyLoad' => true,
        'params' => array(
            'CONTACT_COUNT' => '20',
            'PATH_TO_CONTACT_SHOW' => $arParams['PATH_TO_CONTACT_SHOW'],
            'PATH_TO_CONTACT_EDIT' => $arParams['PATH_TO_CONTACT_EDIT'],
            'PATH_TO_DEAL_EDIT' => $arParams['PATH_TO_DEAL_EDIT'],
            'INTERNAL_FILTER' => array('COMPANY_ID' => $arResult['ELEMENT']['ID']),
            'INTERNAL_CONTEXT' => array('COMPANY_ID' => $arResult['ELEMENT']['ID']),
            'GRID_ID_SUFFIX' => 'COMPANY_SHOW',
            'FORM_ID' => $arResult['FORM_ID'],
            'TAB_ID' => 'tab_lead',
            'NAME_TEMPLATE' => $arParams['NAME_TEMPLATE'],
            'ENABLE_TOOLBAR' => true,
            'PRESERVE_HISTORY' => true
        )
    )
);

$arResult['FIELDS']['tab_order'][] = array(
    'id' => 'COMPANY_ORDER',
    'name' => "Заказы",
    'colspan' => true,
    'type' => 'crm_order_list',
    'componentData' => array(
        'template' => '',
        'enableLazyLoad' => true,
        'params' => array(
            'CONTACT_COUNT' => '20',
            'PATH_TO_CONTACT_SHOW' => $arParams['PATH_TO_CONTACT_SHOW'],
            'PATH_TO_CONTACT_EDIT' => $arParams['PATH_TO_CONTACT_EDIT'],
            'PATH_TO_DEAL_EDIT' => $arParams['PATH_TO_DEAL_EDIT'],
            'INTERNAL_FILTER' => array('COMPANY_ID' => $arResult['ELEMENT']['ID']),
            'INTERNAL_CONTEXT' => array('COMPANY_ID' => $arResult['ELEMENT']['ID']),
            'GRID_ID_SUFFIX' => 'COMPANY_SHOW',
            'FORM_ID' => $arResult['FORM_ID'],
            'TAB_ID' => 'tab_order',
            'NAME_TEMPLATE' => $arParams['NAME_TEMPLATE'],
            'ENABLE_TOOLBAR' => true,
            'PRESERVE_HISTORY' => true,
            'SECTION' => 'ORDER',
        )
    )
);