<?
$MESS["CRM_COMPANY_WGT_PAGE_TITLE"] = "Сводный отчет по компаниям";
$MESS["CRM_COMPANY_WGT_DEMO_TITLE"] = "Это демо-представление, вы можете скрыть его и получить аналитику по вашим компаниям.";
$MESS["CRM_COMPANY_WGT_DEMO_CONTENT"] = "Если у вас еще нет компаний, создайте <a href=\"#URL#\" class=\"#CLASS_NAME#\">первую компанию</a> прямо сейчас!";
$MESS["VIEW_SWITCH_LIST"] = "Список";
$MESS["VIEW_SWITCH_REPORTS"] = "Отчёты";
?>