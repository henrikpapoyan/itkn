BX.ready(function(){
    var arTabLoading = "";
    //обработка открытия вкладки
    BX.addCustomEvent('BX_CRM_INTERFACE_FORM_TAB_SELECTED', BX.delegate(function(self, name, tab_id){
        if (!arTabLoading[tab_id] && self.oTabsMeta[tab_id].name.toLowerCase().indexOf('договоры') !== -1) {
            var innerTab = BX('inner_tab_'+tab_id),
                contactId = 0,
                matches = null,
                waiter = BX.showWait(innerTab);
            if (matches = window.location.href.match(/\/crm\/contact\/show\/([\d\.]+)\//i)) {
                var contactId = parseInt(matches[1]);
                contactId = contactId.toString();
                contactId = contactId.replace(".0000", "");
            }
            if (contactId > 0) {
                //чтобы не грузить повторно
                arTabLoading[tab_id] = true;
                BX.ajax({
                        url: '/ajax/contact/crm_tab_contract.php',
                        method: 'POST',
                        dataType: 'html',
                        data: {
                            id: contactId
                        },
                        onsuccess: function(data)
                        {
                            innerTab.innerHTML = data;
                            BX.closeWait($this, waiter);
                        },
                        onfailure: function(data)
                        {
                            BX.closeWait(innerTab, waiter);
                        }
                    }
                );
            }
        }

        if (!arTabLoading[tab_id] && self.oTabsMeta[tab_id].name.toLowerCase().indexOf('адреса') !== -1) {
            var innerTab = BX('inner_tab_'+tab_id),
                contactId = 0,
                matches = null,
                waiter = BX.showWait(innerTab);
            if (matches = window.location.href.match(/\/crm\/contact\/show\/([\d\.]+)\//i)) {
                var contactId = parseInt(matches[1]);
                contactId = contactId.toString();
                contactId = contactId.replace(".0000", "");
            }
            //innerTab.innerHTML = 'ID текущей компании = '+contactId;
            if (contactId > 0) {
                //чтобы не грузить повторно
                arTabLoading[tab_id] = true;
                BX.ajax({
                        url: '/ajax/contact/crm_tab_address.php',
                        method: 'POST',
                        dataType: 'html',
                        data: {
                            id: contactId
                        },
                        onsuccess: function(data)
                        {
                            innerTab.innerHTML = data;
                            BX.closeWait($this, waiter);
                        },
                        onfailure: function(data)
                        {
                            BX.closeWait(innerTab, waiter);
                        }
                    }
                );
            }
        }
    }));
});