<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

// добавляем поля в фильтр
$arResult['FILTER'][] = ['id' => 'RQ_UF_CRM_1499413026|1', 'name' => GetMessage('FILTER_FIELD_FACT_ADDRESS')];
$arResult['FILTER'][] = ['id' => 'RQ_UF_MODEL_KKT|1', 'name' => GetMessage('FILTER_FIELD_MODEL_KKT')];