<?php

/** Расчет кол-ва активных реестров ККТ */

//Получение кол-ва активных ККТ
$IBLOCK_ID = getIblockIDByCode('registry_kkt');
$arSelect = Array("ID", "NAME","PROPERTY_*");
$arFilter = Array("IBLOCK_ID" => $IBLOCK_ID, "PROPERTY_DEAL" => $arParams['DATA']['ID'], ">=PROPERTY_DATE_OFF" => date('Y-m-d'));
$res = CIBlockElement::GetList(Array("ID"=>"DESC"), $arFilter, false, false, $arSelect);
$arResult['KKT']['COUNT_ACTIVE_KKT'] = intval($res->SelectedRowsCount());

//Получение ID вкладки ККТ
foreach ($arResult['TABS'] as $key => $val){
    if($val['name'] == "ККТ") {
        $arResult['KKT']['TAB_ID'] = $key;
		break;
	}
}

//GK
$PPParent = $this->getComponent()->getParent()->getParent()->getParent();

if($PPParent->arParams['SECTION'] == 'ORDER')
{
    unset($arResult['TABS']['tab_product_rows']);
    unset($arResult['TABS']['tab_quote']);
    unset($arResult['TABS']['tab_bizproc']);
    unset($arResult['TABS']['tab_615638']);
}elseif($PPParent->arParams['SEF_FOLDER'] == '/crm/lead/') //GK
{
    unset($arResult['TABS']['tab_quote']);
    unset($arResult['TABS']['tab_bizproc']);
}elseif($PPParent->arParams['SEF_FOLDER'] == '/crm/company/')
{
    unset($arResult['TABS']['tab_live_feed']);
    unset($arResult['TABS']['tab_quote']);
    //unset($arResult['TABS']['tab_bizproc']);
    unset($arResult['TABS']['tab_lists_78']);
    unset($arResult['TABS']['tab_lists_79']);
    unset($arResult['TABS']['tab_lists_89']);
    unset($arResult['TABS']['tab_lists_90']);
    unset($arResult['TABS']['tab_tree']);

}
