<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

function getCompanyInn($companyID)
{
    $req = new \Bitrix\Crm\EntityRequisite();
    $rs = $req->getList([
        "filter" => [
            "ENTITY_ID" => $companyID,
            "ENTITY_TYPE_ID" => CCrmOwnerType::Company
        ]
    ]);
    if($row = $rs->fetch())
    {
        return $row["RQ_INN"];
    }
    else
    {
        return '';
    }

    //echo '<pre>'.print_r($row, true).'</pre>';

}

if(!empty($arResult["HEADERS"]))
{
    array_push($arResult["HEADERS"],
        [
            "id" => "CONTRAGENT",
            "name" => Loc::getMessage("FIELD_LEAD"),
            "default" => true,
            "sort" => "",
        ],
        [
            "id" => "INN",
            "name" => Loc::getMessage("FIELD_INN"),
            "default" => true,
            "sort" => "",
        ],
        [
            "id" => "INVOICE_SUMM",
            "name" => Loc::getMessage("FIELD_INVOICE_SUMM"),
            "default" => true,
            "sort" => "",
        ]
    );

    $iblockTypeId = COption::GetOptionString("lists", "livefeed_iblock_type_id");

    // далее будет подготовка фильтра
//    $elementObject = CIBlockElement::getList(
//        $gridSort['sort'],
//        $filter,
//        false,
//        $gridOptions->getNavParams(),
//        $selectFields
//    );
}


if(!empty($arResult["RECORDS"]))
{
    $el = new CIBlockElement();
    $CCrmCompany = new CCrmCompany();
    foreach($arResult["RECORDS"] as $key => $arRecord){
        $arData = $arRecord["data"];
        if($arData["MODULE_ID"]=="lists" && intval($arData["DOCUMENT_ID"])>0){
            $iblockID = $el->GetIBlockByID($arData["DOCUMENT_ID"]);
            //Если БП "Согласование"
            if($iblockID == GetIBlockIDByCode('bp_soglasovaniye'))
            {
                //получаем Контрагента
                $db_props = $el->GetProperty($iblockID, $arData["ID"], array("sort" => "asc"), Array("CODE"=>"NAIMENOVANIE_KLIENTA"));
                if($ar_props = $db_props->Fetch())
                {
                    $companyID = $ar_props["VALUE"];
                    $arCompany = $CCrmCompany->GetByID($companyID);
                    $arData["CONTRAGENT"] = '<a href="/crm/company/show/'.$companyID.'/">'.$arCompany["TITLE"].'</a>';
                    $arData["INN"] = getCompanyInn($companyID);
                }
            }

            //Если БП "Заявка на оплату счетов"
            if($iblockID == GetIBlockIDByCode('application_payment_invoices'))
            {
                //получаем Контрагента
                $db_props = $el->GetProperty($iblockID, $arData["ID"], array("sort" => "asc"), Array("CODE"=>"KONTRAGENT"));
                if($ar_props = $db_props->Fetch()){
                    $companyID = $ar_props["VALUE"];
                    $arCompany = $CCrmCompany->GetByID($companyID);
                    $arData["CONTRAGENT"] = '<a href="/crm/company/show/'.$companyID.'/">'.$arCompany["TITLE"].'</a>';
                    $arData["INN"] = getCompanyInn($ar_props["VALUE"]);
                }

                //получаем Сумму счета
                $db_props = $el->GetProperty($iblockID, $arData["ID"], array("sort" => "asc"), Array("CODE"=>"SUMMA_SCHETA"));
                if($ar_props = $db_props->Fetch())
                    $arData["INVOICE_SUMM"] = CurrencyFormat($ar_props["VALUE"], "RUB");

            }
            $arData["IBLOCK_ID"] = $iblockID;
        }
        $arRecord["data"] = $arData;
        $arResult["RECORDS"][$key] = $arRecord;
    }
}

// добавляем поля в фильтр
$arResult['FILTER'][] = ['id' => 'ID', 'name' => GetMessage('FILTER_FIELD_ID')];
$arResult['FILTER'][] = ['id' => 'DOCUMENT_NAME', 'name' => GetMessage('FILTER_FIELD_DOCUMENT_NAME')];
$arResult['FILTER'][] = ['id' => 'COMMENTS', 'name' => GetMessage('FILTER_FIELD_COMMENTS')];
$arResult['FILTER'][] = ['id' => 'WORKFLOW_PROGRESS', 'name' => GetMessage('FILTER_FIELD_WORKFLOW_PROGRESS')];
$arResult['FILTER'][] = ['id' => 'WORKFLOW_STATE', 'name' => GetMessage('FILTER_FIELD_WORKFLOW_STATE')];
$arResult['FILTER'][] = ['id' => 'LEAD', 'name' => GetMessage('FIELD_LEAD')];