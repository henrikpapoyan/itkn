<?
$MESS["BPWC_WLCT_TOTAL"] = "Всего";
$MESS["CC_BLL_NAME"] = "Название";
$MESS["CT_BLL_BUTTON_NEW_PROCESSES"] = "Запустить процесс";

$MESS['FIELD_LEAD'] = "Контрагент";
$MESS['FIELD_INN'] = "ИНН";
$MESS['FIELD_INVOICE_SUMM'] = "Сумма счета";

$MESS["FILTER_FIELD_ID"] = "ID";
$MESS["FILTER_FIELD_DOCUMENT_NAME"] = "Документ";
$MESS["FILTER_FIELD_COMMENTS"] = "Комментарии";
$MESS["FILTER_FIELD_WORKFLOW_PROGRESS"] = "Процесс выполнения";
$MESS["FILTER_FIELD_WORKFLOW_STATE"] = "Статус бизнес-процесса";

?>