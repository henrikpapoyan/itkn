<?php
/**
 * User: German
 * Date: 17.05.2017
 * Time: 15:53
 */
if (array_search($arResult['ENTITY_TYPE_NAME'], array('LEAD', 'CONTACT', 'COMPANY')) !== false)
{
    global $USER_FIELD_MANAGER;

    if($arResult['ENTITY_TYPE_NAME'] == 'LEAD')
    {
        if($arParams['SECTION'] == 'ORDER') //GK
        {
            $arResult['CONFIG']['center'] = 'UF_DEP,ASSIGNED_BY_ID,UF_PARTNER,UF_CLIENT';
            $arResult['CONFIG']['left'] = 'UF_TYPE_ORDER,SOURCE_ID';
        }
        else
        {
            $arResult['CONFIG']['center'] = 'UF_DEP,ASSIGNED_BY_ID,UF_PARTNER,UF_CLIENT';
            $arResult['CONFIG']['left'] = 'UF_NEED,UF_TYPE,SOURCE_ID,SOURCE_DESCRIPTION,UF_KKT';
        }
        global $USER_FIELD_MANAGER;
        $ufEntityID = 'CRM_LEAD';
        $entityID=$arResult['ENTITY_ID'];
        $arUserFields = $USER_FIELD_MANAGER->GetUserFields($ufEntityID, $entityID, LANGUAGE_ID);
        if($arResult['ENTITY_DATA']["SOURCE_ID"]){
            $arResult['ENTITY_DATA']["SOURCE_ID"]["editable"] = 0;
        }
    }
    elseif($arResult['ENTITY_TYPE_NAME'] == 'CONTACT')
    {
        $arResult['CONFIG']['center'] = 'UF_CRM_592D7A849D74F,ASSIGNED_BY_ID,UF_ACTIVE,UF_ARCHIV';

        $ufEntityID = 'CRM_CONTACT';
        $entityID=$arResult['ENTITY_ID'];
        $arUserFields = $USER_FIELD_MANAGER->GetUserFields($ufEntityID, $entityID, LANGUAGE_ID);

    }elseif($arResult['ENTITY_TYPE_NAME'] == 'COMPANY') {
        $arResult['CONFIG']['left'] = 'UF_STATUS,UF_CRM_INN,UF_FULL_NAME,COMPANY_TYPE,UF_SEGMENT,UF_TENANT,UF_OKVED,UF_REGION,UF_CRM_1492416196,UF_LOYALTY,UF_BRAND';
        $arResult['CONFIG']['center'] = 'UF_NDA,UF_KKT,UF_KKT2,UF_CRM_1486035185,UF_PAYMENT,UF_COST,UF_RATE,UF_ACCOUNT_STATE';
        $arResult['CONFIG']['right'] = 'UF_DEP,ASSIGNED_BY_ID,CONTACT_ID,POST,PHONE,EMAIL';
        $arContact_ID = \Bitrix\Crm\Binding\ContactCompanyTable::getCompanyContactIDs($arResult['ENTITY_ID']);
        $arContact = array();
        $UF_SEX = '';
        global $USER_FIELD_MANAGER;
        if(count($arContact_ID) > 0)
        {
            $rsContact = CCrmContact::GetListEx(array(),array('ID' => $arContact_ID[0]));
            $arContact = $rsContact->Fetch();
            $ufEntityID = 'CRM_CONTACT';
            $entityID = $arContact_ID[0];
            $arUserFields = $USER_FIELD_MANAGER->GetUserFields($ufEntityID, $entityID, LANGUAGE_ID);
            $arHONORIFIC_LIST = CCrmStatus::GetStatusList('HONORIFIC');
            $HONORIFIC = $arHONORIFIC_LIST[$arContact['HONORIFIC']];
            if ($arUserFields['UF_SEX']['VALUE'] > 0)
            {
                $rsSex = CUserFieldEnum::GetList(array(), array('ID' =>$arUserFields['UF_SEX']['VALUE']));
                $arSex = $rsSex->Fetch();
                $UF_SEX = $arSex['VALUE'];
            }
        }
        $v = Array(
            'type' => 'text',
            'editable' => 1,
            'caption' => "Контакт",
            'data' => Array(
                'text' => $arContact['FULL_NAME'],//"FieldVal",
                'multiline' => 1,

            )
        );
        $arResult['ENTITY_DATA']['CONTACT_ID'] = $v;
        $v = Array(
            'type' => 'text',
            'editable' => 1,
            'caption' => "Роль (должность)",
            'data' => Array(
                'text' => $arContact['POST'],
                'multiline' => 1,

            )
        );
        $arResult['ENTITY_DATA']['POST'] = $v;
        $v = Array(
            'type' => 'text',
            'editable' => 1,
            'caption' => "Пол",
            'data' => Array(
                'text' => $UF_SEX,
                'multiline' => 1,

            )
        );
        $arResult['ENTITY_DATA']['UF_SEX'] = $v;

        $v = Array(
            'type' => 'text',
            'editable' => 1,
            'caption' => "Обращение",
            'data' => Array(
                'text' => $HONORIFIC,
                'multiline' => 1,

            )
        );
        $arResult['ENTITY_DATA']['HONORIFIC'] = $v;

        $ufEntityID = 'CRM_COMPANY';
        $entityID = $arResult['ENTITY_ID'];
        $arUserFields = $USER_FIELD_MANAGER->GetUserFields($ufEntityID, $entityID, LANGUAGE_ID);
    }
    //echo '$arUserFields = <pre>'.print_r($arUserFields, true).'</pre>';
    foreach ($arUserFields as $FieldName=>$FieldData) {
        //echo $FieldName.'<pre>'.print_r($FieldData, true).'</pre>';
        $text = "";
        $elements = array();
        $iblockID = 0;
        $value = isset($FieldData['VALUE']) ? $FieldData['VALUE'] : '';
        $caption = isset($FieldData['EDIT_FORM_LABEL']) ? $FieldData['EDIT_FORM_LABEL'] : $FieldName;
        $iblockID = intval($FieldData["SETTINGS"]["IBLOCK_ID"]) > 0 ? $FieldData["SETTINGS"]["IBLOCK_ID"] : 0;
        if($FieldData["USER_TYPE_ID"] == 'iblock_element')
        {
            $res = CIBlockElement::GetByID($FieldData["VALUE"]); //GK
            if($ar_res = $res->Fetch())
            {
                $text = $ar_res['NAME'];
            }
			
            if($iblockID > 0)
            {
                $el = new CIBlockElement();
                $dbResultEl = $el->GetList(
                    array('SORT'=>'ASC'),
                    array('IBLOCK_ID' => $iblockID, "ACTIVE"=>"Y"),
                    false,
                    false,
                    array("ID", "NAME")
                );
	            $res = CIBlockElement::GetByID($FieldData["VALUE"]);

                while ($arElem = $dbResultEl->Fetch())
                {
                    $elements[] = array('ID' => $arElem['ID'], 'VALUE' => $arElem['NAME']);
                    if($text === '' && $value !== '' && $value === $arElem['ID'])
                    {
                        $text = $arElem['NAME'];
                    }
                }
            }
        }

        if($FieldData["USER_TYPE_ID"] == 'iblock_section')
        {
            $res = CIBlockSection::GetByID($FieldData["VALUE"]);
            $sec = new CIBlockSection();
            $dbResultSec = $sec->GetList(
                array('SORT'=>'ASC'),
                array('IBLOCK_ID' => $iblockID, "ACTIVE"=>"Y"),
                false,
                array("ID", "NAME"),
                false
            );
            while ($arSec = $dbResultSec->Fetch())
            {
                $elements[] = array('ID' => $arSec['ID'], 'VALUE' => $arSec['NAME']);
                if($text === '' && $value !== '' && $value === $arSec['ID'])
                {
                    $text = $arSec['NAME'];
                }
            }
        }

        if($FieldData["USER_TYPE_ID"] == 'iblock_element' || $FieldData["USER_TYPE_ID"] == 'iblock_section')
        {
            $entityData = array(
                'type' => $FieldData["USER_TYPE_ID"],
                'editable'=> "Y",
                'caption' => $caption,
                'data' => array(
                    'value' => $value, //ID текущего параметра
                    'text' => $text, //значение текущего параметра
                    'items' => $elements //список для выбора
                )
            );
            $arResult['ENTITY_DATA'][$FieldName] = $entityData;
        }
    }
}
