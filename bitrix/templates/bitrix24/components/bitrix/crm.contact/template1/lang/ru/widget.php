<?
$MESS["CRM_CONTACT_WGT_PAGE_TITLE"] = "Сводный отчет по контактам";
$MESS["CRM_CONTACT_WGT_DEMO_TITLE"] = "Это демо-представление, вы можете скрыть его и получить аналитику по вашим контактам.";
$MESS["CRM_CONTACT_WGT_DEMO_CONTENT"] = "Если у вас еще нет контактов, создайте <a href=\"#URL#\" class=\"#CLASS_NAME#\">первый контакт</a> прямо сейчас!";
$MESS["VIEW_SWITCH_LIST"] = "Список";
$MESS["VIEW_SWITCH_REPORTS"] = "Отчёты";
?>