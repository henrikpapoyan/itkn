BX.ready(function(){
    var arTabLoading = "";
    //обработка открытия вкладки
    BX.addCustomEvent('BX_CRM_INTERFACE_FORM_TAB_SELECTED', BX.delegate(function(self, name, tab_id){
        if (!arTabLoading[tab_id] && self.oTabsMeta[tab_id].name.toLowerCase().indexOf('ккт') !== -1) {
            var innerTab = BX('inner_tab_'+tab_id),
                dealId = 0,
                matches = null,
                waiter = BX.showWait(innerTab);
            if (matches = window.location.href.match(/\/crm\/deal\/show\/([\d\.]+)\//i)) {
                var dealId = parseInt(matches[1]);
                dealId = dealId.toString();
                dealId = dealId.replace(".0000", "");
            }
            if (dealId > 0) {
                //чтобы не грузить повторно
                arTabLoading[tab_id] = true;
                BX.ajax({
                        url: '/ajax/deal/crm_tab_kkt.php',
                        method: 'POST',
                        dataType: 'html',
                        data: {
                            id: dealId
                        },
                        onsuccess: function(data)
                        {
                            innerTab.innerHTML = data;
                            BX.closeWait($this, waiter);
                        },
                        onfailure: function(data)
                        {
                            BX.closeWait(innerTab, waiter);
                        }
                    }
                );
            }
        }

    }));
});