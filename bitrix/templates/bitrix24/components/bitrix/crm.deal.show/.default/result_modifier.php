<?php
/**
 * User: German
 * Date: 08.06.2017
 * Time: 11:04
 */
$rsDeal = CCrmDeal::GetList(array(),array('ID' => $arResult['ELEMENT_ID']),array('ID', 'UF_DOGOVOR'));
$arDeal = $rsDeal->Fetch();
$UF_DOGOVOR = "";
if (count($arDeal['UF_DOGOVOR'])>0)
{
    $rsDogovor = CIBlockElement::GetList(array(),array('ID' => $arDeal['UF_DOGOVOR']));
    while($arDogovor = $rsDogovor->Fetch())
    {
        $UF_DOGOVOR .='<span class="fields enumeration"><a href="/services/lists/'.$arDogovor['IBLOCK_ID'].
            '/element/0/'.$arDogovor['ID'].'/">'.$arDogovor['NAME'].'</a></span><br/>';

    }
}

$arDel = array('CURRENCY_ID',
    'OPPORTUNITY',
    'PROBABILITY',
    'OPENED');
foreach ($arResult['FIELDS']['tab_1'] as $i => $field) {
    if($field['id'] == 'UF_DOGOVOR')
    {
        $arResult['FIELDS']['tab_1'][$i]['value'] = $UF_DOGOVOR;
    }
    if(array_search($field['id'], $arDel) !== false)
    {
        unset($arResult['FIELDS']['tab_1'][$i]);
    }
}