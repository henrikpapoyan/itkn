<?php
/**
 * User: German
 * Date: 27.01.2017
 * Time: 16:53
 */

foreach ($arResult['FIELDS']['tab_1'] as $key => $val) {
	if($val['id'] == 'UF_EVENT')
	{
		$arFilter = array('ID' => $arResult['ELEMENT']['ID']);
		$rsDeal = CCrmDeal::GetList(array(), $arFilter, array("UF_EVENT"));
		$arDeal = $rsDeal->Fetch();
		$IBLOCK_ID = 31;
		$stage_id = $arResult['ELEMENT']['STAGE_ID'];
		if (empty($stage_id))
		{
			$stage_id = 'NEW';
		}
		$arFilter = array(
			"IBLOCK_ID" => $IBLOCK_ID,
			"SECTION_CODE" => "SECTION_".$stage_id,
		);
		$rsEvent = CIBlockElement::GetList(array("SORT" =>"ASC") , $arFilter);
		$event = '<select name="UF_EVENT" size="5"  >
					<option value=""  title="нет">нет</option>';
		while ($arEvent = $rsEvent->Fetch())
		{
			$selected = "";
			if($arEvent['ID'] == $arDeal['UF_EVENT'])
			{
				$selected = 'selected ';
			}
			$event .='<option '.$selected.'value="' . $arEvent['ID'] . '"  title="'.
				$arEvent['NAME'] . '">' .
				$arEvent['NAME'] . "</option>\n";

		}
		$event .='</select>';
		$arResult['FIELDS']['tab_1'][$key]['value'] = $event;
	}

}

