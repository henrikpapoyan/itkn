<?
$MESS["CRM_TAB_1"] = "Проект";
$MESS["CRM_TAB_1_TITLE"] = "Свойства проекта";
$MESS["CRM_TAB_2"] = "События";
$MESS["CRM_TAB_2_TITLE"] = "События проекта";
$MESS["CRM_TAB_3"] = "Бизнес-процессы";
$MESS["CRM_TAB_3_TITLE"] = "Бизнес-процессы проекта";
$MESS["CRM_DEAL_EDIT_TITLE"] = "Проект №#ID# &mdash; #TITLE#";
$MESS["CRM_DEAL_CREATE_TITLE"] = "Новый проект";
$MESS["CRM_DEAL_CONV_ACCESS_DENIED"] = "Для выполнения операции необходимы разрешения на создание счетов и предложений.";
$MESS["CRM_DEAL_CONV_GENERAL_ERROR"] = "Общая ошибка.";
$MESS["CRM_DEAL_CONV_DIALOG_TITLE"] = "Создание сущности на основании сделки";
$MESS["CRM_DEAL_CONV_DIALOG_CONTINUE_BTN"] = "Продолжить";
$MESS["CRM_DEAL_CONV_DIALOG_CANCEL_BTN"] = "Отмена";
$MESS["CRM_DEAL_CONV_DIALOG_SYNC_LEGEND"] = "В выбранных сущностях нет полей, в которые можно передать данные из сделки. Пожалуйста, выберите сущности, в которых будут созданы недостающие поля, чтобы сохранить всю доступную информацию.";
$MESS["CRM_DEAL_CONV_DIALOG_SYNC_FILED_LIST_TITLE"] = "Какие поля будут созданы";
$MESS["CRM_DEAL_CONV_DIALOG_SYNC_ENTITY_LIST_TITLE"] = "Выберите сущности, в которых будут созданы недостающие поля";
?>