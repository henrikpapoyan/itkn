<?
$posFilterStart = strpos($arResult["TASK"]["DESCRIPTION"], '~c~');
$posFilterEnd = strpos($arResult["TASK"]["DESCRIPTION"], '~y');
$str = $arResult["TASK"]["DESCRIPTION"];

$companyID = substr($str, 0, $posFilterEnd);
$companyID = substr($companyID, $posFilterStart+3, $posFilterEnd);

$company = CCrmCompany::GetListEx($arOrder = Array('DATE_CREATE' => 'DESC'), $arFilter = array('ID' => $companyID), $arGroupBy = false, $arNavStartParams = false, $arSelectFields = array(), $arOptions = array());
$ob = $company->Fetch();

$newString = substr($str, 0, $posFilterStart);
$newString .= substr($str, $posFilterEnd+3);

$arResult["TASK"]["DESCRIPTION"] = str_replace('**Компания**', $ob['TITLE'], $newString);

$startTime = strtotime($arResult["TASK"]["MODIFIED"]);
//echo $aRow["data"]['~NAME']." затрачено:<br />";
$crntTime = strtotime(date("d.m.Y H:i:s", time()+CTimeZone::GetOffset()));
$diff = $crntTime - $startTime;
$days = (int)floor($diff/86400);
if($days > 0){
    $diff = $diff - ($days*86400);
}
$hour = (int)floor($diff/3600);
$min = floor(($diff%3600)/60);
$sec = $diff%60;
$zatrachenoText = "$days дн. $hour ч. $min мин. $sec сек.";

//echo $zatrachenoText;
//echo '<hr />';
if($days < 1){
    $zatrachenoText = '<span class="green">'.$zatrachenoText.'</span>';
}
if($days == 1){
    $zatrachenoText = '<span class="orange">'.$zatrachenoText.'</span>';
}
if($days > 1){
    $zatrachenoText = '<span class="red">'.$zatrachenoText.'</span>';
}
$arResult["TASK"]["APPROVAL_LONG"] = $zatrachenoText;
?>