<?
$MESS["CRM_TAB_1"] = "Клиент";
$MESS["CRM_TAB_1_TITLE"] = "Свойства клиента";
$MESS["CRM_TAB_2"] = "События";
$MESS["CRM_TAB_2_TITLE"] = "События клиента";
$MESS["CRM_TAB_3"] = "Бизнес-процессы";
$MESS["CRM_TAB_3_TITLE"] = "Бизнес-процессы клиента";
$MESS["CRM_COMPANY_EDIT_DUP_CTRL_SUMMARY_TITLE"] = "Найденные совпадения";
$MESS["CRM_COMPANY_EDIT_DUP_CTRL_SHORT_SUMMARY_TITLE"] = "найдено";
$MESS["CRM_COMPANY_EDIT_DUP_CTRL_WARNING_DLG_TITLE"] = "Подозрение на дубликаты";
$MESS["CRM_COMPANY_EDIT_DUP_CTRL_WARNING_ACCEPT_BTN_TITLE"] = "Игнорировать и сохранить";
$MESS["CRM_COMPANY_EDIT_DUP_CTRL_WARNING_CANCEL_BTN_TITLE"] = "Отменить";
$MESS["CRM_COMPANY_EDIT_DUP_CTRL_TTL_SUMMARY_TITLE"] = "по названию";
$MESS["CRM_COMPANY_EDIT_DUP_CTRL_PHONE_SUMMARY_TITLE"] = "по телефону";
$MESS["CRM_COMPANY_EDIT_DUP_CTRL_EMAIL_SUMMARY_TITLE"] = "по e-mail";
$MESS["CRM_COMPANY_EDIT_TITLE"] = "Компания №#ID# &mdash; #TITLE#";
$MESS["CRM_COMPANY_CREATE_TITLE"] = "Новый клиент";
?>