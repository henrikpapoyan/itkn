<?php
/**
 * User: German
 * Date: 02.08.2017
 * Time: 18:08
 */
foreach ($arResult['FIELDS']['tab_1'] as $k => $v) {
    if($v['id'] == 'DATE_PAY_BEFORE')
    {
        $arResult['FIELDS']['tab_1'][$k]['required'] = true;

    }
    elseif($v['id'] == 'STATUS_ID')
    {
        if($v['value'] != 'P')
        {
            unset($arResult['FIELDS']['tab_1'][$k]['items']['P']);
        }
    }
}
if ($arResult['ELEMENT']['ID'] == 0)
{
    $company_id = $arResult['ELEMENT']['UF_COMPANY_ID'];
    if($company_id > 0) {

        $arCompany = CCrmCompany::GetByID($company_id);

        $requisite = new \Bitrix\Crm\EntityRequisite();

        $fieldsInfo = $requisite->getFormFieldsInfo();

        $select = array_keys($fieldsInfo);
        $arFilter = array(
            '=ENTITY_TYPE_ID' => 4,
            '=ENTITY_ID' => $company_id,
        );
        $rsRQ = $requisite->getList(
            array(
                'order' => array('SORT' => 'ASC', 'ID' => 'ASC'),
                'filter' => $arFilter,
                'select' => $select
            )
        );

        if ($arRQ = $rsRQ->fetch()) {
            if(empty($arRQ['RQ_INN']))
            {
                $arResult['ERROR_INN'] = 'ОШИБКА! Нет ИНН у контрагента. <a href="/crm/company/edit/'.$company_id.'/"> '.$arCompany['TITLE'].' </a>';
            }
            else
            {
                if(strlen($arRQ['RQ_INN']) == 10)
                {
                    if (empty($arRQ['RQ_KPP']))
                    {
                        $arResult['ERROR_INN'] = 'ОШИБКА! Нет КПП у контрагента. <a href="/crm/company/edit/'.$company_id.'/"> '.$arCompany['TITLE'].' </a>';
                    }
                }
            }
        }
        else
        {
            $arResult['ERROR_INN'] = 'ОШИБКА! Нет ИНН у контрагента. <a href="/crm/company/edit/'.$company_id.'/"> '.$arCompany['TITLE'].' </a>';
        }
    }

}
