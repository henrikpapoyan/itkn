<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
    $APPLICATION->SetTitle("Обучение ИБ");

    $courseIsActual = strtotime($arResult["IB_COURSE_ACTIVE_TO"]) > strtotime(date("d.m.Y H:i:s"));

?>
<div id="statistics-container"
     style="<?=! empty($arResult["UF_CURRENT_STUDENTS"]) ? "" : "display:none"?>">
    <div>
        <table class="crmRoleTable testing-results">
            <tbody>
            <tr>
                <th>
                </th>
                <th>
                    Количество <br> пользователей:
                </th>
            </tr>
            <tr>
                <td>Всего обучающихся:</td>
                <td data-statistics-id="all">
                    <?=count($arResult["UF_CURRENT_STUDENTS"])?>
                </td>
            </tr>
            <tr>
                <td>Тестирование пройдено успешно:</td>
                <td data-statistics-id="success">
                    <?=$arResult["COUNT_USERS_TESTING_SUCCESS"]?>
                </td>
            </tr>
            <tr>
                <td>Тестирование не прошли:</td>
                <td data-statistics-id="failed">
                    <?=$arResult["COUNT_USERS_TESTING_FAILED"]?>
                </td>
            </tr>
            <tr>
                <td>Тестирование не проходили:</td>
                <td data-statistics-id="wait">
                    <?=$arResult["COUNT_USERS_TESTING_WAIT"]?>
                </td>
            </tr>
            <? if ( ! empty($arResult["link2DetailFile"])) { ?>
                <tr>
                    <td>Детальный отчёт:</td>
                    <td>
                        <a href="<?=$arResult["link2DetailFile"]?>">Скачать</a>
                    </td>
                </tr>
            <? } ?>
            </tbody>
        </table>
    </div>
    <div id="new-course-sprint-btn" style="<?=$courseIsActual ? "display:none" : ""?>">
        <button name="crmUserSelect"
           href="javascript:void(0)"
           onclick="newTask(this); return false"
           class="webform-small-button bx24-top-toolbar-button webform-small-button-blue"
        >Новый этап обучения</button>
    </div>
</div>
<div id="addNewCourseSprint"
     style="<?=! $courseIsActual && empty($arResult["UF_CURRENT_STUDENTS"]) ? "" : "display:none"?>">
    <div>Пользователи, которые должны пройти обучение:</div>
    <table class="crmRoleTable hidden" id="students_list">
        <tbody>
        <tr>
            <th>
                Список пользователей:
            </th>
        </tr>
        </tbody>
    </table>

    <div><a name="crmUserSelect"
            href="javascript:void(0)"
            onclick="showUserGropupsDialog(); return false"
            class="webform-small-button bx24-top-toolbar-button webform-small-button-blue"
        >Добавить</a></div>

    <div>Указание длительности прохождения курса:</div>
    <div>
        <span class="task-options-inp-container"><input type="text" class="task-options-inp" id="course-duration"
                                                        value="5"></span>
        <span>дней</span>
    </div>
    <div>Пользователи, которым по окончании обучения придет отчёт:</div>
    <? $APPLICATION->IncludeComponent(
            'bitrix:tasks.widget.member.selector',
            '',
            array(
                'DISPLAY' => 'inline',
                'TYPES'   => array('USER', 'USER.EXTRANET', 'USER.MAIL'),
                'DATA'    => $arResult["DEF_REPORT_VIEWERS"]
            )) ?>
    <div>
        <button  name="crmUserSelect"
           href="javascript:void(0)"
           onclick="BX.addClass(this,'wait');createTask(this); return false"
           class="webform-small-button bx24-top-toolbar-button webform-small-button-blue"
        >Начать курс</button >
    </div>
    <div id="error-block"></div>
</div>