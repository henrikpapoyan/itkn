function showUserGropupsDialog() {
    BX.Access.Init({other: {disabled: true}});
    BX.Access.ShowForm({
        bind: '',
        callback: function (selected) {

            var prepareName = function (str) {
                str = str.replace(/&amp;/g, '&');
                str = str.replace(/&quot;/g, '"');
                str = str.replace(/&lt;/g, '<');
                str = str.replace(/&gt;/g, '>');
                str = str.replace(/,/g, '');
                str = str.replace(/;/g, '');

                return str;
            };

            var result = [];
            for (var provider in selected) {
                if (selected.hasOwnProperty(provider)) {
                    for (var varId in selected[provider]) {
                        if (selected[provider].hasOwnProperty(varId)) {
                            var id = varId;
                            if (id.indexOf('U') === 0)
                                id = id.substr(1);
                            if (id.indexOf('IU') === 0)
                                id = id.substr(2);
                            result.push([provider, varId, prepareName(selected[provider][varId].name)]);
                        }
                    }
                }
            }
            if (result) {
                BX.Access.obSelectedBind = {};
                result.forEach(function (item, i, arr) {
                    addUser(item[0], item[1], item[2]);
                });
            }
        }
    });
};

function removeItem(item) {
    var rm = item.parentNode.parentNode;
    $(rm).remove();
    return false;
}

function addUser(provider, id, name) {

    if ($("#students_list [data-id=" + id + "]").length > 0) {
        return;
    }

    $("#students_list").removeClass('hidden');

    var providerName = '';
    switch (provider) {
        case 'user':
            providerName = 'Пользователь';
            break;

        case 'group':
            providerName = 'Группа пользователей';
            break;
        case 'intranet':
            providerName = 'Отдел';
            break;
        case 'socnetgroup':
            providerName = 'Группа социальной сети';
            break;
    }

    var newRow = '<tr data-id="' +
        id + '" data-provider=' + provider +
        '><td><b>' + providerName +
        ':</b><span>' + name +
        '</span>' +
        '<a href = "javascript:void(0)" onclick = "removeItem(this);" ' +
        'class= "crmPermA crmPermADelete" title="Удалить"></a></td></tr>';
    $("#students_list tr").last().after(newRow);
}

function createTask(_this) {

    $(_this).prop('disabled', true);

    var ajaxData = {
        "students": {
            "group": [],
            "user": [],
            "intranet": [],
            "socnetgroup": []
        },
        "reviewers": [],
        "courseDuration": ""
    };

    var studentRows = $("#students_list tr[data-id]");

    $.each(studentRows, function (index, value) {
        ajaxData.students[$(value).data("provider")].push($(value).data("id"));
    });

    ajaxData.courseDuration = $("#course-duration").val();

    var reviewersRows = $(".task-form-field-item");

    $.each(reviewersRows, function (index, value) {
        ajaxData.reviewers.push($(value).data("item-value"));
    });

    if (
        ajaxData.students.group.length === 0 &&
        ajaxData.students.user.length === 0 &&
        ajaxData.students.socnetgroup.length === 0 &&
        ajaxData.students.intranet.length === 0 ||
        (isNaN(parseInt(ajaxData.courseDuration)) || parseInt(ajaxData.courseDuration) <= 0)
    ) {

        $("#error-block").html("Проверьте правильность заполнения полей!");
        $(_this).removeClass("wait");
        $(_this).prop('disabled', false);
        return;
    } else {
        $("#error-block").html("");
    }

    $.ajax({
        type: 'POST',
        url: '/local/ajax/learning/ib.learning.ajax.php',
        data: ajaxData,
        dataType: 'json',
        cache: false,
        success: function (data) {
            if (data.status == "Starting course sprint") {
                $("#addNewCourseSprint").hide();
                $(_this).removeClass("wait");
                $(_this).prop('disabled', false);
                $("[data-statistics-id=all]").html(data.statistic.all);
                $("[data-statistics-id=success]").html(data.statistic.success);
                $("[data-statistics-id=failed]").html(data.statistic.failed);
                $("[data-statistics-id=wait]").html(data.statistic.wait);
                $("#statistics-container").show();
                $("#new-course-sprint-btn").hide();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log('error...', xhr);
        }
    });
}

function newTask(_this) {
    $(_this).prop('disabled', true);
    $.ajax({
        type: 'POST',
        url: '/local/ajax/learning/ib.learning.ajax.php',
        data: {action: "clearCurrentUsers"},
        dataType: 'json',
        cache: false,
        success: function (data) {
            if (data == 'Current users cleared') {
                $(_this).prop('disabled', false);
                $("#statistics-container").hide();
                $("#addNewCourseSprint").show();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log('error...', xhr);
        }
    });
}