<?php
    CUtil::InitJSCore(array('access'));

    CModule::IncludeModule("learning");
    $res      = CCourse::GetList(
        Array("SORT" => "ASC"),
        Array("CODE" => "COURSE_IB"));
    $arCourse = $res->Fetch();
    $lessonID = $arCourse["LESSON_ID"];

    $arUF = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields("LEARNING_LESSONS", $lessonID);

    $reportViewersIDs = array();
    foreach ($arUF["UF_DEF_REPORT_VIEWER"]["VALUE"] as $id) {
        $rsUser             = CUser::GetByID($id);
        $reportViewersIDs[] = $rsUser->Fetch();
    }

    $currentStudents = array();
    foreach ($arUF["UF_CURRENT_STUDENTS"]["VALUE"] as $id) {
        $currentStudents[] = $id;
    }

    $res    = CTest::GetList(array(), array("COURSE_ID" => $arCourse['ID']));
    $arTest = $res->Fetch();

    $arUserResults = getStatistic($arTest['ID'], $arCourse["ACTIVE_FROM"], $currentStudents);

    $arResult["DEF_REPORT_VIEWERS"]          = $reportViewersIDs;
    $arResult["IB_COURSE_ACTIVE_FROM"]       = $arCourse["ACTIVE_FROM"];
    $arResult["IB_COURSE_ACTIVE_TO"]         = $arCourse["ACTIVE_TO"];
    $arResult["UF_CURRENT_STUDENTS"]         = $currentStudents;
    $arResult["COUNT_USERS_TESTING_SUCCESS"] = count($arUserResults["success"]);
    $arResult["COUNT_USERS_TESTING_FAILED"]  = count($arUserResults["failed"]);
    $arResult["COUNT_USERS_TESTING_WAIT"]    = count($arUserResults["wait"]);
    $arResult["link2DetailFile"]             = $arUF["UF_URL_DETAIL_REPORT"]["VALUE"];


    function getStatistic($TEST_ID, $activeFromDate, $students)
    {
        $arUserResults = array("success" => array(), "failed" => array(), "wait" => array(), "attempts" => array());

        $res = CTestAttempt::GetList(
            Array("SCORE" => "DESC"),
            Array(
                "CHECK_PERMISSIONS" => "N",
                "TEST_ID"           => $TEST_ID,
                "STUDENT_ID"        => $students,
                ">DATE_END"         => $activeFromDate
            )
        );

        while ($arAttempt = $res->GetNext()) {
            $arUserResults["attempts"][$arAttempt["USER_ID"]]++;
            if ($arAttempt["COMPLETED"] == 'Y') {
                $arUserResults["success"][$arAttempt['USER_ID']] = $arAttempt;
            } elseif (empty($arUserResults["success"][$arAttempt["USER_ID"]])) {
                $arUserResults["failed"][$arAttempt['USER_ID']] = $arAttempt;
            }
        }

        foreach ($students as $student) {
            if (empty($arUserResults['success'][$student]) && empty($arUserResults['failed'][$student])) {
                $arWaitUser                      = CUser::GetByID($student)->Fetch();
                $arWaitUser["USER_NAME"]         = "(" . $arWaitUser["LOGIN"] . ") " . $arWaitUser["NAME"] . " " .
                                                   $arWaitUser["LAST_NAME"];
                $arUserResults["wait"][$student] = $arWaitUser;
            }
        }

        return $arUserResults;
    }