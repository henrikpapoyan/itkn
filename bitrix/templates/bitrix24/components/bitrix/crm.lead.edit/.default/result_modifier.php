<?php
/**
 * User: German
 * Date: 13.11.2017
 * Time: 16:06
 */
$Parent = $this->GetComponent()->GetParent();
$UF_AUTO = "";
if($arResult['ELEMENT']['ID'] > 0)
{
    $rsLead = CCrmLead::GetList(array(), array('ID' => $arResult['ELEMENT']['ID']), array('UF_AUTO'));
    if($arLead = $rsLead->Fetch())
    {
        $UF_AUTO = $arLead['UF_AUTO'];
    }
}
if($arResult['ELEMENT']['ID'] == 0 || $UF_AUTO == 'N' ||  $Parent->arParams['SECTION'] == 'ORDER')
{
    foreach ($arResult['FIELDS']['tab_1'] as $key => $val) {
        if($val['id'] == 'STATUS_ID')
        {
            foreach ($val['items'] as $iItem => $vItem) {
                if(array_search($iItem, array('4', '5')) === false)
                {
                    unset($arResult['FIELDS']['tab_1'][$key]['items'][$iItem]);
                }
            }
        }
    }

}elseif($UF_AUTO == 'Y')
{
    foreach ($arResult['FIELDS']['tab_1'] as $key => $val) {
        if($val['id'] == 'STATUS_ID')
        {
            foreach ($val['items'] as $iItem => $vItem) {
                if(array_search($iItem, array('4', '5')) !== false)
                {
                    unset($arResult['FIELDS']['tab_1'][$key]['items'][$iItem]);
                }
            }
        }
    }
}
