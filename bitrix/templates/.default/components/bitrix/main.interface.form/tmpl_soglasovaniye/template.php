<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2013 Bitrix
 */

/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

//color schemes
if($arParams["USE_THEMES"])
	$arThemes = CGridOptions::GetThemes($this->GetFolder());
else
	$arThemes = array();
?>

<div class="bx-interface-form">

<script type="text/javascript">
var bxForm_<?=$arParams["FORM_ID"]?> = null;
</script>

<?if($arParams["SHOW_FORM_TAG"]):?>
<form name="form_<?=$arParams["FORM_ID"]?>" id="form_<?=$arParams["FORM_ID"]?>" action="<?=POST_FORM_ACTION_URI?>" method="POST" enctype="multipart/form-data">

<?=bitrix_sessid_post();?>
<input type="hidden" id="<?=$arParams["FORM_ID"]?>_active_tab" name="<?=$arParams["FORM_ID"]?>_active_tab" value="<?=htmlspecialcharsbx($arResult["SELECTED_TAB"])?>">
<?endif?>
			<table cellspacing="0" class="bx-edit-tabs" width="100%">
				<tr>
					<td class="bx-tab-indent"><div class="empty"></div></td>
<?
$nTabs = count($arResult["TABS"]);
foreach($arResult["TABS"] as $tab):
	$bSelected = ($tab["id"] == $arResult["SELECTED_TAB"]);

	$callback = '';
	if(strlen($tab['onselect_callback']))
	{
		$callback = trim($tab['onselect_callback']);
		if(!preg_match('#^[a-z0-9-_\.]+$#i', $callback))
			$callback = '';
	}
?>
					<td title="<?=htmlspecialcharsbx($tab["title"])?>" id="tab_cont_<?=$tab["id"]?>" class="bx-tab-container<?=($bSelected? "-selected":"")?>" onclick="<?if(strlen($callback)):?><?=$callback?>('<?=$tab["id"]?>');<?endif?>bxForm_<?=$arParams["FORM_ID"]?>.SelectTab('<?=$tab["id"]?>');" onmouseover="if(window.bxForm_<?=$arParams["FORM_ID"]?>){bxForm_<?=$arParams["FORM_ID"]?>.HoverTab('<?=$tab["id"]?>', true);}" onmouseout="if(window.bxForm_<?=$arParams["FORM_ID"]?>){bxForm_<?=$arParams["FORM_ID"]?>.HoverTab('<?=$tab["id"]?>', false);}">
						<table cellspacing="0">
							<tr>
								<td class="bx-tab-left<?=($bSelected? "-selected":"")?>" id="tab_left_<?=$tab["id"]?>"><div class="empty"></div></td>
								<td class="bx-tab<?=($bSelected? "-selected":"")?>" id="tab_<?=$tab["id"]?>"><?=htmlspecialcharsbx($tab["name"])?></td>
								<td class="bx-tab-right<?=($bSelected? "-selected":"")?>" id="tab_right_<?=$tab["id"]?>"><div class="empty"></div></td>
							</tr>
						</table>
					</td>
<?
endforeach;
?>
					<td width="100%"<?if($USER->IsAuthorized() && $arParams["SHOW_SETTINGS"] == true):?> ondblclick="bxForm_<?=$arParams["FORM_ID"]?>.ShowSettings()"<?endif?> style="white-space:nowrap; text-align:right">
<?if(count($arResult["TABS"]) > 1 && $arParams["CAN_EXPAND_TABS"] == true):?>
<a href="javascript:void(0)" onclick="bxForm_<?=$arParams["FORM_ID"]?>.ToggleTabs();" title="<?echo GetMessage("interface_form_show_all")?>" id="bxForm_<?=$arParams["FORM_ID"]?>_expand_link" class="bx-context-button bx-down"><span></span></a>
<?endif?>
<?if($arParams["SHOW_SETTINGS"] || !empty($arThemes)):?>
<a href="javascript:void(0)" onclick="bxForm_<?=$arParams["FORM_ID"]?>.menu.ShowMenu(this, bxForm_<?=$arParams["FORM_ID"]?>.settingsMenu);" title="<?echo GetMessage("interface_form_settings")?>" class="bx-context-button bx-form-menu"><span></span></a>
<?endif;?>
					</td>
				</tr>
			</table>
			<table cellspacing="0" class="bx-edit-tab">
				<tr>
					<td>
<?
$bWasRequired = false;
foreach($arResult["TABS"] as $tab):
?>
<div id="inner_tab_<?=$tab["id"]?>" class="bx-edit-tab-inner"<?if($tab["id"] <> $arResult["SELECTED_TAB"]) echo ' style="display:none;"'?>>
<div style="height: 100%;">
<?if($tab["title"] <> ''):?>
	<div class="bx-edit-tab-title">
	<table cellpadding="0" cellspacing="0" border="0" class="bx-edit-tab-title">
		<tr>
	<?
		if($tab["icon"] <> ""):
	?>
			<td class="bx-icon"><div class="<?=htmlspecialcharsbx($tab["icon"])?>"></div></td>
	<?
		endif
	?>
			<td class="bx-form-title"><?=htmlspecialcharsbx($tab["title"])?></td>
		</tr>
	</table>
	</div>
<?endif;?>

<div class="bx-edit-table">
<table cellpadding="0" cellspacing="0" border="0" class="bx-edit-table <?=(isset($tab["class"]) ? $tab['class'] : '')?>" id="<?=$tab["id"]?>_edit_table">
<?
$i = 0;
$cnt = count($tab["fields"]);
$prevType = '';
foreach($tab["fields"] as $field):
	$style = '';
	if(isset($field["show"]))
	{
		if($field["show"] == "N")
		{
			$style = "display:none;";
		}
	}

	$i++;
	if(!is_array($field))
		continue;

	$className = array();
	if($i == 1)
		$className[] = 'bx-top';
	if($i == $cnt)
		$className[] = 'bx-bottom';
	if($prevType == 'section')
		$className[] = 'bx-after-heading';

	if(strlen($field['class']))
		$className[] = $field['class'];
?>
	<tr<?if(!empty($className)):?> class="<?=implode(' ', $className)?>"<?endif?><?if(!empty($style)):?> style="<?= $style ?>"<?endif?>>
<?
if($field["type"] == 'section'):
?>
		<td colspan="2" class="bx-heading"><?=htmlspecialcharsbx($field["name"])?></td>
<?
else:
	$val = (isset($field["value"])? $field["value"] : $arParams["~DATA"][$field["id"]]);
	$valEncoded = '';
	if(!is_array($val))
		$valEncoded = htmlspecialcharsbx(htmlspecialcharsback($val));

	//default attributes
	if(!is_array($field["params"]))
		$field["params"] = array();
	if($field["type"] == '' || $field["type"] == 'text')
	{
		if($field["params"]["size"] == '')
			$field["params"]["size"] = "30";
	}
	elseif($field["type"] == 'textarea')
	{
		if($field["params"]["cols"] == '')
			$field["params"]["cols"] = "40";
		if($field["params"]["rows"] == '')
			$field["params"]["rows"] = "3";
	}
	elseif($field["type"] == 'date')
	{
		if($field["params"]["size"] == '')
			$field["params"]["size"] = "10";
	}

	$params = '';
	if(is_array($field["params"]) && $field["type"] <> 'file')
	{
		foreach($field["params"] as $p=>$v)
			$params .= ' '.$p.'="'.$v.'"';
	}

	if($field["colspan"] <> true):
		if($field["required"])
			$bWasRequired = true;
?>
		<td class="bx-field-name<?if($field["type"] <> 'label') echo' bx-padding'?>"<?if($field["title"] <> '') echo ' title="'.htmlspecialcharsEx($field["title"]).'"'?>><?=($field["required"]? '<span class="required">*</span>':'')?><?if(strlen($field["name"])):?><?=htmlspecialcharsEx($field["name"])?>:<?endif?></td>
<?
	endif
?>
		<td class="bx-field-value"<?=($field["colspan"]? ' colspan="2"':'')?>>
<?
	switch($field["type"]):
		case 'label':
		case 'custom':
			echo $val;
			if($field["name"]=="КПП"){?>
			    <div class="hint_notice" id="KPP">* Для организаций обязательно указание КПП</div>
			    <div><input type="button" name="search_contragent" id="search_contragent" value="Найти контрагента" /></div>
			    <div id="search_result_kontragent"></div>
                <div id="selected_kontragent">Выбранный контрагент: <span></span></div>
            <?}
			break;
		case 'checkbox':
?>
<input type="hidden" name="<?=$field["id"]?>" value="N">
<input type="checkbox" name="<?=$field["id"]?>" value="Y"<?=($val == "Y"? ' checked':'')?><?=$params?>>
<?
			break;
		case 'textarea':
?>
<textarea name="<?=$field["id"]?>"<?=$params?>><?=$valEncoded?></textarea>
<?
			break;
		case 'list':
		case 'select':
?>
<select name="<?=$field["id"]?>"<?=$params?>>
<?
			if(is_array($field["items"])):
				if(!is_array($val))
					$val = array($val);
				foreach($field["items"] as $k=>$v):
?>
	<option value="<?=htmlspecialcharsbx($k)?>"<?=(in_array($k, $val)? ' selected':'')?>><?=htmlspecialcharsbx($v)?></option>
<?
				endforeach;
?>
</select>
<?
			endif;
			break;
		case 'file':
			$arDefParams = array("iMaxW"=>150, "iMaxH"=>150, "sParams"=>"border=0", "strImageUrl"=>"", "bPopup"=>true, "sPopupTitle"=>false, "size"=>20);
			foreach($arDefParams as $k=>$v)
				if(!array_key_exists($k, $field["params"]))
					$field["params"][$k] = $v;

			echo CFile::InputFile($field["id"], $field["params"]["size"], $val);
			if($val <> '')
				echo '<br>'.CFile::ShowImage($val, $field["params"]["iMaxW"], $field["params"]["iMaxH"], $field["params"]["sParams"], $field["params"]["strImageUrl"], $field["params"]["bPopup"], $field["params"]["sPopupTitle"]);

			break;
		case 'date':
		case 'date_short':
?>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.calendar",
	"",
	array(
		"SHOW_INPUT"=>"Y",
		"INPUT_NAME"=>$field["id"],
		"INPUT_VALUE"=>$val,
		"INPUT_ADDITIONAL_ATTR"=>$params,
		"SHOW_TIME" => $field["type"] === 'date'? 'Y' : 'N',
	),
	$component,
	array("HIDE_ICONS"=>true)
);?>
<?
			break;
		default:
?>
<input type="text" name="<?=$field["id"]?>" value="<?=$valEncoded?>"<?=$params?>>
<?
			break;
	endswitch;
?>
		</td>
<?endif?>
	</tr>
<?
	$prevType = $field["type"];
endforeach;
?>
</table>
</div>
</div>
</div>
<?
endforeach;
?>
					</td>
				</tr>
			</table>
<?
if(isset($arParams["BUTTONS"])):
?>
			<div class="bx-buttons">
<?if($arParams["~BUTTONS"]["standard_buttons"] !== false):?>
	<?if($arParams["BUTTONS"]["back_url"] <> ''):?>
	<input type="submit" name="save" value="<?echo GetMessage("interface_form_save")?>" title="<?echo GetMessage("interface_form_save_title")?>" />
	<?endif?>
	<input type="submit" name="apply" value="<?echo GetMessage("interface_form_apply")?>" title="<?echo GetMessage("interface_form_apply_title")?>" />
	<?if($arParams["BUTTONS"]["back_url"] <> ''):?>
	<input type="button" value="<?echo GetMessage("interface_form_cancel")?>" name="cancel" onclick="window.location='<?=htmlspecialcharsbx(CUtil::addslashes($arParams["~BUTTONS"]["back_url"]))?>'" title="<?echo GetMessage("interface_form_cancel_title")?>" />
	<?endif?>
<?endif?>
<?=$arParams["~BUTTONS"]["custom_html"]?>
			</div>
<?endif?>
<?if($arParams["SHOW_FORM_TAG"]):?>
</form>
<?endif?>

<?if($USER->IsAuthorized() && $arParams["SHOW_SETTINGS"] == true):?>
<div style="display:none">

<div id="form_settings_<?=$arParams["FORM_ID"]?>">
<table width="100%">
	<tr class="section">
		<td><?echo GetMessage("interface_form_tabs")?></td>
	</tr>
	<tr>
		<td align="center">
			<table>
				<tr>
					<td style="background-image:none" nowrap>
						<select style="min-width:150px;" name="tabs" size="10" ondblclick="this.form.tab_edit_btn.onclick()" onchange="bxForm_<?=$arParams["FORM_ID"]?>.OnSettingsChangeTab()">
						</select>
					</td>
					<td style="background-image:none">
						<div style="margin-bottom:5px"><input type="button" name="tab_up_btn" value="<?echo GetMessage("intarface_form_up")?>" title="<?echo GetMessage("intarface_form_up_title")?>" style="width:80px;" onclick="bxForm_<?=$arParams["FORM_ID"]?>.TabMoveUp()"></div>
						<div style="margin-bottom:5px"><input type="button" name="tab_down_btn" value="<?echo GetMessage("intarface_form_up_down")?>" title="<?echo GetMessage("intarface_form_down_title")?>" style="width:80px;" onclick="bxForm_<?=$arParams["FORM_ID"]?>.TabMoveDown()"></div>
						<div style="margin-bottom:5px"><input type="button" name="tab_add_btn" value="<?echo GetMessage("intarface_form_add")?>" title="<?echo GetMessage("intarface_form_add_title")?>" style="width:80px;" onclick="bxForm_<?=$arParams["FORM_ID"]?>.TabAdd()"></div>
						<div style="margin-bottom:5px"><input type="button" name="tab_edit_btn" value="<?echo GetMessage("intarface_form_edit")?>" title="<?echo GetMessage("intarface_form_edit_title")?>" style="width:80px;" onclick="bxForm_<?=$arParams["FORM_ID"]?>.TabEdit()"></div>
						<div style="margin-bottom:5px"><input type="button" name="tab_del_btn" value="<?echo GetMessage("intarface_form_del")?>" title="<?echo GetMessage("intarface_form_del_title")?>" style="width:80px;" onclick="bxForm_<?=$arParams["FORM_ID"]?>.TabDelete()"></div>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr class="section">
		<td><?echo GetMessage("intarface_form_fields")?></td>
	</tr>
	<tr>
		<td align="center">
			<table>
				<tr>
					<td style="background-image:none" nowrap>
						<div style="margin-bottom:5px"><?echo GetMessage("intarface_form_fields_available")?></div>
						<select style="min-width:150px;" name="all_fields" multiple size="12" ondblclick="this.form.add_btn.onclick()" onchange="bxForm_<?=$arParams["FORM_ID"]?>.ProcessButtons()">
						</select>
					</td>
					<td style="background-image:none">
						<div style="margin-bottom:5px"><input type="button" name="add_btn" value="&gt;" title="<?echo GetMessage("intarface_form_add_field")?>" style="width:30px;" disabled onclick="bxForm_<?=$arParams["FORM_ID"]?>.FieldsAdd()"></div>
						<div style="margin-bottom:5px"><input type="button" name="del_btn" value="&lt;" title="<?echo GetMessage("intarface_form_del_field")?>" style="width:30px;" disabled onclick="bxForm_<?=$arParams["FORM_ID"]?>.FieldsDelete()"></div>
					</td>
					<td style="background-image:none" nowrap>
						<div style="margin-bottom:5px"><?echo GetMessage("intarface_form_fields_on_tab")?></div>
						<select style="min-width:150px;" name="fields" multiple size="12" ondblclick="this.form.del_btn.onclick()" onchange="bxForm_<?=$arParams["FORM_ID"]?>.ProcessButtons()">
						</select>
					</td>
					<td style="background-image:none">
						<div style="margin-bottom:5px"><input type="button" name="up_btn" value="<?echo GetMessage("intarface_form_up")?>" title="<?echo GetMessage("intarface_form_up_title")?>" style="width:80px;" disabled onclick="bxForm_<?=$arParams["FORM_ID"]?>.FieldsMoveUp()"></div>
						<div style="margin-bottom:5px"><input type="button" name="down_btn" value="<?echo GetMessage("intarface_form_up_down")?>" title="<?echo GetMessage("intarface_form_down_title")?>" style="width:80px;" disabled onclick="bxForm_<?=$arParams["FORM_ID"]?>.FieldsMoveDown()"></div>
						<div style="margin-bottom:5px"><input type="button" name="field_add_btn" value="<?echo GetMessage("intarface_form_add")?>" title="<?echo GetMessage("intarface_form_add_sect")?>" style="width:80px;" onclick="bxForm_<?=$arParams["FORM_ID"]?>.FieldAdd()"></div>
						<div style="margin-bottom:5px"><input type="button" name="field_edit_btn" value="<?echo GetMessage("intarface_form_edit")?>" title="<?echo GetMessage("intarface_form_edit_field")?>" style="width:80px;" onclick="bxForm_<?=$arParams["FORM_ID"]?>.FieldEdit()"></div>
					</td>
				</tr>
			</table>
		</td>
	</tr>
<?if($arResult["IS_ADMIN"]):?>
	<tr class="section">
		<td><?echo GetMessage("interface_form_common")?></td>
	</tr>
	<tr>
		<td><input type="checkbox" name="set_default_settings" id="set_default_settings_<?=$arParams["FORM_ID"]?>" onclick="BX('delete_users_settings_<?=$arParams["FORM_ID"]?>').disabled = !this.checked;"><label for="set_default_settings_<?=$arParams["FORM_ID"]?>"><?echo GetMessage("interface_form_common_set")?></label></td>
	</tr>
	<tr>
		<td><input type="checkbox" name="delete_users_settings" id="delete_users_settings_<?=$arParams["FORM_ID"]?>" disabled><label for="delete_users_settings_<?=$arParams["FORM_ID"]?>"><?echo GetMessage("interface_form_common_del")?></label></td>
	</tr>
<?endif;?>
</table>
</div>

</div>
<?
endif //$GLOBALS['USER']->IsAuthorized()
?>

<?
$variables = array(
	"mess"=>array(
		"collapseTabs"=>GetMessage("interface_form_close_all"),
		"expandTabs"=>GetMessage("interface_form_show_all"),
		"settingsTitle"=>GetMessage("intarface_form_settings"),
		"settingsSave"=>GetMessage("interface_form_save"),
		"tabSettingsTitle"=>GetMessage("intarface_form_tab"),
		"tabSettingsSave"=>"OK",
		"tabSettingsName"=>GetMessage("intarface_form_tab_name"),
		"tabSettingsCaption"=>GetMessage("intarface_form_tab_title"),
		"fieldSettingsTitle"=>GetMessage("intarface_form_field"),
		"fieldSettingsName"=>GetMessage("intarface_form_field_name"),
		"sectSettingsTitle"=>GetMessage("intarface_form_sect"),
		"sectSettingsName"=>GetMessage("intarface_form_sect_name"),
	),
	"ajax"=>array(
		"AJAX_ID"=>$arParams["AJAX_ID"],
		"AJAX_OPTION_SHADOW"=>($arParams["AJAX_OPTION_SHADOW"] == "Y"),
	),
	"settingWndSize"=>CUtil::GetPopupSize("InterfaceFormSettingWnd"),
	"tabSettingWndSize"=>CUtil::GetPopupSize("InterfaceFormTabSettingWnd", array('width'=>400, 'height'=>200)),
	"fieldSettingWndSize"=>CUtil::GetPopupSize("InterfaceFormFieldSettingWnd", array('width'=>400, 'height'=>150)),
	"component_path"=>$component->GetRelativePath(),
	"template_path"=>$this->GetFolder(),
	"sessid"=>bitrix_sessid(),
	"current_url"=>$APPLICATION->GetCurPageParam("", array("bxajaxid", "AJAX_CALL")),
	"GRID_ID"=>$arParams["THEME_GRID_ID"],
);
?>
<script type="text/javascript">
var formSettingsDialog<?=$arParams["FORM_ID"]?>;

bxForm_<?=$arParams["FORM_ID"]?> = new BxInterfaceForm('<?=$arParams["FORM_ID"]?>', <?=CUtil::PhpToJsObject(array_keys($arResult["TABS"]))?>);
bxForm_<?=$arParams["FORM_ID"]?>.vars = <?=CUtil::PhpToJsObject($variables)?>;
<?if($arParams["SHOW_SETTINGS"] == true):?>
bxForm_<?=$arParams["FORM_ID"]?>.oTabsMeta = <?=CUtil::PhpToJsObject($arResult["TABS_META"])?>;
bxForm_<?=$arParams["FORM_ID"]?>.oFields = <?=CUtil::PhpToJsObject($arResult["AVAILABLE_FIELDS"])?>;
<?endif?>
<?
$settingsMenu = array();
if($arParams["SHOW_SETTINGS"])
{
	$settingsMenu[] = array(
		'TEXT' => GetMessage("intarface_form_mnu_settings"),
		'TITLE' => GetMessage("intarface_form_mnu_settings_title"),
		'ONCLICK' => 'bxForm_'.$arParams["FORM_ID"].'.ShowSettings()',
		'DEFAULT' => true,
		'DISABLED' => ($USER->IsAuthorized()? false:true),
		'ICONCLASS' => 'form-settings'
	);
	if(!empty($arResult["OPTIONS"]["tabs"]))
	{
		if($arResult["OPTIONS"]["settings_disabled"] == "Y")
		{
			$settingsMenu[] = array(
				'TEXT' => GetMessage("intarface_form_mnu_on"),
				'TITLE' => GetMessage("intarface_form_mnu_on_title"),
				'ONCLICK' => 'bxForm_'.$arParams["FORM_ID"].'.EnableSettings(true)',
				'DISABLED' => ($USER->IsAuthorized()? false:true),
				'ICONCLASS' => 'form-settings-on'
			);
		}
		else
		{
			$settingsMenu[] = array(
				'TEXT' => GetMessage("intarface_form_mnu_off"),
				'TITLE' => GetMessage("intarface_form_mnu_off_title"),
				'ONCLICK' => 'bxForm_'.$arParams["FORM_ID"].'.EnableSettings(false)',
				'DISABLED' => ($USER->IsAuthorized()? false:true),
				'ICONCLASS' => 'form-settings-off'
			);
		}
	}
}
if(!empty($arThemes))
{
	$themeItems = array();
	foreach($arThemes as $theme)
	{
		$themeItems[] = array(
			'TEXT' => $theme["name"].($theme["theme"] == $arResult["GLOBAL_OPTIONS"]["theme"]? ' '.GetMessage("interface_form_default"):''),
			'ONCLICK' => 'bxForm_'.$arParams["FORM_ID"].'.SetTheme(this, \''.$theme["theme"].'\')',
			'ICONCLASS' => ($theme["theme"] == $arResult["OPTIONS"]["theme"] || $theme["theme"] == "grey" && $arResult["OPTIONS"]["theme"] == ''? 'checked' : '')
		);
	}

	$settingsMenu[] = array(
		'TEXT' => GetMessage("interface_form_colors"),
		'TITLE' => GetMessage("interface_form_colors_title"),
		'CLASS' => 'bx-grid-themes-menu-item',
		'MENU' => $themeItems,
		'DISABLED' => ($USER->IsAuthorized()? false:true),
		'ICONCLASS' => 'form-themes'
	);
}
?>
bxForm_<?=$arParams["FORM_ID"]?>.settingsMenu = <?=CUtil::PhpToJsObject($settingsMenu)?>;

<?if($arResult["OPTIONS"]["expand_tabs"] == "Y"):?>
BX.ready(function(){bxForm_<?=$arParams["FORM_ID"]?>.ToggleTabs(true);});
<?endif?>
</script>

</div>

<?if($bWasRequired):?>
<div class="bx-form-notes"><span class="required">*</span><?echo GetMessage("interface_form_required")?></div>
<?endif?>
<div id="add_new_kontragent">
    <p>По указанным ИНН/КПП в базе CRM не были найдены контрагенты, необходимо создать нового контрагента. Для этого заполните поля ниже.</p>
    <form id="add_new_kontragent_form" name="add_new_kontragent_form" action="/ajax/company/add_new_kontragent.php">
        <table cellpadding="0" cellspacing="0" border="0" class="bx-edit-table " id="tab_el_edit_table">
            <tbody>
            <tr><td colspan="2">Данные контрагента.</td></tr>
            <tr>
                <td class="bx-field-name bx-padding">
                    Название:
                </td>
                <td class="bx-field-value">
                    <input type="text" id="kontragent_name" name="kontragent_name" readonly value="" />
                </td>
            </tr>
            <tr>
                <td class="bx-field-name bx-padding">
                    ИНН:
                </td>
                <td class="bx-field-value">
                    <input type="text" id="kontragent_inn" name="kontragent_inn" readonly value="" />
                </td>
            </tr>
            <tr>
                <td class="bx-field-name bx-padding">
                    КПП:
                </td>
                <td class="bx-field-value">
                    <input type="text" id="kontragent_kpp" name="kontragent_kpp" readonly value="" />
                </td>
            </tr>
            <tr>
                <td class="bx-field-name bx-padding">
                    Тип компании:
                </td>
                <td class="bx-field-value">
                    <?$arCompanyType = CCrmStatus::GetStatusList('COMPANY_TYPE');?>
                    <select name="kontragent_type" id="kontragent_type">
                        <?foreach($arCompanyType as $id => $val):?>
                            <option value="<?=$id?>"><?=$val?></option>
                        <?endforeach;?>
                    </select>
                </td>
            </tr>
            <tr><td colspan="2">Контакт контрагента.</td></tr>
            <tr>
                <td class="bx-field-name bx-padding">
                    <span class="required">*</span> Имя:
                </td>
                <td class="bx-field-value">
                    <input type="text" class="req" id="contact_name" name="contact_name" value="" />
                </td>
            </tr>
            <tr>
                <td class="bx-field-name bx-padding">
                    <span class="required">*</span> Фамилия:
                </td>
                <td class="bx-field-value">
                    <input type="text" class="req" id="contact_lastname" name="contact_lastname" value="" />
                </td>
            </tr>
            <tr>
                <td class="bx-field-name bx-padding">
                    Отчество:
                </td>
                <td class="bx-field-value">
                    <input type="text" id="contact_secondname" name="contact_secondname" value="" />
                </td>
            </tr>
            <tr>
                <td class="bx-field-name bx-padding">
                    <span class="required">*</span> Телефон:
                </td>
                <td class="bx-field-value">
                    <input type="text" class="req" id="contact_phone" name="contact_phone" value="" />
                </td>
            </tr>
            <tr>
                <td class="bx-field-name bx-padding">
                    <span class="required">*</span> E-mail:
                </td>
                <td class="bx-field-value">
                    <input type="text" class="req" id="contact_email" name="contact_email" value="" />
                </td>
            </tr>
            </tbody>
        </table>
    </form>
    <div id="add_new_kotragent_result"></div>
</div>
<?
//Определям ID свойства
$iblockCode = 'soglasovaniye_3';
if($arParams["FORM_IBLOCK_ID"] == GetIBlockIDByCode('soglasovaniye_nda')){
    $iblockCode = 'soglasovaniye_nda';
}
if($arParams["FORM_IBLOCK_ID"] == GetIBlockIDByCode('signing_documents')){
    $iblockCode = 'signing_documents';
}

$propTipDogovoraID = getPropIDbyCode("TIP_DOGOVORA", $iblockCode);
if($arParams["FORM_IBLOCK_ID"] == GetIBlockIDByCode('signing_documents')){
    $propTipDogovoraID = getPropIDbyCode("TIP_DOKUMENTA", $iblockCode);
}
$propKontragentID = getPropIDbyCode("KONTRAGENT", $iblockCode);
$propNapravlenieSdelkiID = getPropIDbyCode("NAPRAVLENIE_SDELKI", $iblockCode);
$propInnID = getPropIDbyCode("INN", $iblockCode);
$propKppID = getPropIDbyCode("KPP", $iblockCode);
?>
<script type="text/javascript">
    //Посимвольное сравнение строк с подсветкой различий в первом значение
    function comparisonByChar(origString, compareString){
        var originalLength = origString.length,
            compareLength = compareString.length,
            returnString = '';

        maxLength = originalLength;
        if(compareLength>originalLength){
            maxLength = compareLength;
        }

        for(i = 0; i<maxLength; i++){
            if(typeof origString[i] === "undefined"){
                returnString += '<span style="color: #FF0000">'+origString.charAt(i)+'</span>';
            } else {
                if(origString[i] != compareString[i]){
                    returnString += '<span style="color: #FF0000">'+origString[i]+'</span>';
                } else {
                    returnString += origString[i];
                }
            }
        }

        return returnString;
    }
    function setContragent(contragentID, contragentTitle){
        //console.log(contragentID + " - " + contragentTitle);
        $("input[name *= 'PROPERTY_<?=$propKontragentID?>'").val(contragentID);
        $("div#selected_kontragent span").html(contragentTitle);
        $("div#search_result_kontragent").slideUp(500, function () {
            $("div#search_result_kontragent").html('');
            $("div#selected_kontragent").slideDown();
        })
    }

    function interrelationships(dealDirction){
        if(dealDirction == "Хозяйственный"){
            $("select[name = PROPERTY_<?=$propTipDogovoraID?>] option").each(function (index, elem) {
                //console.log($(this).text());
                if($(this).text() != 'Общехозяйственный' && $(this).text() != 'Договор подряда' && $(this).text() != 'Договор производственный')
                {
                    $(this).attr("disabled", "disabled").hide();

                }
                else
                {
                    $(this).removeAttr("disabled").show();
                    if($(this).text() == 'Общехозяйственный'){
                        $(this).attr("selected", "selected");
                    }
                }
            });
        }
        if(dealDirction == "Продажа клиент"){
            $("select[name = PROPERTY_<?=$propTipDogovoraID?>] option").each(function (index, elem) {
                //console.log($(this).text());
                if($(this).text() != 'Клиентский')
                {
                    $(this).attr("disabled", "disabled").hide();

                }
                else
                {
                    $(this).removeAttr("disabled").show();
                    if($(this).text() == 'Клиентский'){
                        $(this).attr("selected", "selected");
                    }
                }
            });
        }
        if(dealDirction == "Продажа агент/партнер"){
            $("select[name = PROPERTY_<?=$propTipDogovoraID?>] option").each(function (index, elem) {
                //console.log($(this).text());
                if($(this).text() != 'Продажа' && $(this).text() != 'Агентский')
                {
                    $(this).attr("disabled", "disabled").hide();

                }
                else
                {
                    $(this).removeAttr("disabled").show();
                    if($(this).text() == 'Продажа'){
                        $(this).attr("selected", "selected");
                    }
                }
            });
        }
    }

    BX.ready(function() {
        var addKontragent = new BX.PopupWindow("add_new_kontragent_popup", null, {
            content: BX('add_new_kontragent'),
            closeIcon: {right: "20px", top: "10px"},
            titleBar: {
                content: BX.create("h3", {
                    html: 'Добавление нового контрагента',
                    'props': {'className': 'access-title-bar'}
                })
            },
            zIndex: 0,
            offsetLeft: 0,
            offsetTop: 0,
            draggable: {restrict: false},
            buttons: [
                new BX.PopupWindowButton({
                    text: "Создать контрагента",
                    className: "popup-window-button-accept",
                    events: {
                        click: function () {
                            buttonAdd = $("div#add_new_kontragent_popup div.popup-window-buttons .popup-window-button-accept");
                            //console.log("popup-window-button-accept = ", buttonAdd.attr("class"));
                            var errorCount = 0;
                            $("#add_new_kontragent_form .req").each(function(){
                                if($(this).val().length==0) {
                                    errorCount++;
                                    $(this).focus();
                                    $(this).addClass('null');
                                }
                                else
                                {
                                    $(this).removeClass('null');
                                }
                            });
                            if(errorCount == 0 && !buttonAdd.hasClass("clicked"))
                            {
                                buttonAdd.addClass("clicked");
                                BX.ajax.submit(BX("add_new_kontragent_form"), function (dataRes) {
                                    buttonAdd.removeClass("clicked");
                                    var rezult = jQuery.parseJSON(dataRes);
                                    //console.log("add_new_kontragent_form = ", rezult);
                                    if(rezult.RESULT=="SUCCESS"){
                                        $("input[name *= 'PROPERTY_<?=$propKontragentID?>'").val(rezult.KONTRAGENT_ID);
                                        $("div#selected_kontragent span").html(rezult.KONTRAGENT_TITLE);
                                        $("div#selected_kontragent").slideDown();
                                        $('#add_new_kontragent').html('');
                                        $.each(rezult.MESSAGE, function(index, text){
                                            $('#add_new_kontragent').append(text+'<br />');
                                        });
                                        $("#add_new_kontragent_popup span.popup-window-button-accept").css("display", "none");
                                    } else{
                                        $('#add_new_kotragent_result').html('');
                                        $.each(rezult.MESSAGE, function(index, text){
                                            $('#add_new_kotragent_result').append(text+'<br />');
                                        });
                                    }

                                });
                            }
                        }
                    }
                }),
                new BX.PopupWindowButton({
                    text: "Закрыть",
                    className: "webform-button-link-cancel",
                    events: {
                        click: function () {
                            this.popupWindow.close(); // закрытие окна
                        }
                    }
                })
            ]
        });
        //Вывод уведомления об обязательности заполнения КПП для организаций. ИНН = 10 символов
        $("input[name *= 'PROPERTY_<?=$propInnID?>']").focusout(function() {
            var inn = $(this).val(),
                kpp = $("input[name *= 'PROPERTY_<?=$propKppID?>']").val();
            if(inn.length == 10 && kpp.length==0)
            {
                $("div#KPP.hint_notice").slideDown();
            }
            else
            {
                $("div#KPP.hint_notice").slideUp();
            }
        });

        //Проверяем заполненность КПП
        $("input[name *= 'PROPERTY_<?=$propKppID?>']").focusout(function() {
            var kpp = $(this).val();
            if(kpp.length == 9)
            {
                $("div#KPP.hint_notice").slideUp();
            }
            else if(kpp.length != 0)
            {
                alert("Укажите корректное значение КПП (9 символов)");
            }
        });

        //Обработка нажатия на "Найти контрагента"
        $("input#search_contragent").click(function(){
            var inn = $("input[name *= 'PROPERTY_<?=$propInnID?>']").val(),
                kpp = $("input[name *= 'PROPERTY_<?=$propKppID?>']").val(),
                startSearch = true;

            //Проверяем корректное заполнение ИНН (10 или 12 символов)
            if(inn.length != 10 && inn.length != 12 && inn.length != 15){
                alert("Укажите корректное значение ИНН (10, 12 или 15 символов)");
                startSearch = false;
            }

            //Проверяем, чтобы если ИНН указано для организации, обазятельно был указан КПП
            if(inn.length == 10 && kpp.length < 9){
                alert("Укажите корректное значение КПП (9 символов)");
                startSearch = false;
            }

            if(startSearch){
                $("div#search_result_kontragent").html('');
                //alert("inn = " + inn + " | kpp = " + kpp);
                $.ajax({
                    url: "/ajax/company/get_company_by_inn_kpp.php",
                    type: "POST",
                    dataType: "JSON",
                    data: ({paramINN: inn, paramKPP: kpp, action: 'filter'}),
                    error: function() {
                        alert("Ошибка поиска по ИНН/КПП. Проверьте соединение с интернетом. В случае повторения ошибки свяжитесь с администратором портала.");
                    },
                    success: function(rezult) {
                        //console.log("rezult = ", rezult);
                        if(rezult.length == 0) {
                            $.ajax({
                                url: "/ajax/company/get_company_by_inn_kpp.php",
                                type: "POST",
                                dataType: "JSON",
                                data: ({paramINN: inn, paramKPP: kpp, action: 'search'}),
                                error: function() {
                                    alert("Ошибка поиска по ИНН/КПП в удаленной базе данных. Проверьте соединение с интернетом. В случае повторения ошибки свяжитесь с администратором портала.");
                                },
                                success: function(rezult) {
                                    //console.log(rezult);
                                    if(rezult.length == 0){
                                        $("div#search_result_kontragent").html('По указанному ИНН не найдено компаний. Проверьте правильность указанного ИНН');
                                    } else{
                                        $("input#kontragent_name").val(rezult.TITLE);
                                        $("input#kontragent_inn").val(rezult.INN);
                                        $("input#kontragent_kpp").val(rezult.KPP);
                                        addKontragent.show();
                                    }
                                }
                            })

                        } else {
                            if(rezult[0].FIND_ONLY_BY_INN == "Y"){
                                if(rezult.length == 1){
                                    $("div#search_result_kontragent").html('<p>По ИНН:'+inn+', КПП:'+comparisonByChar(kpp, rezult[0].RQ_KPP)+' НЕ НАЙДЕН контрагент.</p><p>Найден контрагент \"'+rezult[0].TITLE+'\" с таким же ИНН('+inn+'), но другим КПП('+rezult[0].RQ_KPP+').</p><p>Для выбора контрагента \"'+rezult[0].TITLE+'\" внесите это значение ('+rezult[0].RQ_KPP+') в поле "КПП" и нажмите кнопку "Найти контрагента"</p>');
                                } else {
                                    $("div#search_result_kontragent").html('<p>По ИНН:'+inn+', КПП:'+kpp+' НЕ НАЙДЕН контрагент.</p>');
                                    $("div#search_result_kontragent").append('<p>Найдены следующие контрагенты с таким же ИНН. Выберите необходимого контрагента из списка:</p><ul>');
                                    $.each(rezult, function(index, value){
                                        contragentName = value.TITLE.replace("\"", '&#171;').replace("\"", '&#187;');
                                        $("div#search_result_kontragent ul").append("<li onClick=\"setContragent("+value.COMPANY_ID+", '"+contragentName+"')\">"+value.TITLE+" (КПП: "+comparisonByChar(value.RQ_KPP, kpp)+")</li>");
                                    });
                                }
                            } else {
                                if(rezult.length == 1){
                                    $("input[name *= 'PROPERTY_<?=$propKontragentID?>'").val(rezult[0].COMPANY_ID);
                                    $("div#search_result_kontragent").html('По указанным реквизитам выбран контрагент: "'+rezult[0].TITLE+'"');
                                } else {
                                    $("div#search_result_kontragent").append('<p>По указанному ИНН/КПП найдено более одного контрагента. Выберите необходимого контрагента из списка:</p><ul>');
                                    $.each(rezult, function(index, value){
                                        contragentName = value.TITLE.replace("\"", '&#171;').replace("\"", '&#187;');
                                        $("div#search_result_kontragent ul").append("<li onClick=\"setContragent("+value.COMPANY_ID+", '"+contragentName+"')\">"+value.TITLE+"</li>");
                                    });
                                }
                            }
                        }
                        $("div#search_result_kontragent").slideDown();
                    }
                })
            }
        });

        var currDirection = $("select[name = PROPERTY_<?=$propNapravlenieSdelkiID?>] option::selected").text();
        //console.log("Направление сделки", currDirection);
        interrelationships(currDirection);
        $("select[name = PROPERTY_<?=$propNapravlenieSdelkiID?>]").change(function(){
            var currDirection = $("select[name = PROPERTY_<?=$propNapravlenieSdelkiID?>] option::selected").text();
            interrelationships(currDirection);
        })

        <?if($iblockCode == "soglasovaniye_3" || $iblockCode == "soglasovaniye_nda"):
            $propDateEndContract = getPropIDbyCode("DATA_OKONCHANIYA_DOGOVORA", $iblockCode);
            $propUnlimited = getPropIDbyCode("BESSROCHNYY", $iblockCode);?>
            //Бессрочный
            $("select[name = PROPERTY_<?=$propUnlimited?>]").change(function(){
                var currText = $("select[name = PROPERTY_<?=$propUnlimited?>] option::selected").text();
                if(currText == 'Да'){
                    $("input[name *= 'PROPERTY_<?=$propDateEndContract?>']").val("01.01.2099").attr("readonly", "readonly");
                } else {
                    $("input[name *= 'PROPERTY_<?=$propDateEndContract?>']").val("").removeAttr("readonly");
                }

        })
        <?endif;?>
    });
</script>