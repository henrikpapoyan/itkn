<?
if(stristr($arResult["GRID_ID"], 'CRM_INVOICE_LIST') !== FALSE) {
    $currDir = $APPLICATION->GetCurDir();
// исключение для разделов задачи и задачи группы
    $triggerTaskSection = preg_match("/\/company\/personal\/user\/([0-9]+)\/tasks\//", $currDir) ? true : preg_match("/\/workgroups\/group\/([0-9]+)\/tasks\//", $currDir);
    if(!$USER->IsAdmin() && !$triggerTaskSection){
        $arPermitedColumns = array(
            "ID",
            "DATE_BILL",
            "PAY_VOUCHER_DATE",
            "DATE_INSERT",
            getUserPropFieldNameByXmlId('UF_CRM_INVOICE_INN'),
            "COMMENTS",
            "TAX_VALUE",
            "ACCOUNT_NUMBER",
            "RESPONSIBLE_ID",
            "ENTITIES_LINKS",
            "DATE_PAY_BEFORE",
            "STATUS_ID",
            "PRICE",
            getUserPropFieldNameByXmlId('UF_INVOICE_SUMM_FROM_1C'),
            "ORDER_TOPIC",
        );
        $arNewColumnsAll = array();
        foreach($arResult["COLUMNS_ALL"] as $columnKey => $columnData){
            if(in_array($columnKey, $arPermitedColumns)){
                $arNewColumnsAll[$columnKey] = $columnData;
            }
        }
        $arResult["COLUMNS_ALL"] = $arNewColumnsAll;
    }
}
?>