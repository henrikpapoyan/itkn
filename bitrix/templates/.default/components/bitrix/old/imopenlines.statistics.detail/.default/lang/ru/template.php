<?
$MESS["CT_BLL_SELECTED"] = "Число записей";
$MESS["OL_STAT_BACK"] = "Назад";
$MESS["OL_STAT_BACK_TITLE"] = "Вернуться назад";
$MESS["OL_STAT_USER_ID_CANCEL"] = "Отменить фильтр по сотруднику";
$MESS["OL_STAT_USER_ID_CANCEL_TITLE"] = "Отменить фильтр по сотруднику";
$MESS["OL_STAT_FILTER_CANCEL"] = "Отменить фильтр";
$MESS["OL_STAT_FILTER_CANCEL_TITLE"] = "Отменить фильтр";
$MESS["OL_STAT_EXCEL"] = "Экспорт в Excel";
$MESS["OL_STAT_TITLE"] = "#LINE_NAME# - Статистика";
$MESS["FILTER_FIELD_ID"] = "№";
$MESS["FILTER_FIELD_STATUS_DETAIL"] = "Статус (детальный)";
$MESS["FILTER_FIELD_ACTION"] = "Действия";
$MESS["FILTER_FIELD_PAUSE_TEXT"] = "Закреплен";
$MESS["FILTER_FIELD_MESSAGE_COUNT"] = "Сообщений";
$MESS["FILTER_FIELD_DATE_FIRST_ANSWER"] = "Дата ответа оператора";
$MESS["FILTER_FIELD_DATE_OPERATOR_ANSWER"] = "Дата начала работы оператора";
$MESS["FILTER_FIELD_DATE_LAST_MESSAGE"] = "Дата последнего сообщения";
$MESS["FILTER_FIELD_DATE_MODIFY"] = "Дата изменения";
$MESS["FILTER_FIELD_TIME_DIALOG_WO_BOT"] = "Длительность диалога";
?>