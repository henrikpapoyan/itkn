<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

// добавляем поля в фильтр
$arResult['FILTER']['ID'] = ['id' => 'ID', 'name' => GetMessage('FILTER_FIELD_ID')];
$arResult['FILTER']['STATUS_DETAIL'] = ['id' => 'STATUS_DETAIL', 'name' => GetMessage('FILTER_FIELD_STATUS_DETAIL')];
$arResult['FILTER']['ACTION'] = ['id' => 'ACTION', 'name' => GetMessage('FILTER_FIELD_ACTION')];
$arResult['FILTER']['PAUSE_TEXT'] = ['id' => 'PAUSE_TEXT', 'name' => GetMessage('FILTER_FIELD_PAUSE_TEXT')];
$arResult['FILTER']['MESSAGE_COUNT'] = ['id' => 'MESSAGE_COUNT', 'name' => GetMessage('FILTER_FIELD_MESSAGE_COUNT')];
$arResult['FILTER']['DATE_FIRST_ANSWER'] = ['id' => 'DATE_FIRST_ANSWER', 'name' => GetMessage('FILTER_FIELD_DATE_FIRST_ANSWER'), 'type' => 'date'];
$arResult['FILTER']['DATE_OPERATOR_ANSWER'] = ['id' => 'DATE_OPERATOR_ANSWER', 'name' => GetMessage('FILTER_FIELD_DATE_OPERATOR_ANSWER'), 'type' => 'date'];
$arResult['FILTER']['DATE_LAST_MESSAGE'] = ['id' => 'DATE_LAST_MESSAGE', 'name' => GetMessage('FILTER_FIELD_DATE_LAST_MESSAGE'), 'type' => 'date'];
$arResult['FILTER']['DATE_MODIFY'] = ['id' => 'DATE_MODIFY', 'name' => GetMessage('FILTER_FIELD_DATE_MODIFY'), 'type' => 'date'];
$arResult['FILTER']['TIME_DIALOG_WO_BOT'] = ['id' => 'TIME_DIALOG_WO_BOT', 'name' => GetMessage('FILTER_FIELD_TIME_DIALOG_WO_BOT')];