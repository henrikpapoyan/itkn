BX.ready(function(){
    /** Перемещение поля "Канал" их блока технической информации в шапку */
    //Получение информации по каналу лида
    var ufChannelName = $("tr[data-dragdrop-id=UF_CHANNEL] .crm-offer-info-label").text();
    var ufChannelValue = $("tr[data-dragdrop-id=UF_CHANNEL] [class='fields enumeration']").text();

    //Удаляем строку
    $("tr[data-dragdrop-id=UF_CHANNEL]").remove();

    //Подготавливаем HTML для вставки
    $html = `<tr id="crm_lead_show_v12_qpv_left_uf_channel">
        <td class="crm-lead-header-inner-cell crm-lead-header-inner-cell-move"></td>
        <td class="crm-lead-header-inner-cell crm-lead-header-inner-cell-title">${ufChannelName}</td>
        <td class="crm-lead-header-inner-cell">
            <div class="crm-lead-header-enumeration-wrapper">
                <div class="crm-lead-header-enumeration-view-wrapper">${ufChannelValue}</div>
            </div>
        </td>
        </tr>`;

    //Вставляем элемент перед полем "Источник"
    $("tr[id=crm_lead_show_v12_qpv_left_source_id]").before($html)

    /** Обработка открытия вкладки */
    var arTabLoading = "";
    BX.addCustomEvent('BX_CRM_INTERFACE_FORM_TAB_SELECTED', BX.delegate(function(self, name, tab_id){
        if (!arTabLoading[tab_id] && self.oTabsMeta[tab_id].name.toLowerCase().indexOf('взаимосвязи') !== -1) {
            var innerTab = BX('inner_tab_'+tab_id),
                interrelationshipsTab = $("#inner_tab_"+tab_id),
                leadId = 0,
                matches = null,
                waiter = BX.showWait(innerTab),
                lastSearchText = '';
            if (matches = window.location.href.match(/\/crm\/lead\/show\/([\d\.]+)\//i)) {
                leadId = parseInt(matches[1]);
                leadId = leadId.toString();
                leadId = leadId.replace(".0000", "");
            }
            if (leadId > 0) {
                arTabLoading[tab_id] = true;
                BX.ajax({
                        url: '/ajax/lead/lead_tab_interrelationships.php',
                        method: 'POST',
                        dataType: 'html',
                        data: {
                            id: leadId
                        },
                        onsuccess: function(data)
                        {
                            innerTab.innerHTML = data;
                            BX.closeWait(innerTab, waiter);
                        },
                        onfailure: function(data)
                        {
                            BX.closeWait(innerTab, waiter);
                        }
                    }
                );
                //функция для обхода повторного вызова результатов поиска.
                function searchCompany(searchText = ''){
                    if(searchText!=lastSearchText){
                        var likeText = searchText,
                            innerTab = BX('company-list-search');
                        if(likeText.length>2){
                            var waiter = BX.showWait(BX('add-interrelationships-form'));
                            BX.ajax({
                                    url: '/ajax/lead/get_company_list.php',
                                    method: 'POST',
                                    dataType: 'html',
                                    data: {
                                        likeText: likeText
                                    },
                                    onsuccess: function(data)
                                    {
                                        innerTab.innerHTML = data;
                                        BX.closeWait(innerTab, waiter);
                                        $("#company-list-search").slideDown();
                                        lastSearchText = searchText;
                                    },
                                    onfailure: function(data)
                                    {
                                        BX.closeWait(innerTab, waiter);
                                    }
                                }
                            );
                        }
                    }
                }

                var addInterrelationShips = new BX.PopupWindow("add_interrelationships", null, {
                    content: BX('add-interrelationships-block'),
                    closeIcon: {right: "20px", top: "10px"},
                    titleBar: {content: BX.create("span", {html: '<b>Добавление взаимосвязи</b>', 'props': {'className': 'access-title-bar'}})},
                    zIndex: 0,
                    offsetLeft: 0,
                    offsetTop: 0,
                    draggable: {restrict: false},
                    buttons: [
                        new BX.PopupWindowButton({
                            text: "Добавить",
                            className: "popup-window-button-accept",
                            events: {click: function(){
                                $("#add-interrelationships-form").submit();
                            }}
                        }),
                        new BX.PopupWindowButton({
                            text: "Закрыть",
                            className: "webform-button-link-cancel",
                            events: {click: function(){
                                this.popupWindow.close();
                            }}
                        })
                    ]
                });

                var editInterrelationShips = new BX.PopupWindow("edit_interrelationships", null, {
                    content: BX('edit-interrelationships-block'),
                    closeIcon: {right: "20px", top: "10px"},
                    titleBar: {content: BX.create("span", {html: '<b>Изменение взаимосвязи</b>', 'props': {'className': 'access-title-bar'}})},
                    zIndex: 0,
                    offsetLeft: 0,
                    offsetTop: 0,
                    draggable: {restrict: false},
                    buttons: [
                        new BX.PopupWindowButton({
                            text: "Сохранить",
                            className: "popup-window-button-accept",
                            events: {click: function(){
                                $("#edit-interrelationships-form").submit();
                            }}
                        }),
                        new BX.PopupWindowButton({
                            text: "Закрыть",
                            className: "webform-button-link-cancel",
                            events: {click: function(){
                                this.popupWindow.close();
                            }}
                        })
                    ]
                });

                var delInterrelationShips = new BX.PopupWindow("del_interrelationships", null, {
                    content: BX('del-interrelationships-block'),
                    closeIcon: {right: "20px", top: "10px"},
                    titleBar: {content: BX.create("span", {html: '<b>Удаление взаимосвязи</b>', 'props': {'className': 'access-title-bar'}})},
                    zIndex: 0,
                    offsetLeft: 0,
                    offsetTop: 0,
                    draggable: {restrict: false},
                    buttons: [
                        new BX.PopupWindowButton({
                            text: "Удалить",
                            className: "popup-window-button-accept",
                            events: {click: function(){
                                $("#del-interrelationships-form").submit();
                            }}
                        }),
                        new BX.PopupWindowButton({
                            text: "Закрыть",
                            className: "webform-button-link-cancel",
                            events: {click: function(){
                                this.popupWindow.close();
                            }}
                        })
                    ]
                });

                //Вызов формы добавления
                interrelationshipsTab.on("click", 'a.crm-activity-command-add-interrelationships', function(){
                    var waiter = BX.showWait(innerTab),
                        formBlock = BX('add-interrelationships-block');
                    BX.ajax({
                            url: '/ajax/lead/interrelationships_form.php',
                            method: 'POST',
                            dataType: 'html',
                            data: {
                                action: 'add'
                            },
                            onsuccess: function(data)
                            {
                                BX.closeWait(innerTab, waiter);
                                formBlock.innerHTML = data;
                                addInterrelationShips.show(); // появление окна
                            },
                            onfailure: function(data)
                            {
                                BX.closeWait(innerTab, waiter);
                            }
                        }
                    );
                });

                //Вызов формы удаления
                interrelationshipsTab.on("click", 'td.nav_elem .crm-offer-title-del', function(){
                    var linkTypeID = $(this).parent().data('link-type-id'),
                        companyID = $(this).parent().data('company-id'),
                        waiter = BX.showWait(BX("line_"+companyID)),
                        formBlock = BX('del-interrelationships-block');
                    BX.ajax({
                            url: '/ajax/lead/interrelationships_form.php',
                            method: 'POST',
                            dataType: 'html',
                            data: {
                                action: 'del',
                                link_id: linkTypeID,
                                company_id: companyID,
                            },
                            onsuccess: function(data)
                            {
                                BX.closeWait(BX("line_"+companyID), waiter);
                                formBlock.innerHTML = data;
                                delInterrelationShips.show(); // появление окна
                            },
                            onfailure: function(data)
                            {
                                BX.closeWait(BX("line_"+data.COMPANY_ID), waiter);
                            }
                        }
                    );
                });

                //Вызов формы редактирования
                interrelationshipsTab.on("click", 'td.nav_elem .crm-lead-header-inner-edit-btn', function(){
                    var linkTypeID = $(this).parent().data('link-type-id'),
                        companyID = $(this).parent().data('company-id'),
                        waiter = BX.showWait(BX("line_"+companyID)),
                        formBlock = BX('edit-interrelationships-block');
                    BX.ajax({
                            url: '/ajax/lead/interrelationships_form.php',
                            method: 'POST',
                            dataType: 'html',
                            data: {
                                action: 'edit',
                                link_id: linkTypeID,
                                company_id: companyID,
                            },
                            onsuccess: function(data)
                            {
                                BX.closeWait(BX("line_"+companyID), waiter);
                                formBlock.innerHTML = data;
                                editInterrelationShips.show(); // появление окна
                            },
                            onfailure: function(data)
                            {
                                BX.closeWait(BX("line_"+companyID), waiter);
                            }
                        }
                    );
                });

                //Отслеживаем ввод текста в строку с названием компании
                $("div.interrelationships-block").on('change paste keyup', "form.interrelationships-form input#company-text", function(){
                    var likeText = $(this).val();
                    searchCompany(likeText);
                    $("#add_interrelationships_result").text('');
                });

                //Выбор компании из найденных компаний
                $("div.interrelationships-block").on('click', "#company-list-search li", function(){
                    companyID = $(this).data('company-id');
                    $("input#company-id").val(companyID);
                    $("input#company-text").val($(this).text());
                    $("#company-list-search").slideUp();
                });

                //Обрабатываем нажатие на кнопку добавить.
                $("div.interrelationships-block").on('submit', "form.interrelationships-form", function(){
                    var companyID=$(this).find("input#company-id").val(),
                        formAction=$(this).find("input#action").val(),
                        innerTab = BX('add_interrelationships_result'),
                        waiter = BX.showWait(BX('add-interrelationships-form')),
                        formBlock = $(this);
                    if(formAction=="add") {
                        var linkType=formBlock.find("select#interrelationships-type").val(),
                            oldCompanyID = 0;
                    }
                    if(formAction=="edit") {
                        var linkType=formBlock.find("select#interrelationships-type").val(),
                            oldCompanyID = formBlock.find("input#old-company-id").val();
                    }
                    if(formAction=="del"){
                        var linkType = 1,
                            oldCompanyID = 1;
                    }

                    if(linkType>0&&companyID>0&&leadId>0){
                        BX.ajax({
                                url: '/ajax/lead/add_new_interrelationships.php',
                                method: 'POST',
                                dataType: 'JSON',
                                data: {
                                    leadId: leadId,
                                    linkType: linkType,
                                    companyID: companyID,
                                    action: formAction,
                                    oldCompanyID: oldCompanyID,
                                },
                                onsuccess: function(data)
                                {
                                    innerTab.innerHTML = data.MESS;
                                    BX.closeWait(innerTab, waiter);

                                    if(data.RESULT=="success"){
                                        if(formAction=="edit"){
                                            $("div#interrelationships-list table.main-grid-table tbody tr#line_"+oldCompanyID).html(
                                                '<td class="main-grid-cell main-grid-cell-left">' +
                                                    '<span class="main-grid-cell-content link_type">'+formBlock.find("select#interrelationships-type option:selected").text()+'</span>' +
                                                '</td>' +
                                                '<td class="main-grid-cell main-grid-cell-left">' +
                                                    '<span class="main-grid-cell-content company_name">' + formBlock.find("input#company-text").val() + '</span>' +
                                                '</td>' +
                                                '<td class="main-grid-cell main-grid-cell-left nav_elem">' +
                                                    '<span class="main-grid-cell-content" data-elem-id="'+data.ELEM_ID+'" data-link-type-id="'+data.LINK_TYPE_ID+'" data-company-id="'+data.COMPANY_ID+'">' +
                                                        '<span class="crm-lead-header-inner-edit-btn"></span>' +
                                                        '<span class="crm-offer-title-del"></span>' +
                                                    '</span>' +
                                                '</td>'
                                            );
                                            editInterrelationShips.close();
                                            var scroll_el = $("tr#line_"+data.COMPANY_ID); // возьмем содержимое атрибута href, должен быть селектором, т.е. например начинаться с # или .
                                            if (scroll_el.length != 0) { // проверим существование элемента чтобы избежать ошибки
                                                $('html, body').animate({ scrollTop: scroll_el.offset().top }, 500); // анимируем скроолинг к элементу scroll_el
                                            }
                                        }
                                        if(formAction=="add"){
                                            if($("div#interrelationships-list table.main-grid-table tbody").length==0){
                                                window.location.reload()
                                            } else {
                                                $("div#interrelationships-list table.main-grid-table tbody").append(
                                                    '<tr class="main-grid-row main-grid-row-body" id="line_'+data.COMPANY_ID+'">' +
                                                        '<td class="main-grid-cell main-grid-cell-left">' +
                                                            '<span class="main-grid-cell-content link_type">'+formBlock.find("select#interrelationships-type option:selected").text()+'</span>' +
                                                        '</td>' +
                                                        '<td class="main-grid-cell main-grid-cell-left">' +
                                                            '<span class="main-grid-cell-content company_name">' + formBlock.find("input#company-text").val() + '</span>' +
                                                        '</td>' +
                                                        '<td class="main-grid-cell main-grid-cell-left nav_elem">' +
                                                            '<span class="main-grid-cell-content" data-elem-id="'+data.ELEM_ID+'" data-link-type-id="'+data.LINK_TYPE_ID+'" data-company-id="'+data.COMPANY_ID+'">' +
                                                                '<span class="crm-lead-header-inner-edit-btn"></span>' +
                                                                '<span class="crm-offer-title-del"></span>' +
                                                            '</span>' +
                                                        '</td>' +
                                                    '</tr>'
                                                );
                                                var scroll_el = $("tr#line_"+data.COMPANY_ID); // возьмем содержимое атрибута href, должен быть селектором, т.е. например начинаться с # или .
                                                if (scroll_el.length != 0) { // проверим существование элемента чтобы избежать ошибки
                                                    $('html, body').animate({ scrollTop: scroll_el.offset().top }, 500); // анимируем скроолинг к элементу scroll_el
                                                }
                                            }
                                            formBlock.find("input#company-text").val('');
                                            addInterrelationShips.close();
                                        }
                                        if(formAction=="del"){
                                            var scroll_el = $("tr#line_"+data.COMPANY_ID); // возьмем содержимое атрибута href, должен быть селектором, т.е. например начинаться с # или .
                                            if (scroll_el.length != 0) { // проверим существование элемента чтобы избежать ошибки
                                                $('html, body').animate({ scrollTop: scroll_el.offset().top }, 500); // анимируем скроолинг к элементу scroll_el
                                            }
                                            if($("div#interrelationships-list table.main-grid-table tbody tr").length==1){
                                                $("div#interrelationships-list table.main-grid-table tbody tr#line_" + data.COMPANY_ID).slideUp(1000, function(){
                                                    $(this).remove();
                                                });
                                                window.location.reload()
                                            } else {
                                                $("div#interrelationships-list table.main-grid-table tbody tr#line_" + data.COMPANY_ID).slideUp(1000, function() {
                                                    $(this).remove();
                                                });
                                            }
                                            delInterrelationShips.close();
                                        }
                                    }
                                },
                                onfailure: function(data)
                                {
                                    BX.closeWait(innerTab, waiter);
                                }
                            }
                        );
                    }
                    return false;
                });
            }
        }
    }));

});