<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if($_REQUEST['workflow_template_id']){
	$tid = $_REQUEST['workflow_template_id'];
	if($arResult['TEMPLATES'][$tid]['PARAMETERS']['DocDesignerCompanyContract'] && $arResult['DOCUMENT_TYPE'] == 'DEAL'){
		//if($arResult['TEMPLATES'][$tid]['PARAMETERS']['DocDesignerCompanyContract']['Type'] == 'select'){
			CModule::IncludeModule("iblock");
            CModule::IncludeModule("crm");
            $arDeal = CCrmDeal::GetByID(substr($arResult['DOCUMENT_ID'], 5));
            $CompanyID = $arDeal['COMPANY_ID'];
            $InputControlCompCont = '<select name="DocDesignerCompanyContract"><option value="">'.GetMessage('BPABS_NO').'</option>';
            $IBLOCK_IB = COption::GetOptionInt("htmls.docdesigner", "DOCDESIGNER_DOCS_REGISTER_ID");
            $NUMS_IB = COption::GetOptionInt("htmls.docdesigner", "DOCDESIGNER_CONTRACTS_NUMERATORS");
            $arSelect = Array("ID", "NAME", "DATE_CREATE");
			$arFilter = Array("IBLOCK_ID"=>IntVal($IBLOCK_IB), "PROPERTY_COMPANY_ID"=>$CompanyID, "ACTIVE"=>"Y", "PROPERTY_NUMERATOR" => explode(',', $NUMS_IB));

			$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
			while($ob = $res->GetNextElement()){
				$arFields = $ob->GetFields();
				//$arOptions[$arFields['ID']] = $arFields['NAME'] . " (".substr($arFields['DATE_CREATE'], 0, 10).")";
				$InputControlCompCont .= '<option value="'.$arFields['ID'].'">'.$arFields['NAME']. " (".substr($arFields['DATE_CREATE'], 0, 10).")".'</option>';
			}

			$InputControlCompCont .= "</select>";
			$arResult['DOCDESIGNER']['PARAMETERS']['DocDesignerCompanyContract'] = $InputControlCompCont;
		//}
	}
}
?>