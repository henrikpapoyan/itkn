<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$GLOBALS['APPLICATION']->AddHeadString('<script src="/bitrix/js/main/utils.js"></script>', true);
CBPDocument::AddShowParameterInit($arParams["DOCUMENT_TYPE"][0], "only_users", $arParams["DOCUMENT_TYPE"][2], $arParams["DOCUMENT_TYPE"][1]);
?>
<div class="bizproc-page-workflow-start">
<?
if (!empty($arResult["ERROR_MESSAGE"])):
	ShowError($arResult["ERROR_MESSAGE"]);
endif;
//htmls - start
$IBLOCK_ENT = COption::GetOptionInt("htmls.docdesigner", "DOCDESIGNER_COMPANY", 0);
$IBLOCK_TLIB = COption::GetOptionInt("htmls.docdesigner", "DOCDESIGNER_CONTRACTS_TEMPLATES_LIBRARY", 0);
if($IBLOCK_ENT > 0 or $IBLOCK_TLIB > 0){
	CModule::IncludeModule("iblock");
}
//htmls - end
if ($arResult["SHOW_MODE"] == "StartWorkflowSuccess")
{
	ShowNote(str_replace("#TEMPLATE#", $arResult["TEMPLATES"][$arParams["TEMPLATE_ID"]]["NAME"], GetMessage("BPABS_MESSAGE_SUCCESS")));
}
elseif ($arResult["SHOW_MODE"] == "StartWorkflowError")
{
	ShowNote(str_replace("#TEMPLATE#", $arResult["TEMPLATES"][$arParams["TEMPLATE_ID"]]["NAME"], GetMessage("BPABS_MESSAGE_ERROR")));
}
elseif ($arResult["SHOW_MODE"] == "WorkflowParameters")
{
?>
<form method="post" name="start_workflow_form1" action="<?=POST_FORM_ACTION_URI?>" enctype="multipart/form-data">
	<input type="hidden" name="workflow_template_id" value="<?=intval($arParams["TEMPLATE_ID"]) ?>" />
	<input type="hidden" name="document_type" value="<?= htmlspecialchars($arParams["DOCUMENT_TYPE"][2]) ?>" />
	<input type="hidden" name="document_id" value="<?= htmlspecialchars($arParams["DOCUMENT_ID"][2]) ?>" />
	<input type="hidden" name="back_url" value="<?= htmlspecialchars($arResult["back_url"]) ?>" />
	<?= bitrix_sessid_post() ?>
<fieldset class="bizproc-item bizproc-workflow-template">
	<legend class="bizproc-item-legend bizproc-workflow-template-title">
		<?=$arResult["TEMPLATES"][$arParams["TEMPLATE_ID"]]["NAME"]?>
	</legend>
	<?if($arResult["TEMPLATES"][$arParams["TEMPLATE_ID"]]["DESCRIPTION"]!=''):?>
	<div class="bizproc-item-description bizproc-workflow-template-description">
		<?= $arResult["TEMPLATES"][$arParams["TEMPLATE_ID"]]["DESCRIPTION"] ?>
	</div>
	<?endif;

	if (!empty($arResult["TEMPLATES"][$arParams["TEMPLATE_ID"]]["PARAMETERS"]))
	{
?>
	<div class="bizproc-item-text">
		<ul class="bizproc-list bizproc-workflow-template-params">
<?
	foreach ($arResult["TEMPLATES"][$arParams["TEMPLATE_ID"]]["PARAMETERS"] as $parameterKey => $arParameter)
	{
		if ($parameterKey == "TargetUser")
			continue;
		if ($parameterKey == "DocDesignerFile")//htmls - start
			continue;
		if($parameterKey == "DocDesignerCompany"){
			if($IBLOCK_ENT > 0){
				$InputControlComp = '<select name="DocDesignerCompany"><option value="">'.GetMessage('BPABS_NO').'</option>';
				$res = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => $IBLOCK_ENT, "ACTIVE" => "Y"), false, false, Array("ID", "NAME"));
				while($ob = $res->GetNextElement()){
					$arFields = $ob->GetFields();
					$arEnt[$arFields['ID']] = $arFields['NAME'];
					if($arParameter['Default'] == $arFields['ID'])
						$InputControlComp .= '<option value="'.$arFields['ID'].'" selected>'.$arFields['NAME'].'</option>';
					else
						$InputControlComp .= '<option value="'.$arFields['ID'].'">'.$arFields['NAME'].'</option>';
				}
				$InputControlComp .= "</select>";
			}
		}
		if($parameterKey == "DocDesignerDogTemplateID"){
        	if($IBLOCK_TLIB > 0){
				$InputControlTemp = '<select name="DocDesignerDogTemplateID"><option value="">(�� �����������)</option>';
				$res = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => $IBLOCK_TLIB, "ACTIVE" => "Y", "NAME" => "%docx"), false, false, Array("ID", "NAME"));
				while($ob = $res->GetNextElement()){
					$arFields = $ob->GetFields();
					$arEnt[$arFields['ID']] = $arFields['NAME'];
					if($arParameter['Default'] == $arFields['ID'])
						$InputControlTemp .= '<option value="'.$arFields['ID'].'" selected>'.$arFields['NAME'].'</option>';
					else
						$InputControlTemp .= '<option value="'.$arFields['ID'].'">'.$arFields['NAME'].'</option>';
				}
				$InputControlTemp .= "</select>";
			}
		}
		if($parameterKey == "DocDesignerNumerator"){
			$InputControlNumerator = '<select name="DocDesignerNumerator"><option value="0">(�� �����������)</option>';
			$arSelect = Array("ID", "NAME");
			$arFilter = Array("IBLOCK_CODE"=>"htmls.numerators", "ACTIVE"=>"Y");
			$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
			$arResult[0] = GetMessage('NO_NUMERATOR');
			while($ob = $res->GetNextElement()){
				$arFields = $ob->GetFields();
				//$arResult[$arFields['ID']] = $arFields['NAME'];
				$InputControlNumerator .= '<option value="'.$arFields['ID'].'">'.$arFields['NAME'].'</option>';
			}
			//return $arResult;
			$InputControlNumerator .= "</select>";
		}
		if($parameterKey == "DocDesignerJournal"){

		}
		//htmls - end
?>
		<li class="bizproc-list-item bizproc-workflow-template-param">
			<div class="bizproc-field bizproc-field-<?=$arParameter["Type"]?>">
				<label class="bizproc-field-name">
					<?=($arParameter["Required"] ? "<span class=\"required\">*</span> " : "")?>
						<span class="bizproc-field-title"><?=htmlspecialchars($arParameter["Name"])?></span><?
					if (strlen($arParameter["Description"]) > 0):
						?><span class="bizproc-field-description"> (<?=htmlspecialchars($arParameter["Description"])?>)</span><?
					endif;
					?>:
				</label>
				<span class="bizproc-field-value"><?//print "<pre>"; print_r($arParameter); print "</pre>";
					if($parameterKey == "DocDesignerCompany" && $arParameter['Type'] != 'UF:iblock_element'){//htmls - start
						echo $InputControlComp;
					}
					elseif($parameterKey == "DocDesignerDogTemplateID" && $arParameter['Type'] != 'UF:iblock_element'){
						echo $InputControlTemp;
					}
					elseif($parameterKey == "DocDesignerNumerator"){
						echo $InputControlNumerator;
					}
					elseif($parameterKey == "DocDesignerCompanyContract" && $arParameter['Type'] == 'UF:iblock_element'){
						echo $arResult['DOCDESIGNER']['PARAMETERS']['DocDesignerCompanyContract'];
					}
					else{//bitrix
					echo $arResult["DocumentService"]->GetFieldInputControl(
						$arParams["DOCUMENT_TYPE"],
						$arParameter,
						array("Form" => "start_workflow_form1", "Field" => $parameterKey),
						$arResult["PARAMETERS_VALUES"][$parameterKey],
						false,
						true
					);
					}//htmls - end
				?></span>
			</div>
		</li>
<?
	}
?>
		</ul>
	</div>
<?
	}
?>
	<div class="bizproc-item-buttons bizproc-workflow-start-buttons">
		<input type="submit" name="DoStartParamWorkflow" value="<?= GetMessage("BPABS_DO_START") ?>" />
		<input type="submit" name="CancelStartParamWorkflow" value="<?= GetMessage("BPABS_DO_CANCEL") ?>" />
	</div>
</fieldset>
</form>
<?
}
elseif ($arResult["SHOW_MODE"] == "SelectWorkflow" && count($arResult["TEMPLATES"]) > 0)
{
	foreach($_GET as $key => $val):
		if (in_array(strtolower($key), array("sessid", "workflow_template_id")))
			continue;
	endforeach;
	$bFirst = true;
?>
	<ul class="bizproc-list bizproc-workflow-templates">
		<?foreach ($arResult["TEMPLATES"] as $workflowTemplateId => $arWorkflowTemplate):?>
			<li class="bizproc-list-item bizproc-workflow-template">
				<div class="bizproc-item-title">
					<a href="<?=$arResult["TEMPLATES"][$arWorkflowTemplate["ID"]]["URL"]?>"><?=$arWorkflowTemplate["NAME"]?></a>
				</div>
				<?if (strlen($arWorkflowTemplate["DESCRIPTION"]) > 0):?>
				<div class="bizproc-item-description">
					<?= $arWorkflowTemplate["DESCRIPTION"] ?>
				</div>
				<?endif;?>
			</li>
		<?endforeach;?>
	</ul>
<?
}
elseif ($arResult["SHOW_MODE"] == "SelectWorkflow")
{
	ShowNote(GetMessage("BPABS_NO_TEMPLATES"));
}
?>
</div>