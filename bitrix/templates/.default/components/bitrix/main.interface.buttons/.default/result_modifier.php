<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)die();

// выбираем ID состояния лида Активный
$leadStateId = 0;
$leadStateId = getIdElementListState("XML_STATE_LEAD_ACTIVITY");

$countLeads = 0;

$arOrderLead = array();
$arFilterLead = array(
//    "UF_STATE_LEAD" => $leadStateId
    'STATUS_ID' => '4'
);

$arFilterLead['!SOURCE_ID'] = gk_GetOrderFilter(); //GK

$arSelectLead = array("ID");

$dbLeads = CCrmLead::GetList($arOrderLead, $arFilterLead, $arSelectLead);
$countLeads = $dbLeads->SelectedRowsCount();

$arResult["LEAD_ALL_COUNT"] = $countLeads;
?>