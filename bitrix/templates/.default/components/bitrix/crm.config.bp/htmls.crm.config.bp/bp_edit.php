<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();?>
<?
CJSCore::Init(array('jquery'));
?>
<!--script type="text/javascript" src="/bitrix/js/htmls.docdesigner/jquery-1.7.1.min.js?time=<?=time()?>"></script-->

<!--script type="text/javascript" src="/bitrix/js/htmls.docdesigner/tinymce/jscripts/tiny_mce/tiny_mce.js?time=<?=time()?>"></script>
<script type="text/javascript" src="/bitrix/js/htmls.docdesigner/tinymce/jscripts/tiny_mce/jquery.tinymce.js?time=<?=time()?>"></script-->

<!--script src="//tinymce.cachefly.net/4.0/tinymce.min.js"></script-->
<?
$APPLICATION->IncludeComponent(
	'bitrix:crm.config.bp.edit',
	'htmls.crm.config.bp.edit',
	Array(
		'BP_ENTITY_ID' => $arResult['VARIABLES']['entity_id'],
		'BP_BP_ID' => $arResult['VARIABLES']['bp_id'],
		'ENTITY_LIST_URL' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['entity_list'],
		'BP_LIST_URL' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['bp_list'],
		'BP_EDIT_URL' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['bp_edit']
	),
	$component
);
?>