<?
$MESS["CRM_CRL_LEAD_TITLE_DEFAULT"] = "Unbenannt";
$MESS["CRM_CRL_LEAD_TITLE"] = "Bezeichnung";
$MESS["CRM_CRL_NO_CLIENTS"] = "Es wurden keine Kontakte gefunden, die an den wiederkehrenden Lead gebunden werden können";
$MESS["CRM_CRL_DEAL_NOT_EXISTS"] = "Daten des Auftrags konnten nicht erhalten werden";
$MESS["CRM_CRL_RESPONSIBLE"] = "Verantwortliche Person";
?>