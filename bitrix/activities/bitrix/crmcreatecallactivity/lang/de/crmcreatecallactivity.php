<?
$MESS["CRM_CREATE_CALL_EMPTY_PROP"] = "Der erforderliche Parameter ist leer: #PROPERTY#";
$MESS["CRM_CREATE_CALL_SUBJECT"] = "Betreff";
$MESS["CRM_CREATE_CALL_START_TIME"] = "Anfangsdatum";
$MESS["CRM_CREATE_CALL_END_TIME"] = "Abschlussdatum";
$MESS["CRM_CREATE_CALL_IS_IMPORTANT"] = "Wichtig";
$MESS["CRM_CREATE_CALL_DESCRIPTION"] = "Beschreibung";
$MESS["CRM_CREATE_CALL_NOTIFY_VALUE"] = "Erinnern";
$MESS["CRM_CREATE_CALL_NOTIFY_TYPE"] = "Zeitabstand der Erinnerung";
$MESS["CRM_CREATE_CALL_RESPONSIBLE_ID"] = "Verantwortlich";
$MESS["CRM_CREATE_CALL_AUTO_COMPLETE"] = "Aktivität automatisch abschließen, wenn Status aktualisiert wird";
?>