<?
$MESS["CRM_SSMSA_EMPTY_TEXT"] = "Не вказаний текст повідомлення";
$MESS["CRM_SSMSA_EMPTY_PROVIDER"] = "Не вибраний провайдер";
$MESS["CRM_SSMSA_MESSAGE_TEXT"] = "Текст повідомлення";
$MESS["CRM_SSMSA_PROVIDER"] = "Провайдер";
$MESS["CRM_SSMSA_SELECT_PROVIDER"] = "Виберіть...";
$MESS["CRM_SSMSA_EMPTY_PHONE_NUMBER"] = "Не вказано номер телефону";
$MESS["CRM_SSMSA_NO_PROVIDER"] = "SMS провайдер не зареєстрований або видалений";
$MESS["CRM_SSMSA_REST_SESSION_ERROR"] = "REST сесія заблокована";
$MESS["CRM_SSMSA_PAYMENT_REQUIRED"] = "Застосунок не оплачено";
?>