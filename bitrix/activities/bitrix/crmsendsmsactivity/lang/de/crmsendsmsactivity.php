<?
$MESS["CRM_SSMSA_EMPTY_TEXT"] = "Der Nachrichtentext ist leer.";
$MESS["CRM_SSMSA_EMPTY_PROVIDER"] = "Es wurde kein Anbieter ausgewählt.";
$MESS["CRM_SSMSA_MESSAGE_TEXT"] = "Nachrichtentext";
$MESS["CRM_SSMSA_PROVIDER"] = "Anbieter";
$MESS["CRM_SSMSA_EMPTY_PHONE_NUMBER"] = "Telefonnummer ist nicht angegeben";
$MESS["CRM_SSMSA_NO_PROVIDER"] = "SMS-Anbieter ist nicht registriert oder wurde gelöscht.";
$MESS["CRM_SSMSA_REST_SESSION_ERROR"] = "REST-Sitzung gesperrt";
$MESS["CRM_SSMSA_PAYMENT_REQUIRED"] = "App wurde nicht bezahlt";
?>