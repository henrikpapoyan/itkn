<?
$MESS["BPCLSLA_PD_LEAD"] = "Lead-ID";
$MESS["BPCLSLA_PD_STATUS"] = "Status";
$MESS["BPCLSLA_PD_STATUS_DESCR"] = "Wählen Sie einen oder mehrere Status aus, in denen gewartet werden soll. Der Workflow wird warten, bis der Lead in den gewählten oder finalen Status verschoben wird.";
?>