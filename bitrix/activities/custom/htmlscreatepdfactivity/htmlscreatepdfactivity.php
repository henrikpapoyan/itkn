<?//v8.4.2 05.07.2016
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
//define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/bitrix/tmp/DocDesigner.txt");
class CBPHtmlsCreatePdfActivity
	extends CBPActivity
{
	public function __construct($name)
	{
		parent::__construct($name);
		$this->arProperties = array("Title" => "", "PDFTemplate" => "", "PDFNumerator" => "",
		 "CreateCopy" => "", "DocumentSection" => "", "CreateInvoice" => "", 'DocNumber' => '',
          'DocDate' => '', 'FontSize' => 10, 'FileID'=>'', 'MIME'=>'PDF', 'DocumentDisk' => '',
          'OriginalID' => '', 'diskId' => '', 'link2disk' => '', 'link2download' => '',
          'diskID'=>'', 'diskOriginalID' => '', 'url2disk' => '', 'url2download' => '');
	}

	public function Execute()
	{
		if(CModule::IncludeModuleEx("htmls.docdesigner") < 3){
			$rootActivity = $this->GetRootActivity();

            if(!$rootActivity->DocDesignerCompany){
                $rootActivity->DocDesignerCompany = $rootActivity->GetVariable('DocDesignerCompany');
            }
            if(!$rootActivity->DocDesignerNumberPrefix){
                $rootActivity->DocDesignerNumberPrefix = $rootActivity->GetVariable('DocDesignerNumberPrefix');
            }
            if(!$rootActivity->DocDesignerVATRate){
                $rootActivity->DocDesignerVATRate = $rootActivity->GetVariable('DocDesignerVATRate');
            }
            $Numerator = $this->PDFNumerator;
        	if(empty($Numerator) and $Numerator != 0){
        		$this->PDFNumerator = COption::GetOptionInt("htmls.docdesigner", 'DOCDESIGNER_NUM_INVOICE_ID');
        	}
        	/*if(!isset($rootActivity->arProperties["PDFNumerator"])){
				$rootActivity->arProperties["PDFNumerator"] = $this->PDFNumerator;
        	}*/

			$arProperties = $rootActivity->arProperties;
            if(!$arProperties['DocDesignerCompany']){
                $arProperties['DocDesignerCompany'] = $rootActivity->GetVariable('DocDesignerCompany');
            }
            if(!$arProperties['DocDesignerNumberPrefix']){
                $arProperties['DocDesignerNumberPrefix'] = $rootActivity->GetVariable('DocDesignerNumberPrefix');
            }
            if(!$arProperties['DocDesignerVATRate']){
                $arProperties['DocDesignerVATRate'] = $rootActivity->GetVariable('DocDesignerVATRate');
            }
            if(!$arProperties['DocDesignerSubject']){
                $arProperties['DocDesignerSubject'] = $rootActivity->GetVariable('DocDesignerSubject');
            }
            if(!$arProperties['DocDesignerEmail']){
                $arProperties['DocDesignerEmail'] = $rootActivity->GetVariable('DocDesignerEmail');
            }
            if(!$arProperties['DocDesignerMessage']){
                $arProperties['DocDesignerMessage'] = $rootActivity->GetVariable('DocDesignerMessage');
            }
            $arPropertiesTypes = $rootActivity->arPropertiesTypes;
			$workflowId = $this->workflow->GetInstanceId();
			if($this->DocumentSection > 0){
				$arProperties['DocumentSection'] = $this->DocumentSection;
			}
			//$arResult = CDocDesignerBPActivities::CreatePdfDocx($rootActivity, $arProperties, $arPropertiesTypes, $workflowId, $this);//->PDFTemplate, $this->PDFNumerator, $this->CreateCopy, $this->FontSize);
            $DocDes = new CDocDesignerBPActivities();
			$arResult = $DocDes->CreatePdfDocx($rootActivity, $arProperties, $arPropertiesTypes, $workflowId, $this);//->PDFTemplate, $this->PDFNumerator, $this->CreateCopy, $this->FontSize);
            //AddMessage2Log(print_r($arResult, 1));
            $this->DocNumber = $arResult['DocNumber'];
            $this->DocDate = $arResult['DocDate'];
            $this->FileID = $arResult['FileID'];
            $this->OriginalID = $arResult['OriginalID'];
            //$this->diskID = $arResult['diskID'];
            //$this->diskOriginalID = $arResult['diskOriginalID'];
            foreach($arResult['links'] as $link){
              $this->link2disk .= '[url=' . $link['link2disk'] . ']' . $link['NAME'] . '[/url] | ';
              $this->url2disk .= $link['link2disk'];
              $this->link2download .= '[url=' . $link['link2download'] . ']' . $link['NAME'] . '[/url] | ';
              $this->url2download .= $link['link2download'];
              if($link['arFile']['FILE_ID'] == $this->FileID){
                $this->diskID = $link['arFile']['REAL_OBJECT_ID'];
              }
              elseif($link['arFile']['FILE_ID'] == $this->OriginalID){
                $this->diskOriginalID = $link['arFile']['REAL_OBJECT_ID'];
              }
            }

			if($this->CreateInvoice == 'Y'){
				CDocDesignerCRM::CreateInvoice($rootActivity, $arProperties);
			}
		}
		return CBPActivityExecutionStatus::Closed;
	}

    function encodeUrn($uri)
{
   //global $APPLICATION;

   $result = '';
   $parts = preg_split("#(://|:\\d+/|/|\\?|=|&)#", $uri, -1, PREG_SPLIT_DELIM_CAPTURE);

   foreach($parts as $i => $part)
   {
      $result .= $part;//($i % 2)
         //? $part
         //: rawurlencode($APPLICATION->convertCharset($part, LANG_CHARSET, 'UTF-8'));
   }

   return $result;
}

	public static function ValidateProperties($arTestProperties = array(), CBPWorkflowTemplateUser $user = null)
	{
		$arErrors = array();

		/*if ($user == null || !$user->IsAdmin())
		{
			$arErrors[] = array(
				"code" => "perm",
				"message" => GetMessage("BPCA_NO_PERMS"),
			);
		}*/

		if (strlen($arTestProperties["PDFTemplate"]) <= 0)
		{
			$arErrors[] = array(
				"code" => "emptyTemplate",
				"message" => GetMessage("BPPDFA_EMPTY_TEMPLATE"),
			);
		}
		return array_merge($arErrors, parent::ValidateProperties($arTestProperties, $user));
	}

	public static function GetPropertiesDialog($documentType, $activityName, $arWorkflowTemplate, $arWorkflowParameters, $arWorkflowVariables, $arCurrentValues = null, $formName = "")
	{
		$runtime = CBPRuntime::GetRuntime();

		if (!is_array($arWorkflowParameters))
			$arWorkflowParameters = array();
		if (!is_array($arWorkflowVariables))
			$arWorkflowVariables = array();

		if (!is_array($arCurrentValues))
		{
			$arCurrentValues = array("template_select" => "", "pdf_numerator" => "",
				"create_copy" => "", "DocumentSection" => "", "CreateInvoice" => "", 'FontSize' => '', 'MIME' => '', 'DocumentDisk' => '');

			$arCurrentActivity = &CBPWorkflowTemplateLoader::FindActivityByName($arWorkflowTemplate, $activityName);
			if (is_array($arCurrentActivity["Properties"])){
				$arCurrentValues["template_select"] = $arCurrentActivity["Properties"]["PDFTemplate"];
				$arCurrentValues["pdf_numerator"] = $arCurrentActivity["Properties"]["PDFNumerator"];
				$arCurrentValues["create_copy"] = $arCurrentActivity["Properties"]["CreateCopy"];
				$arCurrentValues["DocumentSection"] = $arCurrentActivity["Properties"]["DocumentSection"];
				$arCurrentValues["DocumentDisk"] = $arCurrentActivity["Properties"]["DocumentDisk"];
				$arCurrentValues["create_invoice"] = $arCurrentActivity["Properties"]["CreateInvoice"];
				$arCurrentValues["fontSize_select"] = $arCurrentActivity["Properties"]["FontSize"];
				$arCurrentValues["mime_select"] = $arCurrentActivity["Properties"]["MIME"];
			}
		}

		return $runtime->ExecuteResourceFile(
			__FILE__,
			"properties_dialog.php",
			array(
				"arCurrentValues" => $arCurrentValues,
				"formName" => $formName,
				"documentType" => $documentType,
			)
		);
	}

	public static function GetPropertiesDialogValues($documentType, $activityName, &$arWorkflowTemplate, &$arWorkflowParameters, &$arWorkflowVariables, $arCurrentValues, &$arErrors)
	{
		$arErrors = array();

		$runtime = CBPRuntime::GetRuntime();

		$arProperties = array("PDFTemplate" => $arCurrentValues["template_select"], "PDFNumerator" => $arCurrentValues["pdf_numerator"],
			"CreateCopy" => $arCurrentValues['create_copy'], "DocumentSection" => $arCurrentValues["DocumentSection"][0],
			"CreateInvoice" => $arCurrentValues['create_invoice'], "FontSize" => $arCurrentValues['fontSize_select'],
            "MIME" => $arCurrentValues['mime_select'], "DocumentDisk" => $arCurrentValues["DocumentDisk"][0]);

		$arErrors = self::ValidateProperties($arProperties, new CBPWorkflowTemplateUser(CBPWorkflowTemplateUser::CurrentUser));
		if (count($arErrors) > 0)
			return false;

		$arCurrentActivity = &CBPWorkflowTemplateLoader::FindActivityByName($arWorkflowTemplate, $activityName);
		$arCurrentActivity["Properties"] = $arProperties;

		return true;
	}
}
?>