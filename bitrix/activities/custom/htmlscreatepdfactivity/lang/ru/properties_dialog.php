<?
$MESS['BPPDFA_TEMPLATE_SELECT'] = "Укажите шаблон";
$MESS["BPPDFA_NO_TEMPLATES"] = "Нет сохраненных шаблонов";
$MESS["BPPDFA_PORTRAIT"] = "Портрет";
$MESS["BPPDFA_LANDSCAPE"] = "Ландшафт";
$MESS["BPPDFA_PAGE_ORIENTATION"] = "Ориентация документа";
$MESS["BPPDFA_COMMON_TEMPLATES"] = "Общие шаблоны";
$MESS["BPPDFA_NO_NUMERATORS"] = "Нет нумераторов";
$MESS["BPPDFA_NUMERATOR_SELECT"] = "Выбор нумератора";
$MESS['BPPDFA_CREATE_COPY'] = 'Создать оргинал без факсимиле';

$MESS['BPPDFA_DOCUMENT_SECTION'] = 'Поместить документ в папку';

$MESS['BPPDFA_CREATE_INVOICE'] = 'Создать счет CRM';
$MESS['BPPDFA_FONTSIZE_SELECT'] = 'Размер шрифта';
$MESS['BPPDFA_MIME'] = 'Формат документа';

$MESS['BPDSA_DISK'] = 'Диск: ';
$MESS['BPDSA_DOCUMENT_DISK'] = 'Поместить документ в диск: ';
?>