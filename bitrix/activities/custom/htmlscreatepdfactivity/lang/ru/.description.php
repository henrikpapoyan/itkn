<?
$MESS['BPPDFA_DESCR_DESCR'] = "Конструктор документов: Создание PDF / DOCX -документа";
$MESS['BPPDFA_DESCR_NAME'] = "КД: Создать PDF / DOCX";
$MESS['BPPDFA_DocNumber_RU'] = "PDF/DOCX: номер документа";
$MESS['BPPDFA_DocDate_RU'] = "PDF/DOCX: дата документа";
$MESS['BPPDFA_FileID_RU'] = 'PDF/DOCX: ID файла';
$MESS['BPPDFA_OriginalID_RU'] = 'PDF оригинал: ID файла';
$MESS['BPPDFA_link2disk_RU'] = 'PDF/DOCX: Ссылка на диск';
$MESS['BPPDFA_url2disk_RU'] = 'PDF/DOCX: URL на диск';
$MESS['BPPDFA_link2download_RU'] = 'PDF/DOCX: Ссылка для скачивания';
$MESS['BPPDFA_url2download_RU'] = 'PDF/DOCX: URL для скачивания';
$MESS['BPPDFA_DiskID_RU'] = 'PDF/DOCX: ID диска';
$MESS['BPPDFA_DiskOriginalID_RU'] = 'PDF оригинал: ID диска';
?>