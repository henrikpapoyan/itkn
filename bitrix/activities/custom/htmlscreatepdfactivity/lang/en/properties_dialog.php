<?
$MESS['BPPDFA_TEMPLATE_SELECT'] = "Select template";
$MESS["BPPDFA_NO_TEMPLATES"] = "No templates";
$MESS["BPPDFA_PORTRAIT"] = "Portrait";
$MESS["BPPDFA_LANDSCAPE"] = "Landscape";
$MESS["BPPDFA_PAGE_ORIENTATION"] = "Page orientation";
$MESS["BPPDFA_COMMON_TEMPLATES"] = "Common templates";
$MESS["BPPDFA_NO_NUMERATORS"] = "No numerators";
$MESS["BPPDFA_NUMERATOR_SELECT"] = "Select numerator";
$MESS['BPPDFA_CREATE_COPY'] = 'Save original (no faksimile)';

$MESS['BPPDFA_DOCUMENT_SECTION'] = 'Put doc to folder';

$MESS['BPPDFA_CREATE_INVOICE'] = 'Create CRM-invoice';
$MESS['BPPDFA_FONTSIZE_SELECT'] = 'Font size';
$MESS['BPPDFA_MIME'] = 'mime type';

$MESS['BPDSA_DISK'] = 'Disk: ';
$MESS['BPDSA_DOCUMENT_DISK'] = 'Select disk';
?>