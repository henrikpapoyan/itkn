<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<tr>
	<td align="right" width="40%"><?= GetMessage("BPDOC2EMAILA_SUBJECT") ?>:</td>
	<td width="60%">
		<input type="text" size="50" name="doc2mail_subject" id="id_doc2mail_subject" value="<?=$arCurrentValues['doc2mail_subject']?>"/>
	</td>
</tr>
<tr>
	<td align="right" width="40%"><?= GetMessage("BPDOC2EMAILA_MESSAGE") ?>:</td>
	<td width="60%">
		<textarea name="doc2mail_message" id="id_doc2mail_message" rows="7" cols="37"><?=$arCurrentValues['doc2mail_message']?></textarea>
	</td>
</tr>
<script>
$(document).ready(function () {
    $('.bx-core-adm-dialog-head-block').append('<img src="/bitrix/images/htmls.docdesigner/help.png" style="float: right;" onclick="window.open(\'https://www.htmls.pw/learning/course/index.php?COURSE_ID=2&CHAPTER_ID=081&LESSON_PATH=60.77.81\');" />');
});
</script>
