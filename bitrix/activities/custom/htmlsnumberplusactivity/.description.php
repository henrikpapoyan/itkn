<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arActivityDescription = array(
	"NAME" => GetMessage("BPNP1A_DESCR_NAME"),
	"DESCRIPTION" => GetMessage("BPNP1A_DESCR_DESCR"),
	"TYPE" => "activity",
	"CLASS" => "HtmlsNumberPlusActivity",
	"JSCLASS" => "BizProcActivity",
	"CATEGORY" => array(
		"ID" => "other",
	),
);
?>
