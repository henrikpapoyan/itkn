<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if(CModule::IncludeModuleEx("htmls.docdesigner") < 3){
  $discConverted = \Bitrix\Main\Config\Option::get('disk', 'successfully_converted', false);
  if($discConverted == 'Y'){
    CModule::IncludeModule('disk');
    CModule::IncludeModule('socialnetwork');
    $cDoc = new CDocDesignerContracts;
    $diskTree = $cDoc->getDisks();

    $sections = '<select  id="DocumentDisk" name="DocumentDisk" onchange="getSections(this.value);">';
    $sections .= '<option value="0">---</option>';
    foreach($diskTree as $id => $aDisk){
      if($aDisk['tree']){//deprecated
        $sections .= '<optgroup label="' . GetMessage('BPDSA_DISK') . $aDisk['name'] . '">';
            $sections .= '<option value="' . $aDisk['root'] . '">.' . $aDisk['name'] . '</option>';
            $sections .= $cDoc->getSelect($aDisk['tree'], '', $arCurrentValues['DocumentSection']);
        $sections .= '</optgroup>';
      }
      else{
        //$sections .= '<option value="' . $aDisk['root'] . '">' . GetMessage('BPDSA_DISK') . $aDisk['name'] . '</option>';
        if($arCurrentValues['DocumentDisk'] == $aDisk['root'])
            $sections .= '<option selected value="' . $aDisk['root'] . '">' . GetMessage('BPDSA_DISK') . $aDisk['name'] . '</option>';
        else
            $sections .= '<option value="' . $aDisk['root'] . '">' . GetMessage('BPDSA_DISK') . $aDisk['name'] . '</option>';
      }
    }
  }//if($discConverted == 'Y')
}
?>
<tr>
	<td align="right" width="40%"><?= GetMessage("BPDSA_DOCUMENT_DISK") ?>:</td>
	<td width="60%">
		<?=$sections?>
	</td>
</tr>

<tr>
	<td align="right" width="40%"><?= GetMessage("BPDOCXA_LINK") ?>:</td>
	<td width="60%">
		<?
		echo '<div id="diskSections"></div>';//$sections;
		?>
	</td>
</tr>

<script>
function getTree(data, mov){
  mov = mov + '--';
  select = '';
  for(var sid in data){
      if(currentSection == sid)
          select += '<option value="'+sid+'" selected>'+mov+'>'+data[sid]['name']+'</option>';
      else
          select += '<option value="'+sid+'">'+mov+'>'+data[sid]['name']+'</option>';

      if(tree = data[sid]['tree']){
        select += getTree(tree, mov);
      }
  }
  return select;

}
function getSections(sid){
    currentSection = '<?=$arCurrentValues["DocumentSection"]?>';
    arParam = {sectionId: sid};
    $.post("/bitrix/admin/docdesigner_ajax.php", arParam, function(data){
        select = '<select name="DocumentSection">';
        for(var sid in data){
            if(currentSection == sid)
                select += '<option value="'+sid+'" selected>'+data[sid]['name']+'</option>';
            else
                select += '<option value="'+sid+'">'+data[sid]['name']+'</option>';

            if(tree = data[sid]['tree']){
              select += getTree(tree, '');
            }
        }
        select += '</select>';
        $('#diskSections').html(select);
    }, "json");
}

$(document).ready(function () {
    did = $('#DocumentDisk').val();
    if(did > 0){
        getSections(did);
    }
    $('.bx-core-adm-dialog-head-block').append('<img src="/bitrix/images/htmls.docdesigner/help.png" style="float: right;" onclick="window.open(\'https://www.htmls.pw/learning/course/index.php?COURSE_ID=2&LESSON_ID=82&LESSON_PATH=60.77.82\');" />');
});
</script>