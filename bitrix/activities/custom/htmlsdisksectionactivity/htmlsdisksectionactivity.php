<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
//define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/bitrix/tmp/DocDesigner.txt");
class CBPHtmlsDiskSectionActivity
	extends CBPActivity
{
	public function __construct($name)
	{
		parent::__construct($name);
		$this->arProperties = array("Title" => "", 'DocumentSection'=> '', 'DocumentDisk' => '');
	}

	public function Execute(){
	  global $USER, $DB;
      $discConverted = \Bitrix\Main\Config\Option::get('disk', 'successfully_converted', false);
      if($discConverted == 'Y'){
        $rootActivity = $this->GetRootActivity();
        $rootActivity->SetVariable('DocDesignerDocumentSection', $this->DocumentSection);
    	/*$this->WriteToTrackingService(
    	"The new section is " . $this->DocumentSection,
    	$USER->GetID());*/
      }
	  return CBPActivityExecutionStatus::Closed;
	}

	/*public static function ValidateProperties($arTestProperties = array(), CBPWorkflowTemplateUser $user = null)
	{
	}*/

	public static function GetPropertiesDialog($documentType, $activityName, $arWorkflowTemplate, $arWorkflowParameters, $arWorkflowVariables, $arCurrentValues = null, $formName = "")
	{
	  $runtime = CBPRuntime::GetRuntime();

		if (!is_array($arWorkflowParameters))
			$arWorkflowParameters = array();
		if (!is_array($arWorkflowVariables))
			$arWorkflowVariables = array();

		if (!is_array($arCurrentValues))
		{
			$arCurrentValues = array("DocumentSection" => "", 'DocumentDisk' => '');

			$arCurrentActivity = &CBPWorkflowTemplateLoader::FindActivityByName($arWorkflowTemplate, $activityName);
			if (is_array($arCurrentActivity["Properties"])){
				$arCurrentValues["DocumentSection"] = $arCurrentActivity["Properties"]["DocumentSection"];
				$arCurrentValues["DocumentDisk"] = $arCurrentActivity["Properties"]["DocumentDisk"];
			}
		}

		return $runtime->ExecuteResourceFile(
			__FILE__,
			"properties_dialog.php",
			array(
				"arCurrentValues" => $arCurrentValues,
				"formName" => $formName,
				"documentType" => $documentType,
			)
		);
	}

	public static function GetPropertiesDialogValues($documentType, $activityName, &$arWorkflowTemplate, &$arWorkflowParameters, &$arWorkflowVariables, $arCurrentValues, &$arErrors)
	{
		$arErrors = array();

		$runtime = CBPRuntime::GetRuntime();

		$arProperties = array("DocumentSection" => $arCurrentValues["DocumentSection"], "DocumentDisk" => $arCurrentValues["DocumentDisk"]);

		$arErrors = self::ValidateProperties($arProperties, new CBPWorkflowTemplateUser(CBPWorkflowTemplateUser::CurrentUser));
		if (count($arErrors) > 0)
			return false;

		$arCurrentActivity = &CBPWorkflowTemplateLoader::FindActivityByName($arWorkflowTemplate, $activityName);
		$arCurrentActivity["Properties"] = $arProperties;

		return true;
	}
}
?>