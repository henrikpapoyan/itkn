<?
$MESS ['BPDOCXA_DESCR_DESCR'] = "Конструктор документов: Создание документа в формате DOCX";
$MESS ['BPDOCXA_DESCR_NAME'] = "КД: Создать DOCX";
$MESS ['BPDOCXA_DocNumber_RU'] = "DOCX: номер документа";
$MESS ['BPDOCXA_DocDate_RU'] = "DOCX: дата документа";
$MESS['BPDOCXA_FileID_RU'] = 'DOCX: ID файла';
$MESS['BPDOCXA_DiskID_RU'] = 'DOCX: ID диска';
$MESS['BPDOCXA_link2disk_RU'] = 'DOCX: Ссылка на диск';
$MESS['BPDOCXA_url2disk_RU'] = 'DOCX: URL на диск';
$MESS['BPDOCXA_link2download_RU'] = 'DOCX: Ссылка для скачивания';
$MESS['BPDOCXA_url2download_RU'] = 'DOCX: URL для скачивания';
?>