<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
//AddMessage2Log(print_r($_REQUEST, true));
if(CModule::IncludeModuleEx("htmls.docdesigner") < 3){
    CModule::IncludeModule('disk');
    $cDoc = new CDocDesignerContracts;
    $arSec = $cDoc->getSections(htmlspecialchars($_REQUEST['sectionId']));
    //AddMessage2Log(json_encode($arSec));
    echo json_encode($arSec);
}
?>