<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arActivityDescription = array(
	"NAME" => GetMessage("BPDOCXA_DESCR_NAME"),
	"DESCRIPTION" => GetMessage("BPDOCXA_DESCR_DESCR"),
	"TYPE" => "activity",
	"CLASS" => "HtmlsCreateDocxActivity",
	"JSCLASS" => "BizProcActivity",
	"CATEGORY" => array(
		"ID" => "other",
	),
    "RETURN" => array(
		"DocNumber" => array(
			"NAME" => GetMessage("BPDOCXA_DocNumber_RU"),
			"TYPE" => "string",
		),
        "DocDate" => array(
			"NAME" => GetMessage("BPDOCXA_DocDate_RU"),
			"TYPE" => "string",
		),
        "FileID" => array(
			"NAME" => GetMessage("BPDOCXA_FileID_RU"),
			"TYPE" => "string",
		),
        "diskID" => array(
			"NAME" => GetMessage("BPDOCXA_DiskID_RU"),
			"TYPE" => "string",
		),
        "link2disk" => array(
			"NAME" => GetMessage("BPDOCXA_link2disk_RU"),
			"TYPE" => "array",
		),
        "link2download" => array(
			"NAME" => GetMessage("BPDOCXA_link2download_RU"),
			"TYPE" => "array",
		),
        "url2disk" => array(
			"NAME" => GetMessage("BPDOCXA_url2disk_RU"),
			"TYPE" => "array",
		),
        "url2download" => array(
			"NAME" => GetMessage("BPDOCXA_url2download_RU"),
			"TYPE" => "array",
		)

    )
);
?>
