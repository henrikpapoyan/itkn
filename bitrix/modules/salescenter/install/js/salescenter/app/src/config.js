export const config = Object.freeze({
	databaseConfig: {
		name: 'salescenter.app',
	},
	templateName: 'bx-salescenter-app',
	templateAddPaymentName: 'bx-salescenter-app-add-payment',
	templateAddPaymentProductName: 'bx-salescenter-app-add-payment-product',
	templateAddPaymentBySms: 'bx-salescenter-app-add-payment-by-sms',
	templateAddPaymentBySmsItem: 'bx-salescenter-app-add-payment-by-sms-item',
	moduleId: 'salescenter',
});