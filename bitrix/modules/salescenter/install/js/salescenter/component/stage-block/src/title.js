const Title = {
	template: `
		<div class="salescenter-app-payment-by-sms-item-title">
			<div class="salescenter-app-payment-by-sms-item-title-text">
				<slot></slot>
			</div>
			<slot name="item-hint"></slot>
		</div>
	`
};

export {
	Title
}