<?
$MESS["SOD_SUB_ORDER_TITLE"] = "Заказ №#ACCOUNT_NUMBER# от #DATE_ORDER_CREATE#";
$MESS["SOD_SUB_ORDER_TITLE_SHORT"] = "Заказ №#ACCOUNT_NUMBER#";
$MESS["SOD_TPL_SUMOF"] = "на сумму";
$MESS["SOD_TOTAL_WEIGHT"] = "Общий вес";
$MESS["SOD_COMMON_SUM_NEW"] = "Стоимость";
$MESS["SOD_COMMON_DISCOUNT"] = "Экономия";
$MESS["SOD_DELIVERY"] = "Доставка";
$MESS["SOD_FREE"] = "Бесплатно";
$MESS["SOD_TAX"] = "НДС";
$MESS["SOD_SUMMARY"] = "Итого";