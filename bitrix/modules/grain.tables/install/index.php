<?
global $MESS;
$strPath2Lang = str_replace("\\", "/", __FILE__);
$strPath2Lang = substr($strPath2Lang, 0, strlen($strPath2Lang)-strlen("/install/index.php"));
include(GetLangFileName($strPath2Lang."/lang/", "/install/index.php"));

Class grain_tables extends CModule
{
	var $MODULE_ID = "grain.tables";
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;
	var $PARTNER_NAME;
	var $PARTNER_URI;

	function grain_tables()
	{

		$arModuleVersion = array();

		$path = str_replace("\\", "/", __FILE__);
		$path = substr($path, 0, strlen($path) - strlen("/index.php"));
		include($path."/version.php");


		$this->MODULE_VERSION = $arModuleVersion["VERSION"];
		$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		$this->MODULE_NAME = GetMessage("GRAIN_TABLES_MODULE_NAME");
		$this->MODULE_DESCRIPTION = GetMessage("GRAIN_TABLES_MODULE_DESC");
		$this->PARTNER_URI = GetMessage("GRAIN_TABLES_PARTNER_URL");
		$this->PARTNER_NAME = GetMessage("GRAIN_TABLES_PARTNER_NAME");
	}

	function DoInstall()
	{

		/*patchinstallmutatormark1*/

		$this->InstallFiles();

		RegisterModule("grain.tables");

		RegisterModuleDependences("iblock", "OnIBlockPropertyBuildList", "grain.tables", "GIBlockPropertyTable", "GetUserTypeDescription");
		RegisterModuleDependences("main", "OnUserTypeBuildList", "grain.tables", "GUserPropertyTable", "GetUserTypeDescription");

		/*patchinstallmutatormark2*/

	}

	function DoUninstall()
	{

		global $APPLICATION;

		UnRegisterModuleDependences("iblock", "OnIBlockPropertyBuildList", "grain.tables", "GIBlockPropertyTable", "GetUserTypeDescription");
		UnRegisterModuleDependences("main", "OnUserTypeBuildList", "grain.tables", "GUserPropertyTable", "GetUserTypeDescription");

		UnRegisterModule("grain.tables");

		$this->UnInstallFiles();

		$GLOBALS["errors"] = $this->errors;

		COption::RemoveOption("grain.tables");

		$APPLICATION->IncludeAdminFile(GetMessage("GRAIN_TABLES_INSTALL_TITLE"), $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/grain.tables/install/unstep2.php");

	}


	function InstallFiles()
	{

		//CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/grain.tables/install/admin", $_SERVER["DOCUMENT_ROOT"]."/bitrix/admin", true);
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/grain.tables/install/images",  $_SERVER["DOCUMENT_ROOT"]."/bitrix/images/grain.tables", true, true);
		//CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/grain.tables/install/themes", $_SERVER["DOCUMENT_ROOT"]."/bitrix/themes", true, true);
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/grain.tables/install/components", $_SERVER["DOCUMENT_ROOT"]."/bitrix/components", true, true);
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/grain.tables/install/templates", $_SERVER["DOCUMENT_ROOT"]."/bitrix/templates/.default", true, true);

		return true;
	}

	function UnInstallFiles()
	{

		//DeleteDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/grain.tables/install/admin", $_SERVER["DOCUMENT_ROOT"]."/bitrix/admin");

		//DeleteDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/grain.tables/install/themes/.default/", $_SERVER["DOCUMENT_ROOT"]."/bitrix/themes/.default");//css
		//DeleteDirFilesEx("/bitrix/themes/.default/icons/grain.tables/");//icons
		//DeleteDirFilesEx("/bitrix/themes/.default/start_menu/grain.tables/");//start menu icons
		DeleteDirFilesEx("/bitrix/images/grain.tables/");//images

		DeleteDirFilesEx("/bitrix/components/grain/table.edit/");//components
		DeleteDirFilesEx("/bitrix/components/grain/table.view/");//components
		DeleteDirFilesEx("/bitrix/components/grain/table.filter/");//components

		DeleteDirFilesEx("/bitrix/templates/.default/components/bitrix/system.field.edit/gtable/");//templates
		DeleteDirFilesEx("/bitrix/templates/.default/components/bitrix/system.field.view/gtable/");//templates

		return true;
	}


}

?>