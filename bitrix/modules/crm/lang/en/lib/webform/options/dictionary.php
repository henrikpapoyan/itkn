<?php
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_ACTION_HIDE"] = "Hide";
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_ACTION_SHOW"] = "Show";
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_EVENT_CHANGE"] = "Changes";
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_OP_ANY"] = "Any";
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_OP_CONTAIN"] = "Contains";
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_OP_EMPTY"] = "Empty";
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_OP_EQUAL"] = "Equal to";
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_OP_GREATER"] = "More than";
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_OP_GREATEROREQUAL"] = "More than or equal to";
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_OP_LESS"] = "Less than";
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_OP_LESSOREQUAL"] = "Less than or equal to";
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_OP_NOTCONTAIN"] = "Doesn't contain";
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_OP_NOTEQUAL"] = "Not equal to";
$MESS["CRM_WEBFORM_OPTIONS_DICT_PERC_FIELD_COMPANY_NAME"] = "Company name";
$MESS["CRM_WEBFORM_OPTIONS_DICT_PERC_FIELD_EMAIL"] = "Email";
$MESS["CRM_WEBFORM_OPTIONS_DICT_PERC_FIELD_LASTNAME"] = "Last name";
$MESS["CRM_WEBFORM_OPTIONS_DICT_PERC_FIELD_NAME"] = "First name";
$MESS["CRM_WEBFORM_OPTIONS_DICT_PERC_FIELD_PHONE"] = "Phone";
$MESS["CRM_WEBFORM_OPTIONS_DICT_PERC_FIELD_SECOND_NAME"] = "Second name";
$MESS["CRM_WEBFORM_OPTIONS_DICT_VIEW_POS_CENTER"] = "Center";
$MESS["CRM_WEBFORM_OPTIONS_DICT_VIEW_POS_LEFT"] = "Left";
$MESS["CRM_WEBFORM_OPTIONS_DICT_VIEW_POS_RIGHT"] = "Right";
$MESS["CRM_WEBFORM_OPTIONS_DICT_VIEW_TYPE_PANEL"] = "Sidebar";
$MESS["CRM_WEBFORM_OPTIONS_DICT_VIEW_TYPE_POPUP"] = "Popup";
$MESS["CRM_WEBFORM_OPTIONS_DICT_VIEW_VERT_BOTTOM"] = "Bottom";
$MESS["CRM_WEBFORM_OPTIONS_DICT_VIEW_VERT_TOP"] = "Top";
