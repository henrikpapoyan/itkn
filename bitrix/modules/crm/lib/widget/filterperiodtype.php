<?php
namespace Bitrix\Crm\Widget;

use Bitrix\Main;

class FilterPeriodType
{
	const UNDEFINED = '';
	const YEAR = 'Y';
	const QUARTER = 'Q';
	const MONTH = 'M';
	const CURRENT_MONTH = 'M0';
	const CURRENT_QUARTER = 'Q0';
	const CURRENT_DAY = 'D0';
	const LAST_DAYS_90 = 'D90';
	const LAST_DAYS_60 = 'D60';
	const LAST_DAYS_30 = 'D30';
	const LAST_DAYS_7 = 'D7';
	const BEFORE = 'B';
	const YESTERDAY = 'YS';
	const CURRENT_WEEK = 'W0';
    const LAST_N_DAYS = 'LN';
    const SET_DATE = 'SD';
    const LAST_WEEK = 'WL';
    const LAST_MONTH = 'ML';
    const PERIOD_DATE = 'PD';



	private static $messagesLoaded = false;
	/**
	* @return boolean
	*/
	public static function isDefined($typeID)
	{
		return $typeID === self::UNDEFINED
            || $typeID === self::YESTERDAY
            || $typeID === self::CURRENT_DAY
            || $typeID === self::CURRENT_WEEK
			|| $typeID === self::CURRENT_MONTH
			|| $typeID === self::CURRENT_QUARTER
			|| $typeID === self::LAST_DAYS_7
            || $typeID === self::LAST_DAYS_30
            || $typeID === self::LAST_DAYS_60
            || $typeID === self::LAST_DAYS_90
            || $typeID === self::LAST_N_DAYS
			|| $typeID === self::BEFORE
            || $typeID === self::SET_DATE
            || $typeID === self::LAST_WEEK
            || $typeID === self::LAST_MONTH
            || $typeID === self::YEAR
            || $typeID === self::QUARTER
            || $typeID === self::MONTH
            || $typeID === self::PERIOD_DATE;
	}
	/**
	* @return array Array of strings
	*/
	public static function getAllDescriptions()
	{
		self::includeModuleFile();
		return array(
			self::UNDEFINED  => GetMessage('CRM_FILTER_PERIOD_TYPE_UNDEFINED'),
            self::YESTERDAY => GetMessage('CRM_FILTER_PERIOD_TYPE_YESTERDAY'),
            self::CURRENT_DAY => GetMessage('CRM_FILTER_PERIOD_TYPE_CURRENT_DAY'),
            self::CURRENT_WEEK => GetMessage('CRM_FILTER_PERIOD_TYPE_CURRENT_WEEK'),
			self::CURRENT_MONTH => GetMessage('CRM_FILTER_PERIOD_TYPE_CURRENT_MONTH'),
			self::CURRENT_QUARTER => GetMessage('CRM_FILTER_PERIOD_TYPE_CURRENT_QUARTER'),
			self::LAST_DAYS_7 => GetMessage('CRM_FILTER_PERIOD_TYPE_LAST_DAYS_7'),
            self::LAST_DAYS_30 => GetMessage('CRM_FILTER_PERIOD_TYPE_LAST_DAYS_30'),
            self::LAST_DAYS_60 => GetMessage('CRM_FILTER_PERIOD_TYPE_LAST_DAYS_60'),
            self::LAST_DAYS_90 => GetMessage('CRM_FILTER_PERIOD_TYPE_LAST_DAYS_90'),
			self::LAST_N_DAYS => GetMessage('CRM_FILTER_PERIOD_TYPE_LAST_N_DAYS'),
            self::MONTH => GetMessage('CRM_FILTER_PERIOD_TYPE_MONTH'),
            self::QUARTER => GetMessage('CRM_FILTER_PERIOD_TYPE_QUARTER'),
            self::YEAR => GetMessage('CRM_FILTER_PERIOD_TYPE_YEAR'),
            self::SET_DATE => GetMessage('CRM_FILTER_PERIOD_TYPE_SET_DATE'),
            self::LAST_WEEK => GetMessage('CRM_FILTER_PERIOD_TYPE_LAST_WEEK'),
            self::LAST_MONTH => GetMessage('CRM_FILTER_PERIOD_TYPE_LAST_MONTH'),
			self::PERIOD_DATE => GetMessage('CRM_FILTER_PERIOD_TYPE_PERIOD_DATE'),
			self::BEFORE => GetMessage('CRM_FILTER_PERIOD_TYPE_BEFORE'),
		);
	}
	/**
	* @return string
	*/
	public static function getDescription($typeID)
	{
		$descriptions = self::getAllDescriptions();
		return isset($descriptions[$typeID]) ? $descriptions[$typeID] : '';
	}
	/**
	 * Convert System Date Type to Widget Period.
	 * @param string $dateType Source Date Type (please see Bitrix\Main\UI\Filter\DateType).
	 * @return string
	 */
	public static function convertFromDateType($dateType)
	{
		$result = self::UNDEFINED;

        if($dateType === Main\UI\Filter\DateType::YESTERDAY)
        {
            $result = self::YESTERDAY;
        }
        else if($dateType === Main\UI\Filter\DateType::CURRENT_DAY)
        {
            $result = self::CURRENT_DAY;
        }
        else if($dateType === Main\UI\Filter\DateType::CURRENT_WEEK)
        {
            $result = self::CURRENT_WEEK;
        }
        else if($dateType === Main\UI\Filter\DateType::CURRENT_MONTH)
        {
            $result = self::CURRENT_MONTH;
        }
        else if($dateType === Main\UI\Filter\DateType::CURRENT_QUARTER)
        {
            $result = self::CURRENT_QUARTER;
        }
        else if($dateType === Main\UI\Filter\DateType::LAST_7_DAYS)
        {
            $result = self::LAST_DAYS_7;
        }
        else if($dateType === Main\UI\Filter\DateType::LAST_30_DAYS)
        {
            $result = self::LAST_DAYS_30;
        }
        else if($dateType === Main\UI\Filter\DateType::LAST_60_DAYS)
        {
            $result = self::LAST_DAYS_60;
        }
        else if($dateType === Main\UI\Filter\DateType::LAST_90_DAYS)
        {
            $result = self::LAST_DAYS_90;
		}
		else if($dateType === Main\UI\Filter\DateType::LAST_N_DAYS)
		{
		  $result = self::LAST_N_DAYS;
		}
        else if($dateType === Main\UI\Filter\DateType::MONTH)
        {
            $result = self::MONTH;
        }
		else if($dateType === Main\UI\Filter\DateType::QUARTER)
		{
			$result = self::QUARTER;
		}
        else if($dateType === Main\UI\Filter\DateType::YEAR)
        {
            $result = self::YEAR;
		}
		else if($dateType === Main\UI\Filter\DateType::SET_DATE)
        {
            $result = self::SET_DATE;
		}
        else if($dateType === Main\UI\Filter\DateType::LAST_WEEK)
        {
            $result = self::LAST_WEEK;
        }
        else if($dateType === Main\UI\Filter\DateType::LAST_MONTH)
        {
            $result = self::LAST_MONTH;
		}
        else if($dateType === Main\UI\Filter\DateType::PERIOD_DATE)
        {
            $result = self::PERIOD_DATE;
		}


		return $result;
	}
	/**
	 * Convert Widget Filter Period to System Date Type.
	 * @param string $period Source Widget Filter Period.
	 * @return string
	 */
	public static function convertToDateType($period)
	{
		$result = '';

        if($period === self::YESTERDAY)
        {
            $result = Main\UI\Filter\DateType::YESTERDAY;
        }
        else if($period === self::CURRENT_DAY)
        {
            $result = Main\UI\Filter\DateType::CURRENT_DAY;
        }
        else if($period === self::CURRENT_WEEK)
        {
            $result = Main\UI\Filter\DateType::CURRENT_WEEK;
        }
        else if($period === self::CURRENT_MONTH)
        {
            $result = Main\UI\Filter\DateType::CURRENT_MONTH;
        }
        else if($period === self::CURRENT_QUARTER)
        {
            $result = Main\UI\Filter\DateType::CURRENT_QUARTER;
        }
        else if($period === self::LAST_DAYS_7)
        {
            $result = Main\UI\Filter\DateType::LAST_7_DAYS;
        }
        else if($period === self::LAST_DAYS_30)
        {
            $result = Main\UI\Filter\DateType::LAST_30_DAYS;
        }
        else if($period === self::LAST_DAYS_60)
        {
            $result = Main\UI\Filter\DateType::LAST_60_DAYS;
        }
        else if($period === self::LAST_DAYS_90)
        {
            $result = Main\UI\Filter\DateType::LAST_90_DAYS;
        }
        else if($period === self::LAST_N_DAYS)
        {
            $result = Main\UI\Filter\DateType::LAST_N_DAYS;
        }
        else if($period === self::MONTH)
        {
            $result = Main\UI\Filter\DateType::MONTH;
        }
        else if($period === self::QUARTER)
        {
            $result = Main\UI\Filter\DateType::QUARTER;
        }
        else if($period === self::YEAR)
		{
			$result = Main\UI\Filter\DateType::YEAR;
		}
        else if($period === self::SET_DATE)
        {
            $result = Main\UI\Filter\DateType::SET_DATE;
        }
        else if($period === self::LAST_WEEK)
        {
            $result = Main\UI\Filter\DateType::LAST_WEEK;
        }
        else if($period === self::LAST_MONTH)
        {
            $result = Main\UI\Filter\DateType::LAST_MONTH;
        }
        else if($period === self::PERIOD_DATE)
        {
            $result = Main\UI\Filter\DateType::PERIOD_DATE;
        }
		return $result;
	}
	/**
	* @return void
	*/
	protected static function includeModuleFile()
	{
		if(self::$messagesLoaded)
		{
			return;
		}

		Main\Localization\Loc::loadMessages(__FILE__);
		self::$messagesLoaded = true;
	}
}