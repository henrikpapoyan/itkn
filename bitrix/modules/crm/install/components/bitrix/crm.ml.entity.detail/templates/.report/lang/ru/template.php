<?php
$MESS["CRM_ML_DEAL_FORECAST"] = "Прогноз по сделке";
$MESS["CRM_ML_LEAD_FORECAST"] = "Прогноз по лиду";
$MESS["CRM_ML_DEAL_SUCCESS_PROBABILITY"] = "Вероятность успеха";
$MESS["CRM_ML_LEAD_SUCCESS_PROBABILITY"] = "Вероятность конвертации в сделку";
$MESS["CRM_ML_SUCCESS_PROBABILITY_LOW"] = "Низкая";
$MESS["CRM_ML_SUCCESS_PROBABILITY_MEDIUM"] = "Средняя";
$MESS["CRM_ML_SUCCESS_PROBABILITY_HIGH"] = "Высокая";
$MESS["CRM_ML_FORECAST_DYNAMICS"] = "Динамика изменения прогноза";
$MESS["CRM_ML_INFLUENCING_EVENT"] = "Повлиявшее событие";
$MESS["CRM_ML_MODEL_QUALITY"] = "Качество";
$MESS["CRM_ML_MODEL_QUALITY_DYNAMICS"] = "Динамика изменения качества модели";
$MESS["CRM_ML_MODEL_MODEL_WILL_BE_TRAINED_AGAIN"] = "Будет автоматически переобучена через";
$MESS["CRM_ML_MODEL_SUCCESSFUL_DEALS_IN_TRAINING"] = "Успешных сделок в обучении";
$MESS["CRM_ML_MODEL_FAILED_DEALS_IN_TRAINING"] = "Неуспешных сделок в обучении";
$MESS["CRM_ML_MODEL_SUCCESSFUL_LEADS_IN_TRAINING"] = "Успешных лидов в обучении";
$MESS["CRM_ML_MODEL_FAILED_LEADS_IN_TRAINING"] = "Неуспешных лидов в обучении";
$MESS["CRM_ML_MODEL_MODEL_WILL_BE_TRAINED_IN_DAYS"] = "#DAYS# дней";

