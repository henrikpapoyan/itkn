import {EntityEditorRequisiteController} from "./requisite-controller";
import {EntityEditorRequisiteField} from "./requisite-crm-field";
import {EntityEditorRequisiteAddressField} from "./address-crm-field";
import {EntityEditorClientRequisites} from "./client-requisites";
import "./requisite.css"

export {
	EntityEditorRequisiteField,
	EntityEditorRequisiteAddressField,
	EntityEditorRequisiteController,
	EntityEditorClientRequisites
}
