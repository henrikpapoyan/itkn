<?
$MESS["BX_LIVECHAT_TITLE"] = "Открытая линия";
$MESS["BX_LIVECHAT_USER"] = "консультант";
$MESS["BX_LIVECHAT_ONLINE_LINE_1"] = "Мы онлайн";
$MESS["BX_LIVECHAT_ONLINE_LINE_2"] = "и готовы вам помочь!";
$MESS["BX_LIVECHAT_OFFLINE"] = "Наши консультанты ответят вам в самое ближайшее время!";
$MESS["BX_LIVECHAT_SONET_TITLE"] = "Кликните по нужной иконке и напишите нам через удобное для вас приложение";
$MESS["BX_LIVECHAT_SONET_MORE"] = "Еще #COUNT#...";
$MESS["BX_LIVECHAT_MOBILE_ROTATE"] = "Переверните устройство";
$MESS["BX_LIVECHAT_LOADING"] = "Подождите немного";
$MESS["BX_LIVECHAT_ERROR_TITLE"] = "К сожалению, загрузить онлайн-чат не удалось :(";
$MESS["BX_LIVECHAT_ERROR_DESC"] = "Пожалуйста, воспользуйтесь другими каналами связи или попробуйте открыть чат позже.";
$MESS["BX_LIVECHAT_PORTAL_USER"] = "Вы не можете написать в онлайн-чат, так как в этом браузере вы уже авторизованы как сотрудник этого Битрикс24. <br><br> Но вы можете написать в эту открытую линию у себя на #LINK_START#портале#LINK_END# или воспользоваться другими каналами связи.";
$MESS["BX_LIVECHAT_CLOSE_BUTTON"] = "Закрыть виджет";

$MESS["BX_LIVECHAT_VOTE_BUTTON"] = "Оценить качество обслуживания";
$MESS["BX_LIVECHAT_VOTE_PLUS"] = "Нравится";
$MESS["BX_LIVECHAT_VOTE_MINUS"] = "Не нравится";

$MESS["BX_LIVECHAT_MAIL_BUTTON"] = "Отправить копию разговора";
$MESS["BX_LIVECHAT_MAIL_TITLE"] = "Укажите E-mail адрес на который мы вышлем копию разговора.";
$MESS["BX_LIVECHAT_MAIL_FIELD_EMAIL"] = "Электронная почта";
$MESS["BX_LIVECHAT_MAIL_RESULT"] = "Мы отправили копию разговора на вашу почту.";
$MESS["BX_LIVECHAT_MAIL_SEND"] = "Отправить";
$MESS["BX_LIVECHAT_MAIL_CONSENT_ERROR"] = "Мы не можем отправить копию разговора, так как вы не дали свое согласие на обработку ваших данных.";

$MESS["BX_LIVECHAT_ABOUT_TITLE"] = "Представьтесь, пожалуйста";
$MESS["BX_LIVECHAT_ABOUT_FIELD_NAME"] = "Имя";
$MESS["BX_LIVECHAT_ABOUT_FIELD_EMAIL"] = "Электронная почта";
$MESS["BX_LIVECHAT_ABOUT_RESULT"] = "Приятно познакомиться!";
$MESS["BX_LIVECHAT_ABOUT_SEND"] = "Отправить";
$MESS["BX_LIVECHAT_ABOUT_CONSENT_ERROR"] = "Мы не можем сохранить информацию о вас, так как вы не дали свое согласие на обработку ваших данных.";

$MESS["BX_LIVECHAT_FIELD_NAME"] = "Имя";
$MESS["BX_LIVECHAT_FIELD_MAIL"] = "Электронная почта";
$MESS["BX_LIVECHAT_FIELD_PHONE"] = "Номер телефона";

$MESS["BX_LIVECHAT_CONSENT_TITLE"] = "Согласие на обработку персональных данных";
$MESS["BX_LIVECHAT_CONSENT_CHECKBOX_1"] = "Я даю свое согласие на";
$MESS["BX_LIVECHAT_CONSENT_CHECKBOX_2"] = "обработку моих данных";
$MESS["BX_LIVECHAT_CONSENT_AGREE"] = "Принимаю";
$MESS["BX_LIVECHAT_CONSENT_DISAGREE"] = "Не принимаю";

$MESS["BX_LIVECHAT_COPYRIGHT_TEXT"] = "Бесплатная CRM, чаты и сайты.";

$MESS["BX_LIVECHAT_TEXTAREA_PLACEHOLDER"] = "Введите ваше сообщение...";
$MESS["BX_LIVECHAT_TEXTAREA_FILE"] = "Отправить файл";
$MESS["BX_LIVECHAT_TEXTAREA_SMILE"] = "Выбор смайлов";
$MESS["BX_LIVECHAT_TEXTAREA_GIPHY"] = "Выбор GIF-картинок";

$MESS["BX_LIVECHAT_VOTE_TITLE"] = "Пожалуйста оцените качество обслуживания.";
$MESS["BX_LIVECHAT_VOTE_PLUS_TITLE"] = "Спасибо за оценку!";
$MESS["BX_LIVECHAT_VOTE_MINUS_TITLE"] = "Очень жаль, что мы не смогли помочь вам, мы постараемся стать лучше.";
?>