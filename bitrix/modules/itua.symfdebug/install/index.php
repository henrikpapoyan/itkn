<?
IncludeModuleLangFile(__FILE__);
Class itua_symfdebug extends CModule
{
	const MODULE_ID = 'itua.symfdebug';
	var $MODULE_ID = 'itua.symfdebug';
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;
	var $MODULE_CSS;
	var $strError = '';

	function __construct()
	{
		$arModuleVersion = array();
		include(dirname(__FILE__)."/version.php");
		$this->MODULE_VERSION = $arModuleVersion["VERSION"];
		$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		$this->MODULE_NAME = GetMessage("itua.symfdebug_MODULE_NAME");
		$this->MODULE_DESCRIPTION = GetMessage("itua.symfdebug_MODULE_DESC");

		$this->PARTNER_NAME = GetMessage("itua.symfdebug_PARTNER_NAME");
		$this->PARTNER_URI = GetMessage("itua.symfdebug_PARTNER_URI");
	}

	function InstallDB($arParams = array())
	{		
		RegisterModuleDependences('main', 'OnPageStart', self::MODULE_ID, 'CItuaSymfdebug', 'OnPageStartSymfdebug');
		return true;
	}

	function UnInstallDB($arParams = array())
	{		
		UnRegisterModuleDependences('main', 'OnPageStart', self::MODULE_ID, 'CItuaSymfdebug', 'OnPageStartSymfdebug');
		return true;
	}
	
	function InstallFiles($arParams = array())
	{	
		function full_copy($source, $target) {
		  if (is_dir($source))  {
		    @mkdir($target);
		    $d = dir($source);
		    while (FALSE !== ($entry = $d->read())) {
		      if ($entry == '.' || $entry == '..') continue;
		      full_copy("$source/$entry", "$target/$entry");
		    }
		    $d->close();
		  }
		  else copy($source, $target);
		}
		full_copy($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/'.self::MODULE_ID.'/vendor', $_SERVER['DOCUMENT_ROOT'].'/'.'vendor');
		
		return true;
	}

	function UnInstallFiles()
	{			
		function delFolder($dir)		{
			$files = array_diff(scandir($dir), array('.','..'));
			foreach ($files as $file) {
			(is_dir("$dir/$file")) ? delFolder("$dir/$file") : unlink("$dir/$file");
			}
			return rmdir($dir);
		}
				
		delFolder($_SERVER['DOCUMENT_ROOT'] . '/vendor');		
		
		return true;
	}

	function DoInstall()
	{
		global $APPLICATION;
		$this->InstallFiles();
		$this->InstallDB();
		RegisterModule(self::MODULE_ID);
	}

	function DoUninstall()
	{
		global $APPLICATION;
		UnRegisterModule(self::MODULE_ID);
		$this->UnInstallDB();
		$this->UnInstallFiles();
	}
}
?>
