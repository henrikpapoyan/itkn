<?php
Class CItuaSymfdebug
{
	function OnPageStartSymfdebug()
    {
		require_once $_SERVER["DOCUMENT_ROOT"].'/vendor/autoload.php';			
		
		/**
		 * dd() - dump function
		 * $data - data
		 * $exitRestart - restart buffer and exit
		 */
		function dd( $data )
        {
			$bt =  debug_backtrace();
		    $bt = $bt[0];
		    $dRoot = $_SERVER["DOCUMENT_ROOT"];
		    $dRoot = str_replace("/","\\",$dRoot);
		    $bt["file"] = str_replace($dRoot,"",$bt["file"]);
		    $dRoot = str_replace("\\","/",$dRoot);
		    $bt["file"] = str_replace($dRoot,"",$bt["file"]);
			
			$debFile = "<div style=\'font-size:9pt; color:#000; background:#fff; border:1px dashed #000;\'>";
			$debFile .= "<div style=\'padding:3px 5px; background:#99CCFF; font-weight:bold;\'>File:";		
			$debFile .= $bt["file"]. ' ' .$bt["line"];
			$debFile .= "</div>";
			$debFile .= "</div>";
			
			echo $debFile;
			dump($data);
		}
		
	}		
}
