module.exports = {
	input: './src/diskfile.js',
	output: './dist/diskfile.bundle.js',
	namespace: 'BX.Mobile',
	adjustConfigPhp: false
};