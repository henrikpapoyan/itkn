<?php
namespace Ez\CrmProps;
use Bitrix\Main;

IncludeModuleLangFile(__FILE__);

abstract class CrmProperty
{
    abstract function GetUserTypeDescription();

    function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName)
    {
        return self::GetEditForm($arProperty,$value, $strHTMLControlName);
    }

    function GetPropertyFieldHtmlMulty($arProperty, $value, $strHTMLControlName)
    {
        $values=array();
        foreach ($value as $v)
        {
            if (trim($v['VALUE']) != '')
            {
                $values[]=intval($v['VALUE']);
            }
        }

        ob_start();
        self::getEntitySelectorMulti($values,$strHTMLControlName);
        return ob_get_clean();
    }

    function getEntitySelectorMulti($values,$strHTMLControlName){

        $className=end(explode('\\',get_called_class()));
        $fieldIdentifier = self::getRandomFieldIdentifier($className);

        $GLOBALS["APPLICATION"]->IncludeComponent('bitrix:crm.entity.selector',
            '',
            array(
                'ENTITY_TYPE' => strtoupper($className),
                'INPUT_NAME'  => $fieldIdentifier,
                'INPUT_VALUE' => (is_array($values)) ? $values : '',
                'FORM_NAME'   => $strHTMLControlName["FORM_NAME"],
                'MULTIPLE'    => 'Y',
            ),
            false,
            array('HIDE_ICONS' => 'Y')
        );

        $formNameReplaced=str_replace('_','-',$strHTMLControlName['FORM_NAME']);
        $fieldIdentifierReplaced=strtolower(str_replace('_','-',$fieldIdentifier));
        $classNameToLower=strtolower($className);
        $cssSelector="#crm-crm-form-{$formNameReplaced}-{$fieldIdentifierReplaced}-open_{$fieldIdentifier}";

        echo "
            <script>
                $('{$cssSelector}-text-box').live('DOMSubtreeModified', function () {
                try {
                    $('#{$fieldIdentifier}_hidden_inputs').empty();
                    var arHrefs = $('{$cssSelector}-text-box').find('a');
                    arHrefs = Array.prototype.slice.call(arHrefs);
                    arHrefs.forEach(createInputs{$fieldIdentifier});
                }
                catch (e) {
                    $('#{$fieldIdentifier}').val('');
                }
                });

                function createInputs{$fieldIdentifier}(item, i, arr) {
                    var id = item.getAttribute('href');
                    var input = document.createElement('input');
                    input.type = 'hidden';
                    input.id = '{$fieldIdentifier}';
                    input.value = id.replace(/[^\d]/gi, '');
                    input.name = '{$strHTMLControlName['VALUE']}[]';
                    $(input).appendTo('#{$fieldIdentifier}_hidden_inputs')
                }
            </script>
            <div style='display: none;' id='{$fieldIdentifier}_hidden_inputs'></div>
            <style>
                {$cssSelector}-block-{$classNameToLower} .crm-block-cont-item, {$cssSelector}-block-search .crm-block-cont-item {width: 330px !important;}
                {$cssSelector}-block-{$classNameToLower} .crm-block-cont-contact-info, {$cssSelector}-block-search .crm-block-cont-contact-info {width: 295px !important;}
            </style>
        ";
    }

    function GetAdminListViewHTML($value)
    {
        return self::GetViewForm($value);
    }

    function GetPublicViewHTML($arProperty, $value, $strHTMLControlName)
    {
        return self::GetViewForm($value);
    }

    function GetPublicEditHTML($arProperty, $value, $strHTMLControlName)
    {
        return self::GetEditForm($arProperty,$value, $strHTMLControlName);
    }

    function GetPublicFilterHTML($arUserField, $arHtmlControl)
    {
        ob_start();
        self::getEntitySelectorFilter($arHtmlControl);
        return ob_get_clean();
    }

    function getEntitySelectorFilter($arHtmlControl)
    {
        $className = end(explode('\\', get_called_class()));
        //$fieldIdentifier = self::getRandomFieldIdentifier($className);
        $fieldIdentifier = $arHtmlControl['VALUE'];

        if($_REQUEST['clear_filter'] != 'Y'){
            $value=$_REQUEST[$arHtmlControl['VALUE']];
            $deleteEntitySelector="CRM_set{$fieldIdentifier}(document.getElementById('crm-{$fieldIdentifier}-open'), false)";
        }

        $GLOBALS["APPLICATION"]->IncludeComponent('bitrix:crm.entity.selector', '', array(
                'ENTITY_TYPE' => strtoupper($className),
                'INPUT_NAME'  => $fieldIdentifier,
                'INPUT_VALUE' => ($value) ? $value : '',
                'FORM_NAME'   => $arHtmlControl["FORM_NAME"],
                'MULTIPLE'    => 'N',
                'FILTER'      => 'Y',
            ), false, array('HIDE_ICONS' => 'Y'));

        $classNameToLower = strtolower($className);
        $cssSelector = "#crm-crm-{$fieldIdentifier}-open_{$fieldIdentifier}-block-";

        if($deleteEntitySelector){
            echo "<script>setTimeout({$deleteEntitySelector}, 100);</script>";
        }

        echo "
        <script>
            $(window).load(function () {
                $('#crm-{$fieldIdentifier}-box').live('DOMSubtreeModified', function () {
                if( $('#{$fieldIdentifier}').data('reset')!='1'){
                    try {
                        var deal = $('#crm-{$fieldIdentifier}-box').find('a').attr('href');

                        $('#{$fieldIdentifier}').val(deal.replace(/[^\d]/gi, ''));
                    }
                    catch (e) {
                        $('#{$fieldIdentifier}').val('');
                    }
                }
                else{
                    $('#{$fieldIdentifier}').val('');
                }

                });
            });

            $('[name=\"reset_filter\"]').click(function(){
                    $('#{$fieldIdentifier}').data('reset','1');
                    try{
                        BX.InterfaceGridFilter.prototype._handleCancelButtonClick();
                    } catch (e){}
                });
        </script>
        <style>
            {$cssSelector}{$classNameToLower} .crm-block-cont-item, {$cssSelector}search .crm-block-cont-item {width: 330px !important;}
            {$cssSelector}{$classNameToLower} .crm-block-cont-contact-info, {$cssSelector}search .crm-block-cont-contact-info {width: 295px !important;}
        </style>
        <input type='hidden' id='{$fieldIdentifier}' name='{$arHtmlControl['VALUE']}' value='{$value}'>
        ";
    }

    function GetEditForm($arProperty, $value, $strHTMLControlName)
    {
        ob_start();
        self::getEntitySelector($value,$strHTMLControlName);
        return ob_get_clean();
    }

    function getEntitySelector($value,$strHTMLControlName){

        $className=end(explode('\\',get_called_class()));
        $fieldIdentifier = self::getRandomFieldIdentifier($className);

        $GLOBALS["APPLICATION"]->IncludeComponent('bitrix:crm.entity.selector',
            '',

            array(
                'ENTITY_TYPE' => ($className=='invoice')?'CRM_'.strtoupper($className):strtoupper($className),
                'INPUT_NAME'  => $fieldIdentifier,
                'INPUT_VALUE' => ($value['VALUE'] != '') ? intval($value['VALUE']) : '',
                'FORM_NAME'   => $strHTMLControlName["FORM_NAME"],
                'MULTIPLE'    => 'N',
            ),
            false,
            array('HIDE_ICONS' => 'Y')
        );

        $formNameReplaced=str_replace('_','-',$strHTMLControlName['FORM_NAME']);
        $fieldIdentifierReplaced=strtolower(str_replace('_','-',$fieldIdentifier));
        $classNameToLower=strtolower($className);
        $cssSelector="#crm-crm-form-{$formNameReplaced}-{$fieldIdentifierReplaced}-open_{$fieldIdentifier}";

        echo "
        <script>
            $('{$cssSelector}-text-box').live('DOMSubtreeModified', function () {
                try {
                    var deal = $('{$cssSelector}-text-box').find('a').attr('href');
                    $('#{$fieldIdentifier}').val(deal.replace(/[^\d]/gi, ''));
                }
                catch (e) {
                    $('#{$fieldIdentifier}').val('');
                }
            });
        </script>
        <input type='hidden' id='{$fieldIdentifier}' name='{$strHTMLControlName['VALUE']}' value='{$value['VALUE']}'>
        <style>
            {$cssSelector}-block-{$classNameToLower} .crm-block-cont-item, {$cssSelector}-block-search .crm-block-cont-item {width: 330px !important;}
            {$cssSelector}-block-{$classNameToLower} .crm-block-cont-contact-info, {$cssSelector}-block-search .crm-block-cont-contact-info {width: 295px !important;}
        </style>
        ";
    }

    function GetViewForm($value)
    {
        $className=end(explode('\\',get_called_class()));

        switch ($className)
        {
            case('Contact'):
                $arSelect = array('LAST_NAME','NAME','SECOND_NAME');
                break;
            case('Invoice'):
                $arSelect = array('ORDER_TOPIC');
                break;
            default:
                $arSelect = array('TITLE');
                break;
        }

        $dbRes = call_user_func_array('CCrm'.$className.'::GetList', array(
                array(),
                array('ID' => $value['VALUE'],"CHECK_PERMISSIONS" => "N"),
                $arSelect
            )
        );
        if ($dbRes)
        {
            $res = $dbRes->Fetch();

            switch ($className)
            {
                case('Contact'):
                    $text="{$res['LAST_NAME']} {$res['NAME']} {$res['SECOND_NAME']}";
                    break;
                case('Invoice'):
                    $text = array('ORDER_TOPIC');
                    break;
                default:
                    $text=$res['TITLE'];
                    break;
            }

            $path = \COption::GetOptionString('crm', 'path_to_'.strtolower($className).'_show');

            return "<a href='".str_replace('#'.strtolower($className).'_id#', $value['VALUE'], $path)."' target='_blank'>".$text."</a>";
        }
        else
        {
            return '';
        }
    }

    function getRandomFieldIdentifier($className){
        return strtoupper($className).'_ID'.rand(1, 1000);
    }
}
