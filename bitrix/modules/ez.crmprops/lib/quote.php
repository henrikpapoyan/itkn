<?php
namespace Ez\CrmProps;

IncludeModuleLangFile(__FILE__);

class Quote extends CrmProperty{

    function GetUserTypeDescription()
    {
        return array(
            "PROPERTY_TYPE"=>"N",
            "USER_TYPE"=>"ez_crmquote",
            "DESCRIPTION"=>GetMessage('ez.crmprops_QUOTE_PROPERTY_NAME'),
            "GetPropertyFieldHtml"=>array(__CLASS__, "GetPropertyFieldHtml"),
            "GetAdminListViewHTML"=>array(__CLASS__, "GetAdminListViewHTML"),
            "GetPublicViewHTML"=>array(__CLASS__, "GetPublicViewHTML"),
            "GetPublicEditHTML"=>array(__CLASS__, "GetPublicEditHTML"),
            "GetPublicEditHTMLMulty" => array(__CLASS__,"GetPropertyFieldHtmlMulty"),
            "GetPublicFilterHTML"=>array(__CLASS__, "GetPublicFilterHTML"),
            "GetPropertyFieldHtmlMulty"=>array(__CLASS__, "GetPropertyFieldHtmlMulty"),
        );
    }
}