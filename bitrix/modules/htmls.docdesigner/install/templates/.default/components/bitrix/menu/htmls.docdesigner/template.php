<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (!empty($arResult)):?>
	<table cellspacing="0" class="bx-edit-tabs" width="100%">
		<tr>
			<td class="bx-tab-indent"><div class="empty"></div></td>
				<?
				$nTabs = count($arResult);
				foreach($arResult as $tab):
					$bSelected = (1 == $tab["SELECTED"]);
				?>
									<td title="<?=($tab["TEXT"])?>" id="tab_cont_<?=$tab["id"]?>" class="bx-tab-container<?=($bSelected? "-selected":"")?>">
										<table cellspacing="0">
											<tr>
												<td class="bx-tab-left<?=($bSelected? "-selected":"")?>" id="tab_left_<?=$tab["id"]?>"><div class="empty"></div></td>
												<td class="bx-tab<?=($bSelected? "-selected":"")?>" id="tab_<?=$tab["id"]?>"><a href="<?=$tab["LINK"]?>"><?=($tab["TEXT"])?></a></td>
												<td class="bx-tab-right<?=($bSelected? "-selected":"")?>" id="tab_right_<?=$tab["id"]?>"><div class="empty"></div></td>
											</tr>
										</table>
									</td>
				<?
				endforeach;
				?>
			<td width="100%"></td>
		</tr>
	</table>
<?endif?>