/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
  //config.extraPlugins = 'codesnippet';
  config.toolbar_Full = [
	{ name: 'document', items : [ 'Undo','Redo'] },
    { name: 'clipboard', items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord' ] },
	{ name: 'basicstyles', items : [ 'Bold','Italic','Underline','Subscript','Superscript' ] },
	{ name: 'paragraph', items : [ 'NumberedList','BulletedList','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock' ] },
	{ name: 'links', items : [ 'Link','Unlink' ] },
	{ name: 'insert', items : [ 'Table', 'Border' ] },//'Image',
    { name: 'styles', items : [ 'FontSize' ] },//'Styles','Format','Font',
    { name: 'colors', items : [ 'TextColor','BGColor' ] },
    { name: 'tools', items : [ 'Functions', 'Maximize','Source'] },//,'Blockquote','CodeSnippet'
  ];
  config.toolbar = 'Full';
  config.extraPlugins = 'panel,floatpanel,menu,contextmenu,border,functions';
  config.allowedContent = true;
  config.htmlEncodeOutput = true;
  //config.plugins += ',panel,floatpanel,menu,contextmenu';


};
