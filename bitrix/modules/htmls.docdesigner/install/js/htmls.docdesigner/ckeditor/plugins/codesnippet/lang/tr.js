/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.plugins.setLang( 'codesnippet', 'tr', {
	button: 'Kod parГ§acД±ДџД± ekle',
	codeContents: 'Kod',
	emptySnippetError: 'Kod parГ§acД±ДџД± boЕџ bД±rakД±lamaz',
	language: 'Dil',
	title: 'Kod parГ§acД±ДџД±',
	pathName: 'kod parГ§acД±ДџД±'
} );
