/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.plugins.setLang( 'codesnippet', 'fi', {
	button: 'LisГ¤Г¤ koodileike',
	codeContents: 'KoodisisГ¤ltГ¶',
	emptySnippetError: 'Koodileike ei voi olla tyhjГ¤.',
	language: 'Kieli',
	title: 'Koodileike',
	pathName: 'koodileike'
} );
