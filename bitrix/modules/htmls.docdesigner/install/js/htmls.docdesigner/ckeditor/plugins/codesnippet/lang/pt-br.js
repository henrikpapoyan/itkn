/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.plugins.setLang( 'codesnippet', 'pt-br', {
	button: 'Inserir fragmento de cГіdigo',
	codeContents: 'ConteГєdo do cГіdigo',
	emptySnippetError: 'Um fragmento de cГіdigo nГЈo pode ser vazio',
	language: 'Idioma',
	title: 'Fragmento de cГіdigo',
	pathName: 'fragmento de cГіdigo'
} );
