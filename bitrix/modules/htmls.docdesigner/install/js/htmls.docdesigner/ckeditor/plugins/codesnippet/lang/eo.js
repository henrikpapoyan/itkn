/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.plugins.setLang( 'codesnippet', 'eo', {
	button: 'Enmeti kodaДµeron',
	codeContents: 'Kodenhavo',
	emptySnippetError: 'KodaДµero ne povas esti malplena.',
	language: 'Lingvo',
	title: 'KodaДµero',
	pathName: 'kodaДµero'
} );
