/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.plugins.setLang( 'codesnippet', 'ug', {
	button: 'ЩѓЩ€ШЇ ЩѕШ§Ш±Ъ†Щ‰ШіЩ‰ Щ‚Щ‰ШіШЄЫ‡Ш±Ы‡Шґ',
	codeContents: 'Code content', // MISSING
	emptySnippetError: 'A code snippet cannot be empty.', // MISSING
	language: 'Language', // MISSING
	title: 'Code snippet', // MISSING
	pathName: 'code snippet' // MISSING
} );
