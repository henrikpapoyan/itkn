/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.plugins.setLang( 'codesnippet', 'lt', {
	button: 'Д®terpkite kodo gabaliukД…',
	codeContents: 'Kodo turinys',
	emptySnippetError: 'Kodo fragmentas negali bЕ«ti tusДЌias.',
	language: 'Kalba',
	title: 'Kodo fragmentas',
	pathName: 'kodo fragmentas'
} );
