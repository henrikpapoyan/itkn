/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.plugins.setLang( 'codesnippet', 'pl', {
	button: 'Wstaw fragment kodu',
	codeContents: 'TreЕ›Д‡ kodu',
	emptySnippetError: 'Kod nie moЕјe byД‡ pusty.',
	language: 'JД™zyk',
	title: 'Fragment kodu',
	pathName: 'fragment kodu'
} );
