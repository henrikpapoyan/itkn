/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.plugins.setLang( 'codesnippet', 'cs', {
	button: 'VloЕѕit Гєryvek kГіdu',
	codeContents: 'Obsah kГіdu',
	emptySnippetError: 'Гљryvek kГіdu nemЕЇЕѕe bГЅt prГЎzdnГЅ.',
	language: 'Jazyk',
	title: 'Гљryvek kГіdu',
	pathName: 'Гєryvek kГіdu'
} );
