/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.plugins.setLang( 'codesnippet', 'th', {
	button: 'а№Ѓаё—аёЈаёЃаёЉаёґа№‰аё™аёЄа№€аё§аё™аё‚аё­аё‡аёЈаё«аё±аёЄаё«аёЈаё·аё­а№‚аё„а№‰аё”',
	codeContents: 'Code content', // MISSING
	emptySnippetError: 'A code snippet cannot be empty.', // MISSING
	language: 'Language', // MISSING
	title: 'Code snippet', // MISSING
	pathName: 'code snippet' // MISSING
} );
