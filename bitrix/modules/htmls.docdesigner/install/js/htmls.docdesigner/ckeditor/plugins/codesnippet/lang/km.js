/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.plugins.setLang( 'codesnippet', 'km', {
	button: 'Insert Code Snippet', // MISSING
	codeContents: 'бћ?бћ¶бћЏбћ·бћЂбћ¶бћЂбћјбћЉ',
	emptySnippetError: 'A code snippet cannot be empty.', // MISSING
	language: 'бћ—бћ¶бћџбћ¶',
	title: 'Code snippet', // MISSING
	pathName: 'code snippet' // MISSING
} );
