/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.plugins.setLang( 'codesnippet', 'sk', {
	button: 'VloЕѕte kГіd Snippet-u',
	codeContents: 'Obsah kГіdu',
	emptySnippetError: 'Snippet kГіdu nesmie byЕҐ prГЎzdny.',
	language: 'Jazyk',
	title: 'Snippet kГіdu',
	pathName: 'snippet kГіdu'
} );
