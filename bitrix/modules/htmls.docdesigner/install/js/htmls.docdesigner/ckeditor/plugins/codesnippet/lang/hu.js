/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.plugins.setLang( 'codesnippet', 'hu', {
	button: 'Illeszd be a kГіdtГ¶redГ©ket',
	codeContents: 'KГіd tartalom',
	emptySnippetError: 'A kГіdtГ¶redГ©k nem lehet Гјres.',
	language: 'Nyelv',
	title: 'KГіdtГ¶redГ©k',
	pathName: 'kГіdtГ¶redГ©k'
} );
