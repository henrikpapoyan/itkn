CKEDITOR.plugins.add( 'functions', {
	requires: 'menubutton',
	icons: 'functions,calc,if,else,elseif,spelling',

	init: function( editor ) {
		/*var borderouter = ( editor.config.borderTypeList || [ 'border_bottom:Rahmenlinie unten:border-bottom:1px solid black',
		                                                      'border_top:Rahmenlinie oben:border-top:1px solid black',
		                                                      'border_right:Rahmenlinie rechts:border-right:1px solid black',
		                                                      'border_left:Rahmenlinie links:border-left:1px solid black'] ),//,
		                                                      //'border_double_bottom:Rahmenlinie doppelt unten:border-bottom:3px double black'] ),*/
			var plugin = this,
			items = {},
			parts,
			curBorderType,
			i;

			// Registers command.
			editor.addCommand( 'calculator', {
			    exec: function( editor, item) {
                  selection = editor.getSelection().getSelectedText();
                  editor.insertText("[CALC]" + selection + "[/CALC]");
			    },
 			    remove: function(editor, item){

			    },
            });
            editor.addCommand( 'spelling', {
			    exec: function( editor, item) {
                  selection = editor.getSelection().getSelectedText();
                  editor.insertText("[SPELL]" + selection + "[/SPELL]");
			    },
 			    remove: function(editor, item){

			    },
            });
            editor.addCommand( 'cif', {
			    exec: function( editor, item) {
                  selection = editor.getSelection().getSelectedText();
                  editor.insertText("[if]" + selection + "[/if]");
			    },
 			    remove: function(editor, item){

			    },
            });
            editor.addCommand( 'celse', {
			    exec: function( editor, item) {
                  selection = editor.getSelection().getSelectedText();
                  editor.insertText("[else]" + selection + "[/else]");
			    },
 			    remove: function(editor, item){

			    },
            });
            editor.addCommand( 'celseif', {
			    exec: function( editor, item) {
                  selection = editor.getSelection().getSelectedText();
                  editor.insertText("[elseif]" + selection + "[/elseif]");
			    },
 			    remove: function(editor, item){

			    },
            });
            //DealTypes
            //DealStages


        var lang = {};
        lang.calculator = htmlsDocDesigner.calculator;
        lang.spelling = htmlsDocDesigner.spelling;
        lang.cif = htmlsDocDesigner.cif;
        lang.celseif = htmlsDocDesigner.celseif;
        lang.celse = htmlsDocDesigner.celse;
        lang.functions = htmlsDocDesigner.functions;

		items.calculator= {
    		label: lang.calculator,
    		group: 'functions_main',
    		icon: 'calc',
    		order: 1,
    		onClick: function() {editor.execCommand('calculator', this );}
    	};
        items.spelling= {
    		label: lang.spelling,
    		group: 'functions_main',
    		icon: 'spelling',
    		order: 2,
    		onClick: function() {editor.execCommand('spelling', this );}
    	};
        items.cif= {
    		label: lang.cif,
    		group: 'functions_main',
    		icon: 'if',
    		order: 3,
    		onClick: function() {editor.execCommand('cif', this );}
    	};
        items.celseif= {
    		label: lang.celseif,
    		group: 'functions_main',
    		icon: 'elseif',
    		order: 4,
    		onClick: function() {editor.execCommand('celseif', this );}
    	};
        items.celse= {
    		label: lang.celse,
    		group: 'functions_main',
    		icon: 'else',
    		order: 5,
    		onClick: function() {editor.execCommand('celse', this );}
    	};
        /*
        order=0;
        for(var d in DealStages){
          editor.addCommand( d, {
			    exec: function( editor, item) {
                  editor.insertText('"'+d+'"');
			    }
          });

          order++;
          st = DealStages[d];
          items[d]= {
    		label: st,
    		group: 'deal',
    		//icon: 'else',
    		order: order,
    		onClick: function() {editor.execCommand(d, this );}
    	  };

        }
        */


		// Initialize groups for menu.
		editor.addMenuGroup( 'functions_main', 1 );
		editor.addMenuGroup( 'calculator', 2);
		editor.addMenuGroup( 'deal', 3);
		editor.addMenuItems( items );

		editor.ui.add( 'Functions',  CKEDITOR.UI_MENUBUTTON, {
			label: lang.functions,
			icon: "functions",
			toolbar: 'insert',
			//command: 'border',
			//allowedContent : 'table tr th td[style]{*} caption;',
			onMenu: function() {
				var activeItems = {}, parts;
				//curBorderType = plugin.getCurrentBorderType(editor);
				for ( var prop in items )
						activeItems[ prop ] = CKEDITOR.TRISTATE_OFF;

				return activeItems;
			}
		} );
	}
} );