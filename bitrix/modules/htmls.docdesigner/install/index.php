<?
/*
!!! ATTENTION !!!

The English version has own installation file!

!!! ATTENTION !!!
*/
global $MESS;
$strPath2Lang = str_replace("\\", "/", __FILE__);
$strPath2Lang = substr($strPath2Lang, 0, strlen($strPath2Lang)-18);
include(GetLangFileName($strPath2Lang."/lang/", "/install/index.php"));
require_once($_SERVER['DOCUMENT_ROOT']. "/bitrix/modules/main/classes/general/wizard_util.php");

Class htmls_docdesigner extends CModule{
	var $MODULE_ID = "htmls.docdesigner";
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;
	var $MODULE_CSS;

	function htmls_docdesigner(){
		$arModuleVersion = array();

		$path = str_replace("\\", "/", __FILE__);
		$path = substr($path, 0, strlen($path) - strlen("/index.php"));
		include($path."/version.php");

		if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion)){
			$this->MODULE_VERSION = $arModuleVersion["VERSION"];
			$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		}
		else{
			$this->MODULE_VERSION = DOCDESIGNER_VERSION;
			$this->MODULE_VERSION_DATE = DOCDESIGNER_VERSION_DATE;
		}

		$this->MODULE_NAME = GetMessage("DOCDESIGNER_MODULE_NAME");
		$this->MODULE_DESCRIPTION = GetMessage("DOCDESIGNER_MODULE_DESC");

		$this->PARTNER_NAME = 'HTMLStudio';
		$this->PARTNER_URI = "http://www.htmls.ru";

	}

	function InstallDB($arParams = array()){
         global $DB, $DBType, $APPLICATION;

         $errors = $DB->RunSQLBatch($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/htmls.docdesigner/install/".$DBType."/install.sql");

         if (!empty($errors))
         {
             $APPLICATION->ThrowException(implode("", $errors));
             return false;
         }
		return true;
	}

	function UnInstallDB($arParams = array()){
		global $DB, $DBType, $APPLICATION;
		$errors = $DB->RunSQLBatch($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/htmls.docdesigner/install/".$DBType."/uninstall.sql");

        $DB->StartTransaction();
        $iblock_id = COption::GetOptionInt("htmls.docdesigner", 'DOCDESIGNER_NUMERATORS');
        if(!CIBlock::Delete($iblock_id)){$DB->Rollback();}
        else{$DB->Commit();}

        $DB->StartTransaction();
        $iblock_id = COption::GetOptionInt("htmls.docdesigner", 'DOCDESIGNER_DOCS_REGISTER_ID');
        if(!CIBlock::Delete($iblock_id)){$DB->Rollback();}
        else{$DB->Commit();}

        $DB->StartTransaction();
        $iblock_id = COption::GetOptionInt("htmls.docdesigner", 'DOCDESIGNER_CONTRACTS_TEMPLATES_LIBRARY');
        if(!CIBlock::Delete($iblock_id)){$DB->Rollback();}
        else{$DB->Commit();}

        $DB->StartTransaction();
        $iblock_id = COption::GetOptionString("htmls.docdesigner", 'DOCDESIGNER_COMPANY');
        if(!CIBlock::Delete($iblock_id)){$DB->Rollback();}
        else{$DB->Commit();}

        $DB->StartTransaction();
        $iblock_id = COption::GetOptionInt("htmls.docdesigner", 'DOCDESIGNER_EXAMPLES_IBLOCK_ID');
        if(!CIBlock::Delete($iblock_id)){$DB->Rollback();}
        else{$DB->Commit();}

        if (!empty($errors))
   	    {
           	$APPLICATION->ThrowException(implode("", $errors));
            return false;
   	    }
		UnRegisterModule("htmls.docdesigner");

		return true;
	}

	function InstallEvents(){
		return true;
	}

	function UnInstallEvents(){
		return true;
	}

	function InstallFiles($arParams = array()){
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/htmls.docdesigner/install/templates", $_SERVER["DOCUMENT_ROOT"]."/bitrix/templates", true, true);
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/htmls.docdesigner/install/admin", $_SERVER["DOCUMENT_ROOT"]."/bitrix/admin", true);
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/htmls.docdesigner/install/images",  $_SERVER["DOCUMENT_ROOT"]."/bitrix/images", true, True);
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/htmls.docdesigner/install/js", $_SERVER["DOCUMENT_ROOT"]."/bitrix/js", true, true);
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/htmls.docdesigner/install/css", $_SERVER["DOCUMENT_ROOT"]."/bitrix/css", true, true);
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/htmls.docdesigner/install/components", $_SERVER["DOCUMENT_ROOT"]."/bitrix/components", true, true);
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/htmls.docdesigner/install/activities", $_SERVER["DOCUMENT_ROOT"]."/bitrix/activities", true, true);
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/htmls.docdesigner/install/apps", $_SERVER["DOCUMENT_ROOT"]."/apps", true, true);
		return true;
	}

	function UnInstallFiles(){
		DeleteDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/htmls.docdesigner/install/admin", $_SERVER["DOCUMENT_ROOT"]."/bitrix/admin");
		//DeleteDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/htmls.docdesigner/install/components/htmls", $_SERVER["DOCUMENT_ROOT"]."/bitrix/components");
		DeleteDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/htmls.docdesigner/install/templates", $_SERVER["DOCUMENT_ROOT"]."/bitrix/templates");
		DeleteDirFilesEx("/bitrix/images/htmls.docdesigner/");//images
		DeleteDirFilesEx("/bitrix/js/htmls.docdesigner/");
		DeleteDirFilesEx("/bitrix/css/htmls.docdesigner/");
		DeleteDirFilesEx("/bitrix/components/htmls/company.search/");
		DeleteDirFilesEx("/bitrix/components/htmls/docdesigner.journal/");
		DeleteDirFilesEx("/bitrix/components/htmls/docdesigner.pdf.admin/");
		DeleteDirFilesEx("/apps/htmls.docdesigner/");

        $CurrMenu = file_get_contents($_SERVER["DOCUMENT_ROOT"]."/.left.menu_ext.php");
		$pos = strpos($CurrMenu, "apps/htmls.docdesigner/");
		//AddMessage2Log("pos=".$pos, "htmls.DocDesigner");
		if($pos){
		  $CurrMenu = str_replace(GetMessage('DOC_DESIGNER'), '#DOC_DESIGNER#', $CurrMenu);
			$Suffix = file_get_contents($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/htmls.docdesigner/install/.left.menu_ext.php");
			$Init = $_SERVER["DOCUMENT_ROOT"]."/.left.menu_ext.php";
			copy($Init,$Init.".bak");
			if(touch($Init)){
				$File = fopen($Init,"w");
				fwrite($File, str_replace($Suffix, '', $CurrMenu));
				fclose($File);
			}
		}
	}

	function DoInstall(){
		global $DOCUMENT_ROOT, $APPLICATION, $step, $DB;
		$step = IntVal($step);
		if($step<2){
			$this->InstallFiles();
			$this->InstallDB();
			$APPLICATION->IncludeAdminFile(GetMessage("DOCDESIGNER_INSTALL_TITLE"), $DOCUMENT_ROOT."/bitrix/modules/htmls.docdesigner/install/step1.php");
		}
		/*$this->InstallDB();
		$this->InstallFiles();
		$APPLICATION->IncludeAdminFile(GetMessage("DOCDESIGNER_INSTALL_TITLE"), $DOCUMENT_ROOT."/bitrix/modules/htmls.docdesigner/install/step.php");*/
		elseif($step==2){
    		  CModule::IncludeModule("iblock");
    		  $res = CIBlockType::GetByID('lists');
              $arIBT = $res->Fetch();
              if(!$arIBT){
  				$arFields = Array(
  					'ID'=>'lists', 'SECTIONS'=>'Y', 'IN_RSS'=>'N', 'SORT'=>100,
  					'LANG'=>Array('ru'=>Array('NAME'=>GetMessage("DOCDESIGNER_INSTALL_LISTS"), 'SECTION_NAME'=>GetMessage("DOCDESIGNER_INSTALL_SECTION_NAME"), 'ELEMENT_NAME'=>GetMessage("DOCDESIGNER_INSTALL_ELEMENT_NAME"))));

  				$obBlocktype = new CIBlockType;
  				$DB->StartTransaction();
  				$res = $obBlocktype->Add($arFields);
  				if(!$res){
  					$DB->Rollback();
  					AddMessage2Log('010->Error: '.$obBlocktype->LAST_ERROR, "htmls.docdesigner");
  					die('010->Error: '.$obBlocktype->LAST_ERROR);
  				}
  				else
  					$DB->Commit();
  			}
			if(isset($_REQUEST["create_OwnEnt"])){
				$this->OwnEnt();
			}
			/*if(isset($_REQUEST["create_UF"])){
				$this->UserFields();
			}*/
            if(isset($_REQUEST["urlrewrite"])){
                $this->Add2urlrewrite();
            }
			$this->CreateRegister();
			$this->InstallExamples();
			RegisterModule("htmls.docdesigner");
			RegisterModuleDependences("iblock", "OnBeforeIBlockElementDelete", "htmls.docdesigner", "CDocDesignerProcessing", "OnListElemetDelete");
			RegisterModuleDependences("main", "OnBeforeProlog", "htmls.docdesigner", "CDocDesignerProcessing", "OnBizprocDelete");
			$APPLICATION->IncludeAdminFile(GetMessage("DOCDESIGNER_INSTALL_TITLE"), $DOCUMENT_ROOT."/bitrix/modules/htmls.docdesigner/install/step2.php");
		}
		$GLOBALS["errors"] = $this->errors;
	}

	function CreateRegister(){
		global $MESS;
		CModule::IncludeModule("iblock");
		CModule::IncludeModule("lists");
		$rsSites = CSite::GetList($by="sort", $order="desc", Array());
		while ($arSite = $rsSites->Fetch()){
			$sites[] = $arSite['ID'];
		}
		$ib = new CIBlock;
        $ibp = new CIBlockProperty;
		$check = CIBlock::GetList(Array(),Array('TYPE'=>'lists','CODE'=>'htmls_numerators'), true);
		if($check->SelectedRowsCount() == 0){
			$arProps = array(
				'NUMBER_TEMPLATE' => array('S', 20),
				'NUMBER_LENGTH' => array('N', 30),
				'NUMBER_RESET' => array('S', 40),
				'NUMBER_START' => array('N', 50),
				'CONTRACT_UNIQUE' => array('S', 60),
				'CORP_UNIQUE' => array('S', 70)
			);


			$arFields = Array("ACTIVE" => 'Y',"NAME" => GetMessage('NUMERATOR_NAME'),
				"CODE" => 'htmls_numerators', "IBLOCK_TYPE_ID" => 'lists', "SITE_ID" => $sites);
			if($ID = $ib->Add($arFields)){
				COption::SetOptionInt("htmls.docdesigner", 'DOCDESIGNER_NUMERATORS', $ID);
				$Field = new CListElementField($ID, 'NAME', GetMessage('DDS_NAME'), 10);
				foreach($arProps as $CODE => $v){
					$arFields = Array(
						"NAME" => GetMessage($CODE),
						"ACTIVE" => "Y",
						"SORT" => $v[1],
						"CODE" => $CODE,
						"PROPERTY_TYPE" => $v[0],
						"IBLOCK_ID" => $ID
					);

					$PropID = $ibp->Add($arFields);
					$Field = new CListElementField($ID, 'PROPERTY_' . $PropID, GetMessage($CODE), $v[1]);
				}
			}
			else{
				$this->errors = GetMessage("INSERT_ERROR",Array("#INTERFACE#"=>"Add numeratorss","#CODE#"=>$ib->LAST_ERROR));
			}
		}
		else{
			while($ar_res = $check->Fetch()){
				COption::SetOptionInt("htmls.docdesigner", 'DOCDESIGNER_NUMERATORS', $ar_res['ID']);
			}
		}

		if($NumsID = COption::GetOptionInt("htmls.docdesigner", 'DOCDESIGNER_NUMERATORS')){
			if($NumsID > 0){
				$check = CIBlock::GetList(Array(),Array('TYPE'=>'lists','CODE'=>'htmls_docs_register'), true);
				if($check->SelectedRowsCount() == 0){
                	$arFields = Array(
						"ACTIVE" => 'Y',
						"NAME" => GetMessage('DOCS_REGISTER'),
						"CODE" => 'htmls_docs_register',
						"IBLOCK_TYPE_ID" => 'lists',
						"SITE_ID" => $sites,
					);
					if($ID = $ib->Add($arFields)){
						$Field = new CListElementField($ID, 'NAME', GetMessage('DR_NAME'), 20);
						$Field = new CListElementField($ID, 'DATE_CREATE', GetMessage('DR_DATE_CREATE'), 30);

						$arProps = array(
							'DOC' => array(GetMessage('DR_DOC'), 'S', 10),
							'SUMMA' => array(GetMessage('DR_SUMMA'), 'N', 40),
							'OWN_ENTITIES' => array(GetMessage('DR_OWN_ENTITIES'), 'E', 50, COption::GetOptionString("htmls.docdesigner", 'DOCDESIGNER_COMPANY')),
							'COMPANY_ID' => array(GetMessage('DR_COMPANY'), 'N', 60),
							'DEAL_ID' => array(GetMessage('DR_DEAL'), 'N',70),
							'LEAD_ID' => array('LEAD_ID', 'N',70),
							'CONTACT_ID' => array('CONTACT_ID', 'N',70),
							'INVOICE_ID' => array('INVOICE_ID', 'N',70),
							'OFFER_ID' => array('OFFER_ID', 'N',70),
							'LINK2FILE' => array(GetMessage('DR_FILE'), 'N', 80)
						);
						if(!IsModuleInstalled("disk")){
							$arProps['LINK2FILE'] = array(GetMessage('DR_FILE'), 'F', 80);
						}
						foreach($arProps as $CODE => $v){
							if($CODE == 'DEAL_ID'){
								if(!IsModuleInstalled("crm")) continue;
							}

							$arFields = Array(
								"NAME" => $v[0],
								"ACTIVE" => "Y",
								"SORT" => $v[2],
								"CODE" => $CODE,
								"PROPERTY_TYPE" => $v[1],
								"IBLOCK_ID" => $ID,
								'LINK_IBLOCK_ID' => $v[3]
							);

							$PropID = $ibp->Add($arFields);
							$Field = new CListElementField($ID, 'PROPERTY_' . $PropID, $v[0], $v[2]);
							if($CODE == 'COMPANY_ID'){
								COption::SetOptionInt("htmls.docdesigner", 'DOCDESIGNER_DOCS_REGISTER_COMPANY_ID', $PropID, $v[0]);
							}
							if($CODE == 'DEAL_ID'){
								COption::SetOptionInt("htmls.docdesigner", 'DOCDESIGNER_DOCS_REGISTER_DEAL_ID', $PropID, $v[0]);
							}
							if($CODE == 'LINK2FILE'){
								COption::SetOptionInt("htmls.docdesigner", 'DOCDESIGNER_DOCS_REGISTER_LINK2FILE', $PropID, $v[0]);
							}
						}
						$arFields = Array(
								"NAME" => GetMessage('DR_PREFIX'),
								"ACTIVE" => "Y",
								"SORT" => 900,
								"CODE" => 'PREFIX',
								"PROPERTY_TYPE" => 'S',
								"IBLOCK_ID" => $ID
						);
						$PropID = $ibp->Add($arFields);

						$arFields = Array(
								"NAME" => GetMessage('DR_NUMBER_SYS'),
								"ACTIVE" => "Y",
								"SORT" => 800,
								"CODE" => 'NUMBER',
								"PROPERTY_TYPE" => 'N',
								"IBLOCK_ID" => $ID
						);
						$PropID = $ibp->Add($arFields);

						$arFields = Array(
								"NAME" => GetMessage('DR_BIZPROC_ID'),
								"ACTIVE" => "Y",
								"SORT" => 990,
								"CODE" => 'BIZPROC_ID',
								"PROPERTY_TYPE" => 'S',
								"IBLOCK_ID" => $ID
						);
						$PropID = $ibp->Add($arFields);
						$arFields = Array(
								"NAME" => GetMessage('DR_YEAR'),
								"ACTIVE" => "Y",
								"SORT" => 990,
								"CODE" => 'YEAR',
								"PROPERTY_TYPE" => 'N',
								"IBLOCK_ID" => $ID
						);
						$PropID = $ibp->Add($arFields);
						//numerator table
						$arFields = Array(
								"NAME" => GetMessage('DR_NUMERATOR'),
								"ACTIVE" => "Y",
								"SORT" => 990,
								"CODE" => 'NUMERATOR',
								"PROPERTY_TYPE" => 'E',
								"IBLOCK_ID" => $ID,
								'LINK_IBLOCK_ID' => $NumsID
						);
						$PropID = $ibp->Add($arFields);
						$arFields = Array(
								"NAME" => GetMessage('DR_TABLE'),
								"ACTIVE" => "Y",
								"SORT" => 990,
								"CODE" => 'TABLE',
								"PROPERTY_TYPE" => 'N',
								"IBLOCK_ID" => $ID
						);
						$PropID = $ibp->Add($arFields);
						COption::SetOptionInt("htmls.docdesigner", 'DOCDESIGNER_DOCS_REGISTER_ID', $ID, GetMessage('DOCS_REGISTER'));


						$el = new CIBlockElement;

						$PROP = array();
						$PROP['NUMBER_TEMPLATE'] = "{PREFIX}-{YEAR}-{NUMBER}";
						$PROP['NUMBER_LENGTH'] = 3;
						$PROP['NUMBER_RESET'] = 'Y';
						$PROP['NUMBER_START'] = 1;

						$arLoadProductArray = Array("IBLOCK_SECTION_ID" => false,
							"IBLOCK_ID"      => $NumsID,
							"PROPERTY_VALUES"=> $PROP,
							"NAME"           => GetMessage('NUM_CONTRACT'),
							"ACTIVE"         => "Y"
						);
                        $el->Add($arLoadProductArray);
						//if($PRODUCT_ID = $el->Add($arLoadProductArray))

						$arLoadProductArray = Array("IBLOCK_SECTION_ID" => false,
							"IBLOCK_ID"      => $NumsID,
							"PROPERTY_VALUES"=> $PROP,
							"NAME"           => GetMessage('NUM_INVOICE'),
							"ACTIVE"         => "Y"
						);
                        $el->Add($arLoadProductArray);
						//if($PRODUCT_ID = $el->Add($arLoadProductArray))
						//COption::SetOptionInt("htmls.docdesigner", 'DOCDESIGNER_DOCS_REGISTER_ID', 135, "������ ����������");
					}
					else{
						GetMessage("INSERT_ERROR",Array("#INTERFACE#"=>"Add docs register","#CODE#"=>$ib->LAST_ERROR));
					}
				}
				else{
					while($ar_res = $check->Fetch()){
						COption::SetOptionInt("htmls.docdesigner", 'DOCDESIGNER_DOCS_REGISTER_ID', $ar_res['ID'], GetMessage('DOCS_REGISTER'));
					}
				}
			}
		}
        COption::SetOptionString("htmls.docdesigner", "successfully_converted", 'Y');
        $check = CIBlock::GetList(Array(),Array('TYPE'=>'lists','CODE'=>'htmls_tmp_register'), true);
        if($check->SelectedRowsCount() == 0){
          $arFields = Array(
        		"ACTIVE" => 'Y',
        		"NAME" => GetMessage('DOC_DESIGNER_TEMPLATES'),
        		"CODE" => 'htmls_tmp_register',
        		"IBLOCK_TYPE_ID" => 'lists',
        		"SITE_ID" => $sites,
          );
          if($tID = $ib->Add($arFields)){
            COption::SetOptionInt("htmls.docdesigner", 'DOCDESIGNER_CONTRACTS_TEMPLATES_LIBRARY', $tID);
            $Field = new CListElementField($tID, 'NAME', GetMessage('DDS_NAME'), 20);

    		$arProps = array(
    			'FILE' => array(GetMessage('DR_FILE'), 'F', 80)
    		);
            foreach($arProps as $CODE => $v){
    			$arFields = Array(
    				"NAME" => $v[0],
    				"ACTIVE" => "Y",
    				"SORT" => $v[2],
    				"CODE" => $CODE,
    				"PROPERTY_TYPE" => $v[1],
    				"IBLOCK_ID" => $tID,
    				'LINK_IBLOCK_ID' => $v[3]
    			);

    			$PropID = $ibp->Add($arFields);
    			$Field = new CListElementField($tID, 'PROPERTY_' . $PropID, $v[0], $v[2]);
    			if($CODE == 'FILE'){
    				COption::SetOptionInt("htmls.docdesigner", 'DOCDESIGNER_TMP_REGISTER_FILE', $PropID);
    			}
    		}
          }
        }
	}

	function DoUninstall(){
		global $DB, $DOCUMENT_ROOT, $APPLICATION, $step, $DBType;
		$step = IntVal($step);
		if($step<2){
			$APPLICATION->IncludeAdminFile(GetMessage("DOCDESIGNER_UNINSTALL_TITLE"), $DOCUMENT_ROOT."/bitrix/modules/htmls.docdesigner/install/unstep1.php");
            
        }
		elseif($step==2)
		{
		    $this->UnInstallFiles();
    		$this->UnInstallDB();
    		COption::RemoveOption("htmls.docdesigner");
    		UnRegisterModuleDependences("iblock", "OnBeforeIBlockElementDelete", "htmls.docdesigner", "CDocDesignerProcessing", "OnListElemetDelete");
            UnRegisterModuleDependences("main", "OnBeforeProlog", "htmls.docdesigner", "CDocDesignerProcessing", "OnBizprocDelete");

			if ($_REQUEST['removedata'] == 'Y'){
			  DeleteDirFilesEx("/bitrix/activities/custom/htmlscreatedocxactivity/");
			  DeleteDirFilesEx("/bitrix/activities/custom/htmlscreateinvoiceactivity/");
			  DeleteDirFilesEx("/bitrix/activities/custom/htmlscreatepdfactivity/");
			  DeleteDirFilesEx("/bitrix/activities/custom/htmlsnumberplusactivity/");
			  DeleteDirFilesEx("/bitrix/activities/custom/htmlssendemailactivity/");
			  DeleteDirFilesEx("/bitrix/activities/custom/htmlsdisksectionactivity/");
			  DeleteDirFilesEx("/bitrix/templates/bitrix24/components/bitrix/list.list/htmls.docdesigner/");
			  DeleteDirFilesEx("/bitrix/templates/.default/components/bitrix/list.list/htmls.docdesigner/");
			  DeleteDirFilesEx("/bitrix/templates/.default/components/bitrix/bizproc.document/htmls.bizproc.document/");
			  DeleteDirFilesEx("/bitrix/templates/.default/components/bitrix/bizproc.task/htmls.bizproc.task/");
			  DeleteDirFilesEx("/bitrix/templates/.default/components/bitrix/bizproc.wizards/htmls.bizproc.wizards/");
			  DeleteDirFilesEx("/bitrix/templates/.default/components/bitrix/bizproc.wizards.list/htmls.bizproc.wizards.list/");
			  DeleteDirFilesEx("/bitrix/templates/.default/components/bitrix/bizproc.wizards.start/htmls.bizproc.wizards.start/");
			  DeleteDirFilesEx("/bitrix/templates/.default/components/bitrix/bizproc.wizards.task/htmls.bizproc.wizards.task/");
			  DeleteDirFilesEx("/bitrix/templates/.default/components/bitrix/bizproc.workflow.edit/htmls.bizproc.workflow.edit/");
			  DeleteDirFilesEx("/bitrix/templates/.default/components/bitrix/bizproc.workflow.start/htmls.bizproc.workflow.start/");
			  DeleteDirFilesEx("/bitrix/templates/.default/components/bitrix/crm.activity.task.list/htmls.crm.activity.task.list/");
			  DeleteDirFilesEx("/bitrix/templates/.default/components/bitrix/crm.company/htmls.company.disk/");
			  DeleteDirFilesEx("/bitrix/templates/.default/components/bitrix/crm.contact/htmls.contact.disk/");
			  DeleteDirFilesEx("/bitrix/templates/.default/components/bitrix/crm.deal/htmls.deal.disk/");
			  DeleteDirFilesEx("/bitrix/templates/.default/components/bitrix/crm.lead/htmls.lead.disk/");
			  DeleteDirFilesEx("/bitrix/templates/.default/components/bitrix/crm.company.show/htmls.company.disk/");
			  DeleteDirFilesEx("/bitrix/templates/.default/components/bitrix/crm.contact.show/htmls.contact.disk/");
			  DeleteDirFilesEx("/bitrix/templates/.default/components/bitrix/crm.deal.show/htmls.deal.disk/");
			  DeleteDirFilesEx("/bitrix/templates/.default/components/bitrix/crm.lead.show/htmls.lead.disk/");
			  DeleteDirFilesEx("/bitrix/templates/.default/components/bitrix/crm.config.bp/htmls.crm.config.bp/");
			  DeleteDirFilesEx("/bitrix/templates/.default/components/bitrix/lists/.default/");
			  DeleteDirFilesEx("/bitrix/templates/.default/components/bitrix/lists/htmls.docdesigner.lists/");
			  DeleteDirFilesEx("/bitrix/templates/.default/components/bitrix/lists.lists/htmls.docdesigner.lists/");
			  DeleteDirFilesEx("/bitrix/templates/.default/components/bitrix/lists/htmls.docdesigner.processes/");
			  DeleteDirFilesEx("/bitrix/templates/.default/components/bitrix/lists.lists/htmls.docdesigner.processes/");
			  DeleteDirFilesEx("/bitrix/templates/.default/components/bitrix/main.interface.grid/htmls.main.interface.grid/");
			  DeleteDirFilesEx("/bitrix/templates/.default/components/bitrix/menu/htmls.docdesigner/");
			  DeleteDirFilesEx("/bitrix/templates/.default/components/bitrix/photogallery/docdesigner/");
			  DeleteDirFilesEx("/bitrix/templates/.default/components/bitrix/photogallery.detail.list.ex/docdesigner/");
			  DeleteDirFilesEx("/bitrix/templates/.default/components/bitrix/photogallery.section.list/docdesigner/");

			}
			$APPLICATION->IncludeAdminFile(GetMessage("DOCDESIGNER_UNINSTALL_TITLE"), $DOCUMENT_ROOT."/bitrix/modules/htmls.docdesigner/install/unstep2.php");
		}

	}

	function OwnEnt(){
		global $MESS;
		CModule::IncludeModule("iblock");
		CModule::IncludeModule("lists");
		$check = CIBlock::GetList(Array(),Array('TYPE'=>'lists','CODE'=>'own_company'), true);
		if($check->SelectedRowsCount() == 0){
			$rsSites = CSite::GetList($by="sort", $order="desc", Array());
			while ($arSite = $rsSites->Fetch()){
				$sites[] = $arSite['ID'];
			}
			$ib = new CIBlock;
			$arFields = Array("ACTIVE" => 'Y',"NAME" => GetMessage('DDS_LIST_NAME'),
				"CODE" => 'own_company', "IBLOCK_TYPE_ID" => 'lists', "SITE_ID" => $sites);
			if($ID = $ib->Add($arFields)){
				COption::SetOptionString("htmls.docdesigner", 'DOCDESIGNER_COMPANY', $ID, GetMessage('DDS_LIST_NAME'));
				$Field = new CListElementField($ID, 'NAME', GetMessage('DDS_NAME'), 10);
				$Field = new CListElementField($ID, 'PREVIEW_TEXT', GetMessage('DDS_BANK_DETAILS'), 20);
				$Field = new CListElementField($ID, 'PREVIEW_PICTURE', GetMessage('DDS_SMALL_LOGO'), 30);
				$Field = new CListElementField($ID, 'DETAIL_PICTURE', GetMessage('DDS_BIG_LOGO'), 40);

				$arProps['DDS_FAKSIMILE'] = array('F', 50);
				$arProps['DDS_PERSON'] = array('S', 60);
				$arProps['DDS_INN'] = array('S', 70);
				$arProps['DDS_KPP'] = array('S', 80);
				$arProps['DDS_OGRN'] = array('S', 90);
				$arProps['DDS_RS'] = array('S', 100);
				$arProps['DDS_BANK_NAME'] = array('S', 110);
				$arProps['DDS_BANK_BIK'] = array('S', 120);
				$arProps['DDS_BANK_KS'] = array('S', 130);
				$arProps['DDS_BANK_CITY'] = array('S', 140);
				$arProps['DDS_POST_ADDRESS'] = array('S', 150);
				$arProps['DDS_PHONE'] = array('S', 160);
				$arProps['DDS_ADDRESS_LEGAL'] = array('S', 170);
				$arProps['DDS_ADDRESS'] = array('S', 180);
				$arProps['DDS_CEO_POST'] = array('S', 190);
				$arProps['DDS_CEO_BASE'] = array('S', 200);
				$arProps['DDS_CEO_SIGN'] = array('F', 210);
				$arProps['DDS_ACC_POST'] = array('S', 220);
				$arProps['DDS_ACC_NAME'] = array('S', 230);
				$arProps['DDS_ACC_SIGN'] = array('F', 240);
				$arProps['DDS_STAMP'] = array('F', 180);
				$ibp = new CIBlockProperty;
				foreach($arProps as $CODE => $v){
					$arFields = Array(
						"NAME" => GetMessage($CODE),
						"ACTIVE" => "Y",
						"SORT" => $v[1],
						"CODE" => substr($CODE, 4),
						"PROPERTY_TYPE" => $v[0],
						"IBLOCK_ID" => $ID
					);

					$PropID = $ibp->Add($arFields);
					$Field = new CListElementField($ID, 'PROPERTY_' . $PropID, GetMessage($CODE), $v[1]);
					COption::SetOptionString("htmls.docdesigner", str_replace('DDS', 'DOCDESIGNER_COMPANY', $CODE), $PropID);
				}
			}
			else{
				$this->errors = GetMessage("INSERT_ERROR",Array("#INTERFACE#"=>"Add own entities","#CODE#"=>$ib->LAST_ERROR));
			}
		}
		else{
			while($ar_res = $check->Fetch()){
				COption::SetOptionString("htmls.docdesigner", 'DOCDESIGNER_COMPANY', $ar_res['ID'], GetMessage('DDS_LIST_NAME'));
			}
		}
	}

	function InstallExamples(){
		global $MESS;
		CModule::IncludeModule("iblock");
		$file_name = "/bitrix/modules/htmls.docdesigner/examples/examples.xml";
		$iblock_type = "services";
		$check = CIBlock::GetList(Array(),Array('TYPE'=>'services','CODE'=>'DocDesignerExamples'), true);
		if($check->SelectedRowsCount() == 0){
			ImportXMLFile($file_name, $iblock_type);
			//AddMessage2Log("ImportXMLFile", "htmls.DocDesigner");
		}

		$res = CIBlock::GetList(Array(), Array('TYPE'=>'services', "CODE"=>'DocDesignerExamples'), true);
		while($ar_res = $res->Fetch()){
			$examplesID = $ar_res['ID'];
			//AddMessage2Log("ImportXMLFile=".$examplesID, "htmls.DocDesigner");
		}
		COption::SetOptionInt('htmls.docdesigner', "DOCDESIGNER_EXAMPLES_IBLOCK_ID", $examplesID);

		$arMenuReplace["DOC_JOURNAL"] = GetMessage('DOC_JOURNAL');
		$arMenuReplace["BP_PDF"] = GetMessage('BP_PDF');
		$arMenuReplace["PDF"] = GetMessage('PDF');
		$arMenuReplace["EXAMPLES"] = GetMessage('EXAMPLES');
		$arMenuReplace["EXAMPLES_ID"] = $examplesID;
		CWizardUtil::ReplaceMacros($_SERVER["DOCUMENT_ROOT"]."/apps/htmls.docdesigner/.left.menu.php", $arMenuReplace);
		CWizardUtil::ReplaceMacros($_SERVER["DOCUMENT_ROOT"]."/apps/htmls.docdesigner/examples/index.php", $arMenuReplace);
		CWizardUtil::ReplaceMacros($_SERVER["DOCUMENT_ROOT"]."/apps/htmls.docdesigner/examples/.section.php", $arMenuReplace);

		$CurrMenu = file_get_contents($_SERVER["DOCUMENT_ROOT"]."/.left.menu_ext.php");
		$pos = strpos($CurrMenu, "apps/htmls.docdesigner/");
		//AddMessage2Log("pos=".$pos, "htmls.DocDesigner");
		if(!$pos){
			$Suffix = file_get_contents($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/htmls.docdesigner/install/.left.menu_ext.php");
			$Init = $_SERVER["DOCUMENT_ROOT"]."/.left.menu_ext.php";
			copy($Init,$Init.".bak");
			if(touch($Init)){
				$File = fopen($Init,"a+");
				fwrite($File, $Suffix);
				fclose($File);
			}
			$arReplace["DOC_DESIGNER"] = GetMessage('DOC_DESIGNER');
			CWizardUtil::ReplaceMacros($_SERVER["DOCUMENT_ROOT"]."/.left.menu_ext.php", $arReplace);
		}
	}

	function UserFields(){
		global $MESS;
		CModule::IncludeModule("crm");
		$arProps['DDS_INN'] = array('string', 100);
		$arProps['DDS_KPP'] = array('string', 200);
		$arProps['DDS_OGRN'] = array('string', 300);
		$arProps['DDS_RS'] = array('string', 400);
		$arProps['DDS_BANK_NAME'] = array('string', 500);
		$arProps['DDS_BANK_BIK'] = array('string', 600);
		$arProps['DDS_BANK_KS'] = array('string', 700);
		$arProps['DDS_BANK_CITY'] = array('string', 800);
		$arProps['DDS_POST_ADDRESS'] = array('string', 900);
		$arProps['DDS_PHONE'] = array('string', 910);

		$obUserField  = new CUserTypeEntity();
		$t = time();
		foreach($arProps as $CODE => $v){
			$FName = 'UF_CRM_' . $t;
			$arFields = array('USER_TYPE_ID' => $v[0],
		  						'ENTITY_ID' => 'CRM_COMPANY',
		  						'SORT' => $v[1],
		  						'MULTIPLE' => 'N', 'MANDATORY' => 'N', 'SHOW_FILTER' => 'E',
		  						'EDIT_FORM_LABEL' => Array('ru' => GetMessage($CODE), 'en' => GetMessage($CODE)),
							'LIST_COLUMN_LABEL' => Array('ru' => GetMessage($CODE), 'en' => GetMessage($CODE)),
							'LIST_FILTER_LABEL' => Array('ru' => GetMessage($CODE), 'en' => GetMessage($CODE)),
							'SETTINGS' => Array('DEFAULT_VALUE' => ''),
							'FIELD_NAME' => $FName);
			if($ID = $obUserField->Add($arFields))
				$r = 1;
			else{
				$this->errors = GetMessage("INSERT_ERROR",Array("#INTERFACE#"=>"Add user field","#CODE#"=>$obUserField->LAST_ERROR));
			}
			COption::SetOptionString("htmls.docdesigner", str_replace('DDS', 'DOCDESIGNER_CRM_COMPANY', $CODE), $FName);
			$t++;
		}
	}

    function Add2urlrewrite()
    {
        $ur = $_SERVER["DOCUMENT_ROOT"]."/urlrewrite.php";
        $Add = file_get_contents($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/htmls.docdesigner/install/urlrewrite.php");
        if(file_exists($ur)){
            copy($ur,$ur.".bak");
        }
        if(touch($ur)){
            $File = fopen($ur,"a+");
            fwrite($File, $Add);
            fclose($File);
        }
        else $this->errors = GetMessage("INSERT_ERROR",Array("#INTERFACE#"=>$ur,"#CODE#"=>$Add));

        return true;
    }
}
?>