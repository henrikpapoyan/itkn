<?if(!check_bitrix_sessid()) return;?>
<?
global $errors;
include(GetLangFileName(dirname(__FILE__).'/lang/', '/step2.php'));
if(!$errors):
	echo CAdminMessage::ShowNote(GetMessage("MOD_INST_OK"));
else:
	for($i=0; $i<count($errors); $i++)
		$alErrors .= $errors[$i]."<br>";
	echo CAdminMessage::ShowMessage(Array("TYPE"=>"ERROR", "MESSAGE" =>GetMessage("MOD_INST_ERR"), "DETAILS"=>$alErrors, "HTML"=>true));
endif;
?>
<?=GetMessage("DOCDESIGNER_SETTINGS")?>
<br/>
<?=GetMessage("DOCDESIGNER_PUBLIC")?>
<form action="<?echo $APPLICATION->GetCurPage()?>">
<p>
	<input type="hidden" name="lang" value="<?echo LANG?>">
	<input type="submit" name="" value="<?echo GetMessage("MOD_BACK")?>">
</p>
<form>