<?
/* htmls.docdesigner installer - start */
$arUrlRewrite[] = array(
		"CONDITION" => "#^/apps/htmls.docdesigner/bp/#",
		"RULE" => "",
		"ID" => "bitrix:crm.config.bp",
		"PATH" => "/apps/htmls.docdesigner/bp/index.php",
	    );

$arUrlRewrite[] = array(
		"CONDITION" => "#^/apps/htmls.docdesigner/processes/#",
		"RULE" => "",
		"ID" => "bitrix:lists",
		"PATH" => "/apps/htmls.docdesigner/processes/index.php",
	    );

$arUrlRewrite[] = array(
		"CONDITION" => "#^/apps/htmls.docdesigner/lists/#",
		"RULE" => "",
		"ID" => "bitrix:lists",
		"PATH" => "/apps/htmls.docdesigner/lists/index.php",
	    );

$arUrlRewrite[] = array(
		"CONDITION" => "#^/apps/htmls.docdesigner/sbp/#",
		"RULE" => "",
		"ID" => "bitrix:bizproc.wizards",
		"PATH" => "/apps/htmls.docdesigner/sbp/index.php",
	    );
/* htmls.docdesigner installer - end */
?>