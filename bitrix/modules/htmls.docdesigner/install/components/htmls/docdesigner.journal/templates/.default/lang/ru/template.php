<?
$MESS["CT_BNL_ELEMENT_DELETE_CONFIRM"] = "Будет удалена вся информация, связанная с этой записью. Продолжить?";
$MESS["HTMLS_DOCDESIGNER_OWN_COMPANY"] = "Юр.лицо";
$MESS["HTMLS_DOCDESIGNER_DOCUMENT"] = "Документ";
$MESS["HTMLS_DOCDESIGNER_NUMBER"] = "Номер";
$MESS["HTMLS_DOCDESIGNER_DATE"] = "Дата";
$MESS["HTMLS_DOCDESIGNER_SUMMA"] = "Сумма";
$MESS["HTMLS_DOCDESIGNER_COMPANY"] = "Компания";
$MESS["HTMLS_DOCDESIGNER_DEAL"] = "Сделка";
$MESS["HTMLS_DOCDESIGNER_FILE"] = "Файл";
$MESS["HTMLS_DOCDESIGNER_SELECT_ALL"] = "Применить действие для всех записей в списке";
$MESS["HTMLS_DOCDESIGNER_BUTTON"] = "Удалить";
$MESS["HTMLS_DOCDESIGNER_FILTER"] = "Применить";
$MESS["HTMLS_DOCDESIGNER_CANCEL"] = "Отменить";
?>