<? if( !defined( "B_PROLOG_INCLUDED" ) || B_PROLOG_INCLUDED !== true ) die(); ?>

<?
	if( empty( $arResult['COMPANIES'] ) ) {
		return;
	}
?>

<? $iIndex = 0; ?>
<? foreach( $arResult['COMPANIES'] as $iId => $sName ): ?>
	<li <? if( !$iIndex ): ?>class="first"<? endif; ?>><a class="company-search_search_results_lnk" href="#" rel="<?=$iId?>"><?=$sName?></a></li>
	<? $iIndex++; ?>
<? endforeach ?>
