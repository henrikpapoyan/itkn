jQuery( document ).ready( function() {

	jQuery( '#company-search_search_results' ).css( 'left', ( jQuery( '#company-search_search_input' ).position().left + 2 ) );
	
	jQuery( '#company-search_search_input' ).bind('focus keyup', function() {
		var sName = $( this ).attr( 'value' );
		if( sName.length > 2 ) {
			setTimeout( function() { 
				jQuery.post( window.location.href, { COMPANY_SEARCH_NAME : sName }, function( data ) {
					if( data.length > 1 ) {
						jQuery( '#company-search_search_results' ).show();
						jQuery( '#company-search_search_results' ).html( data );
					}
					else {
						jQuery( '#company-search_search_results' ).hide();
						jQuery( '#company-search_search_results' ).html( '' );
					}
				} );
			}, 100 );
		}
	} );
	
	jQuery( '#company-search_search_input' ).bind('keyup', function() {
		jQuery( '#company-search_search_result_id' ).attr( 'value', '' );
	} );
	
	jQuery( '#company-search_search_results a' ).live( 'click', function() {
		jQuery( '#company-search_search_result_id' ).attr( 'value', jQuery( this ).attr( 'rel' ) );
		jQuery( '#company-search_search_input' ).attr( 'value', jQuery( this ).text() );
		
		return false;
	} );
	
	jQuery( 'body' ).click( function() {
		jQuery( '#company-search_search_results' ).hide();
	} );
} );