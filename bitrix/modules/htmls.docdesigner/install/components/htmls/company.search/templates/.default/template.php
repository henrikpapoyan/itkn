<? if( !defined( "B_PROLOG_INCLUDED" ) || B_PROLOG_INCLUDED !== true ) die(); ?>
<?
$sTemplateDir		= $this->__folder;
$sComponentPath		= $this->__component->__path;
$COMPANY_ID = $_REQUEST['COMPANY_ID'];
//$COMPANY_NAME = htmlspecialchars($_REQUEST['COMPANY_NAME']);
$server=$GLOBALS['SESS_LAST_PROTOCOL'].$GLOBALS['SESS_LAST_HOST'];
?>
<script>
	/**
	 * ���� jQuery �� ���������, ���������� ���
	 */
	if( typeof jQuery == 'undefined' ) {
		document.write( '<script type="text/javascript" src="<?=$sTemplateDir?>/js/jquery-1.7.2.min.js"><\/script>' );
	}
</script>
<script type="text/javascript">
jQuery( document ).ready( function() {

	jQuery( '#company-search_search_results' ).css( 'left', ( jQuery( '#company-search_search_input' ).position().left + 2 ) );

	jQuery( '#company-search_search_input' ).bind('focus keyup', function() {
		var sName = $( this ).attr( 'value' );
		if( sName.length > 2 ) {
			setTimeout( function() {
				jQuery.post( '<?=$server?><?="/bitrix/admin/docdesigner_company_search.php"?>', { COMPANY_SEARCH_NAME : sName }, function( data ) {
					if( data.length > 1 ) {
						jQuery( '#company-search_search_results' ).show();
						jQuery( '#company-search_search_results' ).html( data );
					}
					else {
						jQuery( '#company-search_search_results' ).hide();
						jQuery( '#company-search_search_results' ).html( '' );
					}
				} );
			}, 100 );
		}
	} );

	jQuery( '#company-search_search_input' ).bind('keyup', function() {
		jQuery( '#company-search_search_result_id' ).attr( 'value', '' );
	} );

	jQuery( '#company-search_search_results a' ).live( 'click', function() {
		jQuery( '#company-search_search_result_id' ).attr( 'value', jQuery( this ).attr( 'rel' ) );
		jQuery( '#company-search_search_input' ).attr( 'value', jQuery( this ).text() );

		return false;
	} );

	jQuery( 'body' ).click( function() {
		jQuery( '#company-search_search_results' ).hide();
	} );
} );
</script>
<link rel="stylesheet" type="text/css" href="<?=$sTemplateDir?>/style.css"></link>

<input onblur="if (this.value=='') {this.value='<?=GetMessage("CS_INPUT_PLACEHOLDER")?>'; this.className='colortext';}" onclick="if (this.value=='<?=GetMessage("CS_INPUT_PLACEHOLDER")?>') {this.value=''; this.className='';}" type="text" name="COMPANY_NAME" size="30" id="company-search_search_input" value="<?=$COMPANY_NAME?>" class='colortext' />
<!--input placeholder='<?=GetMessage("CS_INPUT_PLACEHOLDER")?>' type="text" name="COMPANY_NAME" size="30" id="company-search_search_input" value="<?=$COMPANY_NAME?>" class='colortext' autocomplete="off" /-->
<ul id="company-search_search_results" class="company-search_search_results" style="display: none;"></ul>
<input id="company-search_search_result_id" type='hidden' name="COMPANY_ID" value="<?=$COMPANY_ID?>" />

