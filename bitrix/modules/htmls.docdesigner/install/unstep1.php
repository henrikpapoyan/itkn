<form action="<?echo $APPLICATION->GetCurPage()?>">
<?=bitrix_sessid_post()?>
	<input type="hidden" name="lang" value="<?echo LANG?>">
	<input type="hidden" name="id" value="htmls.docdesigner">
	<input type="hidden" name="uninstall" value="Y">
	<input type="hidden" name="step" value="2">
	<?echo CAdminMessage::ShowMessage(GetMessage("DD_UNINSTALL_WARNING"))?>
	<p><?echo GetMessage("DD_UNINSTALL_SAVEDATA")?></p>
	<p><input type="checkbox" name="removedata" id="removedata" value="Y" checked><label for="removedata"><?echo GetMessage("DD_UNINSTALL_SAVETABLE")?></label></p>
	<input type="submit" name="inst" value="<?echo GetMessage("MOD_UNINST_DEL")?>">
</form>