create table if not exists htmls_docdesigner_templates(
  id int(11) NOT NULL auto_increment,
  bizproc varchar(11) NOT NULL,
  template longtext NOT NULL,
  tmp_name varchar(40) NOT NULL,
  orientation varchar(1) NOT NULL,
  common varchar(1) NOT NULL,
  file_name_prefix varchar(40) NOT NULL,
  params longtext NOT NULL,
  PRIMARY KEY  (id)
);