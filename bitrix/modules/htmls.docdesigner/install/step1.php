<?if(!check_bitrix_sessid()) return;?>
<?
global $errors;
include(GetLangFileName(dirname(__FILE__).'/lang/', '/step1.php'));

if($errors===true):
	for($i=0; $i<count($errors); $i++)
		$alErrors .= $errors[$i]."<br>";
	echo CAdminMessage::ShowMessage(Array("TYPE"=>"ERROR", "MESSAGE" =>GetMessage("MOD_INST_ERR"), "DETAILS"=>$alErrors, "HTML"=>true));
endif;
?>
<form action="<?echo $APPLICATION->GetCurPage()?>">
	<?=bitrix_sessid_post()?>
	<p>
		<input type="hidden" name="lang" value="<?echo LANG?>">
		<input type="hidden" name="id" value="htmls.docdesigner">
		<input type="hidden" name="step" value="2">
		<?=CAdminMessage::ShowMessage(Array("TYPE"=>"WARNING", "MESSAGE" =>GetMessage("INSERT_SELF"), "HTML"=>true));?>
		<input type = "checkbox" id = "OwnEnt" value = "1" name = "create_OwnEnt" checked="checked"/>
		<label for = "cInit"><?=GetMessage("CREATE_OWN_ENTITIES")?></label><br/>
		<!--input type = "checkbox" id = "UserFields" value = "1" name = "create_UF" checked="checked"/>
		<label for = "cInit"><?=GetMessage("CREATE_USER_FIELDS")?></label><br/-->
        <input type = "checkbox" id = "urlrewrite" value = "1" name = "urlrewrite" checked="checked"/>
		<label for = "cInit"><?=GetMessage("CREATE_urlrewrite")?></label><br/>
		<input type="submit" name="install" value="<?echo GetMessage("MOD_NEXT_STEP")?>">
	</p>
<form>
