<?
$MESS["BPEMAILA_TASK_CHOOSE_FILES"] = "Выбор файлов для отправки";
$MESS['BPSEA_ACT_TRACK1'] = "Выбрать файлы для отправки должен #VAL#";
$MESS["BPEMAILA_TASK_CHOOSE_FILES_DESCR"] = "Укажите файлы, которые необходимо прикрепить к письму";
$MESS["BPEMAILA_TASK_SEND_FILE_YES"] = "Да";
$MESS["BPEMAILA_TASK_SEND_EMAIL_TO"] = "Получатель";
$MESS["BPEMAILA_TASK_EMAIL_SUBJECT"] = "Тема письма";
$MESS["BPEMAILA_TASK_EMAIL_MESSAGE"] = "Сообщение";
$MESS["BPEMAILA_ACT_BUTTON1"] = "Отправить";
$MESS["BPEMAILA_ACT_APPROVE_TRACK"] = "Пользователь #PERSON# отправил электронное письмо";
$MESS["BPEMAILA_TASK_SEND_FILE"] = "Отправить файл";
$MESS['BPEMAILA_ACT_CANCEL'] = 'Отменить';
?>