<?//8.2.1 07.06.2016
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
CModule::IncludeModule("crm");
class CBPHtmlsSendEmailActivity
	extends CBPActivity
	implements IBPEventActivity, IBPActivityExternalEventListener
{
    private $taskId = 0;
	private $isInEventActivityMode = false;

	public function __construct($name)
	{
		parent::__construct($name);
				$this->arProperties = array(
			"Title" => "",
			"Users" => null,
			"Name" => null,
			"Description" => null,
			"Parameters" => null,
			"OverdueDate" => null,
			"RequestedInformation" => null,
			"ResponcedInformation" => null,
			"Comments" => "",
			"TaskButtonMessage" => "",
			"CancelButtonMessage" => "",
			"CommentLabelMessage" => "",
			"ShowComment" => "Y",
			"StatusMessage" => "",
			"SetStatusMessage" => "Y",
			"SUBJECT" => "",
			"MESSAGE" => "",

		);
	}

	public function Execute()
	{
		if(CModule::IncludeModuleEx("htmls.docdesigner") < 3){
			if ($this->isInEventActivityMode)
				return CBPActivityExecutionStatus::Closed;

			$this->Subscribe($this);

			$this->isInEventActivityMode = false;
			return CBPActivityExecutionStatus::Executing;
		}
		else{
			return CBPActivityExecutionStatus::Closed;
		}
	}

	public function Subscribe(IBPActivityExternalEventListener $eventHandler)
	{
		global $USER;
		if ($eventHandler == null)
			throw new Exception("eventHandler");

		$this->isInEventActivityMode = true;

		$rootActivity = $this->GetRootActivity();
		$documentId = $rootActivity->GetDocumentId();

		$arUsersTmp = $rootActivity->TargetUser;
		if (!is_array($arUsersTmp))
			$arUsersTmp = array($arUsersTmp);

		$this->WriteToTrackingService(str_replace("#VAL#", "{=user:".implode("}, {=user:", $arUsersTmp)."}", GetMessage("BPSEA_ACT_TRACK1")));

		$arEmail = array();
		$DEAL_ID = intval(substr($documentId[2], strlen("DEAL_")));
		$COMPANY_ID = intval(substr($documentId[2], strlen("COMPANY_")));
		$CONTACT_ID = intval(substr($documentId[2], strlen("CONTACT_")));
		$LEAD_ID = intval(substr($documentId[2], strlen("LEAD_")));
		//get company id
		if($DEAL_ID > 0){
			$res = CCrmDeal::GetList(array(),array('ID' => $DEAL_ID),array());
			while($ob = $res->Fetch()){
				$COMPANY_ID = $ob['COMPANY_ID'];
				$CONTACT_ID = $ob['CONTACT_ID'];
			}
		}
		if($LEAD_ID > 0 and $COMPANY_ID == 0){
			$res = CCrmLead::GetList(array(),array('ID' => $LEAD_ID),array());
			while($ob = $res->Fetch()){
				$COMPANY_ID = $ob['COMPANY_ID'];
			}
		}


		$CompanyName = "";
		if($COMPANY_ID > 0){
			//get all contacts
			$res = CCrmContact::GetList($arOrder = array('DATE_CREATE' => 'DESC'), $arFilter = array('COMPANY_ID'=>$COMPANY_ID), $arSelect = array(), $nPageTop = false);
			while($ar = $res->Fetch()){
				$CompanyName = $ar['TITLE'];
				//get all contact email's
				$r = CCrmFieldMulti::GetList($arSort=array(), $arFilter=array('ENTITY_ID'=>'CONTACT', 'ELEMENT_ID' => $ar['ID'], 'TYPE_ID'=>'EMAIL'));
				while($a = $r->Fetch()){
					$arEmail[$a['VALUE']] = $ar['FULL_NAME'] . ' ('.$a['VALUE'].')';
				}
			}
			//get all company email's
			if($CompanyName == ""){
				$arCompany = CCrmCompany::GetByID($COMPANY_ID);
				$CompanyName = $arCompany['TITLE'];
			}
			$res = CCrmFieldMulti::GetList($arSort=array(), $arFilter=array('ENTITY_ID'=>'COMPANY', 'ELEMENT_ID' => $COMPANY_ID, 'TYPE_ID'=>'EMAIL'));
			while($ar = $res->Fetch()){
				$arEmail[$ar['VALUE']] = $CompanyName . ' ('.$ar['VALUE'].')';
			}
		}

        if($CONTACT_ID > 0){
			//get all contact email's
            $res = CCrmFieldMulti::GetList($arSort=array(), $arFilter=array('ENTITY_ID'=>'CONTACT', 'ELEMENT_ID' => $CONTACT_ID, 'TYPE_ID'=>'EMAIL'));
			while($ar = $res->Fetch()){
				$arEmail[$ar['VALUE']] = $CompanyName . ' ('.$ar['VALUE'].')';
			}
		}

        $arProperties = $rootActivity->arProperties;
        $arPropertiesTypes = $rootActivity->arPropertiesTypes;
        $bpFiles = array();
        foreach($arPropertiesTypes as $f => $fParams){
          if($fParams['Type'] == 'file'){
            if($fParams['Multiple'] == 1){
                $bpFiles = array_merge($bpFiles, $arProperties[$f]);
            }
            else{
                $bpFiles[] = $arProperties[$f];
            }
          }
        }


		$arUsers = CBPHelper::ExtractUsers($rootActivity->TargetUser, $documentId, false);
		$arParameters = $this->Parameters;
		if (!is_array($arParameters))
			$arParameters = array($arParameters);

		$runtime = CBPRuntime::GetRuntime();
		$documentService = $runtime->GetService("DocumentService");

		$arParameters["DOCUMENT_ID"] = $documentId;
		$arParameters["DOCUMENT_URL"] = $documentService->GetDocumentAdminPage($documentId);
		$arParameters["DOCUMENT_TYPE"] = $this->GetDocumentType();
		$arParameters["FIELD_TYPES"] = $documentService->GetDocumentFieldTypes($arParameters["DOCUMENT_TYPE"]);
		$arParameters["REQUEST"] = array();
		$arParameters["TaskButtonMessage"] = $this->IsPropertyExists("TaskButtonMessage") ? $this->TaskButtonMessage : GetMessage("BPRIA_ACT_BUTTON1");
		if (strlen($arParameters["TaskButtonMessage"]) <= 0)
			$arParameters["TaskButtonMessage"] = GetMessage("BPEMAILA_ACT_BUTTON1");

        //$arParameters["CancelButtonMessage"] = $this->IsPropertyExists("CancelButtonMessage") ? $this->TaskButtonMessage : GetMessage("BPRIA_ACT_BUTTON1");
		//if (strlen($arParameters["CancelButtonMessage"]) <= 0)
		$arParameters["CancelButtonMessage"] = GetMessage("BPEMAILA_ACT_CANCEL");

		$arParameters["CommentLabelMessage"] = $this->IsPropertyExists("CommentLabelMessage") ? $this->CommentLabelMessage : GetMessage("BPRIA_ACT_COMMENT");
		/*if (strlen($arParameters["CommentLabelMessage"]) <= 0)
			$arParameters["CommentLabelMessage"] = GetMessage("BPRIA_ACT_COMMENT");*/

			$arParameters["ShowComment"] = "N";

		/*$requestedInformation = $this->RequestedInformation;
		if ($requestedInformation && is_array($requestedInformation) && count($requestedInformation) > 0)
		{
			foreach ($requestedInformation as $v)
				$arParameters["REQUEST"][] = $v;
		}*/
		if($rootActivity->arProperties["DocDesignerEmail"]) $arEmail = array();
		if(count($arEmail) > 0){
			$arParameters["REQUEST"][] = array("Title" => GetMessage("BPEMAILA_TASK_SEND_EMAIL_TO"),
	                                               "Name" => "DocDesignerEmail",
	                                               "Type" => "select",
	                                               "Options" => $arEmail,
	                                               "Required" => 1,
	                                               "Multiple" => 1,
	                                               "Default_printable" =>$rootActivity->arProperties["DocDesignerEmail"],
	                                               "Default" =>$rootActivity->arProperties["DocDesignerEmail"]);
        }
        else{
        	$arParameters["REQUEST"][] = array("Title" => GetMessage("BPEMAILA_TASK_SEND_EMAIL_TO"),
	                                               "Name" => "DocDesignerEmail",
	                                               "Type" => "string",
	                                               "Required" => 1,
	                                               "Multiple" => 1,
	                                               "Default_printable" =>$rootActivity->arProperties["DocDesignerEmail"]);
        }

		$arParameters["REQUEST"][] = array("Title" => GetMessage("BPEMAILA_TASK_EMAIL_SUBJECT"),
                                               "Name" => "DocDesignerSubject",
                                               "Type" => "string",
                                               "Required" => 1,
                                               "Multiple" => 0,
                                               "Default_printable" =>$this->SUBJECT,
                                               "Default" =>$this->SUBJECT);
        if($arParameters["DOCUMENT_TYPE"][0] =='crm'){
			$arParameters["REQUEST"][] = array("Title" => GetMessage("BPEMAILA_TASK_EMAIL_MESSAGE"),
	                                               "Name" => "DocDesignerMessage",
	                                               "Type" => "text",
	                                               "Required" => 1,
	                                               "Multiple" => 0,
	                                               "Default_printable" =>$this->MESSAGE,
	                                               "Default" =>($this->MESSAGE));
        }
        else{
	  		$arParameters["REQUEST"][] = array("Title" => GetMessage("BPEMAILA_TASK_EMAIL_MESSAGE"),
	                                               "Name" => "DocDesignerMessage",
	                                               "Type" => "S:HTML",
	                                               "Required" => 1,
	                                               "Multiple" => 0,
	                                               "Default_printable" =>$this->MESSAGE,
	                                               "Default" =>array('TYPE'=>'html', 'TEXT'=>nl2br($this->MESSAGE)));
        }
        $arFiles = CDocDesignerContracts::GetContract($this->GetWorkflowInstanceId());

        foreach($bpFiles as $fid){
          $rsFile = CFile::GetByID($fid);
          $arFile = $rsFile->Fetch();
          $a[] = $arFile;
        }
        //$arFiles = array_merge($arFiles, $bpFiles)
		$tf = count($arFiles);
		$nf = 0;
        $from = COption::GetOptionString('crm', 'mail', '');
		foreach($arFiles as $arFile){
			$nf++;
			if($nf == $tf) 	$def = 'Y';
			else			$def = 'N';
            if($from == '')
			    $fid = $arFile['FILE_ID'];
            else
			    $fid = $arFile['WEB_DAV_ID'];
			$link = $arFile['LINK2REF'];
			$arParameters["REQUEST"][] = array("Title" => GetMessage("BPEMAILA_TASK_SEND_FILE") . "<br/>" . $link,
                                               "Name" => "FILE_ID_" . $fid,
                                               "Type" => ($arParameters["DOCUMENT_TYPE"][0] =='crm')? "bool" : "B",
                                               "Required" => 0,
                                               "Multiple" => 0,
                                               "Default_printable" =>GetMessage("BPEMAILA_TASK_SEND_FILE_YES"),
                                               "Default" => $def);
		}

        foreach($bpFiles as $fid){
          $rsFile = CFile::GetByID($fid);
          $arFile = $rsFile->Fetch();
          //$a[] = $arFile;
          $link = $arFile['ORIGINAL_NAME'];
          $arParameters["REQUEST"][] = array("Title" => GetMessage("BPEMAILA_TASK_SEND_FILE") . "<br/>" . $link,
                                               "Name" => "FILE_ID_" . $fid,
                                               "Type" => ($arParameters["DOCUMENT_TYPE"][0] =='crm')? "bool" : "B",
                                               "Required" => 0,
                                               "Multiple" => 0,
                                               "Default_printable" =>GetMessage("BPEMAILA_TASK_SEND_FILE_YES"),
                                               "Default" => 'N');
        }
        
		$taskService = $this->workflow->GetService("TaskService");
		$this->Name = GetMessage("BPEMAILA_TASK_CHOOSE_FILES");
		$this->Description = GetMessage("BPEMAILA_TASK_CHOOSE_FILES_DESCR");

		$this->taskId = $taskService->CreateTask(
			array(
				"USERS" => $arUsers,
				"WORKFLOW_ID" => $this->GetWorkflowInstanceId(),
				"ACTIVITY" => "RequestInformationActivity",
				"ACTIVITY_NAME" => $this->name,
				"OVERDUE_DATE" => $this->OverdueDate,
				"NAME" => $this->Name,
				"DESCRIPTION" => $this->Description,
				"PARAMETERS" => $arParameters,
			)
		);

		if (!$this->IsPropertyExists("SetStatusMessage") || $this->SetStatusMessage == "Y")
		{
			$message = ($this->IsPropertyExists("StatusMessage") && strlen($this->StatusMessage) > 0) ? $this->StatusMessage : GetMessage("BPRIA_ACT_INFO");
			$this->SetStatusTitle($message);
		}

		$this->workflow->AddEventHandler($this->name, $eventHandler);
	}

	public function Unsubscribe(IBPActivityExternalEventListener $eventHandler)
	{
		if ($eventHandler == null)
			throw new Exception("eventHandler");

		$taskService = $this->workflow->GetService("TaskService");
		$taskService->DeleteTask($this->taskId);

		$this->workflow->RemoveEventHandler($this->name, $eventHandler);

		$this->taskId = 0;
	}

	public function OnExternalEvent($arEventParameters = array())
	{
		global $USER;
		if ($this->executionStatus == CBPActivityExecutionStatus::Closed)
			return;
        //AddMessage2Log(print_r($arEventParameters, true), "htmls.DocDesigner");
        CModule::IncludeModule("htmls.docdesigner");
		$rootActivity = $this->GetRootActivity();
		$documentId = $rootActivity->GetDocumentId();

		$runtime = CBPRuntime::GetRuntime();
		$documentService = $runtime->GetService("DocumentService");
		$arUsers = CBPHelper::ExtractUsers($rootActivity->arProperties["TargetUser"], $this->GetDocumentId(), false);

		$arEventParameters["USER_ID"] = $arUsers[0];//intval($arEventParameters["USER_ID"]);

        $DEAL_ID = intval(substr($documentId[2], strlen("DEAL_")));
		$COMPANY_ID = intval(substr($documentId[2], strlen("COMPANY_")));
		$CONTACT_ID = intval(substr($documentId[2], strlen("CONTACT_")));
		$LEAD_ID = intval(substr($documentId[2], strlen("LEAD_")));
		//get company id
		if($DEAL_ID > 0){
			$res = CCrmDeal::GetList(array(),array('ID' => $DEAL_ID),array());
			while($ob = $res->Fetch()){
				$COMPANY_ID = $ob['COMPANY_ID'];
				$CONTACT_ID = $ob['CONTACT_ID'];
			}
		}


		$taskService = $this->workflow->GetService("TaskService");
        $from = COption::GetOptionString('crm', 'mail', '');
		$arFiles = CDocDesignerContracts::GetContract($this->GetWorkflowInstanceId());
		foreach($arFiles as $arFile){
		  //if($from == '')
		  //	$fid = $arFile['FILE_ID'];
          //else
			$fid = $arFile['WEB_DAV_ID'];

        	if($arEventParameters["RESPONCE"]["FILE_ID_" . $fid] == "Y"){
        		if($from == '')
        			$arAttach[] = $arFile['FILE_ID'];
                else
        			$arAttach[] = $arFile['WEB_DAV_ID'];

        	}
		}

		$arProperties = $rootActivity->arProperties;
		$arPropertiesTypes = $rootActivity->arPropertiesTypes;

        $bpFiles = array();
        foreach($arPropertiesTypes as $f => $fParams){
          if($fParams['Type'] == 'file'){
            if($fParams['Multiple'] == 1){
                $bpFiles = array_merge($bpFiles, $arProperties[$f]);
            }
            else{
                $bpFiles[] = $arProperties[$f];
            }
          }
        }
        foreach($bpFiles as $fid){
			//$fid = $arFile['FILE_ID'];
			if($arEventParameters["RESPONCE"]["FILE_ID_" . $fid] == "Y"){
				$arAttach[] = $fid;
			}
		}

		$arProperties["DocDesignerEmail"] = $arEventParameters["RESPONCE"]["DocDesignerEmail"];
		$arProperties["DocDesignerSubject"] = $arEventParameters["RESPONCE"]["DocDesignerSubject"];
		$arProperties["DocDesignerMessage"] = $arEventParameters["RESPONCE"]["DocDesignerMessage"];
		$arProperties["InstanceId"] = $this->GetWorkflowInstanceId();
        if($CONTACT_ID > 0){
          $arProperties['BINDINGS']['CONTACT'] = array("OWNER_ID"=> $CONTACT_ID,"OWNER_TYPE_ID"=> 3);
        }
        if($COMPANY_ID > 0){
          $arProperties['BINDINGS']['COMPANY'] = array("OWNER_ID"=> $COMPANY_ID,"OWNER_TYPE_ID"=> 4);
        }
		$arProperties["InstanceId"] = $this->GetWorkflowInstanceId();
		CDocDesignerBPActivities::SendEmail($rootActivity, $arProperties, $arPropertiesTypes, $arAttach);

		$taskService->MarkCompleted($this->taskId, $arEventParameters["USER_ID"]);

		$this->Comments = $arEventParameters["COMMENT"];

		$this->WriteToTrackingService(
			str_replace(
				array("#PERSON#", "#COMMENT#"),
				array("{=user:user_".$arEventParameters["USER_ID"]."}", (strlen($arEventParameters["COMMENT"]) > 0 ? ": ".$arEventParameters["COMMENT"] : "")),
				GetMessage("BPEMAILA_ACT_APPROVE_TRACK")
			),
			$arEventParameters["USER_ID"]
		);

		$this->ResponcedInformation = $arEventParameters["RESPONCE"];
		//$rootActivity->SetVariablesTypes($arEventParameters["RESPONCE_TYPES"]);
		$rootActivity->SetVariables($arEventParameters["RESPONCE"]);

		$this->Unsubscribe($this);

		$this->workflow->CloseActivity($this);
	}

	protected function OnEvent(CBPActivity $sender)
	{
		$sender->RemoveStatusChangeHandler(self::ClosedEvent, $this);
		$this->workflow->CloseActivity($this);
	}

	public function HandleFault(Exception $exception)
	{
		if ($exception == null)
			throw new Exception("exception");

		$status = $this->Cancel();
		if ($status == CBPActivityExecutionStatus::Canceling)
			return CBPActivityExecutionStatus::Faulting;

		return $status;
	}

	public function Cancel()
	{
		if (!$this->isInEventActivityMode && $this->taskId > 0)
			$this->Unsubscribe($this);

		return CBPActivityExecutionStatus::Closed;
	}

	/*public static function ValidateProperties($arTestProperties = array(), CBPWorkflowTemplateUser $user = null)
	{
	}*/

	public static function GetPropertiesDialog($documentType, $activityName, $arWorkflowTemplate, $arWorkflowParameters, $arWorkflowVariables, $arCurrentValues = null, $formName = "", $popupWindow = null)
	{
		$runtime = CBPRuntime::GetRuntime();

		if (!is_array($arWorkflowParameters))
			$arWorkflowParameters = array();
		if (!is_array($arWorkflowVariables))
			$arWorkflowVariables = array();

		if (!is_array($arCurrentValues))
		{
			$arCurrentValues = array("doc2mail_subject" => "", "doc2mail_message" => "");

			$arCurrentActivity = &CBPWorkflowTemplateLoader::FindActivityByName($arWorkflowTemplate, $activityName);
			if (is_array($arCurrentActivity["Properties"])){
				$arCurrentValues["doc2mail_subject"] = $arCurrentActivity["Properties"]["SUBJECT"];
				$arCurrentValues["doc2mail_message"] = $arCurrentActivity["Properties"]["MESSAGE"];
			}
		}

		return $runtime->ExecuteResourceFile(
			__FILE__,
			"properties_dialog.php",
			array(
				"arCurrentValues" => $arCurrentValues,
				"formName" => $formName,
			)
		);
	}

	public static function GetPropertiesDialogValues($documentType, $activityName, &$arWorkflowTemplate, &$arWorkflowParameters, &$arWorkflowVariables, $arCurrentValues, &$arErrors)
	{
		$arErrors = array();

		$runtime = CBPRuntime::GetRuntime();

		$arProperties = array("SUBJECT" => $arCurrentValues["doc2mail_subject"], "MESSAGE" => $arCurrentValues["doc2mail_message"]);

		$arErrors = self::ValidateProperties($arProperties, new CBPWorkflowTemplateUser(CBPWorkflowTemplateUser::CurrentUser));
		if (count($arErrors) > 0)
			return false;

		$arCurrentActivity = &CBPWorkflowTemplateLoader::FindActivityByName($arWorkflowTemplate, $activityName);
		$arCurrentActivity["Properties"] = $arProperties;

		return true;
	}
}
?>