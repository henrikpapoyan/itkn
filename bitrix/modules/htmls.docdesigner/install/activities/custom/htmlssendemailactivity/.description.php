<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arActivityDescription = array(
	"NAME" => GetMessage("BPEMAILA_DESCR_NAME"),
	"DESCRIPTION" => GetMessage("BPEMAILA_DESCR_DESCR"),
	"TYPE" => "activity",
	"CLASS" => "HtmlsSendEmailActivity",
	"JSCLASS" => "BizProcActivity",
	"CATEGORY" => array(
		"ID" => "other",
	),
);
?>
