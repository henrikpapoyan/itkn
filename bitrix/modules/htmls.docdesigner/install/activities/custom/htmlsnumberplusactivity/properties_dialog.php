<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>

<tr>
	<td align="right" width="40%"><?= GetMessage("BPDOCXA_LINK") ?>:</td>
	<td width="60%">
		<input type="text" size="50" name="document_link" id="id_document_link" value="<?=$arCurrentValues['document_link']?>"/>
        <input type="button" value="..." onclick="BPAShowSelector('id_document_link', 'string');">
	</td>
</tr>

<script>
$(document).ready(function () {
    $('.bx-core-adm-dialog-head-block').append('<img src="/bitrix/images/htmls.docdesigner/help.png" style="float: right;" onclick="window.open(\'https://www.htmls.pw/learning/course/index.php?COURSE_ID=2&LESSON_ID=80&LESSON_PATH=60.77.80\');" />');
});
</script>