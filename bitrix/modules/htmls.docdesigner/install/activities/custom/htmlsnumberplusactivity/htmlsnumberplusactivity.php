<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
//define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/bitrix/tmp/DocDesigner.txt");
class CBPHtmlsNumberPlusActivity
	extends CBPActivity
{
	public function __construct($name)
	{
		parent::__construct($name);
		$this->arProperties = array("Title" => "", 'document_link'=> '');
	}

	public function Execute(){
	  global $USER, $DB;
		$r = CModule::IncludeModuleEx("htmls.docdesigner");
		if(intval($r) < 3){
			$rootActivity = $this->GetRootActivity();
			$workflowId = $this->workflow->GetInstanceId();
            //AddMessage2Log('document_link='.$this->document_link, "htmls.DocDesigner");
            //AddMessage2Log(print_r($this, true), "htmls.DocDesigner");
			if($rootActivity->arProperties['DocDesignerAutoNumber'] == 'N'){
				$CContract = new CDocDesignerContracts();
				if($path = $CContract->GetPath2Contract($workflowId, $this->document_link)){
				  //AddMessage2Log(print_r($path, true), "htmls.DocDesigner");
					CModule::IncludeModule("iblock");
					$CDocDes = new CDocDesignerProcessing();

					$REG_ID = $path['reg_id'];
					$FileID = $path['file_id'];
                    $fileName = $path['fileName'];
                    $diskId = $path['diskId'];
					$path = $path['path'];
					$Numerator = intval($rootActivity->arProperties["DOCXNumerator"]);
					//$arNum = $CContract->GetLastContractNumber($rootActivity->arProperties['DocDesignerNumberPrefix']);
					$arNum = $CDocDes->GetNumeratorInfo($Numerator, $rootActivity->arProperties['DocDesignerNumberPrefix'], $rootActivity->arProperties['DocDesignerCompany'], $rootActivity->DocDesignerCompanyContract);
					$arNum['PREFIX'] = $rootActivity->arProperties['DocDesignerNumberPrefix'];

					$Word = new PHPWord();
					$l = $arNum['LENGTH'];//COption::GetOptionString("htmls.docdesigner", 'DOCDESIGNER_CONTRACTS_LENGTH_NUMBER', 3);
					if(intval($l) == 0){
						$l = 3;
					}
					$tmp = '';
					for($i=1;$i<=$l;$i++){
						$tmp .= '0';
					}

					if(strpos($path, '://') > 0){//clouds
						copy($path, $_SERVER["DOCUMENT_ROOT"].'/bitrix/tmp/template.docx');
						$path = '/bitrix/tmp/template.docx';
					}
					$document = $Word->loadTemplate($_SERVER["DOCUMENT_ROOT"] . $path);

					//new number
					$snum = str_replace("{PREFIX}", $rootActivity->arProperties['DocDesignerNumberPrefix'], $arNum['TEMPLATE']);
					$snum = str_replace("{YEAR}", $arNum['YEAR'], $snum);
					$snum = str_replace("{NUMBER}", $arNum['SNUMBER'], $snum);
                    $snum = str_replace("{MONTH}", date("m"), $snum);
					//old number
					$tnum = str_replace("{PREFIX}", $rootActivity->arProperties['DocDesignerNumberPrefix'], $arNum['TEMPLATE']);
					$tnum = str_replace("{YEAR}", $arNum['YEAR'], $tnum);
					$tnum = str_replace("{NUMBER}", $tmp, $tnum);
                    $tnum = str_replace("{MONTH}", date("m"), $tnum);

					$document->setValue('${'.$CDocDes->win2uni($tnum).'}', $CDocDes->win2uni($snum));
					$fname = CUtil::translit($snum, 'ru', array("change_case" => false, "replace_other" => '-')).'.docx';
					$fname = $_SERVER["DOCUMENT_ROOT"] . '/bitrix/tmp/htmls.docdesigner/'.$fname;
					$document->save($fname);

                    $IBLOCK_ID = COption::GetOptionInt("htmls.docdesigner", 'DOCDESIGNER_DOCS_REGISTER_ID');

                    $discConverted = \Bitrix\Main\Config\Option::get('disk', 'successfully_converted', false);
                    if($discConverted == 'Y'){
                      $Name = str_replace($tnum, $snum, $arFields['NAME']);
                      CModule::IncludeModule('disk');

                      $file = \Bitrix\Disk\File::loadById($diskId, array('STORAGE'));
                      $bn = basename($fname);
                      $abn = str_replace('.docx', '', $bn);
                      $fileArray = \CFile::makeFileArray($fname);
                      $fileArray['name'] = $abn.'_'.$REG_ID.'.docx';
                      $fileArray['NAME'] = $abn.'_'.$REG_ID.'.docx';
                      $fileArray['type'] = $file->getTypeFile();
                      $fileArray['MODULE_ID'] = \Bitrix\Disk\Driver::INTERNAL_MODULE_ID;
                      $fileId = \CFile::saveFile($fileArray, $fileArray['MODULE_ID']);
                      if($fileId){
                        $el = new CIBlockElement;
    					$el->Update($REG_ID, array('NAME' => $snum));
                        CIBlockElement::SetPropertyValuesEx($REG_ID, $IBLOCK_ID, array('NUMBER' => $arNum['NUMBER'], 'LINK2FILE' => $fileId));

                        $versionModel = $file->addVersion(array(
                            'ID' => $fileId,
                            'FILE_SIZE' => $fileArray['size'],
                        ), $USER->getId(), true);

                        $file->rename($fileArray['NAME']);
                      }
                      //die();
                    }
                    else{
    					$el = new CIBlockElement;
    					$el->Update($REG_ID, array('NAME' => $snum));
    					CIBlockElement::SetPropertyValuesEx($REG_ID, $IBLOCK_ID, array('NUMBER' => $arNum['NUMBER']));//, 'STR_NUMBER' => $arNum['STR_NUMBER']
						$LIBRARY_ID = COption::GetOptionString("htmls.docdesigner", "DOCDESIGNER_CONTRACTS_PATH2LIBRARY");
						$arFile = CFile::MakeFileArray($fname);
						$arFile["type"] = 'docx';
						CIBlockElement::SetPropertyValuesEx($FileID, $LIBRARY_ID, array('FILE' => $arFile));
						$arSelect = array('ID', 'NAME');
						$arFilter = Array("ID"=>IntVal($FileID));
						$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
						while($ob = $res->GetNextElement()){
							$arFields = $ob->GetFields();
							$Name = str_replace($tnum, $snum, $arFields['NAME']);
							$el->Update($FileID, array('NAME' => $Name));
						}
                    }
                    //AddMessage2Log($fname, "htmls.DocDesigner");
					@unlink($fname);
				}
			}
		}
        //AddMessage2Log(CBPActivityExecutionStatus::Closed, "htmls.DocDesigner");
		return CBPActivityExecutionStatus::Closed;
	}

	/*public static function ValidateProperties($arTestProperties = array(), CBPWorkflowTemplateUser $user = null)
	{
	}*/

	public static function GetPropertiesDialog($documentType, $activityName, $arWorkflowTemplate, $arWorkflowParameters, $arWorkflowVariables, $arCurrentValues = null, $formName = "")
	{
	  $runtime = CBPRuntime::GetRuntime();

		if (!is_array($arWorkflowParameters))
			$arWorkflowParameters = array();
		if (!is_array($arWorkflowVariables))
			$arWorkflowVariables = array();

		if (!is_array($arCurrentValues))
		{
			$arCurrentValues = array("document_link" => "");

			$arCurrentActivity = &CBPWorkflowTemplateLoader::FindActivityByName($arWorkflowTemplate, $activityName);
			if (is_array($arCurrentActivity["Properties"])){
				$arCurrentValues["document_link"] = $arCurrentActivity["Properties"]["document_link"];
			}
		}

		return $runtime->ExecuteResourceFile(
			__FILE__,
			"properties_dialog.php",
			array(
				"arCurrentValues" => $arCurrentValues,
				"formName" => $formName,
				"documentType" => $documentType,
			)
		);
	}

	public static function GetPropertiesDialogValues($documentType, $activityName, &$arWorkflowTemplate, &$arWorkflowParameters, &$arWorkflowVariables, $arCurrentValues, &$arErrors)
	{
		$arErrors = array();

		$runtime = CBPRuntime::GetRuntime();

		$arProperties = array("document_link" => $arCurrentValues["document_link"]);

		$arErrors = self::ValidateProperties($arProperties, new CBPWorkflowTemplateUser(CBPWorkflowTemplateUser::CurrentUser));
		if (count($arErrors) > 0)
			return false;

		$arCurrentActivity = &CBPWorkflowTemplateLoader::FindActivityByName($arWorkflowTemplate, $activityName);
		$arCurrentActivity["Properties"] = $arProperties;

		return true;
	}
}
?>