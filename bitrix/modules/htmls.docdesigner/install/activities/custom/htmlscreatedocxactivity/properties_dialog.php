<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

CJSCore::Init(array('jquery'));
?>

<?if(CModule::IncludeModuleEx("htmls.docdesigner") < 3){
  $discConverted = \Bitrix\Main\Config\Option::get('disk', 'successfully_converted', false);
  if($discConverted == 'Y'){
    CModule::IncludeModule('disk');
    CModule::IncludeModule('socialnetwork');
    $cDoc = new CDocDesignerContracts;
    $diskTree = $cDoc->getDisks();

    $sections = '<select id="DocumentDisk" name="DocumentDisk[]" onchange="getSections(this.value);">';//onchange="getSections(this.value);" 
    $sections .= '<option value="0">---</option>';
    foreach($diskTree as $id => $aDisk){
      if($aDisk['tree']){//deprecated
        $sections .= '<optgroup label="' . GetMessage('BPDSA_DISK') . $aDisk['name'] . '">';
        if($arCurrentValues['DocumentSection'] == $aDisk['root'])
            $sections .= '<option selected value="' . $aDisk['root'] . '">.' . $aDisk['name'] . '</option>';
        else
            $sections .= '<option value="' . $aDisk['root'] . '">.' . $aDisk['name'] . '</option>';
        $sections .= $cDoc->getSelect($aDisk['tree'], '', $arCurrentValues['DocumentSection']);
        $sections .= '</optgroup>';
      }
      else{
        if($arCurrentValues['DocumentDisk'] == $aDisk['root'])
            $sections .= '<option selected value="' . $aDisk['root'] . '">' . GetMessage('BPDSA_DISK') . $aDisk['name'] . '</option>';
        else
            $sections .= '<option value="' . $aDisk['root'] . '">' . GetMessage('BPDSA_DISK') . $aDisk['name'] . '</option>';
      }
    }
    $tmpSelect = $cDoc->getTmpList($arCurrentValues['docxTemplate']);
  }//if($discConverted == 'Y')
	CModule::IncludeModule("iblock");
	require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/iblock/admin_tools.php");
	$CDocDesBPA = new CDocDesignerBPActivities;
	$arResult = $CDocDesBPA->GetNumeratorsList();
	if(count($arResult) > 0){
		$select = '<select name="docx_numerator" id="id_docx_numerator">';
		foreach($arResult as $id => $name){
			if($arCurrentValues['docx_numerator'] == $id){
				$select .= "<option value='" . $id . "' selected>" . $name . "</option>";
			}
			else{
				$select .= "<option value='" . $id . "'>" . $name . "</option>";
			}
		}
		$select .= '</select>';
	}
	else{
		$select = GetMessage("BPDOCXA_NO_NUMERATORS");
	}
	$LIBRARY_ID = COption::GetOptionString("htmls.docdesigner", "DOCDESIGNER_CONTRACTS_PATH2LIBRARY");

}

//$tmpSelect = $cDoc->getTmpList($arCurrentValues['docxTemplate']);
//echo $tmpSelect;
?>
<tr>
	<td align="right" width="40%"><span style="color:#FF0000;">*</span> <?= GetMessage("BPDOCXA_TEMPLATE_SELECT") ?>:</td>
	<td width="60%">
		<?=$tmpSelect?>
	</td>
</tr>
<tr>
	<td align="right" width="40%"><?= GetMessage("BPDOCXA_NUMERATOR_SELECT") ?>:</td>
	<td width="60%">
		<?=$select?>
	</td>
</tr>
<!--tr><td colspan="2">&nbsp;</td></tr-->
<tr>
	<td align="right" width="40%"><?= GetMessage("BPDSA_DOCUMENT_DISK") ?>:</td>
	<td width="60%">
		<?=$sections?>
	</td>
</tr>
<tr>
	<td align="right" width="40%"><?= GetMessage("BPDOCXA_DOCUMENT_SECTION") ?>:</td>
	<td width="60%">
		<?
        if($discConverted == 'Y'){
          echo '<div id="diskSections"></div>';//$sections;
        }
        else{
          $prop_fields = array("LINK_IBLOCK_ID" => $LIBRARY_ID, "MULTIPLE_CNT" => 0, "MULTIPLE" => 'N', "VALUE" => $arCurrentValues['DocumentSection']);
			_ShowGroupPropertyField('DocumentSection', $prop_fields, array($prop_fields["VALUE"]));
        }
        ?>
	</td>
</tr>
<script>
function getTree(data, mov){
  mov = mov + '--';
  select = '';
  for(var sid in data){
      if(currentSection == sid)
          select += '<option value="'+sid+'" selected>'+mov+'>'+data[sid]['name']+'</option>';
      else
          select += '<option value="'+sid+'">'+mov+'>'+data[sid]['name']+'</option>';

      if(tree = data[sid]['tree']){
        select += getTree(tree, mov);
      }
  }
  return select;

}
function getSections(sid){
    currentSection = '<?=$arCurrentValues["DocumentSection"]?>';
    arParam = {sectionId: sid};
    $.post("/bitrix/admin/docdesigner_ajax.php", arParam, function(data){
        select = '<select name="DocumentSection[]">';
        for(var sid in data){
            if(currentSection == sid)
                select += '<option value="'+sid+'" selected>'+data[sid]['name']+'</option>';
            else
                select += '<option value="'+sid+'">'+data[sid]['name']+'</option>';

            if(tree = data[sid]['tree']){
              select += getTree(tree, '');
            }
        }
        select += '</select>';
        $('#diskSections').html(select);
    }, "json");
}

$(document).ready(function () {
    did = $('#DocumentDisk').val();
    if(did > 0){
        getSections(did);
    }
    $('.bx-core-adm-dialog-head-block').append('<img src="/bitrix/images/htmls.docdesigner/help.png" style="float: right;" onclick="window.open(\'https://www.htmls.pw/learning/course/index.php?COURSE_ID=2&LESSON_ID=79&LESSON_PATH=60.77.79\');" />');
});
</script>