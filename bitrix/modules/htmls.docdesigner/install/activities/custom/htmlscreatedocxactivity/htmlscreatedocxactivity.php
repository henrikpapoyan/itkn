<?//v8.2.1 07.06.2016
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
//define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/bitrix/tmp/DocDesigner.txt");
class CBPHtmlsCreateDocxActivity
	extends CBPActivity
{
	public function __construct($name)
	{
		parent::__construct($name);
		$this->arProperties = array("Title" => "", "DOCXNumerator" => "", "DocumentSection" => "",
            'DocNumber' => '', 'DocDate' => '', 'FileID'=>'', 'docxTemplate' => '', 'DocumentDisk' => '',
            'link2disk' => '', 'link2download' => '', 'diskID'=>'', 'url2disk' => '', 'url2download' => '');//, "DOCXProject" => "", "DOCXAutoNumber" => "");
	}

	public function Execute()
	{
		if(CModule::IncludeModuleEx("htmls.docdesigner") < 3){
        	$rootActivity = $this->GetRootActivity();
        	$Numerator = $this->DOCXNumerator;
        	if(empty($Numerator)){
        		$this->DOCXNumerator = COption::GetOptionInt("htmls.docdesigner", 'DOCDESIGNER_NUM_CONTRACT_ID');
        	}
        	if(!isset($rootActivity->arProperties["DOCXNumerator"])){
				$rootActivity->arProperties["DOCXNumerator"] = $this->DOCXNumerator;
        	}

        	$arProperties = $rootActivity->arProperties;
			$arPropertiesTypes = $rootActivity->arPropertiesTypes;
			$workflowId = $this->workflow->GetInstanceId();
			if($this->DocumentSection > 0){
				$arProperties['DocumentSection'] = $this->DocumentSection;
			}
            if($this->docxTemplate > 0){
				$arProperties['DocDesignerDogTemplateID'] = $this->docxTemplate;
				$arProperties['_DocDesignerTemplateFromList'] = 1;
			}
            else{
              $arProperties['_DocDesignerTemplateFromList'] = 0;
            }
			$DocDes = new CDocDesignerBPActivities();
			$arResult = $DocDes->CreateDocx($rootActivity, $arProperties, $arPropertiesTypes, $workflowId);//, $this->DOCXAutoNumber, $this->DOCXProject);
            //AddMessage2Log(print_r($arResult, 1));
            $this->DocNumber = $arResult['DocNumber'];
            $this->DocDate = $arResult['DocDate'];
            $this->FileID = $arResult['FileID']['ID'];
            foreach($arResult['links'] as $link){
              if($link['arFile']['FILE_ID'] == $this->FileID){
                $this->diskID = $link['arFile']['REAL_OBJECT_ID'];
              }
              $this->link2disk .= '[url=' . $link['link2disk'] . ']' . $link['NAME'] . '[/url] | ';
              $this->url2disk .= $link['link2disk'];
              $this->link2download .= '[url=' . $link['link2download'] . ']' . $link['NAME'] . '[/url] | ';
              $this->url2download .= $link['link2download'];

            }
		}
		return CBPActivityExecutionStatus::Closed;
	}

	public static function ValidateProperties($arTestProperties = array(), CBPWorkflowTemplateUser $user = null)
	{
		$arErrors = array();

		/*if (strlen($arTestProperties["PDFTemplate"]) <= 0)
		{
			$arErrors[] = array(
				"code" => "emptyTemplate",
				"message" => GetMessage("BPPDFA_EMPTY_TEMPLATE"),
			);
		}*/
		return array_merge($arErrors, parent::ValidateProperties($arTestProperties, $user));
	}

	public static function GetPropertiesDialog($documentType, $activityName, $arWorkflowTemplate, $arWorkflowParameters, $arWorkflowVariables, $arCurrentValues = null, $formName = "")
	{
		$runtime = CBPRuntime::GetRuntime();

		if (!is_array($arWorkflowParameters))
			$arWorkflowParameters = array();
		if (!is_array($arWorkflowVariables))
			$arWorkflowVariables = array();

		if (!is_array($arCurrentValues))
		{
			$arCurrentValues = array("docx_numerator" => "", "DocumentSection" => "",
                                    'docxTemplate' => '', 'DocumentDisk' => '');

			$arCurrentActivity = &CBPWorkflowTemplateLoader::FindActivityByName($arWorkflowTemplate, $activityName);
			if (is_array($arCurrentActivity["Properties"])){
				$arCurrentValues["docx_numerator"] = $arCurrentActivity["Properties"]["DOCXNumerator"];
				$arCurrentValues["DocumentSection"] = $arCurrentActivity["Properties"]["DocumentSection"];
				$arCurrentValues["DocumentDisk"] = $arCurrentActivity["Properties"]["DocumentDisk"];
				$arCurrentValues["docxTemplate"] = $arCurrentActivity["Properties"]["docxTemplate"];
			}
		}

		return $runtime->ExecuteResourceFile(
			__FILE__,
			"properties_dialog.php",
			array(
				"arCurrentValues" => $arCurrentValues,
				"formName" => $formName,
			)
		);
	}

	public static function GetPropertiesDialogValues($documentType, $activityName, &$arWorkflowTemplate, &$arWorkflowParameters, &$arWorkflowVariables, $arCurrentValues, &$arErrors)
	{
		$arErrors = array();

		$runtime = CBPRuntime::GetRuntime();

		$arProperties = array("DOCXNumerator" => $arCurrentValues["docx_numerator"],
        "DocumentSection" => $arCurrentValues["DocumentSection"][0],
        "DocumentDisk" => $arCurrentValues["DocumentDisk"][0],
        "docxTemplate" => $arCurrentValues["docxTemplate"]
        );

		$arErrors = self::ValidateProperties($arProperties, new CBPWorkflowTemplateUser(CBPWorkflowTemplateUser::CurrentUser));
		if (count($arErrors) > 0)
			return false;

		$arCurrentActivity = &CBPWorkflowTemplateLoader::FindActivityByName($arWorkflowTemplate, $activityName);
		$arCurrentActivity["Properties"] = $arProperties;

		return true;
	}
}
?>