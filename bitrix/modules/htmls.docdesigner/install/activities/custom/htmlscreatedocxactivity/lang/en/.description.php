<?
$MESS ['BPDOCXA_DESCR_DESCR'] = "DocDesigner: Generate DOCX-document";
$MESS ['BPDOCXA_DESCR_NAME'] = "DD: Generate DOCX";
$MESS ['BPDOCXA_DocNumber_RU'] = "DOCX: doc's number";
$MESS ['BPDOCXA_DocDate_RU'] = "DOCX: doc's date";
$MESS['BPDOCXA_FileID_RU'] = 'DOCX: file\'s ID';
$MESS['BPDOCXA_DiskID_RU'] = 'DOCX: disk ID';
$MESS['BPDOCXA_link2disk_RU'] = 'DOCX: Link to disk';
$MESS['BPDOCXA_url2disk_RU'] = 'DOCX: URL to disk';
$MESS['BPDOCXA_link2download_RU'] = 'DOCX: Download link';
$MESS['BPDOCXA_link2urldownload_RU'] = 'DOCX: URL link';
?>