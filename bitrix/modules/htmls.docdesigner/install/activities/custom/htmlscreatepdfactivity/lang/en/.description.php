<?
$MESS ['BPPDFA_DESCR_DESCR'] = "DocDesigner: Generate PDF / DOCX";
$MESS ['BPPDFA_DESCR_NAME'] = "DD: Generate PDF / DOCX";
$MESS ['BPPDFA_DocNumber_RU'] = "PDF/DOCX: doc's number";
$MESS ['BPPDFA_DocDate_RU'] = "PDF/DOCX: doc's date";
$MESS['BPPDFA_FileID_RU'] = 'PDF/DOCX: file\'s ID';
$MESS['BPPDFA_OriginalID_RU'] = 'PDF original: file\'s ID';
$MESS['BPPDFA_link2disk_RU'] = 'PDF/DOCX: Link to disk';
$MESS['BPPDFA_url2disk_RU'] = 'PDF/DOCX: URL to disk';
$MESS['BPPDFA_link2download_RU'] = 'PDF/DOCX: Download link';
$MESS['BPPDFA_url2download_RU'] = 'PDF/DOCX: URL link';
$MESS['BPPDFA_DiskID_RU'] = 'PDF/DOCX: disk ID';
$MESS['BPPDFA_DiskOriginalID_RU'] = 'PDF original: disk ID';
?>