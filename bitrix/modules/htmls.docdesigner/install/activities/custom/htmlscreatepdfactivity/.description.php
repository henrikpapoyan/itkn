<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arActivityDescription = array(
	"NAME" => GetMessage("BPPDFA_DESCR_NAME"),
	"DESCRIPTION" => GetMessage("BPPDFA_DESCR_DESCR"),
	"TYPE" => "activity",
	"CLASS" => "HtmlsCreatePdfActivity",
	"JSCLASS" => "BizProcActivity",
	"CATEGORY" => array(
		"ID" => "other",
	),
    "RETURN" => array(
		"DocNumber" => array(
			"NAME" => GetMessage("BPPDFA_DocNumber_RU"),
			"TYPE" => "string",
		),
        "DocDate" => array(
			"NAME" => GetMessage("BPPDFA_DocDate_RU"),
			"TYPE" => "string",
		),
        "FileID" => array(
			"NAME" => GetMessage("BPPDFA_FileID_RU"),
			"TYPE" => "string",
		),
        "OriginalID" => array(
			"NAME" => GetMessage("BPPDFA_OriginalID_RU"),
			"TYPE" => "string",
		),
        "diskID" => array(
			"NAME" => GetMessage("BPPDFA_DiskID_RU"),
			"TYPE" => "string",
		),
        "diskOriginalID" => array(
			"NAME" => GetMessage("BPPDFA_DiskOriginalID_RU"),
			"TYPE" => "string",
		),
        "link2disk" => array(
			"NAME" => GetMessage("BPPDFA_link2disk_RU"),
			"TYPE" => "array",
		),
        "link2download" => array(
			"NAME" => GetMessage("BPPDFA_link2download_RU"),
			"TYPE" => "array",
		),
        "url2disk" => array(
			"NAME" => GetMessage("BPPDFA_url2disk_RU"),
			"TYPE" => "array",
		),
        "url2download" => array(
			"NAME" => GetMessage("BPPDFA_url2download_RU"),
			"TYPE" => "array",
		)
    )
);
?>
