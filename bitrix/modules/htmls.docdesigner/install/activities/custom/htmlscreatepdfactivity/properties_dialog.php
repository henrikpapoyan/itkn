<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<?
//echo $arCurrentValues['DocumentSection'];
$actPath = explode('/', substr($_SERVER['HTTP_REFERER'], strlen('http://'.$_SERVER["HTTP_HOST"])));
$TemplateID = $actPath[count($actPath) - 2];
//echo   $TemplateID;
//print_r($documentType);
if($documentType[0] == 'bizproc'){
	$TemplateID = substr($documentType[2], 5);
}
elseif($documentType[0] == 'lists'){
	$TemplateID = $documentType[2];
}
else{
	$TemplateID = $documentType[2] . '_' . $TemplateID;
}
CModule::IncludeModule("iblock");

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/iblock/admin_tools.php");
?>

<?if(CModule::IncludeModuleEx("htmls.docdesigner") < 3){
  $discConverted = \Bitrix\Main\Config\Option::get('disk', 'successfully_converted', false);
  if($discConverted == 'Y'){
    CModule::IncludeModule('disk');
    CModule::IncludeModule('socialnetwork');
    $cDoc = new CDocDesignerContracts;
    $diskTree = $cDoc->getDisks();

    $sections = '<select id="DocumentDisk" name="DocumentDisk[]" onchange="getSections(this.value);">';
    $sections .= '<option value="0">---</option>';
    foreach($diskTree as $id => $aDisk){
      if($aDisk['tree']){//deprecated
        $sections .= '<optgroup label="' . GetMessage('BPDSA_DISK') . $aDisk['name'] . '">';
        if($arCurrentValues['DocumentSection'] == $aDisk['root'])
            $sections .= '<option selected value="' . $aDisk['root'] . '">.' . $aDisk['name'] . '</option>';
        else
            $sections .= '<option value="' . $aDisk['root'] . '">.' . $aDisk['name'] . '</option>';
        $sections .= $cDoc->getSelect($aDisk['tree'], '', $arCurrentValues['DocumentSection']);
        $sections .= '</optgroup>';
      }
      else{
        if($arCurrentValues['DocumentDisk'] == $aDisk['root'])
            $sections .= '<option selected value="' . $aDisk['root'] . '">' . GetMessage('BPDSA_DISK') . $aDisk['name'] . '</option>';
        else
            $sections .= '<option value="' . $aDisk['root'] . '">' . GetMessage('BPDSA_DISK') . $aDisk['name'] . '</option>';
      }
    }
  }//if($discConverted == 'Y')
	$CDocDes = new CDocDesignerProcessing;
	$arTemp = $CDocDes->GetTemplateList($TemplateID, true);
	if(count($arTemp) > 0){
		$select = '<select name="template_select" id="id_template_select">';
		$common = false;
		foreach($arTemp as $id => $name){
			if($id == 'common'){
				$select .= "<optgroup label='".GetMessage("BPPDFA_COMMON_TEMPLATES")."'>";
				foreach($name as $cid => $cname){
					if($arCurrentValues['template_select'] == $cid){
						$select .= "<option value='" . $cid . "' selected>" . $cname . "</option>";
					}
					else{
						$select .= "<option value='" . $cid . "'>" . $cname . "</option>";
					}
				}
				$select .= '</optgroup>';
			}
			else{
				if($arCurrentValues['template_select'] == $id){
					$select .= "<option value='" . $id . "' selected>" . $name . "</option>";
				}
				else{
					$select .= "<option value='" . $id . "'>" . $name . "</option>";
				}
			}
		}
		$select .= '</select>';
	}
	else{
		$select = GetMessage("BPPDFA_NO_TEMPLATES");
	}

	$CDocDesBPA = new CDocDesignerBPActivities;
	$arResult = $CDocDesBPA->GetNumeratorsList();
	if(count($arResult) > 0){
		$nselect = '<select name="pdf_numerator" id="id_pdf_numerator">';
		foreach($arResult as $id => $name){
			if($arCurrentValues['pdf_numerator'] == $id){
				$nselect .= "<option value='" . $id . "' selected>" . $name . "</option>";
			}
			else{
				$nselect .= "<option value='" . $id . "'>" . $name . "</option>";
			}
		}
		$nselect .= '</select>';
	}
	else{
		$nselect = GetMessage("BPPDFA_NO_NUMERATORS");
	}
}
$arFSz = array(8,10,12);
$fselect = '<select name="fontSize_select" id="id_fontSize_select">';
foreach($arFSz as $fs){
  if($arCurrentValues['fontSize_select'] == $fs){
    $fselect .= "<option value='" . $fs . "' selected>" . $fs . "</option>";
  }
  else{
    $fselect .= "<option value='" . $fs . "'>" . $fs . "</option>";
  }
}
$fselect .= '</select>';

$CreateCopyCheched = '';
if($arCurrentValues['create_copy'] == 'Y'){
	$CreateCopyCheched = ' checked';
}

$CreateInvoiceCheched = '';
if($arCurrentValues['create_invoice'] == 'Y'){
	$CreateInvoiceCheched = ' checked';
}

$LIBRARY_ID = COption::GetOptionString("htmls.docdesigner", "DOCDESIGNER_CONTRACTS_PATH2LIBRARY");
?>
<tr>
	<td align="right" width="40%"><span style="color:#FF0000;">*</span> <?= GetMessage("BPPDFA_TEMPLATE_SELECT") ?>:</td>
	<td width="60%">
		<?=$select?>
	</td>
</tr>
<tr>
	<td align="right" width="40%"><?= GetMessage("BPPDFA_NUMERATOR_SELECT") ?>:</td>
	<td width="60%">
		<?=$nselect?>
	</td>
</tr>
<tr>
	<td align="right" width="40%"><?= GetMessage("BPPDFA_CREATE_COPY") ?>:</td>
	<td width="60%">
		<input type='checkbox' name='create_copy' id='id_create_copy' <?=$CreateCopyCheched?>/>
	</td>
</tr>
<tr>
	<td align="right" width="40%"><?= GetMessage("BPDSA_DOCUMENT_DISK") ?>:</td>
	<td width="60%">
		<?=$sections?>
	</td>
</tr>
<tr>
	<td align="right" width="40%"><?= GetMessage("BPPDFA_DOCUMENT_SECTION") ?>:</td>
	<td width="60%">
		<?
        if($discConverted == 'Y'){
          echo '<div id="diskSections"></div>';//$sections;
        }
        else{
          $prop_fields = array("LINK_IBLOCK_ID" => $LIBRARY_ID, "MULTIPLE_CNT" => 0, "MULTIPLE" => 'N', "VALUE" => $arCurrentValues['DocumentSection']);
			_ShowGroupPropertyField('DocumentSection', $prop_fields, array($prop_fields["VALUE"]));
        }
        ?>
	</td>
</tr>
<tr>
	<td align="right" width="40%"><?= GetMessage("BPPDFA_CREATE_INVOICE") ?>:</td>
	<td width="60%">
		<input type='checkbox' name='create_invoice' id='id_create_invoice' <?=$CreateInvoiceCheched?>/>
	</td>
</tr>
<tr>
	<td align="right" width="40%"><?= GetMessage("BPPDFA_FONTSIZE_SELECT") ?>:</td>
	<td width="60%">
		<?=$fselect?>
	</td>
</tr>
<?
$val = COption::GetOptionString("htmls.docdesigner", 'DOCDESIGNER_CLOUD_DOCX', 'N');
if($val == 'Y'):?>
<?
if(empty($arCurrentValues['mime_select'])){
  $arCurrentValues['mime_select'] = 'PDF';
}
$mime = array('PDF','DOCX');
$mimeselect = '<select name="mime_select" id="id_mime_select">';
foreach($mime as $mm){
  if($arCurrentValues['mime_select'] == $mm){
    $mimeselect .= "<option value='" . $mm . "' selected>" . $mm . "</option>";
  }
  else{
    $mimeselect .= "<option value='" . $mm . "'>" . $mm . "</option>";
  }
}
$mimeselect .= '</select>';
?>
<tr>
	<td align="right" width="40%"><?= GetMessage("BPPDFA_MIME")?>:</td>
	<td width="60%">
		<?=$mimeselect?>
	</td>
</tr>
<?endif;?>
<script>
function getTree(data, mov){
  mov = mov + '--';
  select = '';
  for(var sid in data){
      if(currentSection == sid)
          select += '<option value="'+sid+'" selected>'+mov+'>'+data[sid]['name']+'</option>';
      else
          select += '<option value="'+sid+'">'+mov+'>'+data[sid]['name']+'</option>';

      if(tree = data[sid]['tree']){
        select += getTree(tree, mov);
      }
  }
  return select;

}
function getSections(sid){
    currentSection = '<?=$arCurrentValues["DocumentSection"]?>';
    arParam = {sectionId: sid};
    $.post("/bitrix/admin/docdesigner_ajax.php", arParam, function(data){
      console.log(data);
        select = '<select name="DocumentSection[]">';
        for(var sid in data){
            if(currentSection == sid)
                select += '<option value="'+sid+'" selected>'+data[sid]['name']+'</option>';
            else
                select += '<option value="'+sid+'">'+data[sid]['name']+'</option>';

            if(tree = data[sid]['tree']){
              select += getTree(tree, '');
            }
        }
        select += '</select>';
        $('#diskSections').html(select);
    }, "json");
}

$(document).ready(function () {
    did = $('#DocumentDisk').val();
    if(did > 0){
        getSections(did);
    }

    $('.bx-core-adm-dialog-head-block').append('<img src="/bitrix/images/htmls.docdesigner/help.png" style="float: right;" onclick="window.open(\'https://www.htmls.pw/learning/course/index.php?COURSE_ID=2&LESSON_ID=78&LESSON_PATH=60.77.78\');" />');
});
</script>