<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arActivityDescription = array(
	"NAME" => GetMessage("BPDSA_DESCR_NAME"),
	"DESCRIPTION" => GetMessage("BPDSA_DESCR_DESCR"),
	"TYPE" => "activity",
	"CLASS" => "HtmlsDiskSectionActivity",
	"JSCLASS" => "BizProcActivity",
	"CATEGORY" => array(
		"ID" => "other",
	),
);
?>
