<?
/*******************************************************************************
* Module:   DocDesigner                                                        *
* Class:    CDocDesignerProcessing extends CDocDesigner                        *
* Version:  8.4.13                                                             *
* Author:   Alexey Andreev, HTMLStudio (www.htmls.ru)                          *
* License:  Shareware                                                          *
* Date:     2017-04-07                                                         *
* e-mail:   aav@htmls.ru                                                       *
*******************************************************************************/
if(COption::GetOptionString('htmls.docdesigner', 'DOCDESIGNER_ADDMESSAGE2LOG') == 'Y'){
	define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/bitrix/tmp/DocDesigner.txt");
}
IncludeModuleLangFile(__FILE__);
CModule::IncludeModule("iblock");
class CDocDesignerProcessing extends CDocDesigner{
    protected $JOURNAL_ID;
    public $CURRENCY_ID;
    public $isCRM = true;
	function CDocDesignerProcessing(){
		if(!is_dir($_SERVER['DOCUMENT_ROOT'] . "/bitrix/tmp/"))
			mkdir($_SERVER['DOCUMENT_ROOT'] . "/bitrix/tmp/");

		if(!is_dir($_SERVER['DOCUMENT_ROOT'] . "/bitrix/tmp/htmls.docdesigner/"))
			mkdir($_SERVER['DOCUMENT_ROOT'] . "/bitrix/tmp/htmls.docdesigner/");
		if(!is_dir($_SERVER['DOCUMENT_ROOT'] . "/upload/htmls.docdesigner/"))
			mkdir($_SERVER['DOCUMENT_ROOT'] . "/upload/htmls.docdesigner/");
		$this->Add2Log = COption::GetOptionString('htmls.docdesigner', 'DOCDESIGNER_ADDMESSAGE2LOG');
        $this->JOURNAL_ID = COption::GetOptionInt("htmls.docdesigner", 'DOCDESIGNER_DOCS_REGISTER_ID');
        $this->DECLINATOR = COption::GetOptionInt("htmls.docdesigner", 'DOCDESIGNER_DECLINATOR', 'INNER');
        $this->DigitCol = (COption::GetOptionString('htmls.docdesigner', 'DOCDESIGNER_REMOVE_PENNY', 'N') == 'Y')? 0 : 2;
        $this->quantityFloat = COption::GetOptionString('htmls.docdesigner', 'DOCDESIGNER_QUANTITY_FLOAT', 0);
	}

	function getShortName($Name){
		$arName = explode(' ', $Name);
        if(strtoupper(LANG_CHARSET) == 'UTF-8'){
    		$arName[1] = mb_substr($arName[1], 0, 1, 'UTF-8') . ".";
    		$arName[2] = mb_substr($arName[2], 0, 1, 'UTF-8') . ".";
        }
        else{
    		$arName[1] = substr($arName[1], 0, 1) . ".";
    		$arName[2] = substr($arName[2], 0, 1) . ".";
        }
		return implode(' ', $arName);
	}

	function UserDeclination($sUser, $ParamName, &$arProperties){
		$UserID = substr($sUser, strlen("user_"));
		$rsUser = CUser::GetByID($UserID);
		$arUser = $rsUser->Fetch();
		//$sFIO = $arUser['LAST_NAME'] . ' ' . substr($arUser['NAME'], 0, 1) . ". " . substr($arUser['SECOND_NAME'], 0, 1);
		$fFIO = $arUser['LAST_NAME'] . ' ' . $arUser['NAME'] . " " . $arUser['SECOND_NAME'];
		$arFFIO = $this->getDeclination($fFIO);
		$arProperties[$ParamName . '.FULLNAME.IMP'] = $fFIO;
		$arProperties[$ParamName . '.FULLNAME.RP'] = $arFFIO[1];
		$arProperties[$ParamName . '.FULLNAME.DP'] = $arFFIO[2];
		$arProperties[$ParamName . '.SHORTNAME.IMP'] = $this->getShortName($fFIO);
		$arProperties[$ParamName . '.SHORTNAME.RP'] = $this->getShortName($arFFIO[1]);
		$arProperties[$ParamName . '.SHORTNAME.DP'] = $this->getShortName($arFFIO[2]);

    	$arPos = $this->getDeclination($arUser['WORK_POSITION']);
		$arProperties[$ParamName . '.POSITION.IMP'] = $arUser['WORK_POSITION'];
		$arProperties[$ParamName . '.POSITION.RP'] = $arPos[1];
		$arProperties[$ParamName . '.POSITION.DP'] = $arPos[2];
	}

	//return comlete structure for PDF-print: array(arTable, border)
	//arTable:
	//rows[] = array('row' => $colls, 'width' => $width, 'align' => $align, 'fills' => $fills)
	//$colls = array with cell's data
	//$width = array with cell's width
	//$align = array with cell's align
	//$fills = array with cell's color2fill rect (r,g,b) or empty for white
	##############
	//border = 0/1
	#Parameters#
	//$DocID = array(bizproc ID, template ID)
	//$arProperties:
	// - none for blank
	// - array with bizproc template parameters from $rootActivity for output document
	function PrepareTable($DocID, $arProperties = array(), $arPropertiesTypes = array(), $default = false, $CreateOriginal = false){
		global $MESS;
        $num2str = new CNum2Str;
        if($this->CURRENCY_ID != 'RUB'){
          if(file_exists($GLOBALS["DOCUMENT_ROOT"]."/bitrix/modules/htmls.docdesigner/lang/ru/".$this->CURRENCY_ID.'.php')){
            require($GLOBALS["DOCUMENT_ROOT"]."/bitrix/modules/htmls.docdesigner/lang/ru/".$this->CURRENCY_ID.'.php');
            $num2str->w_1_2 = $MESS['_w_1_2'];
    		$num2str->w_1_19 = $MESS['_w_1_19'];
    		$num2str->des = $MESS['_des'];
    		$num2str->hang = $MESS['_hang'];
    		$num2str->namerub = $MESS['_namerub'];
    		$num2str->nametho = $MESS['_nametho'];
    		$num2str->namemil = $MESS['_namemil'];
    		$num2str->namemrd = $MESS['_namemrd'];
    		$num2str->kopeek = $MESS['_kopeek'];
          }
        }
        $IsCatalog = IsModuleInstalled("catalog");
        foreach($arProperties as $k => $v){
			if($k == 'TargetUser'){
				$this->UserDeclination($v, 'TargetUser', $arProperties);
			}
			elseif($k == 'DocDesignerCompany'){
				$arCompany = $this->GetCompanyData($v);
			}
			else{
				if($arPropertiesTypes[$k]['Type'] == 'S:UserID' || $arPropertiesTypes[$k]['Type'] == 'user' || $arPropertiesTypes[$k]['Type'] == 'S:employee'){
					if(is_array($v))	$sID = $v[0];
					else 				$sID = $v;
					$this->UserDeclination($sID, $k, $arProperties);
				}
			}
		}
		if(count($arCompany) > 0){
			foreach($arCompany as $k => $v){
				$arProperties[$k] = $v;
			}
		}
		if($CreateOriginal){
			$arProperties['Company.CEO_FACSIMILEImg'] = '';
			$arProperties['Company.SEO_FACSIMILE'] = '';
			$arProperties['Company.INVOICE_FACSIMILEImg'] = '';
			$arProperties['Company.INVOICE_FACSIMILE'] = '';
		}
		//AddMessage2Log(print_r($this->bpParameters,1),1);
		//AddMessage2Log(print_r($arProperties, true), "/htmls.DocDesigner");
		//get table structure
		$perms = $this->GetTemplate($DocID, $default);
		$page = 'P';
		if($perms['page']) $page = $perms['page'];
		if($perms['html']){
			$TMP = $perms['html'];
            if(strpos($TMP, "{Multiple:") > 0){
				foreach($arProperties as $name => $val){
					if(is_array($val)){
					  //AddMessage2Log(print_r($val, true), $name."/htmls.DocDesigner");
						if(strpos($TMP, "{Multiple:" . $name ."}") > 0){
							$MultipleTotal = 0;
							$StrTmp = "";
							$ns = 0;
							if(count($val) > 0){
								foreach($val as $str){
									if(strpos($str, ";") > 0){
									  $arStr = explode(";", $str);
									  //AddMessage2Log(print_r($arStr, true), $name."/htmls.DocDesigner");
    									$StrTmp .= "<tr>";
    									$ns++;
    									if(count($arStr) == 3){
    										$StrTmp .= "<td width='5%' align='right'>".$ns."</td>";
    										$StrTmp .= "<td width='55%'>".$arStr[0]."</td>";
    										$StrTmp .= "<td width='10%' align='right'>".$arStr[1]."</td>";
    										$StrTmp .= "<td width='15%' align='right'>".number_format($arStr[2], $this->DigitCol, ',', ' ')."</td>";
    										$StrTmp .= "<td width='15%' align='right'>".number_format($arStr[1]*$arStr[2], $this->DigitCol, ',', ' ')."</td>";
    										$MultipleTotal = $MultipleTotal + $arStr[1]*$arStr[2];
    										$colspan=4;
    									}
    									if(count($arStr) == 2){
    										$StrTmp .= "<td width='5%' align='right'>".$ns."</td>";
    										$StrTmp .= "<td width='80%'>".$arStr[0]."</td>";
    										$StrTmp .= "<td width='15%' align='right'>".number_format($arStr[1], $this->DigitCol, ',', ' ')."</td>";
    										$MultipleTotal = $MultipleTotal + $arStr[1];
    										$colspan=2;
    									}
    									$StrTmp .= "</tr>";
                                    }
                                    else{
                                      if($_v = $this->bpParameters[$name]['Options'][$str])
                                        $StrTmp .= '<li>' . $_v . '</li>';
                                      else
                                        $StrTmp .= '<li>' . $str . '</li>';
                                    }
								}
                                if($arStr){
    								$footer = "<tr><td colspan='".$colspan."'><td align='right'>".number_format($MultipleTotal, $this->DigitCol, ',', ' ')."</td></tr>";
    								$tab = "<table border='1' cellspacing='0' cellpadding='3' width='100%'>".$StrTmp.$footer."</table>";
    								$arProperties['MultipleTotal'] = $MultipleTotal;
                                }
                                else{
                                    $tab = "<ol>".$StrTmp."</ol>";
                                }
							}
							else{
								$tab = "";
							}
							$TMP = str_replace("{Multiple:" . $name ."}", $tab, $TMP);
						}
                        else{

                        }
					}
				}
			}
            $TMP = str_replace('{PageBreak}', '<div style="page-break-after: always;"></div>', $TMP);
			$TMP = $this->win2uni($TMP);
			$dom = new DOMDocument();
			$dom->loadHTML('<meta http-equiv="content-type" content="text/html; charset=utf-8">' . $TMP);
			$tables = $dom->getElementsByTagName('table');
			$TotalSum = 0;
			$TotalSumVAT = 0;
			$vatRate = 0;
			$TotalVAT = 0;
            $TotalDiscount = 0;
            $TotalSumWithoutDiscountTax = 0;
			if($arProperties['DocDesignerVATRate']) $vatRate = $arProperties['DocDesignerVATRate'];

			$products = $arProperties['arCrmProducts'];
            //AddMessage2Log(print_r($products, true), __LINE__."/htmls.DocDesigner");
            $isCRM = IsModuleInstalled("crm");
            if($isCRM){
                $cid = CCrmCatalog::GetDefaultID();
            }
            //$properties = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>$cid));
            //$arProdProps = array();
            //while($prop_fields = $properties->GetNext())

			foreach($tables as $table){
				foreach($table->childNodes as $tbody){
					if($tbody->tagName == 'tbody'){
						foreach($tbody->childNodes as $tr){
							if(strpos($tr->nodeValue, 'CRMProduct:Name}') > 0){
								foreach($products as $sn => $arProd){
									$newTR = $dom->createElement('tr');
									foreach($tr->childNodes as $td){
									  if($td->tagName != 'td') continue;
										$val = $td->nodeValue;
                                        //
										$val = str_replace('{CRMProduct:StrNum}', $sn, $val);
                                        $val = str_replace('{CRMProduct:Desc}', $this->win2uni($arProd['PRODUCT_DESCRIPTION']), $val);
                                        if($arProd['PRODUCT_NAME'])
    									    $val = str_replace('{CRMProduct:Name}', $this->win2uni($arProd['PRODUCT_NAME']), $val);
                                        else
    									    $val = str_replace('{CRMProduct:Name}', $this->win2uni($arProd['ORIGINAL_PRODUCT_NAME']), $val);
    									$val = str_replace('{CRMProduct:Measure}', $this->win2uni($arProd['MEASURE_NAME']), $val);


                                        $db_props = CIBlockElement::GetProperty($cid, $arProd['PRODUCT_ID'], array("sort" => "asc"));
                                        while($ar_props = $db_props->Fetch())
                                            $val = str_replace('{CRMProduct:PROPERTY_'.$ar_props['ID'].'}', $this->win2uni($ar_props["VALUE"]), $val);

										$val = str_replace('{CRMProduct:Quantity}', number_format($arProd['QUANTITY'], $this->quantityFloat, ',', ' '), $val);
										$val = str_replace('{CRMProduct:DiscountRate}', $arProd['DISCOUNT_RATE'], $val);
										$val = str_replace('{CRMProduct:DiscountSum}', $arProd['DISCOUNT_SUM'], $val);
                                        if($arProd['TAX_RATE']){
                                            $vatRate = intval($arProd['TAX_RATE']);
                                        }
                                        if($arProd['VAT_RATE']){
                                            $vatRate = intval($arProd['VAT_RATE'] * 100);
                                        }
                                        $arProperties['ProductVATRate'] = $vatRate;
                                        $val = str_replace('{CRMProduct:VATRate}', $vatRate, $val);
										$val = str_replace('{CRMProduct:Price}', number_format($arProd['PRICE'], $this->DigitCol, ',', ' '), $val);
                                        $priceIncDiscount = round(($arProd['DISCOUNT_SUM'] + round($arProd['PRICE'] / (1 + $vatRate / 100), 2)) * (1 + $vatRate / 100), 2);
										$val = str_replace('{CRMProduct:PriceIncDiscount}', number_format(($priceIncDiscount), $this->DigitCol, ',', ' '), $val);
										//$val = str_replace('{CRMProduct:PriceIncDiscount}', number_format(($arProd['PRICE']+$arProd['DISCOUNT_SUM']), $this->DigitCol, ',', ' '), $val);

                                        $vatInc = 'Y';
                                        if($arProd['TAX_INCLUDED'] == 'N'){
                                          $vatInc = 'N';
                                        }
                                        if($arProd['VAT_INCLUDED'] == 'N'){
                                          $vatInc = 'N';
                                        }
                                        //if($vatInc == 'Y'){
                                            $SUMVat = $arProd['PRICE']*$arProd['QUANTITY'];
                                            $VAT = $arProd['QUANTITY'] * round($arProd['PRICE'] * $vatRate / ($vatRate + 100), 2);//round($SUMVat * $vatRate / ($vatRate + 100),2);
                                            $SUM = $SUMVat - $VAT;

                                        /*}
                                        else{
                                            $SUM = $arProd['PRICE']*$arProd['QUANTITY'];
                                            $VAT = round($SUM * $vatRate / 100,2);
                                            $SUMVat = $SUM + $VAT;

                                        }*/
                                        $val = str_replace('{CRMProduct:Sum}', number_format($SUM, $this->DigitCol, ',', ' '), $val);
                                        $val = str_replace('{CRMProduct:VAT}', number_format($VAT, $this->DigitCol, ',', ' '), $val);
                                        $val = str_replace('{CRMProduct:SumVAT}', number_format($SUMVat, $this->DigitCol, ',', ' '), $val);
                                        //AddMessage2Log($val, __LINE__);
										//$newTD = $dom->createElement('td', $val);
                                        $newTD = $td->cloneNode(true);
                                        $newTD->nodeValue = '';

                                        $d = new DOMDocument;
                                        $d->loadHTML("<meta http-equiv='Content-Type' content='text/html; charset=utf-8' /><div>" . $val . "</div>");
                                        $div = $d->getElementsByTagName('div');
                                        foreach($div as $d)
                                            $newTD->appendChild($dom->importNode($d, true));
										//if(strpos($td->nodeValue, 'Quantity') > 0 or strpos($td->nodeValue, 'Price') > 0 or strpos($td->nodeValue, 'Sum') > 0 or strpos($td->nodeValue, 'StrNum') > 0){
										//	$align = $dom->createAttribute("align");
										//	$align->value = "right";
										//	$newTD->appendChild($align);
						                //}
						                //$border = $dom->createAttribute('border');
										//$border->value = '1';
										//$newTD->appendChild($border);
										$newTR->appendChild($newTD);
									}
									$arTR[] = $newTR;
									$TotalSum = $TotalSum + $SUM;
									$TotalSumVat = $TotalSumVat + $SUMVat;
                                    $TotalVAT = $TotalVAT + $VAT;
                                    $TotalDiscount = $TotalDiscount + $arProd['DISCOUNT_SUM'];
                                    $TotalSumWithoutDiscountTax = $TotalSumWithoutDiscountTax + $arProd['QUANTITY'] *
                                            ($arProd['PRICE'] / (1 + $vatRate / 100) + $arProd['DISCOUNT_SUM']);
								}

								if(count($arTR) > 0){
			                        $tbody->replaceChild($arTR[0], $tr);
			                        for($i=1;$i<count($arTR);$i++){
										$tbody->appendChild($arTR[$i]);
									}
								}
							}
						}
					}
				}
			}

            $TotalQuantity = 0;
            $rowsCount = 0;
            $item = 0;
            foreach($products as $sn => $aRow){
              $Desc = $aRow['PRODUCT_DESCRIPTION'];
              //AddMessage2Log(print_r($aRow, true), __LINE__."/htmls.DocDesigner");
              if($aRow['ORIGINAL_PRODUCT_NAME']) $Name = $aRow['ORIGINAL_PRODUCT_NAME'];
              else $Name = $aRow['PRODUCT_NAME'];

              $arProperties['CRMProduct:ProdDesc_' . $item . ''] = $Desc;//aProd['DESCRIPTION']
              $arProperties['CRMProduct:Name_' . $item . ''] = $Name;
              $arProperties['CRMProduct:Price_' . $item . ''] = $aRow['PRICE'];
              $arProperties['CRMProduct:Quantity_' . $item . ''] = number_format($arProd['QUANTITY'], $this->quantityFloat, ',', ' ');
              $arProperties['CRMProduct:QuantitySpelling_' . $item . ''] = $num2str->num2spell($aRow['QUANTITY']);

              $item++;
              $_arProducts[] = $Name;

              $TotalQuantity = $TotalQuantity + $aRow['QUANTITY'];

              $rowsCount++;
            }
			$html = $dom->saveHTML();
            //AddMessage2Log(print_r($arProperties, true), __LINE__."/htmls.DocDesigner");
			$arProperties['CRMProduct:TotalSum'] = number_format($TotalSum, $this->DigitCol, ',', ' ');
			$arProperties['CRMProduct:TotalSumVAT'] = number_format($TotalSumVat, $this->DigitCol, ',', ' ');
			$arProperties['CRMProduct:DiscountSumTotal'] = number_format($TotalDiscount, $this->DigitCol, ',', ' ');
			$arProperties['CRMProduct:TotalSumNoDiscountTax'] = number_format($TotalSumWithoutDiscountTax, $this->DigitCol, ',', ' ');
            $text = trim($num2str->num2str($TotalSumWithoutDiscountTax));
			//$text = mb_strtoupper(mb_substr($text, 0, 1, 'UTF-8'), 'UTF-8') . mb_substr($text, 1, mb_strlen($text), 'UTF-8');

            //$s = $num2str->num2str($arDeal['OPPORTUNITY']);
            $s = (CDocDesigner::win2uni($text));
            $val2 = CDocDesigner::first_letter_up($s);
            $text = CDocDesigner::uni2win($val2);

			$arProperties['CRMProduct:TotalSumNoDiscountTaxSpelling'] = $text;
			/*if($vatRate > 0){
				$TotalVAT = $TotalSum*$vatRate/(100+$vatRate);
			}*/
			$arProperties['CRMProduct:TotalVAT'] = number_format($TotalVAT, $this->DigitCol, ',', ' ');

            $text = trim($num2str->num2str($TotalSum));
            $s = trim(CDocDesigner::win2uni($text));
            $text = CDocDesigner::first_letter_up($text);
            $text = CDocDesigner::uni2win($val2);
			//$text = mb_strtoupper(mb_substr($text, 0, 1, 'UTF-8'), 'UTF-8') . mb_substr($text, 1, mb_strlen($text), 'UTF-8');
			$arProperties['CRMProduct:TotalSumSpelling'] = $text;

            $text = trim($num2str->num2str($TotalSumVat));
			$s = trim(CDocDesigner::win2uni($text));
            $text = CDocDesigner::first_letter_up($s);
            $text = CDocDesigner::uni2win($text);
			//$text = mb_strtoupper(mb_substr($text, 0, 1, 'UTF-8'), 'UTF-8') . mb_substr($text, 1, mb_strlen($text), 'UTF-8');
			$arProperties['CRMProduct:TotalSumVatSpelling'] = $text;

            $text = (trim($num2str->num2str($TotalVAT)));
            $s = trim($this->win2uni($text));
            $text = $this->first_letter_up($s);
            $text = $this->uni2win($text);
            //$text = (mb_strtoupper(mb_substr($text, 0, 1, 'UTF-8'), 'UTF-8') . mb_substr($text, 1, mb_strlen($text), 'UTF-8'));
			$arProperties['CRMProduct:TotalVATSpelling'] = $text;

            //$arProperties['DEAL.OPPORTUNITY'] = number_format($arProperties['DEAL.OPPORTUNITY'], $this->DigitCol, ',', ' ');

			$arProperties['CRMProduct:NumRows'] = count($products);

			foreach($arProperties as $k => $v){
				if(is_array($v)){

				    if($v['TYPE'] == 'html'){
						$v = $v['TEXT'];
					}
                    else{

                      $list = '<ul>';
                      foreach($v as $str){
                        $list .= '<li>' . $str . '</li>';
                      }
                      $list .= '</ul>';
                      //$TMP = str_replace("{" . $name ."}", $list, $TMP);
                      $v = $list;
                    }
				}
				$html = str_replace('{' . $k . '}', $this->win2uni($v), $html);
			}

            return array(
                        'html' => $html,
                        'page' => $page,
                        'arProperties' => $arProperties,
                        'params' => $perms['params']
                        );
		}
		else{
			//768px=190mm
			$kAbs = 190 / 768;//k for calc width by px
			$kPer = 190 / 100;//k for calc width by %%
			$arAlign = array('left' => 'L', 'center' => 'C', 'right' => 'R');

			$arWidth[3] = array(100, 30, 30, 30);
			$arWidth[2] = array(160, 30);
			$arWidth[5] = array(10, 101, 18, 11, 25, 25);
			$RowColor = array('red'=>220,'green'=>220,'blue'=>220);
			$HeaderColor = array('red'=>150,'green'=>150,'blue'=>150);
			//prepare data by each row structure
			foreach($perms['items']['row'] as $r){
				$tWidth = 190;
				$Row = array();

				$align = array();
				$colls = array();
				$fills = array();
				$borders = array();
				$width = array();
				$colspan = array();
				$colcnt = 0;

				$Multiple = false;
				//if multiple parameter by one col
				if(count($r['col']) == 1){
					$text = strip_tags(htmlspecialcharsBack($r['col'][0]['text']));
					$pos = strpos($text, '{Multiple:');
					if($pos !== false) $Multiple = true;
				}

				if($Multiple){
					$Total = 0;
					$k = str_replace('}', '', str_replace('{Multiple:', '', $text));
					$v=$arProperties[$k];

					$PutTableHeader = false;
					$UseIB = false;
					$fill = false;
					foreach($v as $offer){
						if(empty($offer)) continue;
						if(!$PutTableHeader){
							if($arPropertiesTypes[$k]['Type'] == 'S' or $arPropertiesTypes[$k]['Type'] == 'string'){
								$UseQuantity = count(explode(';', $offer));
								if($UseQuantity > 2){
									if($UseQuantity < 4){
										$arTable[] = array('row' => array(GetMessage('OFFER_TABLE_HEADER_NAME'), GetMessage('OFFER_TABLE_HEADER_QUANTITY'), GetMessage('OFFER_TABLE_HEADER_PRICE'), GetMessage('OFFER_TABLE_HEADER_SUM')), 'width' => $arWidth[3], 'align' => array('C', 'C', 'C', 'C'), 'fills' => array($HeaderColor,$HeaderColor,$HeaderColor,$HeaderColor));
									}
									else{

									$arTable[] = array('row' => array(GetMessage('BILL_TABLE_HEADER_NUM'), GetMessage('BILL_TABLE_HEADER_NAME'), GetMessage('BILL_TABLE_HEADER_QUANTITY'), GetMessage('BILL_TABLE_HEADER_UNIT'), GetMessage('BILL_TABLE_HEADER_PRICE'), GetMessage('BILL_TABLE_HEADER_SUM')), 'width' => array(10,101,18,11,25,25), 'align' => array('C', 'C', 'C', 'C', 'C', 'C'), 'fills' => array(), 'borders' => array('LTRB','LTRB','LTRB','LTRB','LTRB','LTRB'));
										$fill = false;
									}
									$PutTableHeader = true;
								}
								else{
									$arTable[] = array('row' => array(GetMessage('OFFER_TABLE_HEADER_NAME'), GetMessage('OFFER_TABLE_HEADER_SUM')), 'width' => $arWidth[2], 'align' => array('C', 'C'), 'fills' => array($HeaderColor, $HeaderColor));
									$PutTableHeader = true;
								}
							}
							else{
								/*Type => E:EList
								Multiple => 1
								Options => 12*/
								$arTable[] = array('row' => array(GetMessage('OFFER_TABLE_HEADER_NAME'), GetMessage('OFFER_TABLE_HEADER_SUM')), 'width' => $arWidth[2], 'align' => array('C', 'C'), 'fills' => array($HeaderColor, $HeaderColor));
								$PutTableHeader = true;
								$UseIB = true;
								$UseQuantity = 2;
							}
						}
						if($fill){
							$fills = array($RowColor,$RowColor,$RowColor,$RowColor);
							$fill = false;
						}
						else{
							$fills = array(false,false,false,false,false,false);
							$fill = true;
						}

						$Row[] = $offer;
						$arOffer = explode(';', $offer);
						$aligns = array('L', 'R', 'R', 'R');
						if($UseQuantity > 2){
							if($UseQuantity < 4){
								$Sum = $arOffer[1] * $arOffer[2];
								$arOffer[] = number_format($Sum, $this->DigitCol, ',', ' ');
								$arOffer[2] = number_format($arOffer[2], $this->DigitCol, ',', ' ');
								$Total = $Total + $Sum;
							}
							else{
								$aligns = array('R', 'L', 'R', 'C', 'R', 'R');
								$Sum = $arOffer[2] * $arOffer[4];
								$arOffer[] = number_format($Sum, $this->DigitCol, ',', ' ');
								$arOffer[4] = number_format($arOffer[4], $this->DigitCol, ',', ' ');
								$Total = $Total + $Sum;
							}
						}
						else{
							if($UseIB){
								$OfferPropID = COption::GetOptionString("htmls.docdesigner", "DOCDESIGNER_OFFER_PRICE");
								$arSelect = Array("NAME", "PROPERTY_".$OfferPropID);
								$arFilter = Array("ID"=>IntVal($arOffer[0]));
								$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
								while($ob = $res->GetNextElement()){
									$arFields = $ob->GetFields();
									$arOffer[0] = $arFields['NAME'];
									if($IsCatalog){
										$CatalogPrice = CPrice::GetBasePrice(IntVal($arOffer[0]));
										$arOffer[1] = $PROD_SUM + $CatalogPrice["PRICE"];
									}
									else{
										$arOffer[1] = $arFields['PROPERTY_'.$OfferPropID.'_VALUE'];
									}
								}
							}
							$Total = $Total + $arOffer[1];
							$arOffer[1] = number_format($arOffer[1], $this->DigitCol, ',', ' ');
						}
						$arTable[] = array('row' => $arOffer, 'width' => $arWidth[$UseQuantity], 'align' => $aligns, 'fills' => (($UseQuantity == 5)? array() : $fills), 'borders' => (($UseQuantity == 5)? array('LTRB','LTRB','LTRB','LTRB','LTRB','LTRB') : array()));
					}
					if($Total > 0){
						if($UseQuantity < 4)
							$arTable[] = array('row' => array(GetMessage('OFFER_TABLE_HEADER_TOTAL'), number_format($Total, $this->DigitCol, ',', ' ')), 'width' => array(160, 30), 'align' => array('L', 'R'), 'fills' => array($HeaderColor, $HeaderColor));
						//else
						//	$arTable[] = array('row' => array(GetMessage('OFFER_TABLE_HEADER_TOTAL'), number_format($Total, 2, ',', ' ')), 'width' => array(165, 25), 'align' => array('L', 'R'), 'fills' => array($HeaderColor, $HeaderColor));
					}
				}
				else{
					//columns
					foreach($r['col'] as $col){

						$text = strip_tags($col['text']);
						foreach($arProperties as $k => $v){
							$text = str_replace('{' . $k . '}', $v, $text);
						}

						$text = html_entity_decode($text);
						$colls[] = HTMLToTxt($text,false,array("@<[\/\!]*?[a-z]*?>@si"));
						$arStyle = array();
						if($col['style']){
							$arSt = explode(';', $col['style']);
							foreach($arSt as $st){
								$ar = explode(':', $st);
								if($ar[1])
								$arStyle[trim($ar[0])] = $ar[1];
							}
						}

						$align[] = $arAlign[$col['align']];
						$fills[] = $this->hex2RGB($arStyle['background-color']);
						if($arStyle['border-width']){
							$borders[] = 'LTRB';
						}
						else{
							$L='';$T='';$R='';$B='';
							if($arStyle['border-top-width']) $T='T';
							if($arStyle['border-left-width']) $L='L';
							if($arStyle['border-right-width']) $R='R';
							if($arStyle['border-bottom-width']) $B='B';
							if($arStyle['border-top']) $T='T';
							if($arStyle['border-left']) $L='L';
							if($arStyle['border-right']) $R='R';
							if($arStyle['border-bottom']) $B='B';
							$borders[] = $L.$T.$R.$B;
						}
						$colspan[] = $col['colspan'];
						$colcnt += $col['colspan'];

					    if(strpos($arStyle['width'], 'px') > 0){
							$w = round($kAbs * intval($arStyle['width']));
							$tWidth = $tWidth - $w;
							$width[] = $w;
						}
						elseif(strpos($arStyle['width'], '%') > 0){
							$w = round($kPer * intval($arStyle['width']));
							$tWidth = $tWidth - $w;
							$width[] = $w;
						}
						else $width[] = intval($arStyle['width']);

						$Row[] = $col['text'];
					}
				}//end if Multiple

				//if no one width in row, calc width proportionally
				if($tWidth == 190){
					$w = round(190 / $colcnt);
					foreach($width as $k => $v){
						$ost = $tWidth - $w * $colspan[$k];
						if($ost < 0) $width[$k] = $tWidth;
						else $width[$k] = $w * $colspan[$k];
						$tWidth  = $tWidth - $w * $colspan[$k];
					}
				}
				else{
					$tWidth = 190;
					$cnt=0;
					//calc difference in set width and 190mm & count columns for proportionally calc width
					foreach($width as $k => $v){
						if($v == 0) $cnt++;
						else $tWidth = $tWidth - $v;
					}

					$w = round($tWidth / $cnt);
					foreach($width as $k => $v){
						if($v == 0){
							$ost = $tWidth - $w * $colspan[$k];
							if($ost < 0) $width[$k] = $tWidth;
							else $width[$k] = $w * $colspan[$k];
							$tWidth  = $tWidth - $w * $colspan[$k];
						}
					}
				}
				$arTable[] = array('row' => $colls, 'width' => $width, 'align' => $align, 'fills' => $fills, 'borders' => $borders);
			}
			return array('arTable' => $arTable, 'border' => $perms['border'], 'page' => $page);
		}
	}//function PrepareTable

	// return string or assoc array
	function hex2RGB($hexStr='#000', $returnAsString = false, $separator = ',') {
		$hexStr = preg_replace("/[^0-9A-Fa-f]/", '', $hexStr); // get color
		$rgbArray = array();
		if (strlen($hexStr) == 6) { //full color (#000000)
			$colorVal = hexdec($hexStr);
			$rgbArray['red'] = 0xFF & ($colorVal >> 0x10);
			$rgbArray['green'] = 0xFF & ($colorVal >> 0x8);
			$rgbArray['blue'] = 0xFF & $colorVal;
		} elseif (strlen($hexStr) == 3) { //short color (#000)
			$rgbArray['red'] = hexdec(str_repeat(substr($hexStr, 0, 1), 2));
			$rgbArray['green'] = hexdec(str_repeat(substr($hexStr, 1, 1), 2));
			$rgbArray['blue'] = hexdec(str_repeat(substr($hexStr, 2, 1), 2));
		} else {
			return false; //wrong code
		}
		return $returnAsString ? implode($separator, $rgbArray) : $rgbArray;
	}//function hex2RGB

	function getDeclination($text = ''){
        //$CDecl = new NCLNameCaseRu;
        //$CASE = COption::GetOptionInt("htmls.docdesigner", "DOCDESIGNER_DECLINATION_CASE", 1);
        //$txt = $CDecl->q($text, $CASE);
        if(function_exists('morpher_inflect')){
          $value = iconv(strtoupper(LANG_CHARSET), 'UTF-8', $text);
          $res[0] = $text;
  		  $res[1] = iconv('UTF-8', strtoupper(LANG_CHARSET), morpher_inflect($value, 'rod'));
  		  $res[2] = iconv('UTF-8', strtoupper(LANG_CHARSET), morpher_inflect($value, 'dat'));
  		  $res[3] = iconv('UTF-8', strtoupper(LANG_CHARSET), morpher_inflect($value, 'vin'));
  		  $res[4] = iconv('UTF-8', strtoupper(LANG_CHARSET), morpher_inflect($value, 'tvor'));
  		  $res[5] = iconv('UTF-8', strtoupper(LANG_CHARSET), morpher_inflect($value, 'predl'));
  		  return $res;
        }
        else{
          if(strtoupper($this->DECLINATOR) == 'INNER'){
            $nc = new NCLNameCaseRu();
            $res = $nc->q(iconv(strtoupper(LANG_CHARSET), 'UTF-8', $text));
            foreach($res as $key => $value){
        		$res[$key] = iconv('UTF-8', strtoupper(LANG_CHARSET), $value);
        		//$c++;
        	  }
            return $res;
          }
          else{//morpher
      		$url = "http://www.morpher.ru/WebService.asmx/GetXml?s=";

      		ini_set('default_socket_timeout', '10');
      		$fp = fopen($url, "r");
      		$res1 = fread($fp, 500);
      		fclose($fp);
      		if (strlen($res1) > 0){
      			$c = curl_init($url);
      	        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
      	        curl_setopt($c, CURLOPT_TIMEOUT, 2);
      	        curl_setopt($c, CURLOPT_HEADER, 1);
      	        $page = @curl_exec($c);//$p = $page;
      	        curl_close($c);
      	        $page = explode("\n", $page);

      	        if (preg_match("/200/i", $page["0"])){
      	            if( !empty($text) ){
      					$s = iconv(strtoupper(LANG_CHARSET), 'UTF-8', $text);
      					$xml = simplexml_load_file("http://www.morpher.ru/WebService.asmx/GetXml?s=" . $s);
      					$res[0] = $text;
      					$c = 1;
      					foreach($xml as $key0 => $value){
      						$res[$c] = iconv('UTF-8', strtoupper(LANG_CHARSET), $value);
      						$c++;
      					}
      					return $res;
      				}
      	        }
      	        else{
      	        	$res[0] = $text;
      				$res[1] = $text;
      				$res[2] = $text;
      				$res[3] = $text;
      				$res[4] = $text;
      				$res[5] = $text;
      				return $res;
      	        }
      		}
      		else{
      			$res[0] = $text;
      			$res[1] = $text;
      			$res[2] = $text;
      			$res[3] = $text;
      			$res[4] = $text;
      			$res[5] = $text;
      			return $res;
      		}
      		return $text;//something wrong*/
          }
        }
	}//function getDeclination($text = '')

	function GetCompanyData($ID){
		/*BigLogo => DETAIL_PICTURE
		SmallLogo => PREVIEW_PICTURE
		BankDetails => PREVIEW_TEXT
		*/
		$arSelect = Array("PREVIEW_TEXT", "DETAIL_PICTURE", "PREVIEW_PICTURE");
		$stamp = COption::GetOptionString("htmls.docdesigner", "DOCDESIGNER_COMPANY_FAKSIMILE", 'no');
		if($stamp != 'no'){
			$arSelect[] = 'PROPERTY_' . $stamp;
		}
		$ivstamp = COption::GetOptionString("htmls.docdesigner", "DOCDESIGNER_COMPANY_INVOICE_FACSIMILE", 'no');
		if($ivstamp != 'no'){
			$arSelect[] = 'PROPERTY_' . $ivstamp;
		}
		$arFilter = Array("ID"=>IntVal($ID));
		$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
		$arCompany = array();
		while($ob = $res->GetNextElement()){
			$arFields = $ob->GetFields();
			$arCompany['Company.BankDetails'] = $arFields['PREVIEW_TEXT'];
			if($arFields["PREVIEW_PICTURE"]){
				$path = CFile::GetPath($arFields["PREVIEW_PICTURE"]);
				$arCompany['Company.SmallLogo'] = $path;
				if(!strpos($path, '://')){
					$path = ".." . $path;
				}
				$arCompany['Company.SmallLogoImg'] = '<img src="'.$path.'" border="0" />';
			}
			if($arFields["DETAIL_PICTURE"]){
				$path = CFile::GetPath($arFields["DETAIL_PICTURE"]);
				$arCompany['Company.BigLogo'] = $path;
				if(!strpos($path, '://')){
					$path = $_SERVER["DOCUMENT_ROOT"] . $path;
				}
				$arCompany['Company.BigLogoImg'] = '<img src="'.$path.'" border="0" />';
			}
			if($arFields['PROPERTY_' . $stamp . '_VALUE']){
				$path = CFile::GetPath($arFields["PROPERTY_" . $stamp . "_VALUE"]);
				$arCompany['Company.SEO_FACSIMILE'] = $path;
				if(!strpos($path, '://')){
					$path = ".." . $path;
				}
				$arCompany['Company.CEO_FACSIMILEImg'] = '<img src="'.$path.'" border="0" />';
			}
			if($arFields['PROPERTY_' . $ivstamp . '_VALUE']){
				$path = CFile::GetPath($arFields["PROPERTY_" . $ivstamp . "_VALUE"]);
				$arCompany['Company.INVOICE_FACSIMILE'] = $path;
				if(!strpos($path, '://')){
					$path = $_SERVER["DOCUMENT_ROOT"] . $path;
				}
				$arCompany['Company.INVOICE_FACSIMILEImg'] = '<img src="'.$path.'" border="0" />';
			}

		}
		return $arCompany;
	}//function GetCompanyData($ID)

	function SendFile($arFields){
		global $USER;
		$posting = new CPosting;
		$SEND_TO = $arFields['DocDesignerEmail'];
		if(is_array($SEND_TO)){
			if(count($SEND_TO) > 0) $SEND_TO = implode(',', $SEND_TO);
	    	else $SEND_TO = '';
		}
		if(empty($SEND_TO)){
			if($this->Add2Log == 'Y'){
				AddMessage2Log("Empty e-mail send to", "htmls.DocDesigner");
			}
		}

		if(!($from = COption::GetOptionString("crm", "mail")))
			$from=$USER->GetEmail();

		if(is_array($arFields['DocDesignerMessage'])){
			$msg = $arFields['DocDesignerMessage']['TEXT'];
		}
		else{
			$msg = $arFields['DocDesignerMessage'];
		}

		$pFields = Array(
			"FROM_FIELD" => $from,
			"TO_FIELD" => $SEND_TO,
			"SUBJECT" => $arFields['DocDesignerSubject'],
			"BODY_TYPE" => "html",
			"BODY" => nl2br($msg),
			"DIRECT_SEND" => "Y",
			"STATUS" => "D",
			"CHARSET" => 'Windows-1251',
			"SUBSCR_FORMAT" => "html"
		);
		$pID = $posting->Add($pFields);

		if($pID != false){
			foreach($arFields['DocDesignerFile'] as $fid){
				$arFile = CFile::MakeFileArray($fid);
				//$arFile = CFile::MakeFileArray(CFile::GetPath($fid));
                //$rsFile = CFile::GetByID($fid);
                //$arFile = $rsFile->Fetch();
				$file_id = CPosting::SaveFile($pID, $arFile);
				if($file_id===false){
					if($this->Add2Log == 'Y'){
						AddMessage2Log("Add file: " . $posting->LAST_ERROR, "htmls.DocDesigner");
					}
				}
			}
			//$arFile = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"] . $arFields['DocDesignerFile']);

			$posting->ChangeStatus($pID, "P");
			$res = $posting->SendMessage($pID, 0);
			if(!$res){
				if($this->Add2Log == 'Y'){
					AddMessage2Log("Send mail: " . $posting->LAST_ERROR, "htmls.DocDesigner");
				}
				return false;
			}
		}
		else{
			if($this->Add2Log == 'Y'){
				AddMessage2Log("Create mail: " . $posting->LAST_ERROR, "htmls.DocDesigner");
			}
			return false;
		}

		$res = CPosting::Delete($pID);
		//@unlink($_SERVER["DOCUMENT_ROOT"] . $arFields['DocDesignerFile']);
        return true;
	}//SendFile

	function RegisterFile($fname, $workflowId, $elementId = 0, $tmpName = ''){
		global $DB;
		$arFile = CFile::MakeFileArray($fname);
		$arFile["MODULE_ID"] = "htmls.docdesigner";
		$fid = CFile::SaveFile($arFile, "htmls.docdesigner");
		@unlink($fname);
		$res = $DB->Query("INSERT INTO htmls_docdesigner_files (bizproc_id,file_id,element_id,tmp_name) VALUES('".$workflowId."','".$fid."','".$elementId."','".$tmpName."')");
	}//RegisterFile

	function GetFName($bizproc){
		global $DB;
		$res = $DB->Query("SELECT id FROM htmls_docdesigner_templates WHERE bizproc='".$bizproc."'");
		$ar = $res->Fetch();
		return COption::GetOptionString("htmls.docdesigner", 'DOCDESIGNER_FNAMES_' . $ar['id'], false);
	}//GetFName

	function GetLink2Pdf($bpID){
		global $DB;
		$r = $DB->Query("SHOW TABLES LIKE 'htmls_docdesigner_files'");
		if($r->SelectedRowsCount() == 0) {
			return '0';
		}
		$res = $DB->Query("SELECT file_id FROM htmls_docdesigner_files WHERE bizproc_id = '".$bpID."'");
		if($res->SelectedRowsCount() > 0){
			$arPath = array();
			while($ar = $res->Fetch()){
				//$arPath[] = "/bitrix/admin/docdesigner_download.php?action=download&file=".$ar['file_id'];
				$arPath[$ar['file_id']] = 'PDF: ';
				//return $path;//CFile::GetPath($ar['file_id']);
			}
			return $arPath;
		}
		return '0';
	}//GetLink2Pdf

	function GetPath2Pdf($bpID){
		global $DB;
		$r = $DB->Query("SHOW TABLES LIKE 'htmls_docdesigner_files'");
		if($r->SelectedRowsCount() == 0) {
			return '0';
		}
		$res = $DB->Query("SELECT file_id FROM htmls_docdesigner_files WHERE bizproc_id = '".$bpID."' ORDER BY ID DESC");
		if($ar = $res->Fetch()){
			//$path = "/bitrix/admin/docdesigner_download.php?action=download&file=".$ar['file_id'];
			return CFile::GetPath($ar['file_id']);
		}
		return '0';
	}//GetLink2Pdf

	function GetLink2Pdf4Iblock($ID){
		global $DB;
		$r = $DB->Query("SHOW TABLES LIKE 'htmls_docdesigner_files'");
		if($r->SelectedRowsCount() == 0) {
			return false;
		}                                //tmp_name
		$res = $DB->Query("SELECT file_id FROM htmls_docdesigner_files WHERE element_id = '".$ID."'");
		if($res->SelectedRowsCount() > 0){
			$arPath = array();
			while($ar = $res->Fetch()){
				//$arPath[] = "/bitrix/admin/docdesigner_download.php?action=download&file=".$ar['file_id'];
				$arPath[$ar['file_id']] = 'PDF: ';//$ar['tmp_name'];
			}
			return $arPath;//CFile::GetPath($ar['file_id']);
		}
		return false;
	}//GetLink2Pdf4Iblock

	function OnBizprocDelete(){
		global $DB, $USER;
        $DEL = COption::GetOptionString('htmls.docdesigner', 'DOCDESIGNER_DELETE_DOCS_ON_DELETE_BIZPROC', 'N');
        if($_REQUEST['action'] == 'del_bizproc' && $DEL == 'Y'){
          $files = CDocDesignerContracts::GetContract($_REQUEST['id']);
          $discConverted = \Bitrix\Main\Config\Option::get('disk', 'successfully_converted', false);
          if($discConverted == 'Y'){
            //AddMessage2Log(print_r($files, true), "htmls.DocDesigner");
            foreach($files as $aFile){
              $file = \Bitrix\Disk\File::loadById($aFile['WEB_DAV_ID'], array('STORAGE'));
              //$file->markDeleted($USER->GetID());
              $file->delete($USER->GetID());
            }
          }
          $JOURNAL_ID = COption::GetOptionInt("htmls.docdesigner", 'DOCDESIGNER_DOCS_REGISTER_ID');
          $arFilter = array("IBLOCK_ID" => $JOURNAL_ID, "PROPERTY_BIZPROC_ID" => $_REQUEST['id']);//, "PROPERTY_OWN_ENTITIES" => $OWN_COMPANY);
    	  $result = CIBlockElement::GetList(Array(), $arFilter);
    	  while($obj = $result->GetNextElement()){
    	    $props = $obj->GetProperties();
            $LIB_ELEM_ID = $props['LINK2FILE']['VALUE'];
            $ob = $obj->GetFields();

            $DB->StartTransaction();
            if(!CIBlockElement::Delete($ob['ID'])){
          		$strWarning .= 'Error!';
          		$DB->Rollback();
            }
          	else
          		$DB->Commit();

            $DB->StartTransaction();
            if(!CIBlockElement::Delete($LIB_ELEM_ID)){
          		$strWarning .= 'Error!';
          		$DB->Rollback();
            }
          	else
          		$DB->Commit();
          }
        }
		$r = $DB->Query("SHOW TABLES LIKE 'htmls_docdesigner_files'");
		if($r->SelectedRowsCount() == 0) {
			return '0';
		}
		if($_REQUEST['action'] == 'del_bizproc'){//crm
			$res = $DB->Query("SELECT id,file_id FROM htmls_docdesigner_files WHERE bizproc_id = '".$_REQUEST['id']."'");
			if($ar = $res->Fetch()){
				CFile::Delete($ar['file_id']);
				$DB->Query("DELETE FROM htmls_docdesigner_files WHERE id = '".$ar['id']."'");
			}
		}
		elseif($_REQUEST['delete_bizproc_id']){//bizproc
			$res = $DB->Query("SELECT id,file_id FROM htmls_docdesigner_files WHERE bizproc_id = '".$_REQUEST['delete_bizproc_id']."'");
			if($ar = $res->Fetch()){
				CFile::Delete($ar['file_id']);
				$DB->Query("DELETE FROM htmls_docdesigner_files WHERE id = '".$ar['id']."'");
			}
		}
	}//OnBizprocDelete

	function GetProducts($OID, $invoice = false, $Type = 'D'){
		$arResult = false;
        $isCRM = IsModuleInstalled("crm");
        if($isCRM){
    		$res = CCrmProductRow::GetList(array(), $arFilter = array('OWNER_ID' => $OID, 'OWNER_TYPE' => $Type), false, false, array());
    		$cnt = 0;
    		while($arRow = $res->Fetch()){
    			if($invoice){
    				$cnt++;
    				$arResult[] = $cnt.';'.$arRow['PRODUCT_NAME'].';'.$arRow['QUANTITY'].';DD_UNIT;'.$arRow['PRICE'].';'.$arRow['PRODUCT_ID'];
    			}
    			else{//offer
    				$arResult[] = $arRow['PRODUCT_NAME'].';'.$arRow['QUANTITY'].';'.$arRow['PRICE'];
    			}

    		}
        }
		return $arResult;
	}//GetProducts

	function GetCrmProducts($OID, $Type = 'D'){
		if($OID == 'blank'){
			$arResult[1] = array('PRODUCT_NAME' => GetMessage('DOCDESIGNER_PRODUCT_BLANK').'1', 'QUANTITY' => 10, 'PRICE' => 1000);
			$arResult[2] = array('PRODUCT_NAME' => GetMessage('DOCDESIGNER_PRODUCT_BLANK').'2', 'QUANTITY' => 20, 'PRICE' => 1500);
			$arResult[3] = array('PRODUCT_NAME' => GetMessage('DOCDESIGNER_PRODUCT_BLANK').'3', 'QUANTITY' => 30, 'PRICE' => 2500);
			return $arResult;
		}
		$arResult = false;
        $isCRM = IsModuleInstalled("crm");
        if($isCRM){
    		$res = CCrmProductRow::GetList(array('SORT'=>'ASC'), $arFilter = array('OWNER_ID' => $OID, 'OWNER_TYPE' => $Type), false, false, array());
    		$n = 0;
    		while($arRow = $res->Fetch()){
    			$n++;
    			$arResult[$n] = $arRow;
    		}
        }
		return $arResult;
	}//GetProducts

	function OnListElemetDelete($Element_ID){
		global $DB;
		$IBLOCK_ID = COption::GetOptionInt("htmls.docdesigner", 'DOCDESIGNER_CONTRACTS_LIST_ID');

		$db_props = CIBlockElement::GetProperty($IBLOCK_ID, $Element_ID, array("sort" => "asc"), Array("CODE"=>"LINK2LIBRARY"));
		if($ar_props = $db_props->Fetch()){
			$FID = IntVal($ar_props["VALUE"]);
			CFile::Delete($FID);
		}
	}//OnListElemetDelete

	//$BPID - bizproc ID
	//$bpA - bool: true - select template from activity. false - edit template
	function GetTemplateList($BPID, $bpA = false){
		global $DB;
        //AddMessage2Log($BPID);
        //AddMessage2Log($bpA);
		$res = $DB->Query("SELECT * FROM htmls_docdesigner_templates WHERE bizproc = '".$BPID."'");
		$arTemp = array();
		while($arRow = $res->Fetch()){
			$arTemp[$arRow['id']] = $arRow['tmp_name'];
		}
		if($bpA){
			$res = $DB->Query("SELECT * FROM htmls_docdesigner_templates WHERE bizproc != '".$BPID."' AND common='Y'");
			$arCTemp = array();
			while($arRow = $res->Fetch()){
				$arcTemp[$arRow['id']] = $arRow['tmp_name'];
			}
			if(count($arcTemp) > 0){
				$arTemp['common'] = $arcTemp;
			}
		}
        else{
            $res = $DB->Query("SELECT * FROM htmls_docdesigner_templates WHERE bizproc != '".$BPID."' AND common='Y'");
			//$arCTemp = array();
            $arTemp[0] = '------------------------';
			while($arRow = $res->Fetch()){
				$arTemp[$arRow['id']] = $arRow['tmp_name'];
			}
			/*if(count($arcTemp) > 0){
				$arTemp['common'] = $arcTemp;
			}*/
        }
        //AddMessage2Log(print_r($arTemp, true));
		return $arTemp;
	}

	function GetTemplateName($TID){
		global $DB;
		$res = $DB->Query("SELECT * FROM htmls_docdesigner_templates WHERE id = '".$TID."'");
		while($arRow = $res->Fetch()){
			return $arRow['tmp_name'];
		}
		return "Template #ID=".$TID." is missing";
	}

	function GetTemplateAr($TID){

    	global $DB;
		$res = $DB->Query("SELECT tmp_name, orientation, common, params FROM htmls_docdesigner_templates WHERE id = '".$TID."'");
		while($arRow = $res->Fetch()){
			return $arRow;
		}
		return array("tmp_name"=>"Template #ID=".$TID." is missing");
	}

	function SetTemplateName($TID, $Name){
		global $DB;
		$res = $DB->Query("UPDATE htmls_docdesigner_templates SET tmp_name='".$Name."' WHERE id = '".$TID."'");
	}

	function GetNumeratorInfo($NumID, $Prefix = '', $OWN_COMPANY, $ContractID = 0){
		$arSelect = array("NAME", "PROPERTY_NUMBER_TEMPLATE", "PROPERTY_NUMBER_LENGTH", "PROPERTY_NUMBER_RESET", "PROPERTY_NUMBER_START", "PROPERTY_CONTRACT_UNIQUE", "PROPERTY_CORP_UNIQUE");
		$res = CIBlockElement::GetList(Array(), array("IBLOCK_ID"=> COption::GetOptionInt("htmls.docdesigner", 'DOCDESIGNER_NUMERATORS'), "ID" => $NumID), false, false, $arSelect);
		while($ob = $res->GetNextElement()){
			$arFields = $ob->GetFields();
			$NumTmp = $arFields["PROPERTY_NUMBER_TEMPLATE_VALUE"];
			$UsePrefix = strpos($NumTmp, "PREFIX")? "Y" : "N";
			$Reset = $arFields["PROPERTY_NUMBER_RESET_VALUE"];
			$NumLen = $arFields["PROPERTY_NUMBER_LENGTH_VALUE"];
			$NumStart = intval($arFields["PROPERTY_NUMBER_START_VALUE"]);
			$DocName = $arFields['NAME'];
			$ContUniq = $arFields["PROPERTY_CONTRACT_UNIQUE_VALUE"];
            $CorpUniq = $arFields["PROPERTY_CORP_UNIQUE_VALUE"];

			$arFilter = array("IBLOCK_ID" => COption::GetOptionInt("htmls.docdesigner", 'DOCDESIGNER_DOCS_REGISTER_ID'), "PROPERTY_NUMERATOR" => $NumID);//, "PROPERTY_OWN_ENTITIES" => $OWN_COMPANY);
			if($UsePrefix == 'Y'){
				$arFilter["PROPERTY_PREFIX"] = $Prefix;
			}
			if($Reset == 'Y'){
				$arFilter["PROPERTY_YEAR"] = date("Y", time());
			}
			if($ContractID > 0 && $ContUniq == 'Y'){
				$arFilter["PROPERTY_CONTRACT_ID"] = $ContractID;
			}
            if($CorpUniq != 'Y'){
				$arFilter["PROPERTY_OWN_ENTITIES"] = $OWN_COMPANY;
			}
			$result = CIBlockElement::GetList(Array("PROPERTY_NUMBER" => "DESC"), $arFilter, false, Array("nPageSize" => 1), Array("PROPERTY_NUMBER"));
			//no one doc
			if($result->SelectedRowsCount() == 0){
				if($NumStart == 0) $number = 1;
				else $number = $NumStart;
			}
			else{
				while($obj = $result->GetNextElement()){
					$aFields = $obj->GetFields();
					$number = $aFields['PROPERTY_NUMBER_VALUE'];
					$number++;
				}
			}

			$lNumLen = strlen($number);
			if($NumLen > 0 and $NumLen > $lNumLen){
				$delta = $NumLen - $lNumLen;
				$pref = '';
				//if($lNumLen < $len){
					for($i = 1; $i <= $delta; $i++){
						$pref .= '0';
					}
				//}
			}
			$arResult = array('NUMERATOR' => $NumID, 'DOC_NAME' => $DocName, 'NUMBER' => $number, 'YEAR' => date("Y", time()), 'SNUMBER' => $pref . $number, 'TEMPLATE' => $NumTmp, 'LENGTH' => $NumLen);
			return $arResult;
		}
	}

	//return crm user-field list
	function GetCrmFields($options = false){
		global $MESS;
		$arr[] = 'DOCDESIGNER_CRM_COMPANY_BANK_NAME';
		$arr[] = 'DOCDESIGNER_CRM_COMPANY_BANK_BIK';
		$arr[] = 'DOCDESIGNER_CRM_COMPANY_BANK_KS';
    	$arr[] = 'DOCDESIGNER_CRM_COMPANY_BANK_CITY';
		$arr[] = 'DOCDESIGNER_CRM_COMPANY_OGRN';
		$arr[] = 'DOCDESIGNER_CRM_COMPANY_INN';
		$arr[] = 'DOCDESIGNER_CRM_COMPANY_KPP';
		$arr[] = 'DOCDESIGNER_CRM_COMPANY_RS';
		$arr[] = 'DOCDESIGNER_CRM_COMPANY_OGRN';
		$arr[] = 'DOCDESIGNER_CRM_COMPANY_POST_ADDRESS';
		$arr[] = 'DOCDESIGNER_CRM_COMPANY_PHONE';
		foreach($arr as $opt){
			$arSys[] = COption::GetOptionString("htmls.docdesigner", $opt);
		}

		$arDeal['DATE_CREATE'] = GetMessage('DATE_CREATE');
		//$arDeal['OPENED'] = GetMessage('OPENED');
		$arDeal['STAGE_ID'] = GetMessage('STAGE_ID');
		$arDeal['CLOSED'] = GetMessage('CLOSED');
		$arDeal['TYPE_ID'] = GetMessage('TYPE_ID');
		$arDeal['OPPORTUNITY'] = GetMessage('OPPORTUNITY');
		$arDeal['CURRENCY_ID'] = GetMessage('CURRENCY_ID');
		$arDeal['PROBABILITY'] = GetMessage('PROBABILITY');
		$arDeal['COMMENTS'] = GetMessage('COMMENTS');
		$arDeal['BEGINDATE'] = GetMessage('BEGINDATE');
		$arDeal['CLOSEDATE'] = GetMessage('CLOSEDATE');
		$arDeal['TITLE'] = GetMessage('TITLE');
		$arDeal['DEAL.TITLE'] = GetMessage('TITLE');
		$arDeal['DEAL.DATE_CREATE'] = GetMessage('DATE_CREATE');
		$arDeal['DEAL.BEGINDATE'] = GetMessage('BEGINDATE');
		$arDeal['DEAL.CLOSEDATE'] = GetMessage('CLOSEDATE');
		//$arDeal['DEAL.OPENED'] = GetMessage('OPENED');
		$arDeal['DEAL.TYPE_ID'] = GetMessage('TYPE_ID');
		$arDeal['DEAL.OPPORTUNITY'] = GetMessage('OPPORTUNITY');
		$arDeal['DEAL.CURRENCY_ID'] = GetMessage('CURRENCY_ID');
		$arDeal['DEAL.PROBABILITY'] = GetMessage('PROBABILITY');
		$arDeal['DEAL.COMMENTS'] = GetMessage('COMMENTS');
		$arDeal['DEAL.STAGE_ID'] = GetMessage('STAGE_ID');
		$arDeal['DEAL.CLOSED'] = GetMessage('CLOSED');
        $isCRM = IsModuleInstalled("crm");
        if($isCRM){
    		$res=CCrmFieldMulti::GetEntityTypes();
    	    foreach($res as $pref => $arr){
    	    	foreach($arr as $k => $v){
    	    		$arCrmMulti[$pref."_".$k] =	$v['FULL'];
    	    	}
    	    }
        }
        $DealFiles = COption::GetOptionString("htmls.docdesigner", 'DOCDESIGNER_CRM_DISK_DEAL');
		$CompanyFiles = COption::GetOptionString("htmls.docdesigner", 'DOCDESIGNER_CRM_DISK_COMPANY');
		$ContactFiles = COption::GetOptionString("htmls.docdesigner", 'DOCDESIGNER_CRM_DISK_CONTACT');

		$res = CAllUserTypeEntity::GetList(array(),array('ENTITY_ID'=>'CRM_COMPANY', 'LANG'=>'ru'));
		while($ar = $res->Fetch()){
		  //AddMessage2Log(print_r($ar, 1));
			if($ar['XML_ID'] == 'HTMLS_DOCS') continue;
			if($ar['FIELD_NAME'] == $DealFiles || $ar['FIELD_NAME'] == $CompanyFiles || $ar['FIELD_NAME'] == $ContactFiles) continue;
			if(in_array($ar['FIELD_NAME'], $arSys)) continue;
			$arResult['COMPANY'][$ar['FIELD_NAME']] = array('USER_TYPE'=>$ar['USER_TYPE_ID'], 'NAME'=>htmlspecialchars($ar['EDIT_FORM_LABEL'], ENT_QUOTES, LANG_CHARSET),
			//$arResult['COMPANY'][$ar['FIELD_NAME']] = array('USER_TYPE'=>$ar['USER_TYPE_ID'], 'NAME'=>($ar['EDIT_FORM_LABEL']),
					'ENTITY_TYPE' => $ar['SETTINGS']['ENTITY_TYPE']);

			if($ar['SETTINGS']['IBLOCK_ID'] > 0){
				$arResult['DEAL'][$ar['FIELD_NAME']]['IBLOCK_ID'] = $ar['SETTINGS']['IBLOCK_ID'];
				$properties = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>$ar['SETTINGS']['IBLOCK_ID']));
				$arProps = array();
				while ($prop_fields = $properties->GetNext()){
					//if($ar['FIELD_NAME'] == $DealFiles or $ar['FIELD_NAME'] == $CompanyFiles) continue;
					if($prop_fields["CODE"])
						$arProps[$prop_fields["CODE"]] = $prop_fields['NAME'];
					else
						$arProps[$prop_fields["ID"]] = $prop_fields['NAME'];
				}
				if(count($arProps) > 0){
					$arResult['COMPANY'][$ar['FIELD_NAME']]['PROPERTIES'] = $arProps;
				}
			}
		}
        if(!$options){
            $arResult['COMPANY']['COMPANY.ADDRESS'] = GetMessage('ADDRESS');
        	$arResult['COMPANY']['COMPANY.ADDRESS_2'] = GetMessage('ADDRESS_2');
        	$arResult['COMPANY']['COMPANY.ADDRESS_CITY'] = GetMessage('ADDRESS_CITY');
        	$arResult['COMPANY']['COMPANY.ADDRESS_REGION'] = GetMessage('ADDRESS_REGION');
        	$arResult['COMPANY']['COMPANY.ADDRESS_PROVINCE'] = GetMessage('ADDRESS_PROVINCE');
        	$arResult['COMPANY']['COMPANY.ADDRESS_POSTAL_CODE'] = GetMessage('ADDRESS_POSTAL_CODE');
        	$arResult['COMPANY']['COMPANY.ADDRESS_COUNTRY'] = GetMessage('ADDRESS_COUNTRY');

            $arResult['COMPANY']['COMPANY.REG_ADDRESS'] = GetMessage('REG_ADDRESS');
        	$arResult['COMPANY']['COMPANY.REG_ADDRESS_2'] = GetMessage('REG_ADDRESS_2');
        	$arResult['COMPANY']['COMPANY.REG_ADDRESS_CITY'] = GetMessage('REG_ADDRESS_CITY');
        	$arResult['COMPANY']['COMPANY.REG_ADDRESS_REGION'] = GetMessage('REG_ADDRESS_REGION');
        	$arResult['COMPANY']['COMPANY.REG_ADDRESS_PROVINCE'] = GetMessage('REG_ADDRESS_PROVINCE');
        	$arResult['COMPANY']['COMPANY.REG_ADDRESS_POSTAL_CODE'] = GetMessage('REG_ADDRESS_POSTAL_CODE');
        	$arResult['COMPANY']['COMPANY.REG_ADDRESS_COUNTRY'] = GetMessage('REG_ADDRESS_COUNTRY');
        	$arResult['COMPANY']['COMPANY.COMPANY_TYPE'] = GetMessage('COMPANY_TYPE');
        	$arResult['COMPANY']['COMPANY.INDUSTRY'] = GetMessage('INDUSTRY');
        	$arResult['COMPANY']['COMPANY.EMPLOYEES'] = GetMessage('EMPLOYEES');
        	$arResult['COMPANY']['COMPANY.REVENUE'] = GetMessage('REVENUE');
        }

		$res = CAllUserTypeEntity::GetList(array(),array('ENTITY_ID'=>'CRM_DEAL', 'LANG'=>'ru'));
		while($ar = $res->Fetch()){
			if($ar['XML_ID'] == 'HTMLS_DOCS') continue;
			if($ar['FIELD_NAME'] == $DealFiles || $ar['FIELD_NAME'] == $CompanyFiles || $ar['FIELD_NAME'] == $ContactFiles) continue;
            //AddMessage2Log(print_r($ar, true), __LINE__."/htmls.DocDesigner");

			$arResult['DEAL'][$ar['FIELD_NAME']] = array('USER_TYPE'=>$ar['USER_TYPE_ID'], 'NAME'=>$ar['EDIT_FORM_LABEL'],
					'ENTITY_TYPE' => $ar['SETTINGS']['ENTITY_TYPE'], 'MULTIPLE'=>$ar['MULTIPLE']);
            if($ar['USER_TYPE_ID'] == 'crm'){
              foreach($ar['SETTINGS'] as $_type => $_val){
                if($_val == 'Y'){
                  $arResult['DEAL'][$ar['FIELD_NAME']]['CRM_TYPE'] = $_type;
                }
              }
            }
			if($ar['SETTINGS']['IBLOCK_ID'] > 0){
				$arResult['DEAL'][$ar['FIELD_NAME']]['IBLOCK_ID'] = $ar['SETTINGS']['IBLOCK_ID'];
				$properties = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>$ar['SETTINGS']['IBLOCK_ID']));
				$arProps = array("PREVIEW_PICTURE" => ("PREVIEW_PICTURE"), "DETAIL_PICTURE" => ("DETAIL_PICTURE"));
				while ($prop_fields = $properties->GetNext()){
					//if($ar['FIELD_NAME'] == $DealFiles or $ar['FIELD_NAME'] == $CompanyFiles) continue;
					if($prop_fields["CODE"])
						$arProps[$prop_fields["CODE"]] = $prop_fields['NAME'];
					else
						$arProps[$prop_fields["ID"]] = $prop_fields['NAME'];
				}
				if(count($arProps) > 0){
					$arResult['DEAL'][$ar['FIELD_NAME']]['PROPERTIES'] = $arProps;
				}
			}
		}
		if(!$options){
			foreach($arDeal as $key => $val){
				$arResult['DEAL'][$key] = $val;
			}

			//$arResult['CONTACT']['CONTACT.HONORIFIC'] = GetMessage('HONORIFIC');

			$arResult['CONTACT']['CONTACT.LAST_NAME'] = GetMessage('LAST_NAME');
            $arResult['CONTACT']['CONTACT.NAME'] = GetMessage('NAME');
            $arResult['CONTACT']['CONTACT.SECOND_NAME'] = GetMessage('SECOND_NAME');
			$arResult['CONTACT']['CONTACT.FULL_NAME'] = GetMessage('FULL_NAME');
			$arResult['CONTACT']['CONTACT.FULL_FIO'] = GetMessage('FULL_FIO');
			$arResult['CONTACT']['CONTACT.FULL_NAME_SHORT'] = GetMessage('FULL_NAME_SHORT');
			$arResult['CONTACT']['CONTACT.HONORIFIC'] = GetMessage('HONORIFIC');
            $arResult['CONTACT']['CONTACT.TYPE_ID'] = GetMessage('CONTACT_TYPE_ID');
			$arResult['CONTACT']['CONTACT.SOURCE_ID'] = GetMessage('CONTACT_SOURCE_ID');
			$arResult['CONTACT']['CONTACT.BIRTHDATE'] = GetMessage('BIRTH_DATE');
			$arResult['CONTACT']['CONTACT.POST'] = GetMessage('POST');
            $arResult['CONTACT']['CONTACT.ADDRESS'] = GetMessage('ADDRESS');
			$arResult['CONTACT']['CONTACT.ADDRESS_2'] = GetMessage('ADDRESS_2');
			$arResult['CONTACT']['CONTACT.ADDRESS_CITY'] = GetMessage('ADDRESS_CITY');
			$arResult['CONTACT']['CONTACT.ADDRESS_REGION'] = GetMessage('ADDRESS_REGION');
			$arResult['CONTACT']['CONTACT.ADDRESS_PROVINCE'] = GetMessage('ADDRESS_PROVINCE');
			$arResult['CONTACT']['CONTACT.ADDRESS_POSTAL_CODE'] = GetMessage('ADDRESS_POSTAL_CODE');
			$arResult['CONTACT']['CONTACT.ADDRESS_COUNTRY'] = GetMessage('ADDRESS_COUNTRY');
			$arResult['CONTACT']['CONTACT.RQ_IDENT_DOC'] = GetMessage('RQ_IDENT_DOC');
			$arResult['CONTACT']['CONTACT.RQ_IDENT_DOC_SER'] = GetMessage('RQ_IDENT_DOC_SER');
			$arResult['CONTACT']['CONTACT.RQ_IDENT_DOC_NUM'] = GetMessage('RQ_IDENT_DOC_NUM');
			$arResult['CONTACT']['CONTACT.RQ_IDENT_DOC_DATE'] = GetMessage('RQ_IDENT_DOC_DATE');
			$arResult['CONTACT']['CONTACT.RQ_IDENT_DOC_ISSUED_BY'] = GetMessage('RQ_IDENT_DOC_ISSUED_BY');
			$arResult['CONTACT']['CONTACT.RQ_IDENT_DOC_DEP_CODE'] = GetMessage('RQ_IDENT_DOC_DEP_CODE');


			foreach($arCrmMulti as $VarName => $varDesc){
				$arResult['CONTACT']['CONTACT.'.$VarName] = $varDesc;
			}
			//mail("9619045@gmail.com", "contact", print_r($arResult['CONTACT'], true));

			foreach($arCrmMulti as $VarName => $varDesc){
				$arResult['COMPANY']['COMPANY.'.$VarName] = $varDesc;
			}

            $arResult['LEAD']['DATE_CREATE'] = GetMessage('DATE_CREATE');
        	//$arResult['LEAD']['STATUS_ID'] = GetMessage('STATUS_ID');
        	$arResult['LEAD']['OPPORTUNITY'] = GetMessage('OPPORTUNITY');
        	$arResult['LEAD']['CURRENCY_ID'] = GetMessage('CURRENCY_ID');
        	$arResult['LEAD']['COMMENTS'] = GetMessage('COMMENTS');
        	$arResult['LEAD']['TITLE'] = GetMessage('LEAD_TITLE');
        	$arResult['LEAD']['COMPANY_TITLE'] = GetMessage('COMPANY_TITLE');
        	$arResult['LEAD']['LAST_NAME'] = GetMessage('LAST_NAME');
        	$arResult['LEAD']['NAME'] = GetMessage('NAME');
        	$arResult['LEAD']['SECOND_NAME'] = GetMessage('SECOND_NAME');
            $arResult['LEAD']['LEAD.DATE_CREATE'] = GetMessage('DATE_CREATE');
        	//$arResult['LEAD']['LEAD.STATUS_ID'] = GetMessage('STATUS_ID');
        	$arResult['LEAD']['OPPORTUNITY'] = GetMessage('OPPORTUNITY');
        	$arResult['LEAD']['LEAD.CURRENCY_ID'] = GetMessage('CURRENCY_ID');
        	$arResult['LEAD']['LEAD.COMMENTS'] = GetMessage('COMMENTS');
        	$arResult['LEAD']['LEAD.TITLE'] = GetMessage('LEAD_TITLE');
        	$arResult['LEAD']['LEAD.COMPANY_TITLE'] = GetMessage('COMPANY_TITLE');
        	$arResult['LEAD']['LEAD.LAST_NAME'] = GetMessage('LAST_NAME');
        	$arResult['LEAD']['LEAD.NAME'] = GetMessage('NAME');
        	$arResult['LEAD']['LEAD.SECOND_NAME'] = GetMessage('SECOND_NAME');
		}

		$res = CAllUserTypeEntity::GetList(array(),array('ENTITY_ID'=>'CRM_CONTACT', 'LANG'=>'ru'));
		while($ar = $res->Fetch()){
		  if($ar['FIELD_NAME'] == $DealFiles || $ar['FIELD_NAME'] == $CompanyFiles || $ar['FIELD_NAME'] == $ContactFiles) continue;
		  //AddMessage2Log(print_r($ar, 1));
			$arResult['CONTACT'][$ar['FIELD_NAME']] = array('USER_TYPE'=>$ar['USER_TYPE_ID'], 'NAME'=>$ar['EDIT_FORM_LABEL'],
					'ENTITY_TYPE' => $ar['SETTINGS']['ENTITY_TYPE']);

			if($ar['SETTINGS']['IBLOCK_ID'] > 0){
				$arResult['DEAL'][$ar['FIELD_NAME']]['IBLOCK_ID'] = $ar['SETTINGS']['IBLOCK_ID'];
				$properties = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>$ar['SETTINGS']['IBLOCK_ID']));
				$arProps = array();
				while ($prop_fields = $properties->GetNext()){
					//if($ar['FIELD_NAME'] == $DealFiles or $ar['FIELD_NAME'] == $CompanyFiles) continue;
					if($prop_fields["CODE"])
						$arProps[$prop_fields["CODE"]] = $prop_fields['NAME'];
					else
						$arProps[$prop_fields["ID"]] = $prop_fields['NAME'];
				}
				if(count($arProps) > 0){
					$arResult['CONTACT'][$ar['FIELD_NAME']]['PROPERTIES'] = $arProps;
				}
			}
		}

        $res = CAllUserTypeEntity::GetList(array(),array('ENTITY_ID'=>'CRM_LEAD', 'LANG'=>'ru'));
		while($ar = $res->Fetch()){
			$arResult['LEAD'][$ar['FIELD_NAME']] = array('USER_TYPE'=>$ar['USER_TYPE_ID'], 'NAME'=>$ar['EDIT_FORM_LABEL'],
					'ENTITY_TYPE' => $ar['SETTINGS']['ENTITY_TYPE'], 'SETTINGS' => $ar['SETTINGS'], 'field'=>$ar);

			if($ar['SETTINGS']['IBLOCK_ID'] > 0){
				$arResult['DEAL'][$ar['FIELD_NAME']]['IBLOCK_ID'] = $ar['SETTINGS']['IBLOCK_ID'];
				$properties = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>$ar['SETTINGS']['IBLOCK_ID']));
				$arProps = array();
				while ($prop_fields = $properties->GetNext()){
					//if($ar['FIELD_NAME'] == $DealFiles or $ar['FIELD_NAME'] == $CompanyFiles) continue;
					if($prop_fields["CODE"])
						$arProps[$prop_fields["CODE"]] = $prop_fields['NAME'];
					else
						$arProps[$prop_fields["ID"]] = $prop_fields['NAME'];
				}
				if(count($arProps) > 0){
					$arResult['LEAD'][$ar['FIELD_NAME']]['PROPERTIES'] = $arProps;
				}
			}
		}
        /*
        $arResult['LEAD']['DATE_CREATE'] = GetMessage('DATE_CREATE');
    	$arResult['LEAD']['STATUS_ID'] = GetMessage('STATUS_ID');
    	$arResult['LEAD']['OPPORTUNITY'] = GetMessage('OPPORTUNITY');
    	$arResult['LEAD']['CURRENCY_ID'] = GetMessage('CURRENCY_ID');
    	$arResult['LEAD']['COMMENTS'] = GetMessage('COMMENTS');
    	$arResult['LEAD']['TITLE'] = GetMessage('LEAD_TITLE');
    	$arResult['LEAD']['COMPANY_TITLE'] = GetMessage('COMPANY_TITLE');
    	$arResult['LEAD']['LAST_NAME'] = GetMessage('LAST_NAME');
    	$arResult['LEAD']['NAME'] = GetMessage('NAME');
    	$arResult['LEAD']['SECOND_NAME'] = GetMessage('SECOND_NAME');
        */
		return $arResult;
	}//GetCrmFields

	function OnAfterCrmCompanyAdd($arFields){
		$IBLOCK_ID = COption::GetOptionString("htmls.docdesigner", 'DOCDESIGNER_CRM_COMPAMY_IBLOCK_ID');
		$arSelect = Array("ID", "NAME");
		$arFilter = Array("IBLOCK_ID"=>IntVal($IBLOCK_ID), "XML_ID"=> $arFields['ID']);
		$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
		if($res->SelectedRowsCount() == 0){
			$el = new CIBlockElement;
			$arLoadProductArray = Array(
						"IBLOCK_SECTION_ID" => false,
						"IBLOCK_ID"      => $IBLOCK_ID,
						"NAME"           => $arFields['TITLE'],
						"ACTIVE"         => "Y",
						"XML_ID"   		 => $arFields['ID'],
			);
			$el->Add($arLoadProductArray);
		}
	}

	function OnAfterCrmCompanyUpdate($arFields){
		$IBLOCK_ID = COption::GetOptionString("htmls.docdesigner", 'DOCDESIGNER_CRM_COMPAMY_IBLOCK_ID');
		$el = new CIBlockElement;
		$arSelect = Array("ID", "NAME");
		$arFilter = Array("IBLOCK_ID"=>IntVal($IBLOCK_ID), "XML_ID"=> $arFields['ID']);
		$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
		if($ob = $res->GetNextElement()){
			$ibFields = $ob->GetFields();
			if($arFields['TITLE'] != $ibFields['NAME']){
				$res = $el->Update($ibFields['ID'], array('NAME' => $arFields['TITLE']));
			}
		}
		else{
			$arLoadProductArray = Array(
					"IBLOCK_SECTION_ID" => false,
					"IBLOCK_ID"      => $IBLOCK_ID,
					"NAME"           => $arFields['TITLE'],
					"ACTIVE"         => "Y",
					"XML_ID"   		 => $arFields['ID'],
				);
			$el->Add($arLoadProductArray);
		}
	}

	function OnBeforeCrmCompanyDelete($ID){
		global $DB;
		$IBLOCK_ID = COption::GetOptionString("htmls.docdesigner", 'DOCDESIGNER_CRM_COMPAMY_IBLOCK_ID');
		$el = new CIBlockElement;
		$arSelect = Array("ID", "NAME");
		$arFilter = Array("IBLOCK_ID"=>IntVal($IBLOCK_ID), "XML_ID"=>$ID);
		$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
		if($ob = $res->GetNextElement()){
			$ibFields = $ob->GetFields();
			$DB->StartTransaction();
			if(!CIBlockElement::Delete($ibFields['ID'])){
				$DB->Rollback();
			}
			else
				$DB->Commit();
		}
	}

//end class CDocDesignerProcessing
}
?>