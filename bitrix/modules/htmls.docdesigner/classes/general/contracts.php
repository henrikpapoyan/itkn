<?
/*******************************************************************************
* Module:   DocDesigner                                                        *
* Class:    CDocDesignerContracts extends CDocDesigner                         *
* Version:  8.4.11                                                             *
* Author:   Alexey Andreev, HTMLStudio (www.htmls.ru)                          *
* License:  Shareware                                                          *
* Date:     2017-03-01                                                         *
* e-mail:   aav@htmls.ru                                                       *
*******************************************************************************/
if(COption::GetOptionString('htmls.docdesigner', 'DOCDESIGNER_ADDMESSAGE2LOG') == 'Y'){
	define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/bitrix/tmp/DocDesigner.txt");
}
IncludeModuleLangFile(__FILE__);
class CDocDesignerContracts extends CDocDesigner{
	function CDocDesignerContracts(){
		//$this->AUTO_NUMBER = COption::GetOptionString("htmls.docdesigner", 'DOCDESIGNER_CONTRACTS_AUTO_NUMBER', 'Y');
		//$this->RESET_NUMBER = COption::GetOptionString("htmls.docdesigner", 'DOCDESIGNER_CONTRACTS_RESET_NUMBER', 'Y');
		//$this->LENGTH_NUMBER = intval(COption::GetOptionString("htmls.docdesigner", 'DOCDESIGNER_CONTRACTS_LENGTH_NUMBER', '3'));
		//$this->START_NUMBER = intval(COption::GetOptionString("htmls.docdesigner", 'DOCDESIGNER_CONTRACTS_START_NUMBER', '1'));
		//$this->SAVE2LIBRARY = COption::GetOptionString("htmls.docdesigner", 'DOCDESIGNER_CONTRACTS_SAVE2LIBRARY', 'N');
		$this->Add2Log = COption::GetOptionString('htmls.docdesigner', 'DOCDESIGNER_ADDMESSAGE2LOG');
	}

	function GetLastContractNumber($Prefix){
		if($this->AUTO_NUMBER == 'N') return '';
        //prefix?!
		$IBLOCK_ID = COption::GetOptionInt("htmls.docdesigner", 'DOCDESIGNER_CONTRACTS_LIST_ID');
		$arSelect = Array("ID", "PROPERTY_NUMBER");
		$arFilter = Array("IBLOCK_ID" => $IBLOCK_ID, "PROPERTY_PREFIX" => $Prefix);

    	if($this->RESET_NUMBER == 'Y')
			$arFilter['PROPERTY_YEAR'] = date("Y", time());

		$start = $this->START_NUMBER;
		$len = $this->LENGTH_NUMBER;
		$res = CIBlockElement::GetList(Array("PROPERTY_NUMBER" => "DESC"), $arFilter, false, Array("nPageSize" => 1), $arSelect);
		//no one contract
		if($res->SelectedRowsCount() == 0){
			if($start == 0) $number = 1;
			else $number = $start;
		}
		else{
			while($ob = $res->GetNextElement()){
				$arFields = $ob->GetFields();
				$number = $arFields['PROPERTY_NUMBER_VALUE'];
				$number++;
			}
		}

		$NumLen = strlen($number);
		$delta = $len - $NumLen;
		$pref = '';
		if($NumLen < $len){
			for($i = 1; $i <= $delta; $i++){
				$pref .= '0';
			}
		}
		$arResult = array('NUMBER' => $number, 'YEAR' => date("Y", time()), 'STR_NUMBER' => $pref . $number);
		return $arResult;
	}//GetLastContractNumber

    function createTmpList(){
      CModule::IncludeModule("iblock");
	  CModule::IncludeModule("lists");
      $check = CIBlock::GetList(Array(),Array('TYPE'=>'lists','CODE'=>'htmls_tmp_register'), true);
      if($check->SelectedRowsCount() == 0){
        $rsSites = CSite::GetList($by="sort", $order="desc", Array());
		while ($arSite = $rsSites->Fetch()){
			$sites[] = $arSite['ID'];
		}
		$ib = new CIBlock;
        $ibp = new CIBlockProperty;

        $arFields = Array(
      		"ACTIVE" => 'Y',
      		"NAME" => GetMessage('DOC_DESIGNER_TEMPLATES'),
      		"CODE" => 'htmls_tmp_register',
      		"IBLOCK_TYPE_ID" => 'lists',
      		"SITE_ID" => $sites,
        );
        if($tID = $ib->Add($arFields)){
          COption::SetOptionInt("htmls.docdesigner", 'DOCDESIGNER_TMP_REGISTER_LIST', $tID);
          $Field = new CListElementField($tID, 'NAME', GetMessage('DDS_NAME'), 20);

    	  $arProps = array('FILE' => array(GetMessage('DR_FILE'), 'F', 80));
          foreach($arProps as $CODE => $v){
    			$arFields = Array(
    				"NAME" => $v[0],
    				"ACTIVE" => "Y",
    				"SORT" => $v[2],
    				"CODE" => $CODE,
    				"PROPERTY_TYPE" => $v[1],
    				"IBLOCK_ID" => $tID,
    				'LINK_IBLOCK_ID' => $v[3]
    			);

    			$PropID = $ibp->Add($arFields);
    			$Field = new CListElementField($tID, 'PROPERTY_' . $PropID, $v[0], $v[2]);
    			if($CODE == 'FILE'){
    				COption::SetOptionInt("htmls.docdesigner", 'DOCDESIGNER_TMP_REGISTER_FILE', $PropID);
    			}
    	  }
          return $tID;
        }
      }
      else{
        while($ar_res = $check->Fetch()){
  	        COption::SetOptionInt("htmls.docdesigner", 'DOCDESIGNER_TMP_REGISTER_LIST', $ar_res['ID']);
            return $ar_res['ID'];
  	    }
      }
    }

    function getTmpList($selected){
      $tmpListId = COption::GetOptionInt("htmls.docdesigner", 'DOCDESIGNER_TMP_REGISTER_LIST', 0);
      $tmpFileId = COption::GetOptionInt("htmls.docdesigner", 'DOCDESIGNER_TMP_REGISTER_FILE', 0);
      if($tmpListId == 0){
        $tmpListId = self::createTmpList();
      }
      $select = '<select name="docxTemplate">';
      $arSelect = Array("ID", "NAME");
      $arFilter = Array("IBLOCK_ID"=>IntVal($tmpListId), "ACTIVE"=>"Y");
      $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
      if($res->SelectedRowsCount() == 0){
        return GetMessage('NO_ONE_TMP');
      }
      $select .= '<option value="0">'.GetMessage('NO_SELECT').'</option>';
      while($ob = $res->GetNextElement()){
       $arFields = $ob->GetFields();
       if($selected == $arFields['ID'])
            $select .= '<option selected value="'.$arFields['ID'].'">'.$arFields['NAME'].'</option>';
       else
            $select .= '<option value="'.$arFields['ID'].'">'.$arFields['NAME'].'</option>';
      }
      $select .= '</select>';
      return $select;
    }

	function GetDocTemplate($TemplateID, $tmpFromList = 0){
	  global $DB;
      if(intval($TemplateID) == 0) return false;
      CModule::IncludeModule('iblock');
      $discConverted = \Bitrix\Main\Config\Option::get('disk', 'successfully_converted', false);
      if($discConverted != 'Y'){
        $db_props = CIBlockElement::GetProperty(COption::GetOptionString("htmls.docdesigner", 'DOCDESIGNER_CONTRACTS_TEMPLATES_LIBRARY'), $TemplateID, array("sort" => "asc"), Array("CODE"=>"FILE"));
    	if($ar_props = $db_props->Fetch()){
			$FILE_ID = IntVal($ar_props["VALUE"]);
			return CFile::GetPath($FILE_ID);
		}
		else{
			return false;//die('WRONG TEMPLATES LIBRARY');
		}
      }

      if($tmpFromList == 1){
        $tmpListId = COption::GetOptionInt("htmls.docdesigner", 'DOCDESIGNER_TMP_REGISTER_LIST', 0);
        $tmpFileId = COption::GetOptionInt("htmls.docdesigner", 'DOCDESIGNER_TMP_REGISTER_FILE', 0);
        $db_props = CIBlockElement::GetProperty($tmpListId, $TemplateID, array("sort" => "asc"), Array("CODE"=>"FILE"));
    	if($ar_props = $db_props->Fetch()){
			$FILE_ID = IntVal($ar_props["VALUE"]);
      	return CFile::GetPath($FILE_ID);
		}
		else{
			return false;//die('WRONG TEMPLATES LIBRARY');
		}
      }

      $htmlsDiscConverted = \Bitrix\Main\Config\Option::get('htmls.docdesigner', 'successfully_converted', false);
      //$check = CIBlock::GetList(Array(),Array('TYPE'=>'lists','CODE'=>'htmls_tmp_register'), true);
      $check = CIBlockElement::GetList(Array(),Array('CODE'=>'htmls_tmp_register'));
      $tmpList = $check->SelectedRowsCount();
      if($htmlsDiscConverted == 'Y' && $tmpList == 0){
        $diskId = COption::GetOptionString("htmls.docdesigner", 'DOCDESIGNER_CONTRACTS_TEMPLATES_LIBRARY');
        //$strSql = 'SELECT * FROM  b_disk_object WHERE XML_ID=' . $TemplateID . ' AND STORAGE_ID='.$diskId;
        $strSql = 'SELECT * FROM  b_disk_object WHERE REAL_OBJECT_ID=' . $TemplateID;// . ' AND STORAGE_ID='.$diskId;
        //AddMessage2Log($strSql, "htmls.DocDesigner");
        $resDb = $DB->Query($strSql);
        while ($arRes = $resDb->Fetch()){
		    $fileId = $arRes['FILE_ID'];
            return CFile::GetPath($fileId);
		}
        return false;
      }
      else{
        $diskId = COption::GetOptionString("htmls.docdesigner", 'DOCDESIGNER_CONTRACTS_TEMPLATES_LIBRARY');
        $strSql = 'SELECT * FROM  b_disk_storage WHERE ID=' . $diskId;
        $resDb = $DB->Query($strSql);
        $arRes = $resDb->Fetch();
        $iblockId = $arRes['XML_ID'];
		$db_props = CIBlockElement::GetProperty($iblockId, $TemplateID, array("sort" => "asc"), Array("CODE"=>"FILE"));
    	if($ar_props = $db_props->Fetch()){
			$FILE_ID = IntVal($ar_props["VALUE"]);
			return CFile::GetPath($FILE_ID);
		}
		else{
			return false;//die('WRONG TEMPLATES LIBRARY');
		}
      }
	}

	function GetBankInfo($BANK_ID){
		$arSelect = Array("NAME", "PROPERTY_BIK", "PROPERTY_KS", "PROPERTY_CITY");
		$arFilter = Array("ID"=>IntVal($BANK_ID));
		$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
		while($ob = $res->GetNextElement()){
			$arFields = $ob->GetFields();
			$arBank['KS'] = $arFields["PROPERTY_KS_VALUE"];
			$arBank['BIK'] = $arFields["PROPERTY_BIK_VALUE"];
			$arBank['NAME'] = $arFields["NAME"];
			$arBank['CITY'] = $arFields["PROPERTY_CITY_VALUE"];
		}
        return $arBank;
	}

	//return info for own company from lists
	function GetOwnCompanyData($CID, $arSelect = array()){
		$IBLOCK_ID = COption::GetOptionString("htmls.docdesigner", "DOCDESIGNER_COMPANY");
		$arFilter = Array("ID"=>IntVal($CID), 'IBLOCK_ID' => $IBLOCK_ID);
		$res = CIBlockElement::GetList(Array(), $arFilter);//, false, false, $arSelect1);
		$arCompany = array();
		while($ob = $res->GetNextElement()){
			$arFields = $ob->GetFields();
			$arProps = $ob->GetProperties();
			foreach($arProps as $pid => $arValue){
				if(array_key_exists($arValue['ID'], $arSelect)){
					$arCompany["PROPERTY_" . $arValue['ID']] = $arValue['VALUE'];
				}
				else{
					if($arValue['CODE']){
						$arCompany[$arValue['CODE']] = $arValue['VALUE'];
						$arCompany["OWN_COMPANY." . $arValue['CODE']] = $arValue['VALUE'];
					}
					else{
						$arCompany[$arValue['ID']] = $arValue['VALUE'];
						$arCompany["OWN_COMPANY." . $arValue['ID']] = $arValue['VALUE'];
					}
				}
			}
			/*foreach($arSelect as $k => $pid){
				$arCompany[$pid] = $arFields["PROPERTY_" . $k . "_VALUE"];
			}*/
			$arCompany['NAME'] = $arFields['NAME'];
		}
		return $arCompany;
	}

	//add new contract into register (list)
	function RegisterContract($COMPANY_ID, $DEAL_ID, $arNum, $SUM, $BP_ID, $fname, $table, $ContractID = 0, $arBizProc = array(), $arCrmId){
	  //AddMessage2Log(print_r($arCrmId, true), "htmls.DocDesigner");
		$IBLOCK_ID = COption::GetOptionInt("htmls.docdesigner", 'DOCDESIGNER_DOCS_REGISTER_ID');
		if(intval($IBLOCK_ID) == 0){
			$check = CIBlock::GetList(Array(),Array('TYPE'=>'library','CODE'=>'sales_files_s1'), true);
			if($chech->SelectedRowsCount() > 0){
				while($ob = $check->GetNext()){
					$IBLOCK_ID = $ob['ID'];
				}
			}
			else{
				$IBLOCK_ID = 21;
			}
		}
		$Name = $arNum["NAME"];//$arNum['PREFIX'] . '-' . $arNum['YEAR'] . '-' . $arNum['STR_NUMBER'];
		$arProps = array('YEAR' =>$arNum['YEAR'], 'NUMBER' => $arNum['NUMBER'], 'OWN_ENTITIES' => $arNum['COMPANY_ID'],//'STR_NUMBER' => $arNum['STR_NUMBER'],
					'COMPANY_ID' => $arCrmId['company'], 'DEAL_ID' => $arCrmId['deal'], 'SUMMA' => $SUM, 'PREFIX' => $arNum['PREFIX'], 'TABLE' => serialize($table),
					'BIZPROC_ID' => $BP_ID, 'DOC' => $arNum['DOC_NAME'], 'NUMERATOR' => $arNum['NUMERATOR'],
					'CONTRACT_ID' => $ContractID, 'CONTACT_ID' => $arCrmId['contact'], 'LEAD_ID' => $arCrmId['lead']);//, 'LINK2LIBRARY' => $fid);
		if(IsModuleInstalled("disk") || IsModuleInstalled("webdav")){
		}
        else{
			$arProps['LINK2FILE'] = CFile::MakeFileArray($fname);
		}
        $arProps['BizProc'] = json_encode($arBizProc);
        //AddMessage2Log(print_r($arBizProc, true), "htmls.DocDesigner");
		$arFields = array('IBLOCK_ID' => $IBLOCK_ID, 'ACTIVE' => 'Y', 'NAME' => $Name, "PROPERTY_VALUES"=> $arProps);
        //AddMessage2Log(print_r($arFields, true), "htmls.DocDesigner");

		$el = new CIBlockElement;
		if($REG_ID = $el->Add($arFields)){
		  //@unlink($fname);
			return $REG_ID;
		}
		else{
			if($this->Add2Log == 'Y'){
				AddMessage2Log("Error.RegisterContract: ".$el->LAST_ERROR, "htmls.DocDesigner");
			}
			return false;
		}
	}//RegisterContract

	//if use library put file with doc into library
	function RegisterInLibrary($REG_ID, $NAME, $FILE, $DocumentSection, $LIBRARY_ID = 21){
	  global $USER;
		$IBLOCK_ID = COption::GetOptionInt("htmls.docdesigner", 'DOCDESIGNER_DOCS_REGISTER_ID');
		$LIBRARY_ID = COption::GetOptionString("htmls.docdesigner", "DOCDESIGNER_CONTRACTS_PATH2LIBRARY", $LIBRARY_ID);
        $arFile = CFile::MakeFileArray($FILE);
        $path_parts = pathinfo($FILE);
        $discConverted = \Bitrix\Main\Config\Option::get('disk', 'successfully_converted', false);
        $htmlsDiscConverted = \Bitrix\Main\Config\Option::get('htmls.docdesigner', 'successfully_converted', false);
        CModule::IncludeModule('disk');
        //AddMessage2Log($LIBRARY_ID, "htmls.DocDesigner");
        //AddMessage2Log($discConverted, "htmls.DocDesigner");
        //AddMessage2Log($htmlsDiscConverted, "htmls.DocDesigner");
        if($discConverted == 'Y'){
          if($htmlsDiscConverted == 'Y'){
            if($LIBRARY_ID > 0){
              $diskId = $LIBRARY_ID;
            }
            else{//we'll use current user disk
              $storage = \Bitrix\Disk\Driver::getInstance()->getStorageByUserId($USER->GetID());
              $diskId = $storage->getId();
            }

            $rs = \Bitrix\Disk\Storage::getList(array('filter' => array("ID" => $diskId)));
          }
          else{
            $res = CIBlock::GetByID($LIBRARY_ID);
            if($ar_res = $res->GetNext()){
    			$diskId = $ar_res['CODE'];
                $rs = \Bitrix\Disk\Storage::getList(array('filter' => array("ENTITY_ID" => $diskId)));
            }
          }

          if($arObject = $rs->Fetch()){
            //AddMessage2Log(print_r($arObject,1));
            if($DocumentSection > 0){
              $folder = \Bitrix\Disk\Folder::loadById($DocumentSection, array('STORAGE'));
            }
            else{
              $folder = \Bitrix\Disk\Folder::loadById($arObject["ROOT_OBJECT_ID"], array('STORAGE'));
            }
              if($folder)
              {
                  $res = $folder->uploadFile(
                      $arFile,
                      array(
                          'NAME' => $NAME.".".$path_parts['extension'],
                          'CREATED_BY' => $USER->GetID(),
                      )
                  );
                  //AddMessage2Log(print_r($res,1));
                  $aFile = $res->getFile();
                  $ID = $aFile['ID'];
          		if($ID){
          			CIBlockElement::SetPropertyValuesEx($REG_ID, $IBLOCK_ID, array('LINK2FILE' => $ID));
          			@unlink($FILE);
          			return $aFile;
          		}
          		else{
          			if($this->Add2Log == 'Y'){
          				AddMessage2Log("Error.RegisterInLibrary", "htmls.DocDesigner");
          			}
          			return false;
          		}
              }
              else
              {
                  AddMessage2Log("DISK_FILE_UPLOAD_ERROR_COULD_NOT_FIND_FOLDER", "htmls.DocDesigner");
              }
          }
        }
        else{
    		$arFile["type"] = $path_parts['extension'];//'docx';
    		$mess='';

    		$el = new CIBlockElement;
    		$arr = array(
    					"ACTIVE" => "Y",
    					"IBLOCK_ID" => $LIBRARY_ID,
    					//"IBLOCK_SECTION_ID" => somewhere far beyond,
    					"NAME" => $NAME.".".$path_parts['extension'],//'.docx',
    					"MODIFIED_BY" => $GLOBALS["USER"]->GetID(),
    					"PROPERTY_VALUES" => array(
    						'FILE' => $arFile,
    						"WEBDAV_SIZE" => $arFile['size']
    					));
    		if(intval($DocumentSection) > 0){
    			$arr['IBLOCK_SECTION_ID'] = $DocumentSection;
    		}
    		$ID = $el->Add($arr, false, false, false);
    		if($ID){
    			CIBlockElement::SetPropertyValuesEx($REG_ID, $IBLOCK_ID, array('LINK2FILE' => $ID));
    			@unlink($FILE);
    			return $ID;
    		}
    		else{
    			if($this->Add2Log == 'Y'){
    				AddMessage2Log("Error.RegisterInLibrary: ".$el->LAST_ERROR, "htmls.DocDesigner");
    			}
    			return false;
    		}
        }
	}

    //<disk functions>
    function getDisks(){
      $diskTree = array();
      $listStorage = \Bitrix\Disk\Storage::GetModelList(array());
      foreach($listStorage as $oStorage){
        $type = $oStorage->getEntityType();
        $arType = explode('\\', $type);
        if($arType[3] == 'User'){
          $rsUser = CUser::GetByID($oStorage->getEntityId());
          $arUser = $rsUser->Fetch();
          if($arUser['ACTIVE'] == 'N'){
            continue;
          }
        }
        elseif($arType[3] == 'Group'){
          $arGroup = CSocNetGroup::GetByID($oStorage->getEntityId());
          if($arGroup['CLOSED'] == 'Y'){
            continue;
          }
        }
        //echo '<pre>'; print_r($oStorage); echo '</pre>';
          $sid = $oStorage->getId();
          $diskTree[$sid]['name'] = $oStorage->getName();
          $diskTree[$sid]['root'] = $oStorage->getRootObjectId();
          //$diskTree[$sid]['entityId'] = $oStorage->getEntityId();
          //$diskTree[$sid]['entityType'] = $oStorage->getEntityType();
          //$diskTree[$sid]['tree'] = $cDoc->getSections($oStorage->getRootObjectId());
      }
      return $diskTree;
    }
    function getFileTree($secId, $root, &$arSec){
      global $DB;
      $strSql = 'SELECT * FROM b_disk_object WHERE ID=' . $secId;
      $resDb = $DB->Query($strSql);
      if($resDb->SelectedRowsCount() == 1){
        while($aSec = $resDb->Fetch()){
          if($aSec['PARENT_ID'] > 0){
            if($aSec['PARENT_ID'] == $root){
              $arSec[$aSec['ID']] = $aSec['NAME'];
            }
            else{
              self::getFileTree($aSec['PARENT_ID'], $root, $arSec);
            }
          }
          $arSec[$aSec['ID']] = $aSec['NAME'];
        }
      }
    }//getFileTree

    function getSections($secId){
      $parameters = array();
      $parameters["filter"] = array("PARENT_ID" => $secId, 'TYPE' => 2);
      $folders = \Bitrix\Disk\Folder::getList($parameters, array('STORAGE'));
      $arSections = array();
      while($ar = $folders->Fetch()){
        if($ar['NAME'] == '.Dropped') continue;
        if(strtoupper(LANG_CHARSET) == 'WINDOWS-1251'){
          $arSections[$ar['ID']]['name'] = iconv('cp1251', 'UTF-8', $ar['NAME']);
        }
        else{
            $arSections[$ar['ID']]['name'] = $ar['NAME'];
        }
          if($subSec = self::getSections($ar['ID'])){
            $arSections[$ar['ID']]['tree'] = $subSec;
          }
      }
      if(count($arSections) > 0)
        return $arSections;
      else
        return false;
    }//getSections

    function getSelect($ar, $padding, $selected){
      //echo $selected;
      $padding .= '>';
      $strOption = '';
      foreach($ar as $id => $ob){
        if($id == $selected) $s = ' selected';
        else $s = '';
        if($sub = $ob['tree']){
          //$strOption .= '<optgroup label="' . $padding . $ob['name'] . '">';

          $strOption .= '<option value="' . $id . '"' . $s . '>' . $padding . $ob['name'] . '</option>';
          $strOption .= self::getSelect($ob['tree'], $padding, $selected);
          //$strOption .= '</optgroup>';
        }
        else{
          $strOption .= '<option value="' . $id . '"' . $s . '>' . $padding . $ob['name'] . '</option>';
        }
      }
      return $strOption;
    }//getSelect

    function getPath2disk($obj){
      if(strlen($obj['ENTITY_MISC_DATA']) > 0){
        $set = unserialize($obj['ENTITY_MISC_DATA']);
        //AddMessage2Log(print_r($set, true), "htmls.DocDesigner");
        $path2disk = str_replace('#SITE_DIR#', '', $set['BASE_URL']);
        $path2disk .= 'file/';
        //AddMessage2Log($path2disk, "htmls.DocDesigner");
        return $path2disk;
      }
      elseif($obj['ENTITY_TYPE'] == 'Bitrix\Disk\ProxyType\User'){
        $userPage = \COption::getOptionString("socialnetwork", "user_page", false, SITE_ID);
        if(!$userPage)
        {
            $userPage = SITE_DIR . 'company/personal/';
        }
        $userPage .= 'user/' .  $obj['ENTITY_ID'] . '/disk/file/';
        //AddMessage2Log($userPage, "htmls.DocDesigner");
        return $userPage;
      }
      elseif($obj['ENTITY_TYPE'] == 'Bitrix\Disk\ProxyType\Group'){
        $groupPage = \COption::getOptionString("socialnetwork", "workgroups_page", false, SITE_ID);
        if(!$groupPage)
        {
            $groupPage = SITE_DIR . 'workgroups/';
        }
        $groupPage .= 'group/' .  $obj['ENTITY_ID'] . '/disk/file/';
        return $groupPage;
      }
    }//getPath2disk($obj)
    //</disk functions>

	//build link to contract for component template
	function GetContract($bpID, $icon = false, $ElemID = 0, $PropCode = '', $PropValue = 0){
		global $DB;
        //AddMessage2Log($PropCode, "htmls.DocDesigner");
        //AddMessage2Log($PropValue, "htmls.DocDesigner");
        $discConverted = \Bitrix\Main\Config\Option::get('disk', 'successfully_converted', false);
        if($discConverted == 'Y'){
          CModule::IncludeModule('disk');
        }
        CModule::IncludeModule('iblock');
        $htmlsDiscConverted = \Bitrix\Main\Config\Option::get('htmls.docdesigner', 'successfully_converted', false);
        $err_mess = "Function: GetContract<br>Line: ";
		if(intval($bpID) == $bpID && $PropValue == 0){//bizproz from iblock
			$dbRes = $DB->Query("SELECT ID FROM b_bp_workflow_state WHERE DOCUMENT_ID = '" . $bpID . "'");
			while ($arRes = $dbRes->Fetch()){
				$bpID = $arRes['ID'];
			}
		}
        $LIBRARY_ID = COption::GetOptionInt("htmls.docdesigner", 'DOCDESIGNER_CONTRACTS_PATH2LIBRARY');//library id
        if($htmlsDiscConverted == 'Y' && $discConverted == 'Y'){
            $listStorage = \Bitrix\Disk\Storage::GetModelList(array('filter'=>array('ID'=>$LIBRARY_ID)));
            foreach($listStorage as $oStorage){
                $res = CIBlock::GetByID($oStorage->getXmlId());
        		if($ar_res = $res->GetNext()){
        			$PATH2LIBRARY = str_replace('#SITE_DIR#', '', $ar_res['LIST_PAGE_URL']);
        		}
                $oldLib = $oStorage->getXmlId();
            }
        }
        else{
    		$res = CIBlock::GetByID($LIBRARY_ID);
    		if($ar_res = $res->GetNext()){
    			$PATH2LIBRARY = str_replace('#SITE_DIR#', '', $ar_res['LIST_PAGE_URL']);
    		}
        }
        //AddMessage2Log($PATH2LIBRARY, "htmls.DocDesigner");
        //get link to reg element
		$arSelect = array('ID', 'NAME', 'PROPERTY_SUMMA', 'PROPERTY_LINK2FILE', 'PROPERTY_NUMBER', 'LIST_PAGE_URL', "PROPERTY_DOC");
        if($PropValue == 0){
		    $arFilter = Array("IBLOCK_ID" => COption::GetOptionInt("htmls.docdesigner", 'DOCDESIGNER_DOCS_REGISTER_ID'), "PROPERTY_BIZPROC_ID" => $bpID);
        }
        else{
          $arFilter = Array("IBLOCK_ID" => COption::GetOptionInt("htmls.docdesigner", 'DOCDESIGNER_DOCS_REGISTER_ID'), $PropCode => $PropValue);
        }
		if($ElemID > 0){
			$arFilter['ID'] = $ElemID;
		}
		$res = CIBlockElement::GetList(Array("ID" => "ASC"), $arFilter, false, false, $arSelect);
		if($res->SelectedRowsCount() == 0) return false;
		while($ob = $res->GetNextElement()){
			$arFields = $ob->GetFields();
            $web_dav_id = $arFields['PROPERTY_LINK2FILE_VALUE'];

            $db_props = CIBlockElement::GetProperty($oldLib, $arFields['PROPERTY_LINK2FILE_VALUE'], array("sort" => "asc"), Array("CODE"=>"FILE"));
            if($ar_props = $db_props->Fetch()){
              if(intval($ar_props["VALUE"]) > 0)
                $web_dav_id = $ar_props["VALUE"];
            }
            /*else{
              $fileId
            }*/
            //AddMessage2Log($web_dav_id, "htmls.DocDesigner");
            //AddMessage2Log(print_r($arFields, true), "htmls.DocDesigner");
            if($discConverted == 'Y'){
              //disk
              //CModule::IncludeModule('disk');
              //$files = \Bitrix\Disk\File::getModelList(array('filter' => array('XML_ID' => $web_dav_id, 'TYPE' => \Bitrix\Disk\Internals\FileTable::TYPE_FILE)));
              if($web_dav_id > 0){
                $new = true;
                //$qColumnNames = mysql_query("SHOW COLUMNS FROM b_disk_object");
                $qColumnNames = $DB->Query("SHOW COLUMNS FROM b_disk_object");
                //$numColumns = $qColumnNames->SelectedRowsCount();
                //$numColumns = mysql_num_rows($qColumnNames);
                $x = 0;
                while($colname = $qColumnNames->Fetch()){
                  if($colname == 'WEBDAV_IBLOCK_ID'){
                    $new = false;
                    break;
                  }
                }
                /*
                while ($x < $numColumns){
                    $colname = mysql_fetch_row($qColumnNames);
                    if($colname[0] == 'WEBDAV_IBLOCK_ID'){
                      $new = false;
                      break;
                    }
                    //$col[$colname[0]] = $colname[0];
                    $x++;
                }
                */
                //$res = $DB->Query("SELECT `WEBDAV_IBLOCK_ID` FROM `b_disk_object` WHERE 0");
                if ($new) {
                   $strSql = 'SELECT * FROM  b_disk_object WHERE FILE_ID=' . $web_dav_id;
                }
                else{
                  $strSql = 'SELECT * FROM  b_disk_object WHERE FILE_ID=' . $web_dav_id . ' AND WEBDAV_IBLOCK_ID IS NULL';
                }
                //$strSql = 'SELECT * FROM  b_disk_object WHERE FILE_ID=' . $web_dav_id . ' AND WEBDAV_IBLOCK_ID IS NULL';
                //AddMessage2Log($strSql, "htmls.DocDesigner");
                $resDb = $DB->Query($strSql);
                if($resDb->SelectedRowsCount() == 1){
                  //disk
                  while($arFile = $resDb->Fetch()){
                    //$files = \Bitrix\Disk\File::getList(array('filter' => array('ID' => $web_dav_id)));
                    //foreach($files as $file){
                      //$f = $file->getFile();
                      //$file = \Bitrix\Disk\File::loadById($arFile['ID'], array('STORAGE'));
                      //$file->Fetch();
                      //AddMessage2Log(print_r($file, true), "htmls.DocDesigner");
                      $arSec = array();
                      //AddMessage2Log(print_r($arFile, true), "htmls.DocDesigner");
                      $rs = \Bitrix\Disk\Storage::getList(array('filter' => array("ID" => $arFile['STORAGE_ID'])));
                      if($arObject = $rs->Fetch()){
                        //AddMessage2Log(print_r($arObject, true), "htmls.DocDesigner");
                        if($arObject["ROOT_OBJECT_ID"] != $arFile['PARENT_ID']){

                          self::getFileTree($arFile['PARENT_ID'], $arObject["ROOT_OBJECT_ID"], $arSec);
                        }
                        //$folder = \Bitrix\Disk\Folder::loadById($arObject["ROOT_OBJECT_ID"], array('STORAGE'));
                      }

                      //AddMessage2Log(print_r($arSec, true), "htmls.DocDesigner");
                    //}
                    $path = '';
                    if(count($arSec) > 0){
                      $path = implode('/', $arSec) . '/';
                    }
                    $path = self::getPath2disk($arObject) . $path;
                    //$PATH2LIBRARY.'file/'.

                    $disk = '/disk/downloadFile/'.$arFile['ID'].'/?&ncc=1&filename='.$arFile['NAME'];

                    $link2file = '<a target="_blank" href='.$disk.'><img src="/bitrix/images/htmls.docdesigner/savedoc20.png" border="0" /></a>';
                    $link2ref = '<a href="'.$path . $arFile['NAME'].'">';
                    if($icon){
            			$link2ref .= '<img src="/bitrix/images/htmls.docdesigner/gotodoc20.png" border="0" />';
            		}
            		else{
            			$link2ref .= $arFields['PROPERTY_DOC_VALUE'];
            			if($arFields['PROPERTY_DOC_VALUE'] != $arFields['NAME']){
            				$link2ref .= " " . $arFields['NAME'];
            			}
            			if($arFields['PROPERTY_SUMMA_VALUE'] > 0){
            				$link2ref .= " (".$arFields['PROPERTY_SUMMA_VALUE'].")";
            			}
            		}
        		    $link2ref .=  '</a>';
                    $arResult[] = array('LINK2REF' => $link2file . " " . $link2ref,
            								//'FILE_ID' => $arFields['PROPERTY_LINK2FILE_VALUE'],//$ar_props["VALUE"],
            								'TXT2TRACK' => $href2file . " " . $href2ref,
            								'FILE_ID' => $arFile["FILE_ID"],
            								'arFile' => $arFile,
            								'NAME' => $arFields['NAME'],
            								'SUM' => number_format($arFields['PROPERTY_SUMMA_VALUE'], 2, ',', ' '),
            								'TYPE' => $arFields['PROPERTY_DOC_VALUE'],
            								'LINK' => '<a href="'.$path.$arFile['NAME'].'">'.$arFields['NAME'].'</a>',
            								'ICON' => $link2file,
                                            'link2disk' => $path.$arFile['NAME'],
                                            'link2download' => $disk,
                                          'NUMBER' => $arFields['PROPERTY_NUMBER_VALUE'],
                                          'WEB_DAV_ID'=>$arFile['ID']);//$web_dav_id);
                  }
                }
                else{
                  if($new)
                    $strSql = 'SELECT * FROM  b_disk_object WHERE FILE_ID=' . $web_dav_id . '';
                  else
                    $strSql = 'SELECT * FROM  b_disk_object WHERE WEBDAV_ELEMENT_ID=' . $web_dav_id . ' AND WEBDAV_IBLOCK_ID > 0';
                  //AddMessage2Log($strSql, "htmls.DocDesigner");
                  $resDb = $DB->Query($strSql);
                  if($resDb->SelectedRowsCount() == 1){
                    //webdav element
                    while($arFile = $resDb->Fetch()){
                      $disk = '/disk/downloadFile/'.$arFile['ID'].'/?&ncc=1&filename='.$arFile['NAME'];

                      $link2file = '<a target="_blank" href='.$disk.'/><img src="/bitrix/images/htmls.docdesigner/savedoc20.png" border="0" /></a>';
                      $link2ref = '<a href="'.$PATH2LIBRARY.'file/'.$arFile['NAME'].'">';
                      if($icon){
              			$link2ref .= '<img src="/bitrix/images/htmls.docdesigner/gotodoc20.png" border="0" />';
              		}
              		else{
              			$link2ref .= $arFields['PROPERTY_DOC_VALUE'];
              			if($arFields['PROPERTY_DOC_VALUE'] != $arFields['NAME']){
              				$link2ref .= " " . $arFields['NAME'];
              			}
              			if($arFields['PROPERTY_SUMMA_VALUE'] > 0){
              				$link2ref .= " (".$arFields['PROPERTY_SUMMA_VALUE'].")";
              			}
              		}
          		    $link2ref .=  '</a>';
                      $arResult[] = array('LINK2REF' => $link2file . " " . $link2ref,
              								//'FILE_ID' => $arFields['PROPERTY_LINK2FILE_VALUE'],//$ar_props["VALUE"],
              								'TXT2TRACK' => $href2file . " " . $href2ref,
              								'FILE_ID' => $arFile["FILE_ID"],
                                            'arFile' => $arFile,
              								'NAME' => $arFields['NAME'],
              								'SUM' => number_format($arFields['PROPERTY_SUMMA_VALUE'], 2, ',', ' '),
              								'TYPE' => $arFields['PROPERTY_DOC_VALUE'],
              								'LINK' => '<a href="'.$PATH2LIBRARY.'file/'.$arFile['NAME'].'">'.$arFields['NAME'].'</a>',
              								'ICON' => $link2file,
                                            'NUMBER' => $arFields['PROPERTY_NUMBER_VALUE'],
                                            'WEB_DAV_ID'=>$arFile['ID']);//$web_dav_id);
                    }
                  }
                  else{
                    //something wrong
                    //AddMessage2Log('web_dav_id='.$web_dav_id, "htmls.DocDesigner");
                    //AddMessage2Log(print_r($arFields, true), "htmls.DocDesigner");
                  }
                }
              }
              else{
                //AddMessage2Log('web_dav_id='.$web_dav_id, "htmls.DocDesigner");
                //AddMessage2Log(print_r($arFields, true), "htmls.DocDesigner");
              }
            }
            else{
              $db_props = CIBlockElement::GetProperty($LIBRARY_ID, $arFields['PROPERTY_LINK2FILE_VALUE'], array("sort" => "asc"), Array("CODE"=>"FILE"));
              $ar_props = $db_props->Fetch();
    			$link2file = '<a target="_blank" href='.$PATH2LIBRARY.'webdav_bizproc_history_get/'.$arFields['PROPERTY_LINK2FILE_VALUE'].'/'.$arFields['PROPERTY_LINK2FILE_VALUE'].'/><img src="/bitrix/images/htmls.docdesigner/savedoc20.png" border="0" /></a>';
    			$href2file = $_SERVER['SERVER_NAME'] . '/bitrix/admin/docdesigner_download.php?action=download&file='.$ar_props["VALUE"];
        		$link2ref = '<a href="'.$PATH2LIBRARY.'element/view/'.$arFields['PROPERTY_LINK2FILE_VALUE'].'/">';
        		$href2ref = $_SERVER['SERVER_NAME'] . $PATH2LIBRARY.'element/view/'.$arFields['PROPERTY_LINK2FILE_VALUE'].'/';
        		if($icon){
        			$link2ref .= '<img src="/bitrix/images/htmls.docdesigner/gotodoc20.png" border="0" />';
        		}
        		else{
        			$link2ref .= $arFields['PROPERTY_DOC_VALUE'];
        			if($arFields['PROPERTY_DOC_VALUE'] != $arFields['NAME']){
        				$link2ref .= " " . $arFields['NAME'];
        			}
        			if($arFields['PROPERTY_SUMMA_VALUE'] > 0){
        				$link2ref .= " (".$arFields['PROPERTY_SUMMA_VALUE'].")";
        			}
        		}
    		    $link2ref .=  '</a>';
  			    $arResult[] = array('LINK2REF' => $link2file . " " . $link2ref,
								//'FILE_ID' => $arFields['PROPERTY_LINK2FILE_VALUE'],//$ar_props["VALUE"],
								'TXT2TRACK' => $href2file . " " . $href2ref,
								'FILE_ID' => $ar_props["VALUE"],
								'NAME' => $arFields['NAME'],
								'SUM' => number_format($arFields['PROPERTY_SUMMA_VALUE'], 2, ',', ' '),
								'TYPE' => $arFields['PROPERTY_DOC_VALUE'],
								'LINK' => '<a href="'.$PATH2LIBRARY.'element/view/'.$arFields['PROPERTY_LINK2FILE_VALUE'].'/">'.$arFields['NAME'].'</a>',
								'ICON' => $link2file,
                                'NUMBER' => $arFields['PROPERTY_NUMBER_VALUE'],
                                'WEB_DAV_ID'=>$web_dav_id);
            }

		}
        //AddMessage2Log(print_r($arResult, true), "htmls.DocDesigner");
		return $arResult;

	}
	//get path to docx-file, use it as template for set number in project doc
	function GetPath2Contract($bpID, $link2doc){
	  global $DB;
		//$SAVE2LIB = COption::GetOptionString("htmls.docdesigner", 'DOCDESIGNER_CONTRACTS_SAVE2LIBRARY');
		$arSelect = array('ID', 'PROPERTY_LINK2FILE');
		$arFilter = Array("IBLOCK_ID"=>COption::GetOptionInt("htmls.docdesigner", 'DOCDESIGNER_DOCS_REGISTER_ID'), "PROPERTY_BIZPROC_ID"=>$bpID, 'PROPERTY_LINK2FILE' => $link2doc);//IntVal(COption::GetOptionInt("htmls.docdesigner", 'DOCDESIGNER_CONTRACTS_LIST_ID'))
        //AddMessage2Log(print_r($arFilter, true), "htmls.DocDesigner");
        //AddMessage2Log('link2doc='.$link2doc, "htmls.DocDesigner");

        $discConverted = \Bitrix\Main\Config\Option::get('disk', 'successfully_converted', false);
        if($link2doc > 0){
          if($discConverted == 'Y'){
            $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
            $ob = $res->GetNextElement();
            $arFields = $ob->GetFields();

            $web_dav_id = $link2doc;
            $fileId = 0;
           // if($web_dav_id > 0){
              //$strSql = 'SELECT * FROM b_disk_object WHERE WEBDAV_ELEMENT_ID=' . $web_dav_id;
              //AddMessage2Log($strSql, "htmls.DocDesigner");
              //$resDb = $DB->Query($strSql);
              //if($resDb->SelectedRowsCount() == 1){
                //webdav element
              //  $arFile = $resDb->Fetch();
             //   $fileId = $arFile["FILE_ID"];
              //  $fileName = $arFile['NAME'];
              //  $diskId = $arFile["ID"];
              //}
              //else{
                $strSql = 'SELECT * FROM  b_disk_object WHERE FILE_ID=' . $web_dav_id;
                //AddMessage2Log($strSql, "htmls.DocDesigner");
                $resDb = $DB->Query($strSql);
                if($resDb->SelectedRowsCount() == 1){
                  //ok
                  $arFile = $resDb->Fetch();
                  $fileId = $arFile["FILE_ID"];
                  $fileName = $arFile['NAME'];
                  $diskId = $arFile["ID"];
                }
                else{
                  //something wrong
                  //AddMessage2Log('web_dav_id='.$web_dav_id, "htmls.DocDesigner");
                  //AddMessage2Log(print_r($arFields, true), "htmls.DocDesigner");
                  return false;
                }
              //}
              if($fileId > 0){
                $path = CFile::GetPath($fileId);
                return array('path' => $path, 'reg_id' => $arFields['ID'],
                  'file_id' => $fileId,
                  'fileName' => $fileName, 'diskId' => $diskId);
              }
            //}

          }
        }
        else{
          $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
    	  while($ob = $res->GetNextElement()){
  			$arFields = $ob->GetFields();
              //AddMessage2Log(print_r($arFields, true), "htmls.DocDesigner");

      		$arSelectFile = Array("ID", "PROPERTY_FILE");
      		$arFilterFile = Array("ID"=>IntVal($arFields['PROPERTY_LINK2FILE_VALUE']));
      		$resFile = CIBlockElement::GetList(Array(), $arFilterFile, false, false, $arSelectFile);
      		while($ob = $resFile->GetNextElement()){
      			$arFieldsFile = $ob->GetFields();
      			$path = CFile::GetPath($arFieldsFile['PROPERTY_FILE_VALUE']);
      		}

  			return array('path' => $path, 'reg_id' => $arFields['ID'], 'file_id' => $arFields['PROPERTY_LINK2FILE_VALUE']);
    	  }
        }
		return false;
	}

    function getCrmDisk($crm, $cid, $arResult){
      $ufId = COption::GetOptionString("htmls.docdesigner", 'DOCDESIGNER_CRM_DISK_'.$crm);
      //echo 'uf='.$ufId;
      foreach($arResult['FIELDS'] as $tab => $arFields){
        foreach($arFields as $fid => $aField){
          if($aField['id'] == $ufId){
            $arResult['FIELDS'][$tab][$fid]['value'] = '';
            $arFiles = self::GetContract(0, false, 0, 'PROPERTY_'.$crm.'_ID', $cid);
            foreach($arFiles as $file){
              $arResult['FIELDS'][$tab][$fid]['value'] .= $file['LINK2REF'] . '<br/>';
            }
          }
        }
      }
      return $arResult;
    }

	function GetDocxFiles($bpID){
		$SAVE2LIB = COption::GetOptionString("htmls.docdesigner", 'DOCDESIGNER_CONTRACTS_SAVE2LIBRARY');
		$arSelect = array('ID', 'PROPERTY_LINK2LIBRARY');
		$arFilter = Array("IBLOCK_ID"=>IntVal(COption::GetOptionInt("htmls.docdesigner", 'DOCDESIGNER_CONTRACTS_LIST_ID')), "PROPERTY_BIZPROC_ID"=>$bpID);
		$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
		while($ob = $res->GetNextElement()){
			$arFields = $ob->GetFields();
            if($SAVE2LIB == 'N'){
				$arFiles[] = $arFields['PROPERTY_LINK2LIBRARY_VALUE'];
			}
			else{
				$arSelectFile = Array("ID", "PROPERTY_FILE");
				$arFilterFile = Array("ID"=>IntVal($arFields['PROPERTY_LINK2LIBRARY_VALUE']));
				$resFile = CIBlockElement::GetList(Array(), $arFilterFile, false, false, $arSelectFile);
				while($ob = $resFile->GetNextElement()){
					$arFieldsFile = $ob->GetFields();
					$arFiles[] = $arFieldsFile['PROPERTY_FILE_VALUE'];
				}
			}
			return $arFiles;
		}
	}
    /*
	function CheckDir($COMPANY_ID){
		if(!is_dir($_SERVER['DOCUMENT_ROOT'] . "/upload/htmls.docdesigner/COMPANY_".$COMPANY_ID."/"))
			$res = mkdir($_SERVER['DOCUMENT_ROOT'] . "/upload/htmls.docdesigner/COMPANY_".$COMPANY_ID."/");
	}
    */
    //return info for customer from lists
	function GetCustomerData($CID){
		$arOptions = array('OGRN', 'INN','KPP','RS','BANK','POST_ADDRESS','PHONE','FULL_NAME','ADDRESS_LEGAL','ADDRESS','PERSON');
		$arSelect[] = 'NAME';
		foreach($arOptions as $key){
			$arSelect[] = 'PROPERTY_'.$key;
		}
		$arFilter = Array("ID"=>IntVal($CID));
		$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
		$arCompany = array();
		while($ob = $res->GetNextElement()){
			$arFields = $ob->GetFields();
			foreach($arOptions as $key){
				$arCompany[$key] = $arFields["PROPERTY_" . $key . "_VALUE"];
			}
			$arCompany['NAME'] = $arFields['PROPERTY_FULL_NAME_VALUE'];
		}
		return $arCompany;
	}//GetCustomerData
//end class CDocDesignerContracts
}
?>