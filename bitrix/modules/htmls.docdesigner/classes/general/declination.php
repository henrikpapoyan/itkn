<?php

/*
* NCL NameCase Russian Language
*
* �����, ������� ��������� �������� ������� �����, ������ �������� �� �������.
*
* @license Dual licensed under the MIT or GPL Version 2 licenses.
* @author ������ ����� http://seagull.net.ua/ bymer3@gmail.com
* @version 0.1.2 05.05.2011
*
*/

class NCLNameCaseRu
{
    /*
    * ��� ��� ���������
    * @var string
    */

    private $firstName = "";

    /*
    * ������� ��� ���������
    * @var string
    */
    private $secondName = "";

    /*
    * �������� ��� ���������
    * @var string
    */
    private $fatherName = "";

    /*
    * @var integer
    * ��� ��������
    * <li>0 - �� ��������</li>
    * <li>1 - �������</li>
    * <li>2 - �������</li>
    */
    private $gender = 0;

    /*
    * @static integer
    */
    static $MAN = 1;

    /*
    * @static integer
    */
    static $WOMAN = 2;

    /*
    * @static integer
    * ������
    */
    static $IMENITLN = 0;
    static $RODITLN = 1;
    static $DATELN = 2;
    static $VINITELN = 3;
    static $TVORITELN = 4;
    static $PREDLOGN = 5;

    /*
    * @static string
    * ������ �������
    */
    private $vowels = "���������";

    /*
    * @static string
    * ������ ���������
    */
    private $consonant = "���������������������";

    /*
    * @static array()
    * ������ ��������� ��� ������������ ����
    */
    private $ovo = array('���', '���', '���', '���');

    /*
    * @static array()
    * ������ ��������� ��� ������������ ����
    */
    private $ih = array('��', '��', '��');

    /*
    * @var array()
    * ��������� ��������� �����
    */
    private $firstResult = array();

    /*
    * @var array()
    * ��������� ��������� �������
    */
    private $secondResult = array();

    /*
    * @var array()
    * ��������� ��������� ��������
    */
    private $fatherResult = array();

    /*
    * @var integer
    * ����� ������� �� �������� �� ���������� ���/�������
    */
    private $error = "";

    /*
    * @var integer
    * ����� ������� �� �������� ���������� ���
    */
    private $frule = "";

    /*
    * @var integer
    * ����� ������� �� �������� �� ���������� �������
    */
    private $srule = "";

    /*
    * �������, ������� �������� ��� ��������� � $this->firstName, �� �������� ��������� ������� ����.
    *
    * @return boolean
    */

    private function manFirstName()
    {
        //��������� ������
        $LastSymbol = mb_substr($this->firstName, -1, 1, 'utf-8');
        //������������� ������
        $BeforeLast = mb_substr($this->firstName, -2, 1, 'utf-8');
        $needToChange = true;
        $this->firstResult = array();
        //�� ����������
        //������� � ������� �����, �������������� �� ������� ���� �������� -�, -�
        if ($this->in($LastSymbol, $this->vowels) and !$this->in($LastSymbol, '��'))
        {
            $needToChange = false;
            $this->error = 3;
        }
        //���� ����� ��������
        if ($needToChange)
        {
            //������� �����, �������������� �� ����� ��������� (������� ��� ������) � �� -�, ���������� ��� ��, ��� ������� ��������������� �������� ����
            if ($this->in($LastSymbol, $this->consonant) or
                    ($LastSymbol == "�" and $this->in($BeforeLast, $this->consonant)))
            {
                //���� �� � ��� �
                if ($this->in($LastSymbol, '��'))
                {
                    if ($BeforeLast <> "�")
                    {
                        $this->firstResult = $this->padeg($this->firstName, array('�', '�', '�', '��', '�'), true);
                        $this->frule = 1;
                        return true;
                    }
                    else
                    {
                        $this->firstResult = $this->padeg($this->firstName, array('�', '�', '�', '��', '�'), true);
                        $this->frule = 10;
                        return true;
                    }
                }
                else
                {
                    if ($this->firstName == "�����")
                    {
                        $this->firstResult = array("�����", "�����", "�����", "�����", "������", "�����");
                        $this->frule = 200;
                        return true;
                    }
                    elseif ($this->firstName == "���")
                    {
                        $this->firstResult = array("���", "����", "����", "����", "�����", "����");
                        $this->frule = 201;
                        return true;
                    }
                    else
                    {
                        $this->firstResult = $this->padeg($this->firstName, array('�', '�', '�', '��', '�'), false);
                        $this->frule = 2;
                        return true;
                    }
                }
            }
            //������� � ������� �����, �������������� �� -�, ����������, ��� � ����� ��������������� � ����� �� ����������
            elseif ($LastSymbol == "�")
            {
                if ($BeforeLast <> '�')
                {
                    $this->firstResult = $this->padeg($this->firstName, array('�', '�', '�', '��', '�'), true);
                    $this->frule = 3;
                    return true;
                }
                else
                {
                    $this->firstResult = $this->padeg($this->firstName, array('�', '�', '�', '��', '�'), true);
                    $this->frule = 15;
                    return true;
                }
            }
            //������� � ������� �����, �������������� �� -�, -��, -��, -��, ���������� �� �����, �� �������� ��� ����������, ���������� ��� ��������������� � ���������������� �����������
            elseif ($LastSymbol == "�")
            {
                $this->firstResult = $this->padeg($this->firstName, array('�', '�', '�', '��', '�'), true);
                $this->frule = 4;
                return true;
            }
        }

        //���� ������� �� �����, ����� ��������� ��� ����
        if (!isset($this->firstResult[0]))
        {
            $this->makeFirstTheSame();
            return false;
        }
    }

    /*
    * �������, ������� �������� ��� ��������� � $this->firstName, �� �������� ��������� ������� ����.
    *
    * @return boolean
    */

    private function womanFirstName()
    {
        //��������� ������
        $LastSymbol = mb_substr($this->firstName, -1, 1, 'utf-8');
        //������������� ������
        $BeforeLast = mb_substr($this->firstName, -2, 1, 'utf-8');
        $needToChange = true;
        $this->firstResult = array();
        //�� ����������
        //������� �����, �������������� �� ��������� ����
        if ($this->in($LastSymbol, $this->consonant))
        {
            $needToChange = false;
            $this->error = 2;
        }
        //������� � ������� �����, �������������� �� ������� ���� �������� -�, -�
        if ($this->in($LastSymbol, $this->vowels) and !$this->in($LastSymbol, '��'))
        {
            $needToChange = false;
            $this->error = 3;
        }
        //���� ����� ��������
        if ($needToChange)
        {
            //������� � ������� �����, �������������� �� -�, ����������, ��� � ����� ��������������� � ����� �� ����������
            if ($LastSymbol == "�")
            {
                if (!$this->in($BeforeLast, '����'))
                {
                    $this->firstResult = $this->padeg($this->firstName, array('�', '�', '�', '��', '�'), true);
                    $this->frule = 3;
                    return true;
                }
                else
                {
                    //�� ����� ���������
                    if ($BeforeLast == '�')
                    {
                        $this->firstResult = $this->padeg($this->firstName, array('�', '�', '�', '��', '�'), true);
                        $this->frule = 25;
                        return true;
                    }
                    else
                    {
                        $this->firstResult = $this->padeg($this->firstName, array('�', '�', '�', '��', '�'), true);
                        $this->frule = 15;
                        return true;
                    }
                }
            }
            //������� � ������� �����, �������������� �� -�, -��, -��, -��, ���������� �� �����, �� �������� ��� ����������, ���������� ��� ��������������� � ���������������� �����������
            elseif ($LastSymbol == "�")
            {
                if ($BeforeLast <> "�")
                {
                    $this->firstResult = $this->padeg($this->firstName, array('�', '�', '�', '��', '�'), true);
                    $this->frule = 5;
                    return true;
                }
                else
                {
                    $this->firstResult = $this->padeg($this->firstName, array('�', '�', '�', '��', '�'), true);
                    $this->frule = 8;
                    return true;
                }
            }
            //������� ������� �����, �������������� �� ������ ���������, ����������, ��� ��������������� �������� ���� ���� ����, ����
            elseif ($LastSymbol == "�")
            {
                $this->firstResult = $this->padeg($this->firstName, array('�', '�', '�', '��', '�'), true);
                $this->frule = 6;
                return true;
            }
        }
        //���� ������� �� �����, ����� ��������� ��� ����
        if (!isset($this->firstResult[0]))
        {
            $this->makeFirstTheSame();
            return false;
        }
    }

    /*
    * �������, ������� �������� ������� ��������� � $this->secondName, �� �������� ��������� ������� �������.
    *
    * @return boolean
    */

    private function manSecondName()
    {
        //��������� ������
        $LastSymbol = mb_substr($this->secondName, -1, 1, 'utf-8');
        //������������� ������
        $BeforeLast = mb_substr($this->secondName, -2, 1, 'utf-8');
        $needToChange = true;
        $this->secondResult = array();
        //�� ����������
        //������� �� -�, -� � �������������� ������� -�
        if ($this->in($LastSymbol, '��') and $BeforeLast == "�")
        {
            $needToChange = false;
            $this->error = 4;
        }
        /* ������� �������, �������������� ����� ��������� ����� ������������ ������ ������������� ����� � �����������: -���, -���, -��� (�������, ������, ������, ���������, ������, �������) � �������������� ����� � �����������: -��, -�� (��������, ����������, ��������, ������, �����),  + ���������� �� �� */
        if (in_array(mb_substr($this->secondName, -3, 3, 'utf-8'), $this->ovo) or in_array(mb_substr($this->secondName, -2, 2, 'utf-8'), $this->ih))
        {
            $needToChange = false;
            $this->error = 5;
        }
        //���� ����� ��������
        if ($needToChange)
        {
            //������� �������, �������������� �� ����� ��������� (������� ��� ������) � �� -�, ���������� ��� ��, ��� ������� ��������������� �������� ����
            if ($this->in($LastSymbol, $this->consonant) or
                    ($LastSymbol == "�" and $this->in($BeforeLast, $this->consonant)))
            {
                //���� �� � ��� �
                if ($this->in($LastSymbol, '��'))
                {

                    if ((mb_substr($this->secondName, -3, 1, 'utf-8') == "�") or $BeforeLast == '�')
                    {
                        $this->srule = 101;
                        $this->secondResult = $this->padeg($this->secondName, array('�', '�', '�', '��', '�'), true);
                        return true;
                    }
                    //������� -� �������
                    elseif ($BeforeLast == '�' or mb_substr($this->secondName, -3, 1, 'utf-8') == '�')
                    {
                        $this->secondResult = $this->padeg($this->secondName, array('���', '���', '���', '��', '��'), true, true);
                        $this->srule = 102;
                        return true;
                    }
                    //��������
                    elseif ((mb_substr($this->secondName, -3, 3, 'utf-8')) == '���')
                    {
                        $this->secondResult = $this->padeg($this->secondName, array('����', '����', '����', '���', '���'), true, true);
                        $this->srule = 103;
                        return true;
                    }
                    else
                    {
                        $this->secondResult = $this->padeg($this->secondName, array('���', '���', '���', '��', '��'), true, true);
                        $this->srule = 1;
                        return true;
                    }
                }
                else
                {
                    //���� ������������� �, �� ����� �� ������ ��
                    if ($this->in($LastSymbol, '�'))
                    {
                        //���� ����� ����� �� ��, �� ����� ������ �
                        if ($this->in($BeforeLast, '�'))
                        {
                            $this->secondResult = $this->padeg($this->secondName, array('��', '��', '��', '���', '��'), false, true);
                            $this->srule = 2301;
                            return true;
                        }
                        if ($this->in($BeforeLast, '�'))
                        {
                            $this->secondResult = $this->padeg($this->secondName, array('���', '���', '���', '����', '���'), false, true);
                            $this->srule = 2302;
                            return true;
                        }
                        else
                        {
                            $this->secondResult = $this->padeg($this->secondName, array('�', '�', '�', '��', '�'), false);
                            $this->srule = 23;
                            return true;
                        }
                    }
                    else
                    {
                        //���� ������ �� �, �� ����� ��
                        if ($LastSymbol == '�')
                        {
                            $this->secondResult = $this->padeg($this->secondName, array('�', '�', '�', '��', '�'), false);
                            $this->srule = 301;
                            return true;
                        }
                        else
                        {
                            $this->secondResult = $this->padeg($this->secondName, array('�', '�', '�', '��', '�'), false);
                            $this->srule = 3;
                            return true;
                        }
                    }
                }
            }
            //������� � ������� �����, �������������� �� -�, ����������, ��� � ����� ��������������� � ����� �� ����������
            elseif ($LastSymbol == "�")
            {
                //���� ������ �� �, �� ����� �, ��
                if ($this->in($BeforeLast, '�'))
                {
                    $this->secondResult = $this->padeg($this->secondName, array('�', '�', '�', '��', '�'), true);
                    $this->srule = 401;
                    return true;
                }
                elseif ($this->in($BeforeLast, '��'))
                {
                    $this->secondResult = $this->padeg($this->secondName, array('�', '�', '�', '��', '�'), true);
                    $this->srule = 402;
                    return true;
                }
                else
                {
                    $this->secondResult = $this->padeg($this->secondName, array('�', '�', '�', '��', '�'), true);
                    $this->srule = 4;
                    return true;
                }
            }
            //������� � ������� �����, �������������� �� -�, -��, -��, -��, ���������� �� �����, �� �������� ��� ����������, ���������� ��� ��������������� � ���������������� �����������
            elseif ($LastSymbol == "�")
            {
                $this->secondResult = $this->padeg($this->secondName, array('��', '��', '��', '��', '��'), true, true);
                $this->srule = 6;
                return true;
            }
        }
        //���� ������� �� �����, ����� ��������� ��� ����
        if (!isset($this->secondResult[0]))
        {
            $this->makeSecondTheSame();
            return false;
        }
    }

    /*
    * �������, ������� �������� ������� ��������� � $this->secondName, �� �������� ��������� ������� �������.
    *
    * @return boolean
    */

    private function womanSecondName()
    {
        //��������� ������
        $LastSymbol = mb_substr($this->secondName, -1, 1, 'utf-8');
        //������������� ������
        $BeforeLast = mb_substr($this->secondName, -2, 1, 'utf-8');
        $needToChange = true;
        $this->secondResult = array();
        //�� ����������
        //������� �������, �������������� �� ��������� ���� � ������ ����
        if ($this->in($LastSymbol, $this->consonant) or $LastSymbol == '�')
        {
            $needToChange = false;
            $this->error = 1;
        }
        //������� �� -�, -� � �������������� ������� -�
        if ($this->in($LastSymbol, '��') and $BeforeLast == "�")
        {
            $needToChange = false;
            $this->error = 4;
        }
        /* ������� �������, �������������� ����� ��������� ����� ������������ ������ ������������� ����� � �����������: -���, -���, -��� (�������, ������, ������, ���������, ������, �������) � �������������� ����� � �����������: -��, -�� (��������, ����������, ��������, ������, �����),  + ���������� �� �� */
        if (in_array(mb_substr($this->secondName, -3, 3, 'utf-8'), $this->ovo) or in_array(mb_substr($this->secondName, -2, 2, 'utf-8'), $this->ih))
        {
            $needToChange = false;
            $this->error = 5;
        }
        //���� ����� ��������
        if ($needToChange)
        {
            //������� � ������� �����, �������������� �� -�, ����������, ��� � ����� ��������������� � ����� �� ����������
            if ($LastSymbol == "�")
            {
                if ($this->in($BeforeLast, '��'))
                {
                    $this->secondResult = $this->padeg($this->secondName, array('�', '�', '�', '��', '�'), true);
                    $this->srule = 501;
                    return true;
                }
                elseif ($this->in($BeforeLast, '�'))
                {
                    $this->secondResult = $this->padeg($this->secondName, array('�', '�', '�', '��', '�'), true);
                    $this->srule = 502;
                    return true;
                }
                else
                {
                    $this->secondResult = $this->padeg($this->secondName, array('��', '��', '�', '��', '��'), true);
                    $this->srule = 5;
                    return true;
                }
            }
            //������� � ������� �����, �������������� �� -�, -��, -��, -��, ���������� �� �����, �� �������� ��� ����������, ���������� ��� ��������������� � ���������������� �����������
            elseif ($LastSymbol == "�")
            {
                $this->secondResult = $this->padeg($this->secondName, array('��', '��', '��', '��', '��'), true, true);
                $this->srule = 6;
                return true;
            }
        }

        //���� ������� �� �����, ����� ��������� ��� ����
        if (!isset($this->secondResult[0]))
        {
            $this->makeSecondTheSame();
            return false;
        }
    }

    /*
    * �������, ������� �������� �������� ��������� � $this->secondName, �� �������� ��������� ������� �������.
    *
    * @return boolean
    */

    private function manFatherName()
    {
        //��������� ������������� �� ��������
        if ($this->fatherName == '�����')
        {
            $this->fatherResult = $this->padeg($this->fatherName, array('�', '�', '�', '��', '�'), false, false);
            return true;
        }
        elseif (mb_substr($this->fatherName, -2, 2, 'utf-8') == '��')
        {
            $this->fatherResult = $this->padeg($this->fatherName, array('�', '�', '�', '��', '�'), false, false);
            return true;
        }
        else
        {
            $this->makeFatherTheSame();
            return false;
        }
    }

    /*
    * �������, ������� �������� �������� ��������� � $this->fatherName, �� �������� ��������� ������� �������.
    *
    * @return boolean
    */

    private function womanFatherName()
    {
        //��������� ������������� �� ��������
        if (mb_substr($this->fatherName, -2, 2, 'utf-8') == '��')
        {
            $this->fatherResult = $this->padeg($this->fatherName, array('�', '�', '�', '��', '�'), true, false);
            return true;
        }
        else
        {
            $this->makeFatherTheSame();
            return false;
        }
    }

    /*
    * �������, ������� ������ ��� �� ���� ������� � ����� ������������� ������.
    *
    * @return void
    */

    private function makeFirstTheSame()
    {
        $this->firstResult = array_fill(0, 6, $this->firstName);
    }

    /*
    * �������, ������� ������ ������� �� ���� ������� � ����� ������������� ������.
    *
    * @return void
    */

    private function makeSecondTheSame()
    {
        $this->secondResult = array_fill(0, 6, $this->secondName);
    }

    /*
    * �������, ������� ������ ������� �� ���� ������� � ����� ������������� ������.
    *
    * @return void
    */

    private function makeFatherTheSame()
    {
        $this->fatherResult = array_fill(0, 6, $this->fatherName);
    }

    /*
    * ������� ���������, ������ �� ����� � ������.
    *
    * @param $letter - �����
    * @param $string - ������
    *
    * @return boolean
    */

    private function in($letter, $string)
    {

        if ($letter and mb_strpos($string, $letter) === false)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    /*
    * ������� ��������� ��������� ����� ������� �����������.
    *
    * @param $word (string) - �����
    * @param $endings (array) - ���������
    * @param $replaceLast (boolean) - ������ ��������� �����
    * @param $replaceTwoLast (boolean) - ������ ��� ��������� �����
    *
    * @return boolean
    */

    private function padeg($word, $endings, $replaceLast=false, $replaceTwoLast=false)
    {
        $result = array($word);
        if ($replaceTwoLast == true)
        {
            //������� ��������� �����
            $word = mb_substr($word, 0, mb_strlen($word, 'utf-8') - 2, 'utf-8');
        }
        elseif ($replaceLast == true)
        {
            //������� ��������� �����
            $word = mb_substr($word, 0, mb_strlen($word, 'utf-8') - 1, 'utf-8');
        }
        $i = 0;
        for ($i == 0; $i < 5; $i++)
        {
            $result[$i + 1] = $word . $endings[$i];
        }
        return $result;
    }

    /*
    * ��������� �����
    *
    * @param $firstname
    *
    * @return void
    */

    public function setFirstName($firstname="")
    {
        $this->firstName = $firstname;
    }

    /*
    * ��������� �������
    *
    * @param $secondname
    *
    * @return void
    */

    public function setSecondName($secondname="")
    {
        $this->secondName = $secondname;
    }

    /*
    * ��������� ��������
    *
    * @param $secondname
    *
    * @return void
    */

    public function setFatherName($fathername="")
    {
        $this->fatherName = $fathername;
    }

    /*
    * ��������� ����
    *
    * @param $gender
    * - null - �� ����������
    * - NCLNameCaseRu::$MAN - �������
    * - NCLNameCaseRu::$WOMAN - �������
    * @return void
    */

    public function setGender($gender=0)
    {
        $this->gender = $gender;
    }

    /*
    * ��������� �����, �������, ��������
    *
    * @param $firstName - ���
    * @param $secondName - �������
    * @param $fatherName - ��������
    *
    * @return void
    */

    public function setFullName($secondName="", $firstName="", $fatherName="")
    {
        $this->setFirstName($firstName);
        $this->setSecondName($secondName);
        $this->setFatherName($fatherName);
    }

    /*
    * ��������� �����
    *
    * @param $firstname
    *
    * @return void
    */

    public function setName($firstname="")
    {
        $this->setFirstName($firstname);
    }

    /*
    * ��������� �������
    *
    * @param $secondname
    *
    * @return void
    */

    public function setLastName($secondname="")
    {
        $this->setSecondName($secondname);
    }

    /*
    * ��������� �������
    *
    * @param $secondname
    *
    * @return void
    */

    public function setSirname($secondname="")
    {
        $this->setSecondName($secondname);
    }

    /*
    * �������������� ����������� ����
    * @return void
    */

    private function genderDetect()
    {
        if (!$this->gender)
        {
            //����������� ���� �� ��������
            if (isset($this->fatherName) and $this->fatherName)
            {
                $LastTwo = mb_substr($this->fatherName, -2, 2, 'utf-8');
                if ($LastTwo == '��')
                {
                    $this->gender = NCLNameCaseRu::$MAN; // �������
                    return true;
                }
                if ($LastTwo == '��')
                {
                    $this->gender = NCLNameCaseRu::$WOMAN; // �������
                    return true;
                }
            }
            $man = 0; //�������
            $woman = 0; //�������
            $FLastSymbol = mb_substr($this->firstName, -1, 1, 'utf-8');
            $FLastTwo = mb_substr($this->firstName, -2, 2, 'utf-8');
            $FLastThree = mb_substr($this->firstName, -3, 3, 'utf-8');
            $FLastFour = mb_substr($this->firstName, -4, 4, 'utf-8');

            $SLastSymbol = mb_substr($this->secondName, -1, 1, 'utf-8');
            $SLastTwo = mb_substr($this->secondName, -2, 2, 'utf-8');
            $SLastThree = mb_substr($this->secondName, -3, 3, 'utf-8');
            //���� ��� ��������, �� ���������� �� ����� � �������, ����� ������� �����������
            if (isset($this->firstName) and $this->firstName)
            {
                //��������� ������ �������� �� �����
                //���� ��� ������������� �� �, �� ������ ����� �������
                if ($FLastSymbol == '�')
                {
                    $man+=0.9;
                }
                if (in_array($FLastTwo, array('��', '��', '��', '��', '��', '��', '��', '��')))
                {
                    $man+=0.3;
                }
                if ($this->in($FLastSymbol, $this->consonant))
                {
                    $man+=0.01;
                }
                if ($FLastSymbol == '�')
                {
                    $man+=0.02;
                }

                if (in_array($FLastTwo, array('��', '��', '��')))
                {
                    $woman+=0.1;
                }

                if (in_array($FLastTwo, array('��')))
                {
                    $woman+=0.04;
                }

                if (in_array($FLastTwo, array('��', '��')))
                {
                    $man+=0.01;
                }

                if (in_array($FLastThree, array('���', '���', '���', '���', '���')))
                {
                    $man+=0.2;
                }

                if (in_array($FLastThree, array('���')))
                {
                    $woman+=0.15;
                }

                if (in_array($FLastThree, array('���', '���', '���', '���', '���', '���')))
                {
                    $woman+=0.5;
                }

                if (in_array($FLastFour, array('����', '����', '����', '����')))
                {
                    $woman+=0.5;
                }
            }
            if (isset($this->secondName) and $this->secondName)
            {
                if (in_array($SLastTwo, array('��', '��', '��', '��', '��', '��', '��', '��')))
                {
                    $man+=0.4;
                }

                if (in_array($SLastThree, array('���', '���', '���', '���', '���', '��')))
                {
                    $woman+=0.4;
                }

                if (in_array($SLastTwo, array('��')))
                {
                    $woman+=0.4;
                }
            }
            //������ �������, ��� ������ ������
            if ($man > $woman)
            {
                $this->gender = NCLNameCaseRu::$MAN;
            }
            else
            {
                $this->gender = NCLNameCaseRu::$WOMAN;
            }
        }
    }

    /*
    * �������������� ����������� ����
    * ���������� ��� �� ���
    * @return integer
    */

    public function genderAutoDetect()
    {
        $this->gender = null;
        $this->genderDetect();
        return $this->gender;
    }

    /*
    * ����������� ������� ����� ���� ��������, ������ ��� ���������
    * @return integer $number - 1-������ 2-��� 3-��������
    */

    private function detectNamePart($namepart)
    {
        $LastSymbol = mb_substr($namepart, -1, 1, 'utf-8');
        $LastTwo = mb_substr($namepart, -2, 2, 'utf-8');
        $LastThree = mb_substr($namepart, -3, 3, 'utf-8');
        $LastFour = mb_substr($namepart, -4, 4, 'utf-8');

        //������� �����������
        $first = 0;
        $second = 0;
        $father = 0;

        //���� ��������� �� ��������
        if (in_array($LastThree, array('���', '���', '���', '���')))
        {
            $father+=3;
        }

        //������ �� ���
        if (in_array($LastThree, array('���', '���')))
        {
            $first+=0.5;
        }

        //����������
        if (in_array($namepart, array('���', '����', '��������', '��������', '�������', '���������', '��������', '����', '�����', '������', '�����', '������', '���������', '������', '������', '���������', '��������', '����', '�����', '��������', '����������', '������', '�����', '������', '�������', '�����', '��������', '������', '��������', '�������', '������', '�����', '����', '����')))
        {
            $first+=10;
        }

        //������ �� �������
        if (in_array($LastTwo, array('��', '��', '��', '��', '��', '��', '��', '��', '��', '��', '��', '��', '��', '��', '��', '��', '��', '��', '��', '��', '��', '��', '��', '��', '��', '��')))
        {
            $second+=0.4;
        }

        if (in_array($LastThree, array('���', '���', '���', '���', '���', '���', '���', '���', '���', '���', '���', '���', '���', '���', '���', '���', '���')))
        {
            $second+=0.4;
        }

        if (in_array($LastFour, array('����', '����', '����', '����', '����', '����')))
        {
            $second+=0.4;
        }

        $max = max(array($first, $second, $father));

        if ($first == $max)
        {
            return 'N';
        }
        elseif ($second == $max)
        {
            return 'S';
        }
        else
        {
            return 'F';
        }
    }

    /*
    * ��������� ����� �� ����� � �����������, ��� ���, ��� �������, ��� ��������
    * @return string $format - ������ ���� � �������
    */

    public function splitFullName($fullname)
    {
        $this->firstName = '';
        $this->secondName = '';
        $this->fatherName = '';
        $this->gender = null;

        $fullname = trim($fullname);
        $list = explode(' ', $fullname);
        $found = array();
        $duplicate = array();
        $c = count($list);
        for ($i = 0; $i < $c; $i++)
        {
            if (trim($list[$i]))
            {
                $found[$i] = $this->detectNamePart($list[$i]);
            }
        }
        $look = array('S' => false, 'F' => false, 'N' => false);
        //������ �������� - ���� ���������
        foreach ($found as $key => $letter)
        {
            if ($look[$letter]) //���� ��� ����
            {
                $duplicate[$key] = $letter;
            }
            else
            {
                $look[$letter] = true;
            }
        }
        //������ �������� - ������ ���������
        foreach ($duplicate as $key => $letter)
        {
            if (!$look['S'])
            {
                $found[$key] = 'S';
            }
            elseif (!$look['F'])
            {
                $found[$key] = 'F';
            }
            elseif (!$look['N'])
            {
                $found[$key] = 'N';
            }
            else
            {
                $found[$key] = ''; //4 ����� ���� ����������
            }
        }
        $format = '';
        foreach ($found as $key => $letter)
        {
            if ($letter == 'S')
            {
                $this->secondName = $list[$key];
            }
            elseif ($letter == 'N')
            {
                $this->firstName = $list[$key];
            }
            elseif ($letter == 'F')
            {
                $this->fatherName = $list[$key];
            }
            $format.=$letter . ' ';
        }
        return trim($format);
    }

    /*
    * ��������� �����
    *
    * @return boolean
    */

    private function FirstName()
    {
        $this->genderDetect();
        if ($this->firstName)
        {
            if ($this->gender == 1)
            {
                return $this->manFirstName();
            }
            else
            {
                return $this->womanFirstName();
            }
        }
        else
        {
            $this->firstResult = array_fill(0, 6, "");
            return false;
        }
    }

    /*
    * ��������� �������
    *
    * @return boolean
    */

    private function SecondName()
    {
        $this->genderDetect();
        if ($this->secondName)
        {
            if ($this->gender == 1)
            {
                return $this->manSecondName();
            }
            else
            {
                return $this->womanSecondName();
            }
        }
        else
        {
            $this->secondResult = array_fill(0, 6, "");
            return false;
        }
    }

    /*
    * ��������� �������
    *
    * @return boolean
    */

    private function FatherName()
    {
        $this->genderDetect();
        if ($this->fatherName)
        {
            if ($this->gender == 1)
            {
                return $this->manFatherName();
            }
            else
            {
                return $this->womanFatherName();
            }
        }
        else
        {
            $this->fatherResult = array_fill(0, 6, "");
            return false;
        }
    }

    /*
    * ��������� ��� � ������������ �����
    *
    * @return string
    */

    public function getFirstNameCase($number=null)
    {
        if (!isset($this->firstResult[0]) or $this->firstResult[0] <> $this->firstName)
        {
            $this->FirstName();
        }
        if ($number < 0 or $number > 5)
        {
            $number = null;
        }

        if (is_null($number))
        {
            //���������� ��� ������
            return $this->firstResult;
        }
        else
        {
            return $this->firstResult[$number];
        }
    }

    /*
    * ��������� ������� � ������������ �����
    *
    * @return string
    */

    public function getSecondNameCase($number=null)
    {
        if (!isset($this->secondResult[0]) or $this->secondResult[0] <> $this->secondName)
        {
            $this->SecondName();
        }
        if ($number < 0 or $number > 5)
        {
            $number = null;
        }

        if (is_null($number))
        {
            //���������� ��� ������
            return $this->secondResult;
        }
        else
        {
            return $this->secondResult[$number];
        }
    }

    /*
    * ��������� �������� � ������������ �����
    *
    * @return string
    */

    public function getFatherNameCase($number=null)
    {
        if (!isset($this->fatherResult[0]) or $this->fatherResult[0] <> $this->fatherName)
        {
            $this->FatherName();
        }
        if ($number < 0 or $number > 5)
        {
            $number = null;
        }

        if (is_null($number))
        {
            //���������� ��� ������
            return $this->fatherResult;
        }
        else
        {
            return $this->fatherResult[$number];
        }
    }

    /*
    * ��������� ������� � ������������ �����
    *
    * @return string
    */

    public function qFirstName($firstName, $CaseNumber=null, $gender=0)
    {
        $this->gender = $gender;
        $this->firstName = $firstName;
        return $this->getFirstNameCase($CaseNumber);
    }

    /*
    * ��������� ������� � ������������ �����
    *
    * @return string
    */

    public function qSecondName($secondName, $CaseNumber=null, $gender=0)
    {
        $this->gender = $gender;
        $this->secondName = $secondName;
        return $this->getSecondNameCase($CaseNumber);
    }

    /*
    * ��������� �������� � ������������ �����
    *
    * @return string
    */

    public function qFatherName($fatherName, $CaseNumber=null, $gender=0)
    {
        $this->gender = $gender;
        $this->fatherName = $fatherName;
        return $this->getFatherNameCase($CaseNumber);
    }

    /*
    * �������� �� ��� ������ � ����������� �� ������� $format
    * ������ $format
    * S - �������
    * N - ���
    * F - ��������
    *
    * @return array
    */

    public function getFormattedArray($format)
    {
        $length = mb_strlen($format);
        $result = array();
        $cases=array();
        for ($i = 0; $i < $length; $i++)
        {
            $symbol = mb_substr($format, $i, 1);
            if ($symbol == 'S')
            {
                $cases['S']=$this->getSecondNameCase();
            }
            elseif ($symbol == 'N')
            {
                $cases['N']=$this->getFirstNameCase();
            }
            elseif ($symbol == 'F')
            {
                $cases['F']=$this->getFatherNameCase();
            }
        }

        for ($curCase = 0; $curCase<6; $curCase++)
        {
            $line="";
            for ($i = 0; $i < $length; $i++)
            {
                $symbol = mb_substr($format, $i, 1);
                if ($symbol == 'S')
                {
                    $line.=$cases['S'][$curCase];
                }
                elseif ($symbol == 'N')
                {
                    $line.=$cases['N'][$curCase];
                }
                elseif ($symbol == 'F')
                {
                    $line.=$cases['F'][$curCase];
                }
                else
                {
                    $line.=$symbol;
                }
            }
            $result[]=$line;
        }
        return $result;
    }

    /*
    * �������� � ����� $caseNum, � ����������� �� ������� $format
    * ������ $format
    * S - �������
    * N - ���
    * F - ��������
    *
    * �������� getFormatted(1, 'N F')
    * ������� ��� � �������� � ����������� ������
    *
    * @return string
    */

    public function getFormatted($caseNum=0, $format="S N F")
    {
        if (is_null($caseNum))
        {
            return $this->getFormattedArray($format);
        }
        else
        {
            $length = mb_strlen($format);
            $result = "";
            for ($i = 0; $i < $length; $i++)
            {
                $symbol = mb_substr($format, $i, 1);
                if ($symbol == 'S')
                {
                    $result.=$this->getSecondNameCase($caseNum);
                }
                elseif ($symbol == 'N')
                {
                    $result.=$this->getFirstNameCase($caseNum);
                }
                elseif ($symbol == 'F')
                {
                    $result.=$this->getFatherNameCase($caseNum);
                }
                else
                {
                    $result.=$symbol;
                }
            }
            return $result;
        }
    }

    /*
    * �������� ������� ��� �������� � ����� $caseNum, � ����������� �� ������� $format
    * ������ $format
    * S - �������
    * N - ���
    * F - ��������
    *
    * @return string
    */

    public function qFullName($secondName="", $firstName="", $fatherName="", $gender=0, $caseNum=0, $format="S N F")
    {
        $this->gender = $gender;
        $this->firstName = $firstName;
        $this->secondName = $secondName;
        $this->fatherName = $fatherName;

        return $this->getFormatted($caseNum, $format);
    }

    public function getFirstNameRule()
    {
        return $this->frule;
    }

    public function getSecondNameRule()
    {
        return $this->srule;
    }

    /*
    * ������� ��������� �����. ���������� ���� �������� ������, ��� ����� ���� ��� � ����� ����. ���� �������������� �������� ���. � ��� �� �������������� �������� �����. ���� ����� ������, ����� ������������ ������ � ��� ������, ���� ��� ����� ��� ��������� ������.
    *
    * @return string
    */

    public function q($fullname, $caseNum=null, $gender=null)
    {
        $format = $this->splitFullName($fullname);
        $this->gender = $gender;
        $this->genderAutoDetect();
        return $this->getFormatted($caseNum, $format);
    }

}

?>