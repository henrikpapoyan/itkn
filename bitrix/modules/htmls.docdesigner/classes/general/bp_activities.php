<?
/*******************************************************************************
* Module:   DocDesigner                                                        *
* Class:    CDocDesignerBPActivities extends CDocDesigner                      *
* Version:  8.4.12                                                             *
* Author:   Alexey Andreev, HTMLStudio (www.htmls.ru)                          *
* License:  Shareware                                                          *
* Date:     2017-04-07                                                         *
* e-mail:   aav@htmls.ru                                                       *
*******************************************************************************/
IncludeModuleLangFile(__FILE__);
//require_once($_SERVER["DOCUMENT_ROOT"].'/bitrix/modules/htmls.docdesigner/classes/general/phpword/PHPWord.php');
if(COption::GetOptionString('htmls.docdesigner', 'DOCDESIGNER_ADDMESSAGE2LOG') == 'Y'){
	define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/bitrix/tmp/DocDesigner.txt");
}
class CDocDesignerBPActivities extends CDocDesigner{
    public $bpParameters = array();
    public $isCRM = true;

	function CDocDesignerBPActivities(){
		$this->Add2Log = COption::GetOptionString('htmls.docdesigner', 'DOCDESIGNER_ADDMESSAGE2LOG');
        $this->DigitCol = (COption::GetOptionString('htmls.docdesigner', 'DOCDESIGNER_REMOVE_PENNY', 'N') == 'Y')? 0 : 2;
        $this->quantityFloat = COption::GetOptionString('htmls.docdesigner', 'DOCDESIGNER_QUANTITY_FLOAT', '0');

        $this->isCRM = IsModuleInstalled("crm");

	}
    /*
    $type_id = 4 - company
    $type_id = 3 - contact
    */
    function getCrmRequisite($cid, $type_id = 4){
      global $DB;

      //CModule::IncludeModule('crm');
      //AddMessage2Log($cid, "htmls.DocDesigner");
        $requisite = new \Bitrix\Crm\EntityRequisite();
        //AddMessage2Log(print_r($requisite, true), "htmls.DocDesigner");
        $result = $requisite->getList(array("filter" => array("ENTITY_ID" => $cid, "ENTITY_TYPE_ID" => $type_id)));
        //AddMessage2Log(print_r($result, true), "htmls.DocDesigner");
        if($res = $result->Fetch()){
          $bank = new \Bitrix\Crm\EntityBankDetail();
          $resultBank = $bank->getList(array("filter" => array("ENTITY_ID" => $res['ID'])));
          if($resultBank = $resultBank->Fetch()){
            $res['bank'] = $resultBank;
          }

          $Addresses = Bitrix\Crm\EntityRequisite::getAddresses($res["ID"]);
          if($u = $Addresses[6]){
            $res['uridadr'] = $u;
          }
          if($f = $Addresses[1]){
            $res['factadr'] = $f;
          }
          //echo '<pre>';print_r($Addresses);echo '</pre>';
          /*
          $strSql = 'SELECT * FROM  b_crm_addr WHERE ENTITY_TYPE_ID=8 AND TYPE_ID=6 AND ENTITY_ID=' . $res['ID'];
          $resDb = $DB->Query($strSql);
          if($arRes = $resDb->Fetch()){
            $res['uridadr'] = $arRes;
          }

          $strSql = 'SELECT * FROM  b_crm_addr WHERE ENTITY_TYPE_ID=8 AND TYPE_ID=1 AND ENTITY_ID=' . $res['ID'];
          $resDb = $DB->Query($strSql);
          if($arRes = $resDb->Fetch()){
            $res['factadr'] = $arRes;
          }
          */
          return $res;
        }
        else return array();
    }

	//used in CreateInvoice & CreatePdf
	function SetCRMParameters($rootActivity, &$arProperties, &$arPropertiesTypes, $DocumentId, &$CDocDes, $CContract, &$num2str, &$Total, &$DEAL_ID, &$COMPANY_ID, &$invProd, $Numerator, &$arCrmId){
	  //AddMessage2Log(print_r($DocumentId, true), "htmls.DocDesigner");
		$module_id = "htmls.docdesigner";
        $this->DigitCol = (COption::GetOptionString('htmls.docdesigner', 'DOCDESIGNER_REMOVE_PENNY', 'N') == 'Y')? 0 : 2;
        if($DocumentId[1] == 'CCrmDocumentLead'){
        	$LEAD_ID = intval(substr($DocumentId[2], strlen("LEAD_")));
			$Type = 'L';
			$ProdOwner = $LEAD_ID;
		}
        elseif($DocumentId[1] == 'CCrmDocumentDeal'){
        	$DEAL_ID = intval(substr($DocumentId[2], strlen("DEAL_")));
        	$Type = 'D';
			$ProdOwner = $DEAL_ID;
        }
        elseif($DocumentId[1] == 'CCrmDocumentContact'){
        	$contactId = intval(substr($DocumentId[2], strlen("CONTACT_")));
        	//$Type = 'D';
			//$ProdOwner = $DEAL_ID;
        }
        //AddMessage2Log($contactId, "htmls.DocDesigner");
        //RESPONSIBLE_ID - invoice
        //ASSIGNED_BY_ID
        $ResponsibleID = 0;
		//get company id
        $CURRENCY = 'RUB';
		if($DEAL_ID > 0){
		  if($this->isCRM){
		    $arCrmId['deal'] = $DEAL_ID;
			$res = CCrmDeal::GetList(array(),array('ID' => $DEAL_ID),array());
			while($ob = $res->Fetch()){
				$COMPANY_ID = $ob['COMPANY_ID'];
				$CONTACT_ID = $ob['CONTACT_ID'];
                $CURRENCY = $ob['CURRENCY_ID'];
                $DEAL_NAME = $ob['TITLE'];
				$arDeal = $ob;
                $ResponsibleID = $ob['ASSIGNED_BY_ID'];
                //AddMessage2Log('deal_id='.$DEAL_ID, "htmls.DocDesigner");
                //AddMessage2Log(print_r($ob, true), "htmls.DocDesigner");
			}
          }
		}
        //AddMessage2Log($CURRENCY, "htmls.DocDesigner");
		if($LEAD_ID > 0){
		  if($this->isCRM){
		    $arCrmId['lead'] = $LEAD_ID;
			$res = CCrmLead::GetList(array(),array('ID' => $LEAD_ID),array());
			while($ob = $res->Fetch()){
				$COMPANY_ID = $ob['COMPANY_ID'];
                $this->CURRENCY = $ob['CURRENCY_ID'];
				$arDeal = $ob;
                $DEAL_NAME = $ob['TITLE'];
                //AddMessage2Log(print_r($ob, true), "htmls.DocDesigner");
                foreach($arDeal as $k => $v){
                  $arProperties['LEAD.'.$k] = $v;
                  //$arProperties[$key . '.'.$k] = $v;
                }
                if($ResponsibleID == 0)
                    $ResponsibleID = $ob['ASSIGNED_BY_ID'];
                //AddMessage2Log(print_r($arProperties, true), "htmls.DocDesigner");
			}
          }
		}
        $CDocDes->CURRENCY_ID = $CURRENCY;
        //AddMessage2Log($CURRENCY, "htmls.DocDesigner");
        ///AddMessage2Log($GLOBALS["DOCUMENT_ROOT"]."/bitrix/modules/htmls.docdesigner/lang/ru/".$CURRENCY.'.php', "htmls.DocDesigner");
        //AddMessage2Log(file_exists($GLOBALS["DOCUMENT_ROOT"]."/bitrix/modules/htmls.docdesigner/lang/ru/".$CURRENCY.'.php'), "htmls.DocDesigner");
        if($CURRENCY != 'RUB'){
          if(file_exists($GLOBALS["DOCUMENT_ROOT"]."/bitrix/modules/htmls.docdesigner/lang/ru/".$CURRENCY.'.php')){
            require_once($GLOBALS["DOCUMENT_ROOT"]."/bitrix/modules/htmls.docdesigner/lang/ru/".$CURRENCY.'.php');

            $num2str->w_1_2 = $MESS['_w_1_2'];
    		$num2str->w_1_19 = $MESS['_w_1_19'];
    		$num2str->des = $MESS['_des'];
    		$num2str->hang = $MESS['_hang'];
    		$num2str->namerub = $MESS['_namerub'];
    		$num2str->nametho = $MESS['_nametho'];
    		$num2str->namemil = $MESS['_namemil'];
    		$num2str->namemrd = $MESS['_namemrd'];
    		$num2str->kopeek = $MESS['_kopeek'];
          }
        }
        if($DEAL_ID > 0){
            $s = $num2str->num2str($arDeal['OPPORTUNITY']);
            $s = trim(CDocDesigner::win2uni($s));
            $val2 = CDocDesigner::first_letter_up($s);
            $s = CDocDesigner::uni2win($val2);
	        $arProperties['DEAL.OPPORTUNITYSpelling'] = $s;
	        $arProperties['DEAL.OPPORTUNITY_SPELLING'] = $arProperties['DEAL.OPPORTUNITYSpelling'];
	    }
        elseif($LEAD_ID > 0){
            $fullName = $arDeal['LAST_NAME'] . ' ' . $arDeal['NAME'] . ' ' . $arDeal['SECOND_NAME'];
            if(strtoupper(LANG_CHARSET) == 'WINDOWS-1251')
              $shortName = $arDeal['LAST_NAME'] . ' ' . substr($arDeal['NAME'], 0, 1) . '. ' . substr($arDeal['SECOND_NAME'], 0, 1) . '.';
            else
              $shortName = $arDeal['LAST_NAME'] . ' ' . mb_substr($arDeal['NAME'], 0, 1, 'UTF-8') . '. ' . mb_substr($arDeal['SECOND_NAME'], 0, 1, 'UTF-8') . '.';
            $arProperties['LEAD.FULL_NAME'] = $fullName;
            $arProperties['LEAD.SHORT_NAME'] = $shortName;

            $arFFIO = $CDocDes->getDeclination($fullName);
            $arProperties['LEAD.FULL_NAME.RP'] = $arFFIO[1];
            $arProperties['LEAD.FULL_NAME_G'] = $arFFIO[1];
    		$arProperties['LEAD.FULL_NAME.DP'] = $arFFIO[2];
    		$arProperties['LEAD.FULL_NAME_D'] = $arFFIO[2];

		    $s = $num2str->num2str($arDeal['OPPORTUNITY']);
            $s = trim(CDocDesigner::win2uni($s));
            $val2 = CDocDesigner::first_letter_up($s);
            $s = CDocDesigner::uni2win($val2);
		    $arProperties['LEAD.OPPORTUNITYSpelling'] = $s;
		    $arProperties['LEAD.OPPORTUNITY_SPELLING'] = $arProperties['LEAD.OPPORTUNITYSpelling'];
	    }


		if(intval($COMPANY_ID) == 0){
			$COMPANY_ID = substr($DocumentId[2], strlen("COMPANY_"));
		}

		if(!is_dir($_SERVER['DOCUMENT_ROOT'] . "/upload/htmls.docdesigner/COMPANY_".$COMPANY_ID."/"))
			mkdir($_SERVER['DOCUMENT_ROOT'] . "/upload/htmls.docdesigner/COMPANY_".$COMPANY_ID."/");
		//get company info
        //AddMessage2Log('CCrmCompany::GetListEx', "htmls.DocDesigner");
        if($this->isCRM){
    		$res = CCrmCompany::GetListEx(array(),array('ID' => $COMPANY_ID), false, false, array('*', 'UF_*', 'PHONE', 'EMAIL'));
    		while($ob = $res->GetNext()){
    		  //AddMessage2Log(print_r($ob, true), "htmls.DocDesigner");
    		  $arCrmId['company'] = $ob['ID'];
    			$COMPANY_NAME = $ob['TITLE'];
    			$COMPANY = $ob;
                if($ResponsibleID == 0)
                    $ResponsibleID = $ob['ASSIGNED_BY_ID'];
    		}
        }
		//get person info
		if(!isset($arProperties['DocDesignerPerson'])){
			//get person ID from deal

		    $arProperties['DocDesignerPerson'] = $arDeal['CONTACT_ID'];
		}
		elseif(intval($arProperties['DocDesignerPerson']) == 0){
			//get person ID from deal
			$arProperties['DocDesignerPerson'] = $arDeal['CONTACT_ID'];
		}
        if(intval($contactId) > 0){
			//get person ID from deal
			$arProperties['DocDesignerPerson'] = $contactId;
		}
        if($CONTACT_ID > 0 && intval($arProperties['DocDesignerPerson'] == 0)){
          $arProperties['DocDesignerPerson'] = $CONTACT_ID;
        }
        //AddMessage2Log(print_r($arProperties, true), "htmls.DocDesigner");
		if(intval($arProperties['DocDesignerPerson'] > 0)){
		  if($this->isCRM){
			$res = CCrmContact::GetListEx(array(), Array('ID' => $arProperties['DocDesignerPerson']), false, false, array('*', 'UF_*', 'PHONE', 'EMAIL'));//, array());
			while($ob = $res->GetNext()){
			  $arCrmId['contact'] = $ob['ID'];
				$arContact = $ob;
                $arContact['FULL_NAME_SHORT'] = $ob['LAST_NAME'];
                $arContact['FULL_FIO'] = $ob['LAST_NAME'];

                if(strtoupper(LANG_CHARSET) == 'WINDOWS-1251'){
                  $arContact['FULL_NAME_SHORT'] .= ' ' . $ob['NAME'][0] . '.';
                  $arContact['FULL_FIO'] .= ' ' . $ob['NAME'];
                  if($sn = $ob['SECOND_NAME']){
                    $arContact['FULL_NAME_SHORT'] .= ' ' . $sn[0] . '.';
                    $arContact['FULL_FIO'] .= ' ' . $sn;
                  }
                }
                else{
                  $n = iconv("UTF-8", "cp1251", $ob['NAME']);
                  $arContact['FULL_NAME_SHORT'] .= ' ' . iconv("cp1251", "UTF-8", $n[0]) . '.';
                  $arContact['FULL_FIO'] .= ' ' . $ob['NAME'];
                  if($sn = $ob['SECOND_NAME']){
                    $sn = iconv("UTF-8", "cp1251", $sn);
                    $arContact['FULL_NAME_SHORT'] .= ' ' . iconv("cp1251", "UTF-8", $sn[0]) . '.';
                    $arContact['FULL_FIO'] .= ' ' . $ob['SECOND_NAME'];
                  }
                }
                $arContact['FULL_FIO.RP'] = $arContact['FULL_FIO'];
                $arContact['FULL_FIO_G'] = $arContact['FULL_FIO'];
                $arContact['FULL_NAME_SHORT.RP'] = $arContact['FULL_NAME_SHORT'];
                $arContact['FULL_NAME_SHORT_G'] = $arContact['FULL_NAME_SHORT'];
                $arContact['POST_G'] = $arContact['POST'];

                $CONTACT_NAME = $ob['LAST_NAME'] . ' ' . $ob['NAME'] . ' ' . $ob['SECOND_NAME'];
                if($ResponsibleID == 0)
                    $ResponsibleID = $ob['ASSIGNED_BY_ID'];
			}
          }
		}
		else{
			$arContact = array('CONTACT.LAST_NAME'=>'', 'CONTACT.NAME'=>'','CONTACT.SECOND_NAME'=>'',
			'CONTACT.FULL_NAME'=>'','CONTACT.POST'=>'','CONTACT.ADDRESS'=>'',
            'CONTACT.FULL_NAME_SHORT'=>'');
		}

		$arCrmFields = $CDocDes->GetCrmFields();

		$arProperties = self::SetCrmProperties($arProperties, $arCrmFields, $arDeal, $COMPANY, $arContact, 'PDF');
        //AddMessage2Log(print_r($arContact, true), "htmls.DocDesigner");
        //AddMessage2Log(print_r($arCrmFields['COMPANY'], true), "htmls.DocDesigner");

		foreach($arCrmFields['COMPANY'] as $key => $val){
			$arProperties["CrmCompany." . $val['VAR_NAME']] = $arProperties[$val['VAR_NAME']];
            if($arProperties[$val['VAR_NAME'] . '_G'])
                $arProperties["CrmCompany." . $val['VAR_NAME'] . '_G'] = $arProperties[$val['VAR_NAME'] . '_G'];
            if($arProperties[$val['VAR_NAME'] . '.RP'])
                $arProperties["CrmCompany." . $val['VAR_NAME'] . '.RP'] = $arProperties[$val['VAR_NAME'] . '.RP'];
		}
        if($COMPANY_ID > 0){
    		$arCompanyMulti = self::GetCrmMulti($COMPANY_ID, 'COMPANY');
    		foreach($arCompanyMulti as $key => $arr){
    			$arCompanyMulti[$key] = implode(",", $arr);
    		}
    		$arProperties = array_merge($arProperties, $arCompanyMulti);
            //AddMessage2Log(print_r($arProperties, true), "htmls.DocDesigner");
        }
		foreach($arCrmFields['DEAL'] as $key => $val){
    		if(is_array($val)){
    			$arProperties["CrmDeal." . $val['VAR_NAME']] = $arProperties[$val['VAR_NAME']];
    		}
    	}
        foreach($arCrmFields['LEAD'] as $key => $val){
    		if(is_array($val)){
    			$arProperties["CrmLead." . $val['VAR_NAME']] = $arProperties[$val['VAR_NAME']];
    		}
    	}
		foreach($arCrmFields['CONTACT'] as $key => $val){
			$arProperties["CrmContact." . $val['VAR_NAME']] = $arProperties[$val['VAR_NAME']];
		}
        if(intval($arProperties['DocDesignerPerson'] > 0)){
		    $arContactMulti = self::GetCrmMulti($arProperties['DocDesignerPerson'], 'CONTACT');
            foreach($arContactMulti as $key => $arr){
    			$arContactMulti[$key] = implode(",", $arr);
    		}
            $arProperties = array_merge($arProperties, $arContactMulti);
        }
        else{
            $arContact['CONTACT.PHONE_WORK'] = '';
            $arContact['CONTACT.PHONE_HOME'] = '';
        }



        //set responsible info
        if($ResponsibleID > 0){
            $rsUser = CUser::GetByID($ResponsibleID);
    		$arUser = $rsUser->Fetch();

            $fFIO = $arUser['LAST_NAME'] . ' ' . $arUser['NAME'];// . " " . $arUser['SECOND_NAME'];
    		$arFFIO = $CDocDes->getDeclination($fFIO);
    		$arProperties[$ParamName . '.FULLNAME.IMP'] = $fFIO;
    		$arProperties[$ParamName . '.FULLNAME.RP'] = $arFFIO[1];
    		$arProperties[$ParamName . '.FULLNAME.DP'] = $arFFIO[2];
            $arProperties['Responsible.SecondName'] = $arUser['SECOND_NAME'];
            $arProperties['Responsible.Name'] = $arUser['NAME'];
            $arProperties['Responsible.LastName'] = $arUser['LAST_NAME'];
            $arProperties['Responsible.SecondName_Name'] = $fFIO;
            $arProperties['Responsible.SecondName_Name.IMP'] = $fFIO;
            $arProperties['Responsible.SecondName_Name.RP'] = $arFFIO[1];
            $arProperties['Responsible.SecondName_Name.DP'] = $arFFIO[2];
            $arProperties['Responsible.EMAIL'] = $arUser['EMAIL'];
            $arProperties['Responsible.PHONE'] = $arUser['WORK_PHONE'];
            $arProperties['Responsible.Position'] = $arUser['WORK_POSITION'];
            //$arProperties['Responsible.FacsimileImg'] = $arUser['UF_FACSIMILE'];
            if($arUser['UF_FACSIMILE'] > 0){
				$path = CFile::GetPath($arUser['UF_FACSIMILE']);
				//$arCompany['Company.SEO_FACSIMILE'] = $path;
				if(!strpos($path, '://')){
					$path = '..' . $path;
				}
				//$arProperties['Responsible.FacsimileImg'] = $path;
				$arProperties['Responsible.FacsimileImg'] = '<img src="'.$path.'" border="0" />';
			}
            else{
              $arProperties['Responsible.FacsimileImg'] = '';
            }
        }

        //AddMessage2Log(print_r($arProperties, true), "htmls.DocDesigner");
		//set own company info
		$SET = "COMPANY_OGRN,COMPANY_INN,COMPANY_KPP,COMPANY_RS,COMPANY_POST_ADDRESS,COMPANY_ADDRESS_LEGAL,COMPANY_ADDRESS,COMPANY_PHONE,COMPANY_PERSON,COMPANY_BANK_NAME,COMPANY_BANK_KS,COMPANY_BANK_CITY,COMPANY_BANK_BIK";
		$arSet = explode(',',$SET);
		foreach($arSet as $str){
			$arSelect[COption::GetOptionString($module_id, 'DOCDESIGNER_' . $str)] = "PROPERTY_" . COption::GetOptionString($module_id, 'DOCDESIGNER_' . $str);
		}
       /*if($OwnCompany = $rootActivity->DocDesignerCompany){
       }
       else{
           $OwnCompany = $rootActivity->GetVariable('DocDesignerCompany');
       }*/
	    $arOwnCompany = $CContract->GetOwnCompanyData($arProperties['DocDesignerCompany'], $arSelect);
        //AddMessage2Log(print_r($arOwnCompany, true), __LINE__.'.'.__FUNCTION__);
		//$arOwnBank = $CContract->GetBankInfo($arOwnCompany["PROPERTY_" . COption::GetOptionString($module_id, 'DOCDESIGNER_COMPANY_BANK')]);

		$arProperties['Company.NAME'] = $arOwnCompany['NAME'];
		$arProperties['Company.INN'] = $arOwnCompany["PROPERTY_" . COption::GetOptionString($module_id, 'DOCDESIGNER_COMPANY_INN')];
		$arProperties['Company.KPP'] = $arOwnCompany["PROPERTY_" . COption::GetOptionString($module_id, 'DOCDESIGNER_COMPANY_KPP')];
		$arProperties['Company.OGRN'] = $arOwnCompany["PROPERTY_" . COption::GetOptionString($module_id, 'DOCDESIGNER_COMPANY_OGRN')];
		$arProperties['Company.BANK_RS'] = $arOwnCompany["PROPERTY_" . COption::GetOptionString($module_id, 'DOCDESIGNER_COMPANY_RS')];
		$arProperties['Company.BANK_NAME'] = $arOwnCompany["PROPERTY_" . COption::GetOptionString($module_id, 'DOCDESIGNER_COMPANY_BANK_NAME')];//$arOwnBank["NAME"];
		$arProperties['Company.BANK_BIK'] = $arOwnCompany["PROPERTY_" . COption::GetOptionString($module_id, 'DOCDESIGNER_COMPANY_BANK_BIK')];//$arOwnBank["BIK"];
		$arProperties['Company.BANK_KS'] = $arOwnCompany["PROPERTY_" . COption::GetOptionString($module_id, 'DOCDESIGNER_COMPANY_BANK_KS')];//$arOwnBank["KS"];
		$arProperties['Company.BANK_CITY'] = $arOwnCompany["PROPERTY_" . COption::GetOptionString($module_id, 'DOCDESIGNER_COMPANY_BANK_CITY')];//$arOwnBank["KS"];
		$arProperties['Company.ADDRESS_LEGAL'] = $arOwnCompany["PROPERTY_" . COption::GetOptionString($module_id, 'DOCDESIGNER_COMPANY_ADDRESS_LEGAL')];
		$arProperties['Company.ADDRESS'] = $arOwnCompany["PROPERTY_" . COption::GetOptionString($module_id, 'DOCDESIGNER_COMPANY_ADDRESS')];
		$arProperties['Company.POST_ADDRESS'] = $arOwnCompany["PROPERTY_" . COption::GetOptionString($module_id, 'DOCDESIGNER_COMPANY_POST_ADDRESS')];
		$arProperties['Company.PHONE'] = $arOwnCompany["PROPERTY_" . COption::GetOptionString($module_id, 'DOCDESIGNER_COMPANY_PHONE')];

        $OwnFIO = $arOwnCompany["PROPERTY_" . COption::GetOptionString($module_id, 'DOCDESIGNER_COMPANY_PERSON')];
		$arOwnFFIO = $CDocDes->getDeclination($OwnFIO);
		$arCEO_POST = $CDocDes->getDeclination($arOwnCompany["CEO_POST"]);
		$arCEO_BASE = $CDocDes->getDeclination($arOwnCompany["CEO_BASE"]);
		//$document->setValue('OWN_COMPANY_PERSON_FULL_NAME', $CDocDes->win2uni($arOwnFFIO[$CASE]));
		//$document->setValue('OWN_COMPANY_PERSON_SHORT_NAME', $CDocDes->win2uni($CDocDes->getShortName($OwnFIO)));
        $arProperties['Company.CEO_FULL'] = $OwnFIO;
        $arProperties['Company.CEO_FULL_G'] = $arOwnFFIO[1];
        $arProperties['Company.CEO_FULL.RP'] = $arOwnFFIO[1];
        $arProperties['Company.CEO_FULL_D'] = $arOwnFFIO[2];
        $arProperties['Company.CEO_FULL.DP'] = $arOwnFFIO[2];
        $arProperties['Company.CEO_SHORT'] = $CDocDes->getShortName($OwnFIO);
        $arProperties['Company.CEO_NAME'] = $CDocDes->getShortName($OwnFIO);

        $arProperties['Company.CEO_POST_G'] = $arCEO_POST[1];
        $arProperties['Company.CEO_POST.RP'] = $arCEO_POST[1];
        $arProperties['Company.CEO_POST_D'] = $arCEO_POST[2];
        $arProperties['Company.CEO_POST.DP'] = $arCEO_POST[2];

        $arProperties['Company.CEO_BASE_G'] = $arCEO_BASE[1];
        $arProperties['Company.CEO_BASE.RP'] = $arCEO_BASE[1];
        $arProperties['Company.CEO_BASE_D'] = $arCEO_BASE[2];
        $arProperties['Company.CEO_BASE.DP'] = $arCEO_BASE[2];
        //$OwnFIO =
        //AddMessage2Log(print_r($arOwnCompany, 1), "htmls.DocDesigner");
		foreach($arOwnCompany as $pid => $pval){
		  if($pid == 'CEO_SIGN' || $pid == 'ACC_SIGN' || $pid == 'STAMP'){
		    if($pval > 0){
		      $img = CFile::GetPath($pval);
              if(!strpos($img, '://')){
  			    $img = '..' . $img;
   			  }
              if($pid == 'CEO_SIGN'){
                $arProperties['Company.initiallingOfPages'] = $img;
              }
              $arProperties['Company.' . $pid] = '<img src="' . $img . '" />';
              $arProperties['Company.' . $pid.'_CF'] = $img;
		    }
		  }
          else{
			$arProperties['Company.' . $pid] = $pval;
          }
			//$arProperties[$pid] = $pval;
		}
        //AddMessage2Log(print_r($arProperties, true), __LINE__.'.'.__FUNCTION__);
        //if(empty($COMPANY['ADDRESS_LEGAL'])){
        $COMPANY['ADDRESS_LEGAL'] = '';
          $strReq = 'REG_ADDRESS_POSTAL_CODE,REG_ADDRESS_COUNTRY,REG_ADDRESS_PROVINCE,REG_ADDRESS_REGION,REG_ADDRESS_CITY,REG_ADDRESS,REG_ADDRESS_2';
          $arReq = explode(',', $strReq);
          foreach($arReq as $f){
            if(!empty($COMPANY[$f])){
              $COMPANY['ADDRESS_LEGAL'] .= $COMPANY[$f];
              if($f != 'REG_ADDRESS_2') $COMPANY['ADDRESS_LEGAL'] .= ', ';
            }
          }
        //}
        //if(empty($COMPANY['ADDRESS_LEGAL'])){
          $strReq = 'ADDRESS_POSTAL_CODE,ADDRESS_COUNTRY,ADDRESS_PROVINCE,ADDRESS_REGION,ADDRESS_CITY,ADDRESS,ADDRESS_2';
          $arReq = explode(',', $strReq);
          foreach($arReq as $f){
            if(!empty($COMPANY[$f])){
              $COMPANY['POST_ADDRESS'] .= $COMPANY[$f];
              if($f != 'ADDRESS_2') $COMPANY['POST_ADDRESS'] .= ', ';
            }
          }
        //}
        if($COMPANY['ID'] > 0)
            $arCompanyRequisite = self::getCrmRequisite($COMPANY['ID']);
        if($arContact['ID'] > 0)
            $arContactRequisite = self::getCrmRequisite($arContact['ID'], 3);
        //AddMessage2Log(print_r($arConttactRequisite, 1));
        if($uridadr = $arCompanyRequisite['uridadr']){
          $COMPANY['ADDRESS_LEGAL'] = '';
          if($uridadr['POSTAL_CODE']){
            $COMPANY['ADDRESS_LEGAL'] .= $uridadr['POSTAL_CODE'] . ', ';
          }

          if($uridadr['COUNTRY']){
            $COMPANY['ADDRESS_LEGAL'] .= $uridadr['COUNTRY'] . ', ';
          }

          if($uridadr['PROVINCE']){
            $COMPANY['ADDRESS_LEGAL'] .= $uridadr['PROVINCE'] . ', ';
          }

          if($uridadr['REGION']){
            $COMPANY['ADDRESS_LEGAL'] .= $uridadr['REGION'] . ', ';
          }

          $COMPANY['ADDRESS_LEGAL'] .= $uridadr['ADDRESS_1'];
        }

        if($postadr = $arCompanyRequisite['factadr']){
          $COMPANY['POST_ADDRESS'] = '';
          if($postadr['POSTAL_CODE']){
            $COMPANY['POST_ADDRESS'] .= $postadr['POSTAL_CODE'] . ', ';
          }

          if($postadr['COUNTRY']){
            $COMPANY['POST_ADDRESS'] .= $postadr['COUNTRY'] . ', ';
          }

          if($postadr['PROVINCE']){
            $COMPANY['POST_ADDRESS'] .= $postadr['PROVINCE'] . ', ';
          }

          if($postadr['REGION']){
            $COMPANY['POST_ADDRESS'] .= $postadr['REGION'] . ', ';
          }

          $COMPANY['POST_ADDRESS'] .= $postadr['ADDRESS_1'];
        }
        //RQ_DIRECTOR RQ_ACCOUNTANT RQ_CEO_NAME RQ_CEO_WORK_POS
        //RQ_BANK_ADDR
        if($arCompanyRequisite['RQ_COMPANY_NAME'])
		    $arProperties['CrmCompany.NAME'] = $arCompanyRequisite['RQ_COMPANY_NAME'];
        else
		    $arProperties['CrmCompany.NAME'] = $COMPANY_NAME;

        if($arCompanyRequisite['RQ_COMPANY_FULL_NAME'])
		    $arProperties['CrmCompany.FULL_NAME'] = $arCompanyRequisite['RQ_COMPANY_FULL_NAME'];
        else
		    $arProperties['CrmCompany.FULL_NAME'] = $COMPANY_NAME;

        $arProperties['CrmCompany.BANKING_DETAILS'] = str_replace(chr(13), '<br>', $COMPANY['BANKING_DETAILS']);

        if($arCompanyRequisite['RQ_INN'])
		    $arProperties['CrmCompany.INN'] = $arCompanyRequisite['RQ_INN'];
        else
		    $arProperties['CrmCompany.INN'] = $COMPANY[COption::GetOptionString($module_id, 'DOCDESIGNER_CRM_COMPANY_INN')];

        if($arCompanyRequisite['RQ_KPP'])
		    $arProperties['CrmCompany.KPP'] = $arCompanyRequisite['RQ_KPP'];
        else
		    $arProperties['CrmCompany.KPP'] = $COMPANY[COption::GetOptionString($module_id, 'DOCDESIGNER_CRM_COMPANY_KPP')];

        if($arCompanyRequisite['RQ_OGRN'])
            $arProperties['CrmCompany.OGRN'] = $arCompanyRequisite['RQ_OGRN'];
        else
            $arProperties['CrmCompany.OGRN'] = $COMPANY[COption::GetOptionString($module_id, 'DOCDESIGNER_CRM_COMPANY_OGRN')];

        if($arCompanyRequisite['bank']['RQ_BANK_NAME'])
            $arProperties['CrmCompany.BANK_NAME'] = $arCompanyRequisite['bank']['RQ_BANK_NAME'];
        else
            $arProperties['CrmCompany.BANK_NAME'] = $COMPANY[COption::GetOptionString($module_id, 'DOCDESIGNER_CRM_COMPANY_BANK_NAME')];

        if($arCompanyRequisite['bank']['RQ_BIK'])
		    $arProperties['CrmCompany.BANK_BIK'] = $arCompanyRequisite['bank']['RQ_BIK'];
        else
		    $arProperties['CrmCompany.BANK_BIK'] = $COMPANY[COption::GetOptionString($module_id, 'DOCDESIGNER_CRM_COMPANY_BANK_BIK')];

        if($arCompanyRequisite['bank']['RQ_ACC_NUM'])
            $arProperties['CrmCompany.BANK_RS'] = $arCompanyRequisite['bank']['RQ_ACC_NUM'];
        else
            $arProperties['CrmCompany.BANK_RS'] = $COMPANY[COption::GetOptionString($module_id, 'DOCDESIGNER_CRM_COMPANY_RS')];

        if($arCompanyRequisite['bank']['RQ_COR_ACC_NUM'])
		    $arProperties['CrmCompany.BANK_KS'] = $arCompanyRequisite['bank']['RQ_COR_ACC_NUM'];
        else
		    $arProperties['CrmCompany.BANK_KS'] = $COMPANY[COption::GetOptionString($module_id, 'DOCDESIGNER_CRM_COMPANY_BANK_KS')];

        if($arCompanyRequisite['bank']['RQ_BANK_ADDR'])
		    $arProperties['CrmCompany.BANK_CITY'] = $arCompanyRequisite['bank']['RQ_BANK_ADDR'];
        else
		    $arProperties['CrmCompany.BANK_CITY'] = $COMPANY[COption::GetOptionString($module_id, 'DOCDESIGNER_CRM_COMPANY_BANK_CITY')];

		$arProperties['CrmCompany.ADDRESS_LEGAL'] = $COMPANY['ADDRESS_LEGAL'];
		$arProperties['CrmCompany.POST_ADDRESS'] = $COMPANY['POST_ADDRESS'];
		$arProperties['CrmCompany.ADDRESS'] = $COMPANY['ADDRESS'];
		$arProperties['CrmCompany.PHONE'] = $COMPANY[COption::GetOptionString($module_id, 'DOCDESIGNER_CRM_COMPANY_PHONE')];
        $strReq = 'ADDRESS_2,ADDRESS_CITY,ADDRESS_REGION,ADDRESS_PROVINCE,ADDRESS_POSTAL_CODE,ADDRESS_COUNTRY,REG_ADDRESS_2,REG_ADDRESS_CITY,REG_ADDRESS_REGION,REG_ADDRESS_PROVINCE,REG_ADDRESS_POSTAL_CODE,REG_ADDRESS_COUNTRY,REG_ADDRESS';
        $arReq = explode(',', $strReq);
        //AddMessage2Log(print_r($arReq, true), "htmls.DocDesigner");
        //AddMessage2Log(print_r($COMPANY, true), "htmls.DocDesigner");
        foreach($arReq as $f){
          $arProperties['CrmCompany.'.$f] = $COMPANY[$f];
        }

        if($factadr = $arContactRequisite['factadr']){
          $arProperties['CONTACT.ADDRESS'] = $factadr['ADDRESS_1'];
          $arProperties['CONTACT.ADDRESS_2'] = $factadr['ADDRESS_2'];
          $arProperties['CONTACT.ADDRESS_CITY'] = $factadr['CITY'];
          $arProperties['CONTACT.ADDRESS_REGION'] = $factadr['REGION'];
          $arProperties['CONTACT.ADDRESS_PROVINCE'] = $factadr['PROVINCE'];
          $arProperties['CONTACT.ADDRESS_POSTAL_CODE'] = $factadr['POSTAL_CODE'];
          $arProperties['CONTACT.ADDRESS_COUNTRY'] = $factadr['COUNTRY'];
        }
        $arProperties['CONTACT.RQ_IDENT_DOC'] = $arContactRequisite['RQ_IDENT_DOC'];
        $arProperties['CONTACT.RQ_IDENT_DOC_SER'] = $arContactRequisite['RQ_IDENT_DOC_SER'];
        $arProperties['CONTACT.RQ_IDENT_DOC_NUM'] = $arContactRequisite['RQ_IDENT_DOC_NUM'];
        $arProperties['CONTACT.RQ_IDENT_DOC_DATE'] = $arContactRequisite['RQ_IDENT_DOC_DATE'];
        $arProperties['CONTACT.RQ_IDENT_DOC_ISSUED_BY'] = $arContactRequisite['RQ_IDENT_DOC_ISSUED_BY'];
        $arProperties['CONTACT.RQ_IDENT_DOC_DEP_CODE'] = $arContactRequisite['RQ_IDENT_DOC_DEP_CODE'];

		$snum = "";
		if($Numerator > 0){
		  if($this->docDesignerSaved){
		    $snum = $this->docDesignerNumber;
		    $arNum = array();
		    $arNum['DEAL'] = $DEAL_NAME;
		    $arNum['COMPANY'] = $COMPANY_NAME;
			$arNum['CONTACT'] = $CONTACT_NAME;
			$arNum['PREFIX'] = $arProperties['DocDesignerNumberPrefix'];
			$arNum['COMPANY_ID'] = $arProperties['DocDesignerCompany'];
            $arNum["fName"] = $snum;
			$arNum["DocNumber"] = $snum;
			$arNum["NAME"] = $snum;
		  }
			$arNum = $CDocDes->GetNumeratorInfo($Numerator, $arProperties['DocDesignerNumberPrefix'], $arProperties['DocDesignerCompany'], $rootActivity->DocDesignerCompanyContract);
			$arNum['DEAL'] = $DEAL_NAME;
			$arNum['COMPANY'] = $COMPANY_NAME;
			$arNum['CONTACT'] = $CONTACT_NAME;
			$arNum['PREFIX'] = $arProperties['DocDesignerNumberPrefix'];
			$arNum['COMPANY_ID'] = $arProperties['DocDesignerCompany'];
			$snum = str_replace("{PREFIX}", $arProperties['DocDesignerNumberPrefix'], $arNum['TEMPLATE']);
			$snum = str_replace("{YEAR}", $arNum['YEAR'], $snum);
			$snum = str_replace("{NUMBER}", $arNum['SNUMBER'], $snum);
			$snum = str_replace("{MONTH}", date("m"), $snum);
			$NAME = str_replace("/", "-", $snum);
			$NAME = str_replace("\\", "-", $NAME);
			$arNum["fName"] = $NAME;
			$arNum["DocNumber"] = $snum;
			$arNum["NAME"] = $snum;
		}
		else{
			$arNum['DEAL'] = $DEAL_NAME;
			$arNum['COMPANY'] = $COMPANY_NAME;
			$arNum['CONTACT'] = $CONTACT_NAME;
			$arNum['PREFIX'] = $arProperties['DocDesignerNumberPrefix'];
			$arNum['COMPANY_ID'] = $arProperties['DocDesignerCompany'];
			$arNum["NAME"] = "";
		}

		$cnt = 0;
		//$Total = 0;
		if($PaymentStage = intval($rootActivity->GetVariable('PAYMENT_STAGE'))){
			$n = 0;
			if($PROD_SUM = $arProperties['DOGOVOR_SUM_'.$PaymentStage]){
				$cnt++;
				$InvSubj = $rootActivity->GetVariable('INVOICE_SUBJECT');
				$arProperties['ProductID']['n'.$n] = $cnt.';'.$InvSubj.';1;'.GetMessage('DOCDESIGNER_INVOICE_UNIT').';'.$PROD_SUM;
				$Total = $Total + $PROD_SUM;
				$n++;
				$arPropertiesTypes['ProductID']['Type'] = 'S';
				$PaymentStage++;
				$rootActivity->SetVariable('PAYMENT_STAGE', $PaymentStage);
				$rootActivity->SetVariable('PAYMENT_STAGE_SUM', $arProperties['DOGOVOR_SUM_'.$PaymentStage]);
				$invProd[] = array('str', $InvSubj, 1, $PROD_SUM);
				$arProdTable[$cnt] = array('PRODUCT_NAME' => $InvSubj, 'QUANTITY' => 1, 'PRICE' => $PROD_SUM);
			}
			$arProperties['NumRows'] = 1;
		    $arProperties['arCrmProducts'] = $arProdTable;
		}
		else{
			if($arProperties['ProductID']){
				if(empty($arProperties['ProductID']['n0'])){
					$arProduct = $CDocDes->GetProducts($DEAL_ID, true);
					$cnt = 1;
					foreach($arProduct as $k => $sProd){
						$sProd = str_replace('DD_UNIT', GetMessage('DOCDESIGNER_INVOICE_UNIT'), $sProd);
						$arProd = explode(';', $sProd);
						$del = array_pop($arProd);//unset($arProd[count($arProd) - 1]);
						$arProduct[$k] = implode(';', $arProd);
						$Total = $Total + ($arProd[2] * $arProd[4]);
						$tmp = 'crm;'.$sProd;
						$arTmp = explode(';', $tmp);
						$invProd[] = array($arTmp[0],$arTmp[2],$arTmp[3],$arTmp[5],$arTmp[6]);
						$arProdTable[$cnt] = array('PRODUCT_NAME' => $arTmp[2], 'QUANTITY' => $arTmp[3], 'PRICE' => $arTmp[5]);
						$cnt++;
					}
					$arProperties['CrmProducts'] = $arProduct;
					$arPropertiesTypes['CrmProducts']['Type'] = 'string';
					$arPropertiesTypes['CrmProducts']['Multiple'] = '1';
					$arProperties['arCrmProducts'] = $arProdTable;
				}
				else{
					$OfferPropID = COption::GetOptionString($module_id, "DOCDESIGNER_OFFER_PRICE");
					$cnt = 1;
			        foreach($arProperties['ProductID'] as $n => $sProd){
			        	$arProd = explode(';', $sProd);
			        	if(count($arProd) == 1){//iblock element id
				        	$arSelect = Array("NAME", 'PROPERTY_'.$OfferPropID);
							$arFilter = Array("ID"=>IntVal($sProd));
							$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
							$cnt++;
							while($ob = $res->GetNextElement()){
								$arFields = $ob->GetFields();
								$PROD_SUM = $arFields["PROPERTY_".$OfferPropID."_VALUE"];
								$arProperties['ProductID'][$n] = $cnt.';'.$arFields['NAME'].';1;'.GetMessage('DOCDESIGNER_INVOICE_UNIT').';'.$PROD_SUM;
								$Total = $Total + $PROD_SUM;
								$invProd[] = array('ib', $arFields['NAME'], 1, $PROD_SUM,$sProd);
								$arProdTable[$cnt] = array('PRODUCT_NAME' =>$arFields['NAME'], 'QUANTITY' => 1, 'PRICE' => $PROD_SUM);
							}
						}
						elseif(count($arProd) == 2){//name;sum
							$cnt++;
							$arProperties['ProductID'][$n] = $cnt.';'.$arProd[0].';1;'.GetMessage('DOCDESIGNER_INVOICE_UNIT').';'.$arProd[1];
							$Total = $Total + $arProd[1];
							$invProd[] = array('str', $arProd[0], 1, $arProd[1]);
							$arProdTable[$cnt] = array('PRODUCT_NAME' => $arProd[0], 'QUANTITY' => 1, 'PRICE' => $arProd[1]);
						}
						elseif(count($arProd) == 3){//name;kol;sum
							$cnt++;
							$arProperties['ProductID'][$n] = $cnt.';'.$arProd[0].';'.$arProd[1].';'.GetMessage('DOCDESIGNER_INVOICE_UNIT').';'.$arProd[2];
							$Total = $Total + ($arProd[1] * $arProd[2]);
							$invProd[] = array('str', $arProd[0], $arProd[1], $arProd[2]);
							$arProdTable[$cnt] = array('PRODUCT_NAME' => $arProd[0], 'QUANTITY' => $arProd[1], 'PRICE' => $arProd[2]);
						}
						$cnt++;
			        }
		        }
		        $arProperties['NumRows'] = count($arProperties['ProductID']);
		        $arProperties['arCrmProducts'] = $arProdTable;
			}
			else{
				$arProduct = $CDocDes->GetProducts($ProdOwner, true, $Type);
				foreach($arProduct as $k => $sProd){
					$sProd = str_replace('DD_UNIT', GetMessage('DOCDESIGNER_INVOICE_UNIT'), $sProd);
					$arProd = explode(';', $sProd);
					$del = array_pop($arProd);
					$arProduct[$k] = implode(';', $arProd);
					$Total = $Total + ($arProd[2] * $arProd[4]);
					$tmp = 'crm;'.$sProd;
					$arTmp = explode(';', $tmp);
					$invProd[] = array($arTmp[0],$arTmp[2],$arTmp[3],$arTmp[5],$arTmp[6]);
				}
				$arProperties['CrmProducts'] = $arProduct;
				$arPropertiesTypes['CrmProducts']['Type'] = 'string';
				$arPropertiesTypes['CrmProducts']['Multiple'] = '1';
				$arProperties['NumRows'] = count($arProduct);
				$arProperties['arCrmProducts'] = $CDocDes->GetCrmProducts($ProdOwner, $Type);
			}
		}
		$arProperties['BILL_SUM'] = number_format($Total, $this->DigitCol, ',', ' ');
        //AddMessage2Log(print_r($num2str->namerub, true), "htmls.DocDesigner");
		$arProperties['BILL_SUM_SPELLING'] = $num2str->num2str($Total);
		$arProperties['BILL_DATE'] = FormatDate("d F Y", time());//today
		$arProperties['DocDate'] = date("d.m.Y", time());//today
		$arProperties['DocDateSpelling'] = FormatDate("d F Y", time());//today
		$arProperties['DocNumber'] = $arNum['DocNumber'];
		$arProperties['BILL_NUM'] = $arNum['DocNumber'];
		$arProperties['arNum'] = $arNum;
		if($Total == 0){
			$Total = $arProperties['DOGOVOR_SUM'];
		}
		$arProperties['TotalSum'] = $Total;
		$arProperties['DOGOVOR_SUM_SPELLING'] = $num2str->num2str($Total);
        //AddMessage2Log(print_r($arProperties, true), "htmls.DocDesigner");
        return $arProperties;
    }

    function setParameters($par){
      self::$bpParameters = $par;
    }

    function getVariables($workflow_id){
      global $DB;

      if($bpid = $_REQUEST['workflow_template_id']){
        $q = 'SELECT * from b_bp_workflow_template WHERE ID=' . $bpid . '';
      }
      elseif($url = $_REQUEST['back_url'] && !$_REQUEST['workflow_id']){
        $a = explode('/', $url);
        if($a[count($a)-2] * 1 > 0){
          $_REQUEST['workflow_template_id'] = $a[count($a)-2];
          $bpid = $_REQUEST['workflow_template_id'];
          $q = 'SELECT * from b_bp_workflow_template WHERE DOCUMENT_TYPE="type_' . $bpid . '"';
        }
      }
      if($bpid = $_REQUEST['workflow_template_id']){
        $res = $DB->Query($q, false, $err_mess.__LINE__);
        $tmp = $res->Fetch();
        $pars = unserialize(gzuncompress($tmp['PARAMETERS']));
        $var = gzuncompress($tmp['VARIABLES']);
        return array('vars' => unserialize($var), 'pars' =>$pars);
      }
      elseif($workflow_id){ //$wfid = $_REQUEST['workflow_id']
        $res = $DB->Query('SELECT * from b_bp_workflow_state WHERE ID="' . $workflow_id . '"', false, $err_mess.__LINE__);
        $tmp = $res->Fetch();
        $bpid = $tmp['WORKFLOW_TEMPLATE_ID'];
        $res = $DB->Query('SELECT * from b_bp_workflow_template WHERE id=' . $bpid, false, $err_mess.__LINE__);
        $tmp = $res->Fetch();
        $par = unserialize(gzuncompress($tmp['PARAMETERS']));
        $var = gzuncompress($tmp['VARIABLES']);
        return array('vars' => unserialize($var), 'pars' =>$pars);
      }
      else
        return array();
    }

	function CreatePdfDocx($rootActivity, &$arProperties, $arPropertiesTypes, $workflowId, $activity){//$TemplateID = 0, $Numerator, $CreateOriginal){
		global $DB, $MESS;
        $TemplateID = $activity->PDFTemplate;
        $Numerator = $activity->PDFNumerator;
        $CreateOriginal = $activity->CreateCopy;
        $FontSize = $activity->FontSize;
        $MIME = $activity->MIME;
        if($FontSize > 0){

        }
        else{
          $FontSize = 10;
        }

        //AddMessage2Log(print_r($activity, 1));
        if($saved = $rootActivity->GetVariable("docDesignerSaved")){
          if($save == 'Y'){
            $this->docDesignerSaved = true;
            $this->docDesignerNumber = $rootActivity->GetVariable("docDesignerNumber");
            $this->docDesignerDate = $rootActivity->GetVariable("docDesignerDate");
          }
        }
        else{
          $this->docDesignerSaved = false;
        }
        $arCrmId = array('deal'=>0, 'company'=>0, 'lead'=>0, 'contact'=>0, 'invoice'=>0, 'offer'=>0);
		if(empty($TemplateID)) $TemplateID = 0;
		CModule::IncludeModule("bizproc");
		CModule::IncludeModule("iblock");
        $arBizProc = array('arPropperties' => $arProperties, 'arPropertiesTypes' => $arPropertiesTypes,
                    'workflowId' => $workflowId, 'TemplateID' => $TemplateID);//, 'rootActivity' => $rootActivity);

		$CDocDes = new CDocDesignerProcessing();
		$IsCatalog = IsModuleInstalled("catalog");
		if($IsCatalog)
			CModule::IncludeModule("catalog");
       /*
       if(!$arProperties['DocDesignerCompany']){
         $arProperties['DocDesignerCompany'] = $rootActivity->GetVariable('DocDesignerCompany');
       }
       */
       if($arProperties['DocumentSection'] > 0){
			$arProperties['DocDesignerDocumentSection'] = $arProperties['DocumentSection'];
		}
		if($DocumentSection = $rootActivity->GetVariable('DocDesignerDocumentSection')){
			$arProperties['DocDesignerDocumentSection'] = $DocumentSection;
		}
        //AddMessage2Log(print_r($arProperties, true), "htmls.DocDesigner");


        $UID = date("Ymd_His");
		$DocumentType = $rootActivity->GetDocumentType();
		$DocumentId = $rootActivity->GetDocumentId();
        //AddMessage2Log(print_r($DocumentType, 1));
        //AddMessage2Log(print_r($DocumentId, 1));
        if($DocumentType[0] == 'lists'){
          CModule::IncludeModule('iblock');
          $arIBlockElement = GetIBlockElement($DocumentId[2]);
          //AddMessage2Log(print_r($arIBlockElement, 1));
          foreach($arIBlockElement as $name => $val){
            if($name == 'PROPERTIES'){
              foreach($val as $key => $_val){
                $arProperties[$key] = $_val['VALUE'];
                $arProperties[$_val['CODE']] = $_val['VALUE'];
              }
            }
            else{
                $arProperties[$name] = $val;
            }
          }
        }
        //AddMessage2Log(print_r($arProperties, 1));
        $arRootActivity['DocumentType'] = $DocumentType;
        $arRootActivity['DocumentId'] = $DocumentId;
        $arRootActivity['arVariables'] = $rootActivity->arVariables;

		$module_id = "htmls.docdesigner";
		if($DocumentType[0] == 'bizproc'){
			$DocType = substr($DocumentType[2], strlen("type_"));
			$DocId = $DocumentId[2];
			$elementId = $DocId;
		}
		elseif($DocumentType[0] == 'crm'){
			$arState = CBPStateService::GetWorkflowState($workflowId);
			$DocType = $DocumentType[2].'_'.$arState['TEMPLATE_ID'];
			$DocId = $UID;
			$elementId = 0;
			if($DocumentType[2] == 'DEAL' || $DocumentType[2] == 'LEAD'){
				if($DocumentType[2] == 'DEAL')
					$DEAL_ID = substr($DocumentId[2], strlen("DEAL_"));
				else
					$LEAD_ID = substr($DocumentId[2], strlen("LEAD_"));
				if($DEAL_ID > 0)
					$arProduct = $CDocDes->GetProducts($DEAL_ID);
				if($LEAD_ID > 0)
					$arProduct = $CDocDes->GetProducts($LEAD_ID, false, 'L');

				if($arProduct){
					$arProperties['CrmProducts'] = $arProduct;
					$arPropertiesTypes['CrmProducts']['Type'] = 'string';
					$arPropertiesTypes['CrmProducts']['Multiple'] = '1';
					if($DEAL_ID > 0)
						$arProperties['arCrmProducts'] = $CDocDes->GetCrmProducts($DEAL_ID);
					if($LEAD_ID > 0)
						$arProperties['arCrmProducts'] = $CDocDes->GetCrmProducts($LEAD_ID, 'L');
				}
			}
		}
        //AddMessage2Log(print_r($arProperties, true), "htmls.DocDesigner");
		$CContract = new CDocDesignerContracts();
        $num2str = new CNum2Str;
		if($DocumentType[0] == 'crm'){
			//$CContract = new CDocDesignerContracts();

			$Total = 0;
			$DEAL_ID = 0;
			$COMPANY_ID = 0;
			$invProd = array();
			$arProperties = self::SetCRMParameters($rootActivity, $arProperties, $arPropertiesTypes, $DocumentId, $CDocDes, $CContract, $num2str, $Total, $DEAL_ID, $COMPANY_ID, $invProd, $Numerator, $arCrmId);
		}
        else{
            $snum = "";
    		if($Numerator > 0){
    			$arNum = $CDocDes->GetNumeratorInfo($Numerator, $arProperties['DocDesignerNumberPrefix'], $arProperties['DocDesignerCompany'], 0);
    			$arNum['COMPANY'] = $COMPANY_NAME;
    			$arNum['PREFIX'] = $arProperties['DocDesignerNumberPrefix'];
    			$arNum['COMPANY_ID'] = $arProperties['DocDesignerCompany'];
    			$snum = str_replace("{PREFIX}", $arProperties['DocDesignerNumberPrefix'], $arNum['TEMPLATE']);
    			$snum = str_replace("{YEAR}", $arNum['YEAR'], $snum);
    			$snum = str_replace("{NUMBER}", $arNum['SNUMBER'], $snum);
    			$snum = str_replace("{MONTH}", date("m"), $snum);
    			$NAME = str_replace("/", "-", $snum);
    			$NAME = str_replace("\\", "-", $NAME);
    			$arNum["fName"] = $NAME;
    			$arNum["DocNumber"] = $snum;
    			$arNum["NAME"] = $snum;
    		}
    		else{
    			$arNum['COMPANY'] = $COMPANY_NAME;
    			$arNum['PREFIX'] = $arProperties['DocDesignerNumberPrefix'];
    			$arNum['COMPANY_ID'] = $arProperties['DocDesignerCompany'];
    			$arNum["NAME"] = "";
    		}
            $arProperties['DocNumber'] = $arNum['DocNumber'];
		    $arProperties['BILL_NUM'] = $arNum['DocNumber'];
            $arProperties['arNum'] = $arNum;
        }

		if($rootActivity->GetVariable('DocDesignerOfferRowsVar')){
			$arProperties['DocDesignerOfferRows'] = $rootActivity->GetVariable('DocDesignerOfferRowsVar');
		}
        $_arVariables = self::getVariables($rootActivity->GetWorkflowInstanceId());
        if($_arVariables['vars']){
            $CDocDes->bpParameters = $_arVariables['vars'];
            //AddMessage2Log(print_r($CDocDes->bpParameters,1),2);
          $arVariables = $_arVariables['vars'];
          //AddMessage2Log(print_r($CDocDes->bpParameters,1),__LINE__);
          foreach($arVariables as $vname => $vVal){
            if($vVal['Type'] == 'UF:crm'){
              if($vVal['Options']['COMPANY'] == 'Y'){
                $COMPANY_ID = $rootActivity->GetVariable($vname);
                if($COMPANY_ID > 0){
                  if($this->isCRM){
                    $res = CCrmCompany::GetListEx(array(),array('ID' => $COMPANY_ID), false, false, array('*', 'UF_*', 'PHONE', 'EMAIL'));
            		while($ob = $res->GetNext()){
            		  $COMPANY_NAME = $ob['TITLE'];
            		  $COMPANY = $ob;
            		}
                    $arProperties['var' . $vname] = $COMPANY_NAME;
                    $arProperties['var' . $vname . '.NAME'] = $COMPANY_NAME;
                    $arProperties['var' . $vname . '.TITLE'] = $COMPANY_NAME;


                    $arCompanyRequisite = self::getCrmRequisite($COMPANY_ID);
                    if($uridadr = $arCompanyRequisite['uridadr']){
                      $COMPANY['ADDRESS_LEGAL'] = '';
                      if($uridadr['POSTAL_CODE']){
                        $COMPANY['ADDRESS_LEGAL'] .= $uridadr['POSTAL_CODE'] . ', ';
                      }

                      if($uridadr['COUNTRY']){
                        $COMPANY['ADDRESS_LEGAL'] .= $uridadr['COUNTRY'] . ', ';
                      }

                      if($uridadr['PROVINCE']){
                        $COMPANY['ADDRESS_LEGAL'] .= $uridadr['PROVINCE'] . ', ';
                      }

                      if($uridadr['REGION']){
                        $COMPANY['ADDRESS_LEGAL'] .= $uridadr['REGION'] . ', ';
                      }

                      $COMPANY['ADDRESS_LEGAL'] .= $uridadr['ADDRESS_1'];
                    }

                    if($postadr = $arCompanyRequisite['factadr']){
                      $COMPANY['POST_ADDRESS'] = '';
                      if($postadr['POSTAL_CODE']){
                        $COMPANY['POST_ADDRESS'] .= $postadr['POSTAL_CODE'] . ', ';
                      }

                      if($postadr['COUNTRY']){
                        $COMPANY['POST_ADDRESS'] .= $postadr['COUNTRY'] . ', ';
                      }

                      if($postadr['PROVINCE']){
                        $COMPANY['POST_ADDRESS'] .= $postadr['PROVINCE'] . ', ';
                      }

                      if($postadr['REGION']){
                        $COMPANY['POST_ADDRESS'] .= $postadr['REGION'] . ', ';
                      }

                      $COMPANY['POST_ADDRESS'] .= $postadr['ADDRESS_1'];
                    }

                    foreach($COMPANY as $fid => $fval){
                      $arProperties['var' . $vname . '.' . $fid] = $fval;
                    }
                    //RQ_DIRECTOR RQ_ACCOUNTANT RQ_CEO_NAME RQ_CEO_WORK_POS
                    //RQ_BANK_ADDR
                    if($arCompanyRequisite['RQ_COMPANY_NAME'])
            		    $arProperties['var' . $vname . '.NAME'] = $arCompanyRequisite['RQ_COMPANY_NAME'];
                    else
            		    $arProperties['var' . $vname . '.NAME'] = $COMPANY_NAME;

                    if($arCompanyRequisite['RQ_COMPANY_FULL_NAME'])
            		    $arProperties['var' . $vname . '.FULL_NAME'] = $arCompanyRequisite['RQ_COMPANY_FULL_NAME'];
                    else
            		    $arProperties['var' . $vname . '.FULL_NAME'] = $COMPANY_NAME;

                    $arProperties['var' . $vname . '.BANKING_DETAILS'] = str_replace(chr(13), '<br>', $COMPANY['BANKING_DETAILS']);

                    if($arCompanyRequisite['RQ_INN'])
            		    $arProperties['var' . $vname . '.INN'] = $arCompanyRequisite['RQ_INN'];
                    else
            		    $arProperties['var' . $vname . '.INN'] = $COMPANY[COption::GetOptionString($module_id, 'DOCDESIGNER_CRM_COMPANY_INN')];

                    if($arCompanyRequisite['RQ_KPP'])
            		    $arProperties['var' . $vname . '.KPP'] = $arCompanyRequisite['RQ_KPP'];
                    else
            		    $arProperties['var' . $vname . '.KPP'] = $COMPANY[COption::GetOptionString($module_id, 'DOCDESIGNER_CRM_COMPANY_KPP')];

                    if($arCompanyRequisite['RQ_OGRN'])
                        $arProperties['var' . $vname . '.OGRN'] = $arCompanyRequisite['RQ_OGRN'];
                    else
                        $arProperties['var' . $vname . '.OGRN'] = $COMPANY[COption::GetOptionString($module_id, 'DOCDESIGNER_CRM_COMPANY_OGRN')];

                    if($arCompanyRequisite['bank']['RQ_BANK_NAME'])
                        $arProperties['var' . $vname . '.BANK_NAME'] = $arCompanyRequisite['bank']['RQ_BANK_NAME'];
                    else
                        $arProperties['var' . $vname . '.BANK_NAME'] = $COMPANY[COption::GetOptionString($module_id, 'DOCDESIGNER_CRM_COMPANY_BANK_NAME')];

                    if($arCompanyRequisite['bank']['RQ_BIK'])
            		    $arProperties['var' . $vname . '.BANK_BIK'] = $arCompanyRequisite['bank']['RQ_BIK'];
                    else
            		    $arProperties['var' . $vname . '.BANK_BIK'] = $COMPANY[COption::GetOptionString($module_id, 'DOCDESIGNER_CRM_COMPANY_BANK_BIK')];

                    if($arCompanyRequisite['bank']['RQ_ACC_NUM'])
                        $arProperties['var' . $vname . '.BANK_RS'] = $arCompanyRequisite['bank']['RQ_ACC_NUM'];
                    else
                        $arProperties['var' . $vname . '.BANK_RS'] = $COMPANY[COption::GetOptionString($module_id, 'DOCDESIGNER_CRM_COMPANY_RS')];

                    if($arCompanyRequisite['bank']['RQ_COR_ACC_NUM'])
            		    $arProperties['var' . $vname . '.BANK_KS'] = $arCompanyRequisite['bank']['RQ_COR_ACC_NUM'];
                    else
            		    $arProperties['var' . $vname . '.BANK_KS'] = $COMPANY[COption::GetOptionString($module_id, 'DOCDESIGNER_CRM_COMPANY_BANK_KS')];

                    if($arCompanyRequisite['bank']['RQ_BANK_ADDR'])
            		    $arProperties['var' . $vname . '.BANK_CITY'] = $arCompanyRequisite['bank']['RQ_BANK_ADDR'];
                    else
            		    $arProperties['var' . $vname . '.BANK_CITY'] = $COMPANY[COption::GetOptionString($module_id, 'DOCDESIGNER_CRM_COMPANY_BANK_CITY')];

            		$arProperties['var' . $vname . '.ADDRESS_LEGAL'] = $COMPANY['ADDRESS_LEGAL'];
            		$arProperties['var' . $vname . '.POST_ADDRESS'] = $COMPANY['POST_ADDRESS'];
            		$arProperties['var' . $vname . '.ADDRESS'] = $COMPANY['ADDRESS'];
            		$arProperties['var' . $vname . '.PHONE'] = $COMPANY[COption::GetOptionString($module_id, 'DOCDESIGNER_CRM_COMPANY_PHONE')];
                    $strReq = 'ADDRESS_2,ADDRESS_CITY,ADDRESS_REGION,ADDRESS_PROVINCE,ADDRESS_POSTAL_CODE,ADDRESS_COUNTRY,REG_ADDRESS_2,REG_ADDRESS_CITY,REG_ADDRESS_REGION,REG_ADDRESS_PROVINCE,REG_ADDRESS_POSTAL_CODE,REG_ADDRESS_COUNTRY,REG_ADDRESS';
                    $arReq = explode(',', $strReq);
                    foreach($arReq as $f){
                      $arProperties['var' . $vname . '.'.$f] = $COMPANY[$f];
                    }
                  }
                }
                else{
                    $arProperties['var' . $vname] = '';
                    $arProperties['var' . $vname . '.NAME'] = '';
                    $arProperties['var' . $vname . '.TITLE'] = '';
                    $arProperties['var' . $vname . '.NAME'] = $arCompanyRequisite['RQ_COMPANY_NAME'];
                    $arProperties['var' . $vname . '.FULL_NAME'] = $arCompanyRequisite['RQ_COMPANY_FULL_NAME'];
                    $arProperties['var' . $vname . '.BANKING_DETAILS'] = '';
                    $arProperties['var' . $vname . '.INN'] = '';
                    $arProperties['var' . $vname . '.KPP'] = '';
                    $arProperties['var' . $vname . '.OGRN'] = '';
                    $arProperties['var' . $vname . '.BANK_NAME'] = '';
                    $arProperties['var' . $vname . '.BANK_BIK'] = '';
                    $arProperties['var' . $vname . '.BANK_RS'] = '';
                    $arProperties['var' . $vname . '.BANK_KS'] = '';
                    $arProperties['var' . $vname . '.BANK_CITY'] = '';
                    $arProperties['var' . $vname . '.ADDRESS_LEGAL'] = '';
            		$arProperties['var' . $vname . '.POST_ADDRESS'] = '';
            		$arProperties['var' . $vname . '.ADDRESS'] = '';
            		$arProperties['var' . $vname . '.PHONE'] = '';
                    $strReq = 'ADDRESS_2,ADDRESS_CITY,ADDRESS_REGION,ADDRESS_PROVINCE,ADDRESS_POSTAL_CODE,ADDRESS_COUNTRY,REG_ADDRESS_2,REG_ADDRESS_CITY,REG_ADDRESS_REGION,REG_ADDRESS_PROVINCE,REG_ADDRESS_POSTAL_CODE,REG_ADDRESS_COUNTRY,REG_ADDRESS';
                    $arReq = explode(',', $strReq);
                    foreach($arReq as $f){
                      $arProperties['var' . $vname . '.'.$f] = '';
                    }
                }
              }
              elseif($vVal['Options']['CONTACT'] == 'Y'){
                $CONTACT_ID = $rootActivity->GetVariable($vname);
                if($CONTACT_ID > 0){
                  if($this->isCRM){
        			$res = CCrmContact::GetListEx(array(), Array('ID' => $CONTACT_ID), false, false, array('*', 'UF_*', 'PHONE', 'EMAIL'));//, array());
        			while($ob = $res->GetNext()){
        			  $arCrmId['contact'] = $ob['ID'];
        				$arContact = $ob;
                        $arProperties['var' . $vname.'.FULL_NAME_SHORT'] = $ob['LAST_NAME'];
                        $arProperties['var' . $vname.'.FULL_FIO'] = $ob['LAST_NAME'];

                        if(strtoupper(LANG_CHARSET) == 'WINDOWS-1251'){
                          $arProperties['var' . $vname.'.FULL_NAME_SHORT'] .= ' ' . $ob['NAME'][0] . '.';
                          $arProperties['var' . $vname.'.FULL_FIO'] .= ' ' . $ob['NAME'];
                          if($sn = $ob['SECOND_NAME']){
                            $arProperties['var' . $vname.'.FULL_NAME_SHORT'] .= ' ' . $sn[0] . '.';
                            $arProperties['var' . $vname.'.FULL_FIO'] .= ' ' . $sn;
                          }
                        }
                        else{
                          $n = iconv("UTF-8", "cp1251", $ob['NAME']);
                          $arProperties['var' . $vname.'.FULL_NAME_SHORT'] .= ' ' . iconv("cp1251", "UTF-8", $n[0]) . '.';
                          $arProperties['var' . $vname.'.FULL_FIO'] .= ' ' . $ob['NAME'];
                          if($sn = $ob['SECOND_NAME']){
                            $sn = iconv("UTF-8", "cp1251", $sn);
                            $arProperties['var' . $vname.'.FULL_NAME_SHORT'] .= ' ' . iconv("cp1251", "UTF-8", $sn[0]) . '.';
                            $arProperties['var' . $vname.'.FULL_FIO'] .= ' ' . $ob['SECOND_NAME'];
                          }
                        }
                        $arProperties['var' . $vname] = $arContact['FULL_FIO'];
                        $arProperties['var' . $vname.'.FULL_FIO.RP'] = $arContact['FULL_FIO'];
                        $arProperties['var' . $vname.'.FULL_FIO_G'] = $arContact['FULL_FIO'];
                        $arProperties['var' . $vname.'.FULL_NAME_SHORT.RP'] = $arContact['FULL_NAME_SHORT'];
                        $arProperties['var' . $vname.'.FULL_NAME_SHORT_G'] = $arContact['FULL_NAME_SHORT'];
                        $arProperties['var' . $vname.'.POST_G'] = $arContact['POST'];

                        foreach($ob as $fid => $fvalue){
                          $arProperties['var' . $vname.'.'.$fid] = $fvalue;
                        }
                        //$CONTACT_NAME = $ob['LAST_NAME'] . ' ' . $ob['NAME'] . ' ' . $ob['SECOND_NAME'];
                        $arContactRequisite = self::getCrmRequisite($CONTACT_ID, 3);
                        if($factadr = $arContactRequisite['factadr']){
                          $arProperties['var' . $vname.'.ADDRESS'] = $factadr['ADDRESS_1'];
                          $arProperties['var' . $vname.'.ADDRESS_2'] = $factadr['ADDRESS_2'];
                          $arProperties['var' . $vname.'.ADDRESS_CITY'] = $factadr['CITY'];
                          $arProperties['var' . $vname.'.ADDRESS_REGION'] = $factadr['REGION'];
                          $arProperties['var' . $vname.'.ADDRESS_PROVINCE'] = $factadr['PROVINCE'];
                          $arProperties['var' . $vname.'.ADDRESS_POSTAL_CODE'] = $factadr['POSTAL_CODE'];
                          $arProperties['var' . $vname.'.ADDRESS_COUNTRY'] = $factadr['COUNTRY'];
                        }
                        $arProperties['var' . $vname.'.RQ_IDENT_DOC'] = $arContactRequisite['RQ_IDENT_DOC'];
                        $arProperties['var' . $vname.'.RQ_IDENT_DOC_SER'] = $arContactRequisite['RQ_IDENT_DOC_SER'];
                        $arProperties['var' . $vname.'.RQ_IDENT_DOC_NUM'] = $arContactRequisite['RQ_IDENT_DOC_NUM'];
                        $arProperties['var' . $vname.'.RQ_IDENT_DOC_DATE'] = $arContactRequisite['RQ_IDENT_DOC_DATE'];
                        $arProperties['var' . $vname.'.RQ_IDENT_DOC_ISSUED_BY'] = $arContactRequisite['RQ_IDENT_DOC_ISSUED_BY'];
                        $arProperties['var' . $vname.'.RQ_IDENT_DOC_DEP_CODE'] = $arContactRequisite['RQ_IDENT_DOC_DEP_CODE'];

        			}
                  }
        		}
        		else{
        			$arProperties['var' . $vname.'.LAST_NAME'] = '';
                    $arProperties['var' . $vname.'.NAME'] = '';
                    $arProperties['var' . $vname.'.SECOND_NAME'] = '';
        			$arProperties['var' . $vname.'.FULL_NAME'] = '';
                    $arProperties['var' . $vname.'.POST'] = '';
                    $arProperties['var' . $vname.'.ADDRESS'] = '';
                    $arProperties['var' . $vname.'.FULL_NAME_SHORT'] = '';
        		}
              }
            }
            elseif($vVal['Type'] == 'user'){
              //AddMessage2Log($vname, __LINE__);
              //AddMessage2Log($rootActivity->GetVariable($vname), __LINE__);
            }
            else{
              $arProperties['var' . $vname] = str_replace(chr(13), '<br>', $rootActivity->GetVariable($vname));
              $arProperties['var' . $vname] = str_replace(chr(10), '<br>', $rootActivity->GetVariable($vname));

              $arFFIO = $CDocDes->getDeclination($arProperties['var' . $vname]);
              $arProperties['var' . $vname.'.RP'] = $arFFIO[1];
              $arProperties['var' . $vname.'_G'] = $arFFIO[1];
        	  $arProperties['var' . $vname.'.DP'] = $arFFIO[2];
        	  $arProperties['var' . $vname.'_D'] = $arFFIO[2];
            }
              $arPropertiesTypes['var' . $vname]['Type'] = $vVal['Type'];
          }
        }

        if($_docNumber = $rootActivity->GetVariable('DocNumber')){
          if(strlen($_docNumber) > 0){
            $arNum = $arProperties['arNum'];
            $arNum["NAME"] = $_docNumber;
            $arNum["fName"] = $_docNumber;
            $arNum["DocNumber"] = $_docNumber;
            $arProperties['arNum'] = $arNum;
            $arProperties['DocNumber'] = $_docNumber;
            $arProperties['BILL_NUM'] = $_docNumber;
          }
        }
        //AddMessage2Log(print_r($arProperties, true), "htmls.DocDesigner");

        $arResult = $CDocDes->PrepareTable(array($DocType, intval($TemplateID)), $arProperties, $arPropertiesTypes);
		$arProperties = $arResult['arProperties'];
		if($arProperties['MultipleTotal'] > 0){
			$arProperties['TotalSum'] = $arProperties['MultipleTotal'];
		}

		if(intval($TemplateID) > 0){
			$query = "SELECT tmp_name FROM htmls_docdesigner_templates WHERE id='" . intval($TemplateID) . "'";
			$result = $DB->Query($query);
			if($arRes = $result->Fetch()){
				$tmpName = $arRes['tmp_name'];
			}
		}

		//$arNum = $arProperties['arNum'];
		if($DocumentType[0] == 'bizproc'){
			//$COMPANY_ID = $arProperties['DocDesignerCompany'];
			$r = $DB->Query("SELECT NAME from b_bp_workflow_template WHERE DOCUMENT_TYPE = '" . $DocumentType[2] . "'");
			$arDesc = $r->Fetch();
			$tmpName = $arDesc['NAME'];
			$arNum['DOC_NAME'] = $arDesc['NAME'];
			$arNum['COMPANY_ID'] = $arProperties['DocDesignerCompany'];
		}
		$arNum['DOC_NAME'] = $tmpName;
		$AddId2Suffix = true;
		if($arNum["NAME"] == "" and $tmpName != ""){
			$arNum["NAME"] = GetMessage('NO_NUM');//$tmpName;
			//$arNum['DOC_NAME'] = "";
			//$AddId2Suffix = true;
		}
		$arResult['fname'] = $arNum["NAME"];
        //AddMessage2Log($rootActivity->GetVariable('fileNameSuffix'), __LINE__.'.'.__FUNCTION__);
        if($fileNameSuffix = $rootActivity->GetVariable('fileNameSuffix')){
          $arResult['fname'] = $arResult['fname'] . ' ' .$fileNameSuffix;
        }
        /*
        if($_docNumber = $rootActivity->GetVariable('DocNumber')){
          if(strlen($_docNumber) > 0){
            $arNum["NAME"] = $_docNumber;
            $arNum["fName"] = $_docNumber;
            $arNum["DocNumber"] = $_docNumber;
            $arProperties['arNum'] = $arNum;
            $arProperties['DocNumber'] = $_docNumber;
            $arProperties['BILL_NUM'] = $_docNumber;
          }
        }
        AddMessage2Log(print_r($arProperties, true), "htmls.DocDesigner");
        */
        if($rootActivity->GetVariable('DocDesignerOfferRowsVar')){
			$arProperties['DocDesignerOfferRows'] = $rootActivity->GetVariable('DocDesignerOfferRowsVar');
		}

		$arResult['fontSize'] = $FontSize;
		$rootActivity->SetVariable('DocNumber', $arNum["NAME"]);
        if(empty($MIME))
            $arResult['mime'] = 'PDF';
        else
            $arResult['mime'] = $MIME;

        $arResult['footerValue'] = array(
                                    'DocNumber' => $CDocDes->win2uni($arNum["NAME"]),
                                    'DocDate' => $CDocDes->win2uni(FormatDate("d F Y", time())),
                                    'Company.CEO_SHORT' => $CDocDes->win2uni($arProperties['Company.CEO_SHORT']),
                                    'contactFio' => $CDocDes->win2uni($arContact['FULL_NAME_SHORT']),
                                    'initiallingOfPages' => $arProperties['Company.initiallingOfPages']
                                    );
        $accName = $CDocDes->uni2win($arProperties['Company.ACC_NAME']);
        $accName = $CDocDes->getShortName($accName);
        $arResult['compositeFacsimile'] = array(
                                    'CEO_POST' => $CDocDes->win2uni($arProperties['Company.CEO_POST']),
                                    'ACC_POST' => $CDocDes->win2uni($arProperties['Company.ACC_POST']),
                                    'CEO_NAME' => $CDocDes->win2uni($arProperties['Company.CEO_NAME']),
                                    'ACC_NAME' => $CDocDes->win2uni($accName),
                                    'CEO_SIGN' => $arProperties['Company.CEO_SIGN_CF'],
                                    'ACC_SIGN' => $arProperties['Company.ACC_SIGN_CF'],
                                    'STAMP' => $arProperties['Company.STAMP_CF']
                                    );
        //AddMessage2Log(print_r($arResult['footerValue'], true), __LINE__.'.'.__FUNCTION__);
		$pdf = $_SERVER['DOCUMENT_ROOT'] . $CDocDes->CreateFile($arResult, $DocType, $DocId);

        //$CDocDes->RegisterFile($pdf, $workflowId, $elementId, $tmpName);

		/*elseif($arNum["NAME"] == "" and $tmpName == ""){
			$arNum["NAME"] = 'KP';
			$arNum['DOC_NAME'] = 'KP';
		}*/
				if($UserName = $rootActivity->GetVariable("DocDesignerDocumentName")){
					$NAME = $UserName;
					$arNum["NAME"] = $UserName;
				}

        //$arBizProc = array('arPropperties' => $arProperties, 'arPropertiesTypes' => $arPropertiesTypes,
        //            'workflowId' => $workflowId, 'TemplateID' => $TemplateID, 'rootActivity' => $rootActivity);
        $arBizProc['arRootActivity'] = $arRootActivity;

        //AddMessage2Log(print_r($arResult, true), "htmls.DocDesigner");
        $fileId = 0;
        if(file_exists($pdf)){
          //if(!$this->docDesignerSaved){
			if($REG_ID = $CContract->RegisterContract($COMPANY_ID, $DEAL_ID, $arNum, $arProperties['TotalSum'], $workflowId, $pdf, $invProd, $arProperties["DocDesignerCompanyContract"], $arBizProc, $arCrmId)){
				//if(IsModuleInstalled("webdav")){
					//$NAME = $arNum['PREFIX'] . '-' . $arNum['YEAR'] . '-' . $arNum['SNUMBER'] . ' (' . str_replace('"', '', $arNum['COMPANY']) . ')';
					$NAME = trim($tmpName . " " . $arNum['fName']);
					//if($arNum['DEAL']) $NAME = str_replace('{Deal.TITLE}', str_replace('"', '', $arNum['COMPANY']), $NAME);
					if($arNum['DEAL']) $NAME .= ' ' . str_replace('"', '', $arNum['DEAL']) . '';
					if($arNum['COMPANY']) $NAME .= ' (' . str_replace('"', '', $arNum['COMPANY']) . ')';
					if($arNum['CONTACT']) $NAME .= ' (' . str_replace('"', '', $arNum['CONTACT']) . ')';
					if($AddId2Suffix) $NAME .= '_' . $REG_ID;
                    if($fileNameSuffix) $NAME .= '_' . $fileNameSuffix;
                    if($fileNamePrefix) $NAME = $fileNamePrefix . '_' . $NAME;
                    //AddMessage2Log($NAME, __LINE__.'.'.__FUNCTION__);
					//$CContract->RegisterInLibrary($REG_ID, $NAME, $pdf);
					$ElID = $CContract->RegisterInLibrary($REG_ID, $NAME, $pdf, $arProperties['DocDesignerDocumentSection']);
                    $fileId = $ElID['ID'];
                    $arCopy1 = $ElID;
					//self::UpdateFileList($ElID, $DEAL_ID, $COMPANY_ID);
				//}
				//return $REG_ID;
                $rootActivity->SetVariable("docDesignerSaved", 'Y');
                $rootActivity->SetVariable("docDesignerNumber", $arNum["NAME"]);
                $rootActivity->SetVariable("docDesignerDate", time());
			}
          //}
		}

		if($CreateOriginal == 'Y' && $arResult['mime'] == 'PDF'){
			$arResult = $CDocDes->PrepareTable(array($DocType, intval($TemplateID)), $arProperties, $arPropertiesTypes, false, true);
            $arResult['fname'] = $arNum["NAME"];
    		$arResult['fontSize'] = $FontSize;
            $arResult['footerValue'] = array(
                                    'DocNumber' => $CDocDes->win2uni($arNum["NAME"]),
                                    'DocDate' => $CDocDes->win2uni(FormatDate("d F Y", time())),
                                    'Company.CEO_SHORT' => $CDocDes->win2uni($arProperties['Company.CEO_SHORT']),
                                    'contactFio' => $CDocDes->win2uni($arContact['FULL_NAME_SHORT']),
                                    //'initiallingOfPages' => $arProperties['Company.initiallingOfPages']
                                    );
    		$rootActivity->SetVariable('DocNumber', $arNum["NAME"]);
            if(empty($MIME))
                $arResult['mime'] = 'PDF';
            else
                $arResult['mime'] = $MIME;
			$pdf = $_SERVER['DOCUMENT_ROOT'] . $CDocDes->CreateFile($arResult, $DocType, $DocId);
            //AddMessage2Log($pdf, "htmls.DocDesigner");
			if(file_exists($pdf)){
				$arNum["NAME"] = $arNum["NAME"] . " (".GetMessage("ORIGINAL").")";
				if($REG_ID = $CContract->RegisterContract($COMPANY_ID, $DEAL_ID, $arNum, $arProperties['TotalSum'], $workflowId, $pdf, $invProd, $arProperties["DocDesignerCompanyContract"], $arBizProc, $arCrmId)){
					if(IsModuleInstalled("disk") || IsModuleInstalled("webdav")){
						//$NAME = $arNum['PREFIX'] . '-' . $arNum['YEAR'] . '-' . $arNum['SNUMBER'] . ' (' . str_replace('"', '', $arNum['COMPANY']) . ')';
						$NAME = trim($tmpName . " " . $arNum['fName']);
						if($arNum['COMPANY']) $NAME .= ' (' . str_replace('"', '', $arNum['COMPANY']) . ', '.GetMessage('ORIGINAL').')';
						if($AddId2Suffix) $NAME .= '_' . $REG_ID;
                        if($fileNameSuffix) $NAME .= '_' . $fileNameSuffix;
                        if($fileNamePrefix) $NAME = $fileNamePrefix . '_' . $NAME;
                        //AddMessage2Log($NAME, __LINE__.'.'.__FUNCTION__);
						//$CContract->RegisterInLibrary($REG_ID, $NAME, $pdf);
						$ElID = $CContract->RegisterInLibrary($REG_ID, $NAME, $pdf, $arProperties['DocDesignerDocumentSection']);
                        $originalId = $ElID['ID'];
                        $arCopy0 = $ElID;
						//self::UpdateFileList($ElID, $DEAL_ID, $COMPANY_ID);
						//return $REG_ID;
					}
				}
			}
		}
        $bpId = $rootActivity->GetWorkflowInstanceId();
        $arResult['DocDate'] = date("d.m.Y", time());//today
        $arResult['DocDateSpelling'] = FormatDate("d F Y", time());//today
		$arResult['DocNumber'] = $arNum['DocNumber'];
		$arResult['FileID'] = $fileId;
		$arResult['OriginalID'] = $originalId;
		$arResult['Copy0'] = $arCopy0;
		$arResult['Copy1'] = $arCopy1;
		//$arResult['links'] = $rootActivity->GetWorkflowInstanceId();
        $arResult['links'] = [];
        $arLinks = CDocDesignerContracts::GetContract($bpId, true);
        foreach($arLinks as $arLink)
        {
		    $arResult['links'][] = $arLink;//['LINK'] . ' ' . $arLink['ICON'] . "\n";
        }
        //AddMessage2Log(print_r($arCopy1, true), "htmls.DocDesigner");
        //AddMessage2Log(print_r($arLinks, true), "htmls.DocDesigner");
        return $arResult;
	}//CreatePdf

	function CreateDocx($rootActivity, $arProperties, $arPropertiesTypes, $workflowId){//, $DOCXAutoNumber = 'Y', $DOCXProject = 'N'){
		global $DB, $MESS;
		$CASE = COption::GetOptionInt("htmls.docdesigner", "DOCDESIGNER_DECLINATION_CASE", 1);
        $DigitCol = (COption::GetOptionString('htmls.docdesigner', 'DOCDESIGNER_REMOVE_PENNY', 'N') == 'Y')? 0 : 2;
        if(!$arProperties['DocDesignerCompany']){
          $arProperties['DocDesignerCompany'] = $rootActivity->GetVariable('DocDesignerCompany');
          $rootActivity->DocDesignerCompany = $arProperties['DocDesignerCompany'];
        }
        $arCrmId = array('deal'=>0, 'company'=>0, 'lead'=>0, 'contact'=>0, 'invoice'=>0, 'offer'=>0);
		$Numerator = $arProperties["DOCXNumerator"];
		CModule::IncludeModule("iblock");
		$IsCrm = IsModuleInstalled("crm");
		if($IsCrm)
			CModule::IncludeModule("crm");

		$IsCatalog = IsModuleInstalled("catalog");
		if($IsCatalog)
			CModule::IncludeModule("catalog");

		$module_id = "htmls.docdesigner";
		$DocumentId = $rootActivity->GetDocumentId();
		$DocumentType = $rootActivity->GetDocumentType();

        if($DocumentType[0] == 'lists'){
          CModule::IncludeModule('iblock');
          $arIBlockElement = GetIBlockElement($DocumentId[2]);
          //AddMessage2Log(print_r($arIBlockElement, 1));
          foreach($arIBlockElement as $name => $val){
            if($name == 'PROPERTIES'){
              foreach($val as $key => $_val){
                $arProperties[$key] = $_val['VALUE'];
                $arProperties[$_val['CODE']] = $_val['VALUE'];
              }
            }
            else{
                $arProperties[$name] = $val;
            }
          }
        }
        //AddMessage2Log(print_r($arProperties, true));
        if($arProperties['DocumentSection'] > 0){
			$arProperties['DocDesignerDocumentSection'] = $arProperties['DocumentSection'];
		}
		if($DocumentSection = $rootActivity->GetVariable('DocDesignerDocumentSection')){
			$arProperties['DocDesignerDocumentSection'] = $DocumentSection;
		}


        $arVariables = self::getVariables($rootActivity->GetWorkflowInstanceId());

        foreach($arVariables['vars'] as $vname => $vVal){
            $arProperties['var' . $vname] = $rootActivity->GetVariable($vname);
        }
        //AddMessage2Log(print_r($rootActivity->arProperties, true));
        //AddMessage2Log(print_r($arProperties, true));
		$CContract = new CDocDesignerContracts();
		$CDocDes = new CDocDesignerProcessing();



        foreach($arProperties as $k => $v){
			if($k == 'TargetUser'){
				$CDocDes->UserDeclination($v, 'TargetUser', $arProperties);
			}
			elseif($k == 'DocDesignerCompany'){
				$arCompany = $CDocDes->GetCompanyData($v);
			}
			else{
				if($arPropertiesTypes[$k]['Type'] == 'S:UserID' || $arPropertiesTypes[$k]['Type'] == 'user' || $arPropertiesTypes[$k]['Type'] == 'S:employee'){
					if(is_array($v))	$sID = $v[0];
					else 				$sID = $v;
					$CDocDes->UserDeclination($sID, $k, $arProperties);
				}
			}
		}
        //AddMessage2Log(print_r($arProperties,1 ), "htmls.DocDesigner");
		if($path = $CContract->GetDocTemplate($arProperties['DocDesignerDogTemplateID'], $arProperties['_DocDesignerTemplateFromList'])){
			$Word = new PHPWord();
            //$Footer = new PHPWord();
			$num2str = new CNum2Str;
			if($IsCrm){
				$DEAL_ID = substr($DocumentId[2], strlen("DEAL_"));
                $key = substr($DocumentId[2], 0, 4);
                if($key == 'LEAD'){
                    $LEAD_ID = substr($DocumentId[2], strlen("LEAD_"));
                    $res = CCrmLead::GetList(array(),array('ID' => $LEAD_ID),array());
                    $arCrmId['lead'] = $LEAD_ID;
                }
                else{
                  $res = CCrmDeal::GetList(array(),array('ID' => $DEAL_ID),array());
                  $arCrmId['deal'] = $DEAL_ID;
                }
				//get company id
				//$res = CCrmDeal::GetList(array(),array('ID' => $DEAL_ID),array());
				while($ob = $res->Fetch()){
					$COMPANY_ID = $ob['COMPANY_ID'];
                    $arCrmId['company'] = $COMPANY_ID;
					$arDeal = $ob;
				}
                //AddMessage2Log(print_r($arDeal, true), "htmls.DocDesigner");
				foreach($arDeal as $k => $v){
					$arDealMulti[$key . '.'.$k] = $v;
				}
				$arDeal = array_merge($arDeal, $arDealMulti);
                //AddMessage2Log(print_r($arDeal, true), "htmls.DocDesigner");
                foreach($arDeal as $k => $v){
                  $arProperties[$k] = $v;
                  //$arProperties[$key . '.'.$k] = $v;
                }

				if(intval($COMPANY_ID) == 0){
					$COMPANY_ID = substr($DocumentId[2], strlen("COMPANY_"));
				}

				$arCrmFields = $CDocDes->GetCrmFields();

				//$arProperties = self::SetCRMParameters($rootActivity, $arProperties, $arPropertiesTypes, $DocumentId, $CDocDes, $CContract, $num2str, $Total, $DEAL_ID, $COMPANY_ID, $invProd, $Numerator, $arCrmId);

				if(!is_dir($_SERVER['DOCUMENT_ROOT'] . "/upload/htmls.docdesigner/COMPANY_".$COMPANY_ID."/"))
					mkdir($_SERVER['DOCUMENT_ROOT'] . "/upload/htmls.docdesigner/COMPANY_".$COMPANY_ID."/");
    				//get company info
				$res = CCrmCompany::GetListEx(array(),array('ID' => $COMPANY_ID), false, false, array('*', 'UF_*', 'PHONE', 'EMAIL'));
				while($ob = $res->GetNext()){
					$COMPANY_NAME = $ob['TITLE'];
					$COMPANY = $ob;
				}
                if($COMPANY_ID > 0){
    				$arCompanyMulti = self::GetCrmMulti($COMPANY_ID, 'COMPANY');
    				$COMPANY = array_merge($COMPANY, $arCompanyMulti);

                    $arCompanyRequisite = self::getCrmRequisite($COMPANY_ID);
                }
                //AddMessage2Log(print_r($arProperties,1 ), "htmls.DocDesigner");
                if(!$arProperties['CONTACT.FULL_FIO']){
    				//get person info
    				if(!isset($arProperties['DocDesignerPerson'])){
    					//get person ID from deal
    					$arProperties['DocDesignerPerson'] = $arDeal['CONTACT_ID'];
    				}
    				elseif(intval($arProperties['DocDesignerPerson']) == 0){
    					//get person ID from deal
    					$arProperties['DocDesignerPerson'] = $arDeal['CONTACT_ID'];
    				}
                    //AddMessage2Log($arProperties['DocDesignerPerson'], "htmls.DocDesigner");
    				$res = CCrmContact::GetListEx(array(), Array('ID' => $arProperties['DocDesignerPerson']), false, false, array('*', 'UF_*', 'PHONE', 'EMAIL'));//, array());
    				while($ob = $res->GetNext()){
    					$FIO = $ob['LAST_NAME'] . ' ' . $ob['NAME'] . ' ' . $ob['SECOND_NAME'];
    					$arContact = $ob;
                        $arCrmId['contact'] = $ob['ID'];
    				}
                    //AddMessage2Log(print_r($ob,1 ), "htmls.DocDesigner");
    				$arContact['CONTACT.POST'] = $arContact['POST'];
    				$arContact['CONTACT.ADDRESS'] = $arContact['ADDRESS'];
    				$arContact['CONTACT.NAME'] = $arContact['NAME'];
    				$arContact['CONTACT.FULL_NAME'] = $arContact['FULL_NAME'];
    				if(intval($arProperties['DocDesignerPerson'] > 0)){
            		    $arContactMulti = self::GetCrmMulti($arProperties['DocDesignerPerson'], 'CONTACT');
                    }
                    else{
                        $arContact['CONTACT.PHONE_WORK'] = '';
                        $arContact['CONTACT.PHONE_HOME'] = '';
                    }
                }
                else{
                  $FIO = $arProperties['CONTACT.FULL_FIO'];
                }
				$arContact = array_merge($arContact, $arContactMulti);
                //AddMessage2Log("1", "htmls.DocDesigner");
                //AddMessage2Log(print_r($arContact, true), "htmls.DocDesigner");
				//$arProperties = self::SetCrmProperties($arProperties, $arCrmFields, $arDeal, $COMPANY, $arContact, 'DOCX');
                //AddMessage2Log("2", "htmls.DocDesigner");
                //AddMessage2Log(print_r($arProperties, true), "htmls.DocDesigner");
			}
			else{//cms
				$COMPANY_ID = $arProperties['DogDesignerCustomer'];
				if(!is_dir($_SERVER['DOCUMENT_ROOT'] . "/upload/htmls.docdesigner/COMPANY_".$COMPANY_ID."/"))
					mkdir($_SERVER['DOCUMENT_ROOT'] . "/upload/htmls.docdesigner/COMPANY_".$COMPANY_ID."/");
				$COMPANY = $CContract->GetCustomerData($COMPANY_ID);
				$COMPANY_NAME = $COMPANY['NAME'];
				$FIO = $COMPANY['PERSON'];
			}
            $arProperties = self::SetCRMParameters($rootActivity, $arProperties, $arPropertiesTypes, $DocumentId, $CDocDes, $CContract, $num2str, $Total, $DEAL_ID, $COMPANY_ID, $invProd, $Numerator, $arCrmId);
            //AddMessage2Log($FIO, "htmls.DocDesigner");
			$arFFIO = $CDocDes->getDeclination($FIO);
            $arNum = $CDocDes->GetNumeratorInfo($Numerator, $arProperties['DocDesignerNumberPrefix'], $arProperties["DocDesignerCompany"], $arProperties["DocDesignerCompanyContract"]);
            if(strlen($arProperties['DOGOVOR_NUMBER']) > 0){
                $snum = $arProperties['DOGOVOR_NUMBER'];
                $arNum['COMPANY'] = $COMPANY_NAME;
    			//$arNum['PREFIX'] = $arProperties['DocDesignerNumberPrefix'];
    			$arNum['COMPANY_ID'] = $arProperties['DocDesignerCompany'];
                $arNum['SNUMBER'] = $snum;
    			$arNum['NUMBER'] = 0;
                $arNum["NAME"] = $snum;
                //$r = $DB->Query("SELECT NAME from b_bp_workflow_template WHERE ID = '" . $_REQUEST['workflow_template_id'] . "'");
  				//$arDesc = $r->Fetch();
  				//$tmpName = $arDesc['NAME'];
  				//$arNum['DOC_NAME'] = $arDesc['NAME'];
            }
            else{
			//get auto-num
    			//$arNum = $CDocDes->GetNumeratorInfo($Numerator, $arProperties['DocDesignerNumberPrefix'], $arProperties["DocDesignerCompany"], $arProperties["DocDesignerCompanyContract"]);
    			$arNum['COMPANY'] = $COMPANY_NAME;
    			$arNum['PREFIX'] = $arProperties['DocDesignerNumberPrefix'];
    			$arNum['COMPANY_ID'] = $arProperties['DocDesignerCompany'];

    			if($arProperties['DocDesignerAutoNumber'] == 'N'){
       				$l = $arNum['LENGTH'];
    				if(intval($l) == 0){
    					$l = 3;
    				}
    				$num = '';
    				for($i=1;$i<=$l;$i++){
    					$num .= '0';
    				}
    				$arNum['SNUMBER'] = $num;
    				$arNum['NUMBER'] = 0;
    			}

    			$snum = str_replace("{PREFIX}", $arProperties['DocDesignerNumberPrefix'], $arNum['TEMPLATE']);
    			$snum = str_replace("{YEAR}", $arNum['YEAR'], $snum);
    			$snum = str_replace("{NUMBER}", $arNum['SNUMBER'], $snum);
               $snum = str_replace("{MONTH}", date("m"), $snum);

   			$arNum["NAME"] = $snum;
    			if(empty($snum)){
    				$r = $DB->Query("SELECT NAME from b_bp_workflow_template WHERE DOCUMENT_TYPE = '" . $DocumentType[2] . "'");
    				$arDesc = $r->Fetch();
    				//$tmpName = $arDesc['NAME'];
    				$arNum['DOC_NAME'] = $arDesc['NAME'];
    				$arNum['COMPANY_ID'] = $arProperties['DocDesignerCompany'];
    				$arNum["NAME"] = $arDesc['NAME'];
    			}
            }
			$num = $snum;
			//set filename
	        $fname = $_SERVER["DOCUMENT_ROOT"] . '/upload/htmls.docdesigner/COMPANY_'.$COMPANY_ID.'/'.time().'.docx';
	        //load template from file
			if(strpos($path, '://') > 0){//clouds
				copy($path, $_SERVER["DOCUMENT_ROOT"].'/bitrix/tmp/template.docx');
				$path = '/bitrix/tmp/template.docx';
			}
	        $document = $Word->loadTemplate($_SERVER["DOCUMENT_ROOT"] . $path);
	        //set contract or appendix info
			$dt = FormatDate("d F Y", time());//today
            if(strlen($arProperties['DOGOVOR_DATE']) > 0){
                $dt = FormatDate("d F Y", strtotime($arProperties['DOGOVOR_DATE']));
            }

			if(intval($arProperties["DocDesignerCompanyContract"] > 0)){
				//appendix
				$document->setValue('APPENDIX_DATE', $CDocDes->win2uni($dt));
				if($arProperties['DocDesignerAutoNumber'] != 'N'){
					$document->setValue('APPENDIX_NUMBER', $CDocDes->win2uni($num));
				}
				else{
					$document->setValue('APPENDIX_NUMBER', $CDocDes->win2uni('${'.$num.'}'));
				}
				$res = CIBlockElement::GetByID($arProperties["DocDesignerCompanyContract"]);
				if($ar_res = $res->GetNext()){
					$document->setValue('DOGOVOR_NUMBER', $CDocDes->win2uni($ar_res['NAME']));
					$document->setValue('DOGOVOR_DATE', $CDocDes->win2uni(FormatDate("d F Y", MakeTimeStamp($ar_res['DATE_CREATE'], "DD.MM.YYYY HH:MI:SS"))));
                    $DOGOVOR_NUMBER = $CDocDes->win2uni($ar_res['NAME']);
                    $DOGOVOR_DATE = $CDocDes->win2uni(FormatDate("d F Y", MakeTimeStamp($ar_res['DATE_CREATE'], "DD.MM.YYYY HH:MI:SS")));
				}
			}
			else{
				//contract
				$document->setValue('DOGOVOR_DATE', $CDocDes->win2uni($dt));
                $DOGOVOR_DATE = $CDocDes->win2uni($dt);
				if($arProperties['DocDesignerAutoNumber'] != 'N'){
					$document->setValue('DOGOVOR_NUMBER', $CDocDes->win2uni($num));
                    $DOGOVOR_NUMBER = $CDocDes->win2uni($num);
				}
				else{
					$document->setValue('DOGOVOR_NUMBER', $CDocDes->win2uni('${'.$num.'}'));
				}
			}
			/*$document->setValue('DOGOVOR_DATE', $CDocDes->win2uni($dt));
			if($arProperties['DocDesignerAutoNumber'] != 'N'){
				$document->setValue('DOGOVOR_NUMBER', $CDocDes->win2uni($num));
			}
			else{
				$document->setValue('DOGOVOR_NUMBER', $CDocDes->win2uni('${'.$num.'}'));
			}*/

			//set own company info
			$SET = "COMPANY_OGRN,COMPANY_INN,COMPANY_KPP,COMPANY_RS,COMPANY_POST_ADDRESS,COMPANY_ADDRESS_LEGAL,COMPANY_ADDRESS,COMPANY_PHONE,COMPANY_PERSON,COMPANY_BANK_NAME,COMPANY_BANK_KS,COMPANY_BANK_CITY,COMPANY_BANK_BIK";
			$arSet = explode(',',$SET);
			foreach($arSet as $str){
				$n = COption::GetOptionString($module_id, 'DOCDESIGNER_' . $str);
				$arSelect[$n] = "PROPERTY_" . $n;
			}
			$arOwnCompany = $CContract->GetOwnCompanyData($arProperties['DocDesignerCompany'], $arSelect);
			foreach($arOwnCompany as $pid => $pval){
				$arProperties[$pid] = $pval;
        	}

    		foreach($arSet as $str){
				if($str == 'COMPANY_PERSON'){
					$OwnFIO = $arOwnCompany["PROPERTY_" . COption::GetOptionString($module_id, 'DOCDESIGNER_' . $str)];
					$arOwnFFIO = $CDocDes->getDeclination($OwnFIO);
					$document->setValue('OWN_COMPANY_PERSON_FULL_NAME', $CDocDes->win2uni($arOwnFFIO[$CASE]));
					$document->setValue('OWN_COMPANY_PERSON_FULL_NAME.RP', $CDocDes->win2uni($arOwnFFIO[1]));
					$document->setValue('OWN_COMPANY_PERSON_FULL_NAME_G', $CDocDes->win2uni($arOwnFFIO[1]));
					$document->setValue('OWN_COMPANY_PERSON_SHORT_NAME', $CDocDes->win2uni($CDocDes->getShortName($OwnFIO)));
				}
				else
					$document->setValue('OWN_' . $str, $CDocDes->win2uni($arOwnCompany["PROPERTY_" . COption::GetOptionString($module_id, 'DOCDESIGNER_' . $str)]));
			}
			$document->setValue('OWN_COMPANY_NAME', $CDocDes->win2uni($arOwnCompany['NAME']));
			$document->setValue('OWN_COMPANY_FULL_NAME', $CDocDes->win2uni($arOwnCompany['NAME']));
			$document->setValue('OWN_BANK_RS', $CDocDes->win2uni($arOwnCompany["PROPERTY_" . COption::GetOptionString($module_id, 'DOCDESIGNER_COMPANY_RS')]));
			//$arOwnBank = $CContract->GetBankInfo($arOwnCompany["PROPERTY_" . COption::GetOptionString($module_id, 'DOCDESIGNER_COMPANY_BANK')]);
			$document->setValue('OWN_BANK_KS', $CDocDes->win2uni($arOwnCompany["PROPERTY_" . COption::GetOptionString($module_id, 'DOCDESIGNER_COMPANY_BANK_KS')]));
			$document->setValue('OWN_BANK_BIK', $CDocDes->win2uni($arOwnCompany["PROPERTY_" . COption::GetOptionString($module_id, 'DOCDESIGNER_COMPANY_BANK_BIK')]));
			$document->setValue('OWN_BANK_NAME', $CDocDes->win2uni($arOwnCompany["PROPERTY_" . COption::GetOptionString($module_id, 'DOCDESIGNER_COMPANY_BANK_NAME')]));
			$document->setValue('OWN_BANK_CITY', $CDocDes->win2uni($arOwnCompany["PROPERTY_" . COption::GetOptionString($module_id, 'DOCDESIGNER_COMPANY_BANK_CITY')]));
	        //set crm-company info
            $COMPANY['ADDRESS_LEGAL'] = '';
            $strReq = 'REG_ADDRESS_POSTAL_CODE,REG_ADDRESS_COUNTRY,REG_ADDRESS_PROVINCE,REG_ADDRESS_REGION,REG_ADDRESS_CITY,REG_ADDRESS,REG_ADDRESS_2';
            $arReq = explode(',', $strReq);
            foreach($arReq as $f){
              if(!empty($COMPANY[$f])){
                $COMPANY['ADDRESS_LEGAL'] .= $COMPANY[$f];
                if($f != 'REG_ADDRESS_2') $COMPANY['ADDRESS_LEGAL'] .= ', ';
              }
            }
            $strReq = 'ADDRESS_POSTAL_CODE,ADDRESS_COUNTRY,ADDRESS_PROVINCE,ADDRESS_REGION,ADDRESS_CITY,ADDRESS,ADDRESS_2';
            $arReq = explode(',', $strReq);
            foreach($arReq as $f){
              if(!empty($COMPANY[$f])){
                $COMPANY['POST_ADDRESS'] .= $COMPANY[$f];
                if($f != 'ADDRESS_2') $COMPANY['POST_ADDRESS'] .= ', ';
              }
            }

            if($uridadr = $arCompanyRequisite['uridadr']){
              $COMPANY['ADDRESS_LEGAL'] = '';
              if($uridadr['POSTAL_CODE']){
                $COMPANY['ADDRESS_LEGAL'] .= $uridadr['POSTAL_CODE'] . ', ';
              }

              if($uridadr['COUNTRY']){
                $COMPANY['ADDRESS_LEGAL'] .= $uridadr['COUNTRY'] . ', ';
              }

              if($uridadr['PROVINCE']){
                $COMPANY['ADDRESS_LEGAL'] .= $uridadr['PROVINCE'] . ', ';
              }

              if($uridadr['REGION']){
                $COMPANY['ADDRESS_LEGAL'] .= $uridadr['REGION'] . ', ';
              }

              $COMPANY['ADDRESS_LEGAL'] .= $uridadr['ADDRESS_1'];
            }

            if($postadr = $arCompanyRequisite['factadr']){
              $COMPANY['POST_ADDRESS'] = '';
              if($postadr['POSTAL_CODE']){
                $COMPANY['POST_ADDRESS'] .= $postadr['POSTAL_CODE'] . ', ';
              }

              if($postadr['COUNTRY']){
                $COMPANY['POST_ADDRESS'] .= $postadr['COUNTRY'] . ', ';
              }

              if($postadr['PROVINCE']){
                $COMPANY['POST_ADDRESS'] .= $postadr['PROVINCE'] . ', ';
              }

              if($postadr['REGION']){
                $COMPANY['POST_ADDRESS'] .= $postadr['REGION'] . ', ';
              }

              $COMPANY['POST_ADDRESS'] .= $postadr['ADDRESS_1'];
            }
            //RQ_DIRECTOR RQ_ACCOUNTANT RQ_CEO_NAME RQ_CEO_WORK_POS

            if($arCompanyRequisite['RQ_COMPANY_NAME'])
    		    $document->setValue('COMPANY_NAME', $CDocDes->win2uni($arCompanyRequisite['RQ_COMPANY_NAME']));
            else
    		    $document->setValue('COMPANY_NAME', $CDocDes->win2uni($COMPANY_NAME));

            if($arCompanyRequisite['RQ_COMPANY_FULL_NAME'])
    		    $document->setValue('COMPANY_FULL_NAME', $CDocDes->win2uni($arCompanyRequisite['RQ_COMPANY_FULL_NAME']));
            else
    		    $document->setValue('COMPANY_FULL_NAME', $CDocDes->win2uni($COMPANY_NAME));

            if($arCompanyRequisite['RQ_INN'])
    		    $COMPANY['INN'] = $arCompanyRequisite['RQ_INN'];

            if($arCompanyRequisite['RQ_KPP'])
    		    $COMPANY['KPP'] = $arCompanyRequisite['RQ_KPP'];

            if($arCompanyRequisite['RQ_OGRN'])
                $COMPANY['OGRN'] = $arCompanyRequisite['RQ_OGRN'];

            foreach($COMPANY as $k => $v){
              $document->setValue('COMPANY_' . $k, $CDocDes->win2uni($v));
              $document->setValue('COMPANY.' . $k, $CDocDes->win2uni($v));
            }

	        //$document->setValue('COMPANY_NAME', $CDocDes->win2uni($COMPANY_NAME));
			//$document->setValue('COMPANY_FULL_NAME', $CDocDes->win2uni($COMPANY_NAME));
			$document->setValue('COMPANY_ADDRESS_LEGAL', $CDocDes->win2uni($COMPANY['ADDRESS_LEGAL']));
			$document->setValue('COMPANY_ADDRESS', $CDocDes->win2uni($COMPANY['ADDRESS']));

            foreach($arSet as $str){
				if($str == 'COMPANY_PERSON'){
					$document->setValue('COMPANY_PERSON_FULL_NAME', $CDocDes->win2uni($arFFIO[$CASE]));
					$document->setValue('COMPANY_PERSON_FULL_NAME_G', $CDocDes->win2uni($arFFIO[1]));
					$document->setValue('COMPANY_PERSON_FULL_NAME.RP', $CDocDes->win2uni($arFFIO[1]));
					$document->setValue('COMPANY_PERSON_SHORT_NAME', $CDocDes->win2uni($CDocDes->getShortName($FIO)));
					$document->setValue('COMPANY_PERSON_SHORT_NAME_G', $CDocDes->win2uni($CDocDes->getShortName($arFFIO[$CASE])));
					$document->setValue('COMPANY_PERSON_SHORT_NAME.RP', $CDocDes->win2uni($CDocDes->getShortName($arFFIO[$CASE])));
				}
				else{
					if($IsCrm){
						$document->setValue($str, $CDocDes->win2uni($COMPANY[COption::GetOptionString($module_id, 'DOCDESIGNER_CRM_' . $str)]));
					}
					else{
						$document->setValue($str, $CDocDes->win2uni($COMPANY[str_replace('COMPANY_', '', $str)]));
					}
				}
			}

			//set crm-company bank info
			//if($IsCrm){
            if($arCompanyRequisite['bank']['RQ_BANK_NAME'])
                $document->setValue('BANK_NAME', $CDocDes->win2uni($arProperties['CrmCompany.BANK_NAME'] = $arCompanyRequisite['bank']['RQ_BANK_NAME']));
            else
                $document->setValue('BANK_NAME', $CDocDes->win2uni($COMPANY[COption::GetOptionString($module_id, 'DOCDESIGNER_CRM_COMPANY_BANK_NAME')]));

            if($arCompanyRequisite['bank']['RQ_BIK'])
    		    $document->setValue('BANK_BIK', $CDocDes->win2uni($arCompanyRequisite['bank']['RQ_BIK']));
            else
    		    $document->setValue('BANK_BIK', $CDocDes->win2uni($COMPANY[COption::GetOptionString($module_id, 'DOCDESIGNER_CRM_COMPANY_BANK_BIK')]));

            if($arCompanyRequisite['bank']['RQ_ACC_NUM'])
                $document->setValue('BANK_RS', $CDocDes->win2uni($arCompanyRequisite['bank']['RQ_ACC_NUM']));
            else
                $document->setValue('BANK_RS', $CDocDes->win2uni($COMPANY[COption::GetOptionString($module_id, 'DOCDESIGNER_CRM_COMPANY_RS')]));

            if($arCompanyRequisite['bank']['RQ_COR_ACC_NUM'])
    		    $document->setValue('BANK_KS', $CDocDes->win2uni($arCompanyRequisite['bank']['RQ_COR_ACC_NUM']));
            else
    		    $document->setValue('BANK_KS', $CDocDes->win2uni($COMPANY[COption::GetOptionString($module_id, 'DOCDESIGNER_CRM_COMPANY_BANK_KS')]));

            if($arCompanyRequisite['bank']['RQ_BANK_ADDR'])
    		    $document->setValue('BANK_CITY', $CDocDes->win2uni($arCompanyRequisite['bank']['RQ_BANK_ADDR']));
            else
    		    $document->setValue('BANK_CITY', $CDocDes->win2uni($COMPANY[COption::GetOptionString($module_id, 'DOCDESIGNER_CRM_COMPANY_BANK_CITY')]));

                //$document->setValue('BANK_RS', $CDocDes->win2uni($COMPANY[COption::GetOptionString($module_id, 'DOCDESIGNER_CRM_COMPANY_RS')]));
				////$arBank = $CContract->GetBankInfo($COMPANY[COption::GetOptionString($module_id, 'DOCDESIGNER_CRM_COMPANY_BANK')]);
				//$document->setValue('BANK_KS', $CDocDes->win2uni($COMPANY[COption::GetOptionString($module_id, 'DOCDESIGNER_CRM_COMPANY_BANK_KS')]));
				//$document->setValue('BANK_BIK', $CDocDes->win2uni($COMPANY[COption::GetOptionString($module_id, 'DOCDESIGNER_CRM_COMPANY_BANK_BIK')]));
				//$document->setValue('BANK_NAME', $CDocDes->win2uni($COMPANY[COption::GetOptionString($module_id, 'DOCDESIGNER_CRM_COMPANY_BANK_NAME')]));
				//$document->setValue('BANK_CITY', $CDocDes->win2uni($COMPANY[COption::GetOptionString($module_id, 'DOCDESIGNER_CRM_COMPANY_BANK_CITY')]));
			/*}
			else{
				$document->setValue('BANK_RS', $COMPANY['RS']);
				$arBank = $CContract->GetBankInfo($COMPANY['BANK']);
				$document->setValue('BANK_KS', $arBank["KS"]);
				$document->setValue('BANK_BIK', $arBank["BIK"]);
				$document->setValue('BANK_NAME', $CDocDes->win2uni($arBank["NAME"]));
				$document->setValue('BANK_CITY', $CDocDes->win2uni($arBank["CITY"]));
			}*/
            //
			//set bizproc parameters
            //AddMessage2Log(print_r($arProperties, true), "htmls.DocDesigner");


	        if($IsCrm){
				$arProduct = $CDocDes->GetCrmProducts($DEAL_ID, $key[0]);//GetProducts($DEAL_ID);
			}
            //AddMessage2Log(print_r($arProduct, true), "htmls.DocDesigner");
            $TotalVAT = 0;
			if($arProduct){
				$PROD_SUM = 0;
                $TotalSumWithoutDiscountTax = 0;
                //$TotalVAT = 0;
				foreach($arProduct as $p => $arProd){
					//$arProd = explode(';', $sProd);
                    $k = $p - 1;
                    if($arProd['ORIGINAL_PRODUCT_NAME'])
            	        $document->setValue('n'.$k.'_PRODUCT_NAME', $CDocDes->win2uni($arProd['ORIGINAL_PRODUCT_NAME']));
                    else
            	        $document->setValue('n'.$k.'_PRODUCT_NAME', $CDocDes->win2uni($arProd['PRODUCT_NAME']));
					$document->setValue('n'.$k.'_PRODUCT_AMOUNT', $CDocDes->win2uni(number_format($arProd['QUANTITY'], $this->quantityFloat, ',', ' ')));
					$document->setValue('n'.$k.'_PRODUCT_PRICE', $CDocDes->win2uni(number_format($arProd['PRICE'], $DigitCol, ',', ' ')));
					$document->setValue('n'.$k.'_PRODUCT_PRICE_IncDiscount', $CDocDes->win2uni(number_format($arProd['PRICE']+$arProd['DISCOUNT_SUM'], $DigitCol, ',', ' ')));
					$document->setValue('n'.$k.'_PRODUCT_SUM', $CDocDes->win2uni(number_format($arProd['QUANTITY'] * $arProd['PRICE'], $DigitCol, ',', ' ')));
                    if($arProd['TAX_RATE']){
                        $vatRate = intval($arProd['TAX_RATE']);
                    }
                    if($arProd['VAT_RATE']){
                        $vatRate = intval($arProd['VAT_RATE'] * 100);
                    }
                    $document->setValue('n'.$k.'_PRODUCT_VATRate', $CDocDes->win2uni(number_format($vatRate, 0, ',', ' ')));
                    $SUMVat = $arProd['PRICE']*$arProd['QUANTITY'];
                    $VAT = round($SUMVat * $vatRate / ($vatRate + 100),2);
                    $VAT = $SUMVat * $vatRate / ($vatRate + 100);
                    $TotalVAT = $TotalVAT + $VAT;
                    $document->setValue('n'.$k.'_PRODUCT_SumVAT', $CDocDes->win2uni(number_format($VAT, $DigitCol, ',', ' ')));
                    $document->setValue('n'.$k.'_PRODUCT_MEASURE', $CDocDes->win2uni($arProd['MEASURE_NAME']));
                    $document->setValue('n'.$k.'_PRODUCT_DiscountRate', $CDocDes->win2uni(number_format($arProd['DISCOUNT_RATE'], $DigitCol, ',', ' ')));
                    $document->setValue('n'.$k.'_PRODUCT_DiscountSum', $CDocDes->win2uni(number_format($arProd['DISCOUNT_SUM'], $DigitCol, ',', ' ')));
                    $SumNoVAT = $SUMVat - $VAT;
                    $document->setValue('n'.$k.'_PRODUCT_SUM_NO_VAT', $CDocDes->win2uni(number_format($SumNoVAT, $DigitCol, ',', ' ')));
                    $PriceNoVAT = $SumNoVAT / $arProd['QUANTITY'];
                    $document->setValue('n'.$k.'_PRODUCT_PRICE_NO_VAT', $CDocDes->win2uni(number_format($PriceNoVAT, $DigitCol, ',', ' ')));
					$PROD_SUM = $PROD_SUM + $arProd['PRICE'] * $arProd['QUANTITY'];
                    $TotalSumWithoutDiscountTax = $TotalSumWithoutDiscountTax + $arProd['QUANTITY'] *
                        ($arProd['PRICE'] / (1 + $vatRate / 100) + $arProd['DISCOUNT_SUM']);
				}
                //AddMessage2Log($PROD_SUM, "htmls.DocDesigner");
				$sum = number_format($PROD_SUM, $DigitCol, ',', ' ');
				$sum2 = number_format($TotalSumWithoutDiscountTax, $DigitCol, ',', ' ');
				$document->setValue('DOGOVOR_SUM', $sum);
				$document->setValue('PRODUCT_TOTAL_SUM_WITHOUT_DISCOUNT_TAX', $sum2);
				$str = $num2str->num2str($PROD_SUM);
                //AddMessage2Log($str, "htmls.DocDesigner");
                //AddMessage2Log($CDocDes->win2uni($str), "htmls.DocDesigner");
				//$document->setValue('DOGOVOR_SUM_SPELLING', ("htmls.DocDesigner"));
				$document->setValue('DOGOVOR_SUM_SPELLING', $CDocDes->win2uni($str));
				//$document->setValue('Parameter1', $DigitCol);
				//$document->setValue('Parameter2', COption::GetOptionString('htmls.docdesigner', 'DOCDESIGNER_REMOVE_PENNY'));
			}
			else{//string name;kol;sum or element id
				$PROD_SUM = 0;
				$n = 0;
				$OfferPropID = COption::GetOptionString($module_id, "DOCDESIGNER_OFFER_PRICE");
				foreach($arProperties['ProductID'] as $sProd){
					if(empty($sProd)) continue;
					$arProd = explode(';', $sProd);

					if(count($arProd) == 1){//iblock element id
						$ProdID = $sProd;
						$arSelect = Array("NAME", 'PROPERTY_'.$OfferPropID);
						$arFilter = Array("ID"=>IntVal($ProdID));
						$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
						if($ob = $res->GetNextElement()){
							$arFields = $ob->GetFields();
							if($IsCatalog){
								$CatalogPrice = CPrice::GetBasePrice(IntVal($ProdID));
								$PROD_SUM = $PROD_SUM + $CatalogPrice["PRICE"];
							}
							else{
								$PROD_SUM = $PROD_SUM + $arFields["PROPERTY_".$OfferPropID."_VALUE"];
							}
							$document->setValue('n'.$n.'_PRODUCT_NAME', $CDocDes->win2uni($arFields["NAME"]));
						}
						else{
							$document->setValue('n'.$n.'_PRODUCT_NAME', $CDocDes->win2uni($sProd));
						}
					}
					elseif(count($arProd) == 2){//name;sum
						$document->setValue('n'.$n.'_PRODUCT_NAME', $CDocDes->win2uni($arProd[0]));
						$PROD_SUM = $PROD_SUM + $arProd[1];
					}
					elseif(count($arProd) == 3){//name;kol;sum
						$document->setValue('n'.$n.'_PRODUCT_NAME', $CDocDes->win2uni($arProd[0]));
						$PROD_SUM = $PROD_SUM + $arProd[1]*$arProd[2];
					}
					$n++;
				}
				if($PROD_SUM != 0){
					$sum = number_format($PROD_SUM, $DigitCol, ',', ' ');
					$document->setValue('DOGOVOR_SUM', $sum);
					$str = $num2str->num2str($PROD_SUM);
					$document->setValue('DOGOVOR_SUM_SPELLING', $CDocDes->win2uni($str));
				}
			}
			if($arProperties['DOGOVOR_SUM']){
				$DOG_SUM = $arProperties['DOGOVOR_SUM'];
			}
			elseif($arProperties['DEAL.OPPORTUNITY']){
				$DOG_SUM = $arProperties['DEAL.OPPORTUNITY'];
			}
            elseif($arProperties['LEAD.OPPORTUNITY']){
				$DOG_SUM = $arProperties['LEAD.OPPORTUNITY'];
			}
    		else{
    				$DOG_SUM = $PROD_SUM;
    		}

            $arMultiFields = array();
			foreach($arProperties as $NAME => $VALUE){
				if(is_array($VALUE)){

					foreach($VALUE as $k => $v){
						if(substr($k, 0, 1) == 'n') $n = $k;
						else $n = 'n' . $k;
						$document->setValue($n . '_' . $NAME, $CDocDes->win2uni($v));
    				}
                    $arMultiFields[] = $NAME;
                    //$document->setValue($NAME, $CDocDes->win2uni(implode(PHP_EOL, $VALUE)));
				}
				else{
					if(strpos($NAME, '_SUM') > 0){
						$str = $num2str->num2str($VALUE);
						$document->setValue($NAME . '_SPELLING', $CDocDes->win2uni($str));
						$document->setValue($NAME . 'Spelling', $CDocDes->win2uni($str));
						$VALUE = number_format($VALUE, $DigitCol, ',', ' ');
					}
					if($NAME == 'DEAL.OPPORTUNITY' || $NAME == 'LEAD.OPPORTUNITY'){
						$sum = number_format($VALUE, $DigitCol, ',', ' ');
						$document->setValue($NAME, $CDocDes->win2uni($sum));
						$str = $num2str->num2str($VALUE);
						//$str = mb_strtoupper(mb_substr($str, 0, 1)) . mb_substr($str, 1);

                        //$s = $num2str->num2str($arDeal['OPPORTUNITY']);
                        $str = trim(CDocDesigner::win2uni($str));
                        $val2 = CDocDesigner::first_letter_up($str);
                        $str = CDocDesigner::uni2win($val2);

						$document->setValue($NAME . '_SPELLING', $CDocDes->win2uni($str));
						$document->setValue($NAME . 'Spelling', $CDocDes->win2uni($str));
						//$VALUE = number_format($str, 2, ',', ' ');
					}
					/*if(strpos($NAME, '_DATE') > 0 or substr($NAME, 0, 5) == 'DATE_' or substr($NAME, strlen($NAME)-4) == 'DATE'){
						$site_format = CSite::GetDateFormat();
						$arr = ParseDateTime($VALUE, $site_format);
						$stmp = mktime($arr["HH"], $arr["MI"], $arr["SS"], $arr["MM"], $arr["DD"], $arr["YYYY"]);
						$VALUE = FormatDate("d F Y", $stmp);
					}*/
					$document->setValue($NAME, $CDocDes->win2uni($VALUE));
					$document->setValue($key . '.'.$NAME, $CDocDes->win2uni($VALUE));
                    //AddMessage2Log($key . '.'.$NAME.'='.$VALUE, "htmls.DocDesigner");
				}
			}

			$vatRate = 0;
			//$TotalVAT = 0;
			if($arProperties['DocDesignerVATRate']) $vatRate = $arProperties['DocDesignerVATRate'];

            if($TotalVAT == 0){
    			if($vatRate > 0){
    				$TotalVAT = $DOG_SUM*$vatRate/(100+$vatRate);
    				$document->setValue('DOGOVOR_VAT_RATE', $CDocDes->win2uni($vatRate));
    				$sum = number_format($TotalVAT, $DigitCol, ',', ' ');
    				$document->setValue('DOGOVOR_VAT_SUM', $sum);
    				$str = $num2str->num2str($TotalVAT);
    				//$str = mb_strtoupper(mb_substr($str, 0, 1)) . mb_substr($str, 1);

                    //$s = $num2str->num2str($arDeal['OPPORTUNITY']);
                    $str = trim(CDocDesigner::win2uni($str));
                    $val2 = CDocDesigner::first_letter_up($str);
                    $str = $val2;//CDocDesigner::uni2win($val2);

    				$document->setValue('DOGOVOR_VAT_SUM_SPELLING', $str);
    				//$document->setValue('DOGOVOR_VAT_SUM_SPELLING', $CDocDes->win2uni($str));
    			}
            }
            else{
              $document->setValue('DOGOVOR_VAT_RATE', $CDocDes->win2uni($vatRate));
              $sum = number_format($TotalVAT, $DigitCol, ',', ' ');
              $str = $num2str->num2str($TotalVAT);
              $document->setValue('DOGOVOR_VAT_SUM', $sum);
      		  //$str = mb_strtoupper(mb_substr($str, 0, 1)) . mb_substr($str, 1);

              //$s = $num2str->num2str($arDeal['OPPORTUNITY']);
              $str = trim(CDocDesigner::win2uni($str));
              $val2 = CDocDesigner::first_letter_up($s);
              $str = $val2;//CDocDesigner::uni2win($val2);

      		  $document->setValue('DOGOVOR_VAT_SUM_SPELLING', $str);
      		  //$document->setValue('DOGOVOR_VAT_SUM_SPELLING', $CDocDes->win2uni($str));
            }

			if(intval($arProperties["DocDesignerCompanyContract"] > 0)){
				$snum = GetMessage('DOCDESIGNER_APPENDIX') . $snum;
			}
			$NAME = str_replace("/", "-", $snum);
			$NAME = str_replace("\\", "-", $NAME);
            $_NAME = $arNum["NAME"];
            if($fileNameSuffix = $rootActivity->GetVariable('fileNameSuffix')){
              $_NAME = $arNum["NAME"] . ' ' .$fileNameSuffix;
            }
            if($fileNamePrefix = $rootActivity->GetVariable('fileNamePrefix')){
              $_NAME = $fileNamePrefix . ' ' . $arNum["NAME"];
            }
            $fname = $_SERVER["DOCUMENT_ROOT"] . '/upload/htmls.docdesigner/COMPANY_'.$COMPANY_ID.'/'.CUtil::translit($_NAME, 'ru', array("change_case" => false, "replace_other" => '-')).'.docx';
			//$fname = CUtil::translit($NAME, 'ru', array("change_case" => false, "replace_other" => '-'));
            //remove unused ${n_PRODUCT_NAME} or ${n_WORK_LIST} - start
            $arMultiFields[] = "_PRODUCT_NAME";
            $arMultiFields[] = "_WORK_LIST";
			$dom = new DOMDocument();
			$xml = $document->getXML();
			$dom->loadXML($xml);
			$tables = $dom->getElementsByTagNameNS("http://schemas.openxmlformats.org/wordprocessingml/2006/main", 'tbl');
			foreach($tables as $table){
				$NumRow = -1;
				$delRow = array();
				foreach($table->childNodes as $tr){
					$NumRow++;
					if($tr->nodeName != "w:tr") continue;
                    foreach($arMultiFields as $mfName){
    					if(strpos($tr->nodeValue, $mfName) > 0){
    						$delRow[] = $NumRow;
    					}
    					else{
    						continue;
    					}
                    }
				}
				rsort($delRow);
				foreach($delRow as $rid){
					$row = $table->childNodes->item($rid);
					$table->removeChild($row);
				}
			}
			$xml = $dom->saveXML();
			$document->setXML($xml);
            //remove unused ${n_PRODUCT_NAME} or ${n_WORK_LIST} - fin

			$document->save($fname);

            $Footer = new PHPWord();
            $document = $Footer->loadFooter($fname);
            $document->setValue('DOGOVOR_NUMBER', $DOGOVOR_NUMBER);
            $document->setValue('DOGOVOR_DATE', $DOGOVOR_DATE);
            $document->save($fname);

            $Header = new PHPWord();
            $document = $Footer->loadHeader($fname);
            $document->setValue('DOGOVOR_NUMBER', $DOGOVOR_NUMBER);
            $document->setValue('DOGOVOR_DATE', $DOGOVOR_DATE);
            $document->save($fname);

			if(file_exists($fname)){
				if($UserName = $rootActivity->GetVariable("DocDesignerDocumentName")){
					$NAME = $UserName;
					$arNum["NAME"] = $UserName;
				}
				if($REG_ID = $CContract->RegisterContract($COMPANY_ID, $DEAL_ID, $arNum, $DOG_SUM, $workflowId, $fname, array(), $arProperties["DocDesignerCompanyContract"], array(), $arCrmId)){
					if(IsModuleInstalled("disk") || IsModuleInstalled("webdav")){
						//$NAME = $arNum['PREFIX'] . '-' . $arNum['YEAR'] . '-' . $arNum['SNUMBER'] . ' (' . str_replace('"', '', $arNum['COMPANY']) . ')';

						if(strlen($arNum['COMPANY']) > 0)
							$NAME = $NAME . ' (' . str_replace('"', '', $arNum['COMPANY']) . ')';
						//if($CContract->SAVE2LIBRARY == 'Y')
                        if($fileNameSuffix = $rootActivity->GetVariable('fileNameSuffix')){
                          $NAME = $NAME . '_' .$fileNameSuffix;
                        }
                        if($fileNamePrefix = $rootActivity->GetVariable('fileNamePrefix')){
                          $NAME = $fileNamePrefix . '_' . $NAME;
                        }
						$LIB_ID = $CContract->RegisterInLibrary($REG_ID, $NAME.'_'.$REG_ID, $fname, $arProperties['DocDesignerDocumentSection']);
						//self::UpdateFileList($LIB_ID, $DEAL_ID, $COMPANY_ID);
					}
					//return $REG_ID;
                    $arResult['DocDate'] = $dt;//today
            		$arResult['DocNumber'] = $num;
                    $arResult['FileID'] = $LIB_ID;

                    $bpId = $rootActivity->GetWorkflowInstanceId();
                    $arResult['links'] = '';
                    $arLinks = CDocDesignerContracts::GetContract($bpId, true);
                    foreach($arLinks as $arLink)
            		    $arResult['links'][] = $arLink;

                    return $arResult;
				}
			}
		}
		else{
			if($this->Add2Log == 'Y'){
				if(!$arProperties['DocDesignerDogTemplateID']){
					AddMessage2Log('The requested parameter is missing: DocDesignerDogTemplateID', "htmls.DocDesigner");
				}
				else{
					AddMessage2Log('TemplateID = ' . $arProperties['DocDesignerDogTemplateID'], "htmls.DocDesigner");
				}
				AddMessage2Log('DOCDESIGNER_CONTRACTS_TEMPLATES_LIBRARY = ' . COption::GetOptionString("htmls.docdesigner", 'DOCDESIGNER_CONTRACTS_TEMPLATES_LIBRARY'), "htmls.DocDesigner");
				AddMessage2Log(print_r($arProperties, true), "htmls.DocDesigner");
				die('WRONG TEMPLATES LIBRARY (/bitrix/tmp/DocDesigner.txt)');
			}
			else{
				die('WRONG TEMPLATES LIBRARY');
			}
		}
	}//CreateDocx

	function SendEmail($rootActivity, $arProperties, $arPropertiesTypes, $arFiles){
	  global $USER;
	  $arOwnerType = array('LEAD'=> 1, 'DEAL'=> 2, 'CONTACT'=> 3, 'COMPANY'=> 4);
      $siteID = SITE_ID;
      $from = COption::GetOptionString('crm', 'mail', '');
      $now = ConvertTimeStamp(time() + CTimeZone::GetOffset(), 'FULL', $siteID);
      $DocId = $rootActivity->GetDocumentId();
      $DocumentType = $rootActivity->GetDocumentType();
      CModule::IncludeModule('subscribe');
      $discConverted = \Bitrix\Main\Config\Option::get('disk', 'successfully_converted', false);
      if($discConverted == 'Y') $storageTypeID = 3;
      else $storageTypeID = 2;

      //AddMessage2Log($from, "htmls.DocDesigner");
      if($from == ''){
		//$rootActivity = $this->GetRootActivity();
		$CDocDes = new CDocDesignerProcessing();
		$arProperties['DocDesignerFile'] = $arFiles;
		$res = $CDocDes->SendFile($arProperties);
      }
      else{
        $arCrm = explode('_', $DocId[2]);
        $arFields = array(
            "OWNER_ID"=> $arCrm[1],
			"OWNER_TYPE_ID"=> $arOwnerType[$arCrm[0]], // 1=lead, 2=deal, 3=contact, 4=company
			"TYPE_ID"=> 4, // 1=meeting, 2=calling, 3=task, 4=email, 5=action
			"COMMUNICATIONS"=> $arProperties['DocDesignerEmail'],//[ { VALUE:"7976607@gmail.com" } ],
			"SUBJECT"=> $arProperties['DocDesignerSubject'],
                  //"AUTHOR_ID": window.CurrentUser['ID'],
			'START_TIME' => $now,
		    'END_TIME' => $now,
			"COMPLETED"=> "Y",
			"PRIORITY"=> 2, // 1=low, 2=middle, 3=high
			"RESPONSIBLE_ID"=> $USER->GetID(),
			"DESCRIPTION"=> $arProperties['DocDesignerMessage'],
			"DESCRIPTION_TYPE"=> 3, // 1=plain text, 2=bb code, 3=html
			"DIRECTION"=> 2, // 1=incoming, 2=outgoing
            'STORAGE_ELEMENT_IDS'=> $arFiles,
            'STORAGE_TYPE_ID' => $storageTypeID,
            'BINDINGS'=> array(array("OWNER_ID"=> $arCrm[1],"OWNER_TYPE_ID"=> $arOwnerType[$arCrm[0]]))
        );

        //AddMessage2Log(print_r($arFields, true), "htmls.DocDesigner");
        if($this->isCRM){
          if(!($ID = CCrmActivity::Add($arFields, false, false, array('REGISTER_SONET_EVENT' => true)))){
  		    AddMessage2Log(CCrmActivity::GetLastErrorMessage(), "htmls.DocDesigner");
  		  }
          else{
            //$arComms =

            $arComms = array();
            if($cont = $arProperties['BINDINGS']['CONTACT']){
              $arContact =  CCrmContact::GetByID($cont['OWNER_ID']);
              $arSet = array('NAME'=>$arContact['NAME'],'LAST_NAME'=>$arContact['LAST_NAME']);
              if($cid = $arContact['COMPANY_ID']){
                $arCompany =  CCrmCompany::GetByID($cid);
                $arSet['COMPANY_TITLE'] = $arCompany['TITLE'];
              }
              $arFields['BINDINGS'][] = $cont;
              $arComms[] = array('ID' => 0,
                'TYPE' => 'EMAIL',
                'VALUE' => $arProperties['DocDesignerEmail'][0],
                'ENTITY_ID' => $cont['OWNER_ID'],
                'ENTITY_TYPE_ID' => 3,
                'ENTITY_SETTINGS' => serialize($arSet),
                'ACTIVITY_ID' => $ID,
                'OWNER_ID' => $arCrm[1],
                'OWNER_TYPE_ID' => $arOwnerType[$arCrm[0]]);
            }
            //AddMessage2Log(print_r($arComms, true), "htmls.DocDesigner");
            if(count($arComms) > 0)
              CCrmActivity::DoSaveCommunications($ID, $arComms);
          }

          //$from = CUserOptions::GetOption('crm', 'activity_email_addresser', '');
          $urn = CCrmActivity::PrepareUrn($arFields);
          // Try to resolve posting charset -->
      	$postingCharset = '';
      	$siteCharset = defined('LANG_CHARSET') ? LANG_CHARSET : (defined('SITE_CHARSET') ? SITE_CHARSET : 'windows-1251');
      	$arSupportedCharset = explode(',', COption::GetOptionString('subscribe', 'posting_charset'));
      	if(count($arSupportedCharset) === 0){
      		$postingCharset = $siteCharset;
      	}
      	else{
      		foreach($arSupportedCharset as $curCharset){
      			if(strcasecmp($curCharset, $siteCharset) === 0){
      				$postingCharset = $curCharset;
      				break;
      			}
      		}

      		if($postingCharset === ''){
      			$postingCharset = $arSupportedCharset[0];
      		}
      	}
      	//<-- Try to resolve posting charset
          $postingData = array(
      		'STATUS' => 'D',
      		'FROM_FIELD' => $from,
      		'TO_FIELD' => implode(',', $arProperties['DocDesignerEmail']),
      		//'BCC_FIELD' => implode(',', $to),
      		'SUBJECT' => $arProperties['DocDesignerSubject'],
      		'BODY_TYPE' => 'html',
      		'BODY' => $arProperties['DocDesignerMessage'],
      		'DIRECT_SEND' => 'Y',
      		'SUBSCR_FORMAT' => 'html',
      		'CHARSET' => $postingCharset
      	);
          //AddMessage2Log(print_r($postingData, true), "htmls.DocDesigner");
          //AddMessage2Log($urn, "htmls.DocDesigner");

      	CCrmActivity::InjectUrnInMessage($postingData, $urn, CCrmEMailCodeAllocation::GetCurrent());

      	$posting = new CPosting();
      	$postingID = $posting->Add($postingData);

          if($postingID === false){
              AddMessage2Log(print_r($arErrors, true), __LINE__."/htmls.DocDesigner");
        	}
        	else{
        		// Attaching files -->
        		$arRawFiles = isset($arFields['STORAGE_ELEMENT_IDS']) && !empty($arFields['STORAGE_ELEMENT_IDS'])
        			? \Bitrix\Crm\Integration\StorageManager::makeFileArray($arFields['STORAGE_ELEMENT_IDS'], $storageTypeID)
        			: array();

        		foreach($arRawFiles as &$arRawFile){
        			if(isset($arRawFile['ORIGINAL_NAME'])){
        				$arRawFile['name'] = $arRawFile['ORIGINAL_NAME'];
        			}
        			if(!$posting->SaveFile($postingID, $arRawFile)){
        				$arErrors[] = GetMessage('CRM_ACTIVITY_COULD_NOT_SAVE_POSTING_FILE', array('#FILE_NAME#' => $arRawFile['ORIGINAL_NAME']));
        				$arErrors[] = $posting->LAST_ERROR;
                      AddMessage2Log(print_r($arErrors, true), __LINE__."/htmls.DocDesigner");
        				break;
        			}
        		}
        		unset($arRawFile);
        		// <-- Attaching files

        		if(empty($arErrors)){
        			$arUpdateFields = array('ASSOCIATED_ENTITY_ID'=> $postingID);
        			$fromEmail = strtolower(trim(CCrmMailHelper::ExtractEmail($from)));
        			if($crmEmail !== '' && $fromEmail !== $crmEmail)
        			{
        				$arUpdateFields['SETTINGS'] = array('MESSAGE_HEADERS' => array('Reply-To' => "<{$fromEmail}>, <$crmEmail>"));
        			}
        			CCrmActivity::Update($ID, $arUpdateFields, false, false);
        		}

              $posting->ChangeStatus($postingID, "P");
  			$res = $posting->SendMessage($postingID, 0);
  			if(!$res){
  				AddMessage2Log("Send mail: " . $posting->LAST_ERROR, "htmls.DocDesigner");
  				return false;
  			}
        	}
          $res = CPosting::Delete($postingID);
        }

      }
      if($this->isCRM){
		if(((strpos($DocId[2], 'LEAD_') !== false) or (strpos($DocId[2], 'DEAL_') !== false)) & $res){
			CModule::IncludeModule('crm');
			CModule::IncludeModule('iblock');
			$arSelect = Array("ID", "NAME");
			$arFilter = Array("PROPERTY_FILE"=>$arFiles);
			$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
			$Attach = "\n\n";
			while($ob = $res->GetNextElement()){
				$arFields = $ob->GetFields();
				$Attach .= "[[" . $arFields['NAME'] . "]]\n";;
			}

			$EventText = implode("\n", $arProperties['DocDesignerEmail'])."\n\n";
			$EventText .= $arProperties['DocDesignerSubject']."\n\n";
			$EventText .= $arProperties['DocDesignerMessage'];
			$EventText .= $Attach;
			$crm = new CCrmEvent;

			$arFields = array(
				'ENTITY_TYPE' => $DocumentType[2],
				'ENTITY_ID' => substr($DocId[2], 5),
				'EVENT_ID' => 'MESSAGE',
				'EVENT_TEXT_1' => $EventText,
				//'FILES' => array('name' => $arNames, 'type' => $arTypes, 'tmp_name' => $arTmpNames, 'size' => $arSizes)
			);
			$crm->Add($arFields);
			/*if($ID = $crm->Add($arFields)){
				@unlink($_SERVER["DOCUMENT_ROOT"] . $file);
			}*/
		}
      }
	}//SendEmail

	function GetNumeratorsList(){
		$arSelect = Array("ID", "NAME");
		$arFilter = Array("IBLOCK_CODE"=>"htmls_numerators", "ACTIVE"=>"Y");
		$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
		$arResult[0] = GetMessage('NO_NUMERATOR');
		while($ob = $res->GetNextElement()){
			$arFields = $ob->GetFields();
			$arResult[$arFields['ID']] = $arFields['NAME'];
		}
		return $arResult;
	}//GetNumeratorsList

	//update docs-list in company&deal
    //deprecated
	function UpdateFileList($ElID, $DEAL_ID, $COMPANY_ID){
	  global $DB;
	  if($DEAL_ID > 0){
		$arFilter = array("ID" => $DEAL_ID);
		CModule::IncludeModule("crm");
		$FieldName = 'UF_CRM_1430474249';//COption::GetOptionString("htmls.docdesigner", 'DOCDESIGNER_CRM_DEAL_FILES');
		$crm = new CCrmDeal();
		$dbres = $crm->GetList (array(), $arFilter);
		if ($arres = $dbres->Fetch()){
			$new_value = $arres[$FieldName];
		}
        $updFile = array();
		$new_value[] = $ElID;
        foreach($new_value as $fid){
          $arFile = CFile::MakeFileArray($fid);
          $strSql = 'SELECT * FROM  b_disk_object WHERE FILE_ID=' . $fid;
          $resDb = $DB->Query($strSql);
          if($arRes = $resDb->Fetch()){
            $arFile['name'] = $arRes['NAME'];
          }
          $updFile[] = $arFile;
        }
        //AddMessage2Log(print_r($updFile, true), "htmls.DocDesigner");
		$arUpdate = array($FieldName => $updFile);
		$crm->Update($DEAL_ID, $arUpdate);
      }

      if($COMPANY_ID > 0){
		$FieldName = COption::GetOptionString("htmls.docdesigner", 'DOCDESIGNER_CRM_COMPANY_FILES');
		$arFilter = array("ID" => $COMPANY_ID,);
		$crm = new CCrmCompany();
		$dbres = $crm->GetList (array(), $arFilter);
		if ($arres = $dbres->Fetch()){
			$new_value = $arres[$FieldName];
		}

		$new_value[] = $ElID;
		$arUpdate = array($FieldName => $new_value,);
		$crm->Update($COMPANY_ID, $arUpdate);
      }
	}//UpdateFileList

	function SetCrmProperties(&$arProperties, &$arCrmFields, $arDeal, $arCompany, $arContact, $type){
	  if(!$this->isCRM) return;

	  //foreach($arDeal)
		foreach($arCrmFields['DEAL'] as $NAME => $val){
		  if($arDeal[$NAME]){}
		  else{
		    if($val['CRM_TYPE'])
		        $arDeal[$NAME] = 0;
		  }
		  //AddMessage2Log($NAME.'='.COption::GetOptionString("htmls.docdesigner", "DOCDESIGNER_VARIABLES_DEAL_" . $NAME), "htmls.DocDesigner");
			if($DocVar = COption::GetOptionString("htmls.docdesigner", "DOCDESIGNER_VARIABLES_DEAL_" . $NAME)){
			  $Value = self::GetCrmParamValue($arDeal, $NAME, $val, $type);
                if($val['USER_TYPE'] == "string" && $val['MULTIPLE'] == 'Y'){
                  $list = '<ul>';
                  foreach($Value as $str){
                    $list .= '<li>' . $str . '</li>';
                    $arProperties[$DocVar][] = $str;
                  }
                  if(count($Value) == 0){
                    $arProperties[$DocVar] = '';
                  }
                  $list .= '</ul>';
                  //$arProperties[$DocVar] = $list;
                }
                elseif($val['USER_TYPE'] == "employee"){
                  $_val = explode(' ', $Value);
                  //$_valShort = $_val[0];
                  if(strtoupper(LANG_CHARSET) == 'WINDOWS-1251')
                    $shortName = $_val[0] . ' ' . substr($_val[1], 0, 1) . '. ' . substr($_val[2], 0, 1) . '.';
                  else
                    $shortName = $_val[0] . ' ' . mb_substr($_val[1], 0, 1, 'UTF-8') . '. ' . mb_substr($_val[2], 0, 1, 'UTF-8') . '.';
                  $arProperties[$DocVar.'_SHORT'] = $shortName;
                  $arProperties[$DocVar] = $Value;
                }
				elseif(is_array($Value)){
					foreach($Value as $k => $v){
						if($k == 'NAME'){
							$arProperties[$DocVar] = $v;
						}
						elseif($k == 'PROPERTIES'){
							foreach($v as $id => $ar){
								$arProperties[$DocVar.".".$id] = $ar['VALUE'];
							}
						}
						else{
							$arProperties[$DocVar.".".$k] = $v;
						}
					}
				}
                elseif($_type = $val['CRM_TYPE']){
                  if($_type == 'CONTACT'){
                    $cid = str_replace('C_', '', $arDeal[$NAME]);
                    if($cid > 0){
                        $resCont = CCrmContact::GetListEx(array(), Array('ID' => $cid), false, false, array('*', 'UF_*', 'PHONE', 'EMAIL'));//, array());
    			        while($obCont = $resCont->GetNext()){

                          if($obCont['PHONE_WORK']){}
                          else{$obCont['PHONE_WORK'] = '';}

                          if($obCont['PHONE_MOBILE']){}
                          else{$obCont['PHONE_MOBILE'] = '';}

                          if($obCont['PHONE_HOME']){}
                          else{$obCont['PHONE_HOME'] = '';}

    			          foreach($obCont as $_fName => $_fValue){
    			            $arProperties[$DocVar . '.' . $_fName] = $_fValue;
                          }

                          $arContactRequisite = self::getCrmRequisite($cid, 3);

                          if($factadr = $arContactRequisite['factadr']){
                            $arProperties[$DocVar . '.ADDRESS'] = $factadr['ADDRESS_1'];
                            $arProperties[$DocVar . '.ADDRESS_2'] = $factadr['ADDRESS_2'];
                            $arProperties[$DocVar . '.ADDRESS_CITY'] = $factadr['CITY'];
                            $arProperties[$DocVar . '.ADDRESS_REGION'] = $factadr['REGION'];
                            $arProperties[$DocVar . '.ADDRESS_PROVINCE'] = $factadr['PROVINCE'];
                            $arProperties[$DocVar . '.ADDRESS_POSTAL_CODE'] = $factadr['POSTAL_CODE'];
                            $arProperties[$DocVar . '.ADDRESS_COUNTRY'] = $factadr['COUNTRY'];
                          }
                          $arProperties[$DocVar . '.RQ_IDENT_DOC'] = $arContactRequisite['RQ_IDENT_DOC'];
                          $arProperties[$DocVar . '.RQ_IDENT_DOC_SER'] = $arContactRequisite['RQ_IDENT_DOC_SER'];
                          $arProperties[$DocVar . '.RQ_IDENT_DOC_NUM'] = $arContactRequisite['RQ_IDENT_DOC_NUM'];
                          $arProperties[$DocVar . '.RQ_IDENT_DOC_DATE'] = $arContactRequisite['RQ_IDENT_DOC_DATE'];
                          $arProperties[$DocVar . '.RQ_IDENT_DOC_ISSUED_BY'] = $arContactRequisite['RQ_IDENT_DOC_ISSUED_BY'];
                          $arProperties[$DocVar . '.RQ_IDENT_DOC_DEP_CODE'] = $arContactRequisite['RQ_IDENT_DOC_DEP_CODE'];

                          $arProperties[$DocVar] = $obCont['LAST_NAME'] . ' ' . $obCont['NAME'] . ' ' . $obCont['SECOND_NAME'];

                          $arProperties[$DocVar . '.' . 'FULL_NAME_SHORT'] = $obCont['LAST_NAME'];
                          $arProperties[$DocVar . '.' . 'FULL_FIO'] = $obCont['LAST_NAME'];
                          $arProperties[$DocVar . '.' . 'FULL_NAME'] = $obCont['LAST_NAME'];

                          if(strtoupper(LANG_CHARSET) == 'WINDOWS-1251'){
                            $arProperties[$DocVar . '.' . 'FULL_NAME_SHORT'] .= ' ' . $obCont['NAME'][0] . '.';
                            $arProperties[$DocVar . '.' . 'FULL_NAME'] .= ' ' . $obCont['NAME'];
                            if($sn = $obCont['SECOND_NAME']){
                              $arProperties[$DocVar . '.' . 'FULL_NAME_SHORT'] .= ' ' . $sn[0] . '.';
                              $arProperties[$DocVar . '.' . 'FULL_FIO'] .= ' ' . $sn;
                            }
                          }
                          else{
                            $n = iconv("UTF-8", "cp1251", $obCont['NAME']);
                            $arProperties[$DocVar . '.' . 'FULL_NAME_SHORT'] .= ' ' . iconv("cp1251", "UTF-8", $n[0]) . '.';
                            $arProperties[$DocVar . '.' . 'FULL_FIO'] .= ' ' . $obCont['NAME'];
                            $arProperties[$DocVar . '.' . 'FULL_NAME'] .= ' ' . $obCont['NAME'];
                            if($sn = $obCont['SECOND_NAME']){
                              $sn = iconv("UTF-8", "cp1251", $sn);
                              $arProperties[$DocVar . '.' . 'FULL_NAME_SHORT'] .= ' ' . iconv("cp1251", "UTF-8", $sn[0]) . '.';
                              $arProperties[$DocVar . '.' . 'FULL_FIO'] .= ' ' . $obCont['SECOND_NAME'];
                            }
                          }
                          $arProperties[$DocVar . '.' . 'FULL_FIO.RP'] = $arProperties['FULL_FIO'];
                          $arProperties[$DocVar . '.' . 'FULL_FIO_G'] = $arProperties['FULL_FIO'];
                          $arProperties[$DocVar . '.' . 'FULL_NAME_SHORT.RP'] = $arProperties['FULL_NAME_SHORT'];
                          $arProperties[$DocVar . '.' . 'FULL_NAME_SHORT_G'] = $arProperties['FULL_NAME_SHORT'];
                          $arProperties[$DocVar . '.' . 'POST_G'] = $arProperties['POST'];

                          $arContactMulti = self::GetCrmMulti($Value, 'CONTACT');
                          //AddMessage2Log($Value);
                          //AddMessage2Log(print_r($arContactMulti, 1));
                          foreach($arContactMulti as $key => $arr){
                  			$arContactMulti[str_replace('CONTACT.', $DocVar . '.', $key)] = implode(",", $arr);
                  		  }
                          $arProperties = array_merge($arProperties, $arContactMulti);
    			        }
                    }
                    else{
                      $arProperties[$DocVar] = '';
                      $arProperties[$DocVar . '.' . 'FULL_NAME'] = '';
                      $arProperties[$DocVar . '.' . 'FULL_NAME_SHORT'] = '';
                      $arProperties[$DocVar . '.' . 'FULL_FIO'] = '';
                      $arProperties[$DocVar . '.' . 'FULL_FIO.RP'] = '';
                      $arProperties[$DocVar . '.' . 'FULL_FIO_G'] = '';
                      $arProperties[$DocVar . '.' . 'FULL_NAME_SHORT.RP'] = '';
                      $arProperties[$DocVar . '.' . 'FULL_NAME_SHORT_G'] = '';
                      $arProperties[$DocVar . '.' . 'POST_G'] = '';
                      $arProperties[$DocVar . '.' . 'POST'] = '';
                      $arProperties[$DocVar . '.' . 'LAST_NAME'] = '';
                      $arProperties[$DocVar . '.' . 'PHONE_WORK'] = '';
                      $arProperties[$DocVar . '.' . 'PHONE_HOME'] = '';
                      foreach($arCrmFields['CONTACT'] as $_NAME => $_val){
                        $arProperties[str_replace('CONTACT.', $DocVar . '.', $_NAME)] = '';
                      }
                    }
                  }
                  elseif($_type == 'COMPANY'){
                    $coid = str_replace('CO_', '', $arDeal[$NAME]);
                    if($coid > 0){
                        $resCont = CCrmCompany::GetListEx(array(), Array('ID' => $coid), false, false, array('*', 'UF_*', 'PHONE', 'EMAIL'));//, array());
    			        while($obCont = $resCont->GetNext()){
    			          $arCompanyRequisite = self::getCrmRequisite($coid);
                          if($uridadr = $arCompanyRequisite['uridadr']){
                            $obCont['ADDRESS_LEGAL'] = '';
                            if($uridadr['POSTAL_CODE']){$obCont['ADDRESS_LEGAL'] .= $uridadr['POSTAL_CODE'] . ', ';}
                            if($uridadr['COUNTRY']){$obCont['ADDRESS_LEGAL'] .= $uridadr['COUNTRY'] . ', ';}
                            if($uridadr['PROVINCE']){$obCont['ADDRESS_LEGAL'] .= $uridadr['PROVINCE'] . ', ';}
                            if($uridadr['REGION']){$obCont['ADDRESS_LEGAL'] .= $uridadr['REGION'] . ', ';}
                            $obCont['ADDRESS_LEGAL'] .= $uridadr['ADDRESS_1'];
                          }

                          if($postadr = $arCompanyRequisite['factadr']){
                            $obCont['POST_ADDRESS'] = '';
                            if($postadr['POSTAL_CODE']){$obCont['POST_ADDRESS'] .= $postadr['POSTAL_CODE'] . ', ';}
                            if($postadr['COUNTRY']){$obCont['POST_ADDRESS'] .= $postadr['COUNTRY'] . ', ';}
                            if($postadr['PROVINCE']){$obCont['POST_ADDRESS'] .= $postadr['PROVINCE'] . ', ';}
                            if($postadr['REGION']){$obCont['POST_ADDRESS'] .= $postadr['REGION'] . ', ';}
                            $obCont['POST_ADDRESS'] .= $postadr['ADDRESS_1'];
                            $obCont['ADDRESS'] = $postadr['ADDRESS_1'];
                            $obCont['ADDRESS_1'] = $postadr['ADDRESS_2'];
                            $obCont['ADDRESS_POSTAL_CODE'] = $postadr['POSTAL_CODE'];
                            $obCont['ADDRESS_COUNTRY'] = $postadr['COUNTRY'];
                            $obCont['ADDRESS_PROVINCE'] = $postadr['PROVINCE'];
                            $obCont['ADDRESS_REGION'] = $postadr['REGION'];
                            $obCont['ADDRESS_CITY'] = $postadr['CITY'];
                          }
    			          foreach($obCont as $_fName => $_fValue){
    			            $arProperties[$DocVar . '.' . $_fName] = $_fValue;
                          }
                          $arProperties[$DocVar] = $obCont['TITLE'];
    			        }
                        //AddMessage2Log(print_r($obCont, 1));



                        //RQ_DIRECTOR RQ_ACCOUNTANT RQ_CEO_NAME RQ_CEO_WORK_POS
                        //RQ_BANK_ADDR
                        /*
                        if($arCompanyRequisite['RQ_COMPANY_NAME'])
                		    $arProperties['var' . $vname . '.NAME'] = $arCompanyRequisite['RQ_COMPANY_NAME'];
                        else
                		    $arProperties['var' . $vname . '.NAME'] = $COMPANY_NAME;

                        if($arCompanyRequisite['RQ_COMPANY_FULL_NAME'])
                		    $arProperties['var' . $vname . '.FULL_NAME'] = $arCompanyRequisite['RQ_COMPANY_FULL_NAME'];
                        else
                		    $arProperties['var' . $vname . '.FULL_NAME'] = $COMPANY_NAME;

                        $arProperties['var' . $vname . '.BANKING_DETAILS'] = str_replace(chr(13), '<br>', $COMPANY['BANKING_DETAILS']);

                        if($arCompanyRequisite['RQ_INN'])
                		    $arProperties['var' . $vname . '.INN'] = $arCompanyRequisite['RQ_INN'];
                        else
                		    $arProperties['var' . $vname . '.INN'] = $COMPANY[COption::GetOptionString($module_id, 'DOCDESIGNER_CRM_COMPANY_INN')];

                        if($arCompanyRequisite['RQ_KPP'])
                		    $arProperties['var' . $vname . '.KPP'] = $arCompanyRequisite['RQ_KPP'];
                        else
                		    $arProperties['var' . $vname . '.KPP'] = $COMPANY[COption::GetOptionString($module_id, 'DOCDESIGNER_CRM_COMPANY_KPP')];

                        if($arCompanyRequisite['RQ_OGRN'])
                            $arProperties['var' . $vname . '.OGRN'] = $arCompanyRequisite['RQ_OGRN'];
                        else
                            $arProperties['var' . $vname . '.OGRN'] = $COMPANY[COption::GetOptionString($module_id, 'DOCDESIGNER_CRM_COMPANY_OGRN')];

                        if($arCompanyRequisite['bank']['RQ_BANK_NAME'])
                            $arProperties['var' . $vname . '.BANK_NAME'] = $arCompanyRequisite['bank']['RQ_BANK_NAME'];
                        else
                            $arProperties['var' . $vname . '.BANK_NAME'] = $COMPANY[COption::GetOptionString($module_id, 'DOCDESIGNER_CRM_COMPANY_BANK_NAME')];

                        if($arCompanyRequisite['bank']['RQ_BIK'])
                		    $arProperties['var' . $vname . '.BANK_BIK'] = $arCompanyRequisite['bank']['RQ_BIK'];
                        else
                		    $arProperties['var' . $vname . '.BANK_BIK'] = $COMPANY[COption::GetOptionString($module_id, 'DOCDESIGNER_CRM_COMPANY_BANK_BIK')];

                        if($arCompanyRequisite['bank']['RQ_ACC_NUM'])
                            $arProperties['var' . $vname . '.BANK_RS'] = $arCompanyRequisite['bank']['RQ_ACC_NUM'];
                        else
                            $arProperties['var' . $vname . '.BANK_RS'] = $COMPANY[COption::GetOptionString($module_id, 'DOCDESIGNER_CRM_COMPANY_RS')];

                        if($arCompanyRequisite['bank']['RQ_COR_ACC_NUM'])
                		    $arProperties['var' . $vname . '.BANK_KS'] = $arCompanyRequisite['bank']['RQ_COR_ACC_NUM'];
                        else
                		    $arProperties['var' . $vname . '.BANK_KS'] = $COMPANY[COption::GetOptionString($module_id, 'DOCDESIGNER_CRM_COMPANY_BANK_KS')];

                        if($arCompanyRequisite['bank']['RQ_BANK_ADDR'])
                		    $arProperties['var' . $vname . '.BANK_CITY'] = $arCompanyRequisite['bank']['RQ_BANK_ADDR'];
                        else
                		    $arProperties['var' . $vname . '.BANK_CITY'] = $COMPANY[COption::GetOptionString($module_id, 'DOCDESIGNER_CRM_COMPANY_BANK_CITY')];

                		$arProperties['var' . $vname . '.ADDRESS_LEGAL'] = $COMPANY['ADDRESS_LEGAL'];
                		$arProperties['var' . $vname . '.POST_ADDRESS'] = $COMPANY['POST_ADDRESS'];
                		$arProperties['var' . $vname . '.ADDRESS'] = $COMPANY['ADDRESS'];
                        */
                    }
                    else{
                      $arProperties[$DocVar] = '';
                      foreach($arCrmFields['COMPANY'] as $_NAME => $_val){
                        $arProperties[str_replace('COMPANY.', $DocVar . '.', $_NAME)] = '';
                        $_name = str_replace('COMPANY.', '', $_NAME);
                        $_name = $DocVar . '.' . $_NAME;
                        $arProperties[$_name] = '';
                      }
                    }
                  }
                }
				else
					$arProperties[$DocVar] = $Value;
				$arCrmFields['DEAL'][$NAME]['VAR_NAME'] = $DocVar;
			}
			else{
				if($v = $arDeal[$NAME])
					$v = $arDeal[$NAME];
				else
					$v = $arDeal[str_replace('DEAL.', '', $NAME)];

				if(is_array($v)){
					$v = implode(", ", $v);
				}
				else{
					if(strpos($NAME, '_DATE') > 0 or substr($NAME, 0, 5) == 'DATE_' or substr($NAME, strlen($NAME)-4) == 'DATE'){
						$site_format = CSite::GetDateFormat();
						$arr = ParseDateTime($v, $site_format);
                        $v1 = $v;
						$stmp = strtotime($v);//mktime($arr["HH"], $arr["MI"], $arr["SS"], $arr["MM"], $arr["DD"], $arr["YYYY"]);
						$v = FormatDate("d F Y", $stmp);
					}
				}
				$arProperties[$NAME] = $v;//$arDeal[$NAME];
			}
		}
        //AddMessage2Log(print_r($arCrmFields['COMPANY'], true));
		foreach($arCrmFields['COMPANY'] as $NAME => $val){
			if($DocVar = COption::GetOptionString("htmls.docdesigner", "DOCDESIGNER_VARIABLES_COMPANY_" . $NAME)){
				//$arProperties[$DocVar] = self::GetCrmParamValue($arCompany, $NAME, $val, $type);
				$Value = self::GetCrmParamValue($arCompany, $NAME, $val, $type);
				if(is_array($Value)){
					foreach($Value as $k => $v){
						if($k == 'NAME'){
							$arProperties[$DocVar] = $v;
						}
						elseif($k == 'PROPERTIES'){
							foreach($v as $id => $ar){
								$arProperties[$DocVar.".".$id] = $ar['VALUE'];
							}
						}
						else{
							$arProperties[$DocVar.".".$k] = $v;
    					}
					}
				}
				else{
					$arProperties[$DocVar] = $Value;
                   if($val['USER_TYPE'] == 'string'){
                      $CDocDes = new CDocDesignerProcessing();
                      $arDecl = $CDocDes->getDeclination($Value);
                      $arProperties[$DocVar . '_G'] = $arDecl[1];
                      $arProperties[$DocVar . '.RP'] = $arDecl[1];
                    }
                }
				$arCrmFields['COMPANY'][$NAME]['VAR_NAME'] = $DocVar;
			}
			else{
				if($arCompany[$NAME])
					$v = $arCompany[$NAME];
				else
					$v = $arCompany[str_replace('COMPANY.', '', $NAME)];

				if(is_array($v)){
					$v = implode(", ", $v);
				}
				$arProperties[$NAME] = $v;//$arCompany[$NAME];
			}
		}
        //AddMessage2Log(print_r($arCrmFields['CONTACT'], true));
        //AddMessage2Log(print_r($arContact, true));
		foreach($arCrmFields['CONTACT'] as $NAME => $val){
			if($DocVar = COption::GetOptionString("htmls.docdesigner", "DOCDESIGNER_VARIABLES_CONTACT_" . $NAME)){
				//$arProperties[$DocVar] = self::GetCrmParamValue($arContact, $NAME, $val, $type);
				$Value = self::GetCrmParamValue($arContact, $NAME, $val, $type);
				if(is_array($Value)){
					foreach($Value as $k => $v){
						if($k == 'NAME'){
							$arProperties[$DocVar] = $v;
						}
						elseif($k == 'PROPERTIES'){
							foreach($v as $id => $ar){
								$arProperties[$DocVar.".".$id] = $ar['VALUE'];
							}
						}
						else{
							$arProperties[$DocVar.".".$k] = $v;
						}
					}
				}
				else
					$arProperties[$DocVar] = $Value;
				$arCrmFields['CONTACT'][$NAME]['VAR_NAME'] = $DocVar;
			}
			else{
			  if($NAME == 'CONTACT.FULL_FIO' || $NAME == 'CONTACT.FULL_NAME' || $NAME == 'CONTACT.FULL_NAME_SHORT' || $NAME == 'CONTACT.POST'){
			    $CDocDes = new CDocDesignerProcessing();
                $arDecl = $CDocDes->getDeclination($arContact[str_replace('CONTACT.', '', $NAME)]);
                //AddMessage2Log($arContact[$NAME]);
                //AddMessage2Log(print_r($arDecl, true));
                $arProperties[$NAME . '_G'] = $arDecl[1];
                $arProperties[$NAME . '.RP'] = $arDecl[1];
			  }
				if($arContact[$NAME])
					$v = $arContact[$NAME];
				else
					$v = $arContact[str_replace('CONTACT.', '', $NAME)];

				if(is_array($v)){
					$v = implode(", ", $v);
				}
				$arProperties[$NAME] = $v;//$arContact[$NAME];
			}
		}
        //AddMessage2Log(print_r($arContact, true));
        //AddMessage2Log(print_r($arCrmFields['LEAD'], true));
        //AddMessage2Log(print_r($arDeal, true));
        //LEAD: arDeal = obLead
        foreach($arCrmFields['LEAD'] as $NAME => $val){
			if($DocVar = COption::GetOptionString("htmls.docdesigner", "DOCDESIGNER_VARIABLES_LEAD_" . $NAME)){
				$Value = self::GetCrmParamValue($arDeal, $NAME, $val, $type);
				if(is_array($Value)){
					foreach($Value as $k => $v){
						if($k == 'NAME'){
							$arProperties[$DocVar] = $v;
						}
						elseif($k == 'PROPERTIES'){
							foreach($v as $id => $ar){
								$arProperties[$DocVar.".".$id] = $ar['VALUE'];
							}
						}
						else{
							$arProperties[$DocVar.".".$k] = $v;
						}
					}
				}
                elseif($_type = $val['CRM_TYPE']){
                  if($_type == 'CONTACT'){
                    $resCont = CCrmContact::GetListEx(array(), Array('ID' => $Value), false, false, array('*', 'UF_*', 'PHONE', 'EMAIL'));//, array());
			        while($obCont = $resCont->GetNext()){
			          foreach($obCont as $_fName => $_fValue){
			            $arProperties[$DocVar . '.' . $_fName] = $_fValue;
                      }
                      $arProperties[$DocVar] = $obCont['LAST_NAME'] . ' ' . $obCont['NAME'] . ' ' . $obCont['SECOND_NAME'];

                      $arProperties[$DocVar . '.' . 'FULL_NAME_SHORT'] = $obCont['LAST_NAME'];
                      $arProperties[$DocVar . '.' . 'FULL_FIO'] = $obCont['LAST_NAME'];

                      if(strtoupper(LANG_CHARSET) == 'WINDOWS-1251'){
                        $arProperties[$DocVar . '.' . 'FULL_NAME_SHORT'] .= ' ' . $obCont['NAME'][0] . '.';
                        $arProperties[$DocVar . '.' . 'FULL_FIO'] .= ' ' . $obCont['NAME'];
                        if($sn = $obCont['SECOND_NAME']){
                          $arProperties[$DocVar . '.' . 'FULL_NAME_SHORT'] .= ' ' . $sn[0] . '.';
                          $arProperties[$DocVar . '.' . 'FULL_FIO'] .= ' ' . $sn;
                        }
                      }
                      else{
                        $n = iconv("UTF-8", "cp1251", $obCont['NAME']);
                        $arProperties[$DocVar . '.' . 'FULL_NAME_SHORT'] .= ' ' . iconv("cp1251", "UTF-8", $n[0]) . '.';
                        $arProperties[$DocVar . '.' . 'FULL_FIO'] .= ' ' . $obCont['NAME'];
                        if($sn = $obCont['SECOND_NAME']){
                          $sn = iconv("UTF-8", "cp1251", $sn);
                          $arProperties[$DocVar . '.' . 'FULL_NAME_SHORT'] .= ' ' . iconv("cp1251", "UTF-8", $sn[0]) . '.';
                          $arProperties[$DocVar . '.' . 'FULL_FIO'] .= ' ' . $obCont['SECOND_NAME'];
                        }
                      }
                      $arProperties[$DocVar . '.' . 'FULL_FIO.RP'] = $arProperties['FULL_FIO'];
                      $arProperties[$DocVar . '.' . 'FULL_FIO_G'] = $arProperties['FULL_FIO'];
                      $arProperties[$DocVar . '.' . 'FULL_NAME_SHORT.RP'] = $arProperties['FULL_NAME_SHORT'];
                      $arProperties[$DocVar . '.' . 'FULL_NAME_SHORT_G'] = $arProperties['FULL_NAME_SHORT'];
                      $arProperties[$DocVar . '.' . 'POST_G'] = $arProperties['POST'];

			        }
                  }
                  elseif($_type == 'COMPANY'){
                    $resCont = CCrmCompany::GetListEx(array(), Array('ID' => $Value), false, false, array('*', 'UF_*', 'PHONE', 'EMAIL'));//, array());
			        while($obCont = $resCont->GetNext()){
			          foreach($obCont as $_fName => $_fValue){
			            $arProperties[$DocVar . '.' . $_fName] = $_fValue;
                      }

			        }
                  }
                }
				else
					$arProperties[$DocVar] = $Value;
				$arCrmFields['LEAD'][$NAME]['VAR_NAME'] = $DocVar;
			}
			else{
				if($v = $arDeal[$NAME])
					$v = $arDeal[$NAME];
				else
					$v = $arDeal[str_replace('LEAD.', '', $NAME)];

				if(is_array($v)){
					$v = implode(", ", $v);
				}
				else{
					if(strpos($NAME, '_DATE') > 0 or substr($NAME, 0, 5) == 'DATE_' or substr($NAME, strlen($NAME)-4) == 'DATE'){
						$site_format = CSite::GetDateFormat();
						$arr = ParseDateTime($v, $site_format);
                        $v1 = $v;
						$stmp = strtotime($v);//mktime($arr["HH"], $arr["MI"], $arr["SS"], $arr["MM"], $arr["DD"], $arr["YYYY"]);
						$v = FormatDate("d F Y", $stmp);
					}
				}
				//$arProperties['LEAD.'.$NAME] = $v;
				$arProperties[$NAME] = $v;
			}
		}
        //AddMessage2Log(print_r($arProperties, true));
        return $arProperties;
	}//SetCrmProperties

    //get param value by type
	function GetCrmParamValue($arCRM, $NAME, $val, $type){
		global $MESS;
		if($val['USER_TYPE'] == "iblock_element"){
			$IBLOCK_ID = $val['IBLOCK_ID'];
			if(intval($arCRM[$NAME]) == 0){
				$properties = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>$IBLOCK_ID));
				while ($prop_fields = $properties->GetNext()){
					$arProps[$prop_fields["ID"]] = array('VALUE'=>"");
					$arProps[$prop_fields["CODE"]] = array('VALUE'=>"");
				}
				$arFields['NAME'] = '';
				$arFields['PROPERTIES'] = $arProps;
			}
            //$res = CIBlockElement::GetByID($arCRM[$NAME]);
			//if($ar_res = $res->GetNext())
			else{
				$arSelect = Array("ID", "NAME", "PREVIEW_TEXT", "DETAIL_TEXT", "PROPERTY_*");
				$arFilter = Array("ID"=>IntVal($arCRM[$NAME]), "IBLOCK_ID"=>$IBLOCK_ID);
				$res = CIBlockElement::GetList(Array(), $arFilter);//, false, false, $arSelect);
				while($ob = $res->GetNextElement()){
					$arFields = $ob->GetFields();
					$arProps = $ob->GetProperties();
				}
				$arFields['PROPERTIES'] = $arProps;
			}
			if($arFields['PREVIEW_PICTURE'] > 0){
				$path = CFile::GetPath($arFields["PREVIEW_PICTURE"]);
				if(!strpos($path, '://')){
					$path = ".." . $path;
				}
				$arFields['PREVIEW_PICTURE'] = '<img src="'.$path.'" border="0" />';
			}
			if($arFields['DETAIL_PICTURE'] > 0){
				$path = CFile::GetPath($arFields["DETAIL_PICTURE"]);
				if(!strpos($path, '://')){
					$path = ".." . $path;
				}
				$arFields['DETAIL_PICTURE'] = '<img src="'.$path.'" border="0" />';
    		}
            return $arFields;

			$res = CIBlockElement::GetByID($arCRM[$NAME]);
			if($ar_res = $res->GetNext())
				return $ar_res['NAME'];
		}
		if($val['USER_TYPE'] == "enumeration"){
			if(is_array($arCRM[$NAME])){
				if(count($arCRM[$NAME]) == 0) return "";
			}
			else{
				if(intval($arCRM[$NAME]) == 0) return "";
			}
			$res = CUserFieldEnum::GetList(array(), array("ID" => $arCRM[$NAME]));
			if($res->SelectedRowsCount() == 1){
				if($ar_res = $res->GetNext()){
					return $ar_res['VALUE'];
				}
			}
			else{
				$value = "<ul>";
				$arValue = array();
				while($ar_res = $res->Fetch()){
					$value .= "<li>".$ar_res['VALUE']."</li>";
					$arValue[] = $ar_res['VALUE'];
				}
				$value .= "</ul>";
				if($type == 'PDF')
					return $value;
				else
					return implode(",", $arValue);
			}
		}
		if($val['USER_TYPE'] == "boolean"){
			if($arCRM[$NAME] == 1)
				return GetMessage("boolean1");
			else
				return GetMessage("boolean0");
		}
		if($val['USER_TYPE'] == "crm_status"){
			$res = CCrmStatus::GetList(array(), $arFilter=Array('ENTITY_ID'=>$val['ENTITY_TYPE'], 'STATUS_ID'=>$arCRM[$NAME]));
			if($ar_res = $res->GetNext())
				return $ar_res['NAME'];
		}
        if($val['USER_TYPE'] == "employee"){
			$rsUser = CUser::GetByID($arCRM[$NAME]);
            $arUser = $rsUser->Fetch();
			return $arUser['LAST_NAME'] . ' ' . $arUser['NAME'] . ' ' . $arUser['SECOND_NAME'];
		}
        //AddMessage2Log($NAME.'='.$arCRM[$NAME], "htmls.DocDesigner");
        //AddMessage2Log(print_r($val, true), "htmls.DocDesigner");
        if($val['USER_TYPE'] == "datetime"){
			$site_format = CSite::GetDateFormat();
            $stmp = strtotime($arCRM[$NAME]);
            $v = FormatDate("d F Y", $stmp);
            //AddMessage2Log($NAME.'='.$stmp, "htmls.DocDesigner");
            //AddMessage2Log($NAME.'='.$v, "htmls.DocDesigner");
			return $v;
		}
        if($val['USER_TYPE'] == "string" && $val['MULTIPLE'] == 'Y'){
            //AddMessage2Log(print_r($arCRM[$NAME], true), "htmls.DocDesigner");
        }
        if($val['USER_TYPE'] == "crm"){
          if($val['SETTINGS']['COMPANY'] == 'Y'){
            $res = CCrmCompany::GetListEx(array(),array('ID' => $arCRM[$NAME]), false, false, array('*', 'UF_*', 'PHONE', 'EMAIL'));//,array());
    		while($ob = $res->GetNext()){
    			$COMPANY_NAME = $ob['TITLE'];
                return $COMPANY_NAME;
    			//$COMPANY = $ob;
                //if($ResponsibleID == 0)
                //    $ResponsibleID = $ob['ASSIGNED_BY_ID'];
    		}
          }
          elseif($val['SETTINGS']['DEAL'] == 'Y'){
            $res = CCrmDeal::GetList(array(),array('ID' => $arCRM[$NAME]),array());
    		while($ob = $res->Fetch()){
    			$DEAL_NAME = $ob['TITLE'];
                return $DEAL_NAME;
    			//$COMPANY = $ob;
                //if($ResponsibleID == 0)
                //    $ResponsibleID = $ob['ASSIGNED_BY_ID'];
    		}
          }
          elseif($val['SETTINGS']['LEAD'] == 'Y'){
            $res = CCrmLead::GetList(array(),array('ID' => $arCRM[$NAME]),array());
    		while($ob = $res->Fetch()){
    			$LEAD_NAME = $ob['TITLE'];
                return $LEAD_NAME;
    			//$COMPANY = $ob;
                //if($ResponsibleID == 0)
                //    $ResponsibleID = $ob['ASSIGNED_BY_ID'];
    		}
          }
          elseif($val['SETTINGS']['CONTACT'] == 'Y'){
            if($arCRM[$NAME] > 0){
                $res = CCrmContact::GetListEx(array(),array('ID' => $arCRM[$NAME]), false, false, array('*', 'UF_*', 'PHONE', 'EMAIL'));//,array());
        		while($ob = $res->GetNext()){
        			$CONTACT_NAME = $ob['LAST_NAME'] . '  ' . $ob['NAME'];
                    return $CONTACT_NAME;
        			//$COMPANY = $ob;
                    //if($ResponsibleID == 0)
                    //    $ResponsibleID = $ob['ASSIGNED_BY_ID'];
        		}
            }
            else{
              return 0;
            }
          }
        }



		return $arCRM[$NAME];
	}//GetCrmParamValue

	function GetCrmMulti($ID, $ENTITY){
		$res = array();
        if(intval($ID) == 0) return $res;
		$multi = CCrmFieldMulti::GetList($arSort=array(), $arFilter=array('ELEMENT_ID'=>$ID, 'ENTITY_ID' => $ENTITY));
		while($row = $multi->Fetch()){
			$res[$ENTITY.".".$row['COMPLEX_ID']][]=$row['VALUE'];
		}
		return $res;
	}
}
//end class CDocDesignerBPActivities
?>