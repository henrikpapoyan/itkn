<?
/*******************************************************************************
* Module:   DocDesigner                                                        *
* Class:    CDocDesignerCRM extends CDocDesigner                         	   *
* Version:  8.4.2                                                              *
* Author:   Alexey Andreev, HTMLStudio (www.htmls.ru)                          *
* License:  Shareware                                                          *
* Date:     2016-07-05                                                         *
* e-mail:   aav@htmls.ru                                                       *
*******************************************************************************/
if(COption::GetOptionString('htmls.docdesigner', 'DOCDESIGNER_ADDMESSAGE2LOG') == 'Y'){
	define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/bitrix/tmp/DocDesigner.txt");
}
IncludeModuleLangFile(__FILE__);
CModule::IncludeModule("sale");
CModule::IncludeModule("crm");
CModule::IncludeModule("iblock");
class CDocDesignerCRM extends CDocDesigner{
	function CDocDesignerCRM(){
    }

	function CreateInvoice($rootActivity, $arProperties){
	    global $USER;

        if(!$arProperties['DocDesignerCompany']) return;
        $corp = $arProperties['DocDesignerCompany'];

	    $DocumentType = $rootActivity->GetDocumentType();
		$DocumentId = $rootActivity->GetDocumentId();
		if($DocumentType[2] == 'DEAL'){
			$OID = substr($DocumentId[2], strlen("DEAL_"));
			$Type = 'D';
			$res = CCrmDeal::GetList(array(),array('ID' => $OID),array());
			while($ob = $res->Fetch()){
				$COMPANY_ID = $ob['COMPANY_ID'];
				$CONTACT_ID = $ob['CONTACT_ID'];
				$arDeal = $ob;
			}

			$res = CCrmCompany::GetList(array(),array('ID' => $COMPANY_ID),array());
			while($ob = $res->Fetch()){
				$COMPANY_NAME = $ob['TITLE'];
				$arCompany = $ob;
				$arProps = array('COMPANY' => $COMPANY_NAME,
                                'COMPANY_ADR' => $arCompany['ADDRESS_LEGAL'],
                                'INN' => $arCompany[COption::GetOptionString('htmls.docdesigner', 'DOCDESIGNER_CRM_COMPANY_INN')],
                                'KPP' => $arCompany[COption::GetOptionString('htmls.docdesigner', 'DOCDESIGNER_CRM_COMPANY_KPP')]);
			}
			$SaleType = 'CRM_COMPANY';
            $paySys = COption::GetOptionString('htmls.docdesigner', 'DOCDESIGNER_CRM_INVOCE_COMPANY_'.$corp, 0);
		}
		elseif($DocumentType[2] == 'LEAD'){
			$OID = substr($DocumentId[2], strlen("LEAD_"));
			$Type = 'L';

			$SaleType = 'CRM_CONTACT';
            $paySys = COption::GetOptionString('htmls.docdesigner', 'DOCDESIGNER_CRM_INVOCE_CONTACT_'.$corp, 0);
		}
		else
			return;
        if($paySys * 1 == 0) return;
        // user id for order
		//$saleUserId = intval(CSaleUser::GetAnonymousUserID());

        /*
        $personTypeCompany => array(
				'COMPANY' => GetMessage('CRM_INVOICE_PROPERTY_COMPANY_TITLE'),
				'COMPANY_ADR' => GetMessage('CRM_INVOICE_PROPERTY_COMPANY_ADR'),
				'CONTACT_PERSON' => GetMessage('CRM_INVOICE_PROPERTY_COMPANY_CONTACT_PERSON'),
				'EMAIL' => GetMessage('CRM_INVOICE_PROPERTY_COMPANY_EMAIL'),
				'PHONE' => GetMessage('CRM_INVOICE_PROPERTY_COMPANY_PHONE'),
				'INN' => GetMessage('CRM_INVOICE_PROPERTY_COMPANY_INN'),
				'KPP' => GetMessage('CRM_INVOICE_PROPERTY_COMPANY_KPP')
			),
			$personTypeContact => array(
				'FIO' => GetMessage('CRM_INVOICE_PROPERTY_CONTACT_FIO'),
				'ADDRESS' => GetMessage('CRM_INVOICE_PROPERTY_CONTACT_ADDRESS'),
				'EMAIL' => GetMessage('CRM_INVOICE_PROPERTY_CONTACT_EMAIL'),
				'PHONE' => GetMessage('CRM_INVOICE_PROPERTY_CONTACT_PHONE')
			)
        */
		$res = CCrmProductRow::GetList(array(), $arFilter = array('OWNER_ID' => $OID, 'OWNER_TYPE' => $Type), false, false, array());
		$total = 0;
        $arProductRow = array();
		while($arRow = $res->Fetch()){
		    $IB = CIBlockElement::GetByID($_GET["PID"]);
			$arElem = $IB->GetNext();

			$arFields = array(
                "ID" => 0,
				"PRODUCT_ID" => $arRow['PRODUCT_ID'],
                'PRODUCT_NAME' => $arRow['ORIGINAL_PRODUCT_NAME'],
				//"PRODUCT_PRICE_ID" => 1,
				"PRICE" => $arRow['PRICE'],
                "PRICE_ACCOUNT" => $arRow['PRICE'],
                "PRICE_EXCLUSIVE" => $arRow['PRICE'],
				//"CURRENCY" => "RUB",
				"QUANTITY" => $arRow['QUANTITY'],
				//"LID" => SITE_ID,
				//"DELAY" => "N",
				//"CAN_BUY" => "Y",
                'VAT_RATE' => $arRow['TAX_RATE']/100,
                'VAT_INCLUDED' => $arRow['TAX_INCLUDED'],
				"NAME" => $arRow['PRODUCT_NAME'],
                'DISCOUNT_PRICE' => $arRow['DISCOUNT_SUM'],
                'MEASURE_CODE' => 796,
                'MEASURE_NAME' => '��',
                'CUSTOMIZED' => 'Y'
				//"MODULE" => "catalog",
			);
			//CSaleBasket::Add($arFields);
            $arProductRow[] = $arFields;
			$total = $total + $arRow['PRICE'] * $arRow['QUANTITY'];
		}

		$db_ptype = CSalePersonType::GetList(Array("SORT" => "ASC"), Array("LID"=>SITE_ID, "NAME"=>$SaleType));
		while ($ptype = $db_ptype->Fetch()){
			$PERSON_TYPE_ID = $ptype["ID"];
		}
        /*
		$db_ptype = CSalePaySystem::GetList($arOrder = Array("SORT"=>"ASC", "PSA_NAME"=>"ASC"), Array("LID"=>SITE_ID, "PERSON_TYPE_ID"=>$PERSON_TYPE_ID));
		while ($ptype = $db_ptype->Fetch()){
			$PAY_SYSTEM_ID = $ptype["ID"];
		}
        */
        $db_props = CSaleOrderProps::GetList(
          array("SORT" => "ASC"), array("PERSON_TYPE_ID" => $PERSON_TYPE_ID),
          false, false, array());

        $props = array();
		while($arRow = $db_props->Fetch()){
            $props[$arRow['CODE']] = $arRow['ID'];
		}


		$arFields = array(
			//"LID" => SITE_ID,
			"PERSON_TYPE_ID" => $PERSON_TYPE_ID,
			"PAYED" => "N",
			"CANCELED" => "N",
			"STATUS_ID" => "N",
			"PRICE" => $total,
			"CURRENCY" => "RUB",
			//"USER_ID" => $saleUserId,
			"PAY_SYSTEM_ID" => $paySys,//$PAY_SYSTEM_ID,
            'RESPONSIBLE_ID' => $USER->GetID()
            );
		$arFields['UF_DEAL_ID'] = $OID;
		$arFields['UF_COMPANY_ID'] = $COMPANY_ID;
		$arFields['UF_CONTACT_ID'] = $CONTACT_ID;
		$arFields['ORDER_TOPIC'] = $arDeal['TITLE'];
		$arFields['ACCOUNT_NUMBER'] = $rootActivity->GetVariable('DocNumber');
        $arFields['PRODUCT_ROWS'] = $arProductRow;
        $arFields['INVOICE_PROPERTIES'] = array();
        foreach($props as $code => $pid){
            $arFields['INVOICE_PROPERTIES'][$pid] = $arProps[$code];
        }
        //$ORDER_ID = CSaleOrder::Add($arFields);
		//CSaleBasket::OrderBasket($ORDER_ID);
        //AddMessage2Log(print_r($arFields, 1));
		//$arFields["ACCOUNT_NUMBER"] => $rootActivity->GetVariable('DocNumber');
        $crm = new CCrmInvoice();
        $ORDER_ID = $crm->Add($arFields);
        //AddMessage2Log($ORDER_ID);
        $arFields = array();
        $arFields['ACCOUNT_NUMBER'] = $rootActivity->GetVariable('DocNumber');
		$crm->Update($ORDER_ID, $arFields);
        /*
		$db_props = CSaleOrderProps::GetList(array("SORT" => "ASC"),
        array("PERSON_TYPE_ID" => $PERSON_TYPE_ID), false, false, array());
		while($props = $db_props->Fetch()){
    		if($arProps[$props['CODE']]){
    			$arFields = array(
    					"ORDER_ID" => $ORDER_ID,
    					"ORDER_PROPS_ID" => $props['ID'],
    					"NAME" => $props['NAME'],
    					"CODE" => $props['CODE'],
    					"VALUE" => $arProps[$props['CODE']]
    				);
    				CSaleOrderPropsValue::Add($arFields);
    		}

	    }*/
    }

}//CDocDesignerCRM
?>