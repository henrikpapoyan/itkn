<?
class CNum2Str{
	var $w_1_2;
	var $w_1_19;
	var $des;
	var $hang;
	var $namerub;
	var $nametho;
	var $namemil;
	var $namemrd;
	var $kopeek;

	function CNum2Str(){
		global $MESS;
		$this->w_1_2 = GetMessage('_w_1_2');
		$this->w_1_19 = GetMessage('_w_1_19');
		$this->des = GetMessage('_des');
		$this->hang = GetMessage('_hang');
		$this->namerub = GetMessage('_namerub');
		$this->nametho = GetMessage('_nametho');
		$this->namemil = GetMessage('_namemil');
		$this->namemrd = GetMessage('_namemrd');
		$this->kopeek = GetMessage('_kopeek');
        $this->DigitCol = (COption::GetOptionString('htmls.docdesigner', 'DOCDESIGNER_REMOVE_PENNY', 'N') == 'Y')? 0 : 2;
	}

	function semantic($i,&$words,&$fem,$f){
	    $words="";
	    $fl=0;
	    if($i >= 100){
	        $jkl = intval($i / 100);
	        $words.=$this->hang[$jkl];
	        $i%=100;
	    }
	    if($i >= 20){
	        $jkl = intval($i / 10);
	        $words.=$this->des[$jkl];
	        $i%=10;
	        $fl=1;
	    }
	    switch($i){
	        case 1: $fem=1; break;
	        case 2:
	        case 3:
	        case 4: $fem=2; break;
	        default: $fem=3; break;
	    }
	    if( $i ){
	        if( $i < 3 && $f > 0 ){
	            if ( $f >= 2 ) {
	                $words.=$this->w_1_19[$i];
	            }
	            else {
	                $words.=$this->w_1_2[$i];
	            }
	        }
	        else {
	            $words.=$this->w_1_19[$i];
	        }
	    }
	}

	public function num2str($L){
	    $s=" ";
	    $s1=" ";
	    $s2=" ";
	    $kop=intval( ( $L*100 - intval( $L )*100 ));
	    $L=intval($L);
	    if($L>=1000000000){
	        $many=0;
	        $this->semantic(intval($L / 1000000000),$s1,$many,3);
	        $s.=$s1.$this->namemrd[$many];
	        $L%=1000000000;
	    }
	    if($L >= 1000000){
	        $many=0;
	        $this->semantic(intval($L / 1000000),$s1,$many,2);
	        $s.=$s1.$this->namemil[$many];
	        $L%=1000000;
	        if($L==0){
	            $s.=$this->namerub[3];
	        }
	    }
	    if($L >= 1000){
	        $many=0;

	        $this->semantic(intval($L / 1000),$s1,$many,1);
	        $s.=$s1.$this->nametho[$many];
	        $L%=1000;
	        if($L==0){
	            $s.=$this->namerub[3];
	        }
	    }
	    if($L != 0){
	        $many=0;
	        $this->semantic($L,$s1,$many,0);
	        $s.=$s1.$this->namerub[$many];
	    }
        if($this->DigitCol > 0){
    	    if($kop > 0){
    	        $many=0;
    	        $this->semantic($kop,$s1,$many,1);
    	        $s.=$s1.$this->kopeek[$many];
    	    }
    	    else {
    	        $s.=" 00 ".$this->kopeek[3];
    	    }
        }
	    return $s;
	}

    public function num2spell($L){
	    $s=" ";
	    $s1=" ";
	    $s2=" ";
	    $kop=intval( ( $L*100 - intval( $L )*100 ));
	    $L=intval($L);
	    if($L>=1000000000){
	        $many=0;
	        $this->semantic(intval($L / 1000000000),$s1,$many,3);
	        $s.=$s1.$this->namemrd[$many];
	        $L%=1000000000;
	    }
	    if($L >= 1000000){
	        $many=0;
	        $this->semantic(intval($L / 1000000),$s1,$many,2);
	        $s.=$s1.$this->namemil[$many];
	        $L%=1000000;
	        /*if($L==0){
	            $s.=$this->namerub[3];
	        }*/
	    }
	    if($L >= 1000){
	        $many=0;

	        $this->semantic(intval($L / 1000),$s1,$many,1);
	        $s.=$s1.$this->nametho[$many];
	        $L%=1000;
	        /*if($L==0){
	            $s.=$this->namerub[3];
	        }*/
	    }
	    if($L != 0){
	        $many=0;
	        $this->semantic($L,$s1,$many,0);
	        $s.=$s1;//.$this->namerub[$many];
	    }
        /*
        if($this->DigitCol > 0){
    	    if($kop > 0){
    	        $many=0;
    	        $this->semantic($kop,$s1,$many,1);
    	        $s.=$s1.$this->kopeek[$many];
    	    }
    	    else {
    	        $s.=" 00 ".$this->kopeek[3];
    	    }
        }
        */
	    return $s;
	}
}
?>