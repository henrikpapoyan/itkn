<?
/*******************************************************************************
* Module:   DocDesigner                                                        *
* Class:    cPDF extends TCPDF                                                 *
* Version:  8.1.3                                                              *
* Author:   Alexey Andreev, HTMLStudio (www.htmls.ru)                          *
* License:  Shareware                                                          *
* Date:     2015-12-15                                                         *
* e-mail:   aav@htmls.ru                                                       *
*******************************************************************************/
class cPDF extends TCPDF{
    public $background;
    public $footer = array();
    public $footerValue = array();
    public $font;
    public $compositeFacsimile;
    //Page header
    public function Header() {
        // get the current page break margin
        $bMargin = $this->getBreakMargin();
        // get current auto-page-break mode
        $auto_page_break = $this->getAutoPageBreak();
        // disable auto-page-break
        $this->SetAutoPageBreak(false, 0);
        // set bacground image
        $img_file = $this->background;
        $this->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
        // restore auto-page-break status
        $this->SetAutoPageBreak($auto_page_break, $bMargin);
        // set the starting point for the page content
        $this->setPageMark();
    }
    // Page footer
    public function Footer() {
        //global $footer, $font, $compositeFacsimile;
        //mail('7976607@gmail.com', 'docx', print_r($compositeFacsimile, true));
        // Position at 15 mm from bottom
        //AddMessage2Log(print_r($this->footer, true), __LINE__);
        // Set font
        if($this->footer['ffooter'] > 0)
            $this->SetFont($this->font, '', $this->footer['ffooter']);
        else
            $this->SetFont($this->font, '', 8);
        $ceo_sign = '';
        $k = (190 / 540);
        $img = false;
        if(strlen($this->footerValue['initiallingOfPages']) > 5){
		  //if(intval($a['CEO_SIGN']) > 0){
		//    $this->SetY($currPosDir);
		    //$path = $this->compositeFacsimile['imgs']['CEO_SIGN'];
            $img = $this->footerValue['initiallingOfPages'];
            //$img = CFile::GetPath($this->footerValue['initiallingOfPages']);//$path;//$_SERVER['DOCUMENT_ROOT'] . $arFile['SRC'];
            if(!strpos($img, '://')){
					$img = '..' . $img;
			}
            //AddMessage2Log($img, __LINE__);
            $size = getimagesize($img);
            $w = $size[0];
            $h = $size[1];
            if($h > 50){
              $w = intval($w * 50 / $h);
              $h = 50;

            }
            if($w > 120){
              $h = intval($h * 120 / $w);
              $w = 120;
            }
            //mail('7976607@gmail.com', 'w='.$w.',h='.$h, print_r($size, true));
            $ceo_sign = array($img, '', '', $w * $k, $h * $k);

            //$params = urlencode(serialize($ceo_sign));
            $params = ($this->serializeTCPDFtagParameters($ceo_sign));
            $ceo_sign = '<tcpdf method="Image" params="'.$params.'" />';
            $this->SetY(-20);
            if(strpos($this->footer['left'], 'initiallingOfPages') > 0)
                $this->writeHTML($ceo_sign, true, 0, true, 0);
            if(strpos($this->footer['middle'], 'initiallingOfPages') > 0){
                //$this->SetX(60);$this->SetY(-20);
                //$this->writeHTML($ceo_sign, true, 0, true, 0);
                //$this->Image($img, 60, $this->GetY() - $h * $k, $w * $k, $h * $k);
            }
            if(strpos($this->footer['right'], 'initiallingOfPages') > 0){
              $this->SetY(-20);
                $this->SetX(150);//$this->SetY(-$h);
                $this->writeHTML($ceo_sign, true, 0, true, 0);
            }
		}


        $this->SetY(-15);
        $left = $this->footer['left'];
        $left = str_replace('{#P}', $this->getAliasNumPage(), $left);
        $left = str_replace('{#TP}', $this->getAliasNbPages(), $left);

        $left = str_replace('{#TP}', $this->getAliasNbPages(), $left);
        //$left = str_replace('{Corp.CEO_Sign}', '______________', $left);
        $left = str_replace('{initiallingOfPages}', '______________', $left);
        foreach($this->footerValue as $k => $v){
            $left = str_replace('{'.$k.'}', $v, $left);
        }
        //$this->Image($img, 60, $this->GetY() - $h * $k, $w * $k, $h * $k);
        if($this->CurOrientation == 'P')
            $this->Cell(60, 10, $left, 0, 0, 'L', 0, '', 0, false, 'T', 'C');
        else
            $this->Cell(92, 10, $left, 0, 0, 'L', 0, '', 0, false, 'T', 'C');

        $middle = $this->footer['middle'];
        $middle = str_replace('{#P}', $this->getAliasNumPage(), $middle);
        $middle = str_replace('{#TP}', $this->getAliasNbPages(), $middle);
        $middle = str_replace('{initiallingOfPages}', '______________', $middle);
        foreach($this->footerValue as $k => $v){
            $middle = str_replace('{'.$k.'}', $v, $middle);
        }
        if(strpos($this->footer['middle'], 'initiallingOfPages') > 0 && $img){
                //$this->SetX(60);$this->SetY(-20);
                //$this->writeHTML($ceo_sign, true, 0, true, 0);
                //$this->Image($img, $this->GetX(), $this->GetY()-5, $w * $k, $h * $k);
        }
        if($this->CurOrientation == 'P')
            $this->Cell(60, 10, $middle, 0, 0, 'C', 0, '', 0, false, 'T', 'C');
        else
            $this->Cell(92, 10, $middle, 0, 0, 'C', 0, '', 0, false, 'T', 'C');


        //$this->SetY(-15);
        $right = $this->footer['right'];
        $right = str_replace('{#P}', $this->getAliasNumPage(), $right);
        $right = str_replace('{#TP}', $this->getAliasNbPages(), $right);
        $right = str_replace('{initiallingOfPages}', '______________', $right);
        foreach($this->footerValue as $k => $v){
            $right = str_replace('{'.$k.'}', $v, $right);
        }
        if(strpos($this->footer['right'], 'initiallingOfPages') > 0 && $img){
                //$this->SetX(60);$this->SetY(-20);
                //$this->writeHTML($ceo_sign, true, 0, true, 0);
                //$this->Image($img, $this->GetX(), $this->GetY()-5, $w * $k, $h * $k);
        }
        if($this->CurOrientation == 'P')
            $this->Cell(60, 10, $right, 0, 0, 'R');
        else
            $this->Cell(92, 10, $right, 0, 0, 'R');
        // Page number
        //$this->SetY(-15);
    }

    public function writeCompositeFacsimile($a){
        $this->ln(10);
        $this->Cell(50,2*$h, $a['CEO_POST'],0,0,'L');
        $this->Cell(60,2*$h, '','B',0,'L');
        if($a['CEO_NAME']){
            $this->Cell(50,2*$h, '(' . $a['CEO_NAME'] . ')',0,1,'L');
        }
        else{
            $this->Cell(50,2*$h, '',0,1,'L');
        }
        $currPosDir = $this->GetY();

        $this->ln(14);
        if($a['ACC_POST']){
            $this->Cell(50,2*$h, $a['ACC_POST'],0,0,'L');
        }
        else{
            $this->Cell(50,2*$h, '',0,0,'L');
        }
        $this->Cell(60,2*$h, '','B',0,'L');
        if($a['ACC_NAME']){
            $this->Cell(50,2*$h, '(' . $a['ACC_NAME'] . ')',0,1,'L');
        }
        else{
            $this->Cell(50,2*$h, '',0,1,'L');
        }
        $currPosAcc = $this->GetY();

        $k = (190 / 540);

        //mail('7976607@gmail.com', 'k='.$k, $currPos);
        if($a['CEO_SIGN']){
		  //if(intval($a['CEO_SIGN']) > 0){
		    $this->SetY($currPosDir);
		    $path = $a['CEO_SIGN'];
            $img = $path;//$_SERVER['DOCUMENT_ROOT'] . $arFile['SRC'];
            $size = getimagesize($img);
            $w = $size[0];
            $h = $size[1];
            if($h > 50){
              $w = intval($w * 50 / $h);
              $h = 50;

            }
            if($w > 120){
              $h = intval($h * 120 / $w);
              $w = 120;
            }
            //mail('7976607@gmail.com', 'w='.$w.',h='.$h, print_r($size, true));
            $this->Image($img, 60, $this->GetY() - $h * $k, $w * $k, $h * $k);
		  //}
		}

        if($a['ACC_SIGN']){
		  //if(intval($a['SELLER_ACC_SIGN']['VALUE']) > 0){
		    $this->SetY($currPosAcc);
		    $path = $a['ACC_SIGN'];
            $img = $path;//$_SERVER['DOCUMENT_ROOT'] . $arFile['SRC'];
            $size = getimagesize($img);
            $w = $size[0];
            $h = $size[1];
            if($h > 50){
              $w = intval($w * 50 / $h);
              $h = 50;

            }
            if($w > 120){
              $h = intval($h * 120 / $w);
              $w = 120;
            }
            //mail('7976607@gmail.com', 'w='.$w.',h='.$h, print_r($size, true));
            $this->Image($img, 60, $this->GetY() - $h * $k, $w * $k, $h * $k);
		  //}
		}
        //$this->SetY($currPos);
        //$this->ln(4);
        //

        //mail('7976607@gmail.com', 'a', print_r($a, true));
		if($a['STAMP']){

            $path = $a['STAMP'];

			$img = $path;//$_SERVER['DOCUMENT_ROOT'] . $arFile['SRC'];

			$size = getimagesize($img);

			//540px=190mm
			$w = $size[0];
            $h = $size[1];
            if($h > 150){
              $w = intval($w * 150 / $h);
              $h = 150;

            }
            if($w > 150){
              $h = intval($h * 150 / $w);
              $w = 150;
            }
            //$this->SetY($currPosDir);
			$this->Image($img, 30, $currPosDir-10, $w * $k, $h * $k);
		}
    }

    public function CEOFacsimile($a){
        $k = (190 / 540);
        $ceo_sign = ''; $stamp = '';
        //mail('7976607@gmail.com', 'k='.$k, $currPos);
        if($a['CEO_SIGN']){
		  //if(intval($a['CEO_SIGN']) > 0){
		//    $this->SetY($currPosDir);
		    $path = $a['CEO_SIGN'];
            $img = $path;//$_SERVER['DOCUMENT_ROOT'] . $arFile['SRC'];
            $size = getimagesize($img);
            $w = $size[0];
            $h = $size[1];
            if($h > 50){
              $w = intval($w * 50 / $h);
              $h = 50;

            }
            if($w > 120){
              $h = intval($h * 120 / $w);
              $w = 120;
            }
            //mail('7976607@gmail.com', 'w='.$w.',h='.$h, print_r($size, true));
            $ceo_sign = array($img, '', '', $w * $k, $h * $k);
		  //}
		}
        //$res = $this->blayers(array($a['imgs']['CEO_SIGN'],$a['imgs']['STAMP']),true);
        //mail('7976607@gmail.com', 'a', $res);
        //$this->SetY($currPos);
        //$this->ln(4);
        //

        //mail('7976607@gmail.com', 'a', print_r($a, true));

		if($a['STAMP']){

            $path = $a['STAMP'];

			$img = $path;//$_SERVER['DOCUMENT_ROOT'] . $arFile['SRC'];

			$size = getimagesize($img);

			//540px=190mm
			$w = $size[0];
            $h = $size[1];
            if($h > 150){
              $w = intval($w * 150 / $h);
              $h = 150;

            }
            if($w > 150){
              $h = intval($h * 150 / $w);
              $w = 150;
            }
            //$this->SetY($currPosDir);
			$stamp = array($img, '', '', $w * $k, $h * $k);
		}

        return array($ceo_sign, $stamp);
    }
}
?>