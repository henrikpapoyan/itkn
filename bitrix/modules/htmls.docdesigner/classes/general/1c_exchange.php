<?
/*******************************************************************************
* Module:   DocDesigner                                                        *
* Class:    CDocDesigner1Cv8 extends CDocDesigner                              *
* Version:  5.5.1                                                              *
* Author:   Alexey Andreev, HTMLStudio (www.htmls.ru)                          *
* License:  Shareware                                                          *
* Date:     2013-01-05                                                         *
* e-mail:   aav@htmls.ru                                                       *
*******************************************************************************/
if(COption::GetOptionString('htmls.docdesigner', 'DOCDESIGNER_ADDMESSAGE2LOG') == 'Y'){
	define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/bitrix/tmp/DocDesigner.txt");
}
//IncludeModuleLangFile(__FILE__);
class CDocDesigner1Cv8 extends CDocDesigner{
	function CDocDesigner1Cv8(){
		if(!is_dir($_SERVER['DOCUMENT_ROOT'] . "/upload/htmls.docdesigner/1c_exchange/"))
			mkdir($_SERVER['DOCUMENT_ROOT'] . "/upload/htmls.docdesigner/1c_exchange/");
	}
	//product upload functions - start
	function ImportProducts(){
		global $DB;
		$CRM_CATALOG = CCrmCatalog::EnsureDefaultExists();
		$filename = $_SERVER["DOCUMENT_ROOT"] . "/upload/htmls.docdesigner/1c_exchange/crmproducts.xml";
		if(!file_exists($filename)){
			echo "CRM:Products /upload/htmls.docdesigner/1c_exchange/crmproducts.xml not found!";
			return;
		}
		$handle = fopen($filename, "r");
		$contents = fread($handle, filesize($filename));
		fclose($handle);
		$doc = new DOMDocument();
		$doc->loadXML($contents);
		$arProds = $doc->getElementsByTagName($this->win2utf("CatalogObject.Nomenclature"));
		foreach($arProds as $arProd){
			$arElem = array();
			foreach($arProd->childNodes as $node){
				$arElem[$this->utf2win($node->nodeName)] = $this->utf2win($node->nodeValue);
			}
			$arElems[] = $arElem;
		}

		$el = new CIBlockElement;
		foreach($arElems as $arElem){
			if($arElem['IsFolder'] == 'true'){
				$arSecId[$arElem['Ref']] = $this->GetSection($arElem['Ref'], $arElem, $CRM_CATALOG);
			}
			else{
				$arFilter = array('XML_ID' => $arElem['Ref']);
				$list = CIBlockElement::GetList($sort, $arFilter, false, false, array('ID'));
				if($list->selectedRowsCount() == 0){
					$arFields = array('IBLOCK_ID' => $CRM_CATALOG, "ACTIVE" => 'Y',
					"IBLOCK_SECTION_ID" => $this->GetSection($arElem['Parent'], array()), 'XML_ID' => $arElem['Ref'],
					"NAME" => $arElem['Description'], "PREVIEW_TEXT" => $arElem['NameFull']);
					$ID = $el->Add($arFields);
					$arElemId[$arElem['Ref']] = $ID;
				}
				else{
					$ob = $list->GetNextElement();
					$ar = $ob->GetFields();
					$arElemId[$arElem['Ref']] = $ar['ID'];
				}
			}
		}

		$arElems = array();
		$arPrice = $doc->getElementsByTagName($this->win2utf("Row"));
		foreach($arPrice as $ar){
			$arElem = array();
			foreach($ar->childNodes as $node){
				$arElem[$this->utf2win($node->nodeName)] = $this->utf2win($node->nodeValue);
			}
			$arElems[] = $arElem;
		}

		foreach($arElems as $ar){
			if($res = CCrmProduct::GetByID($arElemId[$ar['Nomenclature']])){
				$sQuery = "UPDATE b_crm_product SET CURRENCY_ID='".$ar['Currency']."',PRICE='".$ar['Price']."' WHERE ID = '".$arElemId[$ar['Nomenclature']]."'";
				$DB->Query($sQuery, false, 'File: '.__FILE__.'<br/>Line: '.__LINE__);
			}
			else{
				if($arElemId[$ar['Nomenclature']] > 0){
					$sQuery =  "INSERT INTO b_crm_product(CATALOG_ID,ID,CURRENCY_ID,PRICE) VALUES('".$CRM_CATALOG."','".$arElemId[$ar['Nomenclature']]."','".$ar['Currency']."','".$ar['Price']."')";
					$DB->Query($sQuery, false, 'File: '.__FILE__.'<br/>Line: '.__LINE__);
				}
			}
		}
		@unlink($filename);
		echo "success";
	}//ImportProducts

	//return section_id if section find, null - not find
	function GetSection($XID, $arSec, $IBLOCK_ID){
		global $USER;
		if($XID == '00000000-0000-0000-0000-000000000000') return false;

		$sort = array('sort' => 'asc');
		$filter = array('IBLOCK_ID' => $IBLOCK_ID, 'XML_ID' => $XID);
		$srch = CIBlockSection::GetList($sort, $filter);
		if($srch->selectedRowsCount() == 0 ){
			$bs = new CIBlockSection;
			$arFields = array('IBLOCK_ID' => $IBLOCK_ID, "ACTIVE" => 'Y',
			"NAME" => $arSec['Description'], 'XML_ID' => $XID);
			if($arSec['Parent'] != '00000000-0000-0000-0000-000000000000'){
				$arFields['IBLOCK_SECTION_ID'] = $this->GetSection($arSec['Parent'], array(), $IBLOCK_ID);
			}
			$ID = $bs->Add($arFields);
			return $ID;
		}
		$fetch = $srch->fetch();
		$parent_id = $fetch['ID'];
		return $parent_id;
	}//GetSection($XID, $IBLOCK_ID)
	//product upload functions - fin

    //crm - import start
    function ImportCompanies(){
		global $DB;
        $filename = $_SERVER["DOCUMENT_ROOT"] . "/upload/htmls.docdesigner/1c_exchange/crmcompanies.xml";
        $UF_INN = COption::GetOptionString('htmls.docdesigner', 'DOCDESIGNER_CRM_COMPANY_INN', "");
        $UF_1C = 'UF_CRM_1384007245';
        if($UF_INN == ""){
          echo "�� ����� ��� ��� ������������� ��������";
          @unlink($filename);
          return;
        }
        else{
    		if(!file_exists($filename)){
    			echo "CRM:Companies /upload/htmls.docdesigner/1c_exchange/crmcompanies.xml not found!";
    			return;
    		}
            $module_id = 'htmls.docdesigner';
            $UF_KPP = COption::GetOptionString($module_id, 'DOCDESIGNER_CRM_COMPANY_KPP');
		    $UF_BANK_NAME = COption::GetOptionString($module_id, 'DOCDESIGNER_CRM_COMPANY_BANK_NAME');
		    $UF_BANK_BIK = COption::GetOptionString($module_id, 'DOCDESIGNER_CRM_COMPANY_BANK_BIK');
		    $UF_BANK_RS = COption::GetOptionString($module_id, 'DOCDESIGNER_CRM_COMPANY_RS');
		    $UF_BANK_KS = COption::GetOptionString($module_id, 'DOCDESIGNER_CRM_COMPANY_BANK_KS');
            $UF_BANK_CITY = COption::GetOptionString($module_id, 'DOCDESIGNER_CRM_COMPANY_BANK_CITY');
		    //$UF_ADDRESS_LEGAL = $COMPANY['ADDRESS_LEGAL'];
    		$handle = fopen($filename, "r");
    		$contents = fread($handle, filesize($filename));
    		fclose($handle);
    		$doc = new DOMDocument();
    		$doc->loadXML($contents);
    		$arProds = $doc->getElementsByTagName($this->win2utf("CatalogObject.Company"));
    		foreach($arProds as $arProd){
    			$arElem = array();
    			foreach($arProd->childNodes as $node){
    			  if($this->utf2win($node->nodeName) == 'CONTACT'){
    			    $arContact = array();
                    foreach($node->childNodes as $cNode){
                      if($this->utf2win($cNode->nodeName) == 'PHONE'){
                        $arContact[$this->utf2win($cNode->nodeName)][] = $this->utf2win($cNode->nodeValue);
                      }
                      elseif($this->utf2win($cNode->nodeName) == 'EMAIL'){
                        $arContact[$this->utf2win($cNode->nodeName)][] = $this->utf2win($cNode->nodeValue);
                      }
                      else{
                        $arContact[$this->utf2win($cNode->nodeName)] = $this->utf2win($cNode->nodeValue);
                      }
                    }
                    $arElem['CONTACT'][] = $arContact;
    			  }
                  else{
    				$arElem[$this->utf2win($node->nodeName)] = $this->utf2win($node->nodeValue);
                  }
    			}
    			$arElems[] = $arElem;
    		}
            //AddMessage2Log(print_r($arElems, true));
            $CCrmFieldMulti = new CCrmFieldMulti();

            if(count($arElems > 0)){
              $CrmCompany = new CCrmCompany;
              $CrmContact = new CCrmContact;
              foreach($arElems as $arElem){
                $INN = $arElem['INN'];
                $res = CCrmCompany::GetList(array(), array($UF_INN => $INN));
                if($res->SelectedRowsCount() == 0){
                    $arFields = array('TITLE' => $arElem['NameFull'],
                                        $UF_INN => $INN,
                                        $UF_KPP => $arElem['KPP'],
                                        $UF_BANK_NAME => $arElem['BANK'],
                                        $UF_BANK_BIK => $arElem['BIK'],
                                        $UF_BANK_RS => $arElem['RS'],
                                        $UF_BANK_KS => $arElem['KS'],
                                        $UF_BANK_CITY => $arElem['CITY'],
                                        'ADDRESS' => $arElem['ADDRESS'],
                                        'ADDRESS_LEGAL' => $arElem['ADDRESS_LEGAL'],
                                        'COMPANY_TYPE' => 'CUSTOMER',
                                        $UF_1C => 'Y');//1C
                    if($ID = $CrmCompany->Add($arFields)){
                        if($arElem['PHONE']){
                            $arFields = array('ENTITY_ID' => 'COMPANY', 'ELEMENT_ID' => $ID,
                            'TYPE_ID' => 'PHONE', 'VALUE_TYPE' => 'WORK',
                            'VALUE' => $arElem['PHONE']);
                            $r = $CCrmFieldMulti->Add($arFields);
                        }

                        if($arElem['EMAIL']){
                            $arFields = array('ENTITY_ID' => 'COMPANY', 'ELEMENT_ID' => $ID,
                            'TYPE_ID' => 'EMAIL', 'VALUE_TYPE' => 'EMAIL',
                            'VALUE' => $arElem['EMAIL']);
                            $r = $CCrmFieldMulti->Add($arFields);
                        }
                    }
                    else{
                        AddMessage2Log($CrmCompany->LAST_ERROR);
                        AddMessage2Log(print_r($arFields, true));
                    }
                }
                else{
                  while($company = $res->Fetch()){
                    $ID = $company['ID'];
                  }
                  if($company[$UF_1C] != 'Y'){
                    $CrmCompany->Update($ID, array($UF_1C => 'Y'));
                  }
                }
                if($ID > 0){
                    if($arElem['CONTACT']){
                      foreach($arElem['CONTACT'] as $arContact){
                        $name = $arContact['NAME'];
                        $arName = explode(' ', $name);
                        $arFilter = array('COMPANY_ID' => $ID);
                        if(count($arName) > 1){
                            $arFilter['LAST_NAME'] = $arName[0];
                        }
                        if(count($arName) >= 2){
                            $arFilter['NAME'] = $arName[1];
                        }
                        if(count($arName) == 3){
                            $arFilter['SECOND_NAME'] = $arName[2];
                        }
                        $rCont = CCrmContact::GetList(array(), $arFilter);
                        if($rCont->SelectedRowsCount() == 0){
                            $arFields = array('COMPANY_ID' => $ID, 'SOURCE_ID' => 'SELF', 'TYPE_ID' => 'CLIENT');
                            if(count($arName) > 1){
                                $arFields['LAST_NAME'] = $arName[0];
                            }
                            if(count($arName) >= 2){
                                $arFields['NAME'] = $arName[1];
                            }
                            if(count($arName) == 3){
                                $arFields['SECOND_NAME'] = $arName[2];
                            }

                            if($ContID = $CrmContact->Add($arFields)){
                                if($arContact['PHONE']){
                                  foreach($arContact['PHONE'] as $val){
                                    $arFields = array('ENTITY_ID' => 'CONTACT', 'ELEMENT_ID' => $ContID,
                                    'TYPE_ID' => 'PHONE', 'VALUE_TYPE' => 'WORK',
                                    'VALUE' => $val);
                                    $r = $CCrmFieldMulti->Add($arFields);
                                  }
                                }

                                if($arContact['EMAIL']){
                                  foreach($arContact['EMAIL'] as $val){
                                    $arFields = array('ENTITY_ID' => 'CONTACT', 'ELEMENT_ID' => $ContID,
                                    'TYPE_ID' => 'EMAIL', 'VALUE_TYPE' => 'EMAIL',
                                    'VALUE' => $val);
                                    $r = $CCrmFieldMulti->Add($arFields);
                                  }
                                }
                            }
                            else{
                                AddMessage2Log($CrmContact->LAST_ERROR);
                                AddMessage2Log(print_r($arFields, true));
                            }
                        }
                      }
                    }
                }
              }
            }

    		@unlink($filename);
    		echo "success";
        }
	}//ImportCompanies
	//crm - import end

	//invoice export - start
	function ExportInvoices(){
		global $DB;
		$dt = COption::GetOptionString("DOCDESIGNER_1Cv8Exchange", "last_export_time_committed", 0);
		$ExpFiles = COption::GetOptionString("htmls.docdesigner", "DOCDESIGNER_V8EXCHANGE_FILE_EXPORT");
		$module_id = "htmls.docdesigner";
		$CContract = new CDocDesignerContracts();
		//own company
		$SET = "COMPANY_INN,COMPANY_KPP,COMPANY_RS";
		$arSet = explode(',',$SET);
		foreach($arSet as $str){
			$n = COption::GetOptionString($module_id, 'DOCDESIGNER_' . $str);
			$arSelectOC[$n] = "PROPERTY_" . $n;
		}

		$OwnCompany = array();
		$Customer = array();

		$RegID = IntVal(COption::GetOptionInt("htmls.docdesigner", 'DOCDESIGNER_DOCS_REGISTER_ID'));
		$arSelect = Array('DATE_CREATE', "NAME", "PROPERTY_SUMMA", "PROPERTY_TABLE", 'PROPERTY_COMPANY_ID', 'PROPERTY_OWN_ENTITIES', 'PROPERTY_CONTRACT_ID', 'PROPERTY_BIZPROC_ID', 'PROPERTY_LINK2FILE');
		$arFilter = Array("IBLOCK_ID"=>$RegID, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "PROPERTY_NUMERATOR" => COption::GetOptionInt("htmls.docdesigner", 'DOCDESIGNER_NUMERATOR_1C'));

		if(intval($dt) != 0){
			$start = date($DB->DateFormatToPHP(CSite::GetDateFormat("FULL")), $dt);
			$arFilter['>=DATE_CREATE'] = $start;
		}

        //print_r($arFilter);
		$NUMS_IB = COption::GetOptionInt("htmls.docdesigner", "DOCDESIGNER_CONTRACTS_NUMERATORS");
		$LIB_IB = COption::GetOptionInt("htmls.docdesigner", "DOCDESIGNER_CONTRACTS_PATH2LIBRARY");

		$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
		while($ob = $res->GetNextElement()){
			$arFields = $ob->GetFields();
			$OwnCompanyId = $arFields['PROPERTY_OWN_ENTITIES_VALUE'];
			if(!array_key_exists($OwnCompanyId, $OwnCompany)){
				$OwnCompany[$OwnCompanyId] = $CContract->GetOwnCompanyData($OwnCompanyId, $arSelectOC);
			}
            $ContName = '';
			$ContDate = '';
			$ContXML = '';
			$CompanyId = $arFields['PROPERTY_COMPANY_ID_VALUE'];
			if(!array_key_exists($CompanyId, $Customer)){
				//get company info
				$arRes = CCrmCompany::GetList(array(),array('ID' => $CompanyId),array());
				while($obCom = $arRes->Fetch()){
					$arSelect = Array("ID", "NAME", "DATE_CREATE", 'PROPERTY_BIZPROC_ID', 'PROPERTY_SUMMA', 'PROPERTY_LINK2FILE');
					$arFilter = Array("IBLOCK_ID"=>$RegID, "ACTIVE"=>"Y");
					if(intval($arFields['PROPERTY_CONTRACT_ID_VALUE']) > 0){
						//appendix
						$arFilter["ID"] = $arFields['PROPERTY_CONTRACT_ID_VALUE'];
					}
					else{
						$arFilter["PROPERTY_BIZPROC_ID"] = $arFields['PROPERTY_BIZPROC_ID_VALUE'];
						$arFilter["PROPERTY_NUMERATOR"] = explode(',', $NUMS_IB);
					}
					$resCont = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1), $arSelect);
					while($arCont = $resCont->GetNextElement()){
						$arCont = $arCont->GetFields();
						$obCom['CONTRACTS'][$arCont['PROPERTY_BIZPROC_ID_VALUE']]['XML_ID'] = $arCont['PROPERTY_BIZPROC_ID_VALUE'];
						$obCom['CONTRACTS'][$arCont['PROPERTY_BIZPROC_ID_VALUE']]['OWN_ENTITIES'] = $OwnCompanyId;
						$obCom['CONTRACTS'][$arCont['PROPERTY_BIZPROC_ID_VALUE']]['CONTRACT_NAME'] = $arCont['NAME'];
						$obCom['CONTRACTS'][$arCont['PROPERTY_BIZPROC_ID_VALUE']]['CONTRACT_DATE'] = $arCont['DATE_CREATE'];
						$obCom['CONTRACTS'][$arCont['PROPERTY_BIZPROC_ID_VALUE']]['SUMMA'] = $arCont['PROPERTY_SUMMA_VALUE'];
						$obCom['CONTRACTS'][$arCont['PROPERTY_BIZPROC_ID_VALUE']]['FILE_ID'] = $arCont['PROPERTY_LINK2FILE_VALUE'];
						$ContXML = $arCont['PROPERTY_BIZPROC_ID_VALUE'];
						$ContName = $arCont['NAME'];
						$ContDate = $arCont['DATE_CREATE'];
					}
					$Customer[$CompanyId] = $obCom;
				}
			}
			elseif(!array_key_exists($arCont['PROPERTY_BIZPROC_ID_VALUE'], $Customer[$CompanyId]['CONTRACTS'])){
				$arSelect = Array("ID", "NAME", "DATE_CREATE", 'PROPERTY_BIZPROC_ID', 'PROPERTY_SUMMA');
				$arFilter = Array("IBLOCK_ID"=>$RegID, "ACTIVE"=>"Y");
				if(intval($arFields['PROPERTY_CONTRACT_ID_VALUE']) > 0){
					//appendix
					$arFilter["ID"] = $arFields['PROPERTY_CONTRACT_ID_VALUE'];
				}
				else{
					$arFilter["PROPERTY_BIZPROC_ID"] = $arFields['PROPERTY_BIZPROC_ID_VALUE'];
					$arFilter["PROPERTY_NUMERATOR"] = explode(',', $NUMS_IB);
				}
				$resCont = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1), $arSelect);
				while($arCont = $resCont->GetNextElement()){
					$arCont = $arCont->GetFields();
					$Customer[$CompanyId]['CONTRACTS'][$arCont['PROPERTY_BIZPROC_ID_VALUE']]['XML_ID'] = $arCont['PROPERTY_BIZPROC_ID_VALUE'];
					$Customer[$CompanyId]['CONTRACTS'][$arCont['PROPERTY_BIZPROC_ID_VALUE']]['OWN_ENTITIES'] = $OwnCompanyId;
					$Customer[$CompanyId]['CONTRACTS'][$arCont['PROPERTY_BIZPROC_ID_VALUE']]['CONTRACT_NAME'] = $arCont['NAME'];
					$Customer[$CompanyId]['CONTRACTS'][$arCont['PROPERTY_BIZPROC_ID_VALUE']]['CONTRACT_DATE'] = $arCont['DATE_CREATE'];
					$Customer[$CompanyId]['CONTRACTS'][$arCont['PROPERTY_BIZPROC_ID_VALUE']]['SUMMA'] = $arCont['PROPERTY_SUMMA_VALUE'];
					$Customer[$CompanyId]['CONTRACTS'][$arCont['PROPERTY_BIZPROC_ID_VALUE']]['FILE_ID'] = $arCont['PROPERTY_LINK2FILE_VALUE'];
					$ContName = $arCont['NAME'];
					$ContDate = $arCont['DATE_CREATE'];
					$ContXML = $arCont['PROPERTY_BIZPROC_ID_VALUE'];
				}
			}

			$table = unserialize(htmlspecialchars_decode($arFields['PROPERTY_TABLE_VALUE']));
			$arND = explode(' ', $arFields['NAME']);
			$stmp = MakeTimeStamp($arFields['DATE_CREATE'], "DD.MM.YYYY HH:MI:SS");

			$arDoc[] = array('NUMBER' => $arFields['NAME'], 'DATE' => date('Y-m-d H:i:s', $stmp),
							'COMPANY' => $OwnCompanyId, 'CUSTOMER' => $CompanyId, 'TABLE' => $table,
							'CONTRACT_NAME' => $ContName, 'CONTRACT_DATE' => $ContDate,
							'CONTRACT_XML_ID' => $ContXML, 'FILE_ID' => $arFields['PROPERTY_LINK2FILE_VALUE']);
		}

		if(count($arDoc) == 0){
			echo "noinvoice";
			return;
		}
        $str = '<?xml version="1.0" encoding="UTF-8"?><V8Exch:_1CV8DtUD xmlns:V8Exch="http://www.1c.ru/V8/1CV8DtUD/" xmlns:v8="http://v8.1c.ru/data" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><V8Exch:Data>';

		foreach($OwnCompany as $id => $arCompany){
			$str .= "<CatalogObject.Organizations>";
			$str .= "<Ref>".$id."</Ref>";
			$str .= "<Description>".$arCompany['NAME']."</Description>";
			$str .= "<INN>".$arCompany['PROPERTY_'.COption::GetOptionString($module_id, 'DOCDESIGNER_COMPANY_INN')]."</INN>";
			$str .= "<KPP>".$arCompany['PROPERTY_'.COption::GetOptionString($module_id, 'DOCDESIGNER_COMPANY_KPP')]."</KPP>";
			$str .= "</CatalogObject.Organizations>";
		}

		foreach($Customer as $id => $arCompany){
			$str .= "<CatalogObject.Customers>";
			$str .= "<Ref>".$id."</Ref>";
			$str .= "<Description>".$arCompany['TITLE']."</Description>";
			$str .= "<LegalAddress>".$arCompany['ADDRESS_LEGAL']."</LegalAddress>";
			$str .= "<ActualAddress>".$arCompany['ADDRESS']."</ActualAddress>";
			$str .= "<INN>".$arCompany[COption::GetOptionString($module_id, 'DOCDESIGNER_CRM_COMPANY_INN')]."</INN>";
			$str .= "<KPP>".$arCompany[COption::GetOptionString($module_id, 'DOCDESIGNER_CRM_COMPANY_KPP')]."</KPP>";
			$str .= "<Contracts>";
			foreach($arCompany['CONTRACTS'] as $arContr){
				$str .= "<Contract>";
				$str .= "<Ref>".$arContr['XML_ID']."</Ref>";
				$str .= "<OwnCompany>".$arContr['OWN_ENTITIES']."</OwnCompany>";
				$str .= "<Number>".$arContr['CONTRACT_NAME']."</Number>";
				$str .= "<Date>".$arContr['CONTRACT_DATE']."</Date>";
				$str .= "<Sum>".$arContr['SUMMA']."</Sum>";
                if($ExpFiles == 'Y'){
                	$db_props = CIBlockElement::GetProperty($LIB_IB, $arContr['FILE_ID'], array("sort" => "asc"), Array("CODE"=>"FILE"));
					if($ar_props = $db_props->Fetch()){
						$FILE_ID = IntVal($ar_props["VALUE"]);
						$str .= "<File>" . $FILE_ID ."</File>";
					}
                }
				$str .= "</Contract>";
			}
			$str .= "</Contracts>";
			$str .= "<SettlementAccounts><SettlementAccount>
			<AccountNumber>".$arCompany[COption::GetOptionString($module_id, 'DOCDESIGNER_CRM_COMPANY_RS')]."</AccountNumber>";
			//$Bank = $CContract->GetBankInfo($arCompany[COption::GetOptionString($module_id, 'DOCDESIGNER_CRM_COMPANY_BANK_NAME')]);
			$str .= "<Bank><Name>".$arCompany[COption::GetOptionString($module_id, 'DOCDESIGNER_CRM_COMPANY_BANK_NAME')]."</Name>
			<City>".$arCompany[COption::GetOptionString($module_id, 'DOCDESIGNER_CRM_COMPANY_BANK_CITY')]."</City><BIK>".$arCompany[COption::GetOptionString($module_id, 'DOCDESIGNER_CRM_COMPANY_BANK_BIK')]."</BIK>
			<CorrespondentAccount>".$arCompany[COption::GetOptionString($module_id, 'DOCDESIGNER_CRM_COMPANY_BANK_KS')]."</CorrespondentAccount></Bank>
			</SettlementAccount></SettlementAccounts>";
			$str .= "<Contacts>";

			$contacts = CCrmContact::GetList(array(), array('COMPANY_ID' => $id), array());
			while($ob = $contacts->Fetch()){
				$cName = $ob['LAST_NAME'] . ' ' . $ob['NAME'] . ' ' . $ob['SECOND_NAME'];
				$data = CCrmFieldMulti::GetList(array(), array('ENTITY_ID' => 'CONTACT', 'ELEMENT_ID' => $ob['ID']));
				$phone='';$email='';
				while($cont = $data->Fetch()){
					if($cont['TYPE_ID'] == 'PHONE'){
						$phone = $cont['VALUE'];
					}
					if($cont['TYPE_ID'] == 'EMAIL'){
						$email = $cont['VALUE'];
					}
				}
				$str .= "<Contact>";
				$str .= "<Ref>".$ob['ID']."</Ref>";
				$str .= "<Name>".$cName."</Name>";
				$str .= "<Phone>".$phone."</Phone>";
				$str .= "<Email>".$email."</Email>";
				$str .= "</Contact>";
			}
			$str .= "</Contacts>";
			$str .= "</CatalogObject.Customers>";
		}
		foreach($arDoc as $Doc){
			$str .= "<DocumentObject.Invoice>";
			$str .= "<Date>" . $Doc['DATE'] . "</Date>";
			$str .= "<Number>" . $Doc['NUMBER'] . "</Number>";
			$str .= "<Customer>" . $Doc['CUSTOMER'] . "</Customer>";
			$str .= "<Organization>" . $Doc['COMPANY'] . "</Organization>";
			if($Doc['CONTRACT_XML_ID']){
				$str .= "<Contract>" . $Doc['CONTRACT_XML_ID'] . "</Contract>";
			}
			if($ExpFiles == 'Y'){
				$db_props = CIBlockElement::GetProperty($LIB_IB, $Doc['FILE_ID'], array("sort" => "asc"), Array("CODE"=>"FILE"));
				if($ar_props = $db_props->Fetch()){
					$FILE_ID = IntVal($ar_props["VALUE"]);
					$str .= "<File>" . $FILE_ID ."</File>";
				}
			}

			$str .= "<Products>";
			foreach($Doc['TABLE'] as $Row){
				$str .= "<Row>";
				if($Row[0] == 'str'){
					$str .= "<Product>" . $Row[1] . "</Product>";
					$str .= "<Price>" . $Row[3] . "</Price>";
					$str .= "<Quantity>" . $Row[2] . "</Quantity>";
				}
				elseif($Row[0] == 'ib'){
					$str .= "<Product>" . $this->GetXmlId($Row[4]) . "</Product>";
					$str .= "<Price>" . $Row[3] . "</Price>";
					$str .= "<Quantity>" . $Row[2] . "</Quantity>";
				}
				elseif($Row[0] == 'crm'){
					$str .= "<Product>" . $this->GetXmlId($Row[4]) . "</Product>";
					$str .= "<Price>" . $Row[3] . "</Price>";
					$str .= "<Quantity>" . $Row[2] . "</Quantity>";
				}
				$str .= "</Row>";
			}
			$str .= "</Products>";

			$str .= "</DocumentObject.Invoice>";
		}
		$str .= "</V8Exch:Data></V8Exch:_1CV8DtUD>";
		file_put_contents($_SERVER["DOCUMENT_ROOT"] . "/upload/htmls.docdesigner/1c_exchange/invoices.xml",  $this->win2utf($str));
		COption::SetOptionString("DOCDESIGNER_1Cv8Exchange", "last_export_time_committed", MakeTimeStamp(date("d.m.Y H:i:s"), "DD.MM.YYYY HH:MI:SS"));
		echo "success";
	}//ExportInvoices

	function GetXmlId($id){
		$arSelect = Array("XML_ID");
		$arFilter = Array("ID"=>IntVal($id));
		$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
		while($ob = $res->GetNextElement()){
			$arFields = $ob->GetFields();
			return $arFields['XML_ID'];
		}
		return $id;
	}//GetXmlId
	//invoice export - fin

	function utf2win($str){
		if(strtoupper(LANG_CHARSET) == 'WINDOWS-1251'){
			return iconv('UTF-8', 'WINDOWS-1251', $str);
		}
		return $str;
	}

	function win2utf($str){
		if(strtoupper(LANG_CHARSET) == 'WINDOWS-1251'){
			return iconv('WINDOWS-1251', 'UTF-8', $str);
		}
		return $str;
	}
//end class CDocDesigner1Cv8
}
?>