<?
/*******************************************************************************
* Module:   DocDesigner                                                        *
* Class:    CDocDesignerInvoicing extends CDocDesigner                         *
* Version:  4.0.1                                                              *
* Author:   Alexey Andreev, HTMLStudio (www.htmls.ru)                          *
* License:  Shareware                                                          *
* Date:     2012-08-01                                                         *
* e-mail:   aav@htmls.ru                                                       *
*******************************************************************************/
if(COption::GetOptionString('htmls.docdesigner', 'DOCDESIGNER_ADDMESSAGE2LOG') == 'Y'){
	define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/bitrix/tmp/DocDesigner.txt");
}
//IncludeModuleLangFile(__FILE__);
class CDocDesignerInvoicing extends CDocDesigner{
	function CDocDesignerInvoicing(){
		$this->InvAUTO_NUMBER = COption::GetOptionString("htmls.docdesigner", 'DOCDESIGNER_INVOICES_AUTO_NUMBER', 'Y');
		$this->InvRESET_NUMBER = COption::GetOptionString("htmls.docdesigner", 'DOCDESIGNER_INVOICES_RESET_NUMBER', 'Y');
		$this->InvLENGTH_NUMBER = intval(COption::GetOptionString("htmls.docdesigner", 'DOCDESIGNER_INVOICES_LENGTH_NUMBER', '3'));
		$this->InvSTART_NUMBER = intval(COption::GetOptionString("htmls.docdesigner", 'DOCDESIGNER_INVOICES_START_NUMBER', '1'));
	}

	function GetLastInvoiceNumber($Prefix){

		if($this->InvAUTO_NUMBER == 'N') return '';
        //prefix?!
		$IBLOCK_ID = intval(COption::GetOptionString("htmls.docdesigner", 'DOCDESIGNER_INVOICES_REGISTER'));
		$arSelect = Array("ID", "PROPERTY_NUMBER");
		$arFilter = Array("IBLOCK_ID" => $IBLOCK_ID, "PROPERTY_PREFIX" => $Prefix);
		if($this->InvRESET_NUMBER == 'Y'){
			$arFilter['PROPERTY_YEAR'] = date("Y", time());
        }
		$start = $this->InvSTART_NUMBER;
		$len = $this->InvLENGTH_NUMBER;

		$res = CIBlockElement::GetList(Array("PROPERTY_NUMBER" => "DESC"), $arFilter, false, Array("nPageSize" => 1), $arSelect);
		//no one invoice
		if($res->SelectedRowsCount() == 0){
			if($start == 0) $number = 1;
			else $number = $start;
		}
		else{
			while($ob = $res->GetNextElement()){
				$arFields = $ob->GetFields();
				$number = $arFields['PROPERTY_NUMBER_VALUE'];
				$number++;
			}
		}

		$NumLen = strlen($number);
		$delta = $len - $NumLen;
		$pref = '';
		if($NumLen < $len){
			for($i = 1; $i <= $delta; $i++){
				$pref .= '0';
			}
		}
		$arResult = array('NUMBER' => $number, 'YEAR' => date("Y", time()), 'STR_NUMBER' => $Prefix.'-'.$pref . $number, 'PREFIX' => $Prefix);
		return $arResult;
	}//GetLastInvoiceNumber

	//add new invoice into register (list)
	function RegisterInvoice($arCRM, $CONTRACT_ID, $arNum, $SUM, $BP_ID, $fname, $table){
		$IBLOCK_ID = IntVal(COption::GetOptionString("htmls.docdesigner", 'DOCDESIGNER_INVOICES_REGISTER'));
		$arFile = CFile::MakeFileArray($fname);
		$Name = $arNum['STR_NUMBER'] . ' ' . $arCRM['COMPANY_NAME'];
		$arProps = array('YEAR' =>$arNum['YEAR'], 'NUMBER' => $arNum['NUMBER'], 'NUM_DATE' => $arNum['STR_NUMBER'] . ' ' . date("d.m.Y"),
					'COMPANY_ID' => $arCRM['COMPANY_ID'], 'DEAL_ID' => $arCRM['DEAL_ID'], 'SUM' => $SUM, 'PREFIX' => $arNum['PREFIX'],
					'BIZPROC_ID' => $BP_ID, 'FILE' => $arFile, "CONTRACT_ID" => $CONTRACT_ID,
					'TYPE' => COption::GetOptionString("htmls.docdesigner", 'DOCDESIGNER_INVOICE_TYPE'),
					'BDT' => COption::GetOptionString("htmls.docdesigner", 'DOCDESIGNER_INVOICE_BDT'),
					'TABLE' => serialize($table), 'OWN_COMPANY_ID' => $arCRM['OWN_COMPANY_ID']);
		$arFields = array('IBLOCK_ID' => $IBLOCK_ID, 'ACTIVE' => 'Y', 'NAME' => $Name, "PROPERTY_VALUES"=> $arProps);

		$el = new CIBlockElement;
		if($REG_ID = $el->Add($arFields)){
			@unlink($_SERVER['DOCUMENT_ROOT'] . $fname);
			return $REG_ID;
		}
		else{
			if(COption::GetOptionString('htmls.docdesigner', 'DOCDESIGNER_ADDMESSAGE2LOG') == 'Y'){
				AddMessage2Log("Error.RegisterInvoice: ".$el->LAST_ERROR, "htmls.DocDesigner");
			}
			return false;
		}
	}//RegisterInvoice

	//build link to invoice for component template
	function GetInvoice($bpID){
		return false;
		$arSelect = array('ID', 'NAME', 'PROPERTY_SUM', 'PROPERTY_NUM_DATE', 'PROPERTY_FILE');
		$arFilter = Array("IBLOCK_ID"=>IntVal(COption::GetOptionInt("htmls.docdesigner", 'DOCDESIGNER_INVOICES_REGISTER')), "PROPERTY_BIZPROC_ID"=>$bpID);
		$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
		if($res->SelectedRowsCount() > 0){
			while($ob = $res->GetNextElement()){
				$arFields = $ob->GetFields();
				$path = "/bitrix/admin/docdesigner_download.php?action=download&file=".$arFields['PROPERTY_FILE_VALUE'];
				$link2dwn = "<a href='".$path."'>".GetMessage('IBEL_BIZPROC_DOWNLOAD')."</a>";
				$link2rdr = "<a href='/services/lists/".COption::GetOptionInt("htmls.docdesigner", 'DOCDESIGNER_INVOICES_REGISTER')."/element/0/".$arFields['ID']."/'>".$arFields['PROPERTY_NUM_DATE_VALUE']." (".$arFields['PROPERTY_SUM_VALUE'].")</a>";
				$arResult[] = array('LINK2DOWNLOAD' => $link2dwn, 'LINK2REDIR' => $link2rdr);
			}
			return $arResult;
		}
		else
			return false;
	}//GetInvoice
//end class CDocDesignerInvoicing
}
?>