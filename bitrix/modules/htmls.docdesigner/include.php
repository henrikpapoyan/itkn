<?
/*******************************************************************************
* Module:   DocDesigner                                                        *
* Version:  8.4.7                                                              *
* Author:   Alexey Andreev, HTMLStudio (www.htmls.ru)                          *
* License:  Shareware                                                          *
* Date:     2016-09-06                                                         *
* e-mail:   aav@htmls.ru                                                       *
*******************************************************************************/

$fsize = filesize(__FILE__);

if($fsize > 30000){
	define("DOCDESIGNER_MODULE_DEMO", true);
}
else 	define("DOCDESIGNER_MODULE_DEMO", false);

IncludeModuleLangFile(__FILE__);
include(GetLangFileName($GLOBALS["DOCUMENT_ROOT"]."/bitrix/modules/htmls.docdesigner/lang/", "/classes/general/num2str.php"));
include(GetLangFileName($GLOBALS["DOCUMENT_ROOT"]."/bitrix/modules/htmls.docdesigner/lang/", "/classes/general/bp_activities.php"));
//define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/log.txt");
//define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/bitrix/tmp/DocDesigner.txt");
CModule::AddAutoloadClasses(
	"htmls.docdesigner",
	array(
		//"NCLNameCaseRu" => "classes/general/declination.php",
		"CNum2Str" => "classes/general/num2str.php",
		"CDocDesignerContracts" => "classes/general/contracts.php",
		"CDocDesignerInvoicing" => "classes/general/invoicing.php",
		"CDocDesigner1Cv8" => "classes/general/1c_exchange.php",
		"TCPDF" => "classes/general/tcpdf.php",
		"CDocDesignerCRM" => "classes/general/crm.php",
		"CDocDesignerProcessing" => "classes/general/processing.php",
		"CDocDesignerBPActivities" => "classes/general/bp_activities.php",
		"cPDF" => "classes/general/mypdf.php"
		)
	);
require_once($_SERVER["DOCUMENT_ROOT"].'/bitrix/modules/htmls.docdesigner/classes/general/phpword/PHPWord.php');
require_once($_SERVER["DOCUMENT_ROOT"].'/bitrix/modules/htmls.docdesigner/classes/general/NCL.NameCase.ru.php');

class CDocDesigner{
    //public $bpParameters = array();

	function CDocDesigner(){
		if(!is_dir($_SERVER['DOCUMENT_ROOT'] . "/bitrix/tmp/"))
			mkdir($_SERVER['DOCUMENT_ROOT'] . "/bitrix/tmp/");

		if(!is_dir($_SERVER['DOCUMENT_ROOT'] . "/bitrix/tmp/htmls.docdesigner/"))
			mkdir($_SERVER['DOCUMENT_ROOT'] . "/bitrix/tmp/htmls.docdesigner/");

		if(!is_dir($_SERVER['DOCUMENT_ROOT'] . "/upload/htmls.docdesigner/"))
			mkdir($_SERVER['DOCUMENT_ROOT'] . "/upload/htmls.docdesigner/");
	}

	function GetTemplate($BPID, $default = false){
		global $DB;

		if(is_array($BPID)){
			if($BPID[1] == 0)
				$query = "SELECT template, orientation, params FROM htmls_docdesigner_templates WHERE bizproc='" . $BPID[0] . "' ORDER BY ID ASC";
			else
				$query = "SELECT template, orientation, params FROM htmls_docdesigner_templates WHERE id='" . $BPID[1] . "'"; //bizproc='" . $BPID[0] . "' AND
		}
		else{
			$query = "SELECT template, orientation, params FROM htmls_docdesigner_templates WHERE bizproc='" . $BPID . "' ORDER BY ID ASC";
		}
		$result = $DB->Query($query);
		if($arRes = $result->Fetch()){
			$res = unserialize($arRes['template']);
			if(is_array($res)) return $res;
			else{
				if(empty($arRes['orientation'])) $arRes['orientation'] = 'P';
				if(empty($arRes['params'])) $arRes['params'] = array();
                else{
                  $arRes['params'] = unserialize($arRes['params']);
                }
                return array(
                    'html' => $arRes['template'],
                    'page' => $arRes['orientation'],
                    'params' => $arRes['params']);
			}
		}

	}

	function SetTemplate($BPID, $arParam){//$TMP, $TemplateID, $TemplateName){
		global $DB;

		$TemplateID = $arParam['template_id'];
		$TemplateName = $this->uni2win($arParam['template_name']);//
		$tmp = $this->uni2win($arParam['template_body']);//
		$PO = $arParam['page_orientation'];
		$CT = ($arParam['template_common'] == 'on')? 'Y' : 'N';
		$bgId = $arParam['bgId'];
		if(is_array($arParam['background'])){
		    $arFile = $arParam['background'];
		    $arFile['MODULE_ID'] = 'htmls.docdesigner';
		    if($arParam['bgId'] > 0){
		        $arFile['old_file'] = $arParam['bgId'];
		    }
		    $bgId = CFile::SaveFile($arFile, "htmls.docdesigner");
            //if (intval($fid)>0) $arFields["IMAGE_ID"] = intval($fid); 
            //else $arFields["IMAGE_ID"] = "null";
		}

        if($arParam['delBg'] == 'on'){
          if($bgId > 0){
            CFile::Delete($bgId);
          }
          $bgId = 0;
        }
		
        $arParams = array('lm' => $arParam['leftMargin'], 'rm' => $arParam['rightMargin'], 'tm' => $arParam['topMargin'],
            'bm' => $arParam['bottomMargin'], 'lfooter' => $arParam['left-footer'], 'mfooter' => $arParam['middle-footer'],
            'rfooter' => $arParam['right-footer'], 'ffooter' => $arParam['font-size'], 'bg' => $bgId,
            'pdfFont' => $arParam['pdfFont'], 'docxFont' => $arParam['docxFont']);
        $sParam = $this->uni2win(serialize($arParams));
        if($TemplateID > 0){
        	$query = "UPDATE htmls_docdesigner_templates SET template='" . $tmp . "', tmp_name='".$TemplateName."', orientation='".$PO."', common='".$CT."', params='".$sParam."' WHERE id=" . $TemplateID;//$arRes['id'];
			$result = $DB->Query($query);
		}
		else{
			$query = "INSERT INTO htmls_docdesigner_templates (bizproc, template, tmp_name, orientation, common, params) VALUES('" . $BPID . "', '" . $tmp . "','".$TemplateName."', '".$PO."','".$CT."','".$sParam."')";
			$result = $DB->Query($query);
			$TemplateID = $DB->LastID();
		}
		$this->CreateBlank($BPID, $TemplateID);
	}

	function CreateBlank($BPID, $TemplateID){
		$arProducts = $this->GetCrmProducts('blank');
		$arResult = $this->PrepareTable(array($BPID, intval($TemplateID)), array('arCrmProducts' => $arProducts), array());
        $arResult['mime'] = 'PDF';
        //$this->CreateFile($arResult, $BPID, 'blank', "/bitrix/tmp/htmls.docdesigner/");
		$this->CreateFile($arResult, 'type_' . $BPID.'_'.$TemplateID, 'blank', "/bitrix/tmp/htmls.docdesigner/");
	}

    function CreateFile($arResult, $DocType, $DocId, $path = '', $fname = ''){
      if($arResult['mime'] == 'PDF'){
        return $this->CreatePDF($arResult, $DocType, $DocId, $path, $fname);
      }
      else{
        return $this->CreateDocxEx($arResult, $DocType, $DocId, $path, $fname);
      }
    }
	/*
	function CreateFile
		return path to file
	$arResult - array with pdf-doc structure
	$DocType - folder in /upload/htmls.docdesigner/
		- ID bizproc
		- ID lead (LEAD_)
	$DocId - bizproc id
		- iblocck element ID if bizproc
		- date/time for crm
	$path - path for save file, if empty - bizproc, else - blank
	*/
	function CreatePDF($arResult, $DocType, $DocId, $path = '', $fname = ''){
		global $MESS;
		if($path == ''){
			if(!is_dir($_SERVER['DOCUMENT_ROOT'] . "/upload/htmls.docdesigner/"))
				mkdir($_SERVER['DOCUMENT_ROOT'] . "/upload/htmls.docdesigner/");

			if(!is_dir($_SERVER['DOCUMENT_ROOT'] . "/upload/htmls.docdesigner/".$DocType."/"))
				mkdir($_SERVER['DOCUMENT_ROOT'] . "/upload/htmls.docdesigner/".$DocType."/");
			$path = "/upload/htmls.docdesigner/".$DocType."/";
		}
		$page = 'P';
        if($arResult['page']) $page = $arResult['page'];
        //default params
        if(!$arResult['params']['pdfFont']){
          $arResult['params']['pdfFont'] = 'dejavusanscondensed';
        }
        if(!$arResult['params']['bg']){
          $arResult['params']['background'] = '';
        }
        else{
          $fid = $arResult['params']['bg'];
          if($fid > 0){
            $bgFile = CFile::GetPath($fid);
            $arResult['params']['background'] = $bgFile;
          }
          else{
            $arResult['params']['background'] = '';
          }
        }
        if(!$arResult['params']['lm']){$arResult['params']['lm'] = 15;}
        if(!$arResult['params']['rm']){$arResult['params']['rm'] = 15;}
        if(!$arResult['params']['tm']){$arResult['params']['tm'] = 27;}
        if(!$arResult['params']['bm']){$arResult['params']['bm'] = 25;}
        if(!$arResult['params']['pdfFont']){$arResult['params']['pdfFont'] = 'dejavusanscondensed';}

        //$pdf=new TCPDF($page, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$pdf=new cPDF($page, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        if($ff = $arResult['params']['ffooter']){$pdf->footer['ffooter'] = $ff;}
        if($l = $arResult['params']['lfooter']){$pdf->footer['left'] = $l;}
        if($m = $arResult['params']['mfooter']){$pdf->footer['middle'] = $m;}
        if($r = $arResult['params']['rfooter']){$pdf->footer['right'] = $r;}

        $pdf->footerValue = $arResult['footerValue'];
        $pdf->font = $arResult['params']['pdfFont'];

        $pdf->compositeFacsimile = '';
        if(strlen($arResult['params']['background']) > 10){
            $pdf->background = $arResult['params']['background'];
        }
        $h = 2;
		$l = Array();

		// PAGE META DESCRIPTORS --------------------------------------

		$l['a_meta_charset'] = 'UTF-8';
		$l['a_meta_dir'] = 'ltr';
		$l['a_meta_language'] = 'ru';

		// TRANSLATIONS --------------------------------------
		$l['w_page'] = 'page';
		$pdf->setLanguageArray($l);
        if(DOCDESIGNER_MODULE_DEMO){
			$pdf->SetHeaderData('logoDocDes.jpg', 20, 'DocDesigner - D E M O - M O D E', 'http://www.docdesigner.ru/ +7(495)797-66-07 ' . FormatDate("d F Y", htmls_docdesigner_SITEEXPIREDATE), array(0,64,255), array(0,64,128));
			// set header and footer fonts
			$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
			//$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

			// set default monospaced font
			$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

			//set margins
			$pdf->SetMargins($arResult['params']['lm'], $arResult['params']['tm'], $arResult['params']['rm']);
			$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		}
		else{
			//$pdf->setPrintHeader(false);
			//$pdf->setPrintFooter(false);
            $pdf->SetMargins($arResult['params']['lm'], $arResult['params']['tm'], $arResult['params']['rm']);
			//$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		}
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, $arResult['params']['bm']);
        //set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        if($arResult['fontSize'] > 0)
            $pdf->SetFont($arResult['params']['pdfFont'], '', $arResult['fontSize']);
        else
		    $pdf->SetFont($arResult['params']['pdfFont'], '', 10);
        $pdf->AddPage();
		if($arResult['html']){
		  /*$pref = '<ul>';
          foreach($arResult['params'] as $p => $v){
            $pref .= '<li>' . $p . ' => ' .$v . '</li>';
          }
          $pref .= '</ul>';*/
          $html = $arResult['html'];

          $this->html = $html;

          $this->calcConditions();
          $html = $this->html;

          //$html = '<style>p {margin:0px;}</style>' . $arResult['html'];

          $pos = strpos($html, '{CEOFacsimile}');
          if($pos > 0){
              $ceof = $pdf->CEOFacsimile($arResult['compositeFacsimile']);
              $params = ($pdf->serializeTCPDFtagParameters($ceof[0]));
              $tcpdf = '<tcpdf method="Image" params="'.$params.'" />';
              $params = ($pdf->serializeTCPDFtagParameters($ceof[1]));
              $tcpdf .= '<tcpdf method="Image" params="'.$params.'" />';
              $html = str_replace('{CEOFacsimile}', $tcpdf, $html);
          }

          $pos = strpos($html, '{CompositeFacsimile}');
          if($pos > 0){
              $arHtml = explode('{CompositeFacsimile}', $html);
              $cnt = 0;
              foreach($arHtml as $html){
                if(strlen($html) == 0) break;
                  $pdf->writeHTML($html, true, false, true, false, '');
                  $cnt++;
                  if($cnt < count($arHtml))
                      $pdf->writeCompositeFacsimile($arResult['compositeFacsimile']);
              }
          }
          else{
  		    $pdf->writeHTML($html, true, false, true, false, '');
          }
			//$pdf->writeHTML($arResult['html'], true, false, true, false, '');
		}
		else{
			$Font = $arResult['params']['pdfFont'];
			$FontBD = $arResult['params']['pdfFont'];
			$cs = 'utf-8';
			$Bold = '';

			$pdf->SetFont($Font,'',12);

			$arTable = $arResult['arTable'];
			$border = ($arResult['border'] == 1)? true : false;
			$imgs = array('.png', '.jpg', '.jpeg', '.gif');
			foreach($arTable as $row){
				$pdf->SetWidths($row['width']);
				$pdf->SetAligns($row['align']);
				$str = implode(';', $row['row']);
				$img = '';
				$delta = 10;
				$currentY = $pdf->GetY();
				foreach($imgs as $type){
					if(strpos(strtolower($str), $type) > 0){
						foreach($row['row'] as $k => $data){
							if(strpos(strtolower($data), $type) > 0){
								$img = $data;
								$imgK = $k;
								$row['row'][$k] = '';
								break;
							}
							$delta = $delta + $row['width'][$k];
						}
						break;
					}
				}
				//else
				if(count($row['borders']) == 6){
					//invoice
					$border = true;
					$row['borders'] = array();
				}
				else{
					$border = ($arResult['border'] == 1)? true : false;
				}
				foreach($row['row'] as $k => $data){
					$row['row'][$k] = $this->win2uni($data);
				}
				$pdf->Row($row['row'], $border, $row['fills'], $row['borders']);

				if(!empty($img)){
					$Y = $pdf->GetY();
					if(strpos($img, '://') > 0){
						$pdf->Image($img, $delta, $currentY);//clouds
						$size = getimagesize($img);
					}
					else{
						$pdf->Image($_SERVER["DOCUMENT_ROOT"] . $img, $delta, $currentY);
						$size = getimagesize($_SERVER["DOCUMENT_ROOT"] . $img);
					}
					$down = $currentY + $size[1]*0.3526829268;
					if($Y < $currentY + $down)
						$pdf->SetY($currentY + $down);
				}
			}
		}
        $pdf->setFontSubsetting(false);
		$pdf->SetCreator('Created by DocDesigner.ru');
		$pdf->SetAuthor('HTMLStudio www.htmls.ru');
		if($arResult['fname'] == GetMessage('NO_NUM'))
			$arResult['fname'] = time();

		$fname = CUtil::translit($arResult['fname'], 'ru', array("change_case" => false, "replace_other" => '-'));

        if($fname == ''){
        	if($_fname = $this->GetFName($DocType)){
        		$filename = $_fname . $DocId . ".pdf";
        	}
        	else{
				$filename = $DocType . "_" . $DocId . ".pdf";
			}
		}
		else{
			if(trim($fname) == '-') $fname = time();
			if($_fname = $this->GetFName($DocType)){
        		$filename = $_fname . $fname . ".pdf";
        	}
        	else{
				$filename = $fname . ".pdf";
			}
		}

		$path = $_SERVER["DOCUMENT_ROOT"].$path;
		$pdf->Output($path . $filename, "F");
		return "/upload/htmls.docdesigner/".$DocType."/" . $filename;
	}//CreatePdf

    function CreateDocxEx($arResult, $DocType, $DocId, $path = '', $fname = ''){
		global $MESS;
        $start = microtime();
		if($path == ''){
			if(!is_dir($_SERVER['DOCUMENT_ROOT'] . "/upload/htmls.docdesigner/"))
				mkdir($_SERVER['DOCUMENT_ROOT'] . "/upload/htmls.docdesigner/");

			if(!is_dir($_SERVER['DOCUMENT_ROOT'] . "/upload/htmls.docdesigner/".$DocType."/"))
				mkdir($_SERVER['DOCUMENT_ROOT'] . "/upload/htmls.docdesigner/".$DocType."/");
			$path = "/upload/htmls.docdesigner/".$DocType."/";
		}
		$page = 'P';
		if($arResult['page']) $page = $arResult['page'];

		if($arResult['fname'] == GetMessage('NO_NUM'))
			$arResult['fname'] = time();

		$fname = CUtil::translit($arResult['fname'], 'ru', array("change_case" => false, "replace_other" => '-'));
        if($fname == ''){
        	if($_fname = $this->GetFName($DocType)){
        		$filename = $_fname . $DocId . ".docx";
        	}
        	else{
				$filename = $DocType . "_" . $DocId . ".docx";
			}
		}
		else{
			if(trim($fname) == '-') $fname = time();
			if($_fname = $this->GetFName($DocType)){
        		$filename = $_fname . $fname . ".docx";
        	}
        	else{
				$filename = $fname . ".docx";
			}
		}
		$path = $_SERVER["DOCUMENT_ROOT"].$path;

        $domain = $_SERVER['HTTP_X_FORWARDED_PROTO'] . '://' . $_SERVER['HTTP_HOST'];
        $html2 = str_replace('img src="..', 'img src="'.$domain, $arResult['html']);
        $html2 = str_replace('img src="/var/www/', 'img src="'.$domain, $html2);

        $this->html = $html2;
        $this->calcConditions();
        $html2 = $this->html;

        $fid = $arResult['params']['bg'];
        if($fid > 0){
          $bgFile = CFile::GetPath($fid);
          $arResult['params']['bg'] = $domain .$bgFile;
        }
        else{
          $arResult['params']['bg'] = '';
        }

        $arPost = $this->encrypt($html2);
        $arPost['page'] = $page;
        $arPost['params'] = $arResult['params'];
        $arPost['footerValue'] = $arResult['footerValue'];
        $arPost['domain'] = $_SERVER['HTTP_HOST'];
        $res = $this->PostUrl($arPost);
        //mail('dev@htmls.ru', 'res', $res);
        //AddMessage2Log($res, "htmls.DocDesigner");
        //$step = true;
        $fname = md5($arPost['key']);

        $url='https://www.htmls.pw/dd/tmp/' . $fname.'.docx';
        $file=$_SERVER["DOCUMENT_ROOT"]."/upload/htmls.docdesigner/".$DocType."/" . $filename;
        //AddMessage2Log($url, "htmls.DocDesigner");
        //AddMessage2Log($file, "htmls.DocDesigner");
        $step = 0;
        //while(true){
          // ��������� ����, �� �������, �� ������
          $dest_file = @fopen($file, "w");

          // ��������� cURL-������
          $resource = curl_init();

          // ������������� ����� ���������� �����
          curl_setopt($resource, CURLOPT_URL, $url);

          // ������������� ����� �� �������, ���� ����� ���������� ��������� ����
          curl_setopt($resource, CURLOPT_FILE, $dest_file);

          // ��������� ��� �� �����
          curl_setopt($resource, CURLOPT_HEADER, 0);

          // ��������� ��������
          curl_exec($resource);

          // ��������� cURL-������
          curl_close($resource);

          // ��������� ����
          fclose($dest_file);
          //AddMessage2Log($step, "htmls.DocDesigner");
          //sleep(1);
          //$step++;
          //if($step > 10) break;
        //}
        //file_put_contents($_SERVER["DOCUMENT_ROOT"] . "/upload/htmls.docdesigner/".$DocType."/" . $filename, $str);
		return "/upload/htmls.docdesigner/".$DocType."/" . $filename;
	}//CreateDocx

    function encrypt($html){
      $useMcrypt = COption::GetOptionString("htmls.docdesigner", 'DOCDESIGNER_CLOUD_MCRYPT', 'N');
      $chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
      $length = 49;
      $n = 61;
      $key = '';
      for ($i = 0; $i < $length; $i++)
          $key .= $chars[mt_rand(0, $n)];
      if($useMcrypt == 'Y'){
        $encrypted_data = $this->mc_encrypt($html, $key);
      }
      else{
        $encrypted_data = base64_encode($html);
      }
      $enc_key =$this->my_enc($key);
      return array('data'=>$encrypted_data, 'key'=>$enc_key, 'note' => $useMcrypt);
    }

    function mc_encrypt($encrypt, $key){
      $encrypt = serialize($encrypt);
      $iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC), MCRYPT_DEV_URANDOM);
      $key = pack('H*', $key);
      $mac = hash_hmac('sha256', $encrypt, substr(bin2hex($key), -32));
      $passcrypt = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $encrypt.$mac, MCRYPT_MODE_CBC, $iv);
      $encoded = base64_encode($passcrypt).'|'.base64_encode($iv);
      return $encoded;
    }

    function my_enc($str) {
		$path = $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/htmls.docdesigner/public.key";
		$fpr = fopen($path,"r");
		$pub_key = fread($fpr,1024);
		fclose($fpr);
		openssl_public_encrypt($str,$result,$pub_key);
		return base64_encode($result);
	}

    function PostUrl($arFields) {
      //AddMessage2Log(print_r($arFields, true));

      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, "https://www.htmls.pw/dd/docx.php");
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_USERAGENT, 'PHP/' . phpversion());
      curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($arFields));
      curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
      $response = curl_exec($ch);

      return $response;

  }

  function calcConditions(){
    global $MESS;
    $this->dateFormat = 'd.m.Y';

    preg_match_all("/\[CALC]([^[]*)\[\/CALC]/i", $this->html, $fl_array3);

    foreach($fl_array3[1] as $idx => $exp){
      if(strpos($exp, 'substr') > 0){
        try{
            eval("\$ev = ".$exp.";");
        } catch (Exception $e) {
            $ev = 'eval error: '.  $e->getMessage(). " (".$exp.")";
        }
      }
      if(strpos($exp, 'r_replace') > 0){
        try{
            eval("\$ev = ".$exp.";");
        } catch (Exception $e) {
            $ev = 'eval error: '.  $e->getMessage(). " (".$exp.")";
        }
      }
      elseif(strpos($exp, 'ddDate') > 0){
        preg_match_all("/addDate\(([^[]*)\)/i", $exp, $fl_array4);
        $ev = '';
        if($_str = $fl_array4[1][0]){
          $a = explode(',', $_str);
          if(count($a) == 3){
            $_period = trim($a[2]);
            $_delta = trim($a[1])*1;
            $_date = $this->uni2win(trim($a[0]));
            if($_period == 'm'){
              if($_delta > 0){
                $ev = date($this->dateFormat, strtotime($_date.' +'.$_delta.' month'));
              }
              else{
                $ev = date($this->dateFormat, strtotime($_date.' '.$_delta.' month'));
              }
            }
            elseif($_period == 'd'){
              if($_delta > 0){
                $ev = date($this->dateFormat, strtotime($_date.' +'.$_delta.' day'));
              }
              else{
                $ev = date($this->dateFormat, strtotime($_date.' '.$_delta.' day'));
              }
            }
            elseif($_period == 'w'){
              if($_delta > 0){
                $ev = date($this->dateFormat, strtotime($_date.' +'.$_delta.' week'));
              }
              else{
                $ev = date($this->dateFormat, strtotime($_date.' '.$_delta.' week'));
              }
            }
            elseif($_period == 'y'){
              if($_delta > 0){
                $ev = date($this->dateFormat, strtotime($_date.' +'.$_delta.' year'));
              }
              else{
                $ev = date($this->dateFormat, strtotime($_date.' '.$_delta.' year'));
              }
            }
          }
        }

      }
      else{
        //AddMessage2Log("\$ev = number_format((".str_replace(',', '.', str_replace(' ', '', $exp))."), 2, '.', ' ');");
        try{
            eval("\$ev = number_format((".str_replace(',', '.', str_replace(' ', '', $exp))."), 2, '.', ' ');");
        } catch (Exception $e) {
            $ev = 'eval error: '.  $e->getMessage(). " (".$exp.")";
        }
      }
      $this->replace($fl_array3[0][$idx], $ev);
    }

    $num2str = new CNum2Str;

    if($this->CURRENCY_ID != 'RUB'){
          if(file_exists($GLOBALS["DOCUMENT_ROOT"]."/bitrix/modules/htmls.docdesigner/lang/ru/".$this->CURRENCY_ID.'.php')){
            require($GLOBALS["DOCUMENT_ROOT"]."/bitrix/modules/htmls.docdesigner/lang/ru/".$this->CURRENCY_ID.'.php');

            $num2str->w_1_2 = $MESS['_w_1_2'];
    		$num2str->w_1_19 = $MESS['_w_1_19'];
    		$num2str->des = $MESS['_des'];
    		$num2str->hang = $MESS['_hang'];
    		$num2str->namerub = $MESS['_namerub'];
    		$num2str->nametho = $MESS['_nametho'];
    		$num2str->namemil = $MESS['_namemil'];
    		$num2str->namemrd = $MESS['_namemrd'];
    		$num2str->kopeek = $MESS['_kopeek'];
          }
        }
    preg_match_all("/\[SPELL]([^[]*)\[\/SPELL]/i", $this->html, $fl_array3);

    if(count($fl_array3) > 0){
        //$CNum2Str = new CNum2Str($this->lang, $this->ceil);
        foreach($fl_array3[1] as $idx => $exp){
          try{
              $val2 = $num2str->num2str(str_replace(' ', '', $exp));
          } catch (Exception $e) {
              $val2 = 'eval error: '.  $e->getMessage(). " (".$exp.")";
          }
          $s = trim($this->win2uni($val2));
          $val2 = $this->first_letter_up($s);
          
          $this->replace($fl_array3[0][$idx], $val2);
      }

    }

    preg_match_all("/\[NUM2SPELL]([^[]*)\[\/NUM2SPELL]/i", $this->html, $fl_array3);
    if(count($fl_array3) > 0){
        foreach($fl_array3[1] as $idx => $exp){
          try{

              $val2 = $num2str->num2spell(str_replace(' ', '', $exp));
          } catch (Exception $e) {
              $val2 = 'eval error: '.  $e->getMessage(). " (".$exp.")";
          }
          $s = trim($this->win2uni($val2));
          $val2 = $this->first_letter_up($s);
          $this->replace($fl_array3[0][$idx], $val2);
      }

    }

    preg_match_all("/\[([a-z]+)([^\]]+)*(?:](.*?)\[\/[a-z]+])/mus", html_entity_decode($this->html), $fl_array4, PREG_OFFSET_CAPTURE);
      $result = array();
      $z = array('>', '<', '==', '!=', '>=', '<=');
      if(count($fl_array4) > 0){
        foreach($fl_array4[0] as $id => $arr){
          $cond = $fl_array4[2][$id][0];
          foreach($z as $_z){
            $a = explode($_z, $cond);
            if(count($a) > 1){

              if($a[0] * 1 == 0){
                //fix
                $k = trim(str_replace("'", "", str_replace('"', "", $a[1])));
                if($s = $_SESSION['DealStages'][$k]){
                  $a[1] = '"'.$s.'"';
                }
                if($s = $_SESSION['DealTypes'][$k]){
                  $a[1] = '"'.$s.'"';
                }

                $cond = '"'.trim($a[0]).'"'.$_z.$a[1];
                break;
              }
            }
          }

          $result[$id]['cond'] = $cond;//$fl_array4[2][$id][0];
          $result[$id]['val'] =  $fl_array4[3][$id][0];
          $result[$id]['op'] = $fl_array4[1][$id][0];

          $s = $arr[0];
          if($fl_array4[1][$id][0] == 'if'){

              try{
                  eval("\$ev = (".$cond.")? 1 : 0;");
              } catch (Exception $e) {
                  $ev = 'eval error: '.  $e->getMessage(). " (".$cond.")";
              }
              $result[$id]['eval'] = $ev;
          }
          if($fl_array4[1][$id][0] == 'elseif'){
            try{
                  eval("\$ev = (".$cond.")? 1 : 0;");
              } catch (Exception $e) {
                  $ev = 'eval error: '.  $e->getMessage(). " (".$cond.")";
              }
            $result[$id]['eval'] = $ev;
          }
          if($fl_array4[1][$id][0] == 'else'){
          }
          $result[$id]['reg'] = html_entity_decode(html_entity_decode($s));//$arr[0][0];//
        }
      }
      $lastIf = false;
      $this->html = html_entity_decode($this->html);
      for($i = 0; $i < count($result); $i++){
        if($result[$i]['op'] == 'if' || $result[$i]['op'] == 'elseif'){
          $e = $result[$i]['eval'];
          if($e == 1 && !$lastIf){
            $this->replace($result[$i]['reg'], ($result[$i]['val']));
            $lastIf = true;
            $result[$i]['lastIf'] = $lastIf;
          }
          else{
            $this->replace($result[$i]['reg'], '');
            $lastIf = false || $lastIf;
          }

          if($result[$i+1]['op'] == 'else'){
            $result[$i+1]['lastIf'] = $lastIf;
            if($lastIf)
                $this->replace($result[$i+1]['reg'], '');
            else
                $this->replace($result[$i+1]['reg'], ($result[$i+1]['val']));
            $i++;
            $lastIf = false;
          }
        }
      }
  }
  function replace($that, $than){
    $this->html = str_replace($that, $than, $this->html);
  }

  function first_letter_up($string, $coding='utf-8') {
    //if(strtoupper(LANG_CHARSET) == 'WINDOWS-1251'){
      //$string = self::win2uni($string);
      //$string = ucfirst($string);
      //$string = strtoupper(substr($string, 0, 1)) . substr($string, 1, strlen($string));
    //}
    //else{
      if (function_exists('mb_strtoupper') && function_exists('mb_substr') && !empty($string)) {
        preg_match('#(.)#us', mb_strtoupper(mb_strtolower($string, $coding), $coding), $matches);
        $string = $matches[1] . mb_substr($string, 1, mb_strlen($string, $coding), $coding);
      }
      else {
        $string = ucfirst($string);
      }
    //}
    return $string;
  }
	/*
	function GetWorkflowTemplateId($InstanceId){
		global $DB;
		$dbRes = $DB->Query("SELECT WORKFLOW_TEMPLATE_ID FROM b_bp_workflow_state WHERE ID = '" . $InstanceId . "'");
		while ($arRes = $dbRes->Fetch()){
			return $arRes['WORKFLOW_TEMPLATE_ID'];
		}
	}
	*/
	function win2uni($s = ''){
	  //if(strlen($s) == 0) return $s;
	  	if(strtoupper(LANG_CHARSET) == 'WINDOWS-1251'){
	  		$s = iconv("cp1251", "UTF-8", $s);
	  	}
		return ($s);
	}
    function uni2win($s = ''){
	  //if(strlen($s) == 0) return $s;
	  	if(strtoupper(LANG_CHARSET) == 'WINDOWS-1251'){
	  		$s = iconv("UTF-8", "cp1251", $s);
	  	}
		return ($s);
	}
//end class CDocDesigner
}
?>
