<?
//v3.6.3 17.07.2012
if(COption::GetOptionString('htmls.docdesigner', 'DOCDESIGNER_ADDMESSAGE2LOG') == 'Y'){
	define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/bitrix/tmp/DocDesigner.txt");
}
if(CModule::IncludeModuleEx("htmls.docdesigner") < 3){
	$rootActivity = $this->GetRootActivity();
	$arProperties = $rootActivity->arProperties;
	$arPropertiesTypes = $rootActivity->arPropertiesTypes;
	$workflowId = $this->workflow->GetInstanceId();
	CDocDesignerBPActivities::CreateDocx($rootActivity, $arProperties, $arPropertiesTypes, $workflowId);
}
?>
