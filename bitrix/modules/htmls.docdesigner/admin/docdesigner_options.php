<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
if($_REQUEST['mode'] == 'scan'){
	IncludeModuleLangFile(__FILE__);
	CModule::IncludeModule('lists');
	$CList = new CList($_REQUEST['listID']);
	$arFields = $CList->GetFields();
	if(strtoupper(LANG_CHARSET) == 'WINDOWS-1251'){
		$arProp['no'] = iconv('cp1251', 'utf-8', GetMessage("DOCDESIGNER_NO_SELECT"));
	}
	else{
		$arProp['no'] = GetMessage("DOCDESIGNER_NO_SELECT");
	}
	foreach($arFields as $k => $arr){
		if(strpos($k, 'ROPERTY_') > 0){
			if(strtoupper(LANG_CHARSET) == 'WINDOWS-1251'){
				$arProp[$arr['ID']] = iconv('cp1251', 'utf-8', $arr['NAME']);
			}
			else{
				$arProp[$arr['ID']] = $arr['NAME'];
			}
		}
	}
	$arProp = json_encode($arProp);
	echo $arProp;
}
?>