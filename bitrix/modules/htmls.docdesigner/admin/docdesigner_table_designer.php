<?
/*******************************************************************************
* Module:   DocDesigner                                                        *
* Version:  8.4.1                                                              *
* Author:   Alexey Andreev, HTMLStudio (www.htmls.ru)                          *
* License:  Shareware                                                          *
* Date:     2016-07-03                                                         *
* e-mail:   aav@htmls.ru                                                       *
*******************************************************************************/
//bitrixtemplates.defaultcomponentsbitrixbizproc.workflow.edithtmls.bizproc.workflow.edittemplate.php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");

//CJSCore::Init(array('jquery'));

//include_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/htmls.docdesigner/include.php");
IncludeModuleLangFile(__FILE__);
$DocumentType = $_REQUEST['document_type'];
if(substr($DocumentType, 0, 6) == 'iblock'){
  $DocID = $DocumentType;
}
else{
    $DocID = substr($DocumentType, strlen("type_"));
}
//echo $DocumentType;
$TemplateID = $_REQUEST['template_id'];
$incParam = $_REQUEST;
CUtil::DecodeUriComponent($incParam);

$DemoExp = true;
if(CModule::IncludeModuleEx("htmls.docdesigner") < 3){
	$DemoExp = false;
	$CDocDes = new CDocDesignerProcessing;
	CModule::IncludeModule("crm");
    //AddMessage2Log(print_r($_REQUEST, true), 'request');
    //AddMessage2Log(print_r($_FILES, true), 'files');
    $incParam = array_merge_recursive($_REQUEST, $_FILES);
	if ($_REQUEST["save"] == "Y"){
	  define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/bitrix/tmp/DocDesigner.txt");
	  //AddMessage2Log(print_r($_REQUEST, true));
		echo 'SaveOk';
		$CDocDes->SetTemplate($DocID, $incParam);//['table'], $incParam['template_id'], $incParam['template_name']);
		return;
	}
	$Commom = ''; $bgFile = '';
	if(intval($TemplateID) > 0){
		$arTemplate = $CDocDes->GetTemplateAr($TemplateID);
		$TemplateName = $arTemplate['tmp_name'];
		if($arTemplate['common'] =='Y') $Common = ' checked';
        if(strlen($arTemplate['params']) > 0){
          $arParams = unserialize($arTemplate['params']);
          if($fid = $arParams['bg']){
            $bgFile = CFile::GetPath($fid);
            //$arBgFile = $rsFile->Fetch();
          }
          //print_r($arParams);
          //print_r($bgFile);
        }
        else{
          $arParams = array('lm'=>15, 'bm' => 25, 'tm' => 27, 'rm' => 15, 'ffooter' => 8);
        }
        if(!$arParams['pdfFont']){
          $arParams['pdfFont'] = 'dejavusanscondensed';
        }
	}
	else{
		$TemplateName = GetMessage('DOCDESIGNER_NEW_TEMPLATE');
        $arParams = array('lm'=>15, 'bm' => 25, 'tm' => 27, 'rm' => 15, 'ffooter' => 8);
	}
}
?>
<script src="/bitrix/js/htmls.docdesigner/ckeditor/ckeditor.js"></script>
<script src="/bitrix/js/htmls.docdesigner/ckeditor/adapters/jquery.js"></script>
<script src="/bitrix/js/htmls.docdesigner/ckeditor/menu.min.js"></script>
<script src="/bitrix/js/htmls.docdesigner/jquery.contextmenu.js"></script>
<link rel="stylesheet" type="text/css" href="/bitrix/css/htmls.docdesigner/jquery.contextmenu.css" />
<script src="/bitrix/js/htmls.docdesigner/jquery.dropdownselect.js"></script>
<link rel="stylesheet" type="text/css" href="/bitrix/css/htmls.docdesigner/jquery.dropdownselect.css" />
<?
$iUniqId = '';//rand();
$APPLICATION->ShowTitle(GetMessage("DOCDESIGNER_TABLE_DOC_TITLE"));

//echo '<pre>';print_r($incParam["arWorkflowVariables"]);echo '</pre>';


require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
?>
<script type="text/javascript">

BX.WindowManager.Get().SetTitle('<?= GetMessage("DOCDESIGNER_TABLE_DOC_TITLE") ?>');
function WFSFSave()
{
	var perm	= {
		'save'			: 'Y',
		'document_type'	: '<?=$incParam['document_type']?>',
		'template_id'	: '<?=$incParam['template_id']?>',
		'template_name'	: document.getElementById("template_name").value,
		'page_orientation'	: document.getElementById("page_orientation").value,
		'template_common'	: document.getElementById("template_common").checked,
        'lm'	: document.getElementById("leftMargin").value,
        'rm'	: document.getElementById("rightMargin").value,
        'tm'	: document.getElementById("topMargin").value,
        'bm'	: document.getElementById("bottomMargin").value,
        'lfooter'	: document.getElementById("left-footer").value,
        'mfooter'	: document.getElementById("middle-footer").value,
        'rfooter'	: document.getElementById("right-footer").value,
        'ffooter'	: document.getElementById("font-size").value
	};
    //console.log(perm); return;

    //var form = document.getElementById("docdesignerform");
    //var fd = new FormData(form);
    //console.log(form);
    //perm['fd'] = fd;
	//var perm = 'save=Y&document_type=<?=$incParam['document_type']?>&template_id=<?=$incParam['template_id']?>&template_name='+encodeURIComponent(document.getElementById("template_name").value);
	<?foreach ($arAllowableOperations as $op_id):?>
		perm['perm[<?= $op_id ?>]']	= document.getElementById('<?= $op_id ?>').value;
		//perm += '&perm[<?= $op_id ?>]='+encodeURIComponent(document.getElementById('<?= $op_id ?>').value);
	<?endforeach;?>

	perm['table']	= tmpEditor.getData();//tinyMCE.activeEditor.getContent({format : 'raw'});
    $('#template_body').val(tmpEditor.getData());
	BX.showWait();
    /*
	BX.ajax({
		'url': '/bitrix/admin/docdesigner_table_designer.php?lang=<?=LANGUAGE_ID ?>&entity=CBPVirtualDocument&time=<?=time()?>',
		'method': 'POST',
		'data': perm,
		'dataType': 'json',
		'timeout': 10,
		'processData': false,
		'async': false,
		'start': true,
		'onsuccess': WFSSaveOK,
		'onfailure': WFSSaveN
	});
    */
    //document.forms.docdesignerform.submit();
    f = BX.ajax.prepareForm(document.forms.docdesignerform);
    //console.log(f);
    //console.log(document.forms.docdesignerform);

    BX.ajax.submitAjax(
                        document.forms.docdesignerform,
                        {
                            method: "POST",
                            url: "/bitrix/admin/docdesigner_table_designer.php",
                            processData: false,
                            onsuccess: function()
                            {
                                WFSSaveOK();




                            }
                        }
                    );
                    //BX.showWait();
}

function WFSSaveN(o)
{
	BX.closeWait();
	alert('<?=GetMessage("DOCDESIGNER_DOC_SAVEERR")?>');
}

function WFSSaveOK(permissions)
{
	BX.closeWait();

	rootActivity['Properties']['Permission'] = permissions;
    /*$('#menu_tinymce_contextmenu').hide();
    if (typeof tinyMCE != 'undefined') {
        $(this).find(':tinymce').each(function () {
          var theMce = $(this);

          tinyMCE.execCommand('mceFocus', false, theMce.attr('id'));
          tinyMCE.execCommand('mceRemoveControl', false, theMce.attr('id'));
          $(this).remove();
        });
        //tinyMCE = '';
    }*/
	BX.WindowManager.Get().CloseDialog();
    window.location=window.location;
    location.reload(true);
}

function _switch(div){
  if(div == 't'){
    $('#ddTemplate').hide();
    $('#ddParams').show();
  }
  else{
    $('#ddTemplate').show();
    $('#ddParams').hide();
  }
}

function _help(){
  window.open('https://www.htmls.pw/learning/course/index.php?COURSE_ID=2&CHAPTER_ID=091&LESSON_PATH=60.84.85.91');
}
</script>

<form id="docdesignerform" enctype="multipart/form-data" name="docdesignerform" method="post" action ="/bitrix/admin/docdesigner_table_designer.php">
<div id="ddTemplate">
<input style="float: right; background:url('/bitrix/images/htmls.docdesigner/help.png') no-repeat 50% 50% !important;" type="button" onclick="_help();"/>
<input style="background:url('/bitrix/images/htmls.docdesigner/ic_settings.png') no-repeat 50% 50% !important;" type="button" onclick="_switch('t');"/>
<input type="hidden" name="save" value ="Y" />
<input type="hidden" name="document_type" value ="<?=$incParam['document_type']?>" />
<input type="hidden" name="template_id" value ="<?=$incParam['template_id']?>" />
<input type="hidden" name="template_body" value ="" id="template_body" />
<?=bitrix_sessid_post()?>
<?if(!$DemoExp):?>
<?
$selectPO = '<select name="page_orientation" id="page_orientation">';
if($arTemplate['orientation'] != 'L'){
	$selectPO .= "<option value='P' selected>" . GetMessage("DOCDESIGNER_TEMPLATE_PORTRAIT") . "</option>";
	$selectPO .= "<option value='L'>" . GetMessage("DOCDESIGNER_TEMPLATE_LANDSCAPE") . "</option>";
}
else{
	$selectPO .= "<option value='P'>" . GetMessage("DOCDESIGNER_TEMPLATE_PORTRAIT") . "</option>";
	$selectPO .= "<option value='L' selected>" . GetMessage("DOCDESIGNER_TEMPLATE_LANDSCAPE") . "</option>";

}
$selectPO .= '</select>';
?>
	<?=GetMessage('DOCDESIGNER_TEMPLATE_NAME')?> <input type="text" name="template_name" id="template_name" value="<?=$TemplateName?>" />
	<?=GetMessage('DOCDESIGNER_TEMPLATE_ORIENATION')?><?=$selectPO?>
	<input type="checkbox" name="template_common" id="template_common" <?=$Common?> /><?=GetMessage('DOCDESIGNER_TEMPLATE_COMMON')?><br/><br/>
<?endif;?>
<?
$CompanyIB = COption::GetOptionString('htmls.docdesigner', 'DOCDESIGNER_COMPANY', 0);
//get sys props
$SET = "COMPANY_OGRN,COMPANY_INN,COMPANY_KPP,COMPANY_RS,COMPANY_POST_ADDRESS,COMPANY_ADDRESS_LEGAL,COMPANY_ADDRESS,COMPANY_PHONE,COMPANY_PERSON,COMPANY_BANK_NAME,COMPANY_BANK_KS,COMPANY_BANK_CITY,COMPANY_BANK_BIK,COMPANY_INVOICE_FACSIMILE,COMPANY_FAKSIMILE,COMPANY_BANK";
$arSet = explode(',',$SET);
foreach($arSet as $str){
	$n = COption::GetOptionString('htmls.docdesigner', 'DOCDESIGNER_' . $str);
	$arSys[$n] = "PROPERTY_" . $n;
}

$properties = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>$CompanyIB));
while ($prop_fields = $properties->GetNext()){
	if(array_key_exists($prop_fields['ID'], $arSys)) continue;
	if($prop_fields['CODE'])
		$arCustomFields[$prop_fields["CODE"]] = $prop_fields["NAME"];
	else
    	$arCustomFields[$prop_fields["ID"]] = $prop_fields["NAME"];
}
//print_r($arCustonFields);
if($CompanyIB > 0){
	$arCompany['Company.NAME'] = GetMessage('DOCDESIGNER_TABLE_COMPANY_NAME');
	$arCompany['Company.INN'] = GetMessage('DOCDESIGNER_TABLE_COMPANY_INN');
	$arCompany['Company.KPP'] = GetMessage('DOCDESIGNER_TABLE_COMPANY_KPP');
	$arCompany['Company.OGRN'] = GetMessage('DOCDESIGNER_TABLE_COMPANY_OGRN');
	$arCompany['Company.BANK_NAME'] = GetMessage('DOCDESIGNER_TABLE_COMPANY_BANK_NAME');
	$arCompany['Company.BANK_BIK'] = GetMessage('DOCDESIGNER_TABLE_COMPANY_BANK_BIK');
	$arCompany['Company.BANK_RS'] = GetMessage('DOCDESIGNER_TABLE_COMPANY_BANK_RS');
	$arCompany['Company.BANK_KS'] = GetMessage('DOCDESIGNER_TABLE_COMPANY_BANK_KS');
	$arCompany['Company.BANK_CITY'] = GetMessage('DOCDESIGNER_TABLE_COMPANY_BANK_CITY');
	$arCompany['Company.ADDRESS_LEGAL'] = GetMessage('DOCDESIGNER_TABLE_COMPANY_ADDRESS_LEGAL');
	$arCompany['Company.ADDRESS'] = GetMessage('DOCDESIGNER_TABLE_COMPANY_ADDRESS');
	$arCompany['Company.POST_ADDRESS'] = GetMessage('DOCDESIGNER_TABLE_COMPANY_POST_ADDRESS');

	$arCompany['Company.PHONE'] = GetMessage('DOCDESIGNER_TABLE_COMPANY_PHONE');

	//$arCompany[] = '--------------------------';
    $arCompany['Company.CEO_FULL'] = GetMessage('DOCDESIGNER_TABLE_COMPANY_CEO_FULL');
    $arCompany['Company.CEO_SHORT'] = GetMessage('DOCDESIGNER_TABLE_COMPANY_CEO_SHORT');
	$arCompany['Company.BigLogoImg'] = GetMessage('DOCDESIGNER_TABLE_COMPANY_BIG_LOGO');
	$arCompany['Company.SmallLogoImg'] = GetMessage('DOCDESIGNER_TABLE_COMPANY_SMALL_LOGO');
	$arCompany['Company.BankDetails'] = GetMessage('DOCDESIGNER_TABLE_COMPANY_BANK_DETAILS');
	$arCompany['Company.CEO_FACSIMILEImg'] = GetMessage('DOCDESIGNER_TABLE_COMPANY_SEO_FACSIMILE');
	$arCompany['Company.INVOICE_FACSIMILEImg'] = GetMessage('DOCDESIGNER_TABLE_COMPANY_INVOICE_FACSIMILE');

    if(count($arCustomFields) > 0){
		foreach($arCustomFields as $pid => $pname){
			$arCompany['Company.'. $pid] = $pname;
		}
	}
    $arCompany['CompositeFacsimile'] = GetMessage('DOCDESIGNER_TABLE_COMPANY_COMPOSITE_FACSIMILE');
    $arCompany['CEOFacsimile'] = GetMessage('DOCDESIGNER_TABLE_COMPANY_CEO_FACSIMILE');
}

//if(substr($DocID, 0, 4) == 'DEAL'){
	$arCrmCompany['CrmCompany.NAME'] = GetMessage('DOCDESIGNER_TABLE_COMPANY_NAME');
	$arCrmCompany['CrmCompany.OGRN'] = GetMessage('DOCDESIGNER_TABLE_COMPANY_OGRN');
	$arCrmCompany['CrmCompany.INN'] = GetMessage('DOCDESIGNER_TABLE_COMPANY_INN');
	$arCrmCompany['CrmCompany.KPP'] = GetMessage('DOCDESIGNER_TABLE_COMPANY_KPP');
	$arCrmCompany['CrmCompany.BANK_NAME'] = GetMessage('DOCDESIGNER_TABLE_COMPANY_BANK_NAME');
	$arCrmCompany['CrmCompany.BANK_BIK'] = GetMessage('DOCDESIGNER_TABLE_COMPANY_BANK_BIK');
	$arCrmCompany['CrmCompany.BANK_RS'] = GetMessage('DOCDESIGNER_TABLE_COMPANY_BANK_RS');
	$arCrmCompany['CrmCompany.BANK_KS'] = GetMessage('DOCDESIGNER_TABLE_COMPANY_BANK_KS');
	$arCrmCompany['CrmCompany.BANKING_DETAILS'] = GetMessage('DOCDESIGNER_BANKING_DETAILS');
	$arCrmCompany['CrmCompany.ADDRESS_LEGAL'] = GetMessage('DOCDESIGNER_TABLE_COMPANY_ADDRESS_LEGAL');//.GetMessage('DOCDESIGNER_TABLE_UA');
    $arCrmCompany['CrmCompany.REG_ADDRESS'] = GetMessage('DOCDESIGNER_TABLE_ADDRESS_STREET').GetMessage('DOCDESIGNER_TABLE_UA');
    $arCrmCompany['CrmCompany.REG_ADDRESS_2'] = GetMessage('DOCDESIGNER_TABLE_ADDRESS_2').GetMessage('DOCDESIGNER_TABLE_UA');
    $arCrmCompany['CrmCompany.REG_ADDRESS_CITY'] = GetMessage('DOCDESIGNER_TABLE_CITY').GetMessage('DOCDESIGNER_TABLE_UA');
    $arCrmCompany['CrmCompany.REG_ADDRESS_REGION'] = GetMessage('DOCDESIGNER_TABLE_REGION').GetMessage('DOCDESIGNER_TABLE_UA');
    $arCrmCompany['CrmCompany.REG_ADDRESS_PROVINCE'] = GetMessage('DOCDESIGNER_TABLE_PROVINCE').GetMessage('DOCDESIGNER_TABLE_UA');
    $arCrmCompany['CrmCompany.REG_ADDRESS_POSTAL_CODE'] = GetMessage('DOCDESIGNER_TABLE_POSTAL_CODE').GetMessage('DOCDESIGNER_TABLE_UA');
    $arCrmCompany['CrmCompany.REG_ADDRESS_COUNTRY'] = GetMessage('DOCDESIGNER_TABLE_COUNTRY').GetMessage('DOCDESIGNER_TABLE_UA');
    $arCrmCompany['CrmCompany.POST_ADDRESS'] = GetMessage('DOCDESIGNER_TABLE_COMPANY_ADDRESS');//.GetMessage('DOCDESIGNER_TABLE_PA');
    $arCrmCompany['CrmCompany.ADDRESS'] = GetMessage('DOCDESIGNER_TABLE_ADDRESS_STREET').GetMessage('DOCDESIGNER_TABLE_PA');
    $arCrmCompany['CrmCompany.ADDRESS_2'] = GetMessage('DOCDESIGNER_TABLE_ADDRESS_2').GetMessage('DOCDESIGNER_TABLE_PA');
    $arCrmCompany['CrmCompany.ADDRESS_CITY'] = GetMessage('DOCDESIGNER_TABLE_CITY').GetMessage('DOCDESIGNER_TABLE_PA');
    $arCrmCompany['CrmCompany.ADDRESS_REGION'] = GetMessage('DOCDESIGNER_TABLE_REGION').GetMessage('DOCDESIGNER_TABLE_PA');
    $arCrmCompany['CrmCompany.ADDRESS_PROVINCE'] = GetMessage('DOCDESIGNER_TABLE_PROVINCE').GetMessage('DOCDESIGNER_TABLE_PA');
    $arCrmCompany['CrmCompany.ADDRESS_POSTAL_CODE'] = GetMessage('DOCDESIGNER_TABLE_POSTAL_CODE').GetMessage('DOCDESIGNER_TABLE_PA');
    $arCrmCompany['CrmCompany.ADDRESS_COUNTRY'] = GetMessage('DOCDESIGNER_TABLE_COUNTRY').GetMessage('DOCDESIGNER_TABLE_PA');
	$arCrmCompany['CrmCompany.PHONE'] = GetMessage('DOCDESIGNER_TABLE_COMPANY_PHONE');     //53350

	$arCrmFields = $CDocDes->GetCrmFields();

	if(count($arCrmFields['COMPANY']) > 0){
		$arCrmCompany[] = '--------------------------';
		foreach($arCrmFields['COMPANY'] as $NAME => $val){
			if($DocVar = COption::GetOptionString("htmls.docdesigner", "DOCDESIGNER_VARIABLES_COMPANY_" . $NAME)){
				$arCrmCompany['CrmCompany.'.$DocVar] = $val['NAME'];
			}
			elseif(!is_array($val)){
				$arStCrmCompany[''.$NAME] = $val;
			}
            //else{
              //print "<pre>"; print_r($val); print "</pre>";
              //$arCrmCompany['CrmCompany.'.$DocVar] = $val['NAME'];
            //}
		}
	}//print "<pre>"; print_r($arCrmFields['COMPANY']); print "</pre>";
	if(count($arCrmFields['DEAL']) > 0){
		//$arCrmDeal[] = '--------------------------';
		foreach($arCrmFields['DEAL'] as $NAME => $val){
			$pos = strpos($NAME, '.');

			if($DocVar = COption::GetOptionString("htmls.docdesigner", "DOCDESIGNER_VARIABLES_DEAL_" . $NAME)){
				$arCrmDeal[''.$DocVar] = $val['NAME'];
				$tmpName = $val['NAME'];
                //print "<pre>"; print_r($val); print "</pre>";
				if($val['PROPERTIES'])
					$arCrmDeal[''.$DocVar] = array('NAME'=>$tmpName, 'PROP'=> $val['PROPERTIES']);
                elseif($val['CRM_TYPE']){
                  $arCrmDeal[''.$DocVar] = array('NAME'=>$tmpName, 'PROP'=> $arCrmFields[$val['CRM_TYPE']]);
                }
				/*
				if($val['PROPERTIES']){
					foreach($val['PROPERTIES'] as $pid => $pval){
						$arCrmDeal[''.$DocVar.'.'.$pid] = $pval;
					}
				}
				*/
			}
			elseif(!is_array($val)){
				if($pos === false) continue;
				$arStCrmDeal[$NAME] = $val;
			}
		}
	}
    if(count($arCrmFields['LEAD']) > 0){
		//$arCrmDeal[] = '--------------------------';
		foreach($arCrmFields['LEAD'] as $NAME => $val){
			$pos = strpos($NAME, '.');


			if(!is_array($val)){
				if($pos === false) continue;
				$arStCrmLead[$NAME] = $val;
			}
            elseif($DocVar = COption::GetOptionString("htmls.docdesigner", "DOCDESIGNER_VARIABLES_LEAD_" . $NAME)){
				$arCrmLead[''.$DocVar] = $val['NAME'];
				$tmpName = $val['NAME'];
				if($val['PROPERTIES'])
					$arCrmLead[''.$DocVar] = array('NAME'=>$tmpName, 'PROP'=> $val['PROPERTIES']);
                elseif($val['CRM_TYPE']){
                  $arCrmDeal[''.$DocVar] = array('NAME'=>$tmpName, 'PROP'=> $arCrmFields[$val['CRM_TYPE']]);
                }
				/*
				if($val['PROPERTIES']){
					foreach($val['PROPERTIES'] as $pid => $pval){
						$arCrmDeal[''.$DocVar.'.'.$pid] = $pval;
					}
				}
				*/
			}
		}
	}
	//print "<pre>"; print_r($arCrmContact); print "</pre>";
	if(count($arCrmFields['CONTACT']) > 0){
		//$arCrmDeal[] = '--------------------------';
		foreach($arCrmFields['CONTACT'] as $NAME => $val){
			$pos = strpos($NAME, '.');

			if($DocVar = COption::GetOptionString("htmls.docdesigner", "DOCDESIGNER_VARIABLES_CONTACT_" . $NAME)){
				$arCrmContact['CrmContact.'.$DocVar] = $val['NAME'];
			}
			elseif(!is_array($val)){
				if($pos === false) continue;
				$arStCrmContact[$NAME] = $val;
			}
		}
	}
//}
//echo substr($DocID, 0, 4);
if(substr($DocID, 0, 4) == 'DEAL' or substr($DocID, 0, 4) == 'LEAD'){
  $cid = CCrmCatalog::GetDefaultID();
  $properties = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>$cid));

    //v3.1.1
	//$incParam["arWorkflowParameters"]['CrmProducts'] = array("Multiple" => '1', 'Name' => GetMessage('DOCDESIGNER_TABLE_CRM_PRODUCTS'));
	$arCrmProduct['invoiceTable'] = GetMessage('invoiceTable');
	$arCrmProduct[''] = '----------------------------';
	$arCrmProduct['CRMProduct:StrNum'] = GetMessage('DOCDESIGNER_CRM_PRODUCT_STR_NUM');
	$arCrmProduct['CRMProduct:Name'] = GetMessage('DOCDESIGNER_CRM_PRODUCT_NAME');
    $arCrmProduct['CRMProduct:Desc'] = GetMessage('DOCDESIGNER_CRM_PRODUCT_DESC');
    while ($prop_fields = $properties->GetNext()){
      /*if($prop_fields["CODE"])
        $arCrmProduct['CRMProduct:' . $prop_fields["CODE"]] = $prop_fields["NAME"];
      else*/
        $arCrmProduct['CRMProduct:PROPERTY_' . $prop_fields["ID"]] = $prop_fields["NAME"];
      //echo $prop_fields["ID"]." - ".$prop_fields["NAME"]."<br>";
    }
	$arCrmProduct['CRMProduct:Quantity'] = GetMessage('DOCDESIGNER_CRM_PRODUCT_QUANTITY');
    $arCrmProduct['CRMProduct:Measure'] = GetMessage('DOCDESIGNER_CRM_PRODUCT_MEASURE');
    $arCrmProduct['CRMProduct:Price'] = GetMessage('DOCDESIGNER_CRM_PRODUCT_PRICE');
    $arCrmProduct['CRMProduct:PriceIncDiscount'] = GetMessage('DOCDESIGNER_CRM_PRODUCT_PRICE_INC_DISCOUNT');
    $arCrmProduct['CRMProduct:VATRate'] = GetMessage('DOCDESIGNER_CRM_PRODUCT_VATRate');
    $arCrmProduct['CRMProduct:DiscountRate'] = GetMessage('DOCDESIGNER_CRM_PRODUCT_DISCOUNT_RATE');
    $arCrmProduct['CRMProduct:DiscountSum'] = GetMessage('DOCDESIGNER_CRM_PRODUCT_DISCOUNT_SUM');
    $arCrmProduct['CRMProduct:DiscountSumTotal'] = GetMessage('DOCDESIGNER_CRM_PRODUCT_DISCOUNT_SUM_TOTAL');
    $arCrmProduct['CRMProduct:Sum'] = GetMessage('DOCDESIGNER_CRM_PRODUCT_SUM');
	$arCrmProduct['CRMProduct:TotalSum'] = GetMessage('DOCDESIGNER_CRM_PRODUCT_TOTAL_SUM');
	$arCrmProduct['CRMProduct:VAT'] = GetMessage('DOCDESIGNER_CRM_PRODUCT_VAT');
	$arCrmProduct['CRMProduct:TotalVAT'] = GetMessage('DOCDESIGNER_CRM_PRODUCT_TOTAL_VAT');
	$arCrmProduct['CRMProduct:SumVAT'] = GetMessage('DOCDESIGNER_CRM_PRODUCT_SUM_VAT');
	$arCrmProduct['CRMProduct:TotalSumVAT'] = GetMessage('DOCDESIGNER_CRM_PRODUCT_TOTAL_SUM_VAT');
	$arCrmProduct['CRMProduct:NumRows'] = GetMessage('DOCDESIGNER_CRM_PRODUCT_NUM_ROWS');
	$arCrmProduct['CRMProduct:TotalSumSpelling'] = GetMessage('DOCDESIGNER_CRM_PRODUCT_TOTAL_SUM_SPELLING');
	$arCrmProduct['CRMProduct:TotalSumVatSpelling'] = GetMessage('DOCDESIGNER_CRM_PRODUCT_TOTAL_SUM_VAT_SPELLING');
	$arCrmProduct['CRMProduct:TotalVATSpelling'] = GetMessage('DOCDESIGNER_CRM_PRODUCT_TOTAL_VAT_SPELLING');
	$arCrmProduct['CRMProduct:TotalSumNoDiscountTax'] = GetMessage('DOCDESIGNER_CRM_PRODUCT_TOTAL_SUM_WITHOUT_DISCOUNT_TAX');

}

$arResponsible = array();
$arResponsible['Responsible.LastName'] = GetMessage('DOCDESIGNER_RESPONSIBLE_LAST_NAME');
$arResponsible['Responsible.Name'] = GetMessage('DOCDESIGNER_RESPONSIBLE_NAME');
$arResponsible['Responsible.SecondName'] = GetMessage('DOCDESIGNER_RESPONSIBLE_SECOND_NAME');
$arResponsible['Responsible.SecondName_Name'] = GetMessage('DOCDESIGNER_RESPONSIBLE_FI');
$arResponsible['Responsible.EMAIL'] = GetMessage('DOCDESIGNER_RESPONSIBLE_EMAIL');
$arResponsible['Responsible.PHONE'] = GetMessage('DOCDESIGNER_RESPONSIBLE_PHONE');
$arResponsible['Responsible.Position'] = GetMessage('DOCDESIGNER_RESPONSIBLE_POSITION');
$arResponsible['Responsible.FacsimileImg'] = GetMessage('DOCDESIGNER_RESPONSIBLE_FACSIMILE');

	$incParam["arWorkflowParameters"]['DocNumber'] = array("Multiple" => '0', 'Name' => GetMessage('DOCDESIGNER_DOC_NUMBER'));
	$incParam["arWorkflowParameters"]['DocDate'] = array("Multiple" => '0', 'Name' => GetMessage('DOCDESIGNER_DOC_DATE'));
	$incParam["arWorkflowParameters"]['PageBreak'] = array("Multiple" => '0', 'Name' => GetMessage('DOCDESIGNER_PageBreak'));
//echo "<pre>"; print_r($incParam); echo "</pre>";
//echo $DocumentType;
if(substr($DocID, 0, 7) == 'COMPANY'){
	$arCrmCompany['CrmCompany.NAME'] = GetMessage('DOCDESIGNER_TABLE_COMPANY_NAME');
	$arCrmCompany['CrmCompany.INN'] = GetMessage('DOCDESIGNER_TABLE_COMPANY_INN');
	$arCrmCompany['CrmCompany.KPP'] = GetMessage('DOCDESIGNER_TABLE_COMPANY_KPP');
	$arCrmCompany['CrmCompany.BANK_NAME'] = GetMessage('DOCDESIGNER_TABLE_COMPANY_BANK_NAME');
	$arCrmCompany['CrmCompany.BANK_BIK'] = GetMessage('DOCDESIGNER_TABLE_COMPANY_BANK_BIK');
	$arCrmCompany['CrmCompany.BANK_RS'] = GetMessage('DOCDESIGNER_TABLE_COMPANY_BANK_RS');
	$arCrmCompany['CrmCompany.BANK_KS'] = GetMessage('DOCDESIGNER_TABLE_COMPANY_BANK_KS');
	//$arCrmCompany['CrmCompany.ADDRESS_LEGAL'] = GetMessage('DOCDESIGNER_TABLE_COMPANY_ADDRESS_LEGAL');
	//$arCrmCompany['CrmCompany.ADDRESS'] = GetMessage('DOCDESIGNER_TABLE_COMPANY_ADDRESS');
	$arCrmCompany['CrmCompany.PHONE'] = GetMessage('DOCDESIGNER_TABLE_COMPANY_PHONE');
    //echo '<pre>'; print_r($arCrmCompany); echo '</pre>';
	$arCrmFields = $CDocDes->GetCrmFields();

	if(count($arCrmFields['COMPANY']) > 0){
		$arCrmCompany[] = '--------------------------';
		foreach($arCrmFields['COMPANY'] as $NAME => $val){
			if($DocVar = COption::GetOptionString("htmls.docdesigner", "DOCDESIGNER_VARIABLES_COMPANY_" . $NAME)){
				$arCrmCompany['CrmCompany.'.$DocVar] = $val['NAME'];
			}
			elseif(!is_array($val)){
				$arStCrmCompany[''.$NAME] = $val;
			}
		}
	}

	$incParam["arWorkflowParameters"]['DocNumber'] = array("Multiple" => '0', 'Name' => GetMessage('DOCDESIGNER_DOC_NUMBER'));
	$incParam["arWorkflowParameters"]['DocDate'] = array("Multiple" => '0', 'Name' => GetMessage('DOCDESIGNER_DOC_DATE'));
}
//print_r($arCrmCompany);
//echo '<pre>'; print_r($incParam["arWorkflowParameters"]); echo '</pre>';
?>
<script type="text/javascript">
		window.ddMenuReplaceItems	= [
//			{
//				value : 'value1',
//				label : 'description1' //, - no comma at end of array, IE not supported
//			},
//			{
//				value : 'value2',
//				label : 'description2'
//			}
<?
	//if(substr($DocID, 0, 4) == 'DEAL'){
			?>
			,{label : '<?=GetMessage('DOCDESIGNER_TABLE_CRM_COMPANY')?>',
			value: [
			<?
			foreach($arCrmCompany as $value => $lable){
				?>
				,{
					value : '{<?=$value?>}',
					label : '<?=$lable?>'
				}
				<?
			}?>
			]}
			<?
			if(count($arStCrmCompany) > 0){
				?>
				,{label : 'BX-<?=GetMessage('DOCDESIGNER_TABLE_CRM_COMPANY')?>',
				value: [
				<?
				foreach($arStCrmCompany as $value => $lable){
					?>
					,{
						value : '{<?=$value?>}',
						label : '<?=$lable?>'
					}
					<?
				}?>
				]}
				<?
			}
			?>
			,{label : '<?=GetMessage('DOCDESIGNER_TABLE_CRM_DEAL')?>',
			value: [
			<?
			foreach($arCrmDeal as $value => $lable){
				if(is_array($lable)){
					?>
					,{label : '<?=$lable['NAME']?>',
					value: [

						,{
							value : '{<?=$value?>}',
							label : '<?=$lable['NAME']?>'
						}

					<?
					foreach($lable['PROP'] as $pid => $pval){
					  $pid = str_replace('CONTACT.', '', $pid);
					  $pid = str_replace('COMPANY.', '', $pid);
                      if(is_array($pval)){
                        $pval = $pval['NAME'];
                      }
						?>
						,{
							value : '{<?=$value.'.'.$pid?>}',
							label : '<?=$pval?>'
						}
						<?
					}
                    ?>
                    ]}
                    <?
				}
				else{
					?>
					,{
						value : '{<?=$value?>}',
						label : '<?=$lable?>'
					}
					<?
				}
			}?>
			<!--]}-->
			<?
			if(count($arStCrmDeal) > 0){
				?>
				,{
					value : '',
					label : '--------------------------'
				}
				<?
				foreach($arStCrmDeal as $value => $lable){
					?>
					,{
						value : '{<?=$value?>}',
						label : '<?=$lable?>'
					}
					<?
				}
			}
			?>
			]},

            {label : '<?=GetMessage('DOCDESIGNER_TABLE_CRM_LEAD')?>',
			value: [
			<?
			foreach($arCrmLead as $value => $lable){
				if(is_array($lable)){
					?>
					,{label : '<?=$lable['NAME']?>',
					value: [

						,{
							value : '{<?=$value?>}',
							label : '<?=$MESS['DOCDESIGNER_TABLE_COMPANY_NAME']?>'
						}

					<?
					foreach($lable['PROP'] as $pid => $pval){
						?>
						,{
							value : '{<?=$value.'.'.$pid?>}',
							label : '<?=$pval?>'
						}
						<?
					}
                    ?>
                    ]}
                    <?
				}
				else{
					?>
					,{
						value : '{<?=$value?>}',
						label : '<?=$lable?>'
					}
					<?
				}
			}?>
			<!--]}-->
			<?
			if(count($arStCrmLead) > 0){
				?>
				,{
					value : '',
					label : '--------------------------'
				}
				<?
				foreach($arStCrmLead as $value => $lable){
					?>
					,{
						value : '{<?=$value?>}',
						label : '<?=$lable?>'
					}
					<?
				}
			}
			?>
			]}

			,{label : '<?=GetMessage('DOCDESIGNER_TABLE_CRM_CONTACT')?>',
			value: [
			<?
			foreach($arCrmContact as $value => $lable){
				?>
				,{
					value : '{<?=$value?>}',
					label : '<?=$lable?>'
				}
				<?
			}?>
			<?
			if(count($arStCrmContact) > 0){
				?>
				,{
					value : '',
					label : '--------------------------'
				}
				<?
				foreach($arStCrmContact as $value => $lable){
					?>
					,{
						value : '{<?=$value?>}',
						label : '<?=$lable?>'
					}
					<?
				}
			}
			?>
			]}
    <?//}
    if(substr($DocID, 0, 4) == 'DEAL' or substr($DocID, 0, 4) == 'LEAD'){?>
			,{label : '<?=GetMessage('DOCDESIGNER_CRM_PRODUCT')?>',
			value: [
			<?
			foreach($arCrmProduct as $value => $lable){
				?>
				,{
					value : '{<?=$value?>}',
					label : '<?=$lable?>'
				}
				<?
			}?>
			]}
			<?
	}
    ?>
    /* responsible - start */
    ,{label : '<?=GetMessage('DOCDESIGNER_RESPONSIBLE')?>',
			value: [
			<?
			foreach($arResponsible as $value => $lable){
				?>
				,{
					value : '{<?=$value?>}',
					label : '<?=$lable?>'
				}
				<?
			}?>
			]}
    /* responsible - end*/
    <?

	if(substr($DocID, 0, 7) == 'COMPANY'){
			?>
			,{label : '<?=GetMessage('DOCDESIGNER_TABLE_CRM_COMPANY')?>',
			value: [
			<?
			foreach($arCrmCompany as $value => $lable){
				?>
				,{
					value : '{<?=$value?>}',
					label : '<?=$lable?>'
				}
				<?
			}?>
			]}
			<?
			if(count($arStCrmCompany) > 0){
				?>
				,{label : 'BX-<?=GetMessage('DOCDESIGNER_TABLE_CRM_COMPANY')?>',
				value: [
				<?
				foreach($arStCrmCompany as $value => $lable){
					?>
					,{
						value : '{<?=$value?>}',
						label : '<?=$lable?>'
					}
					<?
				}?>
				]}
				<?
			}
	}
    ?>
    ,{label : '<?=GetMessage('DOCDESIGNER_TABLE_WORKFLOW_PARAMETERS')?>',
			value: [
    <?
    //echo '<pre>'; print_r($incParam["arWorkflowParameters"]); echo '</pre>';
	foreach ($incParam["arWorkflowParameters"] as $name => $param)
	{
	  if($name != 'PageBreak' && $name != 'DocDate' && $name != 'DocNumber'){
	    $sName = CDocDesigner::uni2win($incParam["arWorkflowParameters"][$name]["Name"]);
	  }
      else{
        $sName = $incParam["arWorkflowParameters"][$name]["Name"];
      }
		if($name == 'DocDesignerFile') continue;
		if($name == 'DocDesignerDogTemplateID') continue;
		if($name == 'DocDesignerAutoNumber') continue;
		if($name == 'DocDesignerNumberPrefix') continue;
		if($name == 'DocDesignerCompany'){
			?>
			,{label : '<?=GetMessage('DOCDESIGNER_TABLE_OWN_COMPANY')?>',
			value: [
			<?
			foreach($arCompany as $value => $lable){
				?>
				,{
					value : '{<?=$value?>}',
					label : '<?=$lable?>'
				}
				<?
			}?>
			]}
			<?
			continue;
		}
		/*if(substr($DocID, 0, 4) == 'DEAL'){
			?>
			,{label : CRM:'<?=GetMessage('DOCDESIGNER_TABLE_OWN_COMPANY')?>',
			value: [
			<?
			foreach($arCrmCompany as $value => $lable){
				?>
				,{
					value : '{<?=$value?>}',
					label : '<?=$lable?>'
				}
				<?
			}?>
			]}
			<?

		}*/
		if($incParam["arWorkflowParameters"][$name]["Type"] == 'S:UserID'
             || $incParam["arWorkflowParameters"][$name]["Type"] == 'user'
             || $incParam["arWorkflowParameters"][$name]["Type"] == 'S:employee'){
?>
			,{label : '<?=$incParam["arWorkflowParameters"][$name]["Name"]?>',
			value: [
			{
				value : '{<?=$name?>.FULLNAME.IMP}',
				label : '<?=GetMessage("DOCDESIGNER_TABLE_FULLNAME_IMP")?>'
			}
			,{
				value : '{<?=$name?>.FULLNAME.RP}',
				label : '<?=GetMessage("DOCDESIGNER_TABLE_FULLNAME_RP")?>'
			}
			,{
				value : '{<?=$name?>.FULLNAME.DP}',
				label : '<?=GetMessage("DOCDESIGNER_TABLE_FULLNAME_DP")?>'
			}
			,{
				value : '{<?=$name?>.SHORTNAME.IMP}',
				label : '<?=GetMessage("DOCDESIGNER_TABLE_SHORTNAME_IMP")?>'
			}
			,{
				value : '{<?=$name?>.SHORTNAME.RP}',
				label : '<?=GetMessage("DOCDESIGNER_TABLE_SHORTNAME_RP")?>'
			}
			,{
				value : '{<?=$name?>.SHORTNAME.DP}',
				label : '<?=GetMessage("DOCDESIGNER_TABLE_SHORTNAME_DP")?>'
			}
			,{
				value : '',
				label : '--------------------------'
			}
			,{
				value : '{<?=$name?>.POSITION.IMP}',
				label : '<?=GetMessage("DOCDESIGNER_TABLE_POSITION_IMP")?>'
			}
			,{
				value : '{<?=$name?>.POSITION.RP}',
				label : '<?=GetMessage("DOCDESIGNER_TABLE_POSITION_RP")?>'
			}
			,{
				value : '{<?=$name?>.POSITION.DP}',
				label : '<?=GetMessage("DOCDESIGNER_TABLE_POSITION_DP")?>'
			}

			]}

			<?
		}
		elseif($incParam["arWorkflowParameters"][$name]["Multiple"] == '1'){
			?>
			,{
				value : '{Multiple:<?=$name?>}',
				label : '<?=$sName?>'
			}
			<?
		}
		else{
			?>
			,{
				value : '{<?=$name?>}',
				label : '<?=$sName?>'
			}
			<?
		}
	}
    ?>
    ]}
    ,{label : '<?=GetMessage('DOCDESIGNER_TABLE_WORKFLOW_VARIABLES')?>',
			value: [
    <?
    foreach ($incParam["arWorkflowVariables"] as $name => $param)
	{
	  if($name != 'PageBreak' && $name != 'DocDate' && $name != 'DocNumber'){
	    $sName = CDocDesigner::uni2win($incParam["arWorkflowVariables"][$name]["Name"]);//
	  }
      else{
        $sName = $incParam["arWorkflowParameters"][$name]["Name"];
      }
		if($name == 'DocDesignerFile') continue;
		if($name == 'DocDesignerDogTemplateID') continue;
		if($name == 'DocDesignerNumberPrefix') continue;
		if($name == 'DocDesignerCompany'){
			?>
			,{label : '<?=GetMessage('DOCDESIGNER_TABLE_OWN_COMPANY')?>',
			value: [
			<?
			foreach($arCompany as $value => $lable){
				?>
				,{
					value : '{<?=$value?>}',
					label : '<?=$lable?>'
				}
				<?
			}?>
			]}
			<?
			continue;
		}
		if($incParam["arWorkflowVariables"][$name]["Type"] == 'S:UserID'){
?>
			,{label : '<?=$incParam["arWorkflowVariables"][$name]["Name"]?>',
			value: [
			{
				value : '{<?=$name?>.FULLNAME.IMP}',
				label : '<?=GetMessage("DOCDESIGNER_TABLE_FULLNAME_IMP")?>'
			}
			,{
				value : '{<?=$name?>.FULLNAME.RP}',
				label : '<?=GetMessage("DOCDESIGNER_TABLE_FULLNAME_RP")?>'
			}
			,{
				value : '{<?=$name?>.FULLNAME.DP}',
				label : '<?=GetMessage("DOCDESIGNER_TABLE_FULLNAME_DP")?>'
			}
			,{
				value : '{<?=$name?>.SHORTNAME.IMP}',
				label : '<?=GetMessage("DOCDESIGNER_TABLE_SHORTNAME_IMP")?>'
			}
			,{
				value : '{<?=$name?>.SHORTNAME.RP}',
				label : '<?=GetMessage("DOCDESIGNER_TABLE_SHORTNAME_RP")?>'
			}
			,{
				value : '{<?=$name?>.SHORTNAME.DP}',
				label : '<?=GetMessage("DOCDESIGNER_TABLE_SHORTNAME_DP")?>'
			}
			,{
				value : '',
				label : '--------------------------'
			}
			,{
				value : '{<?=$name?>.POSITION.IMP}',
				label : '<?=GetMessage("DOCDESIGNER_TABLE_POSITION_IMP")?>'
			}
			,{
				value : '{<?=$name?>.POSITION.RP}',
				label : '<?=GetMessage("DOCDESIGNER_TABLE_POSITION_RP")?>'
			}
			,{
				value : '{<?=$name?>.POSITION.DP}',
				label : '<?=GetMessage("DOCDESIGNER_TABLE_POSITION_DP")?>'
			}

			]}

			<?
		}
        if($incParam["arWorkflowVariables"][$name]["Type"] == 'UF:crm'){
          //$_type = $incParam["arWorkflowVariables"][$name]["Default"];
          //$atype = explode('_', $_type);
          if($incParam["arWorkflowVariables"][$name]['Options']['COMPANY'] == 'Y'){
            //company
            ?>
            ,{label : '<?=$incParam["arWorkflowVariables"][$name]["Name"]?>',
            value: [
                /*{
				    value : '{var<?=$name?>.TITLE}',
				    label : '<?=GetMessage("DOCDESIGNER_TABLE_COMPANY_NAME")?>'
			    }*/
                <?
    			foreach($arCrmCompany as $value => $lable){
    			  $value = str_replace('CrmCompany.', '', $value);
    				?>
    				{
    					value : '{var<?=$name?>.<?=$value?>}',
    					label : '<?=$lable?>'
    				},
    				<?
    			}?>
            ]
            }
            <?
          }
          elseif($incParam["arWorkflowVariables"][$name]['Options']['CONTACT'] == 'Y'){
            //contact
            ?>
            ,{label : '<?=$incParam["arWorkflowVariables"][$name]["Name"]?>',
            value: [
                <?
    			foreach($arCrmFields['CONTACT'] as $value => $lable){
    			  $value = str_replace('CONTACT.', '', $value);
    				?>
    				{
    					value : '{var<?=$name?>.<?=$value?>}',
    					label : '<?=$lable?>'
    				},
    				<?
    			}?>
            ]
            }
            <?
          }
        }
		elseif($incParam["arWorkflowVariables"][$name]["Multiple"] == '1'){
			?>
			,{
				value : '{Multiple:<?=$name?>}',
				label : '<?=$sName?>'
			}
			<?
		}
		else{
			?>
			,{
				value : '{var<?=$name?>}',
				label : '<?=$sName?>'
			}
			<?
		}
	}
?>
    ]}
		];

		window.htmlsDocDesigner	= {
			'MENU_TITLE'	: '<?=GetMessage("DOCDESIGNER_MENU_TITLE")?>',
			'NO_BORDER'		: '<?=GetMessage("DOCDESIGNER_NO_BORDER")?>',
			'BORDER'		: '<?=GetMessage("DOCDESIGNER_BORDER")?>',
			'LEFT_BORDER'	: '<?=GetMessage("DOCDESIGNER_LEFT_BORDER")?>',
			'RIGHT_BORDER'	: '<?=GetMessage("DOCDESIGNER_RIGHT_BORDER")?>',
			'TOP_BORDER'	: '<?=GetMessage("DOCDESIGNER_TOP_BORDER")?>',
            'BOTTOM_BORDER'	: '<?=GetMessage("DOCDESIGNER_BOTTOM_BORDER")?>',
			'footerParameters'	: '<?=GetMessage("DOCDESIGNER_footerParameters")?>',
			'pageNumber'	: '<?=GetMessage("DOCDESIGNER_pageNumber")?>',
			'totalPages'	: '<?=GetMessage("DOCDESIGNER_totalPages")?>',
			'contactFio'	: '<?=GetMessage("DOCDESIGNER_contactFio")?>',
			'ownFio'	: '<?=GetMessage("DOCDESIGNER_ownFio")?>',
			'initiallingOfPages'	: '<?=GetMessage("DOCDESIGNER_initiallingOfPages")?>',
			'DocNumber'	: '<?=GetMessage("DOCDESIGNER_DocNumber")?>',
			'DocDate'	: '<?=GetMessage("DOCDESIGNER_DocDate")?>',
			'calculator': '<?=GetMessage("DOCDESIGNER_calculator")?>',
			'spelling'	: '<?=GetMessage("DOCDESIGNER_spelling")?>',
			'cif'	    : '<?=GetMessage("DOCDESIGNER_cif")?>',
			'celseif'	: '<?=GetMessage("DOCDESIGNER_celseif")?>',
			'celse'	    : '<?=GetMessage("DOCDESIGNER_celse")?>',
			'functions'	    : '<?=GetMessage("DOCDESIGNER_functions")?>'
		};
	</script>
<?
if(!$DemoMode){
	$arResult = CDocDesigner::GetTemplate(array($DocID, $TemplateID));
	if($arResult['items']){
		//$perms = $TMP;
		$table = "<table border='" . $arResult['border'] . "' width='" . $arResult['width'] . "' class='data_table slim'>\n";
		foreach($arResult['items']['row'] as $r){
			$table .= "<tr>";
			foreach($r['col'] as $col){
				$table .= "<td align='" . $col['align'] . "' rowspan='" . $col['rowspan'] . "' colspan='" . $col['colspan'] . "' style='" . $col['style'] . "'>" . $col['text'] . "</td>\n";
			}
			$table .= "</tr>\n";
		}
		$table .= "</table>\n";
	}
	else{
		$table = $arResult['html'];
	}
}
else{
	$table = "<table><tr><td>".GetMessage('DOCDESIGNER_DEMO_EXPIRED')."</td></tr></table>";
}
?>
<textarea id="tinymce<?=$iUniqId?>" style="width: 100%; height: 420px;"><?=$table?></textarea>
<script type="text/javascript">
    /*if (typeof tinyMCE != 'undefined') {
        $(this).find(':tinymce').each(function () {
          var theMce = $(this);

          tinyMCE.execCommand('mceFocus', false, theMce.attr('id'));
          tinyMCE.execCommand('mceRemoveControl', false, theMce.attr('id'));
          $(this).remove();
        });
        //tinyMCE = '';
    }*/
CKEDITOR.config.height = 390;
tmpEditor = CKEDITOR.replace('tinymce');
tmpEditor.on( 'instanceReady', function(e) {
      tmpEditor.addMenuGroup('MENU_TITLE', 99);
        ContextMenu();
        
        try {
          $('#pdfFont option[value="<?=$arParams['pdfFont']?>"]').attr('selected','selected');
          $('#docxFont option[value="<?=$arParams['docxFont']?>"]').attr('selected','selected');
          $("#pdfFont").msDropDown();
          $('#pdfFont_child').css('width', '450px');
          $('#pdfFont_child').css('height', '230px');
          $('#pdfFont_child ul').css('width', '450px');
          $('#pdfFont_msdd').css('width', '450px');
        } catch(e) {
            alert(e.message);
        }


        $('#left-footer').contextPopup({
        title: window.htmlsDocDesigner.footerParameters,
        items: [

          {label:window.htmlsDocDesigner.DocNumber, icon: '/bitrix/images/htmls.docdesigner/dn.png',    action:function() { setParam('left-footer','dn'); } },
          {label:window.htmlsDocDesigner.DocDate, icon: '/bitrix/images/htmls.docdesigner/dd.png',    action:function() { setParam('left-footer','dd'); } },
          {label:window.htmlsDocDesigner.ownFio, icon: '/bitrix/images/htmls.docdesigner/own.png',    action:function() { setParam('left-footer','own'); } },
          {label:window.htmlsDocDesigner.contactFio, icon: '/bitrix/images/htmls.docdesigner/cont.png',    action:function() { setParam('left-footer','cont'); } },
          {label:window.htmlsDocDesigner.pageNumber, icon: '/bitrix/images/htmls.docdesigner/p.png',     action:function() { setParam('left-footer', 'p'); } },
          null, /* null can be used to add a separator to the menu items */
          {label:window.htmlsDocDesigner.totalPages, icon: '/bitrix/images/htmls.docdesigner/tp.png',    action:function() { setParam('left-footer','tp'); } },
          {label:window.htmlsDocDesigner.initiallingOfPages, icon: '/bitrix/images/htmls.docdesigner/signature.png',    action:function() { setParam('left-footer','initiallingOfPages'); } }
          //null, /* null can be used to add a separator to the menu items */
          //{label:'Blah Blah',  icon:'icons/shopping-basket.png',     action:function() { alert('clicked 3') }, isEnabled:function() { return false; } },
        ]}
      );
      $('#middle-footer').contextPopup({
        title: window.htmlsDocDesigner.footerParameters,
        items: [
          {label:window.htmlsDocDesigner.DocNumber, icon: '/bitrix/images/htmls.docdesigner/dn.png',     action:function() { setParam('middle-footer','dn'); } },
          {label:window.htmlsDocDesigner.DocDate, icon: '/bitrix/images/htmls.docdesigner/dd.png',     action:function() { setParam('middle-footer','dd'); } },
          {label:window.htmlsDocDesigner.ownFio, icon: '/bitrix/images/htmls.docdesigner/own.png',     action:function() { setParam('middle-footer','own'); } },
          {label:window.htmlsDocDesigner.contactFio, icon: '/bitrix/images/htmls.docdesigner/cont.png',     action:function() { setParam('middle-footer','cont'); } },
          {label:window.htmlsDocDesigner.pageNumber, icon: '/bitrix/images/htmls.docdesigner/p.png',     action:function() { setParam('middle-footer', 'p'); } },
          null,
          {label:window.htmlsDocDesigner.totalPages, icon: '/bitrix/images/htmls.docdesigner/tp.png',     action:function() { setParam('middle-footer','tp'); } },
          {label:window.htmlsDocDesigner.initiallingOfPages, icon: '/bitrix/images/htmls.docdesigner/signature.png',    action:function() { setParam('middle-footer','initiallingOfPages'); } }

          //null, /* null can be used to add a separator to the menu items */
          //{label:'Blah Blah',  icon:'icons/shopping-basket.png',     action:function() { alert('clicked 3') }, isEnabled:function() { return false; } },
        ]}
      );
      $('#right-footer').contextPopup({
        title: window.htmlsDocDesigner.footerParameters,
        items: [
          {label:window.htmlsDocDesigner.DocNumber, icon: '/bitrix/images/htmls.docdesigner/dn.png',     action:function() { setParam('right-footer','dn'); } },
          {label:window.htmlsDocDesigner.DocDate, icon: '/bitrix/images/htmls.docdesigner/dd.png',     action:function() { setParam('right-footer','dd'); } },
          {label:window.htmlsDocDesigner.ownFio, icon: '/bitrix/images/htmls.docdesigner/own.png',     action:function() { setParam('right-footer','own'); } },
          {label:window.htmlsDocDesigner.contactFio, icon: '/bitrix/images/htmls.docdesigner/cont.png',     action:function() { setParam('right-footer','cont'); } },
          {label:window.htmlsDocDesigner.pageNumber, icon: '/bitrix/images/htmls.docdesigner/p.png',     action:function() { setParam('right-footer', 'p'); } },
          null,
          {label:window.htmlsDocDesigner.totalPages, icon: '/bitrix/images/htmls.docdesigner/tp.png',     action:function() { setParam('right-footer','tp'); } },
          {label:window.htmlsDocDesigner.initiallingOfPages, icon: '/bitrix/images/htmls.docdesigner/signature.png',    action:function() { setParam('right-footer','initiallingOfPages'); } }

          //null, /* null can be used to add a separator to the menu items */
          //{label:'Blah Blah',  icon:'icons/shopping-basket.png',     action:function() { alert('clicked 3') }, isEnabled:function() { return false; } },
        ]}
      );

      $('html').click(function(){$('.contextMenuPlugin').hide();});
        //console.log('tmpEditor', window.ddMenuReplaceItems);

      <?
      $val = COption::GetOptionString("htmls.docdesigner", 'DOCDESIGNER_CLOUD_DOCX', 'N');
      if($val != 'Y'):?>
        $('#divDocxFont').hide();
      <?endif;
      ?>
    });
/*$('#tinymce<?=$iUniqId?>').tinymce({
 script_url : '/bitrix/js/htmls.docdesigner/tinymce/jscripts/tiny_mce/tiny_mce.js?time=<?=time()?>',
				// General options
				//mode : "textareas",
				language : '<?=strtolower(LANGUAGE_ID) ?>',
				theme : "advanced",
				keep_styles : false,
				font_size_style_values	: '',
				convert_fonts_to_spans	: false,
				plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",

				// Theme options
				theme_advanced_buttons1 : "tablecontrols, |,table_no_borders, table_borders, table_left_borders, table_right_borders, table_top_borders, table_bottom_borders, |, code",
				theme_advanced_buttons2 : "bold,italic,underline,strikethrough, fontsizeselect",
				theme_advanced_buttons3 : "",
				theme_advanced_buttons4 : "",
				theme_advanced_toolbar_location : "top",
				theme_advanced_toolbar_align : "left",
				theme_advanced_statusbar_location : "",
				theme_advanced_resizing : false });*/
/*
$( function() {
	$( '.dialog-head-icons .bx-icon-close' ).click( function() {
		$('#menu_tinymce_contextmenu').hide();
		window.location.reload();
	} );
} );
*/
	</script>
    </div>
    <div id="ddParams" style="display: none;">
    <input  style="float: right; background:url('/bitrix/images/htmls.docdesigner/help.png') no-repeat 50% 50% !important;" type="button" onclick="_help();"/>
    <input  style="background:url('/bitrix/images/htmls.docdesigner/ic_editor.png') no-repeat 50% 50% !important;" type="button" onclick="_switch('p');"/>
    <table cellspacing="10">
        <tbody>
            <tr><td valign="top" width="20%">
                <strong><?=GetMessage('DOCDESIGNER_DocBackground');?></strong>

                <input id="background" name="background" class="field upload" placeholder="click here to select file" type="file">
                <?if(!empty($bgFile)):?>
                <br><img style="width: 150px; height: 110px;" src="<?=$bgFile?>">
                <input type="hidden" name="bgId" value="<?=$fid?>" />
                <?else:?>
                <input type="hidden" name="bgId" value="0" />
                <?endif;?>
                <br/><input type="checkbox" name="delBg" /><?=GetMessage('DOCDESIGNER_DelBackground');?>
                </td>
                <td valign="top" width="20%">
                    <div class="header_footer">
                        <table><tbody>
                            <tr><td colspan="3" align="center"><strong><?=GetMessage('DOCDESIGNER_DocMargins');?></strong></td></tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td align="center"><?=GetMessage('DOCDESIGNER_DocTopMargin');?>&nbsp; <input id="topMargin" name="topMargin" size="3" value="<?=$arParams['tm']?>" type="text"></td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td><?=GetMessage('DOCDESIGNER_DocLeftMargin');?>&nbsp; <input id="leftMargin" name="leftMargin" size="3" value="<?=$arParams['lm']?>" type="text"></td>
                                <td><img src="/bitrix/images/htmls.docdesigner/page.png" alt="<?=GetMessage('DOCDESIGNER_DocMargins');?>" height="200" width="142"></td>
                                <td><?=GetMessage('DOCDESIGNER_DocRightMargin');?>&nbsp; <input id="rightMargin" name="rightMargin" size="3" value="<?=$arParams['rm']?>" type="text"></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td align="center"><?=GetMessage('DOCDESIGNER_DocBottomMargin');?>&nbsp; <input id="bottomMargin" name="bottomMargin" size="3" value="<?=$arParams['bm']?>" type="text"></td>
                                <td>&nbsp;</td>
                            </tr>
                        </tbody></table>
                        <div class="bx-edit-tab-inner" style="display: block;">
                          <div style="hei-ght: 100%;"><div class="bx-crm-view-fieldset">
                          <strong><?=GetMessage('DOCDESIGNER_DocFooter');?></strong>
                          <div class="bx-crm-view-fieldset-content">
                            <?=GetMessage('DOCDESIGNER_DocLeftFooter');?>: &nbsp;&nbsp;<input class="field" name="left-footer" id="left-footer" value="<?=$arParams['lfooter']?>" type="text"><br>
                            <?=GetMessage('DOCDESIGNER_DocMiddleFooter');?>: <input class="field" name="middle-footer" id="middle-footer" value="<?=$arParams['mfooter']?>" type="text"><br>
                            <?=GetMessage('DOCDESIGNER_DocRightFooter');?>: <input class="field" name="right-footer" id="right-footer" value="<?=$arParams['rfooter']?>" type="text"><br>
                            <?=GetMessage('DOCDESIGNER_DocFooterFont');?>: <input style="width:30px !important;" class="field" name="font-size" id="font-size" value="<?=$arParams['ffooter']?>" type="text">
                          </div></div></div>
                        </div>

                    </div>
                </td>
                <td valign="top">
                <strong><?=GetMessage('DOCDESIGNER_DocFonts');?></strong>
                <br> <br>
                <strong><?=GetMessage('DOCDESIGNER_DocPdfFont');?></strong><br>
                <?if(LANG == 'ru') $s = 'ru';
                else $s = 'en';?>
                    <select name="pdfFont" id="pdfFont">
                      <option value="dejavusansextralight" data-image="/bitrix/images/htmls.docdesigner/sansextralight-<?=$s?>.png">SansExtralight</option>
                      <option value="dejavusanscondensed" data-image="/bitrix/images/htmls.docdesigner/sanscondensed-<?=$s?>.png">SansCondensed</option>
                      <option value="dejavusansmono" data-image="/bitrix/images/htmls.docdesigner/sansmono-<?=$s?>.png">SansMono</option>
                      <option value="dejavuserifcondensed" title="/bitrix/images/htmls.docdesigner/serifcondensed-<?=$s?>.png">SerifCondensed</option>
                      <option value="dejavuserif" data-image="/bitrix/images/htmls.docdesigner/serif-<?=$s?>.png">Serif</option>
                      <option value="dejavusans" data-image="/bitrix/images/htmls.docdesigner/sans-<?=$s?>.png">Sans</option>
                      <option value="times" data-image="/bitrix/images/htmls.docdesigner/times-<?=$s?>.png">Times</option>
                    </select>
                    <br><br> <br>
                    <div id="divDocxFont">
                      <strong><?=GetMessage('DOCDESIGNER_DocDocxFont');?></strong><br>
                      <select name="docxFont" id="docxFont">
                          <option value="Times New Roman">Times New Roman</option>
                          <option value="Arial">Arial</option>
                          <option value="Calibri">Calibri</option>
                          <option value="Arial Narrow">Arial Narrow</option>
                          <option value="Verdana">Verdana</option>
                      </select>
                    </div>
                </td>
            </tr>
        </tbody></table>
    </div>
<?
$tabControl = new CAdminTabControl("tabControl", array());

$tabControl->Begin();

$tabControl->Buttons(array("buttons"=>Array(
	Array("name"=>GetMessage("DOCDESIGNER_DOC_BUTTON_SAVE"), "onclick"=>"WFSFSave();", "title"=>"", "id"=>"dpsavebutton"),
	Array("name"=>GetMessage("DOCDESIGNER_DOC_BUTTON_CANCEL"), "onclick"=>"BX.WindowManager.Get().CloseDialog(); window.location=window.location;", "title"=>"", "id"=>"dpcancelbutton")
	)));
$tabControl->End();
?>
</form>
<style>
.field{
    margin: 5px !important;
}
</style>


<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
?>