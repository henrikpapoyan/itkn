<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
global $DB;
IncludeModuleLangFile(__FILE__);
if(CModule::IncludeModule("bizproc")){
	CModule::IncludeModule("iblock");
	if(CModule::IncludeModuleEx("htmls.docdesigner") > 2) die("DocDesigner: demo-mode is expired");
	if($_REQUEST['ex']){
		$arInstalled = explode(",", COption::GetOptionString("htmls.docdesigner", "DOCDESIGNER_EXAMPLES_INSTALLED_TEMPLATE", ""));
		$IBLOCK_ID = COption::GetOptionInt('htmls.docdesigner', "DOCDESIGNER_EXAMPLES_IBLOCK_ID", 208);
		$ExId = htmlspecialcharsEx($_REQUEST['ex']);
		$arFilter = Array('ID'=>$ExId, 'IBLOCK_ID' => $IBLOCK_ID);
		$list = CIBlockSection::GetList(Array($by=>$order), $arFilter, true, array('UF_*'));
		while($result = $list->GetNext()){
		  //print "<pre>";				print_r($result); 				print "<pre/>";
			$Name = $result['NAME'];
			$Desc = HTMLToTxt($result['DESCRIPTION']);
			$BPTemp = $result['UF_BP_TEMPLATE'];
			$DOCXTemp = $result['UF_DOCX_TEMPLATE'];
			$PDFTemp = $result['UF_PDF_TEMPLATE'];
			$arType = explode('_', $result['CODE']);
			$UpdateVat = true;
			$UpdateNoVat = true;
			if(count($PDFTemp) > 0){
				foreach($PDFTemp as $FID){
					$arFile = CFile::GetFileArray($FID);
					if($arFile['FILE_NAME'] == 'invoice_vat.txt'){
						$InvoiceVatNot0 = COption::GetOptionInt("htmls.docdesigner", "DOCDESIGNER_EXAMPLES_PDF_TEMPLATE_INVOICE_VAT", 0);
						if($InvoiceVatNot0 == 0){
							$fname = $_SERVER["DOCUMENT_ROOT"].$arFile['SRC'];
							$f = fopen($fname, "rb");
   							$datum = fread($f, filesize($fname));
							$query = "INSERT INTO htmls_docdesigner_templates (bizproc, template, tmp_name, orientation, common)
							VALUES('DEAL', '" . $datum . "','".GetMessage('DOCDESIGNER_EXAMPLES_PDF_TEMPLATE_INVOICE_VAT')."', 'P','Y')";
							$result = $DB->Query($query);
							$TemplateID = $DB->LastID();
							COption::SetOptionInt("htmls.docdesigner", "DOCDESIGNER_EXAMPLES_PDF_TEMPLATE_INVOICE_VAT", $TemplateID);
							$arMacros['InvoiceVatNot0'] = $TemplateID;
						}
						else{
							$arMacros['InvoiceVatNot0'] = $InvoiceVatNot0;
							$UpdateVat = false;
						}
					}
					elseif($arFile['FILE_NAME'] == 'invoice_no_vat.txt'){
                       	$InvoiceVat0 = COption::GetOptionInt("htmls.docdesigner", "DOCDESIGNER_EXAMPLES_PDF_TEMPLATE_INVOICE_NO_VAT", 0);
						if($InvoiceVat0 == 0){
							$fname = $_SERVER["DOCUMENT_ROOT"].$arFile['SRC'];
							$f = fopen($fname, "rb");
  							$datum = fread($f, filesize($fname));
							$query = "INSERT INTO htmls_docdesigner_templates (bizproc, template, tmp_name, orientation, common)
							VALUES('DEAL', '" . $datum . "','".GetMessage('DOCDESIGNER_EXAMPLES_PDF_TEMPLATE_INVOICE_NO_VAT')."', 'P','Y')";
							$result = $DB->Query($query);
							$TemplateID = $DB->LastID();
							COption::SetOptionInt("htmls.docdesigner", "DOCDESIGNER_EXAMPLES_PDF_TEMPLATE_INVOICE_NO_VAT", $TemplateID);
							$arMacros['InvoiceVat0'] = $TemplateID;
						}
						else{
							$arMacros['InvoiceVat0'] = $InvoiceVat0;
							$UpdateNoVat = false;
						}
					}
				}
			}
			if(count($DOCXTemp) > 0){
			  //for all bp templates using the same file
				$DOCXTmpID = 0;//COption::GetOptionInt("htmls.docdesigner", "DOCDESIGNER_EXAMPLES_DOCX_TEMPLATE", 0);
				if($DOCXTmpID == 0){
				  $arFile = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"].'/bitrix/modules/htmls.docdesigner/examples/template3.docx');

					$el = new CIBlockElement;
					$arr = array(
						"ACTIVE" => "Y",
						//"IBLOCK_ID" => COption::GetOptionInt("htmls.docdesigner", "DOCDESIGNER_CONTRACTS_TEMPLATES_LIBRARY"),
						"IBLOCK_ID" => COption::GetOptionInt("htmls.docdesigner", "DOCDESIGNER_CONTRACTS_TEMPLATES_LIBRARY"),
						"NAME" => GetMessage('DOCDESIGNER_EXAMPLES_DOCX_TEMPLATE').'.docx',
						"MODIFIED_BY" => $GLOBALS["USER"]->GetID(),
						"PROPERTY_VALUES" => array(
							'FILE' => $arFile,
							//"WEBDAV_SIZE" => $arFile['size']
						)
					);
                    $DOCXTmpID = $el->Add($arr, false, false, false);
                    COption::SetOptionInt("htmls.docdesigner", "DOCDESIGNER_EXAMPLES_DOCX_TEMPLATE", $DOCXTmpID);
				}
			}
			if(intval($BPTemp) > 0){
				$fname = $_SERVER["DOCUMENT_ROOT"].CFile::GetPath($BPTemp);
				$f = fopen($fname, "rb");
       			$datum = fread($f, filesize($fname));
       			fclose($f);
       			$arTmp = unserialize(gzuncompress($datum));
                /*
       			print "<pre>";
				print_r($arTmp);
				print "<pre/>";
				die();
                */
                $LIB_IB = COption::GetOptionInt("htmls.docdesigner", "DOCDESIGNER_CONTRACTS_PATH2LIBRARY");
       			$params = $arTmp['PARAMETERS'];
       			if($params['DocDesignerCompany']){
      				$OWNS_IB = COption::GetOptionInt("htmls.docdesigner", "DOCDESIGNER_COMPANY");
					$params['DocDesignerCompany']['Options'] = $OWNS_IB;
       			}
       			if($params['DocDesignerDogTemplateID']){
       				//$TEMPS_IB = COption::GetOptionInt("htmls.docdesigner", "DOCDESIGNER_CONTRACTS_TEMPLATES_LIBRARY");
       				$TEMPS_IB = COption::GetOptionInt("htmls.docdesigner", "DOCDESIGNER_TMP_REGISTER_LIST");
					$params['DocDesignerDogTemplateID']['Options'] = $TEMPS_IB;
					$params['DocDesignerDogTemplateID']['Default'] = $DOCXTmpID;
       			}
       			if($params['DocDesignerDocumentSection']){
					$params['DocDesignerDocumentSection']['Options'] = $LIB_IB;
       			}
       			$arTmp['PARAMETERS'] = $params;
       			$vars = $arTmp['VARIABLES'];
       			if($vars['DocDesignerDocumentSection']){
       				//$LIB_IB = COption::GetOptionInt("htmls.docdesigner", "DOCDESIGNER_CONTRACTS_PATH2LIBRARY");
					$vars['DocDesignerDocumentSection']['Options'] = $LIB_IB;
       			}
       			$arTmp['VARIABLES'] = $vars;
       			$arTmp = ReplaceMacros($arTmp, $arMacros);
       			$datum = serialize($arTmp);

       			try{
	        		$tid = CBPWorkflowTemplateLoader::ImportTemplate(
	                	0, $arType, 0,
	                	GetMessage('DOCDESIGNER_EXAMPLES_PREFIX'). $Name, $Desc, $datum);
	                if($arMacros['InvoiceVat0'] > 0 && $UpdateNoVat){
                    	$query = "UPDATE htmls_docdesigner_templates SET bizproc='DEAL_".$tid."' WHERE id=".$arMacros['InvoiceVat0'];
						$result = $DB->Query($query);
	                }
	                if($arMacros['InvoiceVatNot0'] > 0 && $UpdateVat){
	                	$query = "UPDATE htmls_docdesigner_templates SET bizproc='DEAL_".$tid."' WHERE id=".$arMacros['InvoiceVatNot0'];
						$result = $DB->Query($query);
	                }
	                $arInstalled[] = $ExId;
	                COption::SetOptionString("htmls.docdesigner", "DOCDESIGNER_EXAMPLES_INSTALLED_TEMPLATE", implode(",", $arInstalled));
                    //print_r($arInstalled);
                    //echo implode(",", $arInstalled);
	                LocalRedirect("/apps/htmls.docdesigner/bp/CRM_DEAL/edit/".$tid."/");
				}
				catch (Exception $e){
					$errTmp = $e->getMessage();
					echo $errTmp;
				}
			}
		}
	}
}
function ReplaceMacros($arTmp, $arMacros){
	$new = array();
	foreach($arTmp as $id => $val){
		if(is_array($val) and $id === "Options" and count($val) == 3){
			$res = ReplaceMacros($val, $arMacros);
			$arTmp[$id] = $res;
		}
		elseif(is_array($val)){
			$arTmp[$id] = ReplaceMacros($val, $arMacros);
		}
		elseif($id === "PDFTemplate"){
    		if($val == 8)//invoice vat = 0
				$arTmp[$id] = $arMacros['InvoiceVat0'];
			elseif($val == 21)//invoice vat !=0
				$arTmp[$id] = $arMacros['InvoiceVatNot0'];
			}
		elseif($val == 21 && $id === "Options"){
			$arTmp[$id] = COption::GetOptionInt("htmls.docdesigner", "DOCDESIGNER_CONTRACTS_PATH2LIBRARY");
			$arTmp['Default_printable'] = '';
			$arTmp['Default'] = '';
		}
    }
	if(count($new) > 0) return $new;
    return $arTmp;
}
?>