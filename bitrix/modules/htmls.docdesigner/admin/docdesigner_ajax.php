<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
if(CModule::IncludeModuleEx("htmls.docdesigner") < 3){
    if($_REQUEST['sectionId']){
        CModule::IncludeModule('disk');
        $cDoc = new CDocDesignerContracts;
        $arSec = $cDoc->getSections(htmlspecialchars($_REQUEST['sectionId']));
        echo json_encode($arSec);
    }
}
?>