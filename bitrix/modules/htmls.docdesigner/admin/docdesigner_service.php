<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
IncludeModuleLangFile(__FILE__);
if(COption::GetOptionString('htmls.docdesigner', 'DOCDESIGNER_ADDMESSAGE2LOG') == 'Y'){
	define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/bitrix/tmp/DocDesignerService.txt");
}
$rsSites = CSite::GetList($by="sort", $order="desc", Array());
while ($arSite = $rsSites->Fetch()){
	$sites[] = $arSite['ID'];
}

if($_REQUEST['GenList'] == 'OwnCompany'){
	CModule::IncludeModule("iblock");
	CModule::IncludeModule("lists");

	$ib = new CIBlock;
	$arFields = Array("ACTIVE" => 'Y',"NAME" => $MESS['DDS_LIST_NAME'],
		"CODE" => 'own_company',"IBLOCK_TYPE_ID" => 'lists',"SITE_ID" => $sites);

	if($ID = $ib->Add($arFields)){
		COption::SetOptionString("htmls.docdesigner", 'DOCDESIGNER_COMPANY', $ID);
		$Field = new CListElementField($ID, 'NAME', $MESS['DDS_NAME'], 10);
		$Field = new CListElementField($ID, 'PREVIEW_TEXT', $MESS['DDS_BANK_DETAILS'], 20);
		$Field = new CListElementField($ID, 'PREVIEW_PICTURE', $MESS['DDS_SMALL_LOGO'], 30);
		$Field = new CListElementField($ID, 'DETAIL_PICTURE', $MESS['DDS_BIG_LOGO'], 40);

		$arProps['DDS_FAKSIMILE'] = array('F', 50);
		$arProps['DDS_PERSON'] = array('S', 60);
		$arProps['DDS_INN'] = array('S', 70);
		$arProps['DDS_KPP'] = array('S', 80);
		$arProps['DDS_OGRN'] = array('S', 90);
		$arProps['DDS_RS'] = array('S', 100);
		$arProps['DDS_BANK'] = array('E', 110);
		$arProps['DDS_POST_ADDRESS'] = array('S', 120);
		$arProps['DDS_PHONE'] = array('S', 130);
		$arProps['DDS_ADDRESS_LEGAL'] = array('S', 140);
		$arProps['DDS_ADDRESS'] = array('S', 150);
		$ibp = new CIBlockProperty;
		foreach($arProps as $CODE => $v){
			$arFields = Array(
				"NAME" => $MESS[$CODE],
				"ACTIVE" => "Y",
				"SORT" => $v[1],
				"CODE" => substr($CODE, 4),
				"PROPERTY_TYPE" => $v[0],
				"IBLOCK_ID" => $ID
			);
			if($v[0] == 'E'){
				$res = CIBlock::GetList(Array(),Array('TYPE'=>'lists',"CODE"=>'banks'), true);
				while($ar_res = $res->Fetch()){
					$arFields['LINK_IBLOCK_ID'] = $ar_res['ID'];
				}
			}

			$PropID = $ibp->Add($arFields);
			$Field = new CListElementField($ID, 'PROPERTY_' . $PropID, $MESS[$CODE], $v[1]);
			COption::SetOptionString("htmls.docdesigner", str_replace('DDS', 'DOCDESIGNER_COMPANY', $CODE), $PropID);
		}
		LocalRedirect('/bitrix/admin/settings.php?lang=ru&mid=htmls.docdesigner&mid_menu=1');
	}
	else{
		if(COption::GetOptionString('htmls.docdesigner', 'DOCDESIGNER_ADDMESSAGE2LOG') == 'Y'){
			AddMessage2Log("own_company->add: ".$ib->LAST_ERROR, "htmls.DocDesigner");
		}
	}
}
elseif($_REQUEST['GenUF'] == 'Company'){
	$arProps['DDS_INN'] = array('string', 100);
	$arProps['DDS_KPP'] = array('string', 200);
	$arProps['DDS_OGRN'] = array('string', 300);
	$arProps['DDS_RS'] = array('string', 400);
	$arProps['DDS_BANK'] = array('iblock_element', 500);
	$arProps['DDS_POST_ADDRESS'] = array('string', 600);
	$arProps['DDS_PHONE'] = array('string', 700);
	$obUserField  = new CUserTypeEntity();
	$t = time();
	foreach($arProps as $CODE => $v){
		$FName = 'UF_CRM_' . $t;
		$arFields = array('USER_TYPE_ID' => $v[0],
   						'ENTITY_ID' => 'CRM_COMPANY',
   						'SORT' => $v[1],
   						'MULTIPLE' => 'N', 'MANDATORY' => 'N', 'SHOW_FILTER' => 'E',
   						'EDIT_FORM_LABEL' => Array('ru' => $MESS[$CODE]),
						'LIST_COLUMN_LABEL' => Array('ru' => $MESS[$CODE]),
						'LIST_FILTER_LABEL' => Array('ru' => $MESS[$CODE]),
						'SETTINGS' => Array('DEFAULT_VALUE' => ''),
						'FIELD_NAME' => $FName);
		if($v[0] == 'iblock_element'){
			$res = CIBlock::GetList(Array(),Array('TYPE'=>'lists',"CODE"=>'banks'), true);
			while($ar_res = $res->Fetch()){
				$arFields['SETTINGS']['IBLOCK_TYPE_ID'] = 'lists';
				$arFields['SETTINGS']['IBLOCK_ID'] = $ar_res['ID'];
				$arFields['SETTINGS']['DISPLAY'] = 'LIST';
				$arFields['SETTINGS']['ACTIVE_FILTER'] = 'N';
			}
		}
		if($ID = $obUserField->Add($arFields))
			$r = 1;
		else
			print $obUserField->LAST_ERROR;
		COption::SetOptionString("htmls.docdesigner", str_replace('DDS', 'DOCDESIGNER_CRM_COMPANY', $CODE), $FName);
		$t++;
	}
	LocalRedirect('/bitrix/admin/settings.php?lang=ru&mid=htmls.docdesigner&mid_menu=1');
}
?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");?>