<?
$MESS['DDS_LIST_NAME'] = 'Own entities';
$MESS['DDS_NAME'] = 'Name';
$MESS['DDS_BANK_DETAILS'] = 'Bank details';
$MESS['DDS_SMALL_LOGO'] = 'Small logo';
$MESS['DDS_BIG_LOGO'] = 'Big logo';
$MESS['DDS_FAKSIMILE'] = 'Facsimile';
$MESS['DDS_PERSON'] = 'Contracts person';
$MESS['DDS_INN'] = 'INN';
$MESS['DDS_KPP'] = 'KPP';
$MESS['DDS_OGRN'] = 'OGRN';
$MESS['DDS_RS'] = 'R/S';
$MESS['DDS_BANK'] = 'Bank';
$MESS['DDS_POST_ADDRESS'] = 'Post address';
$MESS['DDS_PHONE'] = 'Phones';
$MESS['DDS_ADDRESS_LEGAL'] = 'Legal address';
$MESS['DDS_ADDRESS'] = 'Actual address';
//$MESS[''] = '';
?>