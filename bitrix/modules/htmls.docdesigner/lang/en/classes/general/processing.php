<?
$MESS['OFFER_TABLE_HEADER_NAME'] = 'Product (service)';
$MESS['OFFER_TABLE_HEADER_QUANTITY'] = 'Amount';
$MESS['OFFER_TABLE_HEADER_PRICE'] = 'Price';
$MESS['OFFER_TABLE_HEADER_SUM'] = 'Sum';
$MESS['OFFER_TABLE_HEADER_TOTAL'] = 'Total';
//v3.0.1
$MESS['BILL_TABLE_HEADER_NAME'] = 'Product';
$MESS['BILL_TABLE_HEADER_QUANTITY'] = 'Amount';
$MESS['BILL_TABLE_HEADER_PRICE'] = 'Price';
$MESS['BILL_TABLE_HEADER_SUM'] = 'Sum';
$MESS['BILL_TABLE_HEADER_TOTAL'] = 'Total';
$MESS['BILL_TABLE_HEADER_NUM'] = 'No';
$MESS['BILL_TABLE_HEADER_UNIT'] = 'Unit';
//v4.5.1
$MESS['DOCDESIGNER_PRODUCT_BLANK'] = 'Example row #';
//v5.0.1
$MESS['DATE_CREATE'] = 'Date create';
$MESS['STAGE_ID'] = 'Stage';
$MESS['CLOSED'] = 'Closed';
$MESS['TYPE_ID'] = 'Type';
$MESS['OPPORTUNITY'] = 'Opportunity';
$MESS['CURRENCY_ID'] = 'Currency';
$MESS['PROBABILITY'] = 'Probability';
$MESS['COMMENTS'] = 'Comments';
$MESS['BEGINDATE'] = 'Begin dtae';
$MESS['CLOSEDATE'] = 'Close date';
//5.0.3
$MESS['TITLE'] = 'Deal title';
//5.1.2
$MESS['POST'] = 'Post';
$MESS['ADDRESS'] = 'Street, building';
$MESS['NAME'] = 'Name';
$MESS['FULL_NAME'] = 'Full Name';
//5.5.9
$MESS['LAST_NAME'] = 'Last Name';
$MESS['SECOND_NAME'] = 'Second Name';

$MESS['LEAD_TITLE'] = 'Lead Title';
$MESS['COMPANY_TITLE'] = 'Company Title';

$MESS['ADDRESS_2'] = 'Suite / Apartment';
$MESS['ADDRESS_CITY'] = 'City';
$MESS['ADDRESS_REGION'] = 'Region';
$MESS['ADDRESS_PROVINCE'] = 'State / Province';
$MESS['ADDRESS_POSTAL_CODE'] = 'Zip';
$MESS['ADDRESS_COUNTRY'] = 'Country';
$MESS['FULL_NAME_SHORT'] = 'Short Name';
$MESS['FULL_FIO'] = 'Full Name & Second';
$MESS['BIRTH_DATE'] = 'Birth date';

$MESS['RQ_IDENT_DOC'] = 'Ident doc';
$MESS['RQ_IDENT_DOC_SER'] = 'Doc Ser';
$MESS['RQ_IDENT_DOC_NUM'] = 'Doc Num';
$MESS['RQ_IDENT_DOC_DATE'] = 'Doc Date';
$MESS['RQ_IDENT_DOC_ISSUED_BY'] = 'Doc issued by';
$MESS['RQ_IDENT_DOC_DEP_CODE'] = 'Doc dept code';

$MESS['REG_ADDRESS'] = 'Address (r.a.)';
$MESS['REG_ADDRESS_2'] = 'Suite / Apartment (r.a.)';
$MESS['REG_ADDRESS_CITY'] = 'City (r.a.)';
$MESS['REG_ADDRESS_REGION'] = 'Region (r.a.)';
$MESS['REG_ADDRESS_PROVINCE'] = 'State / Province (r.a.)';
$MESS['REG_ADDRESS_POSTAL_CODE'] = 'Zip (r.a.)';
$MESS['REG_ADDRESS_COUNTRY'] = 'Country (r.a.)';
?>