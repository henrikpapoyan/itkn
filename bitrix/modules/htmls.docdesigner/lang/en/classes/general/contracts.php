<?
$MESS["January"] = "jan";
$MESS["February"] = "feb";
$MESS["March"] = "mar";
$MESS["April"] = "apr";
$MESS["May"] = "may";
$MESS["June"] = "jun";
$MESS["July"] = "jul";
$MESS["August"] = "aug";
$MESS["September"] = "sep";
$MESS["October"] = "oct";
$MESS["November"] = "nov";
$MESS["December"] = "dec";

$MESS['DOC_DESIGNER_TEMPLATES'] = 'DocDesigner\' Templates';
$MESS['DOC_DESIGNER_TMP_FILE'] = 'File';
$MESS['DR_FILE'] = 'File';
$MESS['DDS_NAME'] = 'Name';
$MESS['NO_ONE_TMP'] = 'No one DOCX template';
$MESS['NO_SELECT'] = 'DOCX template doesn\'t select';
?>