<?
$MESS["DOCDESIGNER_MODULE_NAME"] = 'Documents Designer';
$MESS["DOCDESIGNER_MODULE_DESC"] = 'Creation of standard documents';
$MESS["DOCDESIGNER_INSTALL_TITLE"] = 'Install DocDesigner';
$MESS["DOCDESIGNER_UNINSTALL_TITLE"] = 'UnInstall DocDesigner';

$MESS['DDS_LIST_NAME'] = 'Own entities';
$MESS['DDS_NAME'] = 'Name';
$MESS['DDS_BANK_DETAILS'] = 'Bank details';
$MESS['DDS_SMALL_LOGO'] = 'Small logo';
$MESS['DDS_BIG_LOGO'] = 'Big logo';
$MESS['DDS_FAKSIMILE'] = 'Faksimile';
$MESS['DDS_PERSON'] = 'CEO faksimile';
$MESS['DDS_INN'] = 'INN';
$MESS['DDS_KPP'] = 'KPP';
$MESS['DDS_OGRN'] = 'OGRN';
$MESS['DDS_RS'] = 'RS';
$MESS['DDS_BANK_NAME'] = 'Bank name';
$MESS['DDS_BANK_BIK'] = "BIK";
$MESS['DDS_BANK_KS'] = "KS";
$MESS['DDS_BANK_CITY'] = "Bank city";
$MESS['DDS_POST_ADDRESS'] = 'Post address';
$MESS['DDS_PHONE'] = 'Phone';
$MESS['DDS_ADDRESS_LEGAL'] = 'Legal address';
$MESS['DDS_ADDRESS'] = 'Real address';

$MESS['NUMBER_TEMPLATE'] = 'Number template';
$MESS['NUMBER_LENGTH'] = 'Number length';
$MESS['NUMBER_RESET'] = 'Reset number';
$MESS['NUMBER_START'] = 'Start number';
$MESS['CONTRACT_UNIQUE'] = 'Unique by CRM item';
$MESS['CORP_UNIQUE'] = 'Disable unique by own\'s companies';
$MESS['NUMERATOR_NAME'] = 'Numerators';

$MESS['DOCS_REGISTER'] = 'Documents register';
$MESS['DR_NAME'] = 'Number';
$MESS['DR_DATE_CREATE'] = 'Date';
$MESS['DR_DOC'] = 'Document';
$MESS['DR_SUMMA'] = 'Sum';
$MESS['DR_OWN_ENTITIES'] = 'Own entitie';
$MESS['DR_COMPANY'] = 'Company';
$MESS['DR_DEAL'] = 'Deal';
$MESS['DR_FILE'] = 'File';
$MESS['DR_PREFIX'] = 'Prefix (sys)';
$MESS['DR_NUMBER_SYS'] = 'Number (sys)';
$MESS['DR_BIZPROC_ID'] = 'Bizproc ID (sys)';
$MESS['DR_YEAR'] = 'Year (sys)';
$MESS['DR_NUMERATOR'] = 'Numerator (sys)';
$MESS['DR_TABLE'] = 'Table (sys)';

$MESS['NUM_CONTRACT'] = 'Contract';
$MESS['NUM_INVOICE'] = 'Invoice';

$MESS['DOC_JOURNAL'] = 'Register';
$MESS['BP_PDF'] = 'BizProc + PDF-tmp';
$MESS['PDF'] = 'PDF-tmp';
//$MESS['TITLE'] = 'DocDesigner';
$MESS['CONFIRM'] = 'Apply for all?';
$MESS['EXAMPLES'] = 'Examples';
$MESS['DOC_DESIGNER'] = 'DocDesigner';

$MESS['DOC_DESIGNER_TEMPLATES'] = 'DocDesigner\' Templates';
$MESS['DOC_DESIGNER_TMP_FILE'] = 'File';

$MESS['DDS_CEO_SIGN'] = 'CEO sign';
$MESS['DDS_CEO_POST'] = 'CEO position name';
$MESS['DDS_CEO_BASE'] = 'Based';
$MESS['DDS_ACC_POST'] = 'Accountant position';
$MESS['DDS_ACC_NAME'] = 'Accountant Name';
$MESS['DDS_ACC_SIGN'] = 'Accountant sign';
$MESS['DDS_STAMP'] = 'Company stamp';
?>
