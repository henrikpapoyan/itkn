<?
$MESS['DOCDESIGNER_TAB_SETTINGS'] = 'Own entities';
$MESS['DOCDESIGNER_TAB_SETTINGS_ALT'] = 'Own entities settings';
$MESS['DOCDESIGNER_TAB_RIGHTS'] = 'Access';
$MESS['DOCDESIGNER_TAB_RIGHTS_ALT'] = 'DocDesigner Access Permissions';
//v1.5.1
$MESS['DOCDESIGNER_COMPANY'] = 'Own entities list';
$MESS['DOCDESIGNER_COMPANY_FAKSIMILE'] = 'Facsimile';
$MESS['DOCDESIGNER_NO_SELECT'] = 'not selected';
//v2.5.1
$MESS['DOCDESIGNER_TAB_CONTRACTS'] = 'Documents register';
$MESS['DOCDESIGNER_TAB_CONTRACTS_ALT'] = 'Documents register settings';
$MESS['DOCDESIGNER_CONTRACTS_START_NUMBER'] = 'Start number';
$MESS['DOCDESIGNER_CONTRACTS_PREFIX_NUMBER'] = 'Number prifix';
$MESS['DOCDESIGNER_CONTRACTS_RESET_NUMBER'] = 'Reset number at the beginning of a year';
$MESS['DOCDESIGNER_CONTRACTS_LENGTH_NUMBER'] = 'Length of number';
$MESS['DOCDESIGNER_CONTRACTS_AUTO_NUMBER'] = 'Automatic numbering';
$MESS['DOCDESIGNER_TAB_COMPANY'] = 'CRM: Company';
$MESS['DOCDESIGNER_TAB_COMPANY_ALT'] = 'Company settings';
$MESS['DOCDESIGNER_CRM_COMPANY_INN'] = 'INN';
$MESS['DOCDESIGNER_CRM_COMPANY_KPP'] = 'KPP';
$MESS['DOCDESIGNER_CRM_COMPANY_RS'] = 'R/S';
$MESS['DOCDESIGNER_CRM_COMPANY_BANK'] = 'BANK';
$MESS['DOCDESIGNER_CRM_COMPANY_OGRN'] = 'OGRN';
$MESS['DOCDESIGNER_CRM_COMPANY_POST_ADDRESS'] = 'Post address';
$MESS['DOCDESIGNER_CRM_COMPANY_PHONE'] = 'Phones';
$MESS['DOCDESIGNER_CONTRACTS_SAVE2LIBRARY'] = 'Save contracts in WebDav';
$MESS['DOCDESIGNER_CONTRACTS_TEMPLATES_LIBRARY'] = 'Templates library';
$MESS['DOCDESIGNER_CONTRACTS_PATH2LIBRARY'] = 'Contracts library';
$MESS['DOCDESIGNER_COMPANY_CODE1C'] = '1C ID';
$MESS['DOCDESIGNER_COMPANY_ADDRESS_LEGAL'] = 'Legal address';
$MESS['DOCDESIGNER_COMPANY_ADDRESS'] = 'Actual address';
$MESS['DOCDESIGNER_COMPANY_PERSON'] = 'Contracts person';
//v3.0.1
$MESS['DOCDESIGNER_TAB_INVOICES'] = 'Invoices register';
$MESS['DOCDESIGNER_TAB_INVOICES_ALT'] = 'Invoices register settings';
$MESS['DOCDESIGNER_INVOICES_LIST'] = 'Invoices register';
$MESS['BX_INVOICES'] = 'Invoices';
$MESS['DOCDESIGNER_INVOICE_TYPE'] = 'Type';
$MESS['DOCDESIGNER_INVOICE_BDT'] = 'Bujet';
$MESS['DOCDESIGNER_TAB_FNAMES'] = 'PDF-names';
$MESS['DOCDESIGNER_TAB_FNAMES_ALT'] = 'PDF-names';
$MESS['DOCDESIGNER_TAB_SERVICE'] = 'Service';
$MESS['DOCDESIGNER_TAB_SERVICE_ALT'] = 'Creation of lists and fields';
$MESS['DOCDESIGNER_CREATE_COMPANY'] = 'Creation of own entities list';
$MESS['DOCDESIGNER_CREATE_BTN'] = 'Create';
$MESS['DOCDESIGNER_CREATE_CRM_COMPANY'] = 'Creation fields for CRM:Company';
$MESS['DOCDESIGNER_SHOW_SERVICE_TAB'] = 'Show service tab';
//v.3.0.2
$MESS['DOCDESIGNER_COMPANY_INVOICE_FACSIMILE'] = 'Invoice facsimile';
//v3.1.1
$MESS['DOCDESIGNER_TAB_PROD_IBLOCK'] = 'Offers';
$MESS['DOCDESIGNER_TAB_PROD_IBLOCK_ALT'] = 'Offers settings';
$MESS['DOCDESIGNER_OFFERS'] = 'Offers IBlock';
$MESS['DOCDESIGNER_OFFER_PRICE'] = 'Price';
$MESS['DOCDESIGNER_TAB_DEFAULT_TEMPLATES'] = 'PDF-templates';
$MESS['DOCDESIGNER_TAB_DEFAULT_TEMPLATES_ALT'] = 'Default PDF-template';
$MESS['DOCDESIGNER_DEFAULT_INVOICE_TEMPLATE'] = 'Default invoice template';
//v3.5.1
$MESS['DOCDESIGNER_TAB_V8EXCHANGE'] = '1С:V8';
$MESS['DOCDESIGNER_TAB_V8EXCHANGE_ALT'] = 'Обмен с 1С:Предприятие 8';
$MESS['DOCDESIGNER_V8EXCHANGE_CONFIGURATIONS'] = '1C:Enterprise 8.2 (8.2.14.528)<br>Accounts department, edition 2.0 (2.0.30.8)<br>Manufacturing enterprise, edition 1.3 (1.3.23.1)<br>(simple application)';
$MESS['DOCDESIGNER_V8EXCHANGE_IMPORT_PRODUCTS'] = 'Загрузка продуктов ->';
$MESS['DOCDESIGNER_V8EXCHANGE_EXPORT_INVOICES'] = 'Выгрузка счетов ->';
$MESS['DOCDESIGNER_V8EXCHANGE_IMPORT_BTN'] = 'Загрузить';
$MESS['DOCDESIGNER_V8EXCHANGE_EXPORT_BTN'] = 'Выгрузить';
$MESS['DOCDESIGNER_V8EXCHANGE_EPF'] = 'External module for 1C:Enterprise 8.2';
$MESS['V8EXCHANGE_DOWNLOAD'] = 'Download';
//v3.5.2
$MESS['DOCDESIGNER_TAB_OTHER'] = 'Other';
$MESS['DOCDESIGNER_TAB_OTHER_ALT'] = 'Other settings';
//v3.6.3
$MESS['DOCDESIGNER_ADDMESSAGE2LOG_HEADER'] = 'Log-file /bitrix/tmp/DocDesigner.txt';
$MESS['DOCDESIGNER_ADDMESSAGE2LOG'] = 'Use AddMessage2Log';
//v4.5.1
$MESS['DOCDESIGNER_CRM_COMPANY_BANK_NAME'] = 'Bank name';
$MESS['DOCDESIGNER_CRM_COMPANY_BANK_BIK'] = 'BIK';
$MESS['DOCDESIGNER_CRM_COMPANY_BANK_KS'] = 'KS';
$MESS['DOCDESIGNER_CRM_COMPANY_BANK_CITY'] = 'City';
$MESS['DOCDESIGNER_CRM_COMPANY_BANK_DETAILS'] = 'Bank details';
$MESS['DOCDESIGNER_CRM_COMPANY_ID_DATA'] = 'ID data';
$MESS['DOCDESIGNER_CRM_COMPANY_OTHER'] = 'Other';
$MESS['DOCDESIGNER_NUMERATOR_1C'] = 'Export to 1C';
//v5.0.1
$MESS['DOCDESIGNER_TAB_VARIABLES'] = 'CRM:Variables';
$MESS['DOCDESIGNER_TAB_VARIABLES_ALT'] = 'CRM: Variables';
$MESS['DOCDESIGNER_CRM_COMPANY_TITLE'] = 'Company';
$MESS['DOCDESIGNER_CRM_DEAL_TITLE'] = 'Deal';
$MESS['DOCDESIGNER_CRM_CONTACT_TITLE'] = 'Contact';
$MESS['DOCDESIGNER_VARIABLES_HINT'] = 'On this page you can appoint names of variables for the user fields.<br/><br/>At the left the field name is specified, on the right it is necessary to specify the name of a variable which will be used in a DOCX template. The name of a variable should be in English, without gaps and special symbols:<br/><br/><table>
<tr><td>User field name</td><td></td><td>Variable name</td><td></td><td>DOCX-variable</td></tr>
<tr><td><b>Field name</b></td><td> -> </td><td><b>FIELD_NAME</b></td><td> -> </td><td><b>${FIELD_NAME}</b></td></tr></table>';
$MESS['DOCDESIGNER_VARIABLE_NAME'] = 'Variable name';

//v5.0.3
$MESS['DATE_CREATE'] = 'Date create';
$MESS['STAGE_ID'] = 'Stage';
$MESS['CLOSED'] = 'Closed';
$MESS['TYPE_ID'] = 'Type';
$MESS['OPPORTUNITY'] = 'Opportunity';
$MESS['CURRENCY_ID'] = 'Currency';
$MESS['PROBABILITY'] = 'Probability';
$MESS['COMMENTS'] = 'Comments';
$MESS['BEGINDATE'] = 'Begin date';
$MESS['CLOSEDATE'] = 'Close date';
$MESS['TITLE'] = 'Deal name';

$MESS['Nominative'] = 'Nominative';
$MESS['Genitive'] = "Genitive";
$MESS['Dative'] = "Dative";
$MESS['Accusative'] = "Accusative";
$MESS['Instrumental'] = "Instrumental";
$MESS['Prepositional'] = "Prepositional";
$MESS['DOCDESIGNER_DECLINATION_CASE'] = 'Full name declination';

$MESS['CONTACT.NAME'] = 'Name';
$MESS['CONTACT.FULL_NAME'] = 'Name Second_name';
$MESS['CONTACT.ADDRESS'] = 'Address';
$MESS['CONTACT.POST'] = 'Post';

$MESS['DOCDESIGNER_CONTRACTS_NUMERATORS'] = "Contract's numerators";
$MESS['DOCDESIGNER_V8EXCHANGE_FILE_EXPORT'] = 'Upload to 1C PDF & DOCX';

$MESS['DOCDESIGNER_DELETE_DOCS_ON_DELETE_BIZPROC'] = 'Delete all docs on biz-proc delete';

$MESS['DOCDESIGNER_DECLINATOR'] = 'Declination service';
$MESS['INNER_DECLINATOR'] = 'Inner';
$MESS['MORPHER'] = 'morpher.ru';

$MESS['DOCDESIGNER_CRM_LEAD_TITLE'] = 'Leads';
$MESS['STATUS_ID'] = 'lead\'s status';
$MESS['LEAD_TITLE'] = 'Lead\'s title';
$MESS['COMPANY_TITLE'] = 'Company title';
$MESS['DOCDESIGNER_REMOVE_PENNY'] = 'No cents';

$MESS['DOCDESIGNER_TAB_LISTS'] = 'Lists';
$MESS['DOCDESIGNER_TAB_LISTS_ALT'] = 'Lists';

$MESS ['DOCDESIGNER_CLOUD_DOCX'] = 'Use external service to generate DOCX';
$MESS ['DOCDESIGNER_CLOUD_DOCX_HINT'] = 'This service will allow you to generate documents in DOCX format based on the template that is used for the PDF. If your portal runs on the LAN to use an external service generating DOCX want to allow access to the site www.htmls.pw. Also your server should support cURL extension of PHP.';

$MESS['DOCDESIGNER_CRM_DISK_HEADER'] = 'CRM Disk';
$MESS['DOCDESIGNER_CRM_DISK_DEAL'] = 'Deal';
$MESS['DOCDESIGNER_CRM_DISK_COMPANY'] = 'Company';
$MESS['DOCDESIGNER_CRM_DISK_CONTACT'] = 'Contact';
$MESS['DOCDESIGNER_CRM_DISK_LEAD'] = 'Lead';
$MESS['DOCDESIGNER_CLOUD_DOCX_HEADER'] = 'External service to generate DOCX';
$MESS['DOCDESIGNER_QUANTITY_FLOAT'] = 'The number of digits of the fractional part';
$MESS['DOCDESIGNER_BASIC_SETTINGS_HEADER'] = 'Basic settings';

$MESS['DOCDESIGNER_CLOUD_MCRYPT'] = 'Use encryption using mcrypt function';
$MESS['DOCDESIGNER_CLOUD_MCRYPT_HINT'] = 'If this option is not checked the source code of the document is transmitted by base64 encoded. <br/> When this option is checked, the source code of the document will be encrypted using standard mcrypt PHP-function.';

?>