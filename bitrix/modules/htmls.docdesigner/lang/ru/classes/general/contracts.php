<?
$MESS["January"] = "января";
$MESS["February"] = "февраля";
$MESS["March"] = "марта";
$MESS["April"] = "апреля";
$MESS["May"] = "мая";
$MESS["June"] = "июня";
$MESS["July"] = "июля";
$MESS["August"] = "августа";
$MESS["September"] = "сентября";
$MESS["October"] = "октября";
$MESS["November"] = "ноября";
$MESS["December"] = "декабря";

$MESS['DOC_DESIGNER_TEMPLATES'] = 'Шаблоны для Конструктора документов';
$MESS['DOC_DESIGNER_TMP_FILE'] = 'Файл шаблона';
$MESS['DR_FILE'] = 'Файл';
$MESS['DDS_NAME'] = 'Название';
$MESS['NO_ONE_TMP'] = 'Не обнаружено шаблонов DOCX';
$MESS['NO_SELECT'] = 'шаблон DOCX не выбран';
?>