<?
$MESS['OFFER_TABLE_HEADER_NAME'] = 'Наименование товара (услуги)';
$MESS['OFFER_TABLE_HEADER_QUANTITY'] = 'Количество';
$MESS['OFFER_TABLE_HEADER_PRICE'] = 'Цена';
$MESS['OFFER_TABLE_HEADER_SUM'] = 'Сумма';
$MESS['OFFER_TABLE_HEADER_TOTAL'] = 'ИТОГО';
//v3.0.1
$MESS['BILL_TABLE_HEADER_NAME'] = 'Товар';
$MESS['BILL_TABLE_HEADER_QUANTITY'] = 'Кол-во';
$MESS['BILL_TABLE_HEADER_PRICE'] = 'Цена';
$MESS['BILL_TABLE_HEADER_SUM'] = 'Сумма';
$MESS['BILL_TABLE_HEADER_TOTAL'] = 'ИТОГО';
$MESS['BILL_TABLE_HEADER_NUM'] = '№';
$MESS['BILL_TABLE_HEADER_UNIT'] = 'Ед.';
//v4.5.1
$MESS['DOCDESIGNER_PRODUCT_BLANK'] = 'Строка товара #';
//v5.0.1
$MESS['DATE_CREATE'] = 'Дата создания';
$MESS['STAGE_ID'] = 'Стадия сделки';
$MESS['CLOSED'] = 'Сделка закрыта';
$MESS['TYPE_ID'] = 'Тип';
$MESS['OPPORTUNITY'] = 'Сумма';
$MESS['CURRENCY_ID'] = 'Валюта';
$MESS['PROBABILITY'] = 'Вероятность';
$MESS['COMMENTS'] = 'Комментарий';
$MESS['BEGINDATE'] = 'Дата начала';
$MESS['CLOSEDATE'] = 'Дата закрытия';
//5.0.3
$MESS['TITLE'] = 'Название сделки';
//5.1.2
$MESS['POST'] = 'Должность';
$MESS['ADDRESS'] = 'Адрес';
$MESS['NAME'] = 'Имя';
$MESS['FULL_NAME'] = 'Имя отчество';
//5.5.9
$MESS['LAST_NAME'] = 'Фамилия';
$MESS['SECOND_NAME'] = 'Отчество';

$MESS['LEAD_TITLE'] = 'Название лида';
$MESS['COMPANY_TITLE'] = 'Название компании';

$MESS['ADDRESS_2'] = 'Квартира / офис';
$MESS['ADDRESS_CITY'] = 'Город';
$MESS['ADDRESS_REGION'] = 'Район';
$MESS['ADDRESS_PROVINCE'] = 'Область';
$MESS['ADDRESS_POSTAL_CODE'] = 'Почтовый индекс';
$MESS['ADDRESS_COUNTRY'] = 'Страна';
$MESS['FULL_NAME_SHORT'] = 'Фамилия И. О.';
$MESS['FULL_FIO'] = 'Фамилия Имя Отчество';
$MESS['BIRTH_DATE'] = 'Дата рождения';

$MESS['RQ_IDENT_DOC'] = ' Вид документа';
$MESS['RQ_IDENT_DOC_SER'] = 'серия документа';
$MESS['RQ_IDENT_DOC_NUM'] = 'номер документа';
$MESS['RQ_IDENT_DOC_DATE'] = 'Дата выдачи документа';
$MESS['RQ_IDENT_DOC_ISSUED_BY'] = 'Кем выдан документ';
$MESS['RQ_IDENT_DOC_DEP_CODE'] = 'Код подразделения';

$MESS['REG_ADDRESS'] = 'Адрес (ю.а.)';
$MESS['REG_ADDRESS_2'] = 'Квартира / офис (ю.а.)';
$MESS['REG_ADDRESS_CITY'] = 'Город (ю.а.)';
$MESS['REG_ADDRESS_REGION'] = 'Район (ю.а.)';
$MESS['REG_ADDRESS_PROVINCE'] = 'Область (ю.а.)';
$MESS['REG_ADDRESS_POSTAL_CODE'] = 'Почтовый индекс (ю.а.)';
$MESS['REG_ADDRESS_COUNTRY'] = 'Страна (ю.а.)';
?>