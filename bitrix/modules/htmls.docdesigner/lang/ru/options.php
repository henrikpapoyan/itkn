<?
$MESS['DOCDESIGNER_TAB_SETTINGS'] = 'Собственные юр. лица';
$MESS['DOCDESIGNER_TAB_SETTINGS_ALT'] = 'Настройки списка "Собственные юридические лица"';
$MESS['DOCDESIGNER_TAB_RIGHTS'] = 'Права на доступ';
$MESS['DOCDESIGNER_TAB_RIGHTS_ALT'] = 'Настройка прав на доступ к модулю "Конструктор документов"';
//v1.5.1
$MESS['DOCDESIGNER_COMPANY'] = 'Список с собственными юр.лицами';
$MESS['DOCDESIGNER_COMPANY_FAKSIMILE'] = 'Факсимиле';
$MESS['DOCDESIGNER_NO_SELECT'] = 'не выбрано';
//v2.5.1
$MESS['DOCDESIGNER_TAB_CONTRACTS'] = 'Документы';
$MESS['DOCDESIGNER_TAB_CONTRACTS_ALT'] = 'Общие настройки для работы с документами';
$MESS['DOCDESIGNER_CONTRACTS_START_NUMBER'] = 'Начальный номер';
$MESS['DOCDESIGNER_CONTRACTS_PREFIX_NUMBER'] = 'Префикс нумерации';
$MESS['DOCDESIGNER_CONTRACTS_RESET_NUMBER'] = 'Сброс нумерации в начале года';
$MESS['DOCDESIGNER_CONTRACTS_LENGTH_NUMBER'] = 'Длина номера';
$MESS['DOCDESIGNER_CONTRACTS_AUTO_NUMBER'] = 'Автоматическая нумерация';
$MESS['DOCDESIGNER_TAB_COMPANY'] = 'CRM: Компания';
$MESS['DOCDESIGNER_TAB_COMPANY_ALT'] = 'Настройка реквизитов компании';
$MESS['DOCDESIGNER_CRM_COMPANY_INN'] = 'ИНН';
$MESS['DOCDESIGNER_CRM_COMPANY_KPP'] = 'КПП';
$MESS['DOCDESIGNER_CRM_COMPANY_RS'] = 'Расчетный счет';
$MESS['DOCDESIGNER_CRM_COMPANY_BANK'] = 'Банк';
$MESS['DOCDESIGNER_CRM_COMPANY_OGRN'] = 'ОГРН';
$MESS['DOCDESIGNER_CRM_COMPANY_POST_ADDRESS'] = 'Почтовый адрес';
$MESS['DOCDESIGNER_CRM_COMPANY_PHONE'] = 'Телефон компании';
$MESS['DOCDESIGNER_CONTRACTS_SAVE2LIBRARY'] = 'Сохранять договоры в библиотеке документов';
$MESS['DOCDESIGNER_CONTRACTS_TEMPLATES_LIBRARY'] = 'Библиотека DOCX-шаблонов';
$MESS['DOCDESIGNER_CONTRACTS_PATH2LIBRARY'] = 'Библиотека для хранения документов (по умолчанию)';
$MESS['DOCDESIGNER_COMPANY_CODE1C'] = 'Код 1С';
$MESS['DOCDESIGNER_COMPANY_ADDRESS_LEGAL'] = 'Юридический адрес';
$MESS['DOCDESIGNER_COMPANY_ADDRESS'] = 'Фактический адрес';
$MESS['DOCDESIGNER_COMPANY_PERSON'] = 'Кто подписывает договоры';
//v3.0.1
$MESS['DOCDESIGNER_TAB_INVOICES'] = 'Реeстр счетов';
$MESS['DOCDESIGNER_TAB_INVOICES_ALT'] = 'Настройки реестра счетов';
$MESS['DOCDESIGNER_INVOICES_LIST'] = 'Хранилище счетов';
$MESS['BX_INVOICES'] = 'Счета';
$MESS['DOCDESIGNER_INVOICE_TYPE'] = 'Тип';
$MESS['DOCDESIGNER_INVOICE_BDT'] = 'Статья бюджета';
$MESS['DOCDESIGNER_TAB_FNAMES'] = 'Имена PDF-файлов';
$MESS['DOCDESIGNER_TAB_FNAMES_ALT'] = 'Имена для PDF-файлов';
$MESS['DOCDESIGNER_TAB_SERVICE'] = 'Сервис';
$MESS['DOCDESIGNER_TAB_SERVICE_ALT'] = 'Создание списков и полей';
$MESS['DOCDESIGNER_CREATE_COMPANY'] = 'Создать список с собственными юр.лицами';
$MESS['DOCDESIGNER_CREATE_BTN'] = 'Создать';
$MESS['DOCDESIGNER_CREATE_CRM_COMPANY'] = 'Создать поля для CRM:Компании';
$MESS['DOCDESIGNER_SHOW_SERVICE_TAB'] = 'Показать сервисную закладку';
//v.3.0.2
$MESS['DOCDESIGNER_COMPANY_INVOICE_FACSIMILE'] = 'Факсимиле для счета';
//v3.1.1
$MESS['DOCDESIGNER_TAB_PROD_IBLOCK'] = 'Предложения';
$MESS['DOCDESIGNER_TAB_PROD_IBLOCK_ALT'] = 'Настройки списка товаров, услуг, предложений';
$MESS['DOCDESIGNER_OFFERS'] = 'Список с предложениями';
$MESS['DOCDESIGNER_OFFER_PRICE'] = 'Стоимость';
$MESS['DOCDESIGNER_TAB_DEFAULT_TEMPLATES'] = 'PDF-шаблоны';
$MESS['DOCDESIGNER_TAB_DEFAULT_TEMPLATES_ALT'] = 'Шаблоны по-умолчанию';
$MESS['DOCDESIGNER_DEFAULT_INVOICE_TEMPLATE'] = 'Шаблон счета по-умолчанию';
//v3.5.1
$MESS['DOCDESIGNER_TAB_V8EXCHANGE'] = '1С:V8';
$MESS['DOCDESIGNER_TAB_V8EXCHANGE_ALT'] = 'Обмен с 1С:Предприятие 8';
$MESS['DOCDESIGNER_V8EXCHANGE_CONFIGURATIONS'] = '1С:Предприятие 8.2 (8.2.14.528)<br>Бухгалтерия предприятия, редакция 2.0 (2.0.30.8)<br>Управление производственным предприятием, редакция 1.3 (1.3.23.1)<br>(обычное приложение)';
$MESS['DOCDESIGNER_V8EXCHANGE_IMPORT_PRODUCTS'] = 'Загрузка продуктов ->';
$MESS['DOCDESIGNER_V8EXCHANGE_EXPORT_INVOICES'] = 'Выгрузка счетов ->';
$MESS['DOCDESIGNER_V8EXCHANGE_IMPORT_BTN'] = 'Загрузить';
$MESS['DOCDESIGNER_V8EXCHANGE_EXPORT_BTN'] = 'Выгрузить';
$MESS['DOCDESIGNER_V8EXCHANGE_EPF'] = 'Внешняя обработка для 1С:Предприятие 8.2';
$MESS['V8EXCHANGE_DOWNLOAD'] = 'Скачать';
//v3.5.2
$MESS['DOCDESIGNER_TAB_OTHER'] = 'Прочее';
$MESS['DOCDESIGNER_TAB_OTHER_ALT'] = 'Прочие настройки';
//v3.6.3
$MESS['DOCDESIGNER_ADDMESSAGE2LOG_HEADER'] = 'Ведение журнала ошибок /bitrix/tmp/DocDesigner.txt';
$MESS['DOCDESIGNER_ADDMESSAGE2LOG'] = 'Подключить функцию AddMessage2Log';
//v4.5.1
$MESS['DOCDESIGNER_CRM_COMPANY_BANK_NAME'] = 'Наименование банка';
$MESS['DOCDESIGNER_CRM_COMPANY_BANK_BIK'] = 'БИК';
$MESS['DOCDESIGNER_CRM_COMPANY_BANK_KS'] = 'Корр.счет';
$MESS['DOCDESIGNER_CRM_COMPANY_BANK_CITY'] = 'Местоположение';
$MESS['DOCDESIGNER_CRM_COMPANY_BANK_DETAILS'] = 'Банковские реквизиты';
$MESS['DOCDESIGNER_CRM_COMPANY_ID_DATA'] = 'Идентификационные данные';
$MESS['DOCDESIGNER_CRM_COMPANY_OTHER'] = 'Прочее';
$MESS['DOCDESIGNER_NUMERATOR_1C'] = 'Выгружать в 1С';
//v5.0.1
$MESS['DOCDESIGNER_TAB_VARIABLES'] = 'CRM:Переменные';
$MESS['DOCDESIGNER_TAB_VARIABLES_ALT'] = 'CRM: Переменные';
$MESS['DOCDESIGNER_CRM_COMPANY_TITLE'] = 'Компания';
$MESS['DOCDESIGNER_CRM_DEAL_TITLE'] = 'Сделка';
$MESS['DOCDESIGNER_CRM_CONTACT_TITLE'] = 'Контакты';
$MESS['DOCDESIGNER_VARIABLES_HINT'] = 'На этой странице Вы можете назначить имена переменных для пользовательских полей.<br/><br/>Слева указано название поля, справа нужно указать название переменной, которая будет использована в DOCX-шаблоне. Название переменной должно быть на английском языке, без пробелов и специальных символов:<br/><br/><table>
<tr><td>Название пользовательского поля</td><td></td><td>Название переменной</td><td></td><td>Переменная в DOCX-шаблоне</td></tr>
<tr><td><b>Название поля</b></td><td> -> </td><td><b>FIELD_NAME</b></td><td> -> </td><td><b>${FIELD_NAME}</b></td></tr></table>';
$MESS['DOCDESIGNER_VARIABLE_NAME'] = 'Название переменной';

//v5.0.3
$MESS['DATE_CREATE'] = 'Дата создания';
$MESS['STAGE_ID'] = 'Стадия сделки';
$MESS['CLOSED'] = 'Сделка закрыта';
$MESS['TYPE_ID'] = 'Тип';
$MESS['OPPORTUNITY'] = 'Сумма';
$MESS['CURRENCY_ID'] = 'Валюта';
$MESS['PROBABILITY'] = 'Вероятность';
$MESS['COMMENTS'] = 'Комментарий';
$MESS['BEGINDATE'] = 'Дата начала';
$MESS['CLOSEDATE'] = 'Дата закрытия';
$MESS['TITLE'] = 'Название сделки';

$MESS['Nominative'] = 'Именительный';
$MESS['Genitive'] = "Родительный";
$MESS['Dative'] = "Дательный";
$MESS['Accusative'] = "Винительный";
$MESS['Instrumental'] = "Творительный";
$MESS['Prepositional'] = "Предложный";
$MESS['DOCDESIGNER_DECLINATION_CASE'] = 'Падеж для вывода полного ФИО';

$MESS['CONTACT.NAME'] = 'Имя';
$MESS['CONTACT.FULL_NAME'] = 'Имя Отчество';
$MESS['CONTACT.ADDRESS'] = 'Адрес';
$MESS['CONTACT.POST'] = 'Должность';

$MESS['DOCDESIGNER_CONTRACTS_NUMERATORS'] = 'Нумераторы договоров';
$MESS['DOCDESIGNER_V8EXCHANGE_FILE_EXPORT'] = 'Загружать в 1С файлы PDF & DOCX';

$MESS['DOCDESIGNER_DELETE_DOCS_ON_DELETE_BIZPROC'] = 'Удалять запись в журнале и документы из библиотеки при удалении бизнес-процесса';

$MESS['DOCDESIGNER_DECLINATOR'] = 'Сервис склонения';
$MESS['INNER_DECLINATOR'] = 'Внутренний';
$MESS['MORPHER'] = 'morpher.ru';

$MESS['DOCDESIGNER_CRM_LEAD_TITLE'] = 'Лиды';
$MESS['STATUS_ID'] = 'статуса лида';
$MESS['LEAD_TITLE'] = 'Название лида';
$MESS['COMPANY_TITLE'] = 'Название компании';
$MESS['DOCDESIGNER_REMOVE_PENNY'] = 'Выводить все суммы без копеек';

$MESS['DOCDESIGNER_TAB_LISTS'] = 'Списки';
$MESS['DOCDESIGNER_TAB_LISTS_ALT'] = 'Универсальные списки';

$MESS['DOCDESIGNER_CLOUD_DOCX'] = 'Использовать внешний сервис для генерации DOCX';
$MESS['DOCDESIGNER_CLOUD_DOCX_HINT'] = 'Данный сервис позволит Вам генерировать документы в формате DOCX на основании шаблона, который используется для PDF. Если Ваш портал работает в локальной сети, для использования внешнего сервиса генерации DOCX требуется разрешить доступ к сайту www.htmls.pw. Кроме того, Ваш сервер должен поддерживать cURL расширение PHP.';

$MESS['DOCDESIGNER_CRM_DISK_HEADER'] = 'Диск CRM<img src="/bitrix/images/htmls.docdesigner/help.png" style="float: right;" onclick="window.open(\'https://www.htmls.pw/learning/course/index.php?COURSE_ID=2&CHAPTER_ID=061&LESSON_PATH=60.61\');" />';
$MESS['DOCDESIGNER_CRM_DISK_DEAL'] = 'Сделки';
$MESS['DOCDESIGNER_CRM_DISK_COMPANY'] = 'Компания';
$MESS['DOCDESIGNER_CRM_DISK_CONTACT'] = 'Контакт';
$MESS['DOCDESIGNER_CRM_DISK_LEAD'] = 'Лид';
$MESS['DOCDESIGNER_CLOUD_DOCX_HEADER'] = 'Внешний сервер генерации DOCX<img src="/bitrix/images/htmls.docdesigner/help.png" style="float: right;" onclick="window.open(\'https://www.htmls.pw/learning/course/index.php?COURSE_ID=2&LESSON_ID=75&LESSON_PATH=60.65.72.75\');" />';
$MESS['DOCDESIGNER_QUANTITY_FLOAT'] = 'Точность количества в документах';
$MESS['DOCDESIGNER_BASIC_SETTINGS_HEADER'] = 'Основные настройки<img src="/bitrix/images/htmls.docdesigner/help.png" style="float: right;" onclick="window.open(\'https://www.htmls.pw/learning/course/index.php?COURSE_ID=2&CHAPTER_ID=072&LESSON_PATH=60.65.72\');" />';

$MESS['DOCDESIGNER_CLOUD_MCRYPT'] = 'Использовать шифрование с помощью функции mcrypt';
$MESS['DOCDESIGNER_CLOUD_MCRYPT_HINT'] = 'Если данная опция не включена исходный код документа передается в формате base64. <br/>При подключении данной опции исходный код документа будет шифроваться с помощью стандартной PHP-функции mcrypt.';

$MESS['DOCDESIGNER_CRM_INVOICE'] = 'CRM: Счет';
$MESS['DOCDESIGNER_CRM_INVOICE_ALT'] = 'Настройка соответствия платежных систем и организаций для создания счета';
$MESS['DOCDESIGNER_CRM_INVOCE_NO_CORP'] = 'Для создания счетов требуется добавить хотя-бы одно собственное юридическое лицо.';

?>