<?
//$MESS['DOCDESIGNER_BP_PARAMETERS'] = 'Параметры бизнес-процесса';
$MESS['DOCDESIGNER_TABLE_DOC_TITLE'] = 'Шаблон табличного документа';
$MESS['DOCDESIGNER_DOC_BUTTON_SAVE'] = 'Сохранить';
$MESS['DOCDESIGNER_DOC_BUTTON_CANCEL'] = 'Отмена';
$MESS["DOCDESIGNER_DOC_SAVEERR"] = "Ошибка сохранения";
$MESS['DOCDESIGNER_TABLE_FULLNAME_IMP'] = 'Фамилия Имя Отчество (им.п.)';
$MESS["DOCDESIGNER_TABLE_SHORTNAME_IMP"] = "Фамилия И. О. (им.п.)";
$MESS['DOCDESIGNER_TABLE_FULLNAME_RP'] = 'Фамилия Имя Отчество (р.п.)';
$MESS["DOCDESIGNER_TABLE_SHORTNAME_RP"] = "Фамилия И. О. (р.п.)";
$MESS['DOCDESIGNER_TABLE_FULLNAME_DP'] = 'Фамилия Имя Отчество (д.п.)';
$MESS["DOCDESIGNER_TABLE_SHORTNAME_DP"] = "Фамилия И. О. (д.п.)";
$MESS["DOCDESIGNER_TABLE_POSITION_IMP"] = "должность (им.п.)";
$MESS['DOCDESIGNER_TABLE_POSITION_RP'] = 'должность (р.п.)';
$MESS["DOCDESIGNER_TABLE_POSITION_DP"] = "должность (д.п.)";
//v1.5.1
$MESS['DOCDESIGNER_TABLE_COMPANY_BIG_LOGO'] = 'Большой логотип';
$MESS['DOCDESIGNER_TABLE_COMPANY_SMALL_LOGO'] = 'Маленький логотип';
$MESS['DOCDESIGNER_TABLE_COMPANY_BANK_DETAILS'] = 'Реквизиты компании';
//$MESS['DOCDESIGNER_TABLE_COMPANY_SEO'] = 'Фамилия И.О. руководителя';
$MESS['DOCDESIGNER_TABLE_COMPANY_SEO_FACSIMILE'] = 'Факсимиле руководителя';
//v2.5.1
$MESS['DOCDESIGNER_TABLE_OWN_COMPANY'] = 'Собственное юр. лицо';
$MESS['DOCDESIGNER_TABLE_COMPANY_NAME'] = 'Наименование';
$MESS['DOCDESIGNER_TABLE_COMPANY_INN'] = 'ИНН';
$MESS['DOCDESIGNER_TABLE_COMPANY_KPP'] = 'КПП';
$MESS['DOCDESIGNER_TABLE_COMPANY_BANK_NAME'] = 'Банк';
$MESS['DOCDESIGNER_TABLE_COMPANY_BANK_BIK'] = 'БИК';
$MESS['DOCDESIGNER_TABLE_COMPANY_BANK_RS'] = 'Расчетный счет';
$MESS['DOCDESIGNER_TABLE_COMPANY_BANK_KS'] = 'Корреспондентский счет';
$MESS['DOCDESIGNER_TABLE_COMPANY_ADDRESS_LEGAL'] = 'Юридический адрес';
$MESS['DOCDESIGNER_TABLE_COMPANY_PHONE'] = 'Телефон';
//v.3.0.1
$MESS['DOCDESIGNER_TABLE_CRM_COMPANY'] = 'CRM: Компания';
//v.3.0.2
$MESS['DOCDESIGNER_TABLE_COMPANY_INVOICE_FACSIMILE'] = 'Факсимиле для счета';
//v3.1.1
$MESS['DOCDESIGNER_TABLE_CRM_PRODUCTS'] = 'CRM: Продукты';
//v3.6.4
$MESS['DOCDESIGNER_MENU_TITLE'] = 'Вставить значение';
//v4.0.1
$MESS['DOCDESIGNER_NO_BORDER'] = 'Без границ';
$MESS['DOCDESIGNER_BORDER'] = 'Все границы';
$MESS['DOCDESIGNER_LEFT_BORDER'] = 'Левая граница';
$MESS['DOCDESIGNER_RIGHT_BORDER'] = 'Правая граница';
$MESS['DOCDESIGNER_TOP_BORDER'] = 'Верхняя граница';
$MESS['DOCDESIGNER_BOTTOM_BORDER'] = 'Нижняя граница';
$MESS['DOCDESIGNER_TEMPLATE_NAME'] = 'Название шаблона';
$MESS['DOCDESIGNER_DEMO_EXPIRED'] = 'Истек демонстрационный период работы модуля.<br>Все шаблоны сохранены и станут доступны после активации коммерческой лицензии.';
$MESS['DOCDESIGNER_NEW_TEMPLATE'] = 'Новый шаблон';
//v4.5.1
$MESS['DOCDESIGNER_TEMPLATE_COMMON'] = 'Общий шаблон';
$MESS['DOCDESIGNER_TEMPLATE_PORTRAIT'] = 'Портрет';
$MESS['DOCDESIGNER_TEMPLATE_LANDSCAPE'] = 'Ландшафт';
$MESS['DOCDESIGNER_TEMPLATE_ORIENATION'] = 'Ориентация страницы';
$MESS['DOCDESIGNER_CRM_PRODUCT_NAME'] = 'Наименование';
$MESS['DOCDESIGNER_CRM_PRODUCT_QUANTITY'] = 'Количество';
$MESS['DOCDESIGNER_CRM_PRODUCT_PRICE'] = 'Цена';
$MESS['DOCDESIGNER_CRM_PRODUCT_SUM'] = 'Сумма';
$MESS['DOCDESIGNER_CRM_PRODUCT_TOTAL_SUM'] = 'Сумма (всего)';
$MESS['DOCDESIGNER_CRM_PRODUCT_VAT'] = 'НДС';
$MESS['DOCDESIGNER_CRM_PRODUCT_TOTAL_VAT'] = 'НДС (всего)';
$MESS['DOCDESIGNER_CRM_PRODUCT_SUM_VAT'] = 'Сумма с НДС';
$MESS['DOCDESIGNER_CRM_PRODUCT_TOTAL_SUM_VAT'] = 'Сумма с НДС (всего)';
$MESS['DOCDESIGNER_CRM_PRODUCT'] = 'Товары сделки';
$MESS['DOCDESIGNER_CRM_PRODUCT_STR_NUM'] = 'Номер строки';
$MESS['DOCDESIGNER_CRM_PRODUCT_NUM_ROWS'] = 'Количество строк';
$MESS['DOCDESIGNER_CRM_PRODUCT_TOTAL_SUM_SPELLING'] = 'Сумма прописью';
$MESS['DOCDESIGNER_DOC_NUMBER'] = 'Номер документа';
$MESS['DOCDESIGNER_DOC_DATE'] = 'Дата документа';
//v5.1.2
$MESS['DOCDESIGNER_TABLE_CRM_DEAL'] = 'CRM: Сделка';
$MESS['DOCDESIGNER_TABLE_CRM_CONTACT'] = 'CRM: Контакт';
$MESS['DOCDESIGNER_CRM_PRODUCT_DESC'] = 'Описание товара';
$MESS['DOCDESIGNER_CRM_PRODUCT_MEASURE'] = 'Единица измерения';
$MESS['DOCDESIGNER_CRM_PRODUCT_VATRate'] = 'Ставка НДС';
$MESS['DOCDESIGNER_CRM_PRODUCT_DISCOUNT_RATE'] = 'Величина скидки';
$MESS['DOCDESIGNER_CRM_PRODUCT_DISCOUNT_SUM'] = 'Сумма скидки';
$MESS['DOCDESIGNER_CRM_PRODUCT_DISCOUNT_SUM_TOTAL'] = 'Сумма скидки (всего)';
$MESS['DOCDESIGNER_TABLE_COMPANY_CEO_SHORT'] = 'Руководитель (Фамилия И.О.)';
$MESS['DOCDESIGNER_TABLE_WORKFLOW_VARIABLES'] = 'Переменные';
$MESS['DOCDESIGNER_TABLE_WORKFLOW_PARAMETERS'] = 'Параметры';
$MESS['DOCDESIGNER_PageBreak'] = 'Новая страница';

$MESS['DOCDESIGNER_RESPONSIBLE'] = 'Ответственный';
$MESS['DOCDESIGNER_RESPONSIBLE_LAST_NAME'] = 'Фамилия';
$MESS['DOCDESIGNER_RESPONSIBLE_NAME'] = 'Имя';
$MESS['DOCDESIGNER_RESPONSIBLE_SECOND_NAME'] = 'Отчество';
$MESS['DOCDESIGNER_RESPONSIBLE_FI'] = 'Фамилия Имя';
$MESS['DOCDESIGNER_RESPONSIBLE_EMAIL'] = 'EMAIL';
$MESS['DOCDESIGNER_RESPONSIBLE_PHONE'] = 'Телефоны';
$MESS['DOCDESIGNER_RESPONSIBLE_POSITION'] = 'Должность';
$MESS['DOCDESIGNER_RESPONSIBLE_FACSIMILE'] = 'Факсимиле';
$MESS['DOCDESIGNER_TABLE_COMPANY_ADDRESS'] = 'Фактический адрес';
$MESS['DOCDESIGNER_CRM_PRODUCT_PRICE_INC_DISCOUNT'] = 'Цена без скидки';
$MESS['DOCDESIGNER_CRM_PRODUCT_TOTAL_SUM_WITHOUT_DISCOUNT_TAX'] = 'Сумма без скидки и налогов';

$MESS['DOCDESIGNER_TABLE_CRM_LEAD'] = 'CRM: Лид';

$MESS['DOCDESIGNER_TABLE_ADDRESS_2'] = 'Квартира / офис';
$MESS['DOCDESIGNER_TABLE_CITY'] = 'Город';
$MESS['DOCDESIGNER_TABLE_REGION'] = 'Район';
$MESS['DOCDESIGNER_TABLE_PROVINCE'] = 'Область';
$MESS['DOCDESIGNER_TABLE_POSTAL_CODE'] = 'Почтовый индекс';
$MESS['DOCDESIGNER_TABLE_COUNTRY'] = 'Страна';
$MESS['DOCDESIGNER_TABLE_UA'] = ' (ю.а.)';
$MESS['DOCDESIGNER_TABLE_PA'] = ' (ф.а.)';
$MESS['DOCDESIGNER_CRM_PRODUCT_TOTAL_SUM_VAT_SPELLING'] = 'Сумма прописью (всего)';

$MESS['DOCDESIGNER_TABLE_COMPANY_OGRN'] = 'ОГРН';
$MESS['DOCDESIGNER_TABLE_ADDRESS_STREET'] = 'Улица, дом, корпус, строение';

$MESS['DOCDESIGNER_footerParameters'] = 'Параметры колонтитула';
$MESS['DOCDESIGNER_pageNumber'] = 'Номер текущей страницы';
$MESS['DOCDESIGNER_totalPages'] = 'Общее количество страниц (только для PDF)';
$MESS['DOCDESIGNER_contactFio'] = 'Фамилия и инициалы контакта';
$MESS['DOCDESIGNER_ownFio'] = 'Фамилия и инициалы руководителя собственной компании';
$MESS['DOCDESIGNER_initiallingOfPages'] = 'Парафирование страниц';
$MESS['DOCDESIGNER_DocNumber'] = 'Номер документа';
$MESS['DOCDESIGNER_DocDate'] = 'Дата документа';
$MESS['DOCDESIGNER_DocMargins'] = 'Поля документа (мм)';
$MESS['DOCDESIGNER_DocLeftMargin'] = 'Слева';
$MESS['DOCDESIGNER_DocRightMargin'] = 'Справа';
$MESS['DOCDESIGNER_DocTopMargin'] = 'Сверху';
$MESS['DOCDESIGNER_DocBottomMargin'] = 'Снизу';
$MESS['DOCDESIGNER_DocFooter'] = 'Нижний колонтитул';
$MESS['DOCDESIGNER_DocLeftFooter'] = 'Лево';
$MESS['DOCDESIGNER_DocRightFooter'] = 'Право';
$MESS['DOCDESIGNER_DocMiddleFooter'] = 'Центр';
$MESS['DOCDESIGNER_DocFooterFont'] = 'Размер шрифта';
$MESS['DOCDESIGNER_DocBackground'] = 'Фоновое изображение';
$MESS['DOCDESIGNER_DelBackground'] = 'Удалить фоновое изображение';
$MESS['DOCDESIGNER_DocFonts'] = 'Выбор шрифтов для';
$MESS['DOCDESIGNER_DocPdfFont'] = 'PDF';
$MESS['DOCDESIGNER_DocDocxFont'] = 'DOCX';
$MESS['DOCDESIGNER_TABLE_COMPANY_COMPOSITE_FACSIMILE'] = 'Составное факсимиле';
$MESS['DOCDESIGNER_TABLE_COMPANY_CEO_FACSIMILE'] = 'Факсимиле руководителя';
$MESS['DOCDESIGNER_calculator'] = 'Вычислить [CALC][/CALC]';
$MESS['DOCDESIGNER_spelling'] = 'Сумма прописью [SPELL][/SPELL]';
$MESS['DOCDESIGNER_cif'] = 'Если [if][/if]';
$MESS['DOCDESIGNER_celseif'] = 'Иначе [else][/else]';
$MESS['DOCDESIGNER_celse'] = 'ИначеЕсли [elseif][/elseif]';
$MESS['DOCDESIGNER_functions'] = 'Функции';
//$MESS['DOCDESIGNER_DocBottomMargin'] = 'Снизу';
$MESS['DOCDESIGNER_TABLE_COMPANY_CEO_FULL'] = 'Руководитель (Фамилия Имя Отчество)';
$MESS['DOCDESIGNER_BANKING_DETAILS'] = 'Банковские реквизиты';
$MESS['DOCDESIGNER_CRM_PRODUCT_TOTAL_VAT_SPELLING'] = 'Сумма НДС прописью (всего)';

$MESS['invoiceTable'] = 'Пример таблицы для счета';
$MESS['DOCDESIGNER_TABLE_COMPANY_BANK_CITY'] = 'Местоположение банка';
$MESS['DOCDESIGNER_TABLE_COMPANY_POST_ADDRESS'] = 'Почтовый адрес';

$MESS['DOCDESIGNER_TABLE_FULLNAME'] = 'Фамилия Имя Отчество';
?>

