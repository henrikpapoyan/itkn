<?
$MESS['DDS_LIST_NAME'] = 'Собственные юридические лица';
$MESS['DDS_NAME'] = 'Название';
$MESS['DDS_BANK_DETAILS'] = 'Банковские реквизиты';
$MESS['DDS_SMALL_LOGO'] = 'Маленький логотип';
$MESS['DDS_BIG_LOGO'] = 'Большой логотип';
$MESS['DDS_FAKSIMILE'] = 'Факсимиле';
$MESS['DDS_PERSON'] = 'Фамилия И.О. руководителя';
$MESS['DDS_INN'] = 'ИНН';
$MESS['DDS_KPP'] = 'КПП';
$MESS['DDS_OGRN'] = 'ОГРН';
$MESS['DDS_RS'] = 'Расчетный счет';
$MESS['DDS_BANK'] = 'Банк';
$MESS['DDS_POST_ADDRESS'] = 'Почтовый адрес';
$MESS['DDS_PHONE'] = 'Телефон компании';
$MESS['DDS_ADDRESS_LEGAL'] = 'Юридичекий адрес';
$MESS['DDS_ADDRESS'] = 'Фактический адрес';
//$MESS[''] = '';
?>