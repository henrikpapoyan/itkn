<?
$MESS["DOCDESIGNER_MODULE_NAME"] = 'Конструктор документов';
$MESS["DOCDESIGNER_MODULE_DESC"] = 'Создание типовых и табличных документов';
$MESS["DOCDESIGNER_INSTALL_TITLE"] = 'Установка модуля DocDesigner';
$MESS["DOCDESIGNER_UNINSTALL_TITLE"] = 'Удаление модуля DocDesigner';

$MESS['DDS_LIST_NAME'] = 'Собственные юридические лица';
$MESS['DDS_NAME'] = 'Название';
$MESS['DDS_BANK_DETAILS'] = 'Банковские реквизиты';
$MESS['DDS_SMALL_LOGO'] = 'Маленький логотип';
$MESS['DDS_BIG_LOGO'] = 'Большой логотип';
$MESS['DDS_FAKSIMILE'] = 'Факсимиле';
$MESS['DDS_PERSON'] = 'Фамилия И.О. руководителя';
$MESS['DDS_INN'] = 'ИНН';
$MESS['DDS_KPP'] = 'КПП';
$MESS['DDS_OGRN'] = 'ОГРН';
$MESS['DDS_RS'] = 'Расчетный счет';
$MESS['DDS_BANK_NAME'] = 'Наименование банка';
$MESS['DDS_BANK_BIK'] = "БИК";
$MESS['DDS_BANK_KS'] = "Корр. счет";
$MESS['DDS_BANK_CITY'] = "Местоположение банка";
$MESS['DDS_POST_ADDRESS'] = 'Почтовый адрес';
$MESS['DDS_PHONE'] = 'Телефон компании';
$MESS['DDS_ADDRESS_LEGAL'] = 'Юридический адрес';
$MESS['DDS_ADDRESS'] = 'Фактический адрес';

$MESS['NUMBER_TEMPLATE'] = 'Шаблон номера';
$MESS['NUMBER_LENGTH'] = 'Длина номера';
$MESS['NUMBER_RESET'] = 'Сброс нумерации в начале года';
$MESS['NUMBER_START'] = 'Начальное значение';
$MESS['CONTRACT_UNIQUE'] = 'Уникальность в рамках договора';
$MESS['CORP_UNIQUE'] = 'Общий для всех юр. лиц';
$MESS['NUMERATOR_NAME'] = 'Нумераторы';

$MESS['DOCS_REGISTER'] = 'Журнал документов';
$MESS['DR_NAME'] = 'Номер';
$MESS['DR_DATE_CREATE'] = 'Дата';
$MESS['DR_DOC'] = 'Документ';
$MESS['DR_SUMMA'] = 'Сумма';
$MESS['DR_OWN_ENTITIES'] = 'Юр. лицо';
$MESS['DR_COMPANY'] = 'Компания';
$MESS['DR_DEAL'] = 'Сделка';
$MESS['DR_FILE'] = 'Файл';
$MESS['DR_PREFIX'] = 'Префикс (сис.)';
$MESS['DR_NUMBER_SYS'] = 'Номер (сис.)';
$MESS['DR_BIZPROC_ID'] = 'ID бизнес-процесса (сис.)';
$MESS['DR_YEAR'] = 'Год (сис.)';
$MESS['DR_NUMERATOR'] = 'Нумератор (сис.)';
$MESS['DR_TABLE'] = 'Спецификация (сис.)';

$MESS['NUM_CONTRACT'] = 'Договор';
$MESS['NUM_INVOICE'] = 'Счет';

$MESS['DOC_JOURNAL'] = 'Журнал документов';
$MESS['BP_PDF'] = 'Бизнес-процессы + PDF-шаблоны';
$MESS['PDF'] = 'PDF-шаблоны';
//$MESS['TITLE'] = 'Конструктор документов';
$MESS['CONFIRM'] = 'Применить действие для всех записей журнала, в том числе на других страницах?';
$MESS['EXAMPLES'] = 'Примеры';
$MESS['DOC_DESIGNER'] = 'Конструктор документов';

$MESS['DOC_DESIGNER_TEMPLATES'] = 'Шаблоны для Конструктора документов';
$MESS['DOC_DESIGNER_TMP_FILE'] = 'Файл шаблона';

$MESS['DDS_CEO_SIGN'] = 'Подпись руководителя';
$MESS['DDS_CEO_POST'] = 'Должность директора';
$MESS['DDS_CEO_BASE'] = 'На основании';
$MESS['DDS_ACC_POST'] = 'Должность бухгалтера';
$MESS['DDS_ACC_NAME'] = 'Главный бухгалтер';
$MESS['DDS_ACC_SIGN'] = 'Подпись главного бухгалтера';
$MESS['DDS_STAMP'] = 'Печать';

$MESS ['DD_UNINSTALL_WARNING'] = "Внимание!\n Модуль будет удален из системы.";
$MESS ['DD_UNINSTALL_SAVEDATA'] = "Если Вы использовали шаблоны или действия бизнес-процессов модуля в работе, удаление файлов может привести к ошибкам в работе Битрикс24.";
$MESS ['DD_UNINSTALL_SAVETABLE'] = "Удалить все шаблоны";

$MESS ['DOCDESIGNER_INSTALL_LISTS'] = "Универсальные списки";
$MESS ['DOCDESIGNER_INSTALL_SECTION_NAME'] = "Группы";
$MESS ['DOCDESIGNER_INSTALL_ELEMENT_NAME'] = "Элементы списка";
?>
