<?
global $MESS;
$MESS['apply4all'] = 'Применить действие для всех записей журнала, в том числе на других страницах?';
$MESS['indexTitle'] = 'Конструктор документов';
$MESS['menuDocDesigner'] = 'Журнал документов';
$MESS['menuBizProc'] = 'CRM: Бизнес-процессы';
$MESS['menuBizProc2'] = 'Бизнес-процессы';
$MESS['menuBizProc3'] = 'Процессы';
$MESS['menuLists'] = 'Справочники';
$MESS['menuPdf'] = 'PDF-шаблоны';
$MESS['menuEx'] = 'Примеры';
//$MESS[''] = '';
?>