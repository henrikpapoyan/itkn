<?
//v8.4.3 10.07.2016
$module_id = "htmls.docdesigner";
$DocDesigner_RIGHT = $APPLICATION->GetGroupRight($module_id);
//define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/bitrix/tmp/DocDesigner.txt");
if ($DocDesigner_RIGHT>="R") :

	global $MESS, $DB;

	include(GetLangFileName($GLOBALS["DOCUMENT_ROOT"]."/bitrix/modules/main/lang/", "/options.php"));
	include(GetLangFileName($GLOBALS["DOCUMENT_ROOT"]."/bitrix/modules/".$module_id."/lang/", "/options.php"));

	$arAllOptionsP = array(
		array("DOCDESIGNER_COMPANY", GetMessage("DOCDESIGNER_COMPANY"), "", Array("selectbox"), $arSelectIBlock),
		array("DOCDESIGNER_COMPANY_FAKSIMILE", GetMessage("DOCDESIGNER_COMPANY_FAKSIMILE"), "", Array("selectbox"), $arProp),
	);

	if ($REQUEST_METHOD=="POST" && $DocDesigner_RIGHT=="W" && check_bitrix_sessid() && $_REQUEST["SUpdate"]){
		for ($i = 0; $i < count($arAllOptionsP); $i++){
			$name = $arAllOptionsP[$i][0];
			$val = $$name;
			if ($arAllOptionsP[$i][3][0] == "checkbox" && $val != "Y")
				$val = "N";
			COption::SetOptionString($module_id, $name, $val, $arAllOptionsP[$i][1]);
		}
	}

	?>
	<script type="text/javascript">
	var stop;

	function EndScan(){
		stop = true;
	}
	function StartScan(val){
		stop = false;
		DoNext(true, val);
	}

	function DoNext(start, val){
		queryString='lang=ru&mode=scan';

		if(start)
			queryString+='&listID='+val;
		else
			queryString+='&step=DoNext';

		CHttpRequest.Action = function(result)
		{
			CloseWaitWindow();
			arr = (eval('('+result+')'));
			innerHtml = '';
			for(var i in arr){
				innerHtml = innerHtml + '<option value="' + i + '">' + arr[i] + '</option>';
			}

			strlist = 'DOCDESIGNER_COMPANY_FAKSIMILE,DOCDESIGNER_COMPANY_OGRN,DOCDESIGNER_COMPANY_INN,DOCDESIGNER_COMPANY_KPP,DOCDESIGNER_COMPANY_ADDRESS_LEGAL,DOCDESIGNER_COMPANY_ADDRESS,DOCDESIGNER_COMPANY_POST_ADDRESS,DOCDESIGNER_COMPANY_PHONE,DOCDESIGNER_COMPANY_PERSON,DOCDESIGNER_COMPANY_RS,DOCDESIGNER_COMPANY_BANK_NAME,DOCDESIGNER_COMPANY_INVOICE_FACSIMILE';
			arr = strlist.split(',');
			for(var i in arr){
				elem = document.getElementById(arr[i]);
				elem.innerHTML = innerHtml;
			}
			if (result.search('DoNext') == -1)
				EndScan();
			else if(!stop)
				setTimeout('DoNext()', 1000);
		}
		ShowWaitWindow();

		CHttpRequest.Send('/bitrix/admin/docdesigner_options.php?'+queryString);
	}

	function StartScan2(val){
		stop = false;
		DoNext2(true, val);
	}

	function DoNext2(start, val){
		queryString='lang=ru&mode=scan';

		if(start)
			queryString+='&listID='+val;
		else
			queryString+='&step=DoNext';

		CHttpRequest.Action = function(result)
		{
			CloseWaitWindow();
			arr = (eval('('+result+')'));
			innerHtml = '';
			for(var i in arr){
				innerHtml = innerHtml + '<option value="' + i + '">' + arr[i] + '</option>';
			}

			strlist = 'DOCDESIGNER_OFFER_PRICE';
			arr = strlist.split(',');
			for(var i in arr){
				elem = document.getElementById(arr[i]);
				elem.innerHTML = innerHtml;
			}
			if (result.search('DoNext') == -1)
				EndScan();
			else if(!stop)
				setTimeout('DoNext2()', 1000);
		}
		ShowWaitWindow();

		CHttpRequest.Send('/bitrix/admin/docdesigner_options.php?'+queryString);
	}

	function StartImport(){
		CHttpRequest.Action = function(result)
		{
			CloseWaitWindow();
			elem = document.getElementById('import_answer');
			elem.innerHTML = result;
		}
		ShowWaitWindow();

		CHttpRequest.Send('/bitrix/admin/docdesigner_1c_exchange.php?action=import');
	}

	function StartExport(){
		CHttpRequest.Action = function(result)
		{
			CloseWaitWindow();
			elem = document.getElementById('export_answer');
			elem.innerHTML = result;
		}
		ShowWaitWindow();
		CHttpRequest.Send('/bitrix/admin/docdesigner_1c_exchange.php?action=export');
	}
	</script>

	<?
	$IsCrm = IsModuleInstalled("crm");
	$aTabs = array(
		array("DIV" => "edit3", "TAB" => GetMessage("DOCDESIGNER_TAB_CONTRACTS"), "ICON" => "docdesigner_settings_icon", "TITLE" => GetMessage("DOCDESIGNER_TAB_CONTRACTS_ALT")),
		array("DIV" => "edit1", "TAB" => GetMessage("DOCDESIGNER_TAB_SETTINGS"), "ICON" => "docdesigner_settings_icon", "TITLE" => GetMessage("DOCDESIGNER_TAB_SETTINGS_ALT")),
	);
    $corp = COption::GetOptionString($module_id, "DOCDESIGNER_COMPANY", 0);
    if($corp > 0 && $IsCrm){
      $aTabs[] = array("DIV" => "edit9", "TAB" => GetMessage("DOCDESIGNER_CRM_INVOICE"), "ICON" => "docdesigner_settings_icon", "TITLE" => GetMessage("DOCDESIGNER_CRM_INVOICE_ALT"));
    }
	//if($IsCrm){
	//	$aTabs[] = array("DIV" => "edit4", "TAB" => GetMessage("DOCDESIGNER_TAB_COMPANY"), "ICON" => "docdesigner_settings_icon", "TITLE" => GetMessage("DOCDESIGNER_TAB_COMPANY_ALT"));}
	//$aTabs[] = array("DIV" => "edit6", "TAB" => GetMessage("DOCDESIGNER_TAB_LISTS"), "ICON" => "docdesigner_settings_icon", "TITLE" => GetMessage("DOCDESIGNER_TAB_LISTS_ALT"));
	$aTabs[] = array("DIV" => "edit7", "TAB" => GetMessage("DOCDESIGNER_TAB_OTHER"), "ICON" => "docdesigner_settings_icon", "TITLE" => GetMessage("DOCDESIGNER_TAB_OTHER_ALT"));

	if($IsCrm){
		$aTabs[] = array("DIV" => "edit8", "TAB" => GetMessage("DOCDESIGNER_TAB_VARIABLES"), "ICON" => "docdesigner_settings_icon", "TITLE" => GetMessage("DOCDESIGNER_TAB_VARIABLES_ALT"));}

    $aTabs[] = array("DIV" => "edit2", "TAB" => GetMessage("DOCDESIGNER_TAB_RIGHTS"), "ICON" => "docdesigner_settings_icon", "TITLE" => GetMessage("DOCDESIGNER_TAB_RIGHTS_ALT"));

    CModule::IncludeModule('lists');
	$CList = new CList(COption::GetOptionString($module_id, "DOCDESIGNER_COMPANY", 0));
	$arFields = $CList->GetFields();
	$arProp['no'] = GetMessage("DOCDESIGNER_NO_SELECT");
	foreach($arFields as $k => $arr){
		if(strpos($k, 'ROPERTY_') > 0){
			$arProp[$arr['ID']] = $arr['NAME'];
		}
	}
    //echo '<pre>'; print_r($arProp); echo '</pre>';
    $arAllOptions = array();
	CModule::IncludeModule("iblock");
	$res = CIBlock::GetList(Array(), Array('TYPE'=>"lists",'ACTIVE'=>'Y'), true);
	if($res->SelectedRowsCount() > 0){
		$arSelectIBlock['no'] = GetMessage("DOCDESIGNER_NO_SELECT");
		while($ar_res = $res->Fetch()){
			$arSelectIBlock[$ar_res["ID"]] = $ar_res['NAME'];
            $CList = new CList($ar_res["ID"]);
	        $arFields = $CList->GetFields();
            //print_r($arFields);
            $arPropL = array();
        	foreach($arFields as $k => $arr){
        		if(strpos($k, 'ROPERTY_') > 0){
        		  if($arr['PROPERTY_TYPE'] == 'F')
        			$arPropL[$arr['ID']] = $arr['NAME'];
        		}
        	}
            if(count($arPropL) > 0)
                $arAllOptions['lists'][] = array('DOCDESIGNER_LIST_'.$ar_res["ID"], $ar_res['NAME'], '', Array("selectbox", $arPropL));
		}
	}

	if(!$IsCrm){
		$res = CIBlock::GetList(Array(), Array('TYPE'=>"catalog",'ACTIVE'=>'Y'), true);
		if($res->SelectedRowsCount() > 0){
			$arSelectCatalog['no'] = GetMessage("DOCDESIGNER_NO_SELECT");
			while($ar_res = $res->Fetch()){
				$arSelectCatalog[$ar_res["ID"]] = $ar_res['NAME'];
			}
		}
	}
	else{
		$arSelectCatalog = $arSelectIBlock;
	}

	if($IsCrm){
		$res = CIBlock::GetList(Array(), Array('TYPE'=>"library",'ACTIVE'=>'Y'), true);
	}
	else{
		$res = CIBlock::GetList(Array(), Array('TYPE'=>"lists",'ACTIVE'=>'Y'), true);
	}

    $discConverted = \Bitrix\Main\Config\Option::get('disk', 'successfully_converted', false);
    if($discConverted == 'Y'){
      CModule::IncludeModule('disk');
      CModule::IncludeModule('socialnetwork');
      if(CModule::IncludeModuleEx("htmls.docdesigner") < 3){
        $cDoc = new CDocDesignerContracts;
        $listStorage = $cDoc->getDisks();
      }
      else{
        $listStorage = \Bitrix\Disk\Storage::GetModelList(array());
      }
    }

	if($res->SelectedRowsCount() > 0){
      if($discConverted == 'Y'){
        $htmlsDiscConverted = \Bitrix\Main\Config\Option::get('htmls.docdesigner', 'successfully_converted', false);
        if($htmlsDiscConverted == 'Y'){
          $arSelectLibrary['no'] = GetMessage("DOCDESIGNER_NO_SELECT");

          //$listStorage = \Bitrix\Disk\Storage::GetModelList(array());
          foreach($listStorage as $id => $name){
            $arSelectLibrary[$id] = $name['name'];
          }
        }
        else{
          $LIBRARY_ID = \Bitrix\Main\Config\Option::get("htmls.docdesigner", "DOCDESIGNER_CONTRACTS_PATH2LIBRARY");
          //echo $LIBRARY_ID;
          $tmpLIBRARY_ID = \Bitrix\Main\Config\Option::get("htmls.docdesigner", "DOCDESIGNER_CONTRACTS_TEMPLATES_LIBRARY");
          //echo $tmpLIBRARY_ID;
          $arWebDav = array();
    	  while($ar_res = $res->Fetch()){
    	    $arWebDav[$ar_res["ID"]] = $ar_res['NAME'];
    	  }
          //echo '<pre>'; print_r($arWebDav); echo '</pre>';

          $arSelectLibrary['no'] = GetMessage("DOCDESIGNER_NO_SELECT");
          $listStorage = \Bitrix\Disk\Storage::GetModelList(array());
          foreach($listStorage as $oStorage){
            //echo $oStorage->getXmlId().'='. $arWebDav[$oStorage->getXmlId()].'->'.$oStorage->getId().'<br>';
            if($LIBRARY_ID == $oStorage->getXmlId()){
              COption::SetOptionString("htmls.docdesigner", "DOCDESIGNER_CONTRACTS_PATH2LIBRARY", $oStorage->getId());
            }
            elseif($tmpLIBRARY_ID == $oStorage->getXmlId()){
              COption::SetOptionString("htmls.docdesigner", "DOCDESIGNER_CONTRACTS_TEMPLATES_LIBRARY", $oStorage->getId());
            }
            //else{
              $arSelectLibrary[$oStorage->getId()] = $oStorage->getName();
            //}
          }
          COption::SetOptionString("htmls.docdesigner", "successfully_converted", 'Y');
          //echo '<pre>'; print_r($arDisk); echo '</pre>';

        }
      }
      else{
		$arSelectLibrary['no'] = GetMessage("DOCDESIGNER_NO_SELECT");
		while($ar_res = $res->Fetch()){
			$arSelectLibrary[$ar_res["ID"]] = $ar_res['NAME'];
		}
      }
      $arSelectTmpLibrary = $arSelectLibrary;
      $res = CIBlock::GetList(Array(), Array('TYPE'=>"lists",'ACTIVE'=>'Y'), true);
      while($ar_res = $res->Fetch()){
	    $arSelectTmpLibrary[$ar_res["ID"]] = $ar_res['NAME'];
	  }
	}
    else{
      //disk
      $arSelectLibrary['no'] = GetMessage("DOCDESIGNER_NO_SELECT");
      $arSelectTmpLibrary['no'] = GetMessage("DOCDESIGNER_NO_SELECT");

      //$listStorage = \Bitrix\Disk\Storage::GetModelList(array());
      foreach($listStorage as $id => $name){
        $arSelectLibrary[$id] = $name['name'];
      }

      $res = CIBlock::GetList(Array(), Array('TYPE'=>"lists",'ACTIVE'=>'Y'), true);
      while($ar_res = $res->Fetch()){
	    $arSelectTmpLibrary[$ar_res["ID"]] = $ar_res['NAME'];
	  }
    }

	//get user fields for company
	if($IsCrm){
		$arUFCompany['no'] = GetMessage('DOCDESIGNER_NO_SELECT');
		CModule::IncludeModule("crm");
		$crm = new CCrmFields($USER_FIELD_MANAGER, 'CRM_COMPANY');
		$arFields = $crm->GetFields();
		foreach($arFields as $CODE => $v){
			//$arUFCompany[$CODE] = htmlspecialchars($v['EDIT_FORM_LABEL'], ENT_QUOTES, LANG_CHARSET);
			$arUFCompany[$CODE] = $v['EDIT_FORM_LABEL'];
		}
	}
	else{

	}
	//check invoices list
	$property_enums = CIBlockPropertyEnum::GetList(Array("SORT"=>"ASC"), Array("IBLOCK_ID"=>COption::GetOptionInt($module_id, 'DOCDESIGNER_INVOICES_REGISTER'), "CODE"=>"TYPE"));
	while($enum_fields = $property_enums->GetNext()){
		$arType[$enum_fields["ID"]] = $enum_fields["VALUE"];
	}

	if($IsCrm){
	    if(CModule::IncludeModuleEx("htmls.docdesigner") < 3){
			$arVariables = CDocDesignerProcessing::GetCrmFields(true);
            //echo '<pre>'.LANG_CHARSET; print_r($arVariables); echo '</pre>';
            foreach($arVariables['DEAL'] as $fid => $ar){
              if($ar['NAME'])
                $arDiskDeal[$fid] = $ar['NAME'];
            }
            foreach($arVariables['COMPANY'] as $fid => $ar){
              if($ar['NAME'])
                $arDiskCompany[$fid] = $ar['NAME'];
            }
            foreach($arVariables['CONTACT'] as $fid => $ar){
              if($ar['NAME'])
                $arDiskContact[$fid] = $ar['NAME'];
            }
            foreach($arVariables['LEAD'] as $fid => $ar){
              if($ar['NAME'])
                $arDiskLead[$fid] = $ar['NAME'];
            }
            //echo '<pre>'; print_r($arVariables); echo '</pre>';
		}
	}

    $arDecl[0] = GetMessage('Nominative');
    $arDecl[1] = GetMessage('Genitive');
    $arDecl[2] = GetMessage('Dative');
    $arDecl[3] = GetMessage('Accusative');
    $arDecl[4] = GetMessage('Instrumental');
    $arDecl[5] = GetMessage('Prepositional');

    if($IsCrm){
	    $res=CCrmFieldMulti::GetEntityTypes();
	    foreach($res as $pref => $arr){
	    	foreach($arr as $k => $v){
	    		$arCrmMulti[$pref."_".$k] =	$v['FULL'];
	    	}

	    }
    }
    //get numerators list
    $IBLOCK_IB = COption::GetOptionInt("htmls.docdesigner", "DOCDESIGNER_NUMERATORS");
	$arSelect = Array("ID", "NAME");
	$arFilter = Array("IBLOCK_ID"=>IntVal($IBLOCK_IB), "ACTIVE"=>"Y");
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
	while($ob = $res->GetNextElement()){
		$arFields = $ob->GetFields();
		$arNumerators[$arFields['ID']] = $arFields['NAME'];
	}

    $arFloat = array(0,1,2);

	//$arAllOptions = array = array(
	$arAllOptions['main'] = array(
			array("DOCDESIGNER_COMPANY", GetMessage("DOCDESIGNER_COMPANY"), "", Array("selectbox", $arSelectIBlock)),
			array("DOCDESIGNER_COMPANY_FAKSIMILE", GetMessage("DOCDESIGNER_COMPANY_FAKSIMILE"), "", Array("selectbox", $arProp)),
			//GetMessage("DOCDESIGNER_CRM_COMPANY_ID_DATA"),
			array('DOCDESIGNER_COMPANY_OGRN', GetMessage('DOCDESIGNER_CRM_COMPANY_OGRN'), '', Array("selectbox", $arProp)),
			array('DOCDESIGNER_COMPANY_INN', GetMessage('DOCDESIGNER_CRM_COMPANY_INN'), '', Array("selectbox", $arProp)),
			array('DOCDESIGNER_COMPANY_KPP', GetMessage('DOCDESIGNER_CRM_COMPANY_KPP'), '', Array("selectbox", $arProp)),
			array('DOCDESIGNER_COMPANY_ADDRESS_LEGAL', GetMessage('DOCDESIGNER_COMPANY_ADDRESS_LEGAL'), '', Array("selectbox", $arProp)),
			array('DOCDESIGNER_COMPANY_ADDRESS', GetMessage('DOCDESIGNER_COMPANY_ADDRESS'), '', Array("selectbox", $arProp)),
			array('DOCDESIGNER_COMPANY_POST_ADDRESS', GetMessage('DOCDESIGNER_CRM_COMPANY_POST_ADDRESS'), '', Array("selectbox", $arProp)),
			array('DOCDESIGNER_COMPANY_PHONE', GetMessage('DOCDESIGNER_CRM_COMPANY_PHONE'), '', Array("selectbox", $arProp)),
			array('DOCDESIGNER_COMPANY_PERSON', GetMessage('DOCDESIGNER_COMPANY_PERSON'), '', Array("selectbox", $arProp)),
			//GetMessage("DOCDESIGNER_CRM_COMPANY_BANK_DETAILS"),
			array('DOCDESIGNER_COMPANY_RS', GetMessage('DOCDESIGNER_CRM_COMPANY_RS'), '', Array("selectbox", $arProp)),
			array('DOCDESIGNER_COMPANY_BANK_NAME', GetMessage('DOCDESIGNER_CRM_COMPANY_BANK_NAME'), '', Array("selectbox", $arProp)),
			array('DOCDESIGNER_COMPANY_BANK_BIK', GetMessage('DOCDESIGNER_CRM_COMPANY_BANK_BIK'), '', Array("selectbox", $arProp)),
			array('DOCDESIGNER_COMPANY_BANK_KS', GetMessage('DOCDESIGNER_CRM_COMPANY_BANK_KS'), '', Array("selectbox", $arProp)),
			array('DOCDESIGNER_COMPANY_BANK_CITY', GetMessage('DOCDESIGNER_CRM_COMPANY_BANK_CITY'), '', Array("selectbox", $arProp)),
			//GetMessage("DOCDESIGNER_CRM_COMPANY_OTHER"),
			array("DOCDESIGNER_COMPANY_INVOICE_FACSIMILE", GetMessage("DOCDESIGNER_COMPANY_INVOICE_FACSIMILE"), "", Array("selectbox", $arProp)),
			array("DOCDESIGNER_COMPANY_CODE1C", GetMessage("DOCDESIGNER_COMPANY_CODE1C"), "", Array("selectbox", $arProp)));
		$arAllOptions['contracts'] = array(
			GetMessage("DOCDESIGNER_BASIC_SETTINGS_HEADER"),
			array("DOCDESIGNER_CONTRACTS_TEMPLATES_LIBRARY", GetMessage("DOCDESIGNER_CONTRACTS_TEMPLATES_LIBRARY"), "", Array("selectbox", $arSelectTmpLibrary)),
			array("DOCDESIGNER_CONTRACTS_PATH2LIBRARY", GetMessage("DOCDESIGNER_CONTRACTS_PATH2LIBRARY"), "", Array("selectbox", $arSelectLibrary)),
            array("DOCDESIGNER_DELETE_DOCS_ON_DELETE_BIZPROC", GetMessage("DOCDESIGNER_DELETE_DOCS_ON_DELETE_BIZPROC"), "", Array("checkbox")),
            array("DOCDESIGNER_REMOVE_PENNY", GetMessage("DOCDESIGNER_REMOVE_PENNY"), "", Array("checkbox")),
            array("DOCDESIGNER_QUANTITY_FLOAT", GetMessage("DOCDESIGNER_QUANTITY_FLOAT"), "", Array("selectbox", $arFloat)),
			array("DOCDESIGNER_DECLINATION_CASE", GetMessage("DOCDESIGNER_DECLINATION_CASE"), "1", Array("selectbox", $arDecl)),
			array("DOCDESIGNER_CONTRACTS_NUMERATORS", GetMessage("DOCDESIGNER_CONTRACTS_NUMERATORS"), "", Array("multiselectbox", $arNumerators)),
            /*GetMessage("DOCDESIGNER_CRM_DISK_HEADER"),
            array("DOCDESIGNER_CRM_DISK_DEAL", GetMessage("DOCDESIGNER_CRM_DISK_DEAL"), "", Array("selectbox", $arDiskDeal)),
            array("DOCDESIGNER_CRM_DISK_COMPANY", GetMessage("DOCDESIGNER_CRM_DISK_COMPANY"), "", Array("selectbox", $arDiskCompany)),
            array("DOCDESIGNER_CRM_DISK_CONTACT", GetMessage("DOCDESIGNER_CRM_DISK_CONTACT"), "", Array("selectbox", $arDiskContact)),
            array("DOCDESIGNER_CRM_DISK_LEAD", GetMessage("DOCDESIGNER_CRM_DISK_LEAD"), "", Array("selectbox", $arDiskLead)),*/
            GetMessage("DOCDESIGNER_CLOUD_DOCX_HEADER"),
            array("DOCDESIGNER_CLOUD_DOCX", GetMessage("DOCDESIGNER_CLOUD_DOCX"), "", Array("checkbox")),
            Array("DOCDESIGNER_CLOUD_DOCX_HINT", "", BeginNote().GetMessage("DOCDESIGNER_CLOUD_DOCX_HINT").EndNote(), Array("statichtml", "")),
            array("DOCDESIGNER_CLOUD_MCRYPT", GetMessage("DOCDESIGNER_CLOUD_MCRYPT"), "", Array("checkbox")),
            Array("DOCDESIGNER_CLOUD_MCRYPT_HINT", "", BeginNote().GetMessage("DOCDESIGNER_CLOUD_MCRYPT_HINT").EndNote(), Array("statichtml", "")));
		$arAllOptions['invoices'] = array(
			//array("DOCDESIGNER_INVOICES_REGISTER", GetMessage("DOCDESIGNER_INVOICES_LIST"), "", Array("selectbox", $arSelectIBlock)),
			array('DOCDESIGNER_INVOICES_START_NUMBER', GetMessage('DOCDESIGNER_CONTRACTS_START_NUMBER'), "1", Array("text", 10)),
			array('DOCDESIGNER_INVOICES_LENGTH_NUMBER', GetMessage('DOCDESIGNER_CONTRACTS_LENGTH_NUMBER'), "3", Array("text", 5)),
			array('DOCDESIGNER_INVOICES_RESET_NUMBER', GetMessage('DOCDESIGNER_CONTRACTS_RESET_NUMBER'), "Y", Array("checkbox")),
			array('DOCDESIGNER_INVOICES_AUTO_NUMBER', GetMessage('DOCDESIGNER_CONTRACTS_AUTO_NUMBER'), "Y", Array("checkbox")),
			array('DOCDESIGNER_INVOICE_TYPE', GetMessage('DOCDESIGNER_INVOICE_TYPE'), "", Array("selectbox", $arType)),
			array("DOCDESIGNER_INVOICE_BDT", GetMessage("DOCDESIGNER_INVOICE_BDT"), "INVOICE", Array("text", 10)));
		$arAllOptions['offers'] = array(
			array("DOCDESIGNER_OFFERS", GetMessage("DOCDESIGNER_OFFERS"), "", Array("selectbox", $arSelectCatalog)),
			array("DOCDESIGNER_OFFER_PRICE", GetMessage("DOCDESIGNER_OFFER_PRICE"), "", Array("selectbox", $arOfferProp)));
        /*
		$arAllOptions['company'] = array(
			GetMessage("DOCDESIGNER_CRM_COMPANY_ID_DATA"),
			array('DOCDESIGNER_CRM_COMPANY_OGRN', GetMessage('DOCDESIGNER_CRM_COMPANY_OGRN'), '', Array("selectbox", $arUFCompany)),
			array('DOCDESIGNER_CRM_COMPANY_INN', GetMessage('DOCDESIGNER_CRM_COMPANY_INN'), '', Array("selectbox", $arUFCompany)),
			array('DOCDESIGNER_CRM_COMPANY_KPP', GetMessage('DOCDESIGNER_CRM_COMPANY_KPP'), '', Array("selectbox", $arUFCompany)),
			GetMessage("DOCDESIGNER_CRM_COMPANY_BANK_DETAILS"),
			array('DOCDESIGNER_CRM_COMPANY_RS', GetMessage('DOCDESIGNER_CRM_COMPANY_RS'), '', Array("selectbox", $arUFCompany)),
			//array('DOCDESIGNER_CRM_COMPANY_BANK', GetMessage('DOCDESIGNER_CRM_COMPANY_BANK'), '', Array("selectbox", $arUFCompany)),
			array('DOCDESIGNER_CRM_COMPANY_BANK_NAME', GetMessage('DOCDESIGNER_CRM_COMPANY_BANK_NAME'), '', Array("selectbox", $arUFCompany)),
			array('DOCDESIGNER_CRM_COMPANY_BANK_BIK', GetMessage('DOCDESIGNER_CRM_COMPANY_BANK_BIK'), '', Array("selectbox", $arUFCompany)),
			array('DOCDESIGNER_CRM_COMPANY_BANK_KS', GetMessage('DOCDESIGNER_CRM_COMPANY_BANK_KS'), '', Array("selectbox", $arUFCompany)),
			array('DOCDESIGNER_CRM_COMPANY_BANK_CITY', GetMessage('DOCDESIGNER_CRM_COMPANY_BANK_CITY'), '', Array("selectbox", $arUFCompany)),
			GetMessage("DOCDESIGNER_CRM_COMPANY_OTHER"),
			array('DOCDESIGNER_CRM_COMPANY_POST_ADDRESS', GetMessage('DOCDESIGNER_CRM_COMPANY_POST_ADDRESS'), '', Array("selectbox", $arUFCompany)),
			array('DOCDESIGNER_CRM_COMPANY_PHONE', GetMessage('DOCDESIGNER_CRM_COMPANY_PHONE'), '', Array("selectbox", $arUFCompany)));
        */
	//);
    if($IsCrm){
      $arAllOptions['contracts'][] = GetMessage("DOCDESIGNER_CRM_DISK_HEADER");
      $arAllOptions['contracts'][] = array("DOCDESIGNER_CRM_DISK_DEAL", GetMessage("DOCDESIGNER_CRM_DISK_DEAL"), "", Array("selectbox", $arDiskDeal));
      $arAllOptions['contracts'][] = array("DOCDESIGNER_CRM_DISK_COMPANY", GetMessage("DOCDESIGNER_CRM_DISK_COMPANY"), "", Array("selectbox", $arDiskCompany));
      $arAllOptions['contracts'][] = array("DOCDESIGNER_CRM_DISK_CONTACT", GetMessage("DOCDESIGNER_CRM_DISK_CONTACT"), "", Array("selectbox", $arDiskContact));
      $arAllOptions['contracts'][] = array("DOCDESIGNER_CRM_DISK_LEAD", GetMessage("DOCDESIGNER_CRM_DISK_LEAD"), "", Array("selectbox", $arDiskLead));
    }

    //$arAllOptions['lists'][] = GetMessage("DOCDESIGNER_LISTS_TITLE");

	$arAllOptions['variables'][] = GetMessage("DOCDESIGNER_CRM_COMPANY_TITLE");
	$arAllOptions['variables'][] = Array("", "", GetMessage('DOCDESIGNER_VARIABLE_NAME'), Array("statichtml", ""));
	foreach($arVariables['COMPANY'] as $FNAME => $arValue){
		$arAllOptions['variables'][] = array("DOCDESIGNER_VARIABLES_COMPANY_" . $FNAME, ($arValue['NAME']), $arValue['XML_ID'], Array("text", 30));
	}
	foreach($arCrmMulti as $VarName => $varDesc){
		$arAllOptions['variables'][] = Array("", $varDesc, "<b>\${COMPANY.".$VarName."}</b>", Array("statichtml", ""));
	}
	$arAllOptions['variables'][] = GetMessage("DOCDESIGNER_CRM_DEAL_TITLE");
	$arAllOptions['variables'][] = Array("", "", GetMessage('DOCDESIGNER_VARIABLE_NAME'), Array("statichtml", ""));
	foreach($arVariables['DEAL'] as $FNAME => $arValue){
		$arAllOptions['variables'][] = array("DOCDESIGNER_VARIABLES_DEAL_" . $FNAME, $arValue['NAME'], $arValue['XML_ID'], Array("text", 30));
	}
	$arDeal['DATE_CREATE'] = GetMessage('DATE_CREATE');
	//$arDeal['OPENED'] = GetMessage('OPENED');
	$arDeal['STAGE_ID'] = GetMessage('STAGE_ID');
	$arDeal['CLOSED'] = GetMessage('CLOSED');
	$arDeal['TYPE_ID'] = GetMessage('TYPE_ID');
	$arDeal['OPPORTUNITY'] = GetMessage('OPPORTUNITY');
	$arDeal['CURRENCY_ID'] = GetMessage('CURRENCY_ID');
	$arDeal['PROBABILITY'] = GetMessage('PROBABILITY');
	$arDeal['COMMENTS'] = GetMessage('COMMENTS');
	$arDeal['BEGINDATE'] = GetMessage('BEGINDATE');
	$arDeal['CLOSEDATE'] = GetMessage('CLOSEDATE');
	$arDeal['TITLE'] = GetMessage('TITLE');
	foreach($arDeal as $var => $name){
		$arAllOptions['variables'][] = Array("", $name, "<b>\${DEAL.".$var."}</b>", Array("statichtml", ""));
	}

	$arAllOptions['variables'][] = GetMessage("DOCDESIGNER_CRM_CONTACT_TITLE");
	$arAllOptions['variables'][] = Array("", "", GetMessage('DOCDESIGNER_VARIABLE_NAME'), Array("statichtml", ""));
	if($arVariables['CONTACT']){
		foreach($arVariables['CONTACT'] as $FNAME => $arValue){
			$arAllOptions['variables'][] = array("DOCDESIGNER_VARIABLES_CONTACT_" . $FNAME, $arValue['NAME'], $arValue['XML_ID'], Array("text", 30));
		}
	}

	$arAllOptions['variables'][] = Array("", GetMessage('CONTACT.NAME'), "<b>\${CONTACT.NAME}</b>", Array("statichtml", ""));
	$arAllOptions['variables'][] = Array("", GetMessage('CONTACT.FULL_NAME'), "<b>\${CONTACT.FULL_NAME}</b>", Array("statichtml", ""));
	$arAllOptions['variables'][] = Array("", GetMessage('CONTACT.ADDRESS'), "<b>\${CONTACT.ADDRESS}</b>", Array("statichtml", ""));
	$arAllOptions['variables'][] = Array("", GetMessage('CONTACT.POST'), "<b>\${CONTACT.POST}</b>", Array("statichtml", ""));
	foreach($arCrmMulti as $VarName => $varDesc){
		$arAllOptions['variables'][] = Array("", $varDesc, "<b>\${CONTACT.".$VarName."}</b>", Array("statichtml", ""));
	}
    //echo '123';
    //echo '<pre>'; print_r($arAllOptions['variables']); echo '</pre>';
    //AddMessage2Log(print_r($arVariables, true));
    $arAllOptions['variables'][] = GetMessage("DOCDESIGNER_CRM_LEAD_TITLE");
	$arAllOptions['variables'][] = Array("", "", GetMessage('DOCDESIGNER_VARIABLE_NAME'), Array("statichtml", ""));
	if($arVariables['LEAD']){
		foreach($arVariables['LEAD'] as $FNAME => $arValue){
			$arAllOptions['variables'][] = array("DOCDESIGNER_VARIABLES_LEAD_" . $FNAME, $arValue['NAME'], $arValue['XML_ID'], Array("text", 30));
		}
	}
    $arLead['DATE_CREATE'] = GetMessage('DATE_CREATE');
	//$arDeal['OPENED'] = GetMessage('OPENED');
	$arLead['STATUS_ID'] = GetMessage('STATUS_ID');
	//$arLead['CLOSED'] = GetMessage('CLOSED');
	//$arLead['TYPE_ID'] = GetMessage('TYPE_ID');
	$arLead['OPPORTUNITY'] = GetMessage('OPPORTUNITY');
	$arLead['CURRENCY_ID'] = GetMessage('CURRENCY_ID');
	//$arLead['PROBABILITY'] = GetMessage('PROBABILITY');
	$arLead['COMMENTS'] = GetMessage('COMMENTS');
	//$arLead['BEGINDATE'] = GetMessage('BEGINDATE');
	//$arLead['CLOSEDATE'] = GetMessage('CLOSEDATE');
	$arLead['TITLE'] = GetMessage('LEAD_TITLE');
    $arLead['COMPANY_TITLE'] = GetMessage('COMPANY_TITLE');
  	$arLead['LAST_NAME'] = GetMessage('LAST_NAME');
  	$arLead['NAME'] = GetMessage('NAME');
  	$arLead['SECOND_NAME'] = GetMessage('SECOND_NAME');
	foreach($arLead as $var => $name){
		$arAllOptions['variables'][] = Array("", $name, "<b>\${LEAD.".$var."}</b>", Array("statichtml", ""));
	}

    //numerators - start
    $arSelect = Array("ID", "NAME");
	$arFilter = Array("IBLOCK_CODE"=>"htmls_numerators", "ACTIVE"=>"Y");
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
	$arSelectNums['no'] = GetMessage("DOCDESIGNER_NO_SELECT");
	while($ob = $res->GetNextElement()){
		$arFields = $ob->GetFields();
		$arSelectNums[$arFields['ID']] = $arFields['NAME'];
	}
    $arDeclinators['inner'] = GetMessage('INNER_DECLINATOR');
    $arDeclinators['morpher'] = GetMessage('MORPHER');
    $arAllOptions["v8exchange"][] = array('DOCDESIGNER_DECLINATOR', GetMessage('DOCDESIGNER_DECLINATOR'), 'INNER', Array("selectbox", $arDeclinators));
	//$arAllOptions["v8exchange"][] = array('DOCDESIGNER_NUMERATOR_1C', GetMessage('DOCDESIGNER_NUMERATOR_1C'), '', Array("selectbox", $arSelectNums));
	//numerators - end

	//v8exchange - start
	//$arAllOptions["v8exchange"][] = array('DOCDESIGNER_V8EXCHANGE_FILE_EXPORT', GetMessage('DOCDESIGNER_V8EXCHANGE_FILE_EXPORT'), '', Array("checkbox"));
	//$path2epf = "/bitrix/modules/".$module_id."/v8exchange/1CV8-BXCRM.epf";
	//$v8epf = "<a href='".$path2epf."' onclick=\"jsUtils.Redirect([], 'fileman_file_download.php?path=".$path2epf."&site=s1&lang=ru'); return false;\">".GetMessage("V8EXCHANGE_DOWNLOAD")."</a>";
	//$arAllOptions["v8exchange"][] = GetMessage("DOCDESIGNER_V8EXCHANGE_CONFIGURATIONS");
	//$arAllOptions["v8exchange"][] = Array("", GetMessage("DOCDESIGNER_V8EXCHANGE_EPF"), $v8epf, Array("statichtml"));
	$arAllOptions["v8exchange"][] = GetMessage("DOCDESIGNER_ADDMESSAGE2LOG_HEADER");
	$arAllOptions["v8exchange"][] = Array("DOCDESIGNER_ADDMESSAGE2LOG", GetMessage("DOCDESIGNER_ADDMESSAGE2LOG"), '', Array("checkbox"));
	//v8exchange - end

    //crm:invoice - start
    if($corp > 0 && $IsCrm){

        $res = CIBlockElement::GetList(array('NAME' => 'ASC'), array('IBLOCK_ID' => $corp));
        if($res->SelectedRowsCount() > 0){
          CModule::IncludeModule("sale");
            $arSelectPS = array();
            $db_ptype = CSalePaySystem::GetList($arOrder = Array("SORT"=>"ASC", "PSA_NAME"=>"ASC"), Array());
    		while ($ptype = $db_ptype->Fetch()){
    			$arSelectPS[$ptype['ID']] = $ptype['NAME'];
    		}
          while($ob = $res->GetNextElement()){
            $arFields = $ob->GetFields();
            $arAllOptions["crm_invoice"][] = array('DOCDESIGNER_CRM_INVOCE_COMPANY_'.$arFields['ID'], $arFields['NAME'] . '.' . GetMessage('DOCDESIGNER_CRM_COMPANY_TITLE'), '', Array("selectbox", $arSelectPS));
            $arAllOptions["crm_invoice"][] = array('DOCDESIGNER_CRM_INVOCE_CONTACT_'.$arFields['ID'], $arFields['NAME'] . '.' . GetMessage('DOCDESIGNER_CRM_CONTACT_TITLE'), '', Array("selectbox", $arSelectPS));
          }
        }
        else{
          $arAllOptions["crm_invoice"][] = GetMessage("DOCDESIGNER_CRM_INVOCE_NO_CORP");
        }
    }
    //crm:invoice - end

	$arAllOptions["tmp"][] = GetMessage("DOCDESIGNER_TAB_PROD_IBLOCK_ALT");

	if ($REQUEST_METHOD=="POST" && $DocDesigner_RIGHT=="W" && check_bitrix_sessid() && $_REQUEST["SUpdate"]){
		foreach($arAllOptions as $aOptGroup){
			foreach($aOptGroup as $option){
			 	__AdmSettingsSaveOption($module_id, $option);
			}
		}
	}

	$tabControl = new CAdminTabControl("tabControl", $aTabs);
	function ShowParamsHTMLByArray($arParams){
		global $module_id;
		foreach($arParams as $Option){
		 	__AdmSettingsDrawRow($module_id, $Option);
		}
	}
	$tabControl->Begin();
	?><form method="POST" action="<?echo $APPLICATION->GetCurPage()?>?mid=<?=htmlspecialchars($mid)?>&lang=<?=LANGUAGE_ID?>"><?
	bitrix_sessid_post();

	$tabControl->BeginNextTab();//contracts
	ShowParamsHTMLByArray($arAllOptions["contracts"]);
	$tabControl->BeginNextTab();//own entities
	//ShowParamsHTMLByArray($arAllOptions["main"]);

	for ($i = 0; $i < count($arAllOptions['main']); $i++):
		$Option = $arAllOptions['main'][$i];
		$val = COption::GetOptionString($module_id, $Option[0], $Option[2]);

		$type = $Option[3];
		?>
		<tr>
			<td valign="top" width="50%"><?
				if ($type[0]=="checkbox")
					echo "<label for=\"".htmlspecialchars($Option[0])."\">".$Option[1]."</label>";
				else
					echo $Option[1];
			?></td>
			<td valign="middle" width="50%">
				<?if($type[0]=="checkbox"):?>
					<input type="checkbox" name="<?echo htmlspecialchars($Option[0])?>" id="<?echo htmlspecialchars($Option[0])?>" value="Y"<?if($val=="Y")echo" checked";?>>
				<?elseif($type[0]=="text"):?>
					<input type="text" size="<?echo $type[1]?>" value="<?echo htmlspecialchars($val)?>" name="<?echo htmlspecialchars($Option[0])?>">
				<?elseif($type[0]=="textarea"):?>
					<textarea rows="<?echo $type[1]?>" cols="<?echo $type[2]?>" name="<?echo htmlspecialchars($Option[0])?>"><?echo htmlspecialchars($val)?></textarea>
				<?elseif($type[0]=="selectbox"):?>
					<?if($Option[0] == "DOCDESIGNER_COMPANY"):?>
						<select name="<?echo htmlspecialchars($Option[0])?>" id="<?echo htmlspecialchars($Option[0])?>" onchange="StartScan(this.value);">
					<?else:?>
						<select name="<?echo htmlspecialchars($Option[0])?>" id="<?echo htmlspecialchars($Option[0])?>">
					<?endif;?>
						<?foreach($Option[3][1] as $v => $k)
						{
							?><option value="<?=$v?>"<?if($val==$v)echo" selected";?>><?=$k?></option><?
						}
						?>
					</select>
				<?endif?>
			</td>
		</tr>
	<?endfor;?>
	<?
    /*
	if($IsCrm){
		$tabControl->BeginNextTab();//company
		ShowParamsHTMLByArray($arAllOptions["company"]);
	}*/
    if($corp > 0 && $IsCrm){
      $tabControl->BeginNextTab();//crm:invoce
      ShowParamsHTMLByArray($arAllOptions["crm_invoice"]);
    }
    //$tabControl->BeginNextTab();//lists
	//ShowParamsHTMLByArray($arAllOptions["lists"]);
	$tabControl->BeginNextTab();//offers
	//ShowParamsHTMLByArray($arAllOptions["templates"]);
	ShowParamsHTMLByArray($arAllOptions["v8exchange"]);

    if($IsCrm){
		$tabControl->BeginNextTab();//variables
		?>
		<tr>
			<td valign="top" width="100%" colspan="2"><?
				echo BeginNote().GetMessage("DOCDESIGNER_VARIABLES_HINT").EndNote();
		?>
			</td>
		</tr>
		<?
		ShowParamsHTMLByArray($arAllOptions["variables"]);
    }

	$tabControl->BeginNextTab();//right
	require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/admin/group_rights.php");
	if($SHOW_SERVICE){
		$tabControl->BeginNextTab();//service
		ShowParamsHTMLByArray($arAllOptions["service"]);
	}

	$tabControl->Buttons();
	?>
	<script language="JavaScript">
	function RestoreDefaults()
	{
		if (confirm('<?echo AddSlashes(GetMessage("MAIN_HINT_RESTORE_DEFAULTS_WARNING"))?>'))
			window.location = "<?echo $APPLICATION->GetCurPage()?>?RestoreDefaults=Y&lang=<?echo LANG?>&mid=<?echo urlencode($mid)."&".bitrix_sessid_get();?>";
	}
	</script>

	<input type="submit" <?if ($DocDesigner_RIGHT<"W") echo "disabled" ?> name="SUpdate" value="<?echo GetMessage("MAIN_SAVE")?>">
	<input type="hidden" name="Update" value="Y">
	<input type="reset" name="reset" value="<?echo GetMessage("MAIN_RESET")?>">
	<?
	$tabControl->End();
	?>
	</form>
<?
endif;
?>
