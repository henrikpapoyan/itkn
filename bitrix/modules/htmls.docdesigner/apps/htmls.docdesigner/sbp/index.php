<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
require_once($_SERVER["DOCUMENT_ROOT"].'/bitrix/modules/htmls.docdesigner/lang/ru/apps/htmls.docdesigner.php');
$APPLICATION->SetTitle(GetMessage('menuBizProc'));
IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/bitrix24/public/crm/configs/bp/index.php");
CModule::IncludeModule('bizproc');
//$APPLICATION->SetTitle(GetMessage("TITLE"));
?><?$APPLICATION->IncludeComponent("bitrix:menu", "htmls.docdesigner", array(
	"ROOT_MENU_TYPE" => "left",
	"MENU_CACHE_TYPE" => "N",
	"MENU_CACHE_TIME" => "3600",
	"MENU_CACHE_USE_GROUPS" => "Y",
	"MENU_CACHE_GET_VARS" => array(
	),
	"MAX_LEVEL" => "1",
	"CHILD_MENU_TYPE" => "left",
	"USE_EXT" => "N",
	"DELAY" => "N",
	"ALLOW_MULTI_SELECT" => "N"
	),
	false
);?>
<br />
<?$APPLICATION->IncludeComponent(
	"bitrix:bizproc.wizards",
	"htmls.bizproc.wizards",
	array(
		"IBLOCK_TYPE" => "bizproc_iblockx",
		"ADMIN_ACCESS" => array(
			0 => "1",
		),
		"SEF_MODE" => "Y",
		"SEF_FOLDER" => "/apps/htmls.docdesigner/sbp/",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"SET_TITLE" => "Y",
		"SET_NAV_CHAIN" => "Y",
		"SKIP_BLOCK" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"COMPONENT_TEMPLATE" => "htmls.bizproc.wizards",
		"SEF_URL_TEMPLATES" => array(
			"index" => "index.php",
			"new" => "new.php",
			"list" => "#block_id#/",
			"view" => "#block_id#/view-#bp_id#.php",
			"start" => "#block_id#/start.php",
			"edit" => "#block_id#/edit.php",
			"task" => "#block_id#/task-#task_id#.php",
			"bp" => "#block_id#/bp.php",
			"setvar" => "#block_id#/setvar.php",
			"log" => "#block_id#/log-#bp_id#.php",
		)
	),
	false
);?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>