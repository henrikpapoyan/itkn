<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Дубли");
CModule::IncludeModule('crm');
$urls = "https://".$_SERVER["SERVER_NAME"]."/local/php_interface/ofd.bitrix24/ajax/guid-verifity.php";
?>
<link href="https://ajax.aspnetcdn.com/ajax/jquery.ui/1.10.4/themes/black-tie/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/jquery.ui/1.10.4/jquery-ui.min.js"></script>
<style>
    /* Базовые стили формы */
    .button-duble {
        margin: 0 auto;

        box-sizing: border-box;
        padding: 40px;
        border-radius: 5px;
        background: RGBA(255, 255, 255, 1);
        -webkit-box-shadow: 0px 0px 15px 0px rgba(0, 0, 0, .45);
        box-shadow: 0px 0px 15px 0px rgba(0, 0, 0, .45);
    }
    /* Базовые стили кнопки */
    .button-one {
        height: 50px;
        width: 100%;
        border-radius: 3px;
        border: rgba(0, 0, 0, .3) 0px solid;
        box-sizing: border-box;
        padding: 10px;
        background: red;
        color: #FFF;
        font-family: 'Open Sans', sans-serif;
        font-weight: 400;
        font-size: 16pt;
        transition: background .4s;
        cursor: pointer;
    }

    .button-two {
        height: 50px;
        width: 100%;
        border-radius: 3px;
        border: rgba(0, 0, 0, .3) 0px solid;
        box-sizing: border-box;
        padding: 10px;
        background: green;
        color: #FFF;
        font-family: 'Open Sans', sans-serif;
        font-weight: 400;
        font-size: 16pt;
        transition: background .4s;
        cursor: pointer;
    }

    .button-tree {
        height: 50px;
        width: 100%;
        border-radius: 3px;
        border: rgba(0, 0, 0, .3) 0px solid;
        box-sizing: border-box;
        padding: 10px;
        background: blue;
        color: #FFF;
        font-family: 'Open Sans', sans-serif;
        font-weight: 400;
        font-size: 16pt;
        transition: background .4s;
        cursor: pointer;
    }

    /* Изменение фона кнопки при наведении */
    .button-one:hover {
        background: #FA4B4B;
    }

    .button-two:hover {
        background: #80b438;
    }

    .button-tree:hover {
        background: #4048F0;
    }
    .example{background:#FFF;width:50%;font-size:80%;border:1px #000 solid;margin:20px 0 20px 40%;padding:15px;position:relative;-moz-border-radius: 3px;-webkit-border-radius: 3px}
    h3 {text-align:center}

    .pbar .ui-progressbar-value {display:block !important}
    .pbar {overflow: hidden}
    .percent {position:relative;text-align: right;}
    .elapsed {position:relative;text-align: right;}

    /**
     * Shadow
     */
    .button::before {
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        -webkit-box-shadow: #959595 0 2px 5px;
        -moz-box-shadow: #959595 0 2px 5px;
        border-radius: 3px;
        box-shadow: #959595 0 2px 5px;
        content: "";
        display: block;
        height: 100%;
        left: 0;
        padding: 2px 0 0;
        position: absolute;
        top: 0;
        width: 100%; }

    .button:active::before { padding: 1px 0 0; }

    /**
     * Grey
     */
    .button {
        -moz-box-shadow: inset 0 0 0 1px #63ad0d;
        -webkit-box-shadow: inset 0 0 0 1px #63ad0d;
        -moz-border-radius: 3px;
        -webkit-border-radius: 3px;
        background: #eee;
        background: -webkit-gradient(linear, 0 0, 0 bottom, from(#eee), to(#e2e2e2));
        background: -moz-linear-gradient(#eee, #e2e2e2);
        background: linear-gradient(#eee, #e2e2e2);
        border: solid 1px #d0d0d0;
        border-bottom: solid 3px #b2b1b1;
        border-radius: 3px;
        box-shadow: inset 0 0 0 1px #f5f5f5;
        color: #555;
        display: inline-block;
        margin: 0 ;
        padding: 10px;
        position: relative;
        text-align: center;
        text-decoration: none;
        text-shadow: 0 1px 0 #fafafa;

        height: 50px;
        width: 100%;

        font-family: 'Open Sans', sans-serif;
        font-weight: 400;
        font-size: 16pt;
        transition: background .4s;
        cursor: pointer;
    }

    .button:hover {
        background: #e4e4e4;
        background: -webkit-gradient(linear, 0 0, 0 bottom, from(#e4e4e4), to(#ededed));
        background: -moz-linear-gradient(#e4e4e4, #ededed);
        background: linear-gradient(#e4e4e4, #ededed);
        border: solid 1px #c2c2c2;
        border-bottom: solid 3px #b2b1b1;
        box-shadow: inset 0 0 0 1px #efefef; }

    .button:active {
        background: #dfdfdf;
        background: -webkit-gradient(linear, 0 0, 0 bottom, from(#dfdfdf), to(#e3e3e3));
        background: -moz-linear-gradient(#dfdfdf, #e3e3e3);
        background: linear-gradient(#dfdfdf, #e3e3e3);
        border: solid 1px #959595;
        box-shadow: inset 0 10px 15px 0 #c4c4c4;
        top:2px;}


    /**
     * Orange
     */
    .button.orange {
        background: #feda71;
        background: -webkit-gradient(linear, 0 0, 0 bottom, from(#feda71), to(#febe4d));
        background: -moz-linear-gradient(#feda71, #febe4d);
        background: linear-gradient(#feda71, #febe4d);
        border: solid 1px #eab551;
        border-bottom: solid 3px #b98a37;
        box-shadow: inset 0 0 0 1px #fee9aa;
        color: #996633;
        text-shadow: 0 1px 0 #fedd9b; }

    .button.orange:hover {
        background: #fec455;
        background: -webkit-gradient(linear, 0 0, 0 bottom, from(#fec455), to(#fecd61));
        background: -moz-linear-gradient(#fec455, #fecd61);
        background: linear-gradient(#fec455, #fecd61);
        border: solid 1px #e6a93d;
        border-bottom: solid 3px #b98a37;
        box-shadow: inset 0 0 0 1px #fedb98; }

    .button.orange:active {
        background: #f9bd4f;
        background: -webkit-gradient(linear, 0 0, 0 bottom, from(#f9bd4f), to(#f0b64d));
        background: -moz-linear-gradient(#f9bd4f, #f0b64d);
        background: linear-gradient(#f9bd4f, #f0b64d);
        border: solid 1px #a77f35;
        box-shadow: inset 0 10px 15px 0 #dba646; }

    /**
     * Twitter - Special Edition
     */
    .button.twitter {
        background: #9fd6fa;
        background: -webkit-gradient(linear, 0 0, 0 bottom, from(#9fd6fa), to(#6bb9f7));
        background: -moz-linear-gradient(#9fd6fa, #6bb9f7);
        background: linear-gradient(#9fd6fa, #6bb9f7);
        border: solid 1px #72bdf4;
        border-bottom: solid 3px #4a9de1;
        box-shadow: inset 0 0 0 1px #bfe4fc;
        color: #fff;
        text-shadow: 0 1px 0 #4598f3; }

    .button.twitter:hover {
        background: #6bb9f7;
        background: -webkit-gradient(linear, 0 0, 0 bottom, from(#6bb9f7), to(#9fd6fa));
        background: -moz-linear-gradient(#6bb9f7, #9fd6fa);
        background: linear-gradient(#6bb9f7, #9fd6fa);
        border: solid 1px #72bdf4;
        border-bottom: solid 3px #4a9de1;
        box-shadow: inset 0 0 0 1px #bfe4fc; }

    .button.twitter:active {
        background: #6bb9f7;
        background: -webkit-gradient(linear, 0 0, 0 bottom, from(#6bb9f7), to(#9fd6fa));
        background: -moz-linear-gradient(#6bb9f7, #9fd6fa);
        background: linear-gradient(#6bb9f7, #9fd6fa);
        border: solid 1px #72bdf4;
        box-shadow: inset 0 10px 15px 0 #50aaf3; }

    /**
     * Facebook - Special Edition
     */
    .button.facebook {
        background: #99b6df;
        background: -webkit-gradient(linear, 0 0, 0 bottom, from(#99b6df), to(#638ec8));
        background: -moz-linear-gradient(#99b6df, #638ec8);
        background: linear-gradient(#99b6df, #638ec8);
        border: solid 1px #6d94ce;
        border-bottom: solid 3px #3867ac;
        box-shadow: inset 0 0 0 1px #bbcfeb;
        color: #fff;
        text-shadow: 0 1px 0 #3c61ab; }

    .button.facebook:hover {
        background: #638ec8;
        background: -webkit-gradient(linear, 0 0, 0 bottom, from(#638ec8), to(#99b6df));
        background: -moz-linear-gradient(#638ec8, #99b6df);
        background: linear-gradient(#638ec8, #99b6df);
        border: solid 1px #6d94ce;
        border-bottom: solid 3px #3867ac;
        box-shadow: inset 0 0 0 1px #bbcfeb; }

    .button.facebook:active {
        background: #638ec8;
        background: -webkit-gradient(linear, 0 0, 0 bottom, from(#638ec8), to(#99b6df));
        background: -moz-linear-gradient(#638ec8, #99b6df);
        background: linear-gradient(#638ec8, #99b6df);
        border: solid 1px #6d94ce;
        box-shadow: inset 0 10px 15px 0 #4176c4; }

    /**
     * XBOX - Special Edition
     */
    .button.xbox {
        background: #c4e125;
        background: -webkit-gradient(linear, 0 0, 0 bottom, from(#c4e125), to(#88a819));
        background: -moz-linear-gradient(#c4e125, #88a819);
        background: linear-gradient(#c4e125, #88a819);
        border: solid 1px #829c15;
        border-bottom: solid 3px #819d15;
        box-shadow: inset 0 0 0 1px #c6da7b;
        color: #fff;
        text-shadow: 0 1px 0 #819d15; }

    .button.xbox:hover {
        background: #88a819;
        background: -webkit-gradient(linear, 0 0, 0 bottom, from(#88a819), to(#c4e125));
        background: -moz-linear-gradient(#88a819, #c4e125);
        background: linear-gradient(#88a819, #c4e125);
        border: solid 1px #829c15;
        border-bottom: solid 3px #819d15;
        box-shadow: inset 0 0 0 1px #c6da7b; }

    .button.xbox:active {
        background: #88a819;
        background: -webkit-gradient(linear, 0 0, 0 bottom, from(#88a819), to(#c4e125));
        background: -moz-linear-gradient(#88a819, #c4e125);
        background: linear-gradient(#88a819, #c4e125);
        border: solid 1px #829c15;
        box-shadow: inset 0 10px 15px 0 #819d15; }

    /**
     * Designmoo - Special Edition
     */
    .button.dsgnmoo {
        background: #f97779;
        background: -webkit-gradient(linear, 0 0, 0 bottom, from(#f97779), to(#ce2424));
        background: -moz-linear-gradient(#f97779, #ce2424);
        background: linear-gradient(#f97779, #ce2424);
        border: solid 1px #be2424;
        border-bottom: solid 3px #bd2524;
        box-shadow: inset 0 0 0 1px #e67e7b;
        color: #fff;
        text-shadow: 0 1px 0 #bd2524; }

    .button.dsgnmoo:hover {
        background: #ce2424;
        background: -webkit-gradient(linear, 0 0, 0 bottom, from(#ce2424), to(#f97779));
        background: -moz-linear-gradient(#ce2424, #f97779);
        background: linear-gradient(#ce2424, #f97779);
        border: solid 1px #be2424;
        border-bottom: solid 3px #bd2524;
        box-shadow: inset 0 0 0 1px #e67e7b; }

    .button.dsgnmoo:active {
        background: #ce2424;
        background: -webkit-gradient(linear, 0 0, 0 bottom, from(#ce2424), to(#f97779));
        background: -moz-linear-gradient(#ce2424, #f97779);
        background: linear-gradient(#ce2424, #f97779);
        border: solid 1px #be2424;
        box-shadow: inset 0 10px 15px 0 #bd2524; }

    q {
        font-weight: 400;
        font-size: 16px;
        font-style: italic; /* Курсивное начертание текста */
        color: #1EB4AA;
        quotes: "\00ab" "\00bb"; /* Кавычки в виде двойных угловых скобок */
    }
    pre{
        font-weight: 400;
        font-size: 16px;
    }
</style>
<div id="myDiv2">
    <form class="button-duble" style="width: 35%;float: left;margin-right: 2%" id="debug"  method="post" onsubmit="return false;">
        <input type="hidden" name="REFR_MASS" value="1">
        <input name="submit" class="button dsgnmoo" type="submit" id="one"
               style="padding: 6px;" value="1. Запустить поиск">
    </form>
</div>
<div style="clear: both;margin-bottom: 10px"></div>
<form id="product_add_result" class="button-duble" style="width: 100%"></form>
<script type="text/javascript">
    $(document).ready(function () {
        $("#one").click(function () { //устанавливаем событие отправки для формы
            //
            //$("#debug").css("display","none");
            $("#product_add_result").empty();
            $("#product_add_result_two").css("display","none");
            $("#del").css("display","block");
            var form_data = $('#debug').serialize(); //собераем все данные из формы
            var url = '<?=$urls?>';
            $.ajax({
                type: "POST", //Метод отправки
                url: url, //путь до php фаила отправителя
                data: form_data,
                success: function(data){
                    // где показываем результат
                    $('#product_add_result').html(data);
                }
            });
        });
    });

</script>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
