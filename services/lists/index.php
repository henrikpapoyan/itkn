<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/intranet/public/services/lists/index.php");
$APPLICATION->SetTitle(GetMessage("SERVICES_TITLE"));
?>

<?$APPLICATION->IncludeComponent(
	"bitrix:lists", 
	".default", 
	array(
		"IBLOCK_TYPE_ID" => "lists",
		"SEF_MODE" => "Y",
		"SEF_FOLDER" => "/services/lists/",
		"CACHE_TYPE" => "Y",
		"CACHE_TIME" => "60",
		"COMPONENT_TEMPLATE" => ".default",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"SEF_URL_TEMPLATES" => array(
			"lists" => "",
			"list" => "#list_id#/view/#section_id#/",
			"list_sections" => "#list_id#/edit/#section_id#/",
			"list_edit" => "#list_id#/edit/",
			"list_fields" => "#list_id#/fields/",
			"list_field_edit" => "#list_id#/field/#field_id#/",
			"list_element_edit" => "#list_id#/element/#section_id#/#element_id#/",
			"list_file" => "#list_id#/file/#section_id#/#element_id#/#field_id#/#file_id#/",
			"bizproc_log" => "#list_id#/bp_log/#document_state_id#/",
			"bizproc_workflow_start" => "#list_id#/bp_start/#element_id#/",
			"bizproc_task" => "#list_id#/bp_task/#section_id#/#element_id#/#task_id#/",
			"bizproc_workflow_admin" => "#list_id#/bp_list/",
			"bizproc_workflow_edit" => "#list_id#/bp_edit/#ID#/",
			"bizproc_workflow_vars" => "#list_id#/bp_vars/#ID#/",
			"bizproc_workflow_constants" => "#list_id#/bp_constants/#ID#/",
			"list_export_excel" => "#list_id#/excel/",
			"catalog_processes" => "catalog_processes/",
		)
	),
	false
);?>

<?
	$uri = $APPLICATION->GetCurUri();
	$idUri = explode("/", $uri);
	$array2 = array("","0","crm", "services", "lists", "view");
	$result = array_diff($idUri,$array2);
	//print_r($result);
	if($result['3']=='76'){
?>

	<script>
        var elements = document.querySelectorAll('span .main-grid-head-title');
        for (var i = 0; i < elements.length; i++) {
            //alert( elements[i].innerHTML ); // "тест", "пройден"
			if(elements[i].innerHTML=="Дата деактивации")
			{
                elements[i].innerHTML="Дата инициации деактивации";
			}
        }

	</script>
<?}?>



<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>