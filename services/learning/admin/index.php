<?php
    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
    $APPLICATION->SetTitle("Панель управления");
    if ( ! $USER->IsAdmin()) {
        ShowError(GetMessage("CRM_PERMISSION_DENIED"));
    } else {
        $APPLICATION->IncludeComponent(
            "bitrix:learning.course.list",
            "admin-panel",
            Array(
                "SORBY"                  => "SORT",
                "SORORDER"               => "ASC",
                "COURSE_DETAIL_TEMPLATE" => "course.php?COURSE_ID=#COURSE_ID#&INDEX=Y",
                "CHECK_PERMISSIONS"      => "Y",
                "COURSES_PER_PAGE"       => "20",
                "SET_TITLE"              => "Y"
            )
        );
    }
    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>